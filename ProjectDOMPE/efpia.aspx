﻿<%@ Page Language="VB" ClassName="Efpia2016" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<script runat="server">
    Sub Page_Load()
        If CheckBrowserCaps() Then Response.End()
    End Sub
    
    Function CheckBrowserCaps() As Boolean
        Dim _strUserAgent As String = Request.ServerVariables("HTTP_USER_AGENT").ToLower
        If _strUserAgent.Contains("googlebot") Then Return True
        If _strUserAgent.Contains("yahoocrawler") Then Return True
        If _strUserAgent.Contains("msnbot") Then Return True
        If _strUserAgent.Contains("slurp") Then Return True
        If _strUserAgent.Contains("altavista") Then Return True
		
        Return False
    End Function
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>www.dompe.com | Trasferimento di valori</title>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/jquery.js"></script>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.widgets.js"></script>
	<script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/ui/jquery-ui.js.axd"></script>
	<script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/efpia.js"></script>
	
	<script>
		$(function () {
			initializeBtns(2016);
			initializeTabs();
		});
		
		//Inizio Foglio 1
		var _filterHCPOnf1 = 0;
		var _prependHtmlHCPf1 = '';
		var _appendHtmlHCPf1 = '';
		
		var _filterHcoOnf1 = 0;
		var _prependHtmlHCOf1 = '';
		var _appendHtmlHCOf1 = '';
		
		$(function () {
			_prependHtmlHCPf1 = getPrependHtmlHcp(11);
			_appendHtmlHCPf1 = getAppendHtmlHcp(11);
			setUpTableHcp(11, _prependHtmlHCPf1, _appendHtmlHCPf1, _filterHCPOnf1);
		
			_prependHtmlHCOf1 = getAppendHtmlHco(12);
			_appendHtmlHCOf1 = getAppendHtmlHco(12)
			setUpTableHcp(12, _prependHtmlHCOf1, _appendHtmlHCOf1, _filterHcoOnf1);
			
			setUpTableHcp(13);
		});
		//Fine Foglio 1
		
		
		//Inizio Foglio 2
		var _filterHCPOnf2 = 0;
		var _prependHtmlHCPf2 = '';
		var _appendHtmlHCPf2 = '';
		
		var _filterHcoOnf2 = 0;
		var _prependHtmlHCOf2 = '';
		var _appendHtmlHCOf2 = '';
		
		$(function () {
			_prependHtmlHCPf2 = getPrependHtmlHcp(21);
			_appendHtmlHCPf2 = getAppendHtmlHcpf2(21);
			setUpTableHcp(21, _prependHtmlHCPf2, _appendHtmlHCPf2, _filterHCPOnf2);
		
			_prependHtmlHCOf2 = getPrependHtmlHco(22);
			_appendHtmlHCOf2 = getAppendHtmlHcof2(22)
			setUpTableHcp(22, _prependHtmlHCOf2, _appendHtmlHCOf2, _filterHcoOnf2);
			
			setUpTableHcp(23);
		});
		//Fine Foglio 2	

		//Inizio Foglio 3
		var _filterHCPOnf3 = 0;
		var _prependHtmlHCPf3 = '';
		var _appendHtmlHCPf3 = '';
		
		var _filterHcoOnf3 = 0;
		var _prependHtmlHCOf3 = '';
		var _appendHtmlHCOf3 = '';
		
		$(function () {
			_prependHtmlHCPf3 = getPrependHtmlHcp(31);
			_appendHtmlHCPf3 = getAppendHtmlHcpf3(31);
			setUpTableHcp(31, _prependHtmlHCPf3, _appendHtmlHCPf3, _filterHCPOnf3);
		
			_prependHtmlHCOf3 = getPrependHtmlHco(32);
			_appendHtmlHCOf3 = getAppendHtmlHcof3(32)
			setUpTableHcp(32, _prependHtmlHCOf3, _appendHtmlHCOf3, _filterHcoOnf3);
			
			setUpTableHcp(33);
		});
		//Fine Foglio 3

		//Inizio Foglio 4
		var _filterHCPOnf4 = 0;
		var _prependHtmlHCPf4 = '';
		var _appendHtmlHCPf4 = '';
		
		var _filterHcoOnf4 = 0;
		var _prependHtmlHCOf4 = '';
		var _appendHtmlHCOf4 = '';
		
		$(function () {
			_prependHtmlHCPf4 = getPrependHtmlHcp(41);
			_appendHtmlHCPf4 = getAppendHtmlHcpf4(41);
			setUpTableHcp(41, _prependHtmlHCPf4, _appendHtmlHCPf4, _filterHCPOnf4);
		
			_prependHtmlHCOf4 = getPrependHtmlHco(42);
			_appendHtmlHCOf4 = getAppendHtmlHcof4(42)
			setUpTableHcp(42, _prependHtmlHCOf4, _appendHtmlHCOf4, _filterHcoOnf4);
			
			setUpTableHcp(43);
		});
		//Fine Foglio 4		

		//Inizio Foglio 5
		var _filterHCPOnf5 = 0;
		var _prependHtmlHCPf5 = '';
		var _appendHtmlHCPf5 = '';
		
		var _filterHcoOnf5 = 0;
		var _prependHtmlHCOf5 = '';
		var _appendHtmlHCOf5 = '';
		
		$(function () {
			_prependHtmlHCPf5 = getPrependHtmlHcp(51);
			_appendHtmlHCPf5 = getAppendHtmlHcpf5(51);
			setUpTableHcp(51, _prependHtmlHCPf5, _appendHtmlHCPf5, _filterHCPOnf5);
		
			_prependHtmlHCOf5 = getPrependHtmlHco(52);
			_appendHtmlHCOf5 = getAppendHtmlHcof5(52)
			setUpTableHcp(52, _prependHtmlHCOf5, _appendHtmlHCOf5, _filterHcoOnf5);
			
			setUpTableHcp(53);
		});
		//Fine Foglio 5
		
		//Inizio Foglio 6
		var _filterHCPOnf6 = 0;
		var _prependHtmlHCPf6 = '';
		var _appendHtmlHCPf6 = '';
		
		var _filterHcoOnf6 = 0;
		var _prependHtmlHCOf6 = '';
		var _appendHtmlHCOf6 = '';
		
		$(function () {
			_prependHtmlHCPf6 = getPrependHtmlHcp(61);
			_appendHtmlHCPf6 = getAppendHtmlHcpf6(61);
			setUpTableHcp(61, _prependHtmlHCPf6, _appendHtmlHCPf6, _filterHCPOnf6);
		
			_prependHtmlHCOf6 = getPrependHtmlHco(62);
			_appendHtmlHCOf6 = getAppendHtmlHcof6(62)
			setUpTableHcp(62, _prependHtmlHCOf6, _appendHtmlHCOf6, _filterHcoOnf6);
			
			setUpTableHcp(63);
		});
		//Fine Foglio 6	

		//Inizio Foglio 7
		var _filterHCPOnf7 = 0;
		var _prependHtmlHCPf7 = '';
		var _appendHtmlHCPf7 = '';
		
		var _filterHcoOnf7 = 0;
		var _prependHtmlHCOf7 = '';
		var _appendHtmlHCOf7 = '';
		
		$(function () {
			_prependHtmlHCPf7 = getPrependHtmlHcp(71);
			_appendHtmlHCPf7 = getAppendHtmlHcpf7(71);
			setUpTableHcp(71, _prependHtmlHCPf7, _appendHtmlHCPf7, _filterHCPOnf7);
		
			_prependHtmlHCOf7 = getPrependHtmlHco(72);
			_appendHtmlHCOf7 = getAppendHtmlHcof7(72)
			setUpTableHcp(72, _prependHtmlHCOf7, _appendHtmlHCOf7, _filterHcoOnf7);
			
			setUpTableHcp(73);
		});
		//Fine Foglio 7	
		
		//Inizio Foglio 8
		var _filterHCPOnf8 = 0;
		var _prependHtmlHCPf8 = '';
		var _appendHtmlHCPf8 = '';
		
		var _filterHcoOnf8 = 0;
		var _prependHtmlHCOf8 = '';
		var _appendHtmlHCOf8 = '';
		
		$(function () {
			_prependHtmlHCPf8 = getPrependHtmlHcp(81);
			_appendHtmlHCPf8 = getAppendHtmlHcpf8(81);
			setUpTableHcp(81, _prependHtmlHCPf8, _appendHtmlHCPf8, _filterHCPOnf8);
		
			_prependHtmlHCOf8 = getPrependHtmlHco(82);
			_appendHtmlHCOf8 = getAppendHtmlHcof8(82)
			setUpTableHcp(82, _prependHtmlHCOf8, _appendHtmlHCOf8, _filterHcoOnf8);
			
			setUpTableHcp(83);
		});
		//Fine Foglio 8	
		
		//Inizio Foglio 9
		var _filterHCPOnf9 = 0;
		var _prependHtmlHCPf9 = '';
		var _appendHtmlHCPf9 = '';
		
		var _filterHcoOnf9 = 0;
		var _prependHtmlHCOf9 = '';
		var _appendHtmlHCOf9 = '';
		
		$(function () {
			_prependHtmlHCPf9 = getPrependHtmlHcp(91);
			_appendHtmlHCPf9 = getAppendHtmlHcpf9(91);
			setUpTableHcp(91, _prependHtmlHCPf9, _appendHtmlHCPf9, _filterHCPOnf9);
		
			_prependHtmlHCOf9 = getPrependHtmlHco(92);
			_appendHtmlHCOf9 = getAppendHtmlHcof9(92)
			setUpTableHcp(92, _prependHtmlHCOf9, _appendHtmlHCOf9, _filterHcoOnf9);
			
			setUpTableHcp(93);
		});
		//Fine Foglio 9	

		
    </script>
	<link rel="stylesheet" href="http://www.dompe.com/ProjectDOMPE/_js/css/jquery-ui.css" />
    <link rel="stylesheet" href="http://www.dompe.com/projectdompe/_css/style.css" />
    <link rel="stylesheet" href="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/css/theme.blue.css" />
	<link rel="stylesheet" href="http://www.dompe.com/projectdompe/_css/efpia.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a href="http://www.dompe.com" title="Domp&eacute; Corporate website - Home Page">
			<img alt="Domp&eacute; Corporate website - Home Page" src="http://www.dompe.com/projectdompe/_slice/logo.gif" /></a>
        </div>
		<div class="widget">
			<a class="ui-button ui-widget ui-corner-all" id="btn2016" href="/projectdompe/efpia.aspx">Trasferimenti di valore 2016</a>
			<a class="ui-button ui-widget ui-corner-all" id="btn2015" href="/projectdompe/efpia_2015.aspx">Trasferimenti di valore 2015</a>
		</div>
        
	<div id="tabs" class="blockwi">
        <ul>
            <li><a href="#tabs-1">Italy / Italia</a></li>
            <li><a href="#tabs-2">Belgium / Belgio</a></li>
            <li><a href="#tabs-3">Czech Republic / Repubblica Ceca</a></li>
			<li><a href="#tabs-4">Germany / Germania</a></li>
			<li><a href="#tabs-5">Poland / Polonia</a></li>
			<li><a href="#tabs-6">Spain / Spagna</a></li>
			<li><a href="#tabs-7">Sweden / Svezia</a></li>
			<li><a href="#tabs-8">Switzerland / Svizzera</a></li>
			<li><a href="#tabs-9">United Kingdom / Regno Unito</a></li>
        </ul>
      
        <div id="tabs-1">
    		<table class="tablesorter11">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

		<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ABENAVOLIGIUSEPPINA MARIA RITA</td><td>Reggiodi Calabria</td><td>IT</td><td>VIA PIOXI TR. PUTORTI' 12</td><td>&nbsp;</td><td>&nbsp;</td><td class=xl75 dir=LTR width=178 style='border-left:none;width:134pt'>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ABENAVOLI SAVERIO ANTONINO</td><td>Campo Calabro</td><td>IT</td><td>VIA S. ANGELO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ACCINNI MASSIMO</td><td>Roma</td><td>IT</td><td>VIA PARASACCHI 206</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ACCORDINI GIORGIO</td><td>Pedemonte</td><td>IT</td><td>VIA DELLA MOLINARA 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ADDIS ANDREA</td><td>Savona</td><td>IT</td><td>VIA PALEOCAPA 22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>81,89</td><td>0</td><td>0</td><td>&nbsp;</td><td>81,89</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ADILETTA GIROLAMO</td><td>Sarno</td><td>IT</td><td>VIA LAVORATE CENTRO 167</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>620,72</td><td>0</td><td>0</td><td>&nbsp;</td><td>620,72</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>AIRO' EDOARDO</td><td>Pisa</td><td>IT</td><td>VIA GIUSEPPE MORUZZI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>125,00</td><td>681,73</td><td>0</td><td>0</td><td>&nbsp;</td><td>806,73</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ALABARDI PATRIZIA</td><td>Gallarate</td><td>IT</td><td>L.GO BOITO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ALFIERI ANTONIO</td><td>Scafati</td><td>IT</td><td>VIA PASSANTI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ALLEGRINI ALDO</td><td>Lucca</td><td>IT</td><td>VIA DELLA CHIESA 800</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>254,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>254,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>AMATI FRANCESCO</td><td>Milano</td><td>IT</td><td>VIA FRANCESCO SFORZA 35</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>249,50</td><td>0</td><td>0</td><td>&nbsp;</td><td>249,50</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ANDREOTTI MARIA FRANCESCA</td><td>Grisignano di Zocco</td><td>IT</td><td>VIA ROSSINI 27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ANTONINI IVANO</td><td>Brescia</td><td>IT</td><td>VIA MARCONI 69</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>APOSTOLO ROMINA</td><td>Cuneo</td><td>IT</td><td>VIA PIER CARLO BOGGIO 12/14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>20,66</td><td>0</td><td>0</td><td>&nbsp;</td><td>20,66</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ARCHITETTO MARIA ANTONIETTA</td><td>Piana degli Albanesi</td><td>IT</td><td>C/O GUARDIA MEDICA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ASSIRELLI BARBARA</td><td>Bologna</td><td>IT</td><td>VIA A. RIGHI N 32</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>27,58</td><td>0</td><td>0</td><td>&nbsp;</td><td>27,58</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>AURICCHIO GIUSEPPE</td><td>Battipaglia</td><td>IT</td><td>VIA AVOGADRO 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>AVERSANO MARIA GLORIA</td><td>Milano</td><td>IT</td><td>PIAZZA DELL'OSPEDALE MAGGIORE 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>277,52</td><td>0</td><td>0</td><td>&nbsp;</td><td>277,52</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>AVINO GENNARO</td><td>Marigliano</td><td>IT</td><td>VIA A. ALISE 88</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>163,93</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>163,93</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BADAGLIACCA GENUEFFO</td><td>Gorizia</td><td>IT</td><td>VIA V.VENETO 171</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>1.206,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.845,67</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BAGGIERI SEBASTIANO SALVATORE</td><td>Milano</td><td>IT</td><td>VIA NOVARA 90</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,05</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,05</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BALOSSI LUCA GIUSEPPE</td><td>Milano</td><td>IT</td><td>P.ZA OSPEDALE MAGGIORE 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>286,71</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.286,71</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BANDI GIULIO</td><td>Anzola dell'Emilia</td><td>IT</td><td>VIA BAIESI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>193,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>193,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BARBACCIA ANTONINO</td><td>Palermo</td><td>IT</td><td>VIA VILLAGRAZIA 435</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,34</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,34</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BARBI EGIDIO</td><td>Trieste</td><td>IT</td><td>VIA DELL'ISTRIA 65/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>208,32</td><td>0</td><td>0</td><td>&nbsp;</td><td>208,32</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BARBIERI MASSIMO</td><td>Bologna</td><td>IT</td><td>VIA AZZO GARDINO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,90</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,90</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BARRANCA VITA</td><td>Partinico</td><td>IT</td><td>VIA BOLOGNA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BASSINO CARLA</td><td>Cantù</td><td>IT</td><td>VIA OSPEDALE 24</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,73</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,73</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BAVETTA ANDREA</td><td>Carini</td><td>IT</td><td>VIA PONTICELLI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BELLOMONTE NUNZIA</td><td>Palermo</td><td>IT</td><td>VIA LENTINI 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BELLUOMINI ROBERTO</td><td>Lucca</td><td>IT</td><td>VIA DI TIGLIO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BENASSI FULVIO</td><td>Roma</td><td>IT</td><td>VIA PORTUENSE 332</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>171,50</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.171,50</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BENEDETTI CLAUDIO</td><td>Ravenna</td><td>IT</td><td>VIA DEI POGGI 74</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>35,10</td><td>0</td><td>0</td><td>&nbsp;</td><td>35,10</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BENEDETTI TOMMASO</td><td>Campi Bisenzio</td><td>IT</td><td>VIA MONTALVO 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>150,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BERGHELLA VINCENZO</td><td>Pescara</td><td>IT</td><td>VIA R.MARGHERITA 53</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>406,13</td><td>0</td><td>0</td><td>&nbsp;</td><td>406,13</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BIANCHI BARBARA</td><td>Fivizzano</td><td>IT</td><td>SALITA S.FRANCESCO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BIANCHI CINZIA</td><td>Barbarano Vicentino</td><td>IT</td><td>VIA CRISPI 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BINDI LUCIA</td><td>Montevarchi</td><td>IT</td><td>VIA CENNANO 80</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>379,25</td><td>0</td><td>0</td><td>&nbsp;</td><td>379,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BISI MARCO</td><td>Parma</td><td>IT</td><td>VIA ORAZIO 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOFFI ROBERTO MARIO ENRICO</td><td>Milano</td><td>IT</td><td>VIA VENEZIAN 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>47,92</td><td>0</td><td>0</td><td>&nbsp;</td><td>47,92</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOI MARIA GRAZIA</td><td>Firenze</td><td>IT</td><td>V.LE MICHELANGELO 41</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>150,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOMBI CLAUDIO</td><td>Bologna</td><td>IT</td><td>VIA CASTIGLIONE 45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>27,58</td><td>0</td><td>0</td><td>&nbsp;</td><td>27,58</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BONETTI RICCARDO</td><td>Ostiglia</td><td>IT</td><td>VIA BONAZZI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>209,17</td><td>0</td><td>0</td><td>&nbsp;</td><td>209,17</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BONGERA PATRIZIA</td><td>Campo Ligure</td><td>IT</td><td>VIA VOLTINO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BONIZZATO MARIACECILIA</td><td>Oppeano</td><td>IT</td><td>VIA XXVI APRILE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BONOMO BERARDINO</td><td>Roma</td><td>IT</td><td>VIA PALLAVICINI 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOSCHI ANDREINA</td><td>Roma</td><td>IT</td><td>VIA IENNER 64</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOTTRIGHI PIETRO</td><td>Piacenza</td><td>IT</td><td>VIA G.TAVERNA 49</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>265,88</td><td>0</td><td>0</td><td>&nbsp;</td><td>265,88</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BOZZA LUIGI</td><td>Napoli</td><td>IT</td><td>VIA MATERDEI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRAGHO' SALVATORE</td><td>Vibo Valentia</td><td>IT</td><td>VIA CIRCONVALLAZIONE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.065,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.625,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRAIDO FULVIO</td><td>Genova</td><td>IT</td><td>L.GO ROSANNA BENZI 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>1.617,35</td><td>3.000,00</td><td>0</td><td>&nbsp;</td><td>4.617,35</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRANDALISE PATRIZIA</td><td>Sedico</td><td>IT</td><td>VIA CADUTI E DISP IN RUS 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRASILIANO PAOLA</td><td>Roma</td><td>IT</td><td>VIA EINSTEIN 20</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRUSASCO VITO</td><td>Genova</td><td>IT</td><td>LARGO ROSANNA BENZI 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>36,68</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.036,68</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRUSEGHIN CHIARA</td><td>Vittorio Veneto</td><td>IT</td><td>PIAZZA FIUME 30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BRUTTI PAOLO</td><td>Verona</td><td>IT</td><td>VIA GARIBALDI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BUGGISANO GIUSEPPE</td><td>Gessate</td><td>IT</td><td>PIAZZA DELLA PACE 11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,03</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,03</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BUJA ADOLFO</td><td>Parma</td><td>IT</td><td>VIA ORAZIO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BUSETTO VALERIA</td><td>Treviso</td><td>IT</td><td>VIA S.ANTONINO 271</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>687,21</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.326,55</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>BUTTAFOCO MASSIMO</td><td>Civitavecchia</td><td>IT</td><td>VIA R. ELENA 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>226,47</td><td>0</td><td>0</td><td>&nbsp;</td><td>226,47</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAGGIANO ELIO</td><td>Avellino</td><td>IT</td><td>VIA LUIGI AMABILE 27C</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>970,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.530,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAI FIORELLA</td><td>Calci</td><td>IT</td><td>VIA BROGIOTTI 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAMARDA MARIA PIA</td><td>Albignasego</td><td>IT</td><td>VIA XVI MARZO 23</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAMERANI ANNALISA</td><td>Bondeno</td><td>IT</td><td>VIA DAZIO 113</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>166,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>166,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAMOIRANO MARINA</td><td>Mondovì</td><td>IT</td><td>VIA SAN ROCCHETTO 99</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAMPO FABIO</td><td>San Cipirello</td><td>IT</td><td>VIA LO MONACO 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAPETTINI ANNA CLIZIA</td><td>Pavia</td><td>IT</td><td>PIAZZALE CAMILLO GOLGI 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>665,00</td><td>331,42</td><td>0</td><td>0</td><td>&nbsp;</td><td>996,42</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAPORIZZO WALTER</td><td>Orte</td><td>IT</td><td>VIA GRAMSCI 57</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>414,87</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.414,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAPPELLETTI TIZIANA</td><td>Milano</td><td>IT</td><td>VIA F.SFORZA 28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>268,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>268,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAPUTO ROSANNA</td><td>Corato</td><td>IT</td><td>C. MED. SOCIALE<spanstyle='mso-spacerun:yes'> </span></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,53</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,53</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARATOZZOLO ANTONINO</td><td>Reggio di Calabria</td><td>IT</td><td>VIA FATA MORGANA 62</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARDAMONE STEFANO</td><td>Pisa</td><td>IT</td><td>VIA MAIORCA 119</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARLUCCI BIAGIO</td><td>Matera</td><td>IT</td><td>VIA MONTESCAGLIOSO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>16,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>16,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARPAGNANO GIOVANNA ELISIANA</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>16,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>16,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARUSO FILIPPO</td><td>Imperia</td><td>IT</td><td>VIA DEL PIANO 149</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CARVALHO MICHAEL PAUL</td><td>Roma</td><td>IT</td><td>P.ZA S.MARIA DELLA PIETA'</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>229,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>229,91</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CASTELLANI VALTER</td><td>Firenze</td><td>IT</td><td>V.LE PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>220,50</td><td>0</td><td>0</td><td>&nbsp;</td><td>220,50</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CASU GAVINO</td><td>Sassari</td><td>IT</td><td>VIA MANNIRONI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>709,32</td><td>0</td><td>0</td><td>&nbsp;</td><td>859,32</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CATTANEO EZIO</td><td>Milano</td><td>IT</td><td>VIA SAN GAETANO 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>217,60</td><td>0</td><td>0</td><td>&nbsp;</td><td>217,60</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CAVALLI ROSSELLA</td><td>Lodi</td><td>IT</td><td>PIAZZALE GOBETTI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>325,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>964,53</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CESTELE MARINA</td><td>Trento</td><td>IT</td><td>VIA VALSUGANA 51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CHECCACCI SERENA</td><td>Firenze</td><td>IT</td><td>V.LE PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>275,36</td><td>0</td><td>0</td><td>&nbsp;</td><td>425,36</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CIACCIO FRANCESCO PAOLO</td><td>Bagheria</td><td>IT</td><td>VIA DIEGO D'AMICO 35</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,33</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CIARALLO GIANFRANCO</td><td>Santorso</td><td>IT</td><td>Via GARZIERE 42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CICCARELLI FEDRA</td><td>Civitanova Marche</td><td>IT</td><td>VIA PIETRO GINEVRI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>730,47</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.369,81</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CICCARELLI MICHELE</td><td>Rozzano</td><td>IT</td><td>V.LE ALESSANDRO MANZONI 56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CIOFALO FRANCESCO</td><td>Sernaglia della Battaglia</td><td>IT</td><td>VIA PIAVE 63/B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CIPOLLA GIUSEPPE</td><td>Lodi</td><td>IT</td><td>P.ZA OSPEDALE 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>299,07</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.299,07</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CITRO AMODIO</td><td>Salerno</td><td>IT</td><td>VIA F. LA FRENCESCA 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>301,13</td><td>0</td><td>0</td><td>&nbsp;</td><td>301,13</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CLEMENTE ROSA</td><td>Bari</td><td>IT</td><td>VIA CENTRO RIABILITATIVO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>31,87</td><td>0</td><td>0</td><td>&nbsp;</td><td>31,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COCCHI ROBERTO</td><td>Busto Arsizio</td><td>IT</td><td>OSP./BRONCOPNEULOGIA<spanstyle='mso-spacerun:yes'> </span></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>263,30</td><td>0</td><td>0</td><td>&nbsp;</td><td>263,30</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COLALUCA PANFILO</td><td>Verona</td><td>IT</td><td>P.LE STEFANI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>351,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>351,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COLI CLAUDIA</td><td>Campi Bisenzio</td><td>IT</td><td>VIA MONTALVO 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>150,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COLMAN ANGELO</td><td>Istrana</td><td>IT</td><td>VIA A.MORO 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>38,63</td><td>0</td><td>0</td><td>&nbsp;</td><td>38,63</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COLOGNI MARCO</td><td>Ponte San Pietro</td><td>IT</td><td>VIA FORLANINI 15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,63</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,63</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COLOMBINI CLAUDIO</td><td>Castelfranco di Sotto</td><td>IT</td><td>C/O AMB.COMUNALE 12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>315,53</td><td>0</td><td>0</td><td>&nbsp;</td><td>315,53</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CONTI LUISA</td><td>Palermo</td><td>IT</td><td>VIA BENEDETTO D'UCRIA 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CORBELLINI ALDO</td><td>Pedrengo</td><td>IT</td><td>C/O AMB.COM.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>298,40</td><td>0</td><td>0</td><td>&nbsp;</td><td>298,40</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CORDANI STEFANO</td><td>La Spezia</td><td>IT</td><td>VIA FIESCHI 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>735,37</td><td>0</td><td>0</td><td>&nbsp;</td><td>735,37</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CORONA NADIA</td><td>Giovo</td><td>IT</td><td>C/O AMB.PED COMUN.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COSMA PAOLO</td><td>Sondalo</td><td>IT</td><td>VIA ZUBIANI 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>333,49</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,49</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COSTANTINI EMANUELA PAOLA</td><td>Atri</td><td>IT</td><td>V.LE RISORGIMENTO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COSTANTINI MARIA GABRIELLA</td><td>Belluno</td><td>IT</td><td>VIA FELTRE 50</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COSTANTINO ELIO</td><td>Matera</td><td>IT</td><td>CONTRADA CATTEDRA AMBULANTE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>119,46</td><td>1.125,00</td><td>0</td><td>&nbsp;</td><td>1.244,46</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COSTIGLIOLA ANTONIO</td><td>Pozzuoli</td><td>IT</td><td>VIA CAMPANA 250</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>COUMINE ZUHEIR</td><td>Firenze</td><td>IT</td><td>V.LE TALENTI 118</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.108,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.668,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CREPALDI MONICA</td><td>Lodi</td><td>IT</td><td>P.ZA OSPEDALE 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>184,08</td><td>0</td><td>0</td><td>&nbsp;</td><td>184,08</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CRESCI CHIARA</td><td>Firenze</td><td>IT</td><td>VIALE GAETANO PIERACCINI 17</td><<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>415,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>415,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CRESCI CHIARA</td><td>Firenze</td><td>IT</td><td>VIALEGAETANO PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>200,00</td><td>826,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.026,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CRIPPA LORENZA</td><td>Gessate</td><td>IT</td><td>PIAZZA DELLA PACE 20</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.340,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.900,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CRISTALLINI STEFANO</td><td>Recanati</td><td>IT</td><td>VIA CAVOUR 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,99</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,99</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>CUCCHIARO EMANUELA</td><td>Povoletto</td><td>IT</td><td>VIA VERDI 28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>D'ADAMO FRANCESCO ANTONIO FORTUNATO</td><td>Roma</td><td>IT</td><td>VIA TIBERIO IMPERATORE 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>201,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>201,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>D'AGOSTINO CATERINA</td><td>Reggio di Calabria</td><td>IT</td><td>VIA GENOVA. 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>D'AGOSTINO SILVANA</td><td>Genova</td><td>IT</td><td>VIA PASTORINO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>197,62</td><td>0</td><td>0</td><td>&nbsp;</td><td>197,62</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DALI DALIA</td><td>Minerbe</td><td>IT</td><td>VIA ROMA 119</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>D'AMICO DOMENICO</td><td>Palermo</td><td>IT</td><td>VIA MARVUGLIA 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DANESI GIANLUCA</td><td>Ravenna</td><td>IT</td><td>VIA MISSIROLI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>18,84</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.018,84</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE BLASIO FRANCESCO</td><td>POZZUOLI</td><td>IT</td><td>VIA TRIPERGOLA, 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>3.000,00</td><td>0</td><td>&nbsp;</td><td>3.000,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE FELICE ALBERTO</td><td>Telese Terme</td><td>IT</td><td>VIA NINO BIXIO 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>345,67</td><td>0</td><td>0</td><td>&nbsp;</td><td>345,67</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE MARCHI ANTONELLA</td><td>Cambiasca</td><td>IT</td><td>VIA PER MIAZZINA 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>273,60</td><td>0</td><td>0</td><td>&nbsp;</td><td>273,60</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE MICHELE FAUSTO</td><td>Napoli</td><td>IT</td><td>VIA CARDARELLI 9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>69,72</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.069,72</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE MICHIEL GIUSEPPE</td><td>Chiavari</td><td>IT</td><td>P.ZA TORRIGLIA 7/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>221,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>221,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE RENZI ELIO</td><td>Avellino</td><td>IT</td><td>VIA SCANDONE 11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE ROSA CONCETTA</td><td>Cava de' Tirreni</td><td>IT</td><td>VIA F. ALFIERI 29</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>290,47</td><td>0</td><td>0</td><td>&nbsp;</td><td>290,47</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE ROSA STEFANIA ROSA CARLA</td><td>Gallarate</td><td>IT</td><td>VIA PASTORI 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>266,18</td><td>0</td><td>0</td><td>&nbsp;</td><td>266,18</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DE STEFANO GIANMARIO</td><td>Negrar</td><td>IT</td><td>VIA OSPEDALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DEL DONNO MARIO</td><td>Benevento</td><td>IT</td><td>VIA DELL'ANGELO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>380,62</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.380,62</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DELLA TORRE FABRIZIO</td><td>Milano</td><td>IT</td><td>VIA STAMPA 15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DENNETTA DONATELLA</td><td>Pesaro</td><td>IT</td><td>VIA CARLO CINELLI 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>457,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>457,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DENTE FEDERICO LORENZO</td><td>Pisa</td><td>IT</td><td>VIA ZAMENHOF 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>321,26</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.321,26</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DI MITA FRANCESCO</td><td>Gaiarine</td><td>IT</td><td>VIA FRACASSI 39</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DI SALVATORE FRANCESCO</td><td>Caserta</td><td>IT</td><td>VIA TESCIONE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>1.042,72</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.042,72</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DI TRANI MARIO</td><td>Altofonte</td><td>IT</td><td>VIA GRAMSCI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DI ZAZZO CESARE</td><td>Caserta</td><td>IT</td><td>VIA ACQUAVIVA 92</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>337,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>337,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DIGENA MARIANNA</td><td>Gravina in Puglia</td><td>IT</td><td>VIA POMPEI 58</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>50,07</td><td>0</td><td>0</td><td>&nbsp;</td><td>50,07</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DIMITRI FEDOR</td><td>Frosinone</td><td>IT</td><td>VIA MARITTIMA 192</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>236,42</td><td>0</td><td>0</td><td>&nbsp;</td><td>236,42</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DOTTORINI MAURIZIO</td><td>Perugia</td><td>IT</td><td>OSP.SILVESTRINI REP.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>18,04</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.018,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>DRAGOTTO GIUSEPPE</td><td>Palermo</td><td>IT</td><td>VIA CARMELO LAZZARO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>EPIFANI VITTORIO</td><td>Fasano</td><td>IT</td><td>VIA NAZIONALE DEI TRULLI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>199,42</td><td>0</td><td>0</td><td>&nbsp;</td><td>199,42</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ERRICO GIOVANNI</td><td>Maglie</td><td>IT</td><td>C/O AMB. PNEUMOLOGIA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>562,81</td><td>0</td><td>0</td><td>&nbsp;</td><td>562,81</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FABBRI ALESSANDRO</td><td>Pistoia</td><td>IT</td><td>VIA OSP. PISTOIA MED. 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>750,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>750,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FACETTI SARA</td><td>Malnate</td><td>IT</td><td>VIA BRENTA 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>531,14</td><td>0</td><td>0</td><td>&nbsp;</td><td>531,14</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FAZIOLI GIULIANO</td><td>Medolla</td><td>IT</td><td>VIA S. MATTEO 9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FERLIGA MAURO</td><td>Chiari</td><td>IT</td><td>VIA BRESCIA 131</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>286,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>286,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FERRARI KATIA</td><td>Firenze</td><td>IT</td><td>V.LE PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>266,59</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.416,59</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FERRITTO LUIGI</td><td>Piedimonte Matese</td><td>IT</td><td>VIA MATESE 12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FERRITTO WALTER MARIA BIAGIO</td><td>Piedimonte Matese</td><td>IT</td><td>VIA MATESE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FERRONI ETTORE</td><td>Comacchio</td><td>IT</td><td>VIA RIVA DI MEZZO 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FICANO ONOFRIO</td><td>Santa Flavia</td><td>IT</td><td>VIA PEZZILLO 42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FIGINI ROSSANA</td><td>Busto Arsizio</td><td>IT</td><td>P.LE PROF.G.SOLARO 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>269,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>269,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FONTANA GIOVANNI</td><td>Firenze</td><td>IT</td><td>V.LE PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>635,00</td><td>2.050,39</td><td>0</td><td>0</td><td>&nbsp;</td><td>2.685,39</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FONTANA GIOVANNI</td><td>VAGLIA</td><td>IT</td><td>VIA DEL CANTO E ROSAIO 10/B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>3.000,00</td><td>0</td><td>&nbsp;</td><td>3.000,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FORMENTIN ELISABETTA</td><td>Padova</td><td>IT</td><td>VIA BELLI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FOSCHINO BARBARO MARIA PIA</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>119,46</td><td>1.125,00</td><td>0</td><td>&nbsp;</td><td>1.244,46</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FRIGERIO CARLO</td><td>Gessate</td><td>IT</td><td>VIA DE GASPERI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.340,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.900,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FRONDA LUCIANA ALDA</td><td>Lamezia Terme</td><td>IT</td><td>VIA OSPEDALE 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>FUMAGALLI RENATO</td><td>Palermo</td><td>IT</td><td>VIA CARMELO LAZZARO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>980,14</td><td>0</td><td>0</td><td>&nbsp;</td><td>980,14</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GACCIONE ANNA TALIA</td><td>Chiaravalle Centrale</td><td>IT</td><td>VIA MARIO CERAVOLO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GAGLIARDI CARLO</td><td>Frosinone</td><td>IT</td><td>VIA MARITTIMA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>230,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>230,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GALLICCHIO ANTONIO</td><td>Sacile</td><td>IT</td><td>VIA REPUBBLICA 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>38,63</td><td>0</td><td>0</td><td>&nbsp;</td><td>38,63</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GAMBA ALBERTO</td><td>Cittadella</td><td>IT</td><td>C/O AMB. GUARDIA MEDICA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GAROFALO MARIA ANTONELLA</td><td>Palermo</td><td>IT</td><td>VIA CATALDO PARISIO 95</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GATTI ALESSANDRO</td><td>Cornuda</td><td>IT</td><td>VIA XXX APRILE 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>37,15</td><td>0</td><td>0</td><td>&nbsp;</td><td>37,15</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GAUDIOSI CARLO</td><td>Telese Terme</td><td>IT</td><td>VIA NINO BIXIO 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GHIO STEFANO</td><td>Pavia</td><td>IT</td><td>V.LE C. GOLGI 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>876,00</td><td>1.527,26</td><td>0</td><td>0</td><td>&nbsp;</td><td>2.403,26</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GINESTRA LUCIANO</td><td>Pescara</td><td>IT</td><td>VIA DEI SABINI 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>402,79</td><td>0</td><td>0</td><td>&nbsp;</td><td>402,79</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GIRGENTI CALOGERO</td><td>Padova</td><td>IT</td><td>VIA POLESINE 40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GIULIANO GIUSEPPE</td><td>Catanzaro</td><td>IT</td><td>VIA INDIPENDENZA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GOLOTTA PIETRO</td><td>Soriano Calabro</td><td>IT</td><td>VIA NAZIONALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GOTI PATRIZIO</td><td>Prato</td><td>IT</td><td>P.ZA OSPEDALE 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>678,97</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.318,31</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GOTTI ENRICO</td><td>Bologna</td><td>IT</td><td>VIA PIETRO ALBERTONI 15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>665,00</td><td>1.164,98</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.829,98</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRANATA MARIA ROSARIA</td><td>Casavatore</td><td>IT</td><td>VIA NICOLA AMORE 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRASSI VERONICA</td><td>Tione di Trento</td><td>IT</td><td>VIA DURONE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRAZIANO MARIO</td><td>Palermo</td><td>IT</td><td>VIA B. GRAVINA 69</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRAZIANO PASQUALE</td><td>La Spezia</td><td>IT</td><td>GALLERIA GOITO 14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>76,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>76,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRAZZINI MICHELA</td><td>Pistoia</td><td>IT</td><td>P.ZA GIOVANNI XXIII</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>600,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>600,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GRECO NATALE</td><td>San Lazzaro di Savena</td><td>IT</td><td>VIA JUSSI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>27,58</td><td>0</td><td>0</td><td>&nbsp;</td><td>27,58</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GROPPA ELISA</td><td>Orgiano</td><td>IT</td><td>PIAZZA DEL FANTE 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GUARINO ANGIOLINA</td><td>Marigliano</td><td>IT</td><td>TRAVERSA TRAV VIA FAIBANO 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>334,41</td><td>0</td><td>0</td><td>&nbsp;</td><td>334,41</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GUCCIARDO DONATELLA</td><td>Palermo</td><td>IT</td><td>VIA GIUSEPPE SCIUTI 79</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GUERRA GIOVANNI</td><td>Lodi</td><td>IT</td><td>VIA ALDO MORO 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>GUGLIOTTA VITO</td><td>Partinico</td><td>IT</td><td>C.SO DEI MILLE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>IANNELLO GIOACCHINO</td><td>Pontedera</td><td>IT</td><td>VIA ROMA 151</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ILIC SNEZANA</td><td>Negrar</td><td>IT</td><td>VIA OSPEDALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>INNOCENTI ILARIA</td><td>Pisa</td><td>IT</td><td>VIA GIUSEPPE MORUZZI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>380,65</td><td>0</td><td>0</td><td>&nbsp;</td><td>380,65</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>IULIANO ANTONIO GABRIELE</td><td>Milano</td><td>IT</td><td>VIA PIO II 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LACEDONIA DONATO</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1.115,00</td><td>3.570,24</td><td>0</td><td>0</td><td>&nbsp;</td><td>4.685,24</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LANZILOTTA PASQUALE</td><td>Pescara</td><td>IT</td><td>VIA R.PAOLINI 45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>366,86</td><td>0</td><td>0</td><td>&nbsp;</td><td>366,86</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LEONE GENNARO</td><td>Caserta</td><td>IT</td><td>VIA TESCIONE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LEVI MINZI SUSANNA</td><td>Due Carrare</td><td>IT</td><td>VIA ROMA 93</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LI CALZI FULVIO</td><td>Palermo</td><td>IT</td><td>P.ZA SALERNO 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>923,12</td><td>0</td><td>0</td><td>&nbsp;</td><td>923,12</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LO CICERO SALVATORE CESARE</td><td>Milano</td><td>IT</td><td>P.ZA OSPEDALE MAGGIORE 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>47,92</td><td>0</td><td>0</td><td>&nbsp;</td><td>47,92</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LO GIUDICE FRANCESCO</td><td>Trieste</td><td>IT</td><td>ST.A DI FIUME 447</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>200,00</td><td>840,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.040,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LO RE ONOFRIA</td><td>Partinico</td><td>IT</td><td>VIA AMODEI 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,33</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LOMBARDO VINCENZO</td><td>Reggio di Calabria</td><td>IT</td><td>VIA ENOTRIA 95</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LORENZATO CAMILLA LAURA</td><td>Ponte nelle Alpi</td><td>IT</td><td>VIA COL DI CUGNAN 5/A</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>LOVISON LOREDANA</td><td>Padova</td><td>IT</td><td>VIA VECELLIO 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MANCA GIULIETTA</td><td>Savona</td><td>IT</td><td>VIA MONTENOTTE 27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.214,93</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.774,93</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MANNINO ANTONINO</td><td>Capaci</td><td>IT</td><td>VIA KENNEDY 85</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARCHESE MARIA CRISTINA</td><td>Treviso</td><td>IT</td><td>VIA ELLERO 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>687,21</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.326,55</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARCHI MARIA RITA</td><td>Padova</td><td>IT</td><td>VIA GIUSTINIANI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>658,37</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.297,71</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MAROCCO ANTONELLA</td><td>Cagliari</td><td>IT</td><td>VIA GIUSEPPE PERETTI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>125,00</td><td>450,71</td><td>0</td><td>0</td><td>&nbsp;</td><td>575,71</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARONI PAOLA</td><td>Bellinzago Novarese</td><td>IT</td><td>VIA RIMEMBRANZA 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>97,39</td><td>0</td><td>0</td><td>&nbsp;</td><td>97,39</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARRAS MARCO</td><td>Civitavecchia</td><td>IT</td><td>C.SO CENTOCELLE 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>92,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>92,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARTELLA FRANCO</td><td>Tiggiano</td><td>IT</td><td>P.ZA ROMA 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>238,35</td><td>0</td><td>0</td><td>&nbsp;</td><td>238,35</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MARTINO INCORONATA</td><td>Casalnuovo di Napoli</td><td>IT</td><td>C.SO UMBERTO 497</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>163,93</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>163,93</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MASTROBERARDINO MICHELE</td><td>Avellino</td><td>IT</td><td>VIA ITALIA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>901,85</td><td>0</td><td>0</td><td>&nbsp;</td><td>901,85</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MATTEOLI ILENIA</td><td>Pisa</td><td>IT</td><td>VIA GIUSEPPE MORUZZI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>380,65</td><td>0</td><td>0</td><td>&nbsp;</td><td>380,65</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MAZID MAHMOUD</td><td>Albano Sant'Alessandro</td><td>IT</td><td>VIA PAPA GIOVANNI XXIII 27/9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MELARA ROSITA</td><td>Mirandola</td><td>IT</td><td>VIA FOGAZZARO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>620,00</td><td>555,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.175,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MELI ELISA PATRIZIA LOREDANA</td><td>Partinico</td><td>IT</td><td>VIA PRINCIPE UMBERTO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MENEGUS ELENA</td><td>Padova</td><td>IT</td><td>VIA MANTRONI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MENOTTI ALBERTO</td><td>Trento</td><td>IT</td><td>VIA RICCAMBONI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>506,60</td><td>0</td><td>0</td><td>&nbsp;</td><td>506,60</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MERELLA PIERLUIGI</td><td>Sassari</td><td>IT</td><td>VIA VERDI 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>635,00</td><td>1.947,14</td><td>0</td><td>0</td><td>&nbsp;</td><td>2.582,14</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MICELI NINA MARIA</td><td>Reggio di Calabria</td><td>IT</td><td>VIA TORRIONE 26 B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MICHELETTO CLAUDIO</td><td>Bussolengo</td><td>IT</td><td>VIA OSPEDALE 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>96,14</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.096,14</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MILANO ANNAMARIA</td><td>Putignano</td><td>IT</td><td>VIA CAPPUCCINI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>31,87</td><td>0</td><td>0</td><td>&nbsp;</td><td>31,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MINELLI FERDINANDO</td><td>Sarezzo</td><td>IT</td><td>VIA IV NOVEMBRE 38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>265,93</td><td>0</td><td>0</td><td>&nbsp;</td><td>265,93</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MINORE GIANCARLO</td><td>Bologna</td><td>IT</td><td>VIA EMILIA LEVANTE 81</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>214,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>214,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MISCI MARIO</td><td>Magnago</td><td>IT</td><td>VIA PERUGINO 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MOLLICA PAOLA</td><td>Reggio di Calabria</td><td>IT</td><td>VAI RIONE 30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MONTAGNA FABIO</td><td>Spinea</td><td>IT</td><td>V.LE VIAREGGIO 30/A</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MONTI PAOLO</td><td>Larciano</td><td>IT</td><td>VIA GRAMSCI 901</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>76,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>76,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MORANDI FABRIZIO</td><td>Varese</td><td>IT</td><td>VIA GUICCIARDINI 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>200,00</td><td>804,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.004,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MORELLI ANDREA</td><td>Ravenna</td><td>IT</td><td>VIA MISSIROLI 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>35,10</td><td>0</td><td>0</td><td>&nbsp;</td><td>35,10</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MOTTA MARIA CARMELA</td><td>Genova</td><td>IT</td><td>VIA CAMOZZINI 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MURER ANTONELLA</td><td>Padova</td><td>IT</td><td>VIA CATULLO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>133,19</td><td>0</td><td>0</td><td>&nbsp;</td><td>133,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>MURRONI SERGIO</td><td>Chiavari</td><td>IT</td><td>P.ZA TORRIGLIA 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>NADDEO ANGELO</td><td>Nova Milanese</td><td>IT</td><td>VIA BRODOLINI 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>55,91</td><td>0</td><td>0</td><td>&nbsp;</td><td>695,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>NAPOLITANO ENRICO LUCIO CLAUDIO</td><td>Liveri</td><td>IT</td><td>VIA SALITA BOCCIANO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>NARDI MADDALENA</td><td>Verona</td><td>IT</td><td>VIA SOGARI 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>NATALE MICHELE ROSARIO</td><td>Napoli</td><td>IT</td><td>VIA ORSI 50</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>NESI DANTE</td><td>San Gimignano</td><td>IT</td><td>P.ZZA BACCANELLA 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>335,15</td><td>0</td><td>0</td><td>&nbsp;</td><td>335,15</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>OLIVERI MARIA</td><td>Alcamo</td><td>IT</td><td>VIA SICILIA 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ORECCHIO GIANFRANCO</td><td>Avellino</td><td>IT</td><td>VIA CANNAVIELLO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>349,12</td><td>0</td><td>0</td><td>&nbsp;</td><td>349,12</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ORSO MIRIAM</td><td>Trento</td><td>IT</td><td>VIA BRONZETTI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PACE ELISABETTA</td><td>Palermo</td><td>IT</td><td>VIA UGO LA MALFA 122</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>490,00</td><td>1.895,86</td><td>0</td><td>0</td><td>&nbsp;</td><td>2.385,86</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PAGANI MARIA BEATRICE</td><td>Milano</td><td>IT</td><td>P.ZZA PIEMONTE 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>165,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>165,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PAGANI MATTEO</td><td>Lodi</td><td>IT</td><td>P.ZA OSPEDALE 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>165,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>165,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PALAZZI TRIVELLI ALBERTO</td><td>Bologna</td><td>IT</td><td>VIA MASSARENTI 254</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>27,58</td><td>0</td><td>0</td><td>&nbsp;</td><td>27,58</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PALAZZINI MASSIMILIANO</td><td>Bologna</td><td>IT</td><td>VIA MASSARENTI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>665,00</td><td>1.334,50</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.999,50</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PALLONE FABIO</td><td>Parma</td><td>IT</td><td>VIA ORAZIO 7/A</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PALMERI LEONARDA</td><td>Borgetto</td><td>IT</td><td>VIA ROMA 128</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PALMIERI ANGELO</td><td>Foggia</td><td>IT</td><td>VIA LUCERA 110</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>221,05</td><td>0</td><td>0</td><td>&nbsp;</td><td>221,05</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PAPALE MARIA</td><td>Roma</td><td>IT</td><td>VIA ELIO CHIANESI 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>543,50</td><td>0</td><td>0</td><td>&nbsp;</td><td>543,50</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PAPERINI FRANCESCO</td><td>EMPOLI</td><td>IT</td><td>Via delle Ville di Cerbaiola 34</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.000,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PARIS CINZIA</td><td>Rieti</td><td>IT</td><td>VIA DEI FLAVI 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.028,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.588,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PAUSINI SILVIA</td><td>Faenza</td><td>IT</td><td>VIA F.LLI ROSSELLI 75</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PECORARO GAETANO</td><td>Angri</td><td>IT</td><td>VIA KENNDY 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PELOSI PIETRO</td><td>San Lorenzello</td><td>IT</td><td>VIA P. SASSO 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>269,35</td><td>0</td><td>0</td><td>&nbsp;</td><td>269,35</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PENTASSUGLIA ANTONELLA</td><td>Roma</td><td>IT</td><td>VIA CRISAFULLI 12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>225,16</td><td>0</td><td>0</td><td>&nbsp;</td><td>225,16</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PERRI FRANCESCO</td><td>Firenze</td><td>IT</td><td>V.LE PIERACCINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>600,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>600,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PESSOTTO FRANCO</td><td>Fontanafredda</td><td>IT</td><td>VIA CAVOUR 5/A</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>37,15</td><td>0</td><td>0</td><td>&nbsp;</td><td>37,15</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PETRINI MADDALENA</td><td>Terni</td><td>IT</td><td>VIA BRAMANTE C/O ASL 4 47</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>333,96</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,96</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PETRONGARI MASSIMO</td><td>Rieti</td><td>IT</td><td>VIA DEL TERMINILLO 54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PEZZELLA VINCENZO</td><td>Caivano</td><td>IT</td><td>VIA BARILE 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PICCOLO ENNIO</td><td>Alcamo</td><td>IT</td><td>VIA MANGIAROTTI 18</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PIERALLINI FEDERICO</td><td>Ponte Buggianese</td><td>IT</td><td>VIA MATTEOTTI 36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>334,40</td><td>0</td><td>0</td><td>&nbsp;</td><td>334,40</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PILI MARCELLO</td><td>Roma</td><td>IT</td><td>VIA CAPO SPARTIVENTO 73</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PIROVANO CLAUDIO ANDREA PAOLO</td><td>Milano</td><td>IT</td><td>PIAZZA PIEMONTE 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,40</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,40</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PISANELLO LORENA</td><td>Battaglia Terme</td><td>IT</td><td>P.ZA DELLA LIBERTA 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PISANTI CHIARA</td><td>Napoli</td><td>IT</td><td>VIA SAN GENNARO AD ANTIGNANO 42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>POLLASTRI GIOVANNI</td><td>Bologna</td><td>IT</td><td>VIA CARAVAGGIO 8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,52</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,52</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>POMERRI ANTONELLA</td><td>Padova</td><td>IT</td><td>VIA DEI COLLI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>240,44</td><td>0</td><td>0</td><td>&nbsp;</td><td>240,44</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PONTILLO ANTONIO</td><td>Matera</td><td>IT</td><td>VIA MONTESCAGLIOSO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>35,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>35,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>POZZOLI GIUSEPPINA ERCOLINA</td><td>Cernusco sul Naviglio</td><td>IT</td><td>P.ZA MARTIRI LIBERTA'</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,03</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,03</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PRADERIO ROBERTO</td><td>San Bonifacio</td><td>IT</td><td>VIA FOGAZZARO 9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PRETE LEONARDO</td><td>Ostuni</td><td>IT</td><td>PRONTO<span style='mso-spacerun:yes'>  </span>SOCCORSO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>166,97</td><td>0</td><td>0</td><td>&nbsp;</td><td>166,97</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PREVIDI MAURO</td><td>Verona</td><td>IT</td><td>VIA G.DEL CARRETTO 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>PROVERA SILVIA</td><td>Negrar</td><td>IT</td><td>VIA OSPEDALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RACCANELLI RITA</td><td>Seregno</td><td>IT</td><td>C/O OSPEDALE TRABATTONI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RADICELLA DIANA</td><td>Napoli</td><td>IT</td><td>VIA LEONARDO BIANCHI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>292,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>292,27</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RAINERI CLAUDIA</td><td>Pavia</td><td>IT</td><td>V.LE C. GOLGI 19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1.541,00</td><td>2.723,44</td><td>0</td><td>0</td><td>&nbsp;</td><td>4.264,44</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RAMPONI SARA</td><td>Cremona</td><td>IT</td><td>VIA FABIO FILZI 56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>202,68</td><td>0</td><td>0</td><td>&nbsp;</td><td>202,68</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RANDAZZO ALDO</td><td>Palmi</td><td>IT</td><td>VIA C.BATTISTI 3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>REDA MARIA</td><td>Casciana Terme</td><td>IT</td><td>C/O CROCE ROSSA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>258,19</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>258,19</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RESTA ONOFRIO</td><td>Bari</td><td>IT</td><td>P.ZA GIULIO CESARE 11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>16,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>16,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RICCARDI GIOVANNI CARLO</td><td>Alessandria</td><td>IT</td><td>P.ZA GARIBALDI 48</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RINALDI ROBERTA</td><td>Palermo</td><td>IT</td><td>VIA CRUILLAS 310</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROCCHI ALBERTO DOMENICO</td><td>Fivizzano</td><td>IT</td><td>SALITA S.FRANCESCO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>293,41</td><td>0</td><td>0</td><td>&nbsp;</td><td>293,41</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROCCHI PATRIZIA</td><td>Tolfa</td><td>IT</td><td>VIA ROMA 125</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROCCO PAOLA ROSARIA ANTONIA</td><td>Caserta</td><td>IT</td><td>VIA DEI VECCHI PINI 17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RONC IAIA</td><td>Trento</td><td>IT</td><td>LARGO MEDAGLIE D'ORO 9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>472,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>472,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RONDININI ROBERTO</td><td>Faenza</td><td>IT</td><td>VIA C/O SAN PIER DAMIANO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>140,38</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.140,38</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROSATI YURI</td><td>Macerata</td><td>IT</td><td>VIA S.LUCIA 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>140,38</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.140,38</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROSI ELISABETTA</td><td>Firenze</td><td>IT</td><td>VIALE GAETANO PIERACCINI 24</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>150,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROSSINI DOMENICO</td><td>Lodi</td><td>IT</td><td>C/O AMB. COM.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>97,39</td><td>0</td><td>0</td><td>&nbsp;</td><td>97,39</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROTUNDO FRANCESCO LUCIANO PIO</td><td>Pordenone</td><td>IT</td><td>VIA N SAURO 1/B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>180,15</td><td>0</td><td>0</td><td>&nbsp;</td><td>180,15</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ROVEDA PAOLO</td><td>Melegnano</td><td>IT</td><td>VIA PANDINA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>280,60</td><td>0</td><td>0</td><td>&nbsp;</td><td>280,60</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RUSSO ANDREA RODOLFO</td><td>Roma</td><td>IT</td><td>VIA FULDA 14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>649,52</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.288,86</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RUSSO MAURIZIO</td><td>Crotone</td><td>IT</td><td>C.SO MESSINA 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>RUSSO VINCENZO</td><td>Catanzaro</td><td>IT</td><td>VIA SINOPOLI 55</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SABATO ROBERTO</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>119,46</td><td>1.125,00</td><td>0</td><td>&nbsp;</td><td>1.244,46</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SABBATINI LUIGI</td><td>Roma</td><td>IT</td><td>VIA C.D.ROCCA 40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>226,45</td><td>0</td><td>0</td><td>&nbsp;</td><td>226,45</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SALIBA JOSEPH</td><td>Castelraimondo</td><td>IT</td><td>VIA DAMIANO CHIESA 14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>254,56</td><td>0</td><td>0</td><td>&nbsp;</td><td>254,56</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SANDUZZI ZAMPARELLI ALESSANDRO</td><td>Napoli</td><td>IT</td><td>VIA LEONARDO BIANCHI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>315,11</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.315,11</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SANGUIGNI LUCIANO</td><td>Sabaudia</td><td>IT</td><td>B.GO VODICE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SAPORITI MATTEO</td><td>Milano</td><td>IT</td><td>VIA F.SFORZA 28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>266,54</td><td>0</td><td>0</td><td>&nbsp;</td><td>266,54</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SASSOLINO STEFANIA</td><td>Trissino</td><td>IT</td><td>P.ZA MAZZINI 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCACCIANOCE SALVATORE</td><td>Viterbo</td><td>IT</td><td>VIA S.LORENZO 101</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>299,40</td><td>0</td><td>0</td><td>&nbsp;</td><td>299,40</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCADUTO VINCENZO</td><td>Bagheria</td><td>IT</td><td>VIA FUMAGALLI 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCAFIDI VINCENZA</td><td>Palermo</td><td>IT</td><td>VIA CRUILLAS 2 B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCARPELLI ENRICO</td><td>Cosenza</td><td>IT</td><td>C.SO DA MUOIO PICCOLO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCARTABELLATI ALESSANDRO</td><td>Crema</td><td>IT</td><td>VIA MACOLLE'14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>367,81</td><td>0</td><td>0</td><td>&nbsp;</td><td>367,81</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCARZO EMANUELE</td><td>Alessandria</td><td>IT</td><td>P.ZZA GARIBALDI 48</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCAVALLI PATRIZIA</td><td>Civita Castellana</td><td>IT</td><td>VIA FERRETTI 169</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>319,70</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.319,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCHENARDI FEDERICO GIUSEPPE MARIO</td><td>Milano</td><td>IT</td><td>V.LE RANZONI 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCICHILONE NICOLA</td><td>Palermo</td><td>IT</td><td>VIA UGO LA MALFA 122</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2.319,97</td><td>2.125,00</td><td>0</td><td>&nbsp;</td><td>4.444,97</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCIMECA SALVATORE</td><td>Palermo</td><td>IT</td><td>VIA GIUSEPPE CRISPI 114</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,33</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCIRE' GIUSEPPE MASSIMO</td><td>Catania</td><td>IT</td><td>VIA MESSINA 829</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>879,02</td><td>0</td><td>0</td><td>&nbsp;</td><td>879,02</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCLIFO' FRANCESCA</td><td>Genova</td><td>IT</td><td>LARGO ROSANNA BENZI 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>131,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>131,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SCURATI GIAN LUCA</td><td>Bussero</td><td>IT</td><td>VIA VERDI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>261,04</td><td>0</td><td>0</td><td>&nbsp;</td><td>261,04</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SELMI MASSIMO</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>230,53</td><td>0</td><td>0</td><td>&nbsp;</td><td>230,53</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SEMENZIN STEFANO</td><td>Montebelluna</td><td>IT</td><td>V.LE DELLA VITTORIA 25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>105,29</td><td>0</td><td>0</td><td>&nbsp;</td><td>105,29</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SEMINO LUIGI AMEDEO</td><td>Genova</td><td>IT</td><td>VIA CORONATA 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SERGIO MARCELLO</td><td>Roma</td><td>IT</td><td>VIA ROBERTO MALATESTA 124</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>238,57</td><td>0</td><td>0</td><td>&nbsp;</td><td>238,57</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SFERLAZZO PAOLO</td><td>Isola delle Femmine</td><td>IT</td><td>VIA STAZIONE 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,33</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SILVESTRE GIUSEPPE</td><td>Verona</td><td>IT</td><td>P.LE STEFANI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>411,20</td><td>0</td><td>0</td><td>&nbsp;</td><td>411,20</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SINICATO MASSIMO</td><td>Senigallia</td><td>IT</td><td>VIA FAGNANI 33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>156,64</td><td>0</td><td>0</td><td>&nbsp;</td><td>156,64</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SOAVE MARIA GRAZIA</td><td>Bovolone</td><td>IT</td><td>VIA M.GORETTI 6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SOLIDORO PAOLO</td><td>Torino</td><td>IT</td><td>C.SO BRAMANTE 8890</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>122,71</td><td>2.000,00</td><td>0</td><td>&nbsp;</td><td>2.122,71</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SOVERA EZIO</td><td>Rozzano</td><td>IT</td><td>VIA UGO FOSCOLO 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>260,62</td><td>0</td><td>0</td><td>&nbsp;</td><td>260,62</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SPARACIO IGNAZIO</td><td>Palermo</td><td>IT</td><td>VIA V.DI MARCO 41</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SPECIALE LORENZO</td><td>Partinico</td><td>IT</td><td>VIA PRINCIPE UMBERTO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>300,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>300,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>SQUARCINA LUCIANO</td><td>Abano Terme</td><td>IT</td><td>VIA CONFIGLIACHI 5\B</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>194,76</td><td>0</td><td>0</td><td>&nbsp;</td><td>194,76</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>STAINER ANNA</td><td>Monza</td><td>IT</td><td>VIA GIOVAN BATTISTA PERGOLESI 31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>125,00</td><td>341,93</td><td>0</td><td>0</td><td>&nbsp;</td><td>466,93</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>STANGHELLINI EVO</td><td>San Pancrazio</td><td>IT</td><td>VIA RANDI 11/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>69,71</td><td>0</td><td>0</td><td>&nbsp;</td><td>69,71</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>STIRPE EMANUELE</td><td>Roma</td><td>IT</td><td>V.le OXFORD 81</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>327,87</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>327,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>STRANO VINCENZO</td><td>Delianuova</td><td>IT</td><td>VIA UMBERTO<span style='mso-spacerun:yes'>  </span>I<spanstyle='mso-spacerun:yes'>  </span>NØ 178</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TACCALITI DANILO</td><td>Fabriano</td><td>IT</td><td>POLIAMBULATORI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>207,87</td><td>0</td><td>0</td><td>&nbsp;</td><td>207,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TADDIO ANDREA</td><td>Trieste</td><td>IT</td><td>VIA DELL'ISTRIA 65/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>275,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>275,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TALIA PATRIZIA MARIA ANNUNZIATA</td><td>Foggia</td><td>IT</td><td>VIA ASCOLI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,53</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,53</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TANSELLA ALESSANDRO</td><td>Porto San Giorgio</td><td>IT</td><td>VIA DELLA MISERICORDIA 7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>134,33</td><td>0</td><td>0</td><td>&nbsp;</td><td>134,33</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TARANTINI GIUSEPPE</td><td>Riva Ligure</td><td>IT</td><td>VIA MARTIRI 107</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>247,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>247,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TARANTINO NICOLO'</td><td>Palermo</td><td>IT</td><td>VIA BRIUCCIA 84</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>278,69</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>278,69</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TASSINARI GIUSTO</td><td>Cento</td><td>IT</td><td>VIA GUERGINO 74</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>212,88</td><td>0</td><td>0</td><td>&nbsp;</td><td>212,88</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TAVANO NICLA</td><td>Melegnano</td><td>IT</td><td>VIA PANDINA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TAZZA ROBERTO</td><td>Terni</td><td>IT</td><td>VIA F. CESI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>265,82</td><td>0</td><td>0</td><td>&nbsp;</td><td>265,82</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TITONE NICOLO'</td><td>Alcamo</td><td>IT</td><td>PIANO S. MARIA 27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>333,34</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>333,34</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TODESCHINI MARIA</td><td>Verona</td><td>IT</td><td>VIA G. DEL CARRETTO 5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TOFFOLETTI RENATA</td><td>Tarcento</td><td>IT</td><td>VIA DANTE 63</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>131,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>131,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TORNESE GIANLUCA</td><td>Trieste</td><td>IT</td><td>VIA DELL'ISTRIA 65/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>291,32</td><td>0</td><td>0</td><td>&nbsp;</td><td>291,32</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TORRICELLI ELENA</td><td>Arezzo</td><td>IT</td><td>VIA BERLINGUER 21</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>422,80</td><td>0</td><td>0</td><td>&nbsp;</td><td>422,80</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TREVISAN FRANCESCO</td><td>Rossiglione</td><td>IT</td><td>VIA ROMA<span style='mso-spacerun:yes'>  </span>59 4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>177,43</td><td>0</td><td>0</td><td>&nbsp;</td><td>177,43</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>TUCI STEFANO</td><td>Monsummano Terme</td><td>IT</td><td>VIA MISERICORDIA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>UBALDI ENZO</td><td>Viterbo</td><td>IT</td><td>C/O ASL</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>560,72</td><td>0</td><td>0</td><td>&nbsp;</td><td>560,72</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>URSO MICHELE</td><td>Bassano del Grappa</td><td>IT</td><td>VIA DEI LOTTI SNC</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>86,59</td><td>0</td><td>0</td><td>&nbsp;</td><td>86,59</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VALLE FRANCESCA</td><td>Zevio</td><td>IT</td><td>VIA OSPEDALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VANNUCCHI VIERI</td><td>Firenze</td><td>IT</td><td>BORGO PINTI 70</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VANNUCCI FRANCO</td><td>Pistoia</td><td>IT</td><td>P.ZA GIOVANNI XXIII</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>639,34</td><td>689,27</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.328,61</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VARRASSI GIUSTINO</td><td>L'Aquila</td><td>IT</td><td>FRAZ. S. VITTORINO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>7.000,00</td><td>0</td><td>&nbsp;</td><td>7.000,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VECCHIO CINZIA</td><td>Veruno</td><td>IT</td><td>VIA PER REVISIATE 13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VENTURA ALESSANDRO</td><td>Trieste</td><td>IT</td><td>VIA DELL'ISTRIA 65/1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>275,00</td><td>0</td><td>0</td><td>&nbsp;</td><td>275,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VENTURINI MARIO RICCARDO FRANCESCO</td><td>Lucca</td><td>IT</td><td>VIA DI TIGLIO 783</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>112,75</td><td>0</td><td>0</td><td>&nbsp;</td><td>112,75</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VERGA ROBERTA</td><td>Fino Mornasco</td><td>IT</td><td>VIA STATALE DEI GIOVI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>250,74</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,74</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VIGNALE ALBERTO GIACOMO</td><td>Alessandria</td><td>IT</td><td>P.ZA GARIBALDI 48</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>157,26</td><td>0</td><td>0</td><td>&nbsp;</td><td>157,26</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VILLA GIUSEPPE PAOLO</td><td>Gioiosa Ionica</td><td>IT</td><td>V.LE RIMEMBRANZE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>110,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>110,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VINCENTI RIGOLETTA</td><td>Livorno</td><td>IT</td><td>V.LE ALFIERI 36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>26,25</td><td>0</td><td>0</td><td>&nbsp;</td><td>26,25</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VINCENZI ANTONELLA</td><td>Vedano al Lambro</td><td>IT</td><td>LARGO REPUBBLICA 10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>150,00</td><td>248,98</td><td>0</td><td>0</td><td>&nbsp;</td><td>398,98</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VITA PIERPAOLO</td><td>Verona</td><td>IT</td><td>VIA VILLAFRANCA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>209,57</td><td>0</td><td>0</td><td>&nbsp;</td><td>209,57</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VITACCA MICHELE</td><td>Lumezzane</td><td>IT</td><td>VIA MAZZINI 122</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>347,82</td><td>0</td><td>0</td><td>&nbsp;</td><td>347,82</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VITALE ANTONIO</td><td>Mercogliano</td><td>IT</td><td>VIA NAZIONALE I</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>650,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>650,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VIVIANO VITTORIO</td><td>Palermo</td><td>IT</td><td>VIA PAPA SERGIO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>500,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>500,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>VOLPE FRANCESCO PAOLO</td><td>Palermo</td><td>IT</td><td>VIA F.M. MAGGIO 16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>560,00</td><td>1.151,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>1.711,70</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZAMBOTTO FRANCO MARIA</td><td>Feltre</td><td>IT</td><td>VIA BAGNOLS SUR CEZE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>147,82</td><td>1.000,00</td><td>0</td><td>&nbsp;</td><td>1.147,82</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZAMPOGNA GIUSEPPE</td><td>Melicucco</td><td>IT</td><td>VIA L. STURZO</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>400,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>400,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZANASI ALESSANDRO</td><td>S. LAZZARO</td><td>IT</td><td>VIA GROTTA, 20</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>3.000,00</td><td>0</td><td>&nbsp;</td><td>3.000,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZANE VANNI</td><td>Nogara</td><td>IT</td><td>VIA ROMA 14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>211,26</td><td>0</td><td>0</td><td>&nbsp;</td><td>211,26</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZANGRILLI PROFETA</td><td>Frosinone</td><td>IT</td><td>VIA FABI A.S.L.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>755,21</td><td>0</td><td>0</td><td>&nbsp;</td><td>755,21</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZAVARISE GIORGIO</td><td>Negrar</td><td>IT</td><td>VIA OSPEDALE</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZEDDE GIULIA</td><td>Cagliari</td><td>IT</td><td>VIA GIUSEPPE PERETTI 1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>125,00</td><td>277,16</td><td>0</td><td>0</td><td>&nbsp;</td><td>402,16</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZERILLO BARTOLOMEO</td><td>Roma</td><td>IT</td><td>VIA MONTI DI CRETA 104</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>327,87</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>327,87</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZILLI CATERINA</td><td>Albignasego</td><td>IT</td><td>VIA VITTORIO VENETO 2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>250,00</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>250,00</td></tr>
<tr><td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td><td>ZUNINO CATERINA</td><td>Genova</td><td>IT</td><td>VIA NINO BIXIO 6/17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>160,70</td><td>0</td><td>0</td><td>&nbsp;</td><td>160,70</td></tr>		




				
                </tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter12">
                <thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche)
    ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
    			
    			
                <tbody>
    	
    <tr>
 <td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
	<td>ACADEMY SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA AOSTA 3/A</td>
	<td>03575410968</td>
	<td>0</td>
	<td>10.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>AIM EDUCATION SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA G. RIPAMONTI, 129</td>
	<td>10553070151</td>
	<td>0</td>
	<td>14.621,55</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>14.621,55</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>AIM ITALY SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA RIPAMONTI, 129</td>
	<td>00943621003</td>
	<td>0</td>
	<td>4.240,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>4.240,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>AIPO RICERCHE</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA FRUA, 15</td>
	<td>04024680961</td>
	<td>0</td>
	<td>12.100,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>12.100,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>AITEF STUDIO SRL</td>
	<td>Roma</td>
	<td>IT</td>
	<td>VIA THAILANDIA 27</td>
	<td>07876431003</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ARISTEA EDUCATION SRL</td>
	<td>GENOVA</td>
	<td>IT</td>
	<td>VIA ROMA 10</td>
	<td>01152790992</td>
	<td>0</td>
	<td>1.800,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.800,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ASS AMICI DEL CENTRO DINO FERRARI</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA FRANCESCO SFORZA 35</td>
	<td>07276710154</td>
	<td>10.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ASS. CULTURALE G. DOSSETTI</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>GIULIO SALVADORI 14-16</td>
	<td>97192920581</td>
	<td>1.500,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
  
	<td>ASSOCIAZIONE ITALIANA PER LA LOTTA AL NEUROBLASTOMA ONLUS</td>
	<td>GENOVA</td>
	<td>IT</td>
	<td>VIA DOMENICO FIASELLA 16</td>
	<td>095032940108</td>
	<td>2.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>2.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>AZIENDA OSPEDALIERA MONALDI COTUGNO</td>
	<td>NAPOLI</td>
	<td>IT</td>
	<td>VIA L. BIANCHI</td>
	<td>06798201213</td>
	<td>10.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>BIOMEDIA SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA TREMOLO 4</td>
	<td>10691860158</td>
	<td>0</td>
	<td>1.500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>C.S.C. CENTRO SERVIZI CONGRESSUALI S.R.L.</td>
	<td>Perugia</td>
	<td>IT</td>
	<td>Via Spirito Gualtieri Lorenzo 11</td>
	<td>075573 0617</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>CAPRIMED SRL</td>
	<td>Napoli</td>
	<td>IT</td>
	<td>Via Sella Orta 3</td>
	<td>0818375841</td>
	<td>0</td>
	<td>5.219,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.219,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>CENTER COMUNICAZIONE E CONGRESSI</td>
	<td>NAPOLI</td>
	<td>IT</td>
	<td>VIA G. QUAGLIARELLO, 27</td>
	<td>05705411212</td>
	<td>0</td>
	<td>16.762,00</td>
  <td >0</td>
	<td>0</td>
	<td>43.000,00</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>59.762,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
  
	<td>Centro UniStem – Sede Dip. Di Biosc Università degli Studi di
  Milano</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>Via F. Sforza 35</td>
	<td>03064870151</td>
	<td>7.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>7.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>CHRYSALIS SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA SERIO 6</td>
	<td>09122140156</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
	<td>35.253,69</td>
	<td>&nbsp;</td>
	<td>35.253,69</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>COGEST M. &amp; C</td>
	<td>VERONA</td>
	<td>IT</td>
	<td>VICOLO SAN SILVESTRO 6</td>
	<td>01773850233</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>COLLAGE SRL</td>
	<td>PALERMO</td>
	<td>IT</td>
	<td>VIA UMBERTO GIORDANO 37/A</td>
	<td>04135950824</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>COMMUNICATION LABORATORY SRL</td>
	<td>BARI</td>
	<td>IT</td>
	<td>STRADA BARI MODUGNO TORITTO, 65</td>
	<td>07021570721</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>CONGRESS LAB</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA BASTIONI DI PORTA VOLTA 10</td>
	<td>04928790965</td>
	<td>0</td>
	<td>52.059,78</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>52.059,78</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>CONSORZIO FUTURO IN RICERCA</td>
	<td>FERRARA</td>
	<td>IT</td>
	<td>VIA SARAGAT 1</td>
	<td>01268750385</td>
	<td>0</td>
	<td>21.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>21.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>DEFLA DI CLAUDIA FORTUNATO</td>
	<td>NAPOLI</td>
	<td>IT</td>
	<td>VIA DEL PARCO MARGHERITA, 49/3</td>
	<td>05239521213</td>
	<td>0</td>
	<td>5.730,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.730,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>DEVITAL SERVICE SPA</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>PIAZZA WAGNER 5</td>
	<td>11978060157</td>
	<td>0</td>
	<td>4.383,61</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>4.383,61</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>E20ECONVEGNI SRL</td>
	<td>TRANI</td>
	<td>IT</td>
	<td>VIA TASSELGRADO, 68</td>
	<td>07010960727</td>
	<td>0</td>
	<td>3.500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ELODIA EVENTI SRL</td>
	<td>NAPOLI</td>
	<td>IT</td>
	<td>TRAV. ANTONINOPIO 42</td>
	<td>07734701217</td>
	<td>0</td>
	<td>1.500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ETAGAMMA SRL</td>
	<td>GENOVA</td>
	<td>IT</td>
	<td>XX SETTEMBRE 20/94</td>
	<td>02170250993</td>
	<td>0</td>
	<td>500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ETRUSCA CONVENTIONS S.N.C.</td>
	<td>PERUGIA</td>
	<td>IT</td>
	<td>VIA BONCIARIO 6D</td>
	<td>02221430545</td>
	<td>0</td>
	<td>3.780,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.780,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>Fondazione Angelo De Gasperis Ospedale Niguarda</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>PIAZZA OSPEDALE MAGGIORE 3</td>
	<td>09536810154</td>
	<td>5.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>FONDAZIONE ANT</td>
	<td>Bologna</td>
	<td>IT</td>
	<td>Via Jacopo di Paolo 36</td>
	<td>0517190111</td>
	<td>3.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>FONDAZIONE GIANNI BENZI ONLUS</td>
	<td>VALENZANO</td>
	<td>IT</td>
	<td>VIA ABATE EUSTASIO, 30</td>
	<td>06780820723</td>
	<td>5.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>FONDAZIONE IRCCS CA' GRANDA</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA FRANCESCO SFORZA 28</td>
	<td>04724150968</td>
	<td>5.600,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.600,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>FONDAZIONE RITA LEVI MONTALCINI</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>VIA CATANZARO 9</td>
	<td>096200110581</td>
	<td>20.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>20.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>FONDAZIONE UMBERTO VERONESI</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>PIAZZA VELASCA 5</td>
	<td>97298700150</td>
	<td>5.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>HEVENTIME</td>
	<td>PESCARA</td>
	<td>IT</td>
	<td>VIA RIGOPIANO 20/2</td>
	<td>01738170685</td>
	<td>0</td>
	<td>1.500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>I&amp;C srl</td>
	<td>BOLOGNA</td>
	<td>IT</td>
	<td>VIA ANDREA COSTA, 202</td>
	<td>04330500374</td>
	<td>0</td>
	<td>21.220,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>21.220,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>IDEA CONGRESS SRL</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>VIA DELLA FARNESINA 224</td>
	<td>06551171009</td>
	<td>0</td>
	<td>26.840,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>26.840,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>INTERMEETING S.R.L</td>
	<td>PADOVA</td>
	<td>IT</td>
	<td>Via Niccolò Tommaseo 63/B</td>
	<td>05276320727</td>
	<td>0</td>
	<td>1.500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>ISTITUTO EUROPEO DI ONCOLOGIA SRL</td>
	<td>Milano</td>
	<td>IT</td>
	<td>VIA RIPAMONTI 435</td>
	<td>08691440153</td>
	<td>10.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>KEY CONGRESSI SRL</td>
	<td>TRIESTE</td>
	<td>IT</td>
	<td>PZA DELLA BORSA 7</td>
	<td>00839230323</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>LATITUDINEZERO DI MARTELLI SILVIA</td>
	<td>L'AQUILA</td>
	<td>IT</td>
	<td>VIA DEL CASTELVECCHIO 1</td>
	<td>01634020661</td>
	<td>0</td>
	<td>3.292,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.292,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MED STAGE SRL</td>
	<td>CASSANO D'ADDA</td>
	<td>IT</td>
	<td>VIA ROSSINI 10</td>
	<td>04749500965</td>
	<td>0</td>
	<td>4.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>4.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MEETING PLANNER srl</td>
	<td>BARI</td>
	<td>IT</td>
	<td>Via S. Matarrese, 2/4</td>
	<td>06178970726</td>
	<td>0</td>
	<td>10.200,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.200,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>METIS SOC. SCIENT.DEI MEDICI DI MED</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>G. MARCONI 25</td>
	<td>05344721005</td>
	<td>0</td>
	<td>36.769,47</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>36.769,47</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MGM CONGRESS&nbsp; SRL</td>
	<td>NAPOLI</td>
	<td>IT</td>
	<td>TRAVERSA PIETRAVALLE 8</td>
	<td>06068191219</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MICOM S.R.L.</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA BERNARDINO VERRO 12</td>
	<td>10547540152</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MIDI 2007</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>VIA GERMANICO 42</td>
	<td>09357181008</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MIKA GROUP SRL</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>FACCHINETTI 13</td>
	<td>12473811003</td>
	<td>0</td>
	<td>500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>MOMEDA EVENTI SRL</td>
	<td>BOLOGNA</td>
	<td>IT</td>
	<td>VIA SAN FELICE, 26</td>
	<td>02599851207</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>NADIREX INTERNATIONAL SRL</td>
	<td>PAVIA</td>
	<td>IT</td>
	<td>VIA RIVIERA, 39</td>
	<td>12521390158</td>
	<td>0</td>
	<td>5.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>5.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>OCM COMUNICAZIONI SNC</td>
	<td>TORINO</td>
	<td>IT</td>
	<td>VIA A. VESPUCCI 69</td>
	<td>07365990014</td>
	<td>0</td>
	<td>500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>OLYMPIA CONGRESSI SRL</td>
	<td>FIUMICINO</td>
	<td>IT</td>
	<td>VIA COPENAGHEN, 18</td>
	<td>07493381003</td>
	<td>0</td>
	<td>500,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>500,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>OSPEDALE PEDIATRICO BAMBIN GESU'</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>PIAZZA S. ONOFRIO, 4</td>
	<td>80403930581</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>PENTA EVENTI SRL</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>PIAZZA PIO XI 62</td>
	<td>11039271009</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>PLANNING CONGRESSI SRL</td>
	<td>BOLOGNA</td>
	<td>IT</td>
	<td>VIA GUELFA, 9</td>
	<td>03759300373</td>
	<td>0</td>
	<td>15.630,32</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>15.630,32</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>PREX S.p.A.</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA FAVA 25</td>
	<td>11233530150</td>
	<td>0</td>
	<td>2.785,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>2.785,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>PROJECT &amp; COMMUNICATION SRL</td>
	<td>BOLOGNA</td>
	<td>IT</td>
	<td>STRADA MAGGIORE 31</td>
	<td>02332271200</td>
	<td>0</td>
	<td>16.700,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>16.700,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>PROMOTER IN EVENTI SRL</td>
	<td>SAN SEVERO</td>
	<td>IT</td>
	<td>VLE MATTEOTTI 36</td>
	<td>03572430712</td>
	<td>0</td>
	<td>3.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>QUICKLINE SAS</td>
	<td>Trieste</td>
	<td>IT</td>
	<td>Via Santa Caterina da Siena, 3</td>
	<td>00760600320</td>
	<td>0</td>
	<td>11.192,40</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>11.192,40</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>SELENE srl</td>
	<td>TORINO</td>
	<td>IT</td>
	<td>VIA SACCHI, 58</td>
	<td>06640240013</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>SINTEX SERVIZI SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA ANTONIO DA RECANATE 2</td>
	<td>07926680963</td>
	<td>0</td>
	<td>1.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>1.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>SOCIETA' ITALIANA DI FARMACOLOGIA</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>V.LE ABRUZZI, 32</td>
	<td>11453180157</td>
	<td>3.000,00</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>SOCIETA' ITALIANA MEDICI DELLO SPORT E DELL'ESERCIZIO</td>
	<td>ROMA</td>
	<td>IT</td>
	<td>VIA AURORA 39</td>
	<td>02878740980</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>10.000,00</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>10.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>SPRINGER HEALTHCARE ITALIA SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA DECEMBRIO 28</td>
	<td>07103410960</td>
	<td>0</td>
	<td>34.507,28</td>
	<td>0,00</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>34.507,28</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>UNIVERSITA' DI CATANIA</td>
	<td>CATANIA</td>
	<td>IT</td>
	<td>Piazza Universit&amp;#xE0;&amp;#x2C; 2</td>
	<td>02772010878</td>
	<td>3.000</td>
	<td>0</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>3.000,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>UPDATE INTERNATIONAL CONGRESS SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA DEI CONTARINI 7</td>
	<td>12244670159</td>
	<td>0</td>
	<td>2.800,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>2.800,00</td>
 </tr>
<tr><td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>

	<td>VICTORY PROJECT CONGRESSI SRL</td>
	<td>MILANO</td>
	<td>IT</td>
	<td>VIA BRONZETTI 20</td>
	<td>03743140968</td>
	<td>0</td>
	<td>2.000,00</td>
  <td >0</td>
	<td>0</td>
	<td>0</td>
  <td >0</td>
	<td>&nbsp;</td>
	<td>2.000,00</td>
 </tr>		
    
	
	
                </tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter13">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>678.418,19</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>
		
        <div id="tabs-2">
            <table  class="tablesorter21">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>&nbsp;</td>
                      <td>0</td>
                     </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter22">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
    			
    			
                <tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter23">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>39.784,98</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <div id="tabs-3">
            <table  class="tablesorter31">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
				 </tbody>
			</table>

			<p>&nbsp;</p>
            <table class="tablesorter32">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
   			
                <tbody>
   					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter33">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>24.463,22</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div id="tabs-4">
            <table  class="tablesorter41">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>TOLLE THOMAS R.</td>
						<td>MUNCHEN</td>
						<td>DE</td>
						<td>VOLLMANNSTRASSE 32 B</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>7.000</td>
						<td>1.320,94</td>
						<td>0</td>
						<td>8.320,94</td>
					 </tr>                    
				</tbody>
			</table>

			<p>&nbsp;</p>
            <table class="tablesorter42">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
    			
    			
                <tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter43">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>112.117,46</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>  

        <div id="tabs-5">
            <table  class="tablesorter51">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter52">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>

				<tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter53">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>23.408,72</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div> 






        <div id="tabs-6">
            <table  class="tablesorter61">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter62">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>

				<tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter63">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>51208,01</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div id="tabs-7">
            <table  class="tablesorter71">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter72">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>

				<tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter73">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>68.841</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>



        <div id="tabs-8">
            <table  class="tablesorter81">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter82">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>

				<tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>ALONGSIDE</td>
						<td>CHIASSO</td>
						<td>CH</td>
						<td>VIA CANTONI 3</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>7.762,85</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter83">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div id="tabs-9">
            <table  class="tablesorter91">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

					<tr>
						<td>HCPs<br><span style='font-size:8px'>Operatori Sanitari</span></td>
						<td>PAGE CLIVE</td>
						<td>FETCHAM</td>
						<td>GB</td>
						<td>BYWAYS, COBHAM ROAD</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>3.000,00</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					 </tr>
                     </tbody>
                </table>

			<p>&nbsp;</p>
            <table class="tablesorter92">
				<thead>
                   <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2016<br />Pubblished on 30 June 2017</td>
                    </tr>
                    <tr>
                        <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;">
    						Full Name<br /><br />
    						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
    						<span style="color:red;">
    							<strong><span style='font-size:8px'>Operatori Sanitari</span></strong>: Citt&agrave;  dove si svolge  prevalentemente la professione<br />
    							<strong><span style='font-size:8px'>Organizzazioni Sanitarie</span></strong>: Sede Legale
    						</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Country of Principal Practice<br /><br />
    						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Principal Practice Address<br /><br />
    						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit&agrave;</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Unique country identifier<br /><br />
    						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						Donations and Grants to HCOs<br /><br />
    						<span style="color:red;">Donazioni e contributi a <span style='font-size:8px'>Organizzazioni Sanitarie</span></span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
    						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
    						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche) ( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
    						Fee for service and consultancy<br /><br />
    						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
    					</th>
                        <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                        <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
    						TOTAL OPTIONAL<br /><br />
    						<span style="color:red;">TOTALE Facoltativo</span>
    					</th>
                    </tr>
                    <tr>
    					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
    						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">da Punto 5.5 a 5.7 CD</span>
    					</th>

    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Modulo 1CE)<br /><br />
    						<span style="color:red;">Allegato 2(CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3 CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3CE)<br /><br />
    						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
    					</th>
    					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
    						(Art. 3.01.1.a CE)<br /><br />
    						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
    					</th>
    					
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
    						<span style="color:red;">Accordi di sponsorizzazione con <span style='font-size:8px'>Organizzazioni Sanitarie</span>/soggetti terzi nominati da <span style='font-size:8px'>Organizzazioni Sanitarie</span> per la realizzazione di eventi</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Registration Fees<br /><br />
    						<span style="color:red;">Quote di iscrizione</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Travel & Accomodation<br /><br />
    						<span style="color:red;">Viaggi e  ospitalit&agrave;</span>
    					</th>
                        <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Fees<br /><br />
    						<span style="color:red;">Corrispettivi</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
    						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
    						<span style="color:red;">Spese riferibili ad attivit&agrave; di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit&agrave;</span>
    					</th>
    					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                    </tr>
                </thead>

				<tbody>
					<tr>
						<td>HCOs<br><span style='font-size:8px'>Organizzazioni Sanitarie</span></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
						<td>0</td>
					</tr>
				</tbody>
            </table>

            <p>&nbsp;</p>
            <table class="tablesorter93">
                <thead>
                    <tr>
                        <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>R&D</td>
                        <td colspan="11">
    						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
    						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione - Punto 5.8 e 5.9 e Allegato 2 CD</span>
    					</td>
                        <td>160.976,63</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>











 		
	</div>     
    </form>

	
	</body>
</html>
