﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
    <script type="text/javascript" src="/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.widgets.js"></script>
    <script>
        $(function () {
            $('table').tablesorter({
                theme: 'blue',
                duplicateSpan: true, // default setting
                widthFixed: true,
                widgets: ['zebra', 'filter'],
                widgetOptions: {
                    filter_external: 'input.search',
                    filter_reset: '.reset'
                }
            });

            $('.sort').click(function () {
                $('table').trigger('sorton', [[[$(this).text(), 'n']]]);
            });

        });
    </script>
    <link rel="stylesheet" href="/ProjectDOMPE/_js/tablesorter/css/theme.blue.css">
    <style>
        .tablesorter-blue td[colspan] { color: red; } 
    </style>
</head>
<body>
    <form id="form1" runat="server">
<ul>
  <li>Sort Column <button class="sort" type="button">0</button> <button class="sort" type="button">1</button>
    (toggle sort direction) - There is no method to use the UI to sort the second column because it has no header; use "sorton" instead.
  </li>
  <li>Search:
    <button class="search" type="button" data-column="2">zyx</button>
    <button class="search" type="button" data-column="3">7</button>
    <button class="search" type="button" data-column="4">Koala</button>
    <button class="search" type="button" data-column="5">edu</button>,
    then toggle <code>duplicateSpan</code> : <button id="dupe" type="button">true</button>.
  </li>
  <li>Searching the first two columns <sup class="results xsmall">†</sup>:
    <ul>
      <li>Search using column <code>0</code> (zero):<br>
        <button class="search" type="button" data-column="0">4</button> (nothing visible in column filter)<br>
        <button class="search" type="button" data-column="1">>4</button> (search second column, nothing visible in filter)
      </li>
      <li>Search using column <code>6</code> (used by "all" filter):<br>
        <button class="search" type="button" data-column="6">4</button> (search both index columns)<sup class="results xsmall">‡</sup><br>
        <button class="search" type="button" data-column="6">1:4</button> (only search "Group" column)<br>
        <button class="search" type="button" data-column="6">2:>4</button> (search second column)
        </li>
    </ul>
  </li>
</ul>

Search:
<input type="search" class="search" data-column="all" placeholder="Search all columns"><sup class="results xsmall">‡</sup>
<button class="reset">Reset</button>
<code id="show-filter"></code>

<p class="xsmall"><span class="results">†</span> The reason for this issue is that the filter input in the index column has this setting:
<code>data-column="0-1"</code>, and it has not yet been worked out how to properly target that input.<br>
<span class="results">‡</span> It is still being investigated as to why the search using the button targeting column 6 and the "all" input have different results (Enter "4" in the input and 4 rows will appear in the result, then click on the "4" to search both index columns - one less row).
</p>

<table class="tablesorter">
  <thead>
    <tr>
      <th rowspan="2" colspan="2">Index (colspan 2)</th>
      <th colspan="4">Products</th>
    </tr>
    <tr>
      <th>Product ID</th>
      <th>Numeric</th>
      <th>Animals</th>
      <th>Url</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="2">Index</th>
      <th>Product ID</th>
      <th>Numeric</th>
      <th id="test">Animals</th>
      <th>Url</th>
    </tr>
  </tfoot>
  <tbody>
    <tr><td>Group 1</td><td style="width:100px">6</td><td>abc 9</td><td>155</td><td>Lion</td><td>http://www.nytimes.com/</td></tr>
    <tr><td>Group 4</td><td>1</td><td>abc 1</td><td>237</td><td colspan="2">Ox http://www.yahoo.com</td></tr>
    <tr><td>Group 1</td><td>2</td><td colspan="4">zyx 1 957 Koala http://www.mit.edu/</td></tr>
    <tr><td>Group 0</td><td>5</td><td>abc 2</td><td>56</td><td>Elephant</td><td>http://www.wikipedia.org/</td></tr>
    <tr><td>Group 3</td><td>0</td><td>abc 123</td><td colspan="2">17 Koala</td><td>http://www.google.com</td></tr>
    <tr><td>Group 2</td><td>8</td><td>zyx 9</td><td>10</td><td>Girafee</td><td>http://www.facebook.com</td></tr>
    <tr><td>Group 1</td><td>3</td><td colspan="2">zyx 4 767</td><td>Bison</td><td>http://www.whitehouse.gov/</td></tr>
    <tr><td>Group 2</td><td>4</td><td>abc 11</td><td>3</td><td>Chimp</td><td>http://www.ucla.edu/</td></tr>
    <tr><td>Group 4</td><td>7</td><td colspan="2">ABC 10 87</td><td>Zebra</td><td>http://www.google.com</td></tr>
    <tr><td>Group 3</td><td>9</td><td>zyx 12</td><td>0</td><td>Koala</td><td>http://www.nasa.gov/</td></tr>
  </tbody>
</table>
    </form>
</body>
</html>