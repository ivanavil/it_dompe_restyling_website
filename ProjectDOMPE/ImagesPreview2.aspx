﻿<%@ Page Language="VB" ClassName="DeckSlideM" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Localization" %>
<%@ Import Namespace="Healthware.Hp3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  
<script runat="server">
    Dim Folder As String
    Dim idLang As Integer = 0
    Dim itemSlide As Integer = 0
    Dim deckKey As Integer = 0
    Dim deckTitle As String = String.Empty
    Dim themeKey As Integer = 0
    Dim saKey As Integer = 0
    Dim ctKey As Integer = 0
    Dim maxShow As Integer = 5
 
    Dim strPrev As String = "Prev"
    Dim strNext As String = "Next"
    Dim strCartella As String
    Dim strDownloadHyperLink As String = String.Empty
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
      '  Response.Write(Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(108)) : Response.End()
        If Not Request("galleryKey") Is Nothing Then
            strCartella = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("galleryKey"))
            Integer.TryParse(strCartella, deckKey)
        End If
        '    Integer.TryParse((Request("galleryKey")), deckKey)
        ' deckKey = 108
        '  GetImagesFolder()
        If deckKey > 0 Then
            strBoxImages.Text = GetImmagini()
        End If
        TraceEvent(54, deckKey)
    End Sub
    
    Sub GetImagesFolder()
        Dim path As String = Server.MapPath("/hp3image/PhotoGallery/" & deckKey)
        If Directory.Exists(path) Then
            Dim filePath As String() = Directory.GetFiles(path, "*.jpg")
            Dim startInt As Integer = 0
            Dim lastItm As Integer = filePath.Length
            Dim contatore As Integer = 0
            Dim block As Integer = 0
            For Each itm As String In filePath
                Dim strimg As String = ""
                If startInt = 0 Then
                    startInt = itm.IndexOf("\hp3image\PhotoGallery\")
                End If
                strimg = "<li><a href=""" & itm.Substring(startInt).Replace("\", "/") & """><img src=""" & itm.Substring(startInt).Replace("\", "/") & """ /></a></li>"
                strBoxImages.Text = strBoxImages.Text + strimg
            Next
        End If
    End Sub
    

    Function GetImmagini() As String
        Dim _out As String = String.Empty
        
        Dim xmlDocument As New XmlDocument()
        xmlDocument.Load(Request.PhysicalApplicationPath & "/HP3Image/PhotoGallery/" & deckKey & "/Gallery.xml")
        For Each node As XmlNode In xmlDocument.SelectNodes("/items/Download")
            If node.Name = "Download" And node.Attributes("active").InnerText = 1 Then
                strDownloadHyperLink = String.Format("<div id='buttons'><a target='_blank' title='{2}' href='{1}' style='padding:8px;position:absolute:top:0;left:0;display:block;color:#E20025;width:120px;height:100%;'>{0}</a></div>", node.SelectSingleNode("label").InnerText, node.SelectSingleNode("hyperlink").InnerText, node.SelectSingleNode("title").InnerText)
            End If
        Next
        
        For Each node As XmlNode In xmlDocument.SelectNodes("/items/Row")
            _out = _out & String.Format("<li><a href='/HP3Image/PhotoGallery/{0}/FullScreen/{1}'><img {3}src='/HP3Image/PhotoGallery/{0}/FullScreen/{1}' alt='{2}'/></a></li>", deckKey, node.SelectSingleNode("NomeFile").InnerText.Replace("'", "´"), node.SelectSingleNode("NomeFile").InnerText.Replace("'", "´"), IIf(String.IsNullOrEmpty(node.SelectSingleNode("Descrizione-it").InnerText.Replace("'", "´")), String.Empty, " caption='' "))
        Next
        'Dim xmlDS = XElement.Load(Request.PhysicalApplicationPath & "/HP3Image/PhotoGallery/" & deckKey & "/Gallery.xml")
        'Dim query = From Row In xmlDS.Elements Select Row
        'For Each row As XElement In query
        '_out = _out & String.Format("<li><a href='/HP3Image/PhotoGallery/{0}/FullScreen/{1}'><img {3}src='/HP3Image/PhotoGallery/{0}/FullScreen/{1}' alt='{2}'/></a></li>", deckKey, row.Element("NomeFile").Value.Replace("'", "´"), row.Element("NomeFile").Value.Replace("'", "´"), IIf(String.IsNullOrEmpty(row.Element("Descrizione-it").Value.Replace("'", "´")), String.Empty, " caption='" & row.Element("Descrizione-it").Value.Replace("'", "´") & "' "))
        'Next

        Return _out
    End Function

    Sub TraceEvent(ByVal idEvent As Integer, deckKey As Integer)
        Dim traceVal As New TraceValue
        traceVal.KeySite.Id = 46
        traceVal.KeyContent.Id = deckKey
        traceVal.KeySiteArea.Id = Request("sa")
        traceVal.KeyContentType.Id = Request("ct")
        traceVal.KeyEvent.Id = idEvent
        traceVal.KeyUser.Id = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("u"))
        traceVal.KeyLanguage.Id = Request("lk")
        traceVal.CustomField = Request("cS")
        traceVal.DateInsert = System.DateTime.Now
        traceVal.IdSession = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("s"))
        Me.BusinessTraceService.Create(traceVal)
    End Sub
</script>

<html xmlns ="http://www.w3.org/1999/xhtml" >
    <head>
        <title><%=Me.ObjectSiteDomain.Label%></title>
        <link rel="stylesheet" title="Standard" type="text/css" media="screen" href="/ProjectDOMPE/_css/style.css.axd" />
        <link href="/ProjectDOMPE/_css/en/style.css.axd" rel="stylesheet" type="text/css" />

        <link href="/ProjectDOMPE/_js/jbgallery/jbgallery-3.0.css" rel="stylesheet" media="screen" />
        <script src="/ProjectDOMPE/_js/jbgallery/libs/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="/ProjectDOMPE/_js/jbgallery/jbgallery-3.0.js" type="text/javascript"></script>
        <script src="/ProjectDOMPE/_js/jbgallery/libs/docs/jquery.scrollTo-1.4.2-min.js" type="text/javascript"></script>
        <script src="/ProjectDOMPE/_js/jquery.spritezoom.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#fullscreen").jbgallery({
                    menu: 'slider',
                    style: 'centered',
                    randomize: 2,
                    slideshow: false,
                    current: 0,
                    caption: true,
                    autohide: false,
                    labels: {                //labels of internal menu
                        play: "play",
                        next: "<%=strnext %>",
                        prev: "<%=strprev %>",
                        stop: "stop",
                        close: "close",
                        info: "info"
                    },
                    timers: {                //timers
                        fade: 400,           //fade duration
                        interval: 7000,      //slideshow interval
                        autohide: 3000       //autohide timeout
                    },
                    delays: {                //delays (timers) to improve usability and reduce events
                        mousemove: 200,      //used by autohide. cancel autohide timeout every XXXms.
                        resize: 500,         //used by ie6 to reduce events handled by window.resize
                        mouseover: 800       //used by tooltip. show tooltip only if the mouse STAY over thumb for XXXms
                    }
                });
				
				/*
				setTimeout(function(){ $(".zoom-demo").spritezoom({}); alert('asd');}, 10000);
				*/
				
            });

            jQuery(document).ready(function() {
                jQuery('a.blank').each(function() {
                    this.target = "_blank";
                });
            });
        </script>
        

        <style type="text/css"> 
            body{font-family:FreeSans,Arial,Helvetica,sans-serif;color:#fff;line-height:17px;letter-spacing:1px;}
            .toolbar a, .toolbar a:visited, .toolbar a:focus,
            .cnt a, .cnt a:visited, .cnt a:focus{color:#fff;border:0;outline: none;}
	        .toolbar a:hover,.toolbar a:focus,
            .cnt a:hover,.cnt a:focus{border-bottom:2px solid #69C;}
            .cnt a, .cnt a:visited, .cnt a:focus{text-decoration:underline;}
            .cnt h2,.cnt h3,.cnt h4{color:#69C;text-transform:uppercase;}
            .cnt .strike{text-decoration:overline;}
            
            #toggle-docs, #docs .cnt, #jbg-menu{background:url('img/opacity.png') repeat;}
            #docs{width:600px;right:100px;top:10%;margin-bottom:50px;position:absolute;z-index:1000;}
            #docs .wrapper{padding:10px;margin:10px;}
            #docs .cnt{margin-top:30px;padding-top:20px;padding-bottom:20px;}
            
            #credits, #nav{z-index:1001;position:fixed;bottom:0;text-decoration:none;font-size:11px; background:#000;padding:5px;margin:0px;}
            #credits{left:0;z-index:1002;}
            #nav{width:100%;}
            
	        .toolbar ul {display:block;list-style-type:none;margin:6px;padding:0;}
            .toolbar ul li {float:right;margin-right:auto;margin-left:auto;}
            .toolbar ul li a, .toolbar ul li a:visited {text-decoration:none;display:block;border-bottom:4px solid transparent;margin-right:10px;}
	        .toolbar ul li a:hover,.toolbar ul li a:focus{border-bottom:4px solid #69C;}
            
            #fullscreen .jbg-caption{bottom:26px;left:0px;right:auto;top:auto;border:0px;}
            #docs .jbgallery a{border-bottom:0px;}
            
            #buttons {z-index:1500;position:absolute;left:0;margin-left:5px;border:solid 1px #E20025;width:136px;font-family:'helvetica neue';font-weight:normal;text-align:center;bottom:75px;display:block;}
        </style>
    </head>
    <body>
        <form id="formDeck" runat="server">
            <asp:ScriptManager id="scrMan" runat="server"></asp:ScriptManager>
            
                <%=strDownloadHyperLink%>

                <%--
                <a href="#" onclick="javascript:openSm();" title="Click here to edit deck"><img alt="Click here to edit deck" src="/nulojix/jbgallery/img/edit.gif" alt="" /></a><%'=deckTitle%>
                <div style="margin:20px 0;"></div>
                <a href="#" onclick="<%=script%>" title="Click here to download deck"><img alt="Click here to download deck" src="/nulojix/jbgallery/img/download.gif" alt="" /></a>
                --%>
            
            
            <div class="jbgallery" id="fullscreen">
    	      <ul>
                   <asp:Literal ID="strBoxImages" runat="server" />
    	        </ul>   
            </div>
            <iframe id="iframePopDownDP" style="display:none;" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" src="/<%=Folder%>/source.htm" width="1px" height="1px"></iframe>
        </form>
    </body>
</html>