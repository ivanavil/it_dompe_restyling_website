﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="System.Xml"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Xml.Linq"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<script language="vb" runat="server">     
    Protected _siteFolder As String = String.Empty
    Dim immaginiSlider As String
    Dim idPhotogallery As Integer
    Dim language As String
    Dim hasApple As Boolean = False
    Private _isMobile As Boolean = False

    Private width As Integer = 750
    Private height As Integer = 520
    Private _galleryAddHeight As Integer = 0
	
	Dim _mainContId as Integer = 0
	Dim _thumStrip as String = String.Empty
	Dim cdiv as Integer = 0
	Dim _galleryAll As String = String.Empty
						
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		_isMobile = IsMobileDevice()
	
        idPhotogallery = 0
        language = Request("l").ToLower
        idPhotogallery = Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("cs")), idPhotogallery)
		
		'idPhotogallery = Id Contenuto principale
        
        If Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("cs")), idPhotogallery) = True Then
            idPhotogallery = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("cs"))
           
            Dim _out As String = String.Empty
            Dim so As New ContentSearcher
            Dim cntVal As New ContentValue
            Dim soRel As New ContentRelatedSearcher
            Dim coll As ContentCollection = Nothing
		  
            If idPhotogallery > 0 Then
                soRel.Content.Key.PrimaryKey = idPhotogallery
                soRel.Content.KeyRelationType = New RelationTypeIdentificator(1)
                soRel.Content.RelationSide = RelationTypeSide.Left
            End If
			
            so.KeySite = Me.PageObjectGetSite
            so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDVideoGallery
            so.SortOrder = ContentGenericComparer.SortOrder.DESC
            so.SortType = ContentGenericComparer.SortType.ByData
            so.RelatedTo = soRel
            coll = Me.BusinessContentManager.Read(so)
			
			
            Dim sb As New StringBuilder()
            If Not coll Is Nothing AndAlso coll.Any Then
                Dim oMainCont As ContentValue = coll(0)
                If Not oMainCont Is Nothing Then
                    _mainContId = oMainCont.Key.PrimaryKey
                End If
				
                If coll.Count > 1 Then
					_galleryAddHeight = 140
                    Dim _limit As Integer = 5
                    Dim i As Integer = 0
                    For Each oCont As ContentValue In coll
                        If i Mod _limit = 0 Then
                            cdiv += 1
                            sb.Append("<div class='dCont' id='dCont" & cdiv & "'>")
                        End If
                        sb.Append("<a id='newVideo" & oCont.Key.Id & "' " & IIf(i = 0, " class='transparent_class'", String.Empty) & " href='#' onclick='javascript:changeVideo(" & oCont.Key.Id & ", " & oCont.Key.PrimaryKey & ");'><img id='" & oCont.Key.PrimaryKey & "' class='thumb' title='' src='" & getImage(oCont.Key.PrimaryKey) & "' /></a>")
                        If i Mod _limit = (_limit - 1) Then
                            sb.Append("</div>")
                        End If
                        i += 1
                        
                        TraceEvent(55, oCont.Key.Id)
                    Next
					
					_galleryAll = " width:" & (cdiv * 100) & "% !important;"

                    If i Mod _limit <> (_limit - 1) Then
					'sb.Append("</div>")
                    End If
                    _thumStrip = sb.ToString()
                Else
                    TraceEvent(55, Request("c"))
                End If
            End If
            
        Else
            idPhotogallery = 0
            myForm.Visible = False
        End If
                
        If Not String.IsNullOrEmpty(Request("v")) AndAlso Request("v") = 2 Then
            If Not String.IsNullOrEmpty(Request("w")) Then
                width = Request("w")
            End If
            If Not String.IsNullOrEmpty(Request("h")) Then
                height = Request("h")
            End If
        End If

        _siteFolder = Me.ObjectSiteFolder()
    End Sub
    
    Sub TraceEvent(ByVal idEvent As Integer, Key As Integer)
        Dim traceVal As New TraceValue
        traceVal.KeySite.Id = 46
        traceVal.KeyContent.Id = Key
        traceVal.KeySiteArea.Id = Request("sa")
        traceVal.KeyContentType.Id = Request("ct")
        traceVal.KeyEvent.Id = idEvent
        traceVal.KeyUser.Id = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("u"))
        traceVal.KeyLanguage.Id = Request("lk")
        traceVal.CustomField = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("cS"))
        traceVal.DateInsert = System.DateTime.Now
        traceVal.IdSession = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("s"))
        Me.BusinessTraceService.Create(traceVal)
    End Sub
    
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
            
        Return False
    End Function
	
    Function getImage(ByVal videoPk as Integer) As String
        Return "/HP3Image/cover/" & Me.BusinessContentManager.GetCover(New ContentIdentificator(videoPk), width)
    End Function
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
    <title>www.dompe.com | Video Gallery</title>
    <link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/Dompe.css" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
	<link rel="stylesheet" href="/ProjectDOMPE/_js/css/jquery-ui.css">
	
	<script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
	<script type="text/javascript" src="/ProjectDOMPE/_js/ui/jquery-ui.js.axd"></script>	   
</head>

<body s>
  <style type="text/css">
	.transparent_class {
	  /* IE 8 */
	  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=20)";

	  /* IE 5-7 */
	  filter: alpha(opacity=20);

	  /* Netscape */
	  -moz-opacity: 0.2;

	  /* Safari 1.x */
	  -khtml-opacity: 0.2;

	  /* Good browsers */
	  opacity: 0.2;
	}  
  
.XClose {
	background-image: url("/Js/shadowbox/close.png");
	background-repeat: no-repeat;
	cursor: pointer;
	float: right;
	height: 16px;
	width: 16px;
	position:absolute;
	top: 0px;
	right:5px;
	text-indent: -9999;
}
.content {
	margin: 0 !important; 
	padding: 0 !important; 
}
			
.galleryTop{
	clear:both;
	float:left;
	width:100%;
	position:relative;
	height:100px;
	overflow:hidden;
}
.galleryAll{
	position:absolute;
	width:100%;
	height:84px;
	left:0;
	top:0;
}
.dCont{
	float:left;
	width:710px;
	height:86px;
	margin:0;
	padding:5px;
}
		
.dCont a{
	float:left;
	width:130px;
	margin-right:10px;
	padding:0;
	border:1px solid #000;
	cursor:pointer;
}
.dCont a img{
	width:130px;
}


.prev{
	position:absolute;
	left:0;
	top:32px;
	width:20px;
	height:20px;
	background-color:#000;
	color:#fff;
	text-decoration:none;
	text-align:center;
}
.next{
	position:absolute;
	/*left:480px;*/
	right:5px;
	top:32px;
	width:20px;
	height:20px;
	background-color:#000;
	color:#fff;
	text-decoration:none;
	text-align:center;
}		
</style>
<form id="myForm" runat="server" enctype="multipart/form-data">          
	<div class="container" style="margin:0;padding:0;width:<%=width%>px;float:left; height:<%=height + _galleryAddHeight%>px; overflow:hidden;">
		<div class="content" style="margin:0;padding:0;width:720px;float:left;">
				<div id="rg-gallery" class="rg-gallery" style="height:<%=height%>px;">
					<% If _isMobile Then%>
						  <video id="player" src="http://<%=Me.ObjectSiteDomain.Domain%>/HP3Media/File/<%=_mainContId%>.mp4" width="<%=width%>"px" height="<%=height%>px" controls="" poster="<%=getImage(_mainContId)%>"></video>
					<%Else%>
						<object width="<%=width%>" height="<%=height%>" id="video_content" name="video_content"  type="application/x-shockwave-flash"  data="/ProjectDOMPE/_swf/mediaplayerWide.swf">
							<param name="movie" value="/ProjectDOMPE/_swf/mediaplayerWideTeaser.swf" />
							<param id="flashvars" name="flashvars" value="fireExternalNotify=true&src=/HP3Media/File/<%=_mainContId%>.mp4&amp;width=<%=width%>&amp;height=<%=height%>&amp;tumb=<%=getImage(_mainContId)%>&amp;skin=/ProjectDOMPE/_swf/videoPlayerDefaultSkin.swf&amp;keepRatio=false&amp;backcolor=0x000000&amp;frontcolor=0xCCCCCC&amp;lightcolor=0x557722&amp;endCallBack=endVideo" />
							<param name="allowfullscreen" value="true" />
							<param name="allowScriptAccess" value="sameDomain" />
							<param name="swLiveConnect" value="true" />
						</object>  
				   <%End If%>
				</div><!-- rg-gallery -->
				<div class="galleryTop"><style type="text/css">.galleryAll{<%=_galleryAll%>}</style>
					<div class="galleryAll">
						<%=_thumStrip%>
					</div>
					<a href="javascript:void(0);" title="Prev" class="prev" style="display:none;">&laquo;</a>
					<a href="javascript:void(0);" title="Next" class="next">&raquo;</a>
				</div>
			</div><!-- content -->
		</div><!-- container -->
	</form> 
	<script type="text/javascript">
	   function testSHB() {
		   $("#video_content").remove();
		   $("#video_content").empty();
		   parent.Shadowbox.close();
	   }
	   
	   function changeVideo(newVideoId, newVideoPk){
			var newPlayer = '';
			<% If _isMobile Then%>
				newPlayer = "<video id='player' src='http://<%=Me.ObjectSiteDomain.Domain%>/HP3Media/File/" + newVideoPk +".mp4' width='<%=width%>px' height='<%=height%>px' controls='' poster='/HP3Image/cover/"+ newVideoId + "_gen_500.jpg'></video>";
			<% Else %>
				var newFlashVar = "fireExternalNotify=true&src=/HP3Media/File/" + newVideoPk + ".mp4&width=500&height=300&tumb=/HP3Image/cover/" + newVideoId + "_gen.jpg&skin=/ProjectDOMPE/_swf/videoPlayerDefaultSkin.swf&keepRatio=false&backcolor=0x000000&frontcolor=0xCCCCCC&lightcolor=0x557722&endCallBack=endVideo";
				newPlayer = "<object width='<%=width%>' height='<%=height%>' id='video_content' name='video_content'  type='application/x-shockwave-flash'  data='/ProjectDOMPE/_swf/mediaplayerWide.swf'><param name='movie' value='/ProjectDOMPE/_swf/mediaplayerWideTeaser.swf' /><param id='flashvars' name='flashvars' value='" + newFlashVar + "' /><param name='allowfullscreen' value='true' /><param name='allowScriptAccess' value='sameDomain' /><param name='swLiveConnect' value='true' /></object>";
			<% End If %>
			$("#rg-gallery").html('');
			$("#rg-gallery").html(newPlayer);
			
			$(".galleryAll .dCont a" ).each(function( index ) {
				$(this).removeAttr("class");
			});
			$('#newVideo' + newVideoId).attr("class", "transparent_class");
	   }
	   
		var cntSliderMap=1;
		var totItem=<%=cdiv%>;
		$( ".galleryTop .prev" ).click(function() {
			 $( ".galleryTop .galleryAll" ).animate({left: "+=100%"}, 1000, function() {});
			 cntSliderMap--;
			 if(cntSliderMap==1){
				 $( ".galleryTop .prev" ).css("display","none");
			 }
			 $( ".galleryTop .next" ).css("display","block");
		});
		$( ".galleryTop .next" ).click(function() {
			 $( ".galleryTop .galleryAll" ).animate({left: "-=100%"}, 1000, function() {});
			 cntSliderMap++;
			 if(cntSliderMap==totItem){
				 $( ".galleryTop .next" ).css("display","none");
			 }
			 $( ".galleryTop .prev" ).css("display","block");
		});

		$(document).ready(function(){
			if (totItem < 2)
				$( ".galleryTop .next" ).hide();
		});
	</script>
</body>
</html>
