﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="System.Xml"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Xml.Linq"%>

<script language="vb" runat="server">     
    Protected _siteFolder As String = String.Empty
    Dim immaginiSlider As String
    Dim idPhotogallery As Integer
    Dim language As String
    Dim hasApple As Boolean = False
    Private _isMobile As Boolean = False

    Private width As Integer = 750
    Private height As Integer = 520
    
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        idPhotogallery = 0
        language = Request("l").ToLower
        idPhotogallery = Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c")), idPhotogallery)
        If Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c")), idPhotogallery) = True Then
        
            idPhotogallery = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c"))
          
        Else
            idPhotogallery = 0
            myForm.Visible = False
        End If
                
        If Not String.IsNullOrEmpty(Request("v")) AndAlso Request("v") = 2 Then
            If Not String.IsNullOrEmpty(Request("w")) Then
                width = Request("w")
            End If
            If Not String.IsNullOrEmpty(Request("h")) Then
                height = Request("h")
            End If
        End If

        _siteFolder = Me.ObjectSiteFolder()
    End Sub
    
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
            
        Return False
        
    End Function
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        '  immaginiSlider = GetVideo()
        _isMobile = IsMobileDevice()
        Dim userAgent As String = HttpContext.Current.Request.UserAgent.ToLower()
        'If (userAgent.Contains("iphone") Or userAgent.Contains("ipad")) Then
        '    hasApple = True
        'End If
        
        If Not Me.IsPostBack Then
            'tracciamento
            Me.BusinessMasterPageManager.Trace()
            Me.BusinessMasterPageManager.TraceExtend()
        End If
        
    End Sub
    Function getImage() As String
        Return "/HP3Image/cover/" & Me.BusinessContentManager.GetCover(New ContentIdentificator(idPhotogallery), width)
    End Function
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
    <title>
            DOMPE
    </title>
    <link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
    <link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/Dompe.css" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/elastislide.css" />
         
          
</head>

  <body>
  <style type="text/css">
				.es-carousel ul{
					display:block;
				}
				#sb-wrapper-inner
				{
				  height: 550px !important;  
				}	
			.XClose 
			{
			     background-image: url("/Js/shadowbox/close.png");
                 background-repeat: no-repeat;
                 cursor: pointer;
                 float: right;
                 height: 16px;
                 width: 16px;
                 position:absolute;
                 top: 0px;
                 right:5px;
                 text-indent: -9999;
            }
            .content
            {
                margin: 0 !important; 
				padding: 0 !important; 
            }
     	</style>
  <form id="myForm" runat="server" enctype="multipart/form-data">          
     		
<div class="container" style="margin:0;padding:0;">
		<div class="content" style="margin:0;padding:0;">
            
				<!--<a class="XClose" href="javascript:void(0);" onclick="testSHB()" style='display:<%=IIf(Request("v") = 2, "none", "block")%>;'>&nbsp;</a>-->
       
				<div id="rg-gallery" class="rg-gallery">
				
				
					<% If _isMobile Then%>
						  <video id="player" src="http://<%=Me.ObjectSiteDomain.Domain%>/HP3Media/File/<%=idPhotogallery%>.mp4" width="<%=width%>"px" height="<%=height%>px" controls="" poster="<%=getImage()%>"></video>
					<%Else%>
						<object width="<%=width%>" height="<%=height%>" id="video_content" name="video_content"  type="application/x-shockwave-flash"  data="/ProjectDOMPE/_swf/mediaplayerWide.swf">
							<param name="movie" value="/ProjectDOMPE/_swf/mediaplayerWideTeaser.swf" />
							<param name="flashvars" value="fireExternalNotify=true&src=/HP3Media/File/<%=idPhotogallery%>.mp4&amp;width=<%=width%>&amp;height=<%=height%>&amp;tumb=<%=getImage()%>&amp;skin=/ProjectDOMPE/_swf/videoPlayerDefaultSkin.swf&amp;keepRatio=false&amp;backcolor=0x000000&amp;frontcolor=0xCCCCCC&amp;lightcolor=0x557722&amp;endCallBack=endVideo" />
							<param name="allowfullscreen" value="true" />
							<param name="allowScriptAccess" value="sameDomain" />
							<param name="swLiveConnect" value="true" />   
						</object>  
				   <%End If%>
           
          
		   
              
				</div><!-- rg-gallery -->
			
			</div><!-- content -->
		</div><!-- container -->

     </form> 
  
   <script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
   <script type="text/javascript">
       function testSHB() {
           $("#video_content").remove();
           $("#video_content").empty();
           parent.Shadowbox.close();
         
       }
   </script>
    </body>

</html>


