﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Utility" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="System.Xml"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Xml.Linq"%>

<script language="vb" runat="server">     
    Protected _siteFolder As String = String.Empty
    Dim immaginiSlider As String
    Protected idPhotogallery As Integer = 0
    Dim language As String
    Dim inPage As Boolean = False
    Protected _title As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _FullScreenLink As String
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
		_langKey = Me.PageObjectGetLang
        idPhotogallery = 0
        language = Request("l").ToLower
        idPhotogallery = Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("c")), idPhotogallery)
        inPage = (Request("inpage") = 1)
        If Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("c")), idPhotogallery) = True Then
            idPhotogallery = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("c"))
        Else
            idPhotogallery = 0
            myForm.Visible = False
        End If
        _siteFolder = Me.ObjectSiteFolder()
        
        _FullScreenLink = "<a id='fullScreenLink' style='font-family: Museo300,Arial,Helvetica,sans-serif;position:absolute; left:240px;top: 478px;color:black;z-index:97;width:auto;display:block;font-weight:bold;' href='javascript:openGallery();' title='Full Screen View'>Full Screen View</a>"
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Not Me.BusinessAccessService.IsAuthenticated Then
        '    Me.ClientLaunchScript("parent.document.location.reload();")
        'Else
        Dim phG = ReadPhotogallery()
        _title = Me.BusinessDictionaryManager.Read("ViewGallery", _langKey.Id, "ViewGallery") 'phG.Title
        immaginiSlider = GetImmagini()
        If Not Me.IsPostBack Then
            'tracciamento
            'Me.BusinessMasterPageManager.Trace()
            'Me.BusinessMasterPageManager.TraceExtend()
        End If
        ' End If
    End Sub
    
    Sub TraceEvent(ByVal idEvent As Integer, Optional ByVal idUser As Integer = 0)
        Dim traceVal As New TraceValue
        traceVal.KeySite.Id = 46
        traceVal.KeyContent.Id = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("c"))
        traceVal.KeySiteArea.Id = Request("sa")
        traceVal.KeyContentType.Id = Request("ct")
        traceVal.KeyEvent.Id = idEvent
        traceVal.KeyUser.Id = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("u"))
        traceVal.KeyLanguage.Id = Request("lk")
        traceVal.DateInsert = System.DateTime.Now
        traceVal.IdSession = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(Request("s"))  'da fare vedere se è possibile mantenerlo
        Me.BusinessTraceService.Create(traceVal)
    End Sub
    
    Private Function ReadPhotogallery() As ContentExtraValue
        Dim so As New ContentExtraSearcher()
        Dim sum As Integer = 0
        Dim tmpInt As Integer = 0
        so.CalculatePagerTotalCount = False
        so.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        so.KeyContentType.Id = Me.PageObjectGetContentType.Id
        so.key.IdLanguage = Me.PageObjectGetLang.Id
        so.key.Id = idPhotogallery
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SetMaxRow = 1
        so.Properties.Add("Title")

        Dim phGColl = Me.BusinessContentExtraManager.Read(so)
        If Not phGColl Is Nothing AndAlso phGColl.Any() Then
            Return phGColl.FirstOrDefault()
        End If
        Return Nothing
    End Function
    
    Function GetImmagini() As String
        Dim _out As String = String.Empty
        Dim xmlDS = XElement.Load(Request.PhysicalApplicationPath & "/HP3Image/PhotoGallery/" & idPhotogallery & "/Gallery.xml")
        Dim query = From Row In xmlDS.Elements Select Row

        For Each row As XElement In query
		
		Try
            If language = "it" Then
                _out = _out & String.Format("<li><a href='{0}'><img src='/HP3Image/PhotoGallery/{1}/thumbs/{2}' data-large='/HP3Image/PhotoGallery/{1}/{2}' alt='{3}' data-description='{3}' /></a></li>", row.Element("Indirizzo").Value, idPhotogallery, row.Element("NomeFile").Value.Replace("'", "´"), row.Element("Descrizione-it").Value.Replace("'", "´"))
            Else
                _out = _out & String.Format("<li><a href='{0}'><img src='/HP3Image/PhotoGallery/{1}/thumbs/{2}' data-large='/HP3Image/PhotoGallery/{1}/{2}' alt='{3}' data-description='{3}' /></a></li>", row.Element("Indirizzo").Value, idPhotogallery, row.Element("NomeFile").Value.Replace("'", "´"), row.Element("Descrizione-en").Value.Replace("'", "´"))
            End If		
		Catch
		End Try
		

        Next
        Return _out
    End Function
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
	<title>DOMPE</title>
	<link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/Dompe.css" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
     <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/style.css" />
    <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/style_new.css" />
	<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/elastislide.css" />
</head>
<body>
	<style type="text/css">
		.es-carousel ul{display:block;}
		.XClose {
			 background-image: url("/Js/shadowbox/close.png");
			 background-repeat: no-repeat;
			 cursor: pointer;
			 float: right;
			 height: 16px;
			 width: 16px;
			 position:absolute;
			 top: 0px;
			 right:5px;
			  text-indent: -9999;
		  }
	</style>
	<form id="myForm" runat="server" enctype="multipart/form-data"> 
		<%=_FullScreenLink%>
		<noscript></noscript>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper" style="padding:0 !important;">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>


           <h2 class="photogallery" ><%=_title %></h2>
            <div class="container">
			
			<div class="content">
					<% If Not (inPage) Then%><a class="XClose" href="javascript:void(0);" onclick="testSHB()"><img src="/Js/shadowbox/close.png" /></a><% End If%>
				<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
                                <%=immaginiSlider %>									
								</ul>
							</div>
							<%--
                            <div><a style="color: #5E96A6; text-decoration:none;" href="<%=String.Format("/HP3Image/PhotoGallery/{0}/gallery.zip", idPhotogallery)%>" title="Download"><%=Me.BusinessDictionaryManager.Read("DMP_SaveGallery", _langKey.Id, "DMP_SaveGallery")%></a></div>
							--%>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->
			
			</div><!-- content -->
		</div><!-- container -->

     </form>    

        <script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
    	<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--%>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.elastislide.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/gallery.js"></script>   
	   <script type="text/javascript">
			function testSHB() {           
				parent.Shadowbox.close();
			}
		   
			function openGallery(){
			    window.open("/ProjectDompe/ImagesPreview.aspx?galleryKey=<%=Request("c")%>&sa=<%=Request("sa")%>&ct=<%=Request("ct")%>&u=<%=Request("u")%>&s=<%=Request("s")%>&lk=<%=Request("lk")%>&cS=<%=Request("cS")%>", '', '', '');
			}
	   </script>
   </body>
</html>