﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<%@ Import Namespace="System.IO"%>

<script language="vb" runat="server">     
    
    Protected _siteFolder As String = String.Empty
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected classbody As String = ""
    Protected _keySiteArea As SiteAreaIdentificator = Nothing
    Protected _keysiteAreaSite As SiteAreaIdentificator = Nothing
    Protected _keycurrTheme As ThemeIdentificator = Nothing
    Protected _keylang As LanguageIdentificator = Nothing
    Private _strDefaultDefDomain As String = String.Empty
    Private _domain As String = String.Empty
    Private _codeLang As String = String.Empty
    Protected _PKContTheme As Integer = 0
    Protected _SiteUrlAnalitics As String = ""
	Protected _loggedInUserId as Integer = 0
    Protected _userTypeId As Integer = 0
    Protected _reservedAreaName As String = "PUBBLICA"
    Protected _tabletCss As String = String.Empty
    Dim mPathRedir As String = ""
    Protected _socialCover As String = "/ProjectDompe/_slice/social3.jpg"
	
    'Check sui browsers---------------------
    Protected _browserVersion As String = String.Empty
    Protected _browserAgentList As String = ",ie8,ie7,ie6,ie5,firefox4,firefox5,"
    Protected _isAnOldBrowser As Integer = 0
    '---------------------------------------
	
    Protected Enum _userType
        Press = 1
        HCP = 2
        Partner = 3
        Sede = 4
    End Enum
	
    Sub checkCookiePopUpAcceptance()
        Dim _oldBrowser As HttpCookie = HttpContext.Current.Request.Cookies("_pc_jb")
		  
        If _oldBrowser Is Nothing Then
            lkbUpdateBrowser.Attributes.Add("style", "color:red;")
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, DateTime.Now.Ticks.ToString, "$(document).ready(function() {javascript: showOldBrowserWarning()});", True)
        Else
            Dim now As DateTime = DateTime.Now
            Dim span As TimeSpan = now.AddYears(1) - now
            _oldBrowser.Expires = DateTime.Now.Add(span)
            HttpContext.Current.Response.Cookies.Add(_oldBrowser)
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _browserVersion = Request.Browser.Type.ToString()
        If _browserAgentList.indexOf("," & _browserVersion.ToLower() & ",") > -1 Then checkCookiePopUpAcceptance()
        If Not String.IsNullOrempty(Request("testOldBrowser")) AndAlso Request("testOldBrowser") = 1 Then checkCookiePopUpAcceptance()
		
        Dim cookie1 As HttpCookie = HttpContext.Current.Request.Cookies("HP3_USER")
      
        _siteFolder = Me.ObjectSiteFolder()
        _keyContentType = Me.PageObjectGetContentType
        _keySiteArea = Me.PageObjectGetSiteArea
        _keysiteAreaSite = Me.ObjectSiteDomain.KeySiteArea
        _keylang = Me.BusinessMasterPageManager.GetLanguageFromUrl()
        _keycurrTheme = Me.PageObjectGetTheme
        _domain = Me.ObjectSiteDomain.Domain
        _codeLang = Me.BusinessMasterPageManager.GetLanguage.Code
       
        If Request("showtrace") = "true" Then Trace.IsEnabled = True
        
        If (IsMobileDevice() And Not IsTablet()) Or Request("testRedir") = "test" Then
            _tabletCss = String.Format("<link href='/{0}/_css/tablet.css' rel='stylesheet' type='text/css' />", _siteFolder)
            
            Dim cntRedir As Integer = 0
            cntRedir = hasMobileContent()
        
            If (cntRedir = 0) Then
                'se non è incluso nella lista
                mPathRedir = "http://m.dompe.com" & Request.Path.ToString() & IIf(Not (String.IsNullOrEmpty(Request.QueryString.ToString())), "?" & Request.QueryString.ToString(), "")  'Request.ServerVariables("QUERY_STRING")
            Else
                'se è incluso nella lista dei contenuti che sul mobile hano una versione ad hoc
                'getlink del nuovo contenuto sul sito mobile 
                Dim mpM As New Healthware.HP3.Core.Web.MasterPageManager
                Dim domain As String = Request.Url.GetLeftPart(UriPartial.Authority)
                Dim newUrl As String
                newUrl = mpM.GetLink(New ContentIdentificator(cntRedir, _keylang.Id), False)
            
                If Not String.IsNullOrEmpty(newUrl) Then
                    mPathRedir = (newUrl).Replace(domain, "http://m.dompe.com") & IIf(Not (String.IsNullOrEmpty(Request.QueryString.ToString())), "?" & Request.QueryString.ToString(), "")
                End If
            End If
            If Not String.IsNullOrEmpty(mPathRedir) Then
                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ javascript:MobileLinkPopup('" & mPathRedir & "');})", True)
                Response.Redirect(mPathRedir, False)
            End If
        End If
        
    End Sub
    
    Public Function IsTablet() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        Dim r As Regex = New Regex("ipad|xoom|sch-i800|playbook|tablet|kindle|nexus")
        Dim isTab As Boolean = r.IsMatch(userAgent)
        Return isTab
        
    End Function
   
    Public Function IsMobileDevice() As Boolean
        
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
            
        Return False
        
    End Function
    
    Public Property PKThemeContent() As Integer
        Get
            Return _PKContTheme
        End Get
        Set(ByVal value As Integer)
            _PKContTheme = value
        End Set
    End Property
   
    
    Function file_exists(ByVal percorso As String) As Boolean
        If File.Exists(HttpContext.Current.Server.MapPath(percorso)) Then
            Return True
        Else
            Return False
        End If
    End Function
	
    

    Function getCustomVar(ByVal _intThemeId As Integer) As String
        Dim ht As New Hashtable
        ht.Add(296, "PARTNER")
        ht.Add(297, "PARTNER")
        ht.Add(298, "PARTNER")
        ht.Add(299, "PARTNER")
        
        ht.Add(256, "HCP")
        ht.Add(257, "HCP")
        ht.Add(258, "HCP")
        ht.Add(259, "HCP")
        ht.Add(260, "HCP")
        ht.Add(262, "HCP")
        
        ht.Add(240, "PRESS")
        ht.Add(241, "PRESS")
        ht.Add(242, "PRESS")
        ht.Add(243, "PRESS")
        ht.Add(295, "PRESS")
        
        If ht.Contains(_intThemeId) Then Return ht(_intThemeId)
        Return "PUBBLICA"
    End Function
    
    
    Function hasMobileContent() As Integer
        
        'Response.Write("ArchivioCtype:" & Dompe.ContentTypeConstants.Id.ArchivioCtype & " ctype:" & Me.PageObjectGetContentType.Id)
        Dim mobileCntID As Integer = 0
        Dim _cacheKey = "_mobileContents"
        If Request("cache") = "false" Then CacheManager.RemoveByKey(_cacheKey)
        
        Dim hsMobileCnt As Hashtable
        hsMobileCnt = CacheManager.Read(_cacheKey, 1)
        
        If Not (hsMobileCnt Is Nothing) AndAlso (hsMobileCnt.Count > 0) Then
            If hsMobileCnt.Contains(Me.PageObjectGetContent.Id) Then
                mobileCntID = hsMobileCnt(Me.PageObjectGetContent.Id)
            End If
        Else
            hsMobileCnt = New Hashtable
            hsMobileCnt.Add(515, 544) 'La sede di Tirana
            hsMobileCnt.Add(139, 543) 'La sede di New York
            hsMobileCnt.Add(55, 542)  'Il Centro Ricerche di Napoli
            hsMobileCnt.Add(54, 541)  'Il Polo dell’Aquila            
            hsMobileCnt.Add(53, 540) 'La sede di Milano
            hsMobileCnt.Add(465, 549) 'Drug discovery
            hsMobileCnt.Add(44, 548) 'Network R&D
            hsMobileCnt.Add(46, 547) 'Pipeline R&D
            hsMobileCnt.Add(74, 539) 'Processo selezione e candidature           
            
            CacheManager.Insert(_cacheKey, hsMobileCnt, 1, 60)
            
            If hsMobileCnt.Contains(Me.PageObjectGetContent.Id) Then
                mobileCntID = hsMobileCnt(Me.PageObjectGetContent.Id)
            End If
        End If

        Return mobileCntID
    End Function
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        _SiteUrlAnalitics = Request.Url.Host
		
        _reservedAreaName = getCustomVar(Me.PageObjectGetTheme.Id)
               
        If Not Me.ObjectTicket Is Nothing AndAlso Not String.IsNullOrEmpty(ObjectTicket.Roles.ToString) AndAlso Me.ObjectTicket.User.Key.Id > 0 Then
            _loggedInUserId = Me.ObjectTicket.User.Key.Id
			
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleDoctor) Then _userTypeId = _userType.HCP 'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('User','" & utente & "-- doctor')});", True)
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleMediaPress) Then _userTypeId = _userType.Press 'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('User','" & utente & "-- media press')});", True)
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RolePartner) Then _userTypeId = _userType.Partner
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleSede) Then _userTypeId = _userType.Sede
        End If
        
        If ChechLang(_keylang, _keysiteAreaSite) Then
            Dim codeLanguage As String = Me.BusinessMasterPageManager.GetUrlParam(Healthware.HP3.Core.Web.MasterPageManager.ParamEnumerator.Language).Name.ToLower
            If codeLanguage.ToLower <> _codeLang.ToLower Then
                Me.BusinessAccessService.SetCurrentLanguageByCode(codeLanguage)
                Response.Redirect(Request.Url.ToString, False)
            End If
        End If
             
        Dim ctrlPath As String = String.Empty
                 
        GetPageTitle()
        GetMetaTag()
       
        If file_exists("/" & _siteFolder & "/_css/imgPage/" & Me.BusinessMasterPageManager.GetLanguage.Code & "/" & Me.BusinessMasterPageManager().GetSiteArea.Id & ".css") = True Then
            MySiteareaCss.Text = "<link type=""text/css"" rel=""stylesheet"" href=""/" & _siteFolder & "/_css/imgPage/" & Me.BusinessMasterPageManager.GetLanguage.Code & "/" & Me.BusinessMasterPageManager().GetSiteArea.Id & ".css"" media=""screen"" />"
        Else
            MySiteareaCss.Text = ""
            MySiteareaCss.Visible = False
        End If
        
        
        'ctrlPath = String.Format("/{0}/HP3Common/header.ascx", Me._siteFolder)
        'LoadSubControl(ctrlPath, Me.phHeader)
        
        'ctrlPath = String.Format("/{0}/HP3Common/footer.ascx", Me._siteFolder)
        'LoadSubControl(ctrlPath, Me.phFooter)
        
        '*******************************************************************
        'VERIFICA RUOLO UTENTE / SEZIONE
        
        Dim oRoleColl As New RoleCollection
        Dim accessoSezione As Boolean = False
        oRoleColl = Me.BusinessSiteAreaManager.ReadRoles(Me.PageObjectGetSiteArea)
             
        
        If oRoleColl Is Nothing OrElse oRoleColl.Count = 0 Then
            accessoSezione = True
        Else
            Dim id_ruolo_utente As Array
            Dim id_r As Integer
            If Not (oRoleColl Is Nothing) Then
                id_ruolo_utente = Split(Me.ObjectTicket.Roles.ToString, ",")
                If id_ruolo_utente.Length() - 1 > 0 Then
                    For id_r = 0 To id_ruolo_utente.Length() - 1
                        If (oRoleColl.HasRole(CLng(id_ruolo_utente(id_r).ToString))) Then accessoSezione = True
                    Next
                Else
                    If Not Me.ObjectTicket Is Nothing AndAlso Not String.IsNullOrEmpty(ObjectTicket.Roles.ToString) Then
                        If (oRoleColl.HasRole(CLng(Me.ObjectTicket.Roles.ToString))) Then accessoSezione = True
                    End If
                End If
            End If
        End If
 
        If accessoSezione = True Then
            ctrlPath = Me.BusinessContentTypeManager().LoadPathControl(Me._siteFolder, _keyContentType)
            If Not String.IsNullOrEmpty(ctrlPath) Then
                LoadSubControl(ctrlPath, Me.phHome)
            End If
        Else
            If _reservedAreaName = "HCP" And Request.Url.ToString.ToLower.IndexOf("/richiedi-l-articolo") > -1 Then
                'LoadSubControl("~/ProjectDOMPE/HP3Service/RichiestaArticolo.ascx", Me.phHome)
                LoadSubControl("~/ProjectDOMPE/HP3Service/Medical_HomeRichiedi_Articolo.ascx", Me.phHome)
				
            ElseIf _reservedAreaName = "HCP" And Request.Url.ToString.IndexOf("/area-medico-welcome-page") = -1 Then
                Response.Redirect("/area-medico-welcome-page/", False)
				
            ElseIf _reservedAreaName = "HCP" And Request.Url.ToString.IndexOf("/area-medico-welcome-page") > -1 Then
                LoadSubControl("~/ProjectDOMPE/HP3Service/MedicalHome.ascx", Me.phHome)
				
            ElseIf _reservedAreaName = "PRESS" Then
                If _keylang.Id = 1 Then Response.Redirect("/giornalista/", False)
                If _keylang.Id = 2 Then Response.Redirect("/journalist/", False)
                If _keylang.Id = 3 Then Response.Redirect("/gazetar/", False)
            Else
                Response.Redirect("/", False)
            End If
        End If
        '*******************************************************************  
            
        If Not Me.IsPostBack Then
            'tracciamento
            Me.BusinessMasterPageManager.Trace()
            Me.BusinessMasterPageManager.TraceExtend()
            
            '**********************************************************************************
            ' CONTROLLO DIMENSIONE TESTO
            '***********************************************************************************
            Dim useCache As Boolean = False
            useCache = CacheManager.Exists("BodySize")
            If useCache Then
                Dim classB As String = CacheManager.Read("BodySize", 1).ToString
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ SizeBody('" & classB & "');})", True)
            End If
            '************ END Controllo dimensione *********************************************
        End If
        
        'Testo Linkbutton oldBrowsers---------
        If _keylang.Id = 2 Then lkbUpdateBrowser.Text = "Continue visiting the website &raquo;"
        If _keylang.Id = 3 Then lkbUpdateBrowser.Text = "Vazhdo lundrimin në website &raquo;"
        '-------------------------------------
    End Sub
    
    Function GetLink(ByVal currentTheme As ThemeIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(currentTheme, Nothing, False)
    End Function

    Function ChechLang(ByVal keyLang As LanguageIdentificator, ByVal keysiteareaSite As SiteAreaIdentificator) As Boolean
        If keyLang Is Nothing Then Return False
        
        Dim langColl As LanguageCollection = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(keysiteareaSite)
       
        If Not langColl Is Nothing AndAlso langColl.Count > 0 Then
            For Each lang As LanguageValue In langColl
            
                If lang.Key.Code = keyLang.Code Then
                    Return True
                
                End If
            Next
        End If
        
        Return False
    End Function

    Sub GetPageTitle()
        Dim pageTitle As String = Me.BusinessMasterPageManager.GetPageTitle()
        ltlPageTitle.Text = "Dompé Corporate"
        If Not String.IsNullOrEmpty(pageTitle) Then ltlPageTitle.Text += " - " & pageTitle
    End Sub
    
    'loads the page meta tag, based on the page content or theme
    Sub GetMetaTag()
        Dim metaKey As String = String.Empty
        Dim metaDesc As String = String.Empty
        Dim propert As String = ""
       
             
        metaKey = Me.BusinessMasterPageManager.GetMetaKeywords
        metaDesc = Me.BusinessMasterPageManager.GetMetaDescription()
       
        Dim strImg As String = Me.PageObjectGetContent.Id
        
        'If Me.PageObjectGetTheme.Id = Dompe Then
        '    ltlMetaProperty.Text = "/ProjectDOMPE/_slice/CoverFB/" & strImg & ".jpg"
        'Else
        'ltlMetaProperty.Text = propert
        'End If
                      
        If metaKey <> String.Empty Then
            ltlMetaKeywords.Text = Server.HtmlDecode(metaKey)
        Else
            If _keylang.Id = 1 Then ltlMetaKeywords.Text = Server.HtmlDecode("dompé,nathalie,sergio,aringhieri,ricerca e sviluppo,diabetologia,oftalmologia,trapianti d'organo,innovazione,farmaci orfani,responsabilità sociale,l'aquila,soluzione terapeutiche,apparato respiratorio,patologie osteoarticolari,malattie rare,automedicazione,tosse secca,cefalea,mal di gola,tosse grassa,Ipertensione arteriosa polmonare,BPCO,Broncopneumopatia Cronica Ostruttiva,Asma,Rinite")
            If _keylang.Id = 2 Then ltlMetaKeywords.Text = Server.HtmlDecode("dompé,nathalie,sergio,aringhieri,research and development,diabetology,ophtalmology,organ transplantation,innovation,orphan drugs,social responsability,l'aquila,Therapeutic solutions,Respiratory Apparatus,Osteoarticular Diseases,Rare Diseases,Self-medication,dry cough,headache,sore throat,cough,Hipertension pulmonary arterial,COPD, Chronic obstructive pulmonary disease,Asthma,Rhinitis")
            If _keylang.Id = 3 Then ltlMetaKeywords.Text = Server.HtmlDecode("dompé,nathalie,sergio,aringhieri,kërkim dhe zhvillim,diabetologji,oftalmologji,transplant organesh,inovacion,barna jetime,përgjegjësi sociale,l'aquila,zgjidhje")
        End If
        
        If metaDesc <> String.Empty Then
            ltlMetaDescription.Text = Server.HtmlDecode(metaDesc)
            ltlfbDescription.Text = Server.HtmlDecode(metaDesc)
            ltlTwDescription.Text = Server.HtmlDecode(metaDesc)
        Else
            
            If _keylang.Id = 1 Then
                ltlMetaDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."
                ltlfbDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."
                ltlTwDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."

            ElseIf _keylang.Id = 2 Then
                ltlMetaDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."
                ltlfbDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."
                ltlTwDescription.Text = "Dompé è una delle principali aziende biofarmaceutiche in Italia, focalizzata sullo sviluppo di soluzioni terapeutiche innovative per malattie rare, spesso orfane di cura."
                
            ElseIf _keylang.Id = 3 Then
                ltlMetaDescription.Text = "Dompé-ja është një prej ndërmarrjeve kryesore biofarmaceutike në Itali, e përqendruar në zhvillimin e zgjidhjeve terapeutike inovative për sëmundjet e rralla, "
                ltlfbDescription.Text = "Dompé-ja është një prej ndërmarrjeve kryesore biofarmaceutike në Itali, e përqendruar në zhvillimin e zgjidhjeve terapeutike inovative për sëmundjet e rralla, "
                ltlTwDescription.Text = "Dompé-ja është një prej ndërmarrjeve kryesore biofarmaceutike në Itali, e përqendruar në zhvillimin e zgjidhjeve terapeutike inovative për sëmundjet e rralla, "
            End If
        End If
		
        ltlfbTitle.Text = "Dompé Corporate"
        ltlTwTitle.Text = "Dompé Corporate"
        If Me.BusinessMasterPageManager.GetContent.Id > 0 Then
            Dim ocVal As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(Me.BusinessMasterPageManager.GetContent.Id, _keylang.Id))
            If Not ocVal Is Nothing AndAlso ocVal.Key.Id > 0 Then
                ltlfbTitle.Text = ocVal.Title.Replace("'", "‘")
                ltlTwTitle.Text = ocVal.Title.Replace("'", "‘")
                If file_exists("/HP3Image/cover/" & ocVal.Key.Id.ToString() & "-2_gen.jpg") Then _socialCover = "/HP3Image/cover/" & ocVal.Key.Id.ToString() & "-2_gen.jpg"
            End If
        End If
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub

    Sub notifyupdaterowser(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim _polCookie As New HttpCookie("_pc_jb")
        Dim now As DateTime = DateTime.Now
        Dim span As TimeSpan = now.AddYears(1) - now
        _polCookie.Expires = DateTime.Now.Add(span)
        HttpContext.Current.Response.Cookies.Add(_polCookie)
        Response.Redirect(Request.Url.ToString, False)
    End Sub
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id, label)
    End Function
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><asp:Literal ID="ltlPageTitle" runat="server"></asp:Literal></title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	
	<meta name="google-site-verification" content="CEzA94GOZ0GvpcOSzEbi1tfNIqkKGBrsJ43H4xzWfAI" />
    <%--<meta name="google-site-verification" content="LT0kjCXld7kByc_qajTa2bc7nnRmCddgUwOwNE4zVFs" />--%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
    <meta name="metadescription" content='<asp:literal ID="ltlMetaDescription" EnableViewState="false" runat="server" />' />
    <meta name="metakeywords" content='<asp:literal ID="ltlMetaKeywords" EnableViewState="false" runat="server" />' /> 
	
	
	<!-- Twitter Card data -->
	<meta name="twitter:url" content="<%=Request.Url.ToString()%>" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@publisher_handle" />
	<meta name="twitter:title" content='<asp:literal ID="ltlTwTitle" EnableViewState="false" runat="server" />' />
	<meta name="twitter:description" content='<asp:literal ID="ltlTwDescription" EnableViewState="false" runat="server" />' />
	<meta name="twitter:creator" content="@bbicio" />
	<meta name="twitter:image" content="http://www.dompe.com<%=_socialCover%>" />

	<!-- Open Graph data -->
	<meta property="og:title" content='<asp:literal ID="ltlfbTitle" EnableViewState="false" runat="server" />' />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<%=Request.Url.ToString()%>" />
	<meta property="og:image" content="http://www.dompe.com<%=_socialCover%>" />
	<meta property="og:description" content='<asp:literal ID="ltlfbDescription" EnableViewState="false" runat="server" />' />  
	<meta property="og:site_name" content="Dompe Corporate" />	
	
	
	<link rel="stylesheet" title="Standard" type="text/css" media="print" href="/<%=_siteFolder %>/_css/print.css.axd" /> 
	<link rel="stylesheet" title="Standard" type="text/css" media="screen" href="/Js/shadowbox/shadowbox.css" />
    
    <%--
	<link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico" />
	<link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico" />
    --%>

	<link rel="icon" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico" sizes="16x16 24x24 32x32 48x48 64x64 128x128" type="image/vnd.microsoft.icon" />
	<link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico"/> 
	<link rel="apple-touch-icon-precomposed" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico"/>  
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico"/>  
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/<%=_siteFolder %>/_slice/favicon/Dompe.ico"/>
	<meta name="msapplication-TileImage" content="/<%=_siteFolder %>/_slice/favicon/Dompe.ico" />
	<meta name="msapplication-TileColor" content="#FFFFFF" />


	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/ui/jquery-ui.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/json2.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jHp3LabelInside.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/superfish.js.axd"></script>
	<link rel="stylesheet" href="/<%=_siteFolder %>/_js/css/jquery-ui.css" />
	<link href="/<%=_siteFolder %>/_css/it/font.css.axd" rel="stylesheet" type="text/css" />
	
    <link href="/<%=_siteFolder %>/_css/style.css" rel="stylesheet" type="text/css" />
	<link href="/<%=_siteFolder %>/_css/it/style.css.axd" rel="stylesheet" type="text/css" />
    <link href="/<%=_siteFolder %>/_css/custom.css" rel="stylesheet" type="text/css" />
    <%=_tabletCss%>

	<asp:Literal ID="MySiteareaCss" runat="server"></asp:Literal> 
	<script language="JavaScript" type="text/JavaScript" src="/Js/shadowbox/shadowbox.js.axd"></script>
    <script language="JavaScript" type="text/JavaScript" src="/Js/global.js"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/jquery.easing.1.3.js", _siteFolder)%>"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/jquery.jkit.1.2.16.min.js", _siteFolder)%>"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/superfish.min.js", _siteFolder)%>"></script>

	 <script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-48312377-3']);
		_gaq.push(['_setDomainName', '<%= _SiteUrlAnalitics %>']);
		_gaq.push(['_setCustomVar', 1, 'UserType', '<%=_userTypeId%>', 3]);
		_gaq.push(['_setCustomVar', 2, 'UserId', '<%=_loggedInUserId%>', 1]);
		_gaq.push(['_setCustomVar', 3, 'ReservedArea', '<%=_reservedAreaName%>', 3]);
		_gaq.push(['_trackPageview', location.pathname + location.search + location.hash]);
	</script>

	<style type="text/css">
		.oldB {font-weight:bold;text-decoration:none;}
		.oldB:hover {text-decoration:none;}
		.oldB:selected {text-decoration:none;}

        #warning-popup {
            background-color: #ccc;
            bottom: 0;
            color: #000;
            font-size: 13px;
            font-weight: bold;
            left: 0;
            opacity: 0.9;
            padding: 10px;
            position: fixed;
            text-align: center;
            width: 100%;
            z-index: 100;
        }

        .scrollup {
            background: url("/ProjectDompe/_slice/icon_gototop.png") no-repeat scroll 0 0 transparent;
            bottom: 20px;
            display: none;
            height: 40px;
            position: fixed;
            right: 20px;
            text-indent: -9999px;
            width: 40px;
        }
    </style>    
</head>
<body id="BodyMS">
	<form id="myForm" runat="server" enctype="multipart/form-data">
        <!--mPathRedirrr <%=mPathRedir%> -->
        <a class="scrollup" href="#">Scroll</a>
		<asp:PlaceHolder ID="plhLogout" runat="server" EnableViewState="false"></asp:PlaceHolder>                   
		<asp:PlaceHolder ID="phHeader"  runat="server" EnableViewState="false" />



<div class="header">
    <div class="container">
        <asp:PlaceHolder ID="phlLogin" runat="server"></asp:PlaceHolder>
        <div class="headerBg">

            <div class="headerCenter" style="padding-top:70px !important;">
                <h1><a href="http://www.dompe.com" title="www.dompe.com">www.dompe.com</a></h1>                
            </div>            
        </div>
    </div>
</div>
<div class="header2">
    <div class="container">
        <div class="headerCenter2"><a href="http://www.dompe.com" title="www.dompe.com">www.dompe.com</a></div>
        <div class="headerBottom" style="display: none;">
            <asp:Literal ID="ltlMenu" runat="server"></asp:Literal>
        </div>
    </div>
</div>





		<asp:PlaceHolder ID="phHome"  runat="server" Visible="true"/>
		<asp:PlaceHolder ID="phFooter"  runat="server"  EnableViewState="false"/>

		<!-- Theme: <%=_keycurrTheme.Id%> ---- Content Type <%= _keyContentType.Id%>  ----   Sitearea:  <%= _keySiteArea.Id%>     ----   Thema:  <%= _keycurrTheme.Id%> ---- Lang: <%= _keylang.Id%> -->
		<div id="oldBrowserPopUp" style="display:none;"  title="&nbsp;">
			<div class="oldBrowserPopUpCont">	
				<img alt="Dompé Corporate" width="120px" src="/ProjectDOMPE/_slice/logoWhite.gif" />
				<p style="text-align:left; margin-bottom:0; margin-top:20px;font-size:14px;font-family: 'Museo300',Arial,Helvetica,sans-serif;line-height:20px;">
					<%= Me.BusinessDictionaryManager.Read("OldBrowser1", Me.PageObjectGetLang.Id, "OldBrowser1") %>
				</p>
				<p style="font-size:14px;font-family: 'Museo300',Arial,Helvetica,sans-serif;line-height:20px;">
					<%= Me.BusinessDictionaryManager.Read("OldBrowser2", Me.PageObjectGetLang.Id, "OldBrowser2") %>
				</p>
				<div style="text-align:right;font-size:16px;font-family: 'Museo300',Arial,Helvetica,sans-serif;"><strong><asp:linkbutton text="Continua comunque la visita al sito web &raquo;" id="lkbUpdateBrowser" runat="server" onclick="notifyupdaterowser" /></strong></div>
				<p style="font-size:9px;font-family: 'Museo300',Arial,Helvetica,sans-serif;">
					<%= Me.BusinessDictionaryManager.Read("OldBrowser3", Me.PageObjectGetLang.Id, "OldBrowser3") %>
				</p>
			</div>
		</div>	
	</form>
    <div id="dialog-message" style="display:none;" title="<%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_TitleBoxLinkExternal") %>">
        <div class="popupCont">	
            <img alt="Dompé Corporate" width="120px" src="/ProjectDOMPE/_slice/logoWhite.gif" />
            <p style="text-align:center; margin-bottom:0; margin-top:0;">
                <%= Me.BusinessDictionaryManager.Read("PLY_MessageBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_MessageBoxLinkExternal")%>
            </p>
        </div>
    </div>

    <div id="dialog-message-m" style="display:none;" title="<%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_TitleBoxLinkExternal") %>">
        <div class="popupCont">	
            <img alt="Dompé Corporate" width="120px" src="/ProjectDOMPE/_slice/logoWhite.gif" />
            <p style="text-align:center; margin-bottom:0; margin-top:0;">
                <%= Me.BusinessDictionaryManager.Read("PLY_MessageBoxMobile", Me.PageObjectGetLang.Id, "Vuoi essere reindirizzato alla versione mobile?")%>
            </p>
        </div>
    </div>


<!--POP UP COOKIES-->
<div id="warning-popup" style="display:none;">
    <p><%=GetLabel("cookiesdisclaimerpop")%></p>
    <p></p>
    <span><a style="color:#e20025;" id="accept-cookie" href="#">Ok</a></span>
</div>
<script src="/<%=_siteFolder %>/_js/jquery_cookie.js" type="text/javascript"></script>


<script type="text/javascript">
	$(document).ready(function(){
	    var langid= <%=_keylang.Id%>;

	    if (jQuery.cookie('cookie-compliance-user-response') ) {
	        $('#warning-popup').hide();
	    }else{$('#warning-popup').show();}

		$('#accept-cookie').bind('click', function(e){
			e.preventDefault();
			$.cookie('cookie-compliance-user-response', '1',{path:'/'});
			$('#warning-popup').hide();
		});

		jQuery(window).scroll(function(){
		    if (jQuery(this).scrollTop() > 100) {
		        jQuery('.scrollup').fadeIn();
		    } else {
		        jQuery('.scrollup').fadeOut();
		    }
		});

        jQuery('.scrollup').click(function(){
            jQuery("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
	});
</script>


<!--END COOKIES-->

<script type="text/javascript" language="javascript">

	<%' If Not Me.objectSiteDomain.IsTest Then %>

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	
	<%' End If %>
	
    var _currDomain = '<%=String.Format("https://{0}", Me.ObjectSiteDomain.Domain.ToString)%>';
    var _linkToEsclude = '<%=Healthware.DMP.Configuration.AppSettingsData.InternalUrls%>';

    $(document).ready(function () {

        InitEvents();

        bindExternalLinks(_currDomain, _linkToEsclude);
    });

	function showOldBrowserWarning() {
		$(function () {
			$("#oldBrowserPopUp").dialog({
				height:430,
				width:550,
				modal: true,
				autoOpen: true,
				draggable:false,
				resizable: false,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
			});
		});
	}	

    function InitEvents()
    {
        $('ul.sf-menu').superfish({
            pathClass: 'current'
        });
		$(".headerBottom").show();
        $('body').jKit();
    }

    $(document).scroll(function () {
        if ($(window).scrollTop() > 100) {
            secondHeader();
        } else {
            firstHeader();
        }
    });
    function firstHeader() {
        $('.header2').fadeOut();
        $('.header').fadeIn();
    }
    function secondHeader() {
        $('.header').fadeOut();
        $('.header2').fadeIn();
    }
</script>
</body>
</html>