﻿/* EFPIA shared functions */


/*Inizializzazione TABS+*/
function initializeTabs() {
	$("#tabs").tabs();
};

//INIZIO BOTTONI
function initializeBtns(_selBtn) {   
	$( ".widget input[type=submit], .widget a, .widget button" ).button();
	$( "button, input, a" ).click( function( event ) {
	  //event.preventDefault();
	} );
	$('#btn' + _selBtn).addClass('selButton');
};





//PREPEND Tabella
function getPrependHtmlHcp(_intFoglio){
	return "<tr style='background-color: #E6E3DF;' class='headerData odd'><td colspan='14' data-sorter='false' style='text-align: center;padding:10px 0;'><strong>INDIVIDUAL NAMED DISCLOSURE</strong> - one line per HCP (i.e. all transfers of value during a year for an individual HCP will be summed up: itemization should be available for the individual Recipient or public authorities' consultation only, as appropriate)<br /><br /><span style='color:red;'><strong>DATI SU BASE INDIVIDUALE</strong> - una riga per ciascun operatore sanitario (ossia sarà indicato l'importo complessivo di tutti i trasferimenti di valore effettuati nell\'arco dell\'anno a favore di ciascun operatore sanitario: il dettaglio sarà reso disponibile solo per il singolo Destinatario o per le Autorità competenti, su richiesta)</span></td></tr>";
};

function getPrependHtmlHco(_intFoglio){
	return "<tr style='background-color: #E6E3DF;' class='headerData odd'><td colspan='14' data-sorter='false' style='text-align: center;padding:10px 0;'><strong>INDIVIDUAL NAMED DISCLOSURE</strong> - one line per HCO (i.e. all transfers of value during a year for an individual HCO will be summed up: itemization should be available for the individual Recipient or public authorities' consultation only, as appropriate)<br /><br /><span style='color:red;'><strong>DATI SU BASE INDIVIDUALE</strong> - una riga per organizzazione sanitaria (ossia saranno indicati in aggregato tutti i trasferimenti di valore effettuati nell'arco dell'anno a favore di ciascuna organizzazione sanitaria: il dettaglio sarà reso disponibile solo per la  singola Organizzazione sanitaria o per le autorità competenti)</span></td></tr>";
};


//PREPEND Tabella


//APPEND Tabella-----------------------------

//FOGLIO 1
function getAppendHtmlHcp(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>54.106,83</td><td>159.567,62</td><td>26.600</td><td>&nbsp;</td><td>&nbsp;</td><td>240.274,45</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>89</td><td>188</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>218</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari  –  Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>38,36</td><td>40,96</td><td>29,27</td><td>0</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
};


function getAppendHtmlHco(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari  ?  Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};


//FOGLIO 2
function getAppendHtmlHcpf2(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof2(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 2

//FOGLIO 3
function getAppendHtmlHcpf3(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof3(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};


//FOGLIO 4
function getAppendHtmlHcpf4(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof4(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 4



//FOGLIO 5
function getAppendHtmlHcpf5(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof5(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 5


//FOGLIO 6
function getAppendHtmlHcpf6(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>7.000,00</td><td>1.167,67</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>1</td><td>1</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>100</td><td>100</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof6(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 6


//FOGLIO 7
function getAppendHtmlHcpf7(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof7(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 7


//FOGLIO 8
function getAppendHtmlHcpf8(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>7.000,00</td><td>1.207,60</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>1</td><td>1</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>100</td><td>100</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof8(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 8


//FOGLIO 9
function getAppendHtmlHcpf9(_intFoglio){
	return "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  – nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata –  Punto 5.5 CD</span></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari – Punto 5.5 CD </span></em></td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
};


function getAppendHtmlHcof9(_intFoglio){
	return "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
};
//FOGLIO 9



//FINE APPEND Tabella

function setUpTableHcp(_intFoglio){
	$(".tablesorter" + _intFoglio).tablesorter({
		theme: 'blue',
		widgets: ['zebra', 'filter']
	})
}	
	
function setUpTableHcp(_intFoglio, _strPrependHtml, _strAppendHtml, _intFilterOn){
	$(".tablesorter" + _intFoglio).tablesorter({
		theme: 'blue',
		widgets: ['zebra', 'filter']
	}).bind('filterStart filterEnd', function (e, filter) {
		
		if (e.type === 'filterStart') {
			$('.tablesorter' + _intFoglio + ' tr.eofData').remove();
			$('.tablesorter' + _intFoglio + ' tr.headerData').remove();
			
		} else if (e.type === 'filterEnd') {
			//value of search input box
			if ( $('.tablesorter' + _intFoglio + ' input.tablesorter-filter[data-column="1"]').val().length == 0 ) {
				_intFilterOn = 0;
				$('.tablesorter' + _intFoglio).prepend(_strPrependHtml).append(_strAppendHtml);
			} else {
				_intFilterOn = 1;
				if ($('.tablesorter' + _intFoglio + ' tbody tr:visible[role="row"]').length >= 1) {
					$('.tablesorter' + _intFoglio).prepend(_strPrependHtml);
				} else {
					$('.tablesorter' + _intFoglio).append(_strAppendHtml);
				}
			}
		} else {
			return;
		}
	}).bind("sortStart sortEnd",function(e, t){
		if (e.type === 'sortStart') {
			$('.tablesorter' + _intFoglio + ' tr.eofData').remove();
			$('.tablesorter' + _intFoglio + ' tr.headerData').remove();
		}
		else { //sortEnd
			if (_intFilterOn == 1)  {
				if ($('.tablesorter' + _intFoglio + ' tbody tr:visible[role="row"]').length >= 1) {
					//no eof
					$('.tablesorter' + _intFoglio).prepend(_strPrependHtml);
				} else {
					//eof
					$('.tablesorter' + _intFoglio).append(_strAppendHtml);
				}
			}
			else {
				$('.tablesorter' + _intFoglio).prepend(_strPrependHtml).append(_strAppendHtml);
			}
		}
	});	
};
