﻿(function ($) {
    //***********************************
    // MEMBERS
    var _me = null;
    var _this = null;
    var _old = $.fn.jHp3LabelInside; // store original reference to the method (if any)
    var _options = {
        labelClass: 'label-default',
        attribute: 'title'
    };

    $.fn.jHp3LabelInside = function (options) {
        function _init(options) {
            _options = $.extend(_options, options);

            var id = _me.attr('id');
            if (!id || '' == id) {
                _me.attr('id', 'tmpId' + new Date().getUTCMilliseconds().toString())
            }

            _initGhost();

            return _me;
        }

        function _initGhost() {
            var parent = _me.parent();

            var ghost = $("<input type='text'></input>");
            ghost.val(_me.attr(_options.attribute))
                     .addClass(_options.labelClass)
                     .appendTo(parent)

            var id = _me.attr('id');
            ghost.focus(function () {
                ghost.hide();
                $('#' + id).show().focus();
            });
            _me.blur(function () {
                if (this.value == '') {
                    $(this).hide();
                    ghost.show();
                }
            });

            _me.hide();
            ghost.show();
        }

        _me = $(this);
        _this = this;
        return _init(options);
    }
})(jQuery);