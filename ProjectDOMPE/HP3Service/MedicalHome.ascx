﻿<%@ Control Language="VB" ClassName="ChiSiamo" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace=" Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _sedi As String = String.Empty
    Protected _langId As Integer = 0
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _text As String = String.Empty
    Protected _title As String = String.Empty
    Protected _titleClass As String = String.Empty
    Protected _titleStyle As String = String.Empty
    
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _langId = Me.PageObjectGetLang.Id
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        
    End Sub
    
    Protected Sub Page_Load()
        
        InitView()
        
    End Sub
    
    Protected Sub InitView()
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key = _themeKey
        soTheme.KeyLanguage.Id = _langId
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentExtraSearcher
            so.KeySite = _siteKey
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = 1'theme.KeyLanguage.Id
            so.key.Id = theme.KeyContent.Id
            so.CalculatePagerTotalCount = False
            so.SetMaxRow = 1
            Dim coll = Me.BusinessContentExtraManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                If Not cont Is Nothing Then
                    _text = cont.FullText
                    _title = cont.TitleContentExtra
                    _titleClass = cont.Code
                    _titleStyle = cont.TitleOriginal
                    Dim replacing = GetPlaceHolders()
                    If Not replacing Is Nothing AndAlso replacing.Any() Then
                        For Each item As KeyValuePair(Of String, String) In replacing
                            _text = _text.Replace(item.Key, item.Value)
                        Next
                    End If
                    
                End If
            End If
        End If
               
    End Sub
    
    Protected Function GetPlaceHolders() As Dictionary(Of String, String)
        
        Dim replacing As New Dictionary(Of String, String)
        replacing.Add("[BRAND]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idBrand, _langId))
        replacing.Add("[FOCUSON]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idFocuson, _langId))
        replacing.Add("[LINKUTILI]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idInfoutili, _langId))
        replacing.Add("[ARCHIVIORCP]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArchivioRCP, _langId))
        replacing.Add("[RICHIESTAARTICOLO]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArticoloMedical, _langId))
		
        Return replacing
        
    End Function
    
    
    
    Protected Function GetContentLink(idContent As Integer) As String
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key.Id = Dompe.ThemeConstants.idManagement
        soTheme.KeyLanguage.Id = _langId
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentSearcher
            Dim idDompe As Integer = idContent
            so.KeySite = Me.PageObjectGetSite
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = idDompe
            Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                Return Me.BusinessMasterPageManager.GetLink(cont.Key, theme, False)
            End If
        End If
        
        Return String.Empty
        
    End Function
    
</script>

<%--<div class="body areaMedica">
    <div class="box-container titleSection">
        <div class="boxCnt">
            <div class="container">
                <h2 class="<%=_titleClass %>" <%=_titleStyle%>><%=_title %></h2>
            </div>
        </div>
    </div>
    <%= _text%>
</div>--%>

<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/areaMedica.jpg');">            
        </div>    
         <div class="txt">
                <h1 class="<%=_titleClass %>" <%=_titleStyle%>><%=_title %></h1>          
            </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <div class="breadcrumbs">
        <ul>
        	<li><a href="#" title="Home">Home</a><span><img src="/ProjectDOMPE/_slice/arr-bc.jpg" /></span></li>
        	<li><a href="#" title="<%=_title %>"><%=_title %></a></li>
        </ul>
    </div><!--end breadcrumbs--> 
</section>
<section>
    <div class="container"><%= _text%></div>
</section>
