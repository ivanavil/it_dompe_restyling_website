﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>


<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Private _launch As String = String.Empty
    Private _isMobile As Boolean = False

	Dim videoHeight as Integer = 400
	Dim videoTitle as String = String.Empty
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
    End Sub
        
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    Dim _canCheckApproval As Boolean = False
    
    Sub Page_Load()	
	
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        
        Dim contentId As Integer = Me.PageObjectGetContent.Id

       
        _cacheKey = "cacheKey:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId
        If Request("cache") = "false" Or _canCheckApproval Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		
        Dim _isApproved As Boolean = False
        		
        Dim arrData(7) As String
        Dim _strCachedContent() As String = CacheManager.Read(_cacheKey, 1)
        If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
            arrData = _strCachedContent
            If _currentTheme.Id = Dompe.ThemeConstants.idManagement Then                        
                titolo_sezione_mng.Text = arrData(0)
            End If
            
            titolo_sezione.Text = arrData(0)
            ltlContentDetail.Text = arrData(1)
            _classSection = arrData(2)
            _inlineStyleTitle = arrData(3)
            _isApproved = arrData(4)
            videoTitle = arrData(5)
            _launch = arrData(6)
            
            'testo_lancio.Text = _launch
			
            If Not _isApproved Then Response.Redirect(Request.Url.Scheme.ToString() & "://" & Me.ObjectSiteDomain.Domain, False)
            If arrData(6).IndexOf("addthis_toolbox") > -1 Then phSocialSharing.Visible = True
        Else
            Dim oStringBuilder As StringBuilder = New StringBuilder()
            Dim serchcont As New ContentExtraSearcher
            If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
                Dim soEx As New ContentExtraSearcher()
                soEx.key.Id = contentId
                soEx.key.IdLanguage = _langKey.Id
                soEx.CalculatePagerTotalCount = False
                soEx.SetMaxRow = 1
                soEx.KeySite = Me.PageObjectGetSite
                soEx.Display = SelectOperation.All
                If Not _canCheckApproval Then soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
                Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
                If Not contentextraColl Is Nothing AndAlso contentextraColl.Any() Then
                    contentextraVal = contentextraColl.FirstOrDefault
                    
                    _classSection = contentextraVal.Code
                    _inlineStyleTitle = contentextraVal.TitleOriginal
                    _launch = contentextraVal.Launch
                    titolo_sezione.Text = contentextraVal.TitleContentExtra '& Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus)
                    oStringBuilder.Append("<div class='content'>")
                    
                    If _currentTheme.Id = Dompe.ThemeConstants.idManagement Then
                        
                        titolo_sezione_mng.Text = String.Format("<h1  class='{2} ' {3}>{0}</h1><h2>{1}</h2>", contentextraVal.TitleContentExtra, contentextraVal.Copyright, _classSection, _inlineStyleTitle)
                    Else
                        titolo_sezione.Text = String.Format("<h1 class='{1}' {2}>{0}</h1>", contentextraVal.TitleContentExtra, _classSection, _inlineStyleTitle)
                    End If
                    
                    If Not String.IsNullOrEmpty(contentextraVal.Abstract) Then oStringBuilder.Append("<h4>" & contentextraVal.Abstract & "</h4>")

                    
                    
                    _launch = contentextraVal.Launch
                    
                    Dim strSocialShare As String = getSocialShare()
                    If Not String.IsNullOrEmpty(strSocialShare) Then _launch = strSocialShare & _launch
					
                    
                    oStringBuilder.Append(contentextraVal.FullText)
                    
				  
                    If Not String.IsNullOrEmpty(contentextraVal.FullTextOriginal) Then oStringBuilder.Append("</br></br><div>" & contentextraVal.FullTextOriginal & "</div>")
                    
                    If _siteArea.Id = Dompe.SiteAreaConstants.IDSiteareaPosizioniAperte Then
                        Dim jsClick As String = "recordOutboundLink(""mailto:" & contentextraVal.LinkUrl & "?subject=" & contentextraVal.LinkLabel & """,""Mail to"",""Send"",""" & contentextraVal.Title & """);return false;"
                        oStringBuilder.Append("<div class='dwnBox'><h5>" & Me.BusinessDictionaryManager.Read("HeaderCandidatura", _langKey.Id, "HeaderCandidatura") & "</h5>" _
                            & "<a onclick='" & jsClick & "' href='mailto:" & contentextraVal.LinkUrl & "?subject=" & contentextraVal.LinkLabel & "'>" _
                            & "<span class='txt'><span class='title'>" & Me.BusinessDictionaryManager.Read("TitleCandidatura", _langKey.Id, "TitleCandidatura") & "</span>" _
                            & " </span> <span class='readMore2'>" & Me.BusinessDictionaryManager.Read("LinkMailToCandidatura", _langKey.Id, "LinkMailToCandidatura") & "</span>" _
                            & "</a></div>")
                    End If
                    oStringBuilder.Append("</div>")
					
                    If _siteArea.Name = Dompe.SiteAreaConstants.Management Then imgPath = GetImage(contentextraVal.Key, contentextraVal.Title)
                    
					
                    oStringBuilder.Append(loadLink(contentextraVal.Key.PrimaryKey))
                    oStringBuilder.Append(LoadVideo(contentextraVal.Key.PrimaryKey))
                    oStringBuilder.Append(LoadGallery(contentextraVal.Key.PrimaryKey))
                    

                    _isApproved = IIf(contentextraVal.ApprovalStatus = 1, True, False)
                    
                    If _currentTheme.Id = Dompe.ThemeConstants.idManagement Then
                        arrData(0) = titolo_sezione_mng.Text
                    Else
                        arrData(0) = titolo_sezione.Text
                    End If
                    
                    arrData(1) = oStringBuilder.ToString()
                    arrData(2) = contentextraVal.Code
                    arrData(3) = _inlineStyleTitle
                    arrData(4) = _isApproved
                    arrData(5) = contentextraVal.Title
                    arrData(6) = _launch
                    videoTitle = contentextraVal.Title
					
                    'Cache Content and send to the oputput-------
                    If contentextraVal.ApprovalStatus = 1 Then CacheManager.Insert(_cacheKey, arrData, 1, 60)
                    ltlContentDetail.Text = Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus) & oStringBuilder.ToString()
                    '--------------------------------------------
                Else
                    Response.Redirect(Request.Url.Scheme.ToString() & "://" & Me.ObjectSiteDomain.Domain, False)
                End If
            End If
        End If
        
        
        If _currentTheme.Id = Dompe.ThemeConstants.idManagement Then
            div_Menu.Visible = True
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuManagement.ascx", _siteFolder)
            Me.LoadSubControl(ctrlPath, menuManagement)
            menuManagement.Visible = True
            div_banner_mng.Visible = True
            div_banner.Visible = False
            
            
        ElseIf _currentTheme.Id = Dompe.ThemeConstants.IdStorie Then
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuStorie.ascx", _siteFolder)
            Me.LoadSubControl(ctrlPath, menuStories)
            menuStories.Visible = True
            div_stories.Visible = True
        ElseIf _currentTheme.Id = Dompe.ThemeConstants.IDProgettiIniziative Then
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuProgIniziative.ascx", _siteFolder)
            Me.LoadSubControl(ctrlPath, menuIniziative)
            menuIniziative.Visible = True
            div_iniziative.Visible = True
			
			
        ElseIf _currentTheme.Id = Dompe.ThemeConstants.idWeCare Then
            If titolo_sezione.Text.ToLower.Contains("in the woods") And Me.PageObjectGetLang.Id = 1 Then
                
                Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuWoods.ascx", _siteFolder)
                Me.LoadSubControl(ctrlPath, menuWeCare)
            Else
                
                Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuWeCare.ascx", _siteFolder)
                Me.LoadSubControl(ctrlPath, menuWeCare)
            End If
           
            menuWeCare.Visible = True
            div_weCare.Visible = True
			
        ElseIf IsInFooter(_currentTheme.Id) Then
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuCondizioni.ascx", _siteFolder)
            Me.LoadSubControl(ctrlPath, menuCondizioni)
            menuCondizioni.Visible = True
            div_condizioni.Visible = True
        Else
            
            Dim _ctrlPath As String = String.Format("/{0}/HP3Common/BoxRelatedContent.ascx", _siteFolder)
            Me.LoadSubControl(_ctrlPath, ph_RelatedContent)
            div_RelatedContent.Visible = True
            ph_RelatedContent.Visible = True
        End If
        
        
        
		
        '-------------------------
        'Additional Controls
        '-------------------------
        'plhSideRed.Visible = (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAPeople)
        'plhBoxRedDett.Visible = ((Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAPeople) Or (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSASergioDmp) Or (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAAringhieri))
        'If _siteArea.Name = Dompe.SiteAreaConstants.Management Then LoadSubControl(String.Format("/{0}/HP3Common/BoxManagement.ascx", _siteFolder), phlManagement)
        'LoadSubControl(String.Format("/{0}/HP3Common/BoxNews.ascx", _siteFolder), Phlnews)
        If Dompe.ContentBoxImage.IdContentList.Contains("{" & Me.PageObjectGetContent.Id.ToString & "}") Then LoadSubControl(String.Format("/{0}/HP3Common/BoxImage.ascx", _siteFolder), PhlBoxImage)
        LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        LoadSubControl(String.Format("/{0}/HP3Common/BoxLeggiAnche.ascx", _siteFolder), plh_LeggiAnche)
        
        'LoadSubControl(String.Format("/{0}/HP3Common/BoxLinks.ascx", _siteFolder), PhlLinks)
    End Sub
	
    Function getSocialShare() As String
        Dim octxS As New ContentContextSearcher()
        With octxS
            .KeyContent = Me.PageObjectGetContent
            .KeyContext = New ContextIdentificator(44)
        End With
        Dim octxC As ContentContextCollection = Me.BusinessContentManager.ReadContentContext(octxS)
        If Not octxC Is Nothing AndAlso octxC.Any Then
            phSocialSharing.Visible = True
            Return "<div class='addthis_toolbox addthis_default_style addthis_32x32_style'><a class='addthis_button_preferred_1'></a><a class='addthis_button_preferred_2'></a><a class='addthis_button_google_plusone_share'></a><a class='addthis_button_linkedin'></a></div>"
        End If
		
        Return String.Empty
    End Function
    
    Protected Function IsInFooter(ByVal themeId As Integer) As Boolean
        Dim so As New ThemeSearcher()
        so.KeyFather.Id = Dompe.ThemeConstants.idFooter
        so.LoadHasSon = False
        so.LoadRoles = False
        so.KeyLanguage = _langKey
        Dim coll = Me.BusinessThemeManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any() Then
            Return coll.Any(Function(th) th.Key.Id = themeId)
        End If
        Return False
    End Function
    
    'Function LoadContentAssociati(ByRef PK As Integer) As String
    '    Dim _out As String = String.Empty
		
    '    Dim so As New ContentSearcher
    '    Dim cntVal As New ContentValue
    '    Dim soRel As New ContentRelatedSearcher
    '    Dim coll As ContentCollection = Nothing
      
    '    If PK > 0 Then
    '        soRel.Content.Key.PrimaryKey = PK
    '        soRel.Content.KeyRelationType = New RelationTypeIdentificator(4)
    '        soRel.Content.RelationSide = RelationTypeSide.Right
    '    End If
        
    '    so.KeySite = Me.PageObjectGetSite
    '    so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
    '    so.SortOrder = ContentGenericComparer.SortOrder.DESC
    '    so.SortType = ContentGenericComparer.SortType.ByData
    '    so.RelatedTo = soRel
      
    '    coll = Me.BusinessContentManager.Read(so)
    '    If Not coll Is Nothing AndAlso coll.Any Then
    '        _out = "<div class='dwnBox'><h5>" & Me.BusinessDictionaryManager.Read("DMP_RelatedMaterial", _langKey.Id, "DMP_RelatedMaterial") & "</h5>"
    '        For Each oContent As ContentValue In coll
    '            _out += "<a href='/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(oContent.Key.PrimaryKey) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & Me.PageObjectGetLang.Id & "&cS=" & Me.PageObjectGetContent.PrimaryKey & "' onclick=""recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(oContent.Key.PrimaryKey) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & Me.PageObjectGetLang.Id & "&cS=" & Me.PageObjectGetContent.PrimaryKey & "','Download Dettaglio del contenuto','Download','" & oContent.Title & "','" & oContent.Key.PrimaryKey & "');return false;"">" & _
    '                "<span class='txt'><span class='title'>" & oContent.Title & "</span>" & _
    '                "<span>" & IIf(oContent.Launch.Length > 120, Left(oContent.Launch, 120) & "...", oContent.Launch) & "</span>" & _
    '                "</span><span class='readMore2'>" & Me.BusinessDictionaryManager.Read("Download", _langKey.Id, "Download") & "</span></a>"
    '        Next
    '        _out += "</div>"
		
    '        'rpdDownloadRelated.DataSource = coll
    '        'rpdDownloadRelated.DataBind()
    '    End If
		
    '    Return _out
    'End Function
	
    Function loadLink(ByRef PK As Integer) As String
        Dim _sbLink As New StringBuilder
        Dim _strLinkItem As String = "<li style='list-style-type: square;'><a href='{0}' title='{1}'>{2}</a></li>"
		
        Dim oColl As ContentCollection = Me.BusinessContentManager.ReadContentRelation(New ContentIdentificator(PK), New ContentRelationTypeIdentificator(Dompe.RelationType.LinkRelation))
        If Not oColl Is Nothing AndAlso oColl.Any Then
            For Each oCon As ContentValue In oColl
                _sbLink.AppendFormat(_strLinkItem, oCon.LinkUrl, oCon.Title, oCon.Title)
            Next
			
            Return "<ul style='margin-top:20px;'>" & _sbLink.ToString() & "</ul>"
        End If

        Return String.Empty
    End Function
    

    Function LoadGallery(ByRef PK As Integer) As String
        Dim soRel As New ContentRelatedSearcher
        Dim _out As String = String.Empty
        Dim coll As ContentCollection = Me.BusinessContentManager.ReadContentRelation(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id), New ContentRelationTypeIdentificator(Dompe.RelationType.GalleryRelation))
        If Not coll Is Nothing AndAlso coll.Any Then
            For Each oCont As ContentValue In coll
                'If oCont.SiteArea.Key.Id = 110 Then
                _out = "<iframe width='600' height='610' style='border:0px;' id='contGallery' src='/ProjectDOMPE/HP3Popup/PhotoGalleryPublic.aspx?l=" & _langKey.Code & "&inpage=1&c=" & SimpleHash.UrlEncryptRijndaelAdvanced(oCont.Key.Id) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & _langKey.Id & "&cS=" & Me.PageObjectGetContent.PrimaryKey & "' data-cont='188'></iframe>"
                'End If
            Next
        End If
        
        Return _out
    End Function
    
    Function LoadVideo(ByRef PK As Integer) As String
        Dim _out As String = String.Empty
		
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        If PK > 0 Then
            soRel.Content.Key.PrimaryKey = PK
            soRel.Content.KeyRelationType = New RelationTypeIdentificator(1)
            soRel.Content.RelationSide = RelationTypeSide.Left
        End If
        
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDVideoGallery
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
      
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Dim w As Integer = 0
            Dim h As Integer = 0
            If Not String.IsNullOrEmpty(coll(0).Copyright) Then
                w = coll(0).Copyright.Split("|")(0)
                h = coll(0).Copyright.Split("|")(1)
            End If
			
            If coll.Count > 1 Then
                videoHeight = h + 140
                If Request.UserAgent.ToString.ToLower.IndexOf("firefox") > -1 Then videoHeight = h + 200
            Else
                videoHeight = h
                If Request.UserAgent.ToString.ToLower.IndexOf("firefox") > -1 Then
                    videoHeight = h + 60
                Else
                    videoHeight = h + 10
                End If
            End If
			
            _out = "<div class='dVideo'><div style='float:left;padding-top:25px;font-weight:bold;font-size:20px;width:490px'>" & coll(0).PageTitle & "</div><a title='" & coll(0).PageTitle & "' href=""javascript:showVideo('" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(PK) & "', " & w & ", " & h & ", " & videoHeight & ", " & coll(0).Key.Id & ");""><img alt='' src='/ProjectDompe/_slice/videoIco.gif' style='float:right;'></a></div>"
            '_out = "<div class='dVideo'><a href=""javascript:showVideo('" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(PK) & "', " & w & ", " & h & ");"">" & coll(0).PageTitle & "</a></div>"
			
			
        End If
        
        Return _out
    End Function
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = idContent
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
     
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
    Function GetImageContent(ByVal IDcont As Integer, ByVal ImgWidth As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, Me.PageObjectGetLang.Id), ImgWidth)
        Return res
    End Function
    
    Function GetImageContent() As String
        Dim res As String = ""
        Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High
        res = Me.BusinessContentManager.GetCover(Me.PageObjectGetContent, 1280, 323, imgqual, 1)
        If Not String.IsNullOrEmpty(res) Then
            res = "/HP3Image/cover/" & res
        Else
            div_banner.Visible = False
        End If
        Return res
    End Function
</script>
<style>
	.ui-dialog .ui-dialog-content {
		width:95% !important;
	}

        .txt h2
        {
            text-align: center;
            color: #e0002d;
            font-family: 'CooperHewitt-Medium';
        }
    </style>


<section>
    <asp:PlaceHolder id="div_banner" runat="server" >
     <div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('<%=GetImageContent()%>');">            
        </div>    
         <div class="txt">
                <asp:Literal ID="titolo_sezione" runat="server" />         
         </div>
         <div class="layer">&nbsp;</div>
    </div><!--end slider-->
    </asp:PlaceHolder>

    <asp:PlaceHolder id="div_banner_mng" runat="server" Visible="false" >
     <div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/management.jpg');">       
             <div class="container">
                <img class="man" src="/ProjectDOMPE/_slice/<%=Me.PageObjectGetContent.Id%>_mng.png" />
             </div>     
        </div> 
        
         <div class="txt">
                <asp:Literal ID="titolo_sezione_mng" runat="server" />         
         </div>
         <div class="layer">&nbsp;</div>
    </div><!--end slider-->
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    
</section>

<section>
    <div class="container">
         <div class="colLeft">
            <asp:PlaceHolder ID="plhBoxRedDett" runat="server">
        	    <div class='<%'=IIf(_currentTheme.Id = Dompe.ThemeConstants.idNews Or _siteArea.Id = Dompe.SiteAreaConstants.IDComunicazioneInterna, "boxBlueDett bigBoxBlueDett", "boxRedDett bigBoxRedDett")%>'>
            	    <div class="listSide" id="div_Menu" runat="server" visible="false">                	   
                        <asp:PlaceHolder runat="server" ID="menuManagement" Visible="false"></asp:PlaceHolder>               
                    </div><!--end listSide-->
                    <div class="listSide" id="div_stories" runat="server" visible="false">
                        <asp:PlaceHolder runat="server" ID="menuStories" Visible="false"></asp:PlaceHolder>
                    </div>
                    <asp:Panel id="div_iniziative" runat="server" visible="false">
                        <asp:PlaceHolder runat="server" ID="menuIniziative" Visible="false"></asp:PlaceHolder>
                    </asp:Panel>

                    <div class="listSide" id="div_weCare" runat="server" visible="false">
                        <asp:PlaceHolder runat="server" ID="menuWeCare" Visible="false"></asp:PlaceHolder>
                    </div>						
					
                    <div class="listSide" id="div_condizioni" runat="server" visible="false">
                        <asp:PlaceHolder runat="server" ID="menuCondizioni" Visible="false"></asp:PlaceHolder>
                    </div>
                    <div class="listSide" id="div_RelatedContent" runat="server" visible="false">                	   
                        <asp:PlaceHolder runat="server" ID="ph_RelatedContent" Visible="false"></asp:PlaceHolder>               
                    </div>
                </div><!--end boxRedDett-->                
                </asp:PlaceHolder>
        </div><!--end colLeft-->

         <div class="colRight">
             <div class="content">
                <div class="sideL" id="cnt_boxlancio">
                    <%=_launch%>
                </div>
                
              
                <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>

				<div class="generic">		
                    <asp:literal id="ltlContentDetail" runat="server" EnableViewState="False" />
				<asp:Repeater ID="rptPhotoGallery" runat="server" EnableViewState="false"  visible="false">
				<HeaderTemplate>
					<h4 class="photoTitle" style="float:left; width:100%;"><%= Me.BusinessDictionaryManager.Read("DMP_TitleAlbumDettaglio", _langKey.Id, "DMP_TitleAlbumDettaglio")%></h4>              
				</HeaderTemplate> 
				<ItemTemplate>
					<div class="boxPhotoGallery noMargin">
						<div class="cnt">
							<a class="effectHover" title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundFull('PhotoGallery','Open Album','<%# CType(Container.DataItem, ContentValue).Title%>')"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
							<a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800"><%# GetImageContent(DataBinder.Eval(Container.DataItem, "Key.Id"), 150, DataBinder.Eval(Container.DataItem, "Title"))%></a>
							<h3><a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></h3>
							<p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>    
						</div>
					</div><!--end boxPhotoGallery-->               
				</ItemTemplate>
				<FooterTemplate></FooterTemplate>
				</asp:Repeater>
                
            	<asp:PlaceHolder ID="phlManagement" runat="server"></asp:PlaceHolder>
				<asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
				<asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
				<asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            </div>
            </div>                       
        </div><!--end colRight-->

       
    </div><!--end container-->
</section>

<asp:PlaceHolder ID="plh_LeggiAnche" runat="server"></asp:PlaceHolder>
	
	<asp:placeholder id="phSocialSharing" runat="server" visible="false">
		<script type='text/javascript'>var addthis_config = { 'data_track_addressbar': true };</script>
		<script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-533ef6f344e9cf07'></script>	
	</asp:placeholder>

	<script type="text/javascript">
		<% 'If Not _isMobile Then %>
	    //$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;
	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
	    //    } else {
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
	    //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
	    //        }
	    //    }
	    //});
	   <%' End If %>


	    function showVideo(videoPk, width, height, videoHeight, videoId) {
	        _dialog = $('<iframe id="internal1" style="overflow:hidden !important;width: 95% !important; height:' + videoHeight + 'px !important" frameBorder="0" class="externalSite" src="/ProjectDompe/HP3Popup/VideoGallery_2.aspx?l=IT&v=2&w=' + width + '&h=' + height + '&c=' + videoId + '&sa=<%=Me.PageObjectGetSiteArea.Id%>&ct=<%=Me.PageObjectGetContentType.Id%>&u=<%=SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id)%>&s=<%=SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession)%>&lk=<%=_langKey.Id%>&cS=' + videoPk + '" />').dialog({
	           title: '<%=videoTitle.Replace("'", "\'").Replace("&", "\&")%>',
	           closeOnEscape: true,
	           autoOpen: true,
	           width: width + 75,
	           height: videoHeight, //height + 140,
	           modal: true,
	           draggable: false,
	           resizable: false,
	           overlay: {
	               opacity: 70,
	               background: 'grey'
	           }
	       });
       };
	</script>