<%@ Control Language="VB" ClassName="Literature" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.DMP.BL" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>


<script runat="server">
    'TODO: Generalizzare gli errori a livello di dictionary
    ' Aggiungere anche Approval
    
    Protected _siteFolder As String = String.Empty
    Protected _emptyLiteratureArchiveTemplate As String = String.Empty
    Protected _ajaxLiteraturePage As String = String.Empty
    Protected _ajaxTagsPage As String = String.Empty
    Protected _siteArea As Integer = 0
    Protected _brand As Integer = 0
    Protected _bitMask As String = String.Empty
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _userKey As UserIdentificator = Nothing
    Protected _topKeys As String = String.Empty
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _geo As Integer = 0
    Const _MaxItem As Integer = 10
    Protected _lancioThema As String = ""
    Protected strSezione As String = ""
    Protected _cssInlineCode As String = String.Empty
    Protected _notAvaible As String = ""
    Private _isMobile As Boolean = False
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        
        _siteFolder = Me.ObjectSiteFolder
        '  _bitMask = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BitMaskAree 'dm Global Variable
        '  _brand = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BrandCurrent
        _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Archive.htm", _siteFolder)
        _ajaxLiteraturePage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _ajaxTagsPage = String.Format("/{0}/HP3Handler/Tags.ashx", _siteFolder)
        _siteArea = Me.PageObjectGetSiteArea.Id
        _keyContentType = Me.PageObjectGetContentType
        _userKey = Me.ObjectTicket.User.Key
        _langKey = Me.PageObjectGetLang
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        _contentKey = Me.PageObjectGetContent
    End Sub
	
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    Dim _CheckApproval As Integer = 1
    
    Protected Sub Page_Load(sender As Object, e As System.EventArgs)
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
        
        Dim idContentTheme As Integer = 0
        Dim idContent As Integer = Me.PageObjectGetContent.Id
        If idContent > 0 Then
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme, _langKey)
            _lancioThema = valthem.Launch
            If Not String.IsNullOrEmpty(_lancioThema) Then
                divThemelaunch.Visible = True
            End If
            idContentTheme = valthem.KeyContent.Id
            _topKeys = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _langKey.Id)
            Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent = _topKeys
            If idContentTheme <> idContent Then
                InitDetailView()
            Else
                InitView()
            End If
        Else
            InitView()
        End If
        
        Dim _ctrlPath As String = String.Format("/{0}/HP3Common/BoxRelatedContent.ascx", _siteFolder)
        Me.LoadSubControl(_ctrlPath, ph_RelatedContent)
        div_RelatedContent.Visible = True
        ph_RelatedContent.Visible = True
        
        
        If _siteArea = Dompe.SiteAreaConstants.IDSiteareaNews Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/News.htm", _siteFolder)
            strSezione = "<h1 class='white'>News</h1>"
            _cssInlineCode = String.Format("background-image: url('/{0}/_slice/ufficio_stampa.jpg');", _siteFolder)
            If idContent = 0 Then
                disclaimerNews.Visible = True
                ltlDisclaimer.Text = BusinessDictionaryManager.Read("DMP_disclaimerNews", _langKey.Id, "DMP_disclaimerNews")
            End If
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDSiteareaPubblicazione Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Pubblicazione.htm", _siteFolder)
            strSezione = "<h1 class='grey'>Pubblicazioni</h1>"
            _cssInlineCode = String.Format("background-image: url('/{0}/_slice/imgTopPubb.jpg');", _siteFolder)
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDSiteareaSedi Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Sedi.htm", _siteFolder)
            strSezione = "<h1>sedi</h1>"
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDSiteareaNewsMediaPress Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
            strSezione = "<h1>Press</h1>"
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDEventiConferenze Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Eventi.htm", _siteFolder)
            strSezione = "<h1>Eventi</h1>"
        Else
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Archive.htm", _siteFolder)
            strSezione = "<h1>Generico</h1>"
        End If
        
        _notAvaible = Me.BusinessDictionaryManager.Read("DMP_notavaible", _langKey.Id, "DMP_notavaible")
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    
    Protected Sub InitDetailView()
        
        Dim path As String = String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder)
        If Me.BusinessMasterPageManager.GetSiteArea.Id = Dompe.SiteAreaConstants.IDEventiConferenze Then
            path = String.Format("/{0}/HP3Service/DetailContentEventi.ascx", _siteFolder)
        End If
        LoadSubControl(path, plhDetail)
        plhArchive.Visible = False
        plhDetail.Visible = True
              
    End Sub
    
   
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub

    Protected Sub InitView()
        
        plhDetail.Visible = False
        plhArchive.Visible = True
        
          
        '  InitAreaFilters()
    End Sub

  
</script>

<script language="javascript" type="text/javascript">
    var templateUrl = "<%=_emptyLiteratureArchiveTemplate%>";
    var ajaxLiteraturePage = "<%=_ajaxLiteraturePage%>";
    var nPage = 1;
    var _loadingLibrary = false; 
    var _pageSize = <%=_MaxItem %>;
    var _defaultBitmask = '<%=_bitMask%>';
    var _currentBitmask = _defaultBitmask;
    var _defaultKeysToExclude = "<%=_topKeys%>";
    var _loadingStatus = false;
    var yearsList=0;

    $(document).ready(function () {
        FilterApply();
    });

  

    function FilterApply(){
        nPage = 1;
        $(".notavaible").remove();
        $(".archiveCnt").remove();
   
        var so = prepareSearcherLiterature('', _defaultBitmask, _defaultKeysToExclude);
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        
        InitAutoCompleater();
        InitSearch();
    
    }

    function yearsOpen(){
        if(yearsList==0){
            $('.yearsList').css('display','block');
            yearsList=1;
        }else{
            $('.yearsList').css('display','none');
            yearsList=0;
        }
    }
	
    function selYear(id){
        $('.yearsList').css('display','none');
        yearsList=0;
        $('.yearSearch').html(id);
        $('.yearHidden').val(id);
    }


    function GetTags()
    {
        var tags = '';             
        $("ul#tagsView li").each(function(){
            tags += $(this).attr('data-tag') + ',' || '';
        });

        return tags;
    }

    function InitSearch(){
        $('.filterBtn #btnTagSearch').click(function(){
            DoSearch();
        });
    }

    function DoSearch()
    {
        _loadingStatus=false;
        $("#literatureArchive").empty();            
        var tags = GetTags();                                    
        var so = prepareSearcherLiterature(tags, _currentBitmask, '')
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
    }

    function InitAutoCompleater(){      

        var searchField = $('.bgFilter #tagSearch');
        searchField.val('');
        searchField.keyup(function (e) {                    
            ReadTags();        
        });      

        searchField.focusout(function(e){
            searchField.val('');    
        });
        
    }

    function ReadTags(){
        
        var postData = {
            op: '<%=Keys.EnumeratorKeys.TagOperationType.List %>',
            search: $('.bgFilter #tagSearch').val(),
            limit: '50'  
        };

        $('.bgFilter #tagSearch').autocomplete({
            minLength:2,
            focus: function(event, ui){                
                return false; 
            },
            select: function(event, ui){                           

                $("#tagsView").append("<li data-tag=" + ui.item.value + ">" + ui.item.label + "<a id='removeTag' title='Delete'>X</a></li>");
                $("ul#tagsView li[data-tag="+ ui.item.value +"] a").click(function(){
                    $(this).closest('li[data-tag='+ ui.item.value +']').remove();
                    DoSearch();
                });              
               
                $("ul#tagsView li[data-tag="+ ui.item.value +"]").hover(
                 function(){
                     $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").removeClass("hidden");
                 },
                 function(){
                     $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").addClass("hidden");
                 }
                );

                DoSearch();

                $("#tagSearch").val('');
                return false;            
                    
            },
            source: function(request, response){

                $.ajax({
                    type: "POST",
                    url: '<%=_ajaxTagsPage %>',            
                data: postData,
                success: function( data ) {
                    if (data){
                        if (data.Success) {        
                            if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %>')
                            {
                                response( $.map( data.Result, function( item ) {
                                    return {
                                        label: item.TagName,
                                        value: item.TagKey

                                    }
                                }));                            
                            } else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %>')
                            {
                                response('','');
                            }
                        }
                        else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                            document.location.reload();
                        }
                    }
                },
                 error: function(XMLHttpRequest, textStatus, errorThrown) {
                     //alert(textStatus);
                 }
     
             });
            }
        });
}

function InitReadMore()
{
    //            $("#moreInfo").on('click', function () {            
    var keysToExclude = _defaultKeysToExclude;
    var tags = GetTags(); 
    nPage = nPage + 1; 
    var contentSearcher = prepareSearcherLiterature(tags, _currentBitmask, keysToExclude);
    Literature.read(contentSearcher);
    return false;
    //        });  
}

var ReadLiteratureCallback = function (data) {  
    if(data){
        if (data.Success) {        
            if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %> )
                {     
                    _loadingStatus = true;
                var container = $("#literatureArchive");
                $.tmpl("ArchiveTemplate", data.Result.Collection).appendTo(container);
                
                if(data.Result.PageNumber == data.Result.PageTotalNumber)
                {  
                    $('#moreInfo').hide();
                    $('#moreInfoBar').hide();
                }
                else
                { 
                    $('#moreInfo').show();
                    $('#moreInfoBar').show();
                }
                    

            } else if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %> )
                {
                  $('#moreInfo').hide();
            $('#moreInfoBar').hide();
                    
            // if(!_loadingStatus)
            $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
                }
    }
            else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
        document.location.reload();
    }
    }
    else
    {
        $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
            }      
       
            };

            var OnReadLiteratureArchiveCompleteCallback = function () {
             
                $('#LiteratureArchiveLoader').hide();
                _loadingLibrary=false;
                $("#literatureArchive").fadeIn('fast');        

            };


            function prepareSearcherLiterature(TagsP, bitmask, keysToExclude) {
                var year = $(".yearSearch").html();
                var word = $("#tagSearchArchivio").val();
                var searcher = {          
                    SiteareId: '<%=_siteArea%>',
            ContenTypeId: '<%=_keyContentType.Id%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=_themeKey.Id %>',
            Brand: '',
            BitMask: bitmask,
            Display: true,
            Active: true,
            Delete: false,
            ApprovalStatus:'<%=_CheckApproval%>',
            Year: year,
            Word: word,
            Tags:'',
            PageNumber:nPage,
            PageSize:_pageSize,
            KeysToExclude: keysToExclude
        };

        recordOutboundFull("Archivio <%= strSezione %>","Caricamento","page: "+ nPage)

        if(TagsP != '')
            searcher.Tags =  TagsP;


        //        if(CountriesP != '')
        //            searcher.Countries =  CountriesP;
                    
        return searcher;

    };

    function LoadLiteratureArchive(contentSearcher, callback, onCompleteCallback)   //old LoadProfile
    {
        $('#LiteratureArchiveLoader').show();
        $('#moreInfo').hide();
        $('#moreInfoBar').hide();  
      
        var postData = {
            op: '<%=Cint(Keys.EnumeratorKeys.LiteratureViewType.List)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: ajaxLiteraturePage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {
                if (typeof callback === "function") {
                    callback(data);
                }
                $(".archiveCnt").hover(
         function () {
             $(this).children('.effectHover').css('display','none');
         },
         function () {
             $(this).children('.effectHover').css('display','block');
         }
     );
            },
            error: function (data) {
                //alert("error")
                //$("#literatureArchive").fadeIn('fast');
            }
        }).complete(function () {            
            if(onCompleteCallback && typeof(onCompleteCallback) == 'function') onCompleteCallback();
            //if (typeof onCompleteCallback === "function") {
            // onCompleteCallback();
            // }
        }); 
    }


    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {
            $.template("ArchiveTemplate", template);

            //se non � LoaderFunction nothing
            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    var Literature = Literature || new function (){
           

        this.read = function (contentSearcher) {
            if (true == _loadingLibrary) return;

            _loadingLibrary = true;

            $('#LiteratureArchiveLoader').show();
            $('#moreInfo').hide();
            $('#moreInfoBar').hide();
            //var container = jQuery('#literatureArchive');  
            
            LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        };
    }

    function FilterArea(item){
        
        _loadingStatus=false;
        $('.contentHome .filter .aree a[data-mask='+ item +']').toggleClass('sel');
        nPage=1;

        var bitmask = GetBitmask();
        _currentBitmask = bitmask;
        
        var tags = GetTags();  

        var keysToExclude = '';
        var contentSearcher = prepareSearcherLiterature(tags, bitmask, keysToExclude);
        $("#literatureArchive").empty();
        LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback)
    }

    function GetBitmask()
    {
        var mask = '';
        $('.contentHome .filter .aree a.sel').each(function(){
            mask += $(this).attr('data-mask') + ',' || '';
        });

        if(mask == '' ) {
            mask = _defaultBitmask;
        }

        return mask;
    }
        
   
</script>
<asp:PlaceHolder runat="server" ID="plhArchive">
<%--<div class="presentation">
    <div class="container">
        <div class="presentationBox">
            <div class="presentationBox2">
                <div class="presentationBox3">
                    <h2><%= Me.BusinessDictionaryManager.Read("Archive_" & _themeKey.Id, _langKey.Id, "SoluzioniTeraupetiche_" & _themeKey.Id)%></h2>
                </div>
            </div>
        </div>
    </div>
</div>--%>
<asp:PlaceHolder ID="plhBoxLiterature" runat="server"></asp:PlaceHolder>
    <section>
        <div class="cont-img-top">
    	<div class="bg-top" style="<%=_cssInlineCode %>">
            
        </div>    
            <div class="txt">
                <%= strSezione%>  
            </div>
            <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <%--<div class="box-container titleSection" style="<%=_cssInlineCode %>">
        <div class="boxCnt">
            <div class="container">
                <%= strSezione%>
            </div>
        </div>
        </div>--%>
         <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    </section>
<section>
    <div class="container">
    <div class="colLeft">
       
           <div class="listSide" id="div_RelatedContent" runat="server" visible="false">                	   
                <asp:PlaceHolder runat="server" ID="ph_RelatedContent" Visible="false"></asp:PlaceHolder>               
            </div>
       
    </div><!--end colLeft-->
    <div class="colRight" style="padding-top:0px;">
        
        <div class="containerArchiveTot">
            <div class="positionRel" id="disclaimerNews" runat="server" visible="false">
                    <div class="generic">
                        <div class="colArchive">
                            <asp:Literal ID="ltlDisclaimer" runat="server"></asp:Literal>
                        </div> 
                    </div> 
            </div> 
            <div id="literatureArchive"></div>
            <div id="LiteratureArchiveLoader" style="clear:both; float:left; width:100%; height:200px;" class="positionRel"><div style="text-align:center; clear:both; float:left; position:absolute; left:50%; top:50px; margin-left:-16px; height:200px;"><img src="/<%=_siteFolder%>/_slice/ajax-loader.gif" alt="loading" /></div></div>
            <%--<div class="bar" id="moreInfoBar">
                <div class="bar2">
                <a id="moreInfo" href="javascript:void(0);" onclick="InitReadMore();"><span><%=Me.BusinessDictionaryManager.Read("DMP_MORE_INFO", _langKey.Id, "Altri Risultati")%></span></a>
                </div>
            </div>--%>



            <div id="moreInfoBar" class="bar" style="clear: both; width:100%;margin: 0 auto;">
                <div class="bar2" style="width:300px; margin:0 auto; background-color:#ECECEC; clear:both; height:35px; float:left;">
                <a onclick="InitReadMore();" href="javascript:void(0);" id="moreInfo" style="text-align: center; padding-top: 10px; float: left; clear: both; width: 100%; color: #000; font-weight: bold;"><span><%=Me.BusinessDictionaryManager.Read("DMP_MORE_INFO", _langKey.Id, "Altri Risultati")%></span></a>
                </div>
            </div>



        </div>
   
       <div class="container" style="display:none;">
         <div class="bodyBox">
        <div id="divThemelaunch" class="themeLaunch" runat="server" visible="false"><%= _lancioThema%></div>
           <div class="searchBox">
                <h4 class="searchTitle"><%=Me.BusinessDictionaryManager.Read("DMP_Search", _langKey.Id, "DMP_Search")%></h4>	
                  
                <div class="searchForm">
                  
                         <input type="text"  id="tagSearchArchivio" />
                 
                     <div class="yearSearch" onclick="javascript:yearsOpen();"></div>

                    <ul class="yearsList" style="display: none;">
                        <li><a title="" href="javascript:void(0);" onclick="javascript:selYear('');">&nbsp;</a></li>
                        <li><a title="2013" href="javascript:void(0);" onclick="javascript:selYear('2013');">2013</a></li>
                        <li><a title="2012" href="javascript:void(0);" onclick="javascript:selYear('2012');">2012</a></li>
                        <li><a title="2011" href="javascript:void(0);" onclick="javascript:selYear('2011');">2011</a></li>
                    </ul>
                       <input id="hdnyear" class="yearHidden" type="hidden" value="-1" />
                       <a id="A1" href="javascript:void(0);" onclick="FilterApply()" runat="server" class="btnSearch"><%= Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch")%></a>
                </div>      
            </div>   <!--end filter--> 
            </div> <!--End body box-->
        </div> <!--End container-->
        
    </div>
        </div>
    </section>

</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">

</asp:PlaceHolder>
<script type="text/javascript">
	<%' If Not _isMobile Then %>
    //$(document).scroll(function () {
    //    var hDoc = $(document).height();
    //    var colLeftH = $('.colLeft').height();
    //    var ctrlEnd = hDoc - 110;
    //    var hWin = $(window).scrollTop() + colLeftH;

    //    if (hWin >= ctrlEnd) {
		
    //        $('.colLeft').css("position", "fixed");
    //        $('.colLeft').css("top", "auto");
    //        $('.colLeft').css("bottom", "110px");
    //    }else{
    //        if ($(window).scrollTop() > 431) {
    //            $('.colLeft').css("position", "fixed");
    //            $('.colLeft').css("top", "115px");
    //            $('.colLeft').css("bottom", "auto");
    //        } else {
    //            $('.colLeft').css("position", "absolute");
    //            $('.colLeft').css("top", "546px");
    //            $('.colLeft').css("bottom", "auto");
    //        }
    //    }
    //});
    <%' End If %>
</script>
