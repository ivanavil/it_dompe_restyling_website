﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = "/Dompe/_slice/imgCenter.jpg"
    Dim _Last As Integer = 0
    Dim TitlePage As String = ""
       
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _lang = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        
    End Sub
        
    
    Sub Page_Load()
        contentId = Me.PageObjectGetContent.Id
        ' DivImagesTop.Attributes("class") = Me.PageObjectGetLang.Code & "_" & Me.PageObjectGetSiteArea.Name
      
        TitlePage = Me.BusinessThemeManager.Read(_currentTheme).Launch
        
        Dim serchcont As New ContentExtraSearcher
                
        If Not Page.IsPostBack Then
            LoadMenu()
        End If
                  
    End Sub
    
    Sub LoadMenu()
        Dim cntcoll As New ContentCollection
        Dim cntsearch As New ContentSearcher
        Dim cntVal As New ContentValue
      
        cntsearch.KeySiteArea.Domain = _siteArea.Domain
        cntsearch.KeySiteArea.Name = _siteArea.Name
        cntsearch.key.IdLanguage = _lang.Id
        cntsearch.KeyContentType.Domain = Dompe.ContentTypeConstants.Domain.DMP
        cntsearch.KeyContentType.Name = Dompe.ContentTypeConstants.Name.Ricerca
        cntsearch.KeyContentType.Id = Dompe.ContentTypeConstants.Id.RicercaCtype
        cntsearch.SortType = ContentGenericComparer.SortType.ByData
        cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
        
        cntcoll = Me.BusinessContentManager.Read(cntsearch)
        
        If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
            _Last = cntcoll.Count - 1
            rptSiteMenu.DataSource = cntcoll
            rptSiteMenu.DataBind()
            rptAncore.DataSource = cntcoll
            rptAncore.DataBind()
        End If
    End Sub
    
  
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = contentId
      
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
       
  
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _lang
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function getClass(ByVal index As Integer) As String
        Dim res As String = ""
        
        Select Case index
            Case 0
                res = "rdFirst"
                Return res
            Case 1
                res = "rdSecond"
                Return res
           
        End Select
        
        If (index Mod 2 = 0) Then
            If index = _Last Then
                res = "rdLastBlack"
                Return res
            Else
                res = "rdBlack"
                Return res
            End If
        End If
      
        If (index Mod 2 = 1) Then
            If index = _Last Then
                res = "rdLastWhite"
                Return res
            Else
                res = "rdWhite"
                Return res
            End If
        End If
       
        
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _lang.Id, defaultLabel)
    End Function
</script>
<script type="text/javascript">
 $(document).ready(function(){
     $(".rdMenuBox div").hover(
			function () {
			    $(this).css('background-position', 'left -126px');
			    $(this).children('.txt').css('color', '#efede6');
			},
			function () {
			    $(this).css('background-position', 'left top');
			    $(this).children('.txt').css('color', '#333');
			}
		);
});

function ancMenu(id) {
    var p = $("#cnt" + id);
    var position = p.position();
    var scrolVar = position.top - 164;
    scroll(0, scrolVar);
}

</script>

<div class="presentation">
      <div class="presentationBox">
             <div class="presentationBox2">
                  <div class="presentationBox3">
                   	<div class="container">
        	           	<h1><%= Me.BusinessDictionaryManager.Read("PLY_TITLE_" & _currentTheme.Name, _lang.Id, "PLY_TITLE_" & _currentTheme.Name)%></h1>
                    </div>
                </div>
            </div>
       </div>
 </div>
   <div class="rdMenu">
   <div class="container">
    <asp:Repeater ID="rptAncore" runat="server">
     <HeaderTemplate><div class="rdMenuBox"></HeaderTemplate>
     <ItemTemplate>
       <div class="rdMenuItem<%# Container.ItemIndex + 1 %>">
       <img src="/ProjectDOMPE/_slice/trasp.gif" width="109" height="126" alt="Menu" usemap="#menuRd<%# Container.ItemIndex + 1 %>">
               <a href="javascript:void(0);" onclick="javascript:ancMenu(<%# Container.ItemIndex + 1 %>);" class="txt"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
              <map name="menuRd<%# Container.ItemIndex + 1 %>">
              <area shape="poly" coords="52,124,1,92,1,28,54,1,110,30,109,92" href="javascript:void(0);" onclick="javascript:ancMenu(<%# Container.ItemIndex + 1 %>);" alt="">
              </map>
         </div>         
       </ItemTemplate>
    <FooterTemplate></div></FooterTemplate>
    </asp:Repeater>
   </div>
   </div>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
    <div class="titleContainer">
    	<div class="container">
        	<div class="titleContainerBox">
               <h3><%= TitlePage%></h3>
            </div>
        </div>
    </div>
     <asp:Repeater ID="rptSiteMenu" runat="server">
                <HeaderTemplate>
               </HeaderTemplate> 
                <ItemTemplate>
                <div class="<%# getClass(Container.ItemIndex) %>" id="cnt<%# Container.ItemIndex + 1 %>">
                  <div class="container">
                    <div class="rdBox">
                       <div class="rdBox2">
                           <div class="rdBox3">
                             <h3><i><%# DataBinder.Eval(Container.DataItem, "Title")%></i></h3> 
                            <%# DataBinder.Eval(Container.DataItem, "Launch")%>
                            </div>
                       </div>
                    </div>
                   </div>
                    </div>
               </ItemTemplate>
             <FooterTemplate></FooterTemplate>
     </asp:Repeater>
</asp:PlaceHolder> 
              
