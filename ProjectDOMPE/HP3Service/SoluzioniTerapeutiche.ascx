﻿<%@ Control Language="VB" ClassName="SoluzioniTerapeutiche" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Threading.Tasks" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>

<script runat="server">
  
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _text As String = String.Empty
    Protected _title As String = String.Empty
    Protected _titleClass As String = String.Empty
    Protected _titleStyle As String = String.Empty
	Private _cacheKey As String = String.Empty
	Protected _siteArea As SiteAreaIdentificator = Nothing
    
    Protected Sub Page_Init()
        _siteFolder = Me.ObjectSiteFolder
        _langKey = Me.PageObjectGetLang
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
		_siteArea = Me.PageObjectGetSiteArea
    End Sub

    
    Protected Sub Page_Load()
        _cacheKey = "_cacheSoluzioneTerapeutiche1Livello:" & _langKey.id & "|" & _themeKey.Id & "|" & _siteArea.Id & "|"
        If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If

		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_text = _strCachedContent
		Else
			InitView()
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub InitView()
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key = _themeKey
        soTheme.KeyLanguage.Id = _langKey.Id
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentExtraSearcher
            so.KeySite = _siteKey
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = theme.KeyContent.Id
            so.CalculatePagerTotalCount = False
            so.SetMaxRow = 1
            Dim coll = Me.BusinessContentExtraManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                If Not cont Is Nothing Then
                    _text = cont.FullText
                    _title = cont.TitleContentExtra
                    _titleClass = cont.Code
                    _titleStyle = cont.TitleOriginal
                    Dim replacing = GetPlaceHolders()
                    If Not replacing Is Nothing AndAlso replacing.Any() Then
                        For Each item As KeyValuePair(Of String, String) In replacing
                            _text = _text.Replace(item.Key, item.Value)
                        Next
                    End If
				                    
                End If
				
				'Cache Content and send to the oputput-------
				CacheManager.Insert(_cacheKey, _text, 1, 120)
				'--------------------------------------------
            End If
        End If
               
    End Sub
    
    Protected Function GetPlaceHolders() As Dictionary(Of String, String)
        Dim replacing As New Dictionary(Of String, String)
        replacing.Add("[APPARATORESPIRATORIO]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idApparatoRespiratorio, _langKey.Id))
        replacing.Add("[MALATTIERARE]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idSpecialityCare, _langKey.Id))
        replacing.Add("[PATOLOGIEOSTEOARTICOLARI]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idInfiammazione, _langKey.Id))
        replacing.Add("[AUTOMEDICAZIONE]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idAutomedicazione, _langKey.Id))
        replacing.Add("[NETWORKRD]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idNetwork, _langKey.Id))
        replacing.Add("[CARDIOLOGIA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idCardilogia, _langKey.Id))
        replacing.Add("[GASTROENTEROLOGIA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idGastroenterologia, _langKey.Id))
        replacing.Add("[RESPIRATORIO]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idRespiratorio, _langKey.Id))
        
        Return replacing
    End Function
</script>

<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/soluzioni.jpg');">            
        </div>    
         <div class="txt">
                <h1><%=Me.BusinessDictionaryManager.Read("theme_" & _themeKey.Id, _langKey.Id, "Soluzioni terapeutiche")%></h1>          
            </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>



<section>
    <div class="container"><%=_text %></div>
</section>