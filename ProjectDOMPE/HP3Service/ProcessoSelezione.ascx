﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private MAXLinksStorie As Integer = 4
    Private _classSection As String = String.Empty
    Protected _ddlJobOpportunities As String = String.Empty
    Protected _jobDescriptions As String = String.Empty
    Protected _jobDescriptionLabel As String = String.Empty
    Protected _privacyLabel As String = String.Empty
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
        
    
    Sub Page_Load()

        InitView()
       
                  
    End Sub
    
    Protected Sub InitView()
        
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        Dim serchcont As New ContentExtraSearcher
        Dim idContentTheme As Integer = 0
        If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            idContentTheme = valthem.KeyContent.Id
           
            If contentId = idContentTheme Then
                contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
           
                If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                    Dim strTesto As String = contentextraVal.FullText
                    strTesto = loadStorieStudenti(strTesto)
                    strTesto = loadStorieProfessionisti(strTesto)
                    titolo_sezione.Text = contentextraVal.Title
                    testo_title.Text = contentextraVal.Title
                    testo_sezione.Text = strTesto
                    testo_lancio.Text = contentextraVal.Launch
                    testo_abstract.Text = contentextraVal.Abstract
                    test_selfapplication.Text = contentextraVal.FullTextOriginal
                    _classSection = contentextraVal.Code
                    'LoadContentAssociati(contentextraVal.Key.PrimaryKey)
                End If
                 
            Else
                contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(contentId, _langKey.Id))
                'Dim img As String = GetImagePersonal(contentextraVal.Key, contentextraVal.Title)
                'If Not img Is Nothing AndAlso img.Length > 1 Then
                '    imgPath = "<img src=""/HP3Image/cover/" & img & """ title=""" & contentextraVal.Title & """></img>"
                'End If
                Dim myFilePath As String = Server.MapPath("~/ProjectDOMPE/_slice/imgPage/interna/" & _langKey.Code.ToLower & "/" & contentextraVal.Key.PrimaryKey & ".jpg")
                If File.Exists(myFilePath) Then
                    ltlimag.Text = "background-image:url(""/ProjectDOMPE/_slice/imgPage/interna/" & _langKey.Code.ToLower & "/" & contentextraVal.Key.PrimaryKey & ".jpg"");height:320px;"
                    ltlpresentation.Text = "border-bottom: 10px solid #C30505;height: 320px;"
                End If
                
                titolo_sezione.Text = contentextraVal.Title
                testo_title.Text = contentextraVal.Title
                testo_sezione.Text = contentextraVal.FullText
                '  testo_ruolo.Text = "<br /><div class=""content""><h4>" & contentextraVal.Title & "</h4><i>" & contentextraVal.SubTitle & "</i></div>"
            End If
          
            Dim _ctrlPath As String = String.Format("/{0}/HP3Common/BoxRelatedContent.ascx", _siteFolder)
            Me.LoadSubControl(_ctrlPath, ph_RelatedContent)
            div_RelatedContent.Visible = True
            ph_RelatedContent.Visible = True
            LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        End If
        
        If Not IsPostBack() Then
            If (_siteArea.Id = Dompe.SiteAreaConstants.IDSiteareaProfessionisti Or _siteArea.Id = Dompe.SiteAreaConstants.IDSiteareaStudenti) Then
                LoadSubControl(String.Format("/{0}/HP3Common/BoxStorie.ascx", _siteFolder), phlStorie)
            End If
         
            LoadSubControl(String.Format("/{0}/HP3Common/BoxNews.ascx", _siteFolder), Phlnews)
            If Dompe.ContentBoxImage.IdContentList.Contains("{" & Me.PageObjectGetContent.Id.ToString & "}") Then
                LoadSubControl(String.Format("/{0}/HP3Common/BoxImage.ascx", _siteFolder), PhlBoxImage)
            End If
            LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
            LoadSubControl(String.Format("/{0}/HP3Common/BoxLinks.ascx", _siteFolder), PhlLinks)
            
        End If
        
        BindJobOpportunities()
        
    End Sub
    
    Protected Sub BindJobOpportunities()
        Dim defaultText As String = Me.BusinessDictionaryManager.Read("DMP_DdlJobOpp", _langKey.Id, "DMP_DdlJobOpp")
        Dim defaultValue As String = String.Empty
		
		'------------------
		Dim soEx As New ContentExtraSearcher()
		soEx.key.IdLanguage = _langKey.Id
		soEx.CalculatePagerTotalCount = False
		soEx.KeySite = Me.PageObjectGetSite
		soEx.KeyContentType.Id = Dompe.ContentTypeConstants.Id.PosizioniAperteCtype
		soEx.KeySitearea.Id = Dompe.SiteAreaConstants.IDSiteareaPosizioniAperte
		soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
		soEx.key.IdLanguage = _langKey.Id
		
		soEx.Properties.Add("Title")
		soEx.Properties.Add("FullText")
		
		soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
		Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
		If Not contentextraColl is Nothing AndAlso contentextraColl.Count > 0 Then
            Dim sbDescr As New StringBuilder()
            Dim sb As New StringBuilder()
            sb.AppendFormat("<a href='javascript:void(0);' title='{0}' class='selDrop' id='selPosizione'>{1}</a>", defaultText, defaultText)
            sb.Append("<ul id='ulPosizione'>")
			Dim description As String = String.Empty
			For Each item as contentextraValue In contentextraColl
				description = item.Title
				sb.AppendFormat("<li><a class='aPosizione' data-pos='{0}' href='#' title='{1}'>{2}</a></li>", item.Key.Id, description, description)
				sbDescr.AppendFormat("<div id='{0}' style='display:none;'>{1}</div>", item.Key.Id, item.FullText)
			Next
            sb.Append("</ul>")
            _ddlJobOpportunities = sb.ToString()
            _jobDescriptions = sbDescr.ToString()
		End If
		'------------------
		
        'Dim so As New ContextSearcher()
        'so.KeyContextGroup = New ContextGroupIdentificator(Dompe.ContextGroup.Jobopportunities)
        
        'Dim ctxMan As New ContextManager()
        'Dim coll = ctxMan.Read(so)
        'If Not coll Is Nothing AndAlso coll.Any() Then
            
        '    Dim sbDescr As New StringBuilder()
        '    Dim sb As New StringBuilder()
            
        '    sb.AppendFormat("<a href='javascript:void(0);' title='{0}' class='selDrop' id='selPosizione'>{1}</a>", defaultText, defaultText)
        '    sb.Append("<ul id='ulPosizione'>")
            
        '    Dim description As String = String.Empty
        '    For Each item In coll
        '        description = Me.BusinessDictionaryManager.Read(item.description, _langKey.Id, item.description)
        '        sb.AppendFormat("<li><a class='aPosizione' data-pos='{0}' href='#' title='{1}'>{2}</a></li>", item.Key.Id, description, description)
        '        sbDescr.AppendFormat("<div id='{0}' style='display:none;'>{1}</div>", item.Key.Id, Me.BusinessDictionaryManager.Read(item.Key.KeyName, _langKey.Id, item.Key.KeyName))
        '    Next
        '    sb.Append("</ul>")
        '    _ddlJobOpportunities = sb.ToString()
        '    _jobDescriptions = sbDescr.ToString()
        'End If
        
        _jobDescriptionLabel = Me.BusinessDictionaryManager.Read("DMP_JObDescrLabel", _langKey.Id, "DMP_JObDescrLabel")
        _privacyLabel = Me.BusinessDictionaryManager.Read("DMP_JobPrivacyCheck", _langKey.Id, "DMP_JobPrivacyCheck")
                       
    End Sub
    
    Function loadStorieStudenti(ByVal fullText As String) As String
        Dim res As String = fullText
        Dim strLink As String = ""
        Dim searchCont As New ContentExtraSearcher
        Dim searchColl As New ContentExtraCollection
        
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("LinkStudenti_" & _langKey.Id)
        If useCache Then
            res = CacheManager.Read("LinkStudenti_" & _langKey.Id, 1).ToString
        Else
            searchCont.KeySite.Id = 46
            searchCont.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaStudenti
            searchCont.KeyContentType.Id = Dompe.ContentTypeConstants.Id.ProcessoSelezioneCtype
            searchCont.key.IdLanguage = _langKey.Id
            searchCont.SortOrder = ContentGenericComparer.SortOrder.DESC
            searchCont.SortType = ContentGenericComparer.SortType.ByData
            searchCont.SetMaxRow = MAXLinksStorie
            searchCont.Properties.Add("Key.Id")
            searchCont.Properties.Add("Key.PrimaryKey")
            searchCont.Properties.Add("Launch")
            searchCont.Properties.Add("Title")
            searchCont.Properties.Add("SubTitle")
            searchCont.Properties.Add("TitleContentExtra")
           
            
            searchColl = Me.BusinessContentExtraManager.Read(searchCont)
        
            If Not searchColl Is Nothing AndAlso searchColl.Count > 0 Then
                strLink = "<div class=""archiveBox""><div class=""searchBox1""><h4 class=""photoTitle"">" & Me.BusinessDictionaryManager.Read("TitleBoxStorieStudenti", _langKey.Id, "TitleBoxStorieStudenti") & "</h4><p>" & Me.BusinessDictionaryManager.Read("SubTitleBoxStorieStudenti", _langKey.Id, "SubTitleBoxStorieStudenti") & "</p></div>"
                For Each itm As ContentExtraValue In searchColl
                    strLink = strLink & "<div class=""boxPhotoGallery""><div class=""cnt""><a title=""" & itm.Title & """ class=""effectHover"" href=""" & GetLink(itm, Dompe.ThemeConstants.IDStudenti) & """>" & itm.Title & "</a><a title=""" & itm.Title & """ href=""" & GetLink(itm, Dompe.ThemeConstants.IDStudenti) & """>" & GetImageStorie(itm.Key, itm.Title) & "</a><h3><a href=""" & GetLink(itm, Dompe.ThemeConstants.IDStudenti) & """>" & itm.Title & "</a></h3><p><b>" & itm.TitleContentExtra & "</b></p><p><i>" & itm.SubTitle & "</i></p></div></div>"
                Next
                strLink = strLink & "</div>"
            End If
      
            res = fullText.Replace("[LINK STUDENTI]", strLink)
            CacheManager.Insert("LinkStudenti_" & _langKey.Id, res, 1, 60)
        End If
        Return res
    End Function
    
    Function loadStorieProfessionisti(ByVal fullText As String) As String
        Dim res As String = fullText
        Dim strLink As String = ""
        Dim searchCont As New ContentExtraSearcher
        Dim searchColl As New ContentExtraCollection
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("LinkProfessionisti_" & _langKey.Id)
        If useCache Then
            res = CacheManager.Read("LinkProfessionisti_" & _langKey.Id, 1).ToString
        Else
            searchCont.KeySite.Id = 46
            searchCont.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaProfessionisti
            searchCont.KeyContentType.Id = Dompe.ContentTypeConstants.Id.ProcessoSelezioneCtype
            searchCont.key.IdLanguage = _langKey.Id
            searchCont.SortOrder = ContentGenericComparer.SortOrder.DESC
            searchCont.SortType = ContentGenericComparer.SortType.ByData
            searchCont.SetMaxRow = MAXLinksStorie
            searchCont.Properties.Add("Key.Id")
            searchCont.Properties.Add("Key.PrimaryKey")
            searchCont.Properties.Add("Launch")
            searchCont.Properties.Add("Title")
            searchCont.Properties.Add("SubTitle")
            searchCont.Properties.Add("TitleContentExtra")
            searchColl = Me.BusinessContentExtraManager.Read(searchCont)
        
            If Not searchColl Is Nothing AndAlso searchColl.Count > 0 Then
                strLink = "<div class=""archiveBox""><div class=""searchBox1""><h4 class=""photoTitle"">" & Me.BusinessDictionaryManager.Read("TitleBoxStorieProfessionisti", _langKey.Id, "TitleBoxStorieProfessionisti") & "</h4><p>"& Me.BusinessDictionaryManager.Read("SubTitleBoxStorieProfessionisti", _langKey.Id, "SubTitleBoxStorieProfessionisti") & "</p></div>"
                For Each itm As ContentExtraValue In searchColl
                    strLink = strLink & "<div class=""boxPhotoGallery""><div class=""cnt""><a title=""" & itm.Title & """ class=""effectHover"" href=""" & GetLink(itm, Dompe.ThemeConstants.IDProfessionisti) & """>" & itm.Title & "</a><a title=""" & itm.Title & """ href=""" & GetLink(itm, Dompe.ThemeConstants.IDProfessionisti) & """>" & GetImageStorie(itm.Key, itm.Title) & "</a><h3><a href=""" & GetLink(itm, Dompe.ThemeConstants.IDProfessionisti) & """>" & itm.Title & "</a></h3><p><b>" & itm.TitleContentExtra & "</b></p><p><i>" & itm.SubTitle & "</i></p></div></div>"
                Next
                strLink = strLink & "</div>"
            End If
            res = fullText.Replace("[LINKPROFESSIONISTI]", strLink)
            CacheManager.Insert("LinkProfessionisti_" & _langKey.Id, res, 1, 60)
        End If
        Return res
    End Function
    
    Function GetLink(ByVal keyContent As ContentValue, ByVal themaID As Integer) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent.Key, New ThemeValue(themaID), False)
    End Function
    
    'Sub LoadContentAssociati(ByRef PK As Integer)
    '    Dim so As New ContentSearcher
    '    Dim cntVal As New ContentValue
    '    Dim soRel As New ContentRelatedSearcher
    '    Dim coll As ContentCollection = Nothing
      
    '    If PK > 0 Then
    '        soRel.Content.Key.PrimaryKey = PK
    '        soRel.Content.KeyRelationType.Id = 1
    '        soRel.Content.RelationSide = RelationTypeSide.Right
    '    End If
        
      
    '    so.KeySite = Me.PageObjectGetSite
    '    so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
    '    so.SortOrder = ContentGenericComparer.SortOrder.DESC
    '    so.SortType = ContentGenericComparer.SortType.ByData
    '    so.RelatedTo = soRel
      
    '    coll = Me.BusinessContentManager.Read(so)
    '    If Not coll Is Nothing AndAlso coll.Any Then
    '        rpdDownloadRelated.DataSource = coll
    '        rpdDownloadRelated.DataBind()
    '    End If
    'End Sub
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = idContent
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
         
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    
     
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImageStorie(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        Dim quality As New ImageQualityValue()
        quality.CompositingQuality = ImageQualityValue.EnumCompositingQuality.HighQuality
        quality.SmoothingMode = ImageQualityValue.EnumSmoothingMode.HighQuality
        quality.ImageQuality = ImageQualityValue.EnumImageQuality.High
        quality.JPEGImageCompression = ImageQualityValue.EnumJPEGcompression.L50
        Dim img As String = Me.BusinessContentManager.GetCover(IDcont, 135, 135, quality, 0)
        If Not String.IsNullOrEmpty(img) Then res = "<img src=""/HP3Image/cover/" & img & """ title=""" & title & """ />"
        Return res
    End Function
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
     
    Function GetImagePersonal(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        Dim quality As New ImageQualityValue()
        quality.CompositingQuality = ImageQualityValue.EnumCompositingQuality.HighQuality
        quality.SmoothingMode = ImageQualityValue.EnumSmoothingMode.HighQuality
        quality.ImageQuality = ImageQualityValue.EnumImageQuality.High
        quality.JPEGImageCompression = ImageQualityValue.EnumJPEGcompression.L50
        res = Me.BusinessContentManager.GetCover(IDcont, 900, 250, quality, 1)
        Return res
    End Function
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
    Function GetImageContent() As String
        Dim res As String = ""
        Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High

        
        res = Me.BusinessContentManager.GetCover(Me.PageObjectGetContent, 1280, 323, imgqual, 1)
        
        If Not String.IsNullOrEmpty(res) Then
            
            res = "/HP3Image/cover/" & res
        Else
            div_banner.Visible = False
        End If
        Return res
    End Function
</script>


<section>
 	<asp:PlaceHolder id="div_banner" runat="server" >
         <div class="cont-img-top">
    	    <div class="bg-top" style="background-image:url('<%=GetImageContent()%>');">                
            </div>    
             <div class="txt">
                    <h1 class="<%=_classSection%>" style="text-align:center;"><asp:Literal ID="testo_title" runat="server" /></h1>        
             </div>
             <div class="layer">&nbsp;</div>
        </div><!--end slider-->
    <%--<div class="box-container titleSection" style="background-image:url('<%=GetImageContent()%>')">
    	<div class="boxCnt">
            <div class="container">
               <h1 class="<%=_classSection%>" style="text-align:center;"><asp:Literal ID="testo_title" runat="server" /></h1>
            </div>
        </div>
    </div><!--end titleSection-->--%>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>

</section>
<section>
    <div class="container">
    	 <div class="colLeft">        	
            	<div class="listSide">
                	<div class="listSide" id="div_RelatedContent" runat="server" visible="false">                	   
                        <asp:PlaceHolder runat="server" ID="ph_RelatedContent" Visible="false"></asp:PlaceHolder>               
                    </div>
                </div><!--end listSide-->
        </div><!--end colLeft-->
        
        <div class="colRight">
            <div class="content">
                <asp:Literal ID="testo_lancio" runat="server" />
                <asp:Literal ID="testo_abstract" runat="server" />
                <div class="greyBox displayNone">
                	<%--<h3 class="accordion"><a href="javascript:void(0);" title="Lorem ipsum">Lorem ipsum<span>Lorem ipsum</span></a></h3>--%>
                    <div class="cnt displayNone">
                    	<h4>Lorem ipsum</h4>
                        <p>Lorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolor</p>
                    	<h4>Lorem ipsum</h4>
                        <p>Lorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolor</p>
                    	<h4>Lorem ipsum</h4>
                        <p>Lorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolor</p>
                    	<h4>Lorem ipsum</h4>
                        <p>Lorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolorLorem ipsum sit dolor</p>
                    </div>
                </div>
                <div class="greyForm">
                    <div class="dropdownBox" style="z-index:97 !important;">
                        <%=_ddlJobOpportunities%>                        
                    </div>
                    <div id="contJobDescr" style="display:none;">
                        <h4><%=_jobDescriptionLabel%></h4>
                        <div id="contJDescr"><%=_jobDescriptions%></div>
                    </div>
                    <div class="rowGrey">
                    	<input type="checkbox" id="chkPrivacy"/><span><%=_privacyLabel %></span>
                    </div>
                    <a onclick="CheckForm();" href="javascript:void(0);" title='<%=Me.BusinessDictionaryManager.Read("DMP_InviaCandidatura", _langKey.Id)%>' class="btn"><%=Me.BusinessDictionaryManager.Read("DMP_InviaCandidatura", _langKey.Id)%></a>
                </div>
                <asp:Literal ID="test_selfapplication" runat="server" />
                <p><asp:Literal ID="testo_sezione" runat="server" Visible="false"/></p>
	        </div>
        </div><!--end colRight-->        
    </div><!--end container-->
    
</section>



<%--  <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>--%>
<div class="presentation" style='<asp:Literal ID="ltlpresentation" runat="server" /> display:none;'>
   <div class="presentationBox"  style='<asp:Literal ID="ltlimag" runat="server" />'>
             <div class="presentationBox2">
                    <div class="presentationBox3">
                             <div class="container">
                                  <h2><asp:Literal ID="titolo_sezione" runat="server" /></h2>
                         <%--         <%= imgPath%> --%>
                             </div>
                       </div>
             </div>
      </div>
  </div>

<div class="body" style="display:none;">
  <div class="container">
       <div class="bodyBox">
        <asp:Literal ID="testo_optional" runat="server" />
          <div class="leftBody">
            <div class="content">
            <h3></h3>
            
            <asp:Literal ID="testo_ruolo" runat="server" />
              </div>
       <asp:Repeater ID="rpdDownloadRelated" runat="server">
       <HeaderTemplate></HeaderTemplate>
       <ItemTemplate>
        <a href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>" onclick="recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>','Download Processo Selezione','Click','<%# CType(Container.DataItem, ContentValue).Title%>')">
         <span class="title"><%# CType(Container.DataItem, ContentValue).Title%></span>
                <span><%# IIf(CType(Container.DataItem, ContentValue).Launch.Length > 120, Left(CType(Container.DataItem, ContentValue).Launch, 120) & "...", CType(Container.DataItem, ContentValue).Launch)%> </span>
 <%--                 
                <span> <%# getSizeFile(CType(Container.DataItem, DownloadValue).DownloadTypeCollection(0).Weight)%></span>--%>
               <span class="readMore">Download</span>
       </ItemTemplate>
       <FooterTemplate></FooterTemplate>
       </asp:Repeater>
          </div>
        <div class="rightBody">
              <asp:PlaceHolder ID="phlStorie" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
            <div class="rightBodyRow">
              <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            </div>
        </div>
        </div>
   </div>
</div>
<div id="dialog-privacy-selezione" style="font-size:10px;"><%=GetLabel("formcandid", "formcandid")%></div>
<script type="text/javascript">
    //popup privacy
    $(function () {
        $("#dialog-privacy-selezione").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            resizable: false,
			width: 720,
        });
    });

    $("#opener-privacy-selezione").click(function (e) {
        e.preventDefault();
        $("#dialog-privacy-selezione").dialog("open");
    });
</script>
<script type="text/ecmascript">

    var _selectProfile = 0;

    function CheckForm()
    {
        if (_selectProfile == 0)
        {
            alert("<%=Me.BusinessDictionaryManager.Read("DMP_SelProfWarning", _langKey.Id, "DMP_SelProfWarning").Replace("'", "\'")%>");
            return false;
        }

        CheckPrivacy();
    }

    function CheckPrivacy()
    {
        if ($("#chkPrivacy").attr("checked") == undefined) {
            alert("<%=Me.BusinessDictionaryManager.Read("DMP_checkPrivacyWarning", _langKey.Id, "DMP_checkPrivacyWarning").Replace("'", "\'")%>");
            return false;
        }
        var possel = $('#selPosizione').html();
        document.location = 'mailto:selezione.farmaceutici@dompe.com?subject=' + possel;
    }
    $("#selPosizione").click(function () {
        $('#ulPosizione').toggle();
    });
    $(".aPosizione").click(function () {

        if (_selectProfile == 0)
            _selectProfile++;

       $('#selPosizione').html($(this).html());
       $("#contJobDescr").fadeIn('fast');

        var element = $(this).attr("data-pos");

        $("#contJDescr div").each(function (index) {
            $(this).hide();
        });

        $("div#" + element).fadeIn('fast');

        $('#ulPosizione').hide();
        return false;
    });
    $(".body").click(function (e) {

        if (e.target.id != 'selPosizione') {
            $('#ulPosizione').hide();
        }
    });

    $("#aBoxRed").click(function () {
        return CheckPrivacyBox1();
    });
    $("#aBoxbrown").click(function () {
        return CheckPrivacyBox2();
    });
    function CheckPrivacyBox1() {
        if ($("#chkPrivacy1").attr("checked") == undefined) {
            alert("<%=Me.BusinessDictionaryManager.Read("DMP_checkPrivacyWarning1", _langKey.Id, "DMP_checkPrivacyWarning1").Replace("'", "\'")%>");
            return false;
        }
       
       // document.location = 'mailto:selezione.spa@dompe.it'
    }
    function CheckPrivacyBox2() {
        if ($("#chkPrivacy2").attr("checked") == undefined) {
            alert("<%=Me.BusinessDictionaryManager.Read("DMP_checkPrivacyWarning2", _langKey.Id, "DMP_checkPrivacyWarning2").Replace("'", "\'")%>");
            return false;
        }
        //document.location = 'mailto:selezione.personale@dompe.it'
    }
</script>

	<script type="text/javascript">

	    //$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;
	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
	    //    } else {
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
	    //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
	    //        }
	    //    }
	    //});

	</script>