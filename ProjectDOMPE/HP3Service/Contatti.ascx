﻿<%@ Control Language="VB" ClassName="Contatti" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dmp.Helpers" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Register TagPrefix="HP3" TagName="Captcha" Src="~/HP3Common/ctlCaptcha.ascx" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _token As String = String.Empty
    Protected _langKey as LanguageIdentificator = Nothing
    Protected _langKeyId As Integer = 0
    Private captchaCode As String
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _token = Guid.NewGuid().ToString
        _langKey = Me.PageObjectGetLang
        _langKeyId = _langKey.Id 
    End Sub
    
    Protected Sub Page_Load()
        btnContactUs.Text = GetLabel("Invia")
        If Not Page.IsPostBack Then
            InitView()
            CaptchaInfo()
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Protected Sub InitView()
        
        hdnToken.Value = _token
        Session("token") = _token
        
    End Sub
    
    'Captcha system is used to protect the system by BOT automatic registration
    Sub CaptchaInfo()
        'CaptchaPanel.Visible = True
        'If Not Page.IsPostBack Then
        captchaCode = ImageUtility.GetCaptchaCode()
        IDCaptcha.Code = captchaCode
        HiddenCaptcha.Text = captchaCode
        'Else
        'IDCaptcha.Code = ViewState("CaptchaValue.Value")
        'End If
        IDCaptcha.Code = HiddenCaptcha.Text
        IDCaptcha.Width = "120"
        IDCaptcha.Height = "30"
        IDCaptcha.ForeColor = Drawing.Color.LightGray
        IDCaptcha.BackColor = Drawing.Color.White
        IDCaptcha.TextBackColor = Drawing.Color.DarkSlateGray
        IDCaptcha.TextForeColor = Drawing.Color.DarkSlateGray
    End Sub
    Function CheckCaptcha() As Boolean
        Dim result As Boolean = True
        success_txt.text = ""
        If (textCaptcha.Text <> ImageUtility.DecryptCaptchaCode(HiddenCaptcha.Text)) Then
            success_txt.Text = GetLabel("captcha_Error_")
            ', idLang, "The numeric code you have entered for the captcha protection is incorrect. Please retry."
            result = False
        End If
        
        Return result
    End Function
    
    Protected Sub btnContactUs_Click(ByVal sender As Object, ByVal ev As EventArgs)
        
        If CheckCaptcha() Then
            Dim sessionToken As String = Session("token")
            If hdnToken.Value.Equals(sessionToken) Then
                Session.Remove("token")
            
                Dim replacing As New Dictionary(Of String, String)
                replacing.Add("[NAME]", txtName.Text & " " & txtSurname.Text)
                replacing.Add("[TELEPHONE]", txtTel.Text)
                replacing.Add("[MESSAGE]", txtMessage.Text)
                replacing.Add("[EMAIL]", txtEmail.Text)
                replacing.Add("[FOOTER]", String.Empty)
            
                Dim htmlTemplate As String = Me.BusinessDictionaryManager.Read("DMP_htmlTemplate", _langKey.Id, "DMP_htmlTemplate")
            
                MailHelper.SendMessage(New MailTemplateIdentificator("ContactUs"), _langKey.Id, htmlTemplate, replacing, False)
            
                plhDone.Visible = True
                plhContactForm.Visible = False
            End If
        End If
        CaptchaInfo()
    End Sub
    
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id, label)
    End Function
</script>


<style>
	.contactCheck{width:auto;}
	.contactCheck input{width:auto;}
    
</style>
    <section>
        <div class="cont-img-top">
            <div class="bg-top" style="background-image: url('<%=String.Format("/{0}/_slice/contatti.jpg", _siteFolder)%>');">
                
            </div>
            <div class="txt">                    
                        <h1><%=GetLabel("Contatti")%></h2>                    
            </div>
            <div class="layer">&nbsp;</div>
        </div>
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    </section>
<section>
    <div class="container">
        <div class="colRight" style="border: 0px;">
        	<div class="content">
                <div class="generic">
    	<asp:PlaceHolder runat="server" ID="plhContactForm">
        
            <div class="form-container">
        	    <div class="left-form">
					<div class="generic">
                	    <span><%=GetLabel("Nome")%> <span class="red">*</span></span>
                        <asp:TextBox runat="server" CssClass="contactName" ID="txtName" TextMode="SingleLine" ValidationGroup="contactUs" TabIndex="1" />
                    </div>

                    <div class="generic">
                	    <span><%=GetLabel("Email")%> <span class="red">*</span></span>
                        <asp:TextBox runat="server" CssClass="contactEmail" ID="txtEmail" TextMode="SingleLine" ValidationGroup="contactUs" TabIndex="3" />
                    </div>
                                        
                    <div class="generic">
                	    <span><%=GetLabel("Messaggio")%> <span class="red">*</span><%--<span class="small">(<%=GetLabel("MaxXChar")%>)</span>--%></span>
                        <asp:TextBox runat="server" ID="txtMessage" CssClass="contactMessage" TextMode="MultiLine" ValidationGroup="contactUs" TabIndex="5" />
                    </div>
                </div>
                
        	    <div class="right-form">
                    <div class="generic">
                	    <span><%=GetLabel("Cognome")%> <span class="red">*</span></span>
                        <asp:TextBox runat="server" CssClass="contactSurname" ID="txtSurname" TextMode="SingleLine" ValidationGroup="contactUs" TabIndex="2" />
                    </div>

                    <div class="generic">
                	    <span><%=GetLabel("RecTel")%></span>
                        <asp:TextBox runat="server" ID="txtTel" TextMode="SingleLine" ValidationGroup="contactUs" TabIndex="4" />
                      </div>  
                        
                            
                                                                        
                   
                </div>
                <div class="generic marginBott20">
                    <p class="size14"><input type="checkbox" id="chk" class="contactCheck" style="width:auto;line-height:10px;margin:3px 3px 0;padding:0;float:left;height:auto;"  /><%=GetLabel("ContattiDisclaimer")%></p>
                </div>

                <div class="generic size14">
                       		<p style="padding-top:0px !important;"><span class="red">*</span><%=GetLabel("NoteCntct")%></p>
                    </div>

                <div class="right-form" style="margin:0px;">
                    <div class="generic">
                    <asp:placeholder ID="CaptchaPanel" runat="server">
                                <div class="captcha">
                                <span><%=GetLabel("Captcha")%> <span class="red">*</span></span>
                                 <br /><HP3:Captcha ID="IDCaptcha" runat="server" />
                                <asp:TextBox ID="textCaptcha" runat="server"  alt="Captcha code" ></asp:TextBox>  
                                                                
                                <asp:RequiredFieldValidator ID="ReqtextCaptcha" runat="server" ErrorMessage="*" ControlToValidate="textCaptcha" Font-Bold="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="HiddenCaptcha" Visible="false" runat="server" alt="Captcha code" />   
                                    <div style="clear:both;float:left;width:100%; color:Red"> 
                                    <asp:Literal  ID="success_txt" runat="server"   >
                                    </asp:Literal>
                                </div>                          
                                </div>
                            </asp:placeholder>
                        </div>
                    </div>

                <div class="generic size14">
                       	
                            <div class="btn-big"><asp:LinkButton  Text="Invia" runat="server" ID="btnContactUs" OnClick="btnContactUs_Click"  CausesValidation="true"  TabIndex="4"  /></div>
                </div>

            </div>
                  
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" ID="plhDone" Visible="false">
		<div style="text-align:center;padding: 30px 0;font-weight:bold;"><%=Me.BusinessDictionaryManager.Read("DMP_ContactMailSent", _langKey.Id, "DMP_ContactMailSent") %></div>
		</asp:PlaceHolder>
        <asp:HiddenField runat="server" ID="hdnToken" />

                    </div>
              </div>
          </div>
    </div>
</section>
<div id="dialog-privacy-contatti" style="font-size:12px;"><%=GetLabel("privacy123")%></div>

<script type="text/javascript">
    //popup privacy
     $(function () {
         $("#dialog-privacy-contatti").dialog({
             modal: true,
             autoOpen: false,
             draggable:false,
             resizable: false,
			 width: 700
         });
     });

     $("#opener-privacy-contatti").click(function (e) {
         e.preventDefault();
        $("#dialog-privacy-contatti").dialog("open");
    });
</script>

<script type="text/javascript">

    $(document).ready(function () {
        
        var button = $("#<%=btnContactUs.ClientID%>");
		var chk = $("#chk");
        var email = $(".form-container .contactEmail");
        var name = $(".form-container .contactName");
        var surname = $(".form-container .contactSurname");
        var message = $(".form-container .contactMessage");
        var captcha = $("#<%=textCaptcha.ClientID%>");
        
        var emailPattern = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

		var _outMessage = '';
		
		var _strCheck = '';
		var _strEmail = '';
		var _strEmail2 = '';
		var _strNome = '';
		var _strCognome = '';
		var _strMessage = '';
		var _strCaptcha = '';
		var _langId = '<%=_langKeyId%>';
		
		if ( _langId == 1 ) {
			_strCheck = '- L\'accettazione del disclaimer è obbligatoria.';
			_strEmail = '\n- Il campo Email è obbligatorio.';
			_strEmail2 = '\n- Controllare il valore immesso nel campo email.';
			_strNome = '\n- Il campo Nome è obbligatorio.';
			_strCognome = '\n- Il campo Cognome è obbligatorio.';
			_strMessage = '\n- Il campo Messaggio è obbligatorio.';
			_strCaptcha = '\n- Il campo Captcha è obbligatorio.';
		}
		if ( _langId == 2 ) {
			_strCheck = '- Disclaimer acceptance is mandatory.';
			_strEmail = '\n- Email is mandatory.';
			_strEmail2 = '\n- Please check Email format.';
			_strNome = '\n- Name is mandatory.';
			_strCognome = '\n- Surname is mandatory.';
			_strMessage = '\n- Message is mandatory';
			_strCaptcha = '\n- Captcha is mandatory';
		}
		if ( _langId == 3 ) {
		    _strCheck = '- Pranimi i përjashtimit nga përgjegjësia është i detyrueshëm.';
		    _strEmail = '\n- Elementi Email është i detyrueshëm.';
		    _strEmail2 = '\n- Kontrollo saktësinë e email-it të futur.';
		    _strNome = '\n- Elementi Emri është i detyrueshëm.';
		    _strCognome = '\n- Elementi Emri është i detyrueshëm.';
		    _strMessage = '\n- Elementi Mesazh është i detyrueshëm.';
		    _strCaptcha = '\n- Elementi Captcha është i detyrueshëm.';
		}		
		
		if (_langId == 4) {
		    _strCheck = '- Es obligatorio aceptar el descargo de responsabilidad.';
		    _strEmail = '\n- El campo Correo electrónico es obligatorio.';
		    _strEmail2 = '\n- Compruebe el dato indicado en el campo Correo electrónico.';
		    _strNome = '\n- El campo Nombre es obligatorio.';
		    _strCognome = '\n- El campo Nombre es obligatorio.';
		    _strMessage = '\n- El campo Mensaje es obligatorio.';
		    _strCaptcha = '\n- El campo Captcha es obligatorio.';
		}		
		
        button.click(function () {
			_outMessage = '';
            if (! chk.attr("checked") ) {
				_outMessage = _strCheck;
            }
			
            if ($.trim(email.val()) == '') {
                _outMessage += _strEmail;
            }
            
            if (!emailPattern.test(email.val()))
            {
                _outMessage += _strEmail2;
            }

            if ($.trim(name.val()) == '') {
                _outMessage += _strNome;
            }

            if ($.trim(surname.val()) == '') {
                _outMessage += _strCognome;
            }
            
            if ($.trim(message.val()) == '')
            {
                _outMessage += _strMessage;
            }

            if ($.trim(captcha.val()) == '') {
                _outMessage += _strCaptcha;
            }

			
			if (_outMessage != '') {
				alert(_outMessage);
				return false;
			}
        });
    });
</script>