﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="System.Text"%>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="System.Web.UI"%>
<%@ Import Namespace="Healthware.Dompe.Helper" %>


<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _currentThemeVal As ThemeValue = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = "/Dompe//ProjectDOMPE/_slice/imgCenter.jpg"
    Protected _TitlePage As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        
        Dim so As New ThemeSearcher()
        so.Key = Me.PageObjectGetTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        _currentThemeVal = Helper.GetTheme(so)
    End Sub
        
    
    Sub Page_Load()
        contentId = Me.PageObjectGetContent.Id
        Dim serchcont As New ContentExtraSearcher
        If Request("cache") = "false" Then CacheManager.RemoveByKey("cacheKey:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId)
        
        If contentId = 0 Then
            LoadUserList("cacheKey:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId)
            plhDetail.Visible = False
            plhContentGeneric.Visible = True
            LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        Else
            Dim path As String = String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder)
            LoadSubControl(path, plhDetail)
            plhDetail.Visible = True
            plhContentGeneric.Visible = False
        End If
    End Sub
    
    Sub LoadUserList(ByVal _cacheKey As String)
        Dim strManagementArchive As String = CacheManager.Read(_cacheKey, 1)
        If Not String.IsnullOrEmpty(strManagementArchive) Then
            'Response.write("cached")
            'mmgmArchive.Text = strManagementArchive
        Else
            'Response.write("NOT cached")
            Dim cntcoll As New ContentExtraCollection
            Dim cntsearch As New ContentExtraSearcher
            Dim cntVal As New ContentExtraValue
            cntsearch.key.IdLanguage = _langKey.Id
            cntsearch.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
            cntsearch.KeySiteArea.Name = Dompe.SiteAreaConstants.Management
            cntsearch.KeyContentType.Domain = Dompe.ThemeConstants.Domain.DMP
            cntsearch.KeyContentType.Name = Dompe.ContentTypeConstants.Name.Management
            cntsearch.SortType = ContentGenericComparer.SortType.ByData
            cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
            cntsearch.KeysToExclude = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent()
            cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)

            Dim ssb As StringBuilder = New StringBuilder()
            Dim iii As Integer = 1
			
            ssb.Append("<h2>" & Me.BusinessDictionaryManager.Read("DMP_Management", _langKey.Id, "DMP_Management") & "</h2>")
            If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
                For Each manager As ContentExtraValue In cntColl
                    ssb.Append("<div class='managementItem" & iii & "'>")
                    ssb.Append("<img src='/ProjectDOMPE/_slice/trasp.gif' width='142' height='164' alt='" & manager.Title & "' usemap='#managementItem" & iii & "' />")
                    ssb.Append("<h3>" & manager.Title & "</h3>")
                    ssb.Append("<map name='managementItem" & iii & "'>")
                    ssb.Append("<area shape='poly' coords='73,161,-1,123,0,39,70,-1,141,41,140,124' href='" & GetLink(manager) & "' alt='" & manager.Title & "'>")
                    ssb.Append("</map>")
                    ssb.Append("</div>")
                    iii += 1
                Next
                CacheManager.Insert(_cacheKey, ssb.ToString(), 1, 60)
                'mmgmArchive.Text = ssb.ToString()
                'rptUser.DataSource = cntcoll
                'rptUser.DataBind()
            End If
        End If
    End Sub
    
    Function GetContentPKbyID(ByVal id As Integer) As Integer
        Dim serchct As New ContentExtraSearcher
        Dim serchColl As ContentExtraCollection
        Dim PK As Integer = 0
        serchct.key.Id = id
        serchct.Properties.Add("Key.PrimaryKey")
        
        serchColl = Me.BusinessContentExtraManager.Read(serchct)
        
        If Not serchColl Is Nothing AndAlso serchColl.Count > 0 Then
            PK = serchColl(0).Key.PrimaryKey
        End If
        
        Return PK
    End Function
      
    
    
    
    'Function GetLink(ByVal keyContent As ContentIdentificator) As String
    '    Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    'End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
     
     
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function GetLink(ByVal keyContent As Integer) As String
           
        Return Me.BusinessMasterPageManager.GetLink(New ContentIdentificator(keyContent, _langKey.Id), _currentThemeVal, False)
        
    End Function
</script>

<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
    <section>
    <div class="cont-img-top">
        <div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/management.jpg');">
        </div><!--end brandSection-->     	    
                <div class="txt">
                    <h1>Management</h1>
                </div>
            <div class="layer">&nbsp;</div>         
     </div>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

    <section class="management-section">
    <div class="container">
    	<h1>Top Management</h1>
    	<div class="sep">&nbsp;</div>
        <div class="generic">
            <a href="<%=GetLink(7)%>" title="Sergio Dompé">
        	<div class="man-box big">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/sergio_dompe.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(7)%>" title="Sergio Dompé">Sergio Dompé</a>
                    <span>Chairman</span>
                </div>
        	</div><!--end man-box-->
            </a>
            <a href="<%=GetLink(6)%>" title="Eugenio Aringhieri">
                <div class="man-box big">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/eugenio_aringhieri.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(6)%>" title="Eugenio Aringhieri">Eugenio Aringhieri</a>
                    <span>Chief Executive Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
		</div>
    </div>
</section>    


<section class="management-section">
    <div class="container">
    	<h1>Management</h1>
    	<div class="sep">&nbsp;</div>
        <div class="generic">
            <a href="<%=GetLink(117)%>" title="Marcello Allegretti">
        	<div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/marcello_allegretti.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(117)%>" title="Marcello Allegretti">Marcello Allegretti</a>
                    <span>Chief Scientific Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
            <a href="<%=GetLink(660)%>" title="Amalia Crescenzio">
                <div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/amalia_crescenzio.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(660)%>" title="Amalia Crescenzio">Amalia Crescenzio</a>
                    <span>Chief Compliance Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
            <a href="<%=GetLink(113)%>" title="Giuseppe Andreano">
           <div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/giuseppe_andreano.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(113)%>" title="Giuseppe Andreano">Giuseppe Andreano</a>
                    <span>Chief Financial Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
            <a href="<%=GetLink(115)%>" title="Carmen Di Marino">
            <div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/dimarino.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(115)%>" title="Carmen Di Marino">Carmen Di Marino</a>
                    <span>Chief Legal Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
            <a href="<%=GetLink(661)%>" title="Paolo Patri">
            <div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/paolo_patri.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(661)%>" title="Paolo Patri">Paolo Patri</a>
                    <span>Chief Manufacturing Officer</span>
                </div>
        	</div><!--end man-box-->
                </a>
            <a href="<%=GetLink(546)%>" title="Davide Polimeni">
            <div class="man-box">
                <div class="imgs">
					<img src="/ProjectDOMPE/_slice/davide_polimeni.jpg" /> 
                </div>
                <div class="bottom">
                	<a href="<%=GetLink(546)%>" title="Davide Polimeni">Davide Polimeni</a>
                    <span>Head of Primary Care</span>
                </div>
        	</div><!--end man-box-->
                </a>
        </div>
    </div><!--end container--> 
</section>

</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">
&nbsp;
</asp:PlaceHolder>
