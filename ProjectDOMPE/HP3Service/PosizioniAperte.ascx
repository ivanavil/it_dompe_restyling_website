﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = "/Dompe/_slice/imgCenter.jpg"
    Dim _Last As Integer = 0
    Dim _maxItem As Integer = 8
    Dim strLancioThema As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _lang = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        
    End Sub
        
    
    Sub Page_Load()
        contentId = Me.PageObjectGetContent.Id
        ' DivImagesTop.Attributes("class") = Me.PageObjectGetLang.Code & "_" & Me.PageObjectGetSiteArea.Name
        Dim serchcont As New ContentExtraSearcher
        Try
            strLancioThema = Me.BusinessThemeManager.Read(_currentTheme, _lang).Launch
            If Not String.IsNullOrEmpty(strLancioThema) Then divThemelaunch.Visible = True
        Catch ex As Exception
            divThemelaunch.Visible = False
            strLancioThema = ""
        End Try
     
        If Not Page.IsPostBack Then
            LoadPosizioni()
        End If
        
        If contentId > 0 Then
            LoadSubControl(String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder), PhlDetail)
            PhlDetail.Visible = True
            plhContentGeneric.Visible = False
        Else
            PhlDetail.Visible = False
            plhContentGeneric.Visible = True
        End If
        
    End Sub
    
    Sub LoadPosizioni()
        Dim cntcoll As New ContentExtraCollection
        Dim cntsearch As New ContentExtraSearcher
        Dim cntVal As New ContentExtraValue
      
        cntsearch.KeySiteArea.Id = _siteArea.Id
        cntsearch.KeyContentType.Id = Dompe.ContentTypeConstants.Id.PosizioniAperteCtype
        cntsearch.SortType = ContentGenericComparer.SortType.ByData
        cntsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
        cntsearch.key.IdLanguage = _lang.Id
        cntsearch.Properties.Add("Key.Id")
        cntsearch.Properties.Add("Key.PrimaryKey")
        cntsearch.Properties.Add("Launch")
        cntsearch.Properties.Add("Title")
        cntsearch.Properties.Add("LinkLabel")
        cntsearch.Properties.Add("LinkUrl")
        cntsearch.Properties.Add("FullText")
        cntsearch.SetMaxRow = _maxItem
        cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
        
        If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
            rptSitePosizioni.DataSource = cntcoll
            rptSitePosizioni.DataBind()
        End If
    End Sub
    
  
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = contentId
      
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal vo As ContentExtraValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, _lang.Id), 135, title, title)
        Return res
    End Function
  
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _lang
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
 
    Function getPulsante(ByVal vo As ContentExtraValue) As String
        Dim strLink As String = ""
        If Not String.IsNullOrEmpty(vo.FullText) Then
            strLink = "<a href=" & GetLink(vo) & ">" & Me.BusinessDictionaryManager.Read("DMP_Dettaglio", _lang.Id, "DMP_Dettaglio") & "</a>"
        End If
        Return strLink
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _lang.Id, defaultLabel)
    End Function
    
    Sub itemdata(ByVal s As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim obj As ContentExtraValue = e.Item.DataItem
            If Not obj Is Nothing Then
                Dim _strUrl As String = String.Empty
                Dim _has2Buttons As Boolean = False
                If Not String.IsNullOrEmpty(obj.FullText.Trim) Then
                    _strUrl = GetLink(obj)
                    _has2Buttons = True
                End If
                Dim _ltlCover As Literal = e.Item.FindControl("ltlCover")
                Dim _ltlTitle As Literal = e.Item.FindControl("ltlTitle")
                Dim _clickCover As Literal = e.Item.FindControl("clickCover")
                Dim _clickTitle As Literal = e.Item.FindControl("clickTitle")
                Dim _ltlButtons As Literal = e.Item.FindControl("ltlButtons")

             If Not _has2Buttons Then 
					_strUrl = "mailto:" & obj.LinkUrl & "?subject=" & obj.LinkLabel
                End If
				
				_ltlCover.Text = _strUrl
                _ltlTitle.Text = _strUrl
                
                Dim traceScriptDetail As String = "recordOutboundLink('" & _strUrl & "','Detail Job','Open Position','" & obj.Title & "');"
                _ltlButtons.Text = "<a title='" & obj.Title & "' href='" & _strUrl & "' onclick=""" & traceScriptDetail & """>" & Me.BusinessDictionaryManager.Read("DMP_Dettaglio", _lang.Id, "DMP_Dettaglio") & "</a>"
                _clickCover.Text = "recordOutboundLink();"
                _clickTitle.Text = "recordOutboundLink();"
                Dim traceScriptMail1 As String = "recordOutboundLink('mailto:" & obj.LinkUrl & "?subject=" & obj.LinkLabel & "','Mail to','Send','" & obj.Title & "');"
               
				If Not _has2Buttons Then 
                    _ltlButtons.Text = String.Empty
                    _ltlButtons.Text = "<a title='" & obj.Title & "' href='" & _strUrl & "' onclick=""" & traceScriptMail1 & """>" & Me.BusinessDictionaryManager.Read("DMP_InviaMail", _lang.Id, "DMP_InviaMail") & "</a>"
                    _clickCover.Text = "recordOutboundLink(""mailto:" & obj.LinkUrl & "?subject=" & obj.LinkLabel & """,""Mail to"",""Send"",""" & obj.Title & """);"
                    _clickTitle.Text = "recordOutboundLink(""mailto:" & obj.LinkUrl & "?subject=" & obj.LinkLabel & """,""Mail to"",""Send"",""" & obj.Title & """);"
                Else
                    _ltlButtons.Text += "<a title='" & obj.Title & "' href='mailto:" & obj.LinkUrl & "?subject=" & obj.LinkLabel & "' onclick=""" & traceScriptMail1 & """>" & Me.BusinessDictionaryManager.Read("DMP_InviaMail", _lang.Id, "DMP_InviaMail") & "</a>"
				End If
            End If
        End If
    End Sub
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var sfMenu = $('.sf-menu').superfish({});
        $(".archiveCnt").hover(
			function () {
			    $(this).children('.effectHover').css('display', 'none');
			},
			function () {
			    $(this).children('.effectHover').css('display', 'block');
			}
		);
    });
</script>

<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
<div class="presentation">
<div class="presentationBox">
                <div class="presentationBox2">
                    <div class="presentationBox3">
                     	<div class="container">
        	              	<h2><%= Me.BusinessDictionaryManager.Read("PLY_TITLE_" & _currentTheme.Name, _lang.Id, "PLY_TITLE_" & _currentTheme.Name)%></h2>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="body">
    <div class="container">
       <div class="bodyBox">
       <div id="divThemelaunch" class="themeLaunch" runat="server" visible="false"><%= strLancioThema%></div>
     <asp:Repeater ID="rptSitePosizioni" runat="server" OnItemDataBound="itemData">
                <HeaderTemplate>
                <div class="archiveBox">
               </HeaderTemplate> 
                <ItemTemplate>
               <div class="archiveCnt">
                <div class="effectHover" style="display: block;"><%# DataBinder.Eval(Container.DataItem, "Title")%></div>
                  <div class="cover">
                  <a title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href='<asp:literal id="ltlCover" runat="server" />' onclick='<asp:literal id="clickCover" runat="server" />'> <%# GetImage(DataBinder.Eval(Container.DataItem, "Key.Id"), DataBinder.Eval(Container.DataItem, "Title"))%></a>
                  </div>
                   <div class="cnt">
                      <h3>
                      <a title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href='<asp:literal id="ltlTitle" runat="server" />' onclick='<asp:literal id="clickTitle" runat="server" />'><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                      </h3>
                      <p>
                      <%# DataBinder.Eval(Container.DataItem, "Launch")%>
                     </p>
                  </div>
                  <div class="tools">
                        <asp:literal id="ltlButtons" runat="server" />
                                   
                 </div>
               </div>
               </ItemTemplate>
             <FooterTemplate></div></FooterTemplate>
     </asp:Repeater>
     </div>
    </div>
</div>
</asp:PlaceHolder> 
 <asp:PlaceHolder ID="PhlDetail" runat="server" Visible="false">&nbsp;</asp:PlaceHolder>
