﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Protected _TitlePage As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
        
    
    Sub Page_Load()
        contentId = Me.PageObjectGetContent.Id
     
        Dim serchcont As New ContentExtraSearcher
        If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            'se il content non è quello associato al tema allora carico il dettaglio
            If Not IsPostBack() Then
                LoadSubControl(String.Format("/{0}/HP3Common/BoxNews.ascx", _siteFolder), Phlnews)
                If Dompe.ContentBoxImage.IdContentList.Contains("{" & Me.PageObjectGetContent.Id.ToString & "}") Then
                    LoadSubControl(String.Format("/{0}/HP3Common/BoxImage.ascx", _siteFolder), PhlBoxImage)
                End If
                LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
                LoadSubControl(String.Format("/{0}/HP3Common/BoxLinks.ascx", _siteFolder), PhlLinks)
            
            End If
            
            contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
           
            If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                _TitlePage = contentextraVal.Title
                testo_title.Text = contentextraVal.Title
                testo_sezione.Text = contentextraVal.FullText
                testo_subtitle.Text = contentextraVal.Abstract
                testo_optional.Text = contentextraVal.FullTextOriginal
         
            End If
            plhContentGeneric.Visible = False
            plhDetail.Visible = True
        Else
            LoadMenu()
            plhContentGeneric.Visible = True
            plhDetail.Visible = False
        End If
     
    End Sub
    
    Sub LoadMenu()
        Dim cntcoll As New ContentExtraCollection
        Dim cntsearch As New ContentExtraSearcher
        Dim cntVal As New ContentExtraValue
      
        cntsearch.key.IdLanguage = _langKey.Id
        cntsearch.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
        cntsearch.KeySiteArea.Name = Me.PageObjectGetSiteArea.Name
        cntsearch.KeyContentType.Domain = Dompe.ThemeConstants.Domain.DMP
        cntsearch.KeyContentType.Name = Dompe.ContentTypeConstants.Name.Product
        cntsearch.SortType = ContentGenericComparer.SortType.ByData
        cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
        'cntsearch.KeysToExclude = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent()
     
        cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
        
        If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
            rptSiteMenu.DataSource = cntcoll
            rptSiteMenu.DataBind()
        End If
      
    End Sub
    
    Function GetContentPKbyID(ByVal id As Integer) As Integer
        Dim serchct As New ContentExtraSearcher
        Dim serchColl As ContentExtraCollection
        Dim PK As Integer = 0
        serchct.key.Id = id
        'serchct.Display = SelectOperation.Enabled
        'serchct.Active = SelectOperation.Enabled
        'serchct.Delete = SelectOperation.Disabled
        serchct.Properties.Add("Key.PrimaryKey")
        
        serchColl = Me.BusinessContentExtraManager.Read(serchct)
        
        If Not serchColl Is Nothing AndAlso serchColl.Count > 0 Then
            PK = serchColl(0).Key.PrimaryKey
        End If
        
        Return PK
    End Function
      
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = contentId
      
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentExtraValue) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent.Key, GetTheme(), False)
    End Function
    
      
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As Integer, ByVal imgNum As Integer) As String
        Dim res As String = ""
        Dim qual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        qual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.Medium
        res = Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, _langKey.Id), 380, 525, qual, imgNum)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        var sfMenu = $('.sf-menu').superfish({});

        $(".terapHome").hover(
			function () {
			    $(this).addClass('terapHomeOn');

			},
			function () {
			    $(this).removeClass('terapHomeOn');
			}

		);
    });
</script>
<div class="presentation">
         <div class="presentationBox">
                <div class="presentationBox2">
                    <div class="presentationBox3">
    	                <div class="container">
        	             	<h2><%= Me.BusinessDictionaryManager.Read("SoluzioniTeraupetiche_" & _currentTheme.Id, _langKey.Id, "SoluzioniTeraupetiche_" & _currentTheme.Id)%></h2>
                        </div>
                   </div>
              </div>
        </div>
</div>

<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
     <asp:Repeater ID="rptSiteMenu" runat="server">
                <HeaderTemplate>
                </HeaderTemplate> 
                <ItemTemplate>
                <div class="terapHome" <%# iif(DataBinder.Eval(Container, "ItemIndex", "")= 0,"style=""margin: 30px 0 0 0;""","")%>>
                  <div class="container">
                 	<div class="terapHomeBox">
                    <div class="imgMalattiaCont">
                   <img alt="<%# DataBinder.Eval(Container.DataItem, "Title")%> bn" src="/HP3Image/Cover/<%# GetImage(DataBinder.Eval(Container.DataItem, "Key.Id"), 1)%>" />
                    </div>
                     <div class="imgMalattiaCont1">
                      <img alt="<%# DataBinder.Eval(Container.DataItem, "Title")%> color" src="/HP3Image/Cover/<%# GetImage(DataBinder.Eval(Container.DataItem, "Key.Id"), 0)%>" />
                   </div>
                   <div class="malattia">
                   <div class="searchBox1">
                   <h3 class="malattiaTitle"><%# DataBinder.Eval(Container.DataItem, "Title")%></h3>
                   </div>
                   <div class="malattiaTitleBox"><p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p></div>
                 <div class="malattiaInfoBox">
                 <div class="imgs">&nbsp;</div>
                 <div class="infos"> <%# DataBinder.Eval(Container.DataItem, "FullTextComment")%></div>
                 </div>
                <%--  <a href="<%# GetLink(Container.DataItem) %>"><%= Me.BusinessDictionaryManager.Read("PLY_goto", _langKey.Id, "PLY_goto")%></a>--%>
                   </div>
                     </div>
                 </div>
             </div>
              </ItemTemplate>
             <FooterTemplate></FooterTemplate>
     </asp:Repeater>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">
<div class="body">
  <div class="container">
       <div class="bodyBox">
        <asp:Literal ID="testo_optional" runat="server" />
          <div class="leftBody">
            <div class="content">
            <h3><asp:Literal ID="testo_title" runat="server" /></h3>
              <h4><asp:Literal ID="testo_subtitle" runat="server" /></h4>
              <asp:Literal ID="testo_sezione" runat="server" />
              </div>
          </div>
        <div class="rightBody">
            <asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
            <asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
            <div class="rightBodyRow">
              <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            </div>
        </div>
        </div>
   </div>
</div>
</asp:PlaceHolder>
