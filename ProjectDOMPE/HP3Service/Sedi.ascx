﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private _idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cittasel As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyDetail As String = String.Empty
    Protected _themeDescription As String = String.Empty
	
	Dim _strLinkMilan As String = String.Empty
	Dim _strLinkNapoli As String = String.Empty
	Dim _strLinkLaquila As String = String.Empty
    Dim _strLinkNy As String = String.Empty
    Dim _strLinkTirana As String = String.Empty
    Dim arrSedi As New ArrayList
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        _idContent = Me.PageObjectGetContent.Id
    End Sub
        
    Dim _canCheckApproval As Boolean = False
    Protected Sub Page_Load()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        InitView()
        
        If _langKey.Id = 1 Then
            _strLinkMilan = "/sedi/53_la-sede-di-milano.html"
            _strLinkNapoli = "/sedi/55_il-centro-ricerche-di-napoli.html"
            _strLinkLaquila = "/sedi/54_il-polo-dellaquila.html"
            _strLinkNy = "/sedi/139_la-sede-di-new-york.html"
            _strLinkTirana = "/sedes/515_la-sede-de-tirana.html"
		
        ElseIf _langKey.Id = 2 Then
            _strLinkMilan = "/offices/53_milan-headquarters.html"
            _strLinkNapoli = "/offices/55_the-research-centre-in-naples.html"
            _strLinkLaquila = "/offices/54_the-production-site-in-laquila.html"
            _strLinkNy = "/offices/139_new-york-office.html"
            _strLinkTirana = "/offices/515_tirana-offices.html"
            
        ElseIf _langKey.Id = 3 Then
            _strLinkMilan = "/seli/53_selia-ne-milano.html"
            _strLinkNapoli = "/seli/55_qendra-kerkimore-ne-napoli.html"
            _strLinkLaquila = "/seli/54_qendra-ne-laquila.html"
            _strLinkNy = "/seli/139_selia-ne-new-york.html"
            _strLinkTirana = "/seli/515_selia-e-tiranes.html"
            
        ElseIf _langKey.Id = 4 Then
            _strLinkMilan = "/sedes/53_la-sede-de-milan.html"
            _strLinkNapoli = "/sedes/54_el-polo-de-laquila.html"
            _strLinkLaquila = "/sedes/55_el-centro-de-napoles.html"
            _strLinkNy = "/sedes/139_la-sede-de-nueva-york.html"
            _strLinkTirana = "/sedes/515_la-sede-de-tirana.html"
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub InitView()
        Dim so As New ThemeSearcher()
        so.Key = _currentTheme
        so.LoadHasSon = False
        so.LoadRoles = False
        so.KeyLanguage=_langKey
        so.Properties.Add("Description")
        
        Dim theme = Helper.GetTheme(so)
        If Not theme Is Nothing Then _themeDescription = theme.Description
        _cacheKey = "cacheKey:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id
        If Request("cache") = "false" or _canCheckApproval Then CacheManager.RemoveByKey(_cacheKey)
        

        Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
        If Not String.IsNullOrEmpty(_strCachedContent) Then
            
            'ltlSedi.Text = _strCachedContent
        Else
            
            'ltlSedi.Text = loadSedi()
        End If
        
        'If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
        '    Dim path As String = String.Format("/{0}/HP3Service/DetailSedi.ascx", _siteFolder)
        '    LoadSubControl(path, phlSedeDetail)
        '    phlSedeDetail.Visible = True
        '    plhSedi.Visible = False
        'End If
        'If Not IsPostBack() Then LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
    End Sub
    
    Function loadSedi() As String
        Dim so As New ContentExtraSearcher
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = _siteArea.Id
        so.SortOrder = ContentGenericComparer.SortOrder.ASC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.key.IdLanguage = _langKey.Id
        If Not _canCheckApproval Then
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
        Else
            Me.BusinessContentExtraManager.Cache = False
        End If
     
        Dim coll = Me.BusinessContentExtraManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Dim _sb As StringBuilder = New StringBuilder()
            Dim i As Integer = 1
            Dim _urlSede As String = String.Empty
            Dim _labelDettaglio As String = Me.BusinessDictionaryManager.Read("DMP_Dettaglio", _langKey.Id, "DMP_Dettaglio")
            Dim coords As Array = Nothing
            
			
            _sb.Append("<div class='sede visualmaps'><strong><a href='#' class='lnkSede' rel='all'>" & Me.BusinessDictionaryManager.Read("VisMapCompl", _langKey.Id, "VISUALIZZA LA MAPPA COMPLETA").ToUpper() & "</a></strong></div><ul>")
            For Each item As ContentExtraValue In coll
                _urlSede = GetLink(item.Key)
                
                _sb.Append("<li><a href='javascript:void(0);' id=""" & item.Key.Id & """ onclick='openDetail(this)' class='lnkSede' rel='" & item.SubTitle.ToLower().Replace("'", "_") & "'>" & item.SubTitle & "</a><br />")
                
                Dim txtTel As String = item.BookReferer
                If txtTel.Contains("|") Then
                    Dim listItems = (txtTel).Split("|")
                    Dim textDisplay = ""
                    Dim label = ""
                    Dim strEmail = ""
                    txtTel = ""
                    For i = 0 To listItems.Length - 1
                        Dim listEmail = (listItems(i)).Split(":")
                        label = listEmail(0)
                        strEmail = listEmail(1)
                        txtTel += "<br>" + label + ":<br /><a href='mailto:" + strEmail + "'>" + strEmail + "</a>"
                    Next        
                    txtTel = "<span class='sede_email'>" + txtTel + "</span>"
                    'txtTel = ""
                End If
                
                _sb.Append("<span id=""detail_" & item.Key.Id & """ class='detail_sede closed'>" & item.FullTextOriginal & " " & item.FullTextComment & "<br/>" & txtTel & "</span></li>")
					
                coords = item.Abstract.Split("|")
                
                
                arrSedi.Add("{ key: """ & item.SubTitle.ToLower().Replace("'", "_") & """, value: [""" & item.SubTitle & """, """ & item.TitleOriginal & """, """ & coords(0) & """, """ & coords(coords.Length - 1) & """, """", """", """ & item.Copyright & """, """ & item.FullTextOriginal & " " & item.FullTextComment & """, """"] }")
                
                _urlSede = String.Empty
				      
            Next
      
            _sb.Append("</ul>")
			
            CacheManager.Insert(_cacheKey, _sb.ToString(), 1, 60)
            Return _sb.ToString()
        End If
		
        Return String.Empty
    End Function
    
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)

    End Function
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
         
    Function GetTheme(keycontent As ContentIdentificator) As ThemeValue
        Dim so As New ThemeSearcher
        'so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        so.KeyContent = keycontent
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
    Function GetImageSede(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 186, title, title)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
</script>

<style type="text/css">
    .opened
    {
        display:block;
    }
    .closed
    {
        display:none;
    }
    span.detail_sede
    {
        font-family: 'CooperHewitt-Medium';
        padding-left:40px;
        color: #1a181b;
        font-size: 15px;
        width:209px !important;
    }
    .sede_email
    {
        font-size:13px !important;
        line-height:18px !important;
    }
    .sede_email a
    {
        padding-left:0 !important;
        font-size:13px !important;
    } 
</style>

<section>
    <div class="cont-img-top">
        <div class="bg-top" style="background-image: url('<%=String.Format("/{0}/_slice/sedi.jpg", _siteFolder)%>');">                                       
        </div>
        <div class="txt">
                    <h1><%=_themeDescription %></h1>
        </div> 
        <div class="layer">&nbsp;</div>
    </div>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>


<section>
    <div class="container">
        <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--%>
        <script  src="https://maps.google.com/maps/api/js?sensor=false&language=en"  type="text/javascript"></script> 
        

        <%--<ul id="ulPosizione">
          <li><a href="#" rel="all">All</a></li>
          <li><a href="#" rel="milan">Milano</a></li>
          <li><a href="#" rel="aquila">L'Aquila</a></li>
          <li><a href="#" rel="napoli">Napoli</a></li>
        </ul>--%>
         <div class="colLeft">
             <div class="sediSide">
                <%=loadSedi()%>
              </div>
         </div>
        <div class="colRight">
             <div class="content">
                    <div style="width:805px; height:520px; overflow:hidden;  background-color:#f4f4f4; " id="map" ></div>
              </div>
        </div>
    </div>
</section>


    <asp:literal id="ltlSedi" runat="server" EnableViewState="false" />



<script type="text/javascript">

    
    <% 
    Dim cont As Integer = 1
    Dim strMap = ""
    Dim str As String = ""
    For Each str In arrSedi
   
        strMap += str & IIf(cont < arrSedi.Count, ", ", "")
           
    Next
    %>

    var myvar = [<%=strMap%>];

    //var myvar = [{ key: "milano", value: ["value1", "45.465454", "9.186515999999983"] }, { key: "aquila", value: ["value2", "42.3571186", "13.350809000000027"] }, { key: "napoli", value: ["value3", "40.8545925", "14.224773700000014"] }];


    function openDetail(obj)
    {
        var spanDetail = $(obj).attr("id");
        
        
        $(".detail_sede").removeClass("opened").addClass("closed");

        if ($("#detail_" + spanDetail).hasClass("opened")) {
            $("#detail_" + spanDetail).removeClass("opened").addClass("closed");
        } else {
            $("#detail_" + spanDetail).removeClass("closed").addClass("opened");
        }
        
    }
</script>

<script  src="/ProjectDOMPE/_js/map.js"  type="text/javascript"></script> 

