﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Private _isMobile as boolean = false
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        contentId = Me.PageObjectGetContent.Id
    End Sub
	
	Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
             if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
                 userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
                Return true
            End If
        End if
            
        Return False
    End Function
    
    Sub Page_Load()
		_isMobile = IsMobileDevice()
        If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            idContentTheme = valthem.KeyContent.Id
          
           ' verifico se il content è quello associato al tema allora carico il contenuto generico
            If contentId = idContentTheme Then
                Dim src As New ContentExtraSearcher
                Dim sCol As New ContentExtraCollection
                
                src.KeySiteArea.Id = _siteArea.Id
                src.KeyContentType.Id = Me.PageObjectGetContentType.Id
                src.key.IdLanguage = _langKey.Id
                src.key.Id = contentId
                sCol = Me.BusinessContentExtraManager.Read(src)
                
             
                If Not sCol Is Nothing AndAlso sCol.Count > 0 Then
                    titolo_sezione.Text = sCol(0).Title
                    testo_sezione.Text = sCol(0).FullText
                  
                    testo_sezione_under.Text = sCol(0).FullTextOriginal
                    
                End If
             
                plhContentGeneric.Visible = True           
          
            
            End If
                               
            ' Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
            '  LoadSubControl(pathPressMenu, PHL_Press_Menu)
            
       
        End If
        Dim so As New ThemeSearcher
        Dim coll As ThemeCollection = Nothing
        so.KeySite = Me.PageObjectGetSite
        so.KeyFather.Id = Dompe.ThemeConstants.idAreaPress
        so.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        coll = Me.BusinessThemeManager.Read(so)
           
        rptSiteMenu.DataSource = coll
        rptSiteMenu.DataBind()
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    

    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
  
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
    
</script>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">

    <section>
        <div class="cont-img-top">
    	    <div class="bg-top pressContattiSection">            
            </div>    
            <div class="txt">
               <h1 class="white"><asp:Literal ID="titolo_sezione" runat="server" /></h1>       
            </div>
            <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    </section>


    <section>
 	
    <div class="container">
        <div class="colLeft">
        	
            	<div class="listSide">
                	<asp:Repeater ID="rptSiteMenu" runat="server">
                    <HeaderTemplate><ul></HeaderTemplate> 
                    <ItemTemplate>
                        <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# DataBinder.Eval(Container.DataItem, "Description")%></a></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>  

                </div><!--end listSide-->            
            
        </div><!--end colLeft-->

        <div class="colRight">
             <div class="content">
                <asp:Literal ID="testo_sezione" runat="server" /> 
       
                 <div class="colAllFixed">

                     <asp:Literal ID="testo_sezione_under" runat="server" /> 
        	
                </div><!--end colRight-->
            </div>
        </div>
    </div><!--end container-->
        
   
    </section><!--end body-->

	<script type="text/javascript">
		<%' If Not _isMobile Then %>
		//	$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
			
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	   <%' End If %>
	</script>


</asp:PlaceHolder>