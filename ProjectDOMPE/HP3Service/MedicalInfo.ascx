﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
	Private _isMobile as boolean = false
	Dim _finalBrandDetailText As String = String.Empty

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
    End Sub
	
	Public Function IsMobileDevice() As Boolean
		Dim userAgent = Request.UserAgent.ToString().ToLower()
		If Not String.IsNullOrEmpty(userAgent) Then
			if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
			userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
				Return true
			End If
		End if
		Return False
	End Function	
        
    Dim _canCheckApproval as Boolean = false
	
    Sub Page_Load()
		_isMobile = IsMobileDevice()
		If Me.ObjectTicket.Roles.HasRole(79) Then _canCheckApproval = True
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        
        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/MenuAreaMedico.ascx", _siteFolder)
        LoadSubControl(pathPressMenu, PHL_Med_Menu)
        
        Dim pathRichiediArticolo As String = String.Format("/{0}/HP3Common/BoxRichiediArticolo.ascx", _siteFolder)
        LoadSubControl(pathRichiediArticolo, phRichiediArticolo)
		
		_cacheKey = "_cacheAreaMedicoDettaglioInfoUtili:" & _langKey.id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & Me.PageObjectGetContent.Id
		If Request("cache") = "false" OR _canCheckApproval Then
			CacheManager.RemoveByKey(_cacheKey)
		End If
		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_finalBrandDetailText = _strCachedContent
		Else		
			Dim so As New ThemeSearcher()
			so.Key = Me.PageObjectGetTheme
			so.KeyLanguage.Id = _langId
			so.LoadHasSon = False
			so.LoadRoles = False
			so.Properties.Add("Description")
		 
			Dim coll = Me.BusinessThemeManager.Read(so)
		 
			If Not coll Is Nothing AndAlso coll.Any() Then
				titolo_sezione.Text = coll(0).Description
			End If
		  
			Dim cntSrc As New ContentExtraSearcher
			Dim cntcoll As New ContentExtraCollection
			Dim cntsearch As New ContentExtraSearcher
			Dim cntVal As New ContentExtraValue
			cntsearch.key.IdLanguage = 1'_langKey.Id
			'cntsearch.key.Id = Me.PageObjectGetContent.Id
			cntsearch.KeySite = Me.PageObjectGetSite
			cntsearch.KeySiteArea.Id = _siteArea.Id
			If Not _canCheckApproval Then cntsearch.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
			cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
			cntsearch.SortType = ContentGenericComparer.SortType.ByData
			cntsearch.Properties.Add("Title")
            cntsearch.Properties.Add("FullText")
            cntsearch.Properties.Add("FullTextOriginal")
			cntsearch.Properties.Add("ApprovalStatus")
			cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
			
			Dim _sb as New StringBuilder()
			
			If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
				For Each ocExtra as ContentExtraValue in cntcoll
					_sb.Append("<div class='accCont'>")
					_sb.Append("<div class='singleAcc' val='c'>")
					_sb.Append("<div class='bgAccLink'><div id='imgs' class='closeAcc'>close</div><h3>"  & ocExtra.Title & checkApproval(ocExtra.ApprovalStatus) & "</h3></div>")
                    _sb.Append("<div class='infoAcc links' style='display: none;'>")
					_sb.Append(getRelatedLink(ocExtra.Key.Id))
                    '_sb.Append("<div class='leftInfoUtili'>")
                    '_sb.Append(ocExtra.FullText)
                    '_sb.Append("</div>")
                    '_sb.Append("<div class='rightInfoUtili'>")
					'_sb.Append(ocExtra.FullTextOriginal)
					'_sb.Append("</div>")
					_sb.Append("</div>")					
					_sb.Append("</div>")
					_sb.Append("</div>")
				Next
				
				_finalBrandDetailText = _sb.ToString() 'cntcoll(0).FullText
				
				'Cache Content and send to the oputput-------
				CacheManager.Insert(_cacheKey, _finalBrandDetailText, 1, 120)
				'--------------------------------------------
			End If
		End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
		fulltext.Text = _finalBrandDetailText
    End Sub
	
	Function getRelatedLink(ByVal intContentId as Integer) as String
		Dim _sbLeft as New StringBuilder
		Dim _sbRight as New StringBuilder
		Dim strLinkItem as String = "<div class='singleLink'><h4>{0}</h4><p>{1}</p><div style='display: none;' class='vai'><a title='{2}' href='{3}' path='{4}'>vai al link</a></div></div>"
		
		Dim oColl as contentCollection = me.businesscontentmanager.ReadContentRelation(New ContentIdentificator(intContentId, 1), New ContentRelationTypeIdentificator(1))
		If Not oColl Is Nothing AndAlso oColl.Any Then
			Dim _i as Integer = 1
			For Each oCon as contentValue in oColl
				If (_i Mod 2) > 0 Then
					_sbLeft.AppendFormat(strLinkItem, oCon.Title, oCon.Launch, oCon.Title, oCon.LinkUrl, oCon.LinkUrl)
				Else
					_sbRight.AppendFormat(strLinkItem, oCon.Title, oCon.Launch, oCon.Title, oCon.LinkUrl, oCon.LinkUrl)
				End If
				_i += 1
			Next
			
			Return "<div class='leftInfoUtili'>" & _sbLeft.ToString() & "</div>" & "<div class='rightInfoUtili'>" & _sbRight.ToString() & "</div>"
		Else
			Return "<div>Non ci sono link utili disponibili.</div>"
		End If

		Return String.Empty
	End Function
	
	Function checkApproval(ByVal approvalStatus as integer) as String
	If approvalStatus > 1 Then Return "<span style='color:red;font-weight:bold;'> - NON APPROVATO</span>"
	
	Return String.Empty
	End Function

    Function GetLink(ByVal vo As ContentValue) As String

        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
</script>


<section>
    <div class="cont-img-top">
        <div class="bg-top brandSection">
        </div><!--end brandSection-->  
    	
            <div class="txt">
                <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>
            </div>
        <div class="layer">&nbsp;</div> 
    
        </div>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
  </section>
<section>
	<div class="container">
        <div class="colLeft">
        	
            	<div class="listSide">
                   <asp:PlaceHolder ID="PHL_Med_Menu" runat="server" Visible="true" />
                </div>    
            
        </div><!--end colLeft-->

         <div class="colRight"> 
             <div class="content">               
			    <asp:literal id="fulltext" runat="server" EnableViewState="False" />    
                <asp:PlaceHolder ID="phRichiediArticolo" runat="server" Visible="true" />
             </div>
        </div><!--end colRight-->
        
    </div><!--end container-->
 </section>

<style>
.vai {float:right !important;position:relative !important;right:2px !important;}
</style>		
	
    <script type="text/javascript">
        $(".singleLink").mouseover(function () {
            $(this).find("h4").css("color", "#e20025");
            //$(this).find("p").css("display", "none");
            $(this).find(".vai").css("display", "block")
        })
		.mouseout(function () {
		    $(this).find("h4").css("color", "#939291");
		    $(this).find("p").css("display", "block");
		    $(this).find(".vai").css("display", "none")
		});
		
		$(".singleLink").click(function() {
			ExternalLinkPopupen($(this).find("a").attr("path"));
		});
	</script>
    
    <script type="text/javascript">
        $(".bgAccLink").click(function () {
            var valore = $(this).parent().attr("val");
            if (valore == "c" || valore == undefined) {
                $(this).parent().attr("val", "o");
                $(this).next().css("display", "block");
                $(this).find("#imgs").removeClass("closeAcc").addClass("openAcc");
            } else {
                $(this).parent().attr("val", "c");
                $(this).next().css("display", "none");
                $(this).find("#imgs").removeClass("openAcc").addClass("closeAcc");
            }
        });
	</script>
	<script type="text/javascript">
		<%' If Not _isMobile Then %>
		//$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	   <%' End If %>
	</script>	