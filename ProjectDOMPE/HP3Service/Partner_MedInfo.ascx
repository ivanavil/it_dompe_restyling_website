﻿<%@ Control  Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Linq"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.XML"%>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="Healthware.Dmp.Helpers" %>
<script runat="server">
 Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0

	Dim strQuery As String = "readPartners"
	Dim _objCachedDataSet As DataSet = Nothing
	Dim oDt as DataTable = Nothing	
	Dim intUserId as INteger = 0
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
    End Sub
	
	Function LoadPartner() As DataTable
		Dim _cacheKey as String = "partnerDataSet"
		Dim oDt as DataTable = Nothing
		If request("cache") = "false" Then cacheManager.RemoveByKey(_cacheKey)
		
		_objCachedDataSet = cacheManager.Read(_cacheKey, 1)
		If _objCachedDataSet Is Nothing Then
			If Not String.IsNullOrEmpty(strQuery) Then
				Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQuery, False)
			
				If Not String.IsNullOrEmpty(query) Then
					Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, Nothing) 'Al posto di Nothing vanno  passati gli SqlParams
					If Not ds Is Nothing Then
						_objCachedDataSet = ds
						'Cache Content and send to the oputput-------
						CacheManager.Insert(_cacheKey, _objCachedDataSet, 1, 120)
						'--------------------------------------------
					Else
						Return Nothing
					End If
				End If
			End If
		End If	

		Return  _objCachedDataSet.Tables(0)
	End Function

	
	Sub QueryPartner()
		ddlPartner.Items.Clear()
		ddlCountry.Items.Clear()
		ddlProduct.Items.Clear()
		ddlTherapeutic.Items.Clear()
		
		ddlPartner.Enabled = False
		ddlCountry.Enabled = False
		ddlProduct.Enabled = False
		ddlTherapeutic.Enabled = False
			
		If intUserId > 0 Then
			If Not oDt Is Nothing AndAlso oDt.Rows.Count > 0 Then
				Dim partners = (From row In oDt Where row("partner_userId") = intUserId
				Select row.Item("partner_directpartner")).Distinct()
				'.ToArray()
				If Not partners Is NOthing AndAlso partners.Any Then
					For Each i In partners
						ddlPartner.Items.Add(new listitem(i, i))
					Next

					If partners.Count = 1 Then
						loadCountries(Nothing, Nothing)
					Else
						ddlPartner.Enabled = True
						ddlPartner.Items.INsert(0, new listitem("Please select a partner", ""))
						ddlPartner.SelectedIndex = 0
					End If
				End If
			End If
		Else
			'Redirect		
		End If
	End Sub
	
	Sub loadCountries(sender as object, e as eventargs)
		ddlCountry.Items.Clear()
		ddlProduct.Items.Clear()
		ddlTherapeutic.Items.Clear()
		
		ddlCountry.Enabled = False
		ddlProduct.Enabled = False
		ddlTherapeutic.Enabled = False		

		If intUserId > 0 Then
			If Not oDt Is Nothing AndAlso oDt.Rows.Count > 0 Then
				Dim countries = (From row In oDt Where row("partner_userid") = intUserId And row("partner_directPartner") = ddlPartner.SelectedItem.value
				Select row.Item("partner_Country")).Distinct()
				If Not countries Is NOthing AndAlso countries.Any Then
					For Each i In countries
						ddlCountry.Items.Add(new listitem(i, i))
					Next
					If countries.Count = 1 Then
						LoadProduct(Nothing, Nothing)
					Else
						ddlCountry.Enabled = True
						ddlCountry.Items.INsert(0, new listitem("Please select a country", ""))
						ddlCountry.SelectedIndex = 0
					End If
				End If
			End If		
		End If
	End Sub
	

	Sub LoadProduct(sender as object, e as eventargs)
		ddlProduct.Items.Clear()
		ddlTherapeutic.Items.Clear()
		
		ddlProduct.Enabled = False
		ddlTherapeutic.Enabled = False

		If intUserId > 0 Then
			If Not oDt Is Nothing AndAlso oDt.Rows.Count > 0 Then
				Dim products = (From row In oDt Where row("partner_userid") = intUserId And row("partner_directPartner") = ddlPartner.SelectedItem.value And row("partner_country") = ddlCountry.SelectedItem.Value
				Select row.Item("partner_brand")).Distinct()			
				If Not products Is NOthing AndAlso products.Any Then
					For Each i In products
						ddlProduct.Items.Add(new listitem(i, i))
					Next
					If products.Count = 1 Then
						LoadTherapeutic(Nothing, Nothing)
					Else
						ddlProduct.Enabled = True
						ddlProduct.Items.INsert(0, new listitem("Please select a product", ""))
						ddlProduct.SelectedIndex = 0
					End If
				End If
			End If		
		End If	
	End Sub
	
	
	Sub LoadTherapeutic(sender as object, e as eventargs)
		ddlTherapeutic.Items.Clear()
		ddlTherapeutic.Enabled = False
		
		If intUserId > 0 Then
			If Not oDt Is Nothing AndAlso oDt.Rows.Count > 0 Then
				Dim ats = (From row In oDt Where row("partner_userid") = intUserId And row("partner_directPartner") = ddlPartner.SelectedItem.value And row("partner_country") = ddlCountry.SelectedItem.Value And row("partner_brand") = ddlProduct.SelectedItem.Value
				Select row.Item("partner_areaterapeutica")).Distinct()			
				If Not ats Is Nothing AndAlso ats.Any Then
					For Each i In ats
						ddlTherapeutic.Items.Add(new listitem(i, i))
					Next
					If ats.Count > 1 Then
						ddlTherapeutic.Enabled = True
						ddlTherapeutic.Items.INsert(0, new listitem("Please select a theurapeutic", ""))
						ddlTherapeutic.SelectedIndex = 0
					End If
				End If
			End If		
		End If			
	
	End Sub		

	
	Sub preLoadForm()
		If intUserId > 0 Then
			Dim oUsval as Healthware.HP3.Core.User.ObjectValues.uservalue = Me.BusinessUserManager.Read(Me.Objectticket.User.Key)
			If Not oUsval is Nothing AndAlso oUsval.Key.id > 0 Then
				txbMailAddress.Text = oUsval.Email.ToString()
				txbPhoneNumber.Text = "+39 0258383375" 'oUsval.Telephone(0).ToString()
				txbInsertDate.Text = Datetime.Now.ToString()
				txbInsertDate.Enabled = False
			End If
		End If
	End Sub
	
    
    Sub Page_Load()
		intUserId = Me.objectTicket.User.Key.Id
		oDt = LoadPartner()
		
		If Not Page.IsPostBack Then 			
			'Filter List by PartnerId--------------------
			QueryPartner()
			'--------------------------------------------
			
			preloadForm()
		End If
	
		'Load Menu Sx--------------------------------
        Dim soT As New ThemeSearcher
        Dim collT As ThemeCollection = Nothing
        soT.KeySite = Me.PageObjectGetSite
        soT.KeyFather.Id = Dompe.ThemeConstants.idAreaPartner
        soT.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        collT = Me.BusinessThemeManager.Read(soT)
           
        If Not collT Is Nothing AndAlso collT.Count > 0 Then
            rptSiteMenu.DataSource = collT
            rptSiteMenu.DataBind()
        End If
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
		'-------------------------------------------
    End Sub
    
    Function GetLink(ByVal vo As ThemeValue) As String
        If vo.Key.Id = Dompe.ThemeConstants.partnerNews Then
            Return Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idNews, _langKey.Id)
        End If
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, vo.Key.Id, _langKey.Id)
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
            strclass = "class=""sel"""
        End If
        Return strclass
    End Function
	
	'Form

	
	Sub LoadForm(sender as object, e as eventargs)
	End Sub
	
	Sub saveRequest(sender as object, e as eventargs)
        'Controllo Inserimento
        Dim ErrMess As String = String.Empty
		Dim mFileName As String = String.Empty
		
		If String.IsNUllOrEmpty(ddlPartner.SelectedItem.Value) Then ErrMess = ErrMess + "Partner Name, "
		If String.IsNUllOrEmpty(ddlCountry.SelectedItem.Value) Then ErrMess = ErrMess + "Country, "
		If String.IsNUllOrEmpty(txbMailAddress.Text) Then ErrMess = ErrMess + "Contact Mail Address, "
		If String.IsNUllOrEmpty(txbPhoneNumber.Text) Then ErrMess = ErrMess + "Contact Phone Number, "
		If String.IsNUllOrEmpty(ddlProduct.SelectedItem.Value) Then ErrMess = ErrMess + "Product, "
        If String.IsNUllOrEmpty(ddlTherapeutic.SelectedItem.Value) Then ErrMess = ErrMess + "Therapeutic Area, "
		If String.IsNUllOrEmpty(txtRequest.Text) Then ErrMess = ErrMess + "Request, "
		
        Dim MIMEType As String = Nothing
        If Not String.IsNUllOrEmpty(ErrMess) Then
            ErrMess = "MISSED: " + Left(ErrMess, Len(ErrMess) - 2)
            ltlErrorMessage.Text = "<div style='color:red;'>" & ErrMess & "</div>"
            Exit Sub
		Else
			ltlErrorMessage.Text = String.Empty
        End If
		'------------------------
		
		'Salva nel DB lo storico delle richieste
		Dim mIdNuovo As Int32 = 0
		Dim mMedicoNome As String = String.Empty
		Dim mCognome As String = String.Empty
		Dim mNome As String = String.Empty
		Dim mSesso As String = String.Empty
		Dim mMail As String = String.Empty	

		Dim oUsVal As userValue = Me.BusinessUserManager.Read(Me.ObjectTicket.User.Key)
        If Not oUsVal Is Nothing AndAlso oUsVal.Key.Id > 0 Then
			mNome = "."
			mCognome = oUsVal.Surname
        End If
		
		'INSERIMENTO DB-----------------------------
		Dim strQueryInsert As String = "insertPartnerRequest"
		If Not String.IsNullOrEmpty(strQuery) Then
			Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQueryInsert, False)
		
			If Not String.IsNullOrEmpty(query) Then
				Dim args = New List(Of SqlParameter)
				args.Add(New SqlParameter("@partner_request_date", DateTime.Now))
				args.Add(New SqlParameter("@partner_request_fullname", mNome & " " & mCognome))
				args.Add(New SqlParameter("@partner_request_phone", txbPhoneNumber.Text.Trim))
				args.Add(New SqlParameter("@partner_request_product", ddlProduct.SelectedItem.Value))
				args.Add(New SqlParameter("@partner_request_therapeutic", ddlTherapeutic.SelectedItem.Value))
				args.Add(New SqlParameter("@partner_request_request", txtRequest.Text.Trim))
				args.Add(New SqlParameter("@partner_request_adverse", ddlAdverse.SelectedItem.Value))
				args.Add(New SqlParameter("@partner_request_age", txtAge.Text.Trim))
				args.Add(New SqlParameter("@partner_request_sex", ddlSesso.SelectedItem.Value))
				args.Add(New SqlParameter("@partner_request_user_id", intUserId))			
				
				Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, args.ToArray())
				If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
					Dim dt = ds.Tables(0)
					If Not dt.Rows Is Nothing AndAlso 0 <> dt.Rows.Count Then
						Dim row = dt.Rows(0)
						mIdNuovo = row(0)
					End If
				End If
			End If
		End If		
		'-------------------------------------------
		
		Dim enc As Encoding
		Dim strTicks as String = String.Empty '"_" & DateTime.Now.Ticks.ToString()
		
        Dim objXMLTW As New XmlTextWriter(Server.MapPath("/HP3Download/MedInfoInquiry/xForm" & strTicks & ".xml"), enc)
        objXMLTW.WriteStartDocument()
        'Top level (Parent element)
        objXMLTW.WriteStartElement("APPLICANT")

        'Child elements, from request form
        objXMLTW.WriteStartElement("REQUESTER_LASTNAME")
        objXMLTW.WriteString(mNome)
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("REQUESTER_ORGANIZATION")
        objXMLTW.WriteString(mCognome)
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("REQUESTER_PHONE_NO")
        objXMLTW.WriteString(txbPhoneNumber.Text.ToString())
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("REQUESTER_SEX")
        objXMLTW.WriteString(mSesso)
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("REQUESTER_E_MAIL")
        objXMLTW.WriteString(txbMailAddress.Text.ToString())
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("REQUESTER_COUNTRY")
        objXMLTW.WriteString(ddlCountry.SelectedItem.Value.ToString())
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("INQUIRY_PRODUCT")
        objXMLTW.WriteString(ddlProduct.SelectedItem.Value.ToString())
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("INQUIRY_PRODUCT_CATEGORY")
        objXMLTW.WriteString(ddlTherapeutic.SelectedItem.Value.ToString())
        objXMLTW.WriteEndElement()
        objXMLTW.WriteStartElement("INQUIRY_ADVERSE_EVENT")
        If ddlAdverse.SelectedItem.Text = "YES" Then
            objXMLTW.WriteString("YES")
        Else
            objXMLTW.WriteString("NO")
        End If
        objXMLTW.WriteEndElement()
        If ddlAdverse.SelectedItem.Text = "YES" Then
            objXMLTW.WriteStartElement("AE_ADVERSE_EVENT")
            objXMLTW.WriteString("YES")
            objXMLTW.WriteEndElement()
            objXMLTW.WriteStartElement("PATIENT_SEX")
            If Not IsDBNull(ddlSesso.SelectedItem.Value.ToString()) Then
                mSesso = ddlSesso.SelectedItem.Value.ToString()
                If mSesso = "M" Then
                    mSesso = "MALE"
                ElseIf mSesso = "F" Then
                    mSesso = "FEMALE"
                End If
            Else
                mSesso = ""
            End If
            objXMLTW.WriteString(mSesso)
            objXMLTW.WriteEndElement()
            objXMLTW.WriteStartElement("PATIENT_AGE_AT_TIME_OF_EVENT")
            objXMLTW.WriteString(txtAge.Text.ToString())
            objXMLTW.WriteEndElement()
        End If
        objXMLTW.WriteEndElement() 'End top level element
        objXMLTW.WriteEndDocument() 'End Document
        objXMLTW.Flush() 'Write to file
        objXMLTW.Close()
		objXMLTW.Dispose()
		'---------------------------------------------
		
		'Invio mezzo email del file xml---------------
        Dim replacing As New Dictionary(Of String, String)
        replacing.Add("[REQUEST]", txtRequest.Text.ToString())
		replacing.Add("[FOOTER]", String.Empty)
		
		Dim oMailAttach As MailAttachmentsCollection = Nothing
        If System.IO.File.Exists(Server.MapPath("/HP3Download/MedInfoInquiry/xForm" & strTicks & ".xml")) Then
            oMailAttach = New MailAttachmentsCollection
            oMailAttach.Add(New MailAttachment(Server.MapPath("/HP3Download/MedInfoInquiry/xForm" & strTicks & ".xml")))
        End If
		
		Dim htmlTemplate As String = Me.BusinessDictionaryManager.Read("DMP_htmlTemplate", _langKey.Id, "DMP_htmlTemplate")
		MailHelper.SendMessage(New MailTemplateIdentificator("PartnerMedInfo"), _langKey.Id, htmlTemplate, replacing, False, "", txbMailAddress.Text.ToString(), "Partner Medical Info - Inquiry n° " + mIdNuovo.ToString(), oMailAttach)
		'---------------------------------------------
		
		'Cancellazione file .xml----------------------
		Try
			File.Delete(Server.MapPath("/HP3Download/MedInfoInquiry/xForm" & strTicks & ".xml"))
		Catch
		End Try
		'---------------------------------------------
		
		'Cancellazione file xml e reset del form------
		dFormOkMessage.Visible = True
		ltlFormOkMessage.Text = "<p style='text-align:center;'>-- Inquiry n° <strong>" + mIdNuovo.ToString() + "</strong> of <strong>" & txbInsertDate.Text.ToString() & "</strong> added --</p>"
		dForm.Visible = False
		'---------------------------------------------
		
		PannelloDinamico.Update()
	End Sub
	
	Sub LoadAdverse(sender as object, e as eventargs)
		If ddlAdverse.SelectedItem.Value = "N" Then
			txtAge.Text = String.Empty
			ddlSesso.selectedIndex = 0
			rowSex.Visible = False
			rowAge.Visible = False
		Else
			rowSex.Visible = True
			rowAge.Visible = True
		End If
	End Sub    
</script>
<style>
	/*new form 26032014*/
	.redForm{
		color:#E0002D;
		font-weight:bold;
	}
	.contNewForm{
		clear:both;
		float:left;
		width:560px;
		padding:20px 20px 20px 20px;
		margin:0 0 25px 0;
		background-color:#E7E7E7;	
	}
	.contNewForm p{
		font-size:13px;
		margin:0 0 25px 0;
		text-transform:uppercase;
	}
	
	.contNewForm input, .contNewForm select, .contNewForm textarea {padding:5px;height:auto !important;}
	
	span.request{
		color:#ffffff;
		font-weight:bold;
		background-color:#333333;
		padding:3px 3px 3px 3px;
	}
	.rowNewForm{
		clear:both;
		float:left;
		width:560px;
		margin:0 0 20px 0;
	}
	.rowNewForm label{
		clear:both;
		float:left;
		width:auto;	
		font-size:18px;
		font-weight:bold;
		margin:0 0 13px 0;
	}
	.rowNewForm input{
		clear:both;
		float:left;
		width:560px;
		border:0;
		height:30px;	
	}
	.rowNewForm select{
		clear:both;
		float:left;
		width:560px;
		border:0;
		height:30px;	
	}
	.rowNewForm .left{
		clear:both;
		float:left;
		width:270px;
	}
	.rowNewForm .left input, .rowNewForm .left select{
		clear:both;
		float:left;
		width:270px;
		border:0;
		height:30px;		
	}
	.rowNewForm .right{
		float:left;
		width:270px;
		margin:0 0 0 20px;	
	}
	.rowNewForm .right input, .rowNewForm .right select{
		clear:both;
		float:left;
		width:270px;
		border:0;
		height:30px;	
	}
	.rowEmailCont{
		clear:both;
		float:left;
		width:530px;
		padding:15px 15px 15px 15px;
		background-color:#333333;
		margin:0 0 20px 0;	
	}
	.rowNewFormEmail{
		clear:both;
		float:left;
		width:530px;
		margin:0 0 20px 0;
	}
	.rowNewFormEmail label{
		clear:both;
		float:left;
		width:auto;	
		color:#ffffff;
		font-size:14px;
		font-weight:bold;
		margin:0 0 7px 0;
	}
	.rowNewFormEmail input{
		clear:both;
		float:left;
		width:530px;
		border:0;
		height:30px;	
	}
	.btnNewForm{
		clear:both;
		float:left;
		width:150px;	
		background-color:#e20025;
		text-align:center;
		font-weight:bold;
		color:#ffffff;
	}
	p.subInfo{
		clear:both;
		font-style:italic;
		float:left;
		width:100%;	
		margin:20px 0 0 0;
		font-size:11px;
		line-height:16px;
	}
	table.tableNewForm{
		clear:both;
		float:left;
		width:600px;	
		border:0;	
	}
	table.tableNewForm thead tr{
		clear:both;
		float:left;
		width:580px;	
		font-weight:bold;
		background-color:#f7f7f7;
		padding:3px 10px 3px 10px;	
		margin:0 0 1px 0;
	}
	table.tableNewForm tbody tr{
		clear:both;
		float:left;
		width:580px;	
		/*background-color:#e9e5e5;*/
		padding:3px 10px 3px 10px;	
		margin:0 0 1px 0;
	}
	table.tableNewForm th, table.tableNewForm td{
		font-size:11px;
		text-align:left;
		text-transform:uppercase;
	}
	/*end new form 26032014*/
	
	textarea {width:100%;font-family:arial;font-size:12px}
</style>
<asp:ScriptManager ID="ScriptManager1" ScriptMode="Release" runat="server">
    <CompositeScript>
        <Scripts>
            <asp:ScriptReference Name="MicrosoftAjax.js" />
            <asp:ScriptReference Name="MicrosoftAjaxWebForms.js" />
        </Scripts>
    </CompositeScript>
</asp:ScriptManager>

<section>

    <div class="cont-img-top">
    	    <div class="bg-top" style="background-image:url('/ProjectDompe/_slice/medInfoHeader.jpg');">                
            </div>    
            <div class="txt">
                    <h1 class="white">Medical info</h1>
            </div>
        <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>

 	<%--<div class="networkSection" style="background-image: url('/ProjectDompe/_slice/medInfoHeader.jpg') !important;background-position:center top;">
    	<div class="boxCntGeneric">
            <div class="container">
                <h2 class="white">Medical info</h2>
            </div>
        </div>
    </div><!--end networkSection-->--%>
</section>
    <div class="container">
         <div class="colLeft">        	
                <div class="listSide">                	
                     <asp:Repeater ID="rptSiteMenu" runat="server">
                        <HeaderTemplate><ul></HeaderTemplate> 
                        <ItemTemplate>
                            <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# Me.BusinessDictionaryManager.Read("thm_" & DataBinder.Eval(Container.DataItem, "key.id"), _langKey.Id)%></a></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>  
                </div>            
        </div><!--end colLeft-->
        <div class="colRight" style="padding-top: 0px !important;">
		<asp:UpdatePanel ID="PannelloDinamico" updatemode="conditional" runat="server">
			<ContentTemplate>

		
			<div class="contNewForm" id="dFormOkMessage" visible="false" runat="server">
				<asp:literal id="ltlFormOkMessage" runat="server" />
			</div>
            <div class="contNewForm" id="dForm" runat="server">
				<p>Dear Partner,<br>Welcome to the Dompé Medical Information web page. By providing us with all information requested below, yo will be able to send both scientific inquires and publicaton requests to the Dompé Med Info Team. Before sending your inquiry, please verify your e-mail address specified below (please modify it if needed).</p>
				<asp:literal id="ltlErrorMessage" runat="server" />
				<div class="rowNewForm">
					<label>Partner<span class="redForm">*</span></label>
					<asp:dropdownlist enabled="false" id="ddlPartner" runat="server" OnSelectedIndexChanged="loadCountries" autopostback="True" />
				</div>
				<div class="rowNewForm">
					<label>Country<span class="redForm">*</span></label>
					<asp:dropdownlist enabled="false" id="ddlCountry" runat="server" OnSelectedIndexChanged="LoadProduct" autopostback="True" />
				</div>
				<div class="rowNewForm">
					<label>Contact  Mail Address<span class="redForm">*</span></label>
					<asp:textbox id="txbMailAddress" runat="server" />
				</div>
				<div class="rowNewForm">
					<div>
						<label>Contact Phone Number</label>
						<asp:textbox id="txbPhoneNumber" enabled="false" runat="server" />
					</div>
				</div>
				<div class="rowNewForm">
					<div>
						<label>Insert date<sup class="redForm">*</sup></label>
						<asp:textbox id="txbInsertDate" runat="server" />
					</div>
				</div>
				<div class="rowNewForm">
					<label>Product<span class="redForm">*</span></label>
					<asp:dropdownlist enabled="false" id="ddlProduct" runat="server" OnSelectedIndexChanged="LoadTherapeutic" autopostback="True" />
				</div><!--end rowNewForm-->
				<div class="rowNewForm">
					<label>Therapeutic<span class="redForm">*</span></label>
					<asp:dropdownlist enabled="false" id="ddlTherapeutic" runat="server" />
				</div><!--end rowNewForm-->

				<div class="rowNewForm">
					<div>
						<label>Request<sup class="redForm">*</sup></label>
						<asp:textbox id="txtRequest" textmode="multiline" rows="8" runat="server" />
					</div>
				</div><!--end rowNewForm-->
				<div class="rowNewForm">
					<label>Is your request associated with an adverse event?<span class="redForm">*</span></label>
					<asp:dropdownlist id="ddlAdverse" runat="server" OnSelectedIndexChanged="LoadAdverse" autopostback="True">
						<asp:ListItem value="NO" selected="true" Text="NO" />
						<asp:ListItem value="YES" Text="YES" />
					</asp:dropdownlist>
				</div>
				<div class="rowNewForm" id="rowAge" runat="server" visible="false">
					<label>Età</label>
					<asp:textbox id="txtAge" runat="server" />
				</div> 
				<div class="rowNewForm" id="rowSex" runat="server" visible="false">
					<label>Sesso<span class="redForm">*</span></label>
					<asp:dropdownlist id="ddlSesso" runat="server" autopostback="True">
						<asp:ListItem value="M" Text="M" />
						<asp:ListItem value="F" Text="F" />
					</asp:dropdownlist>
				</div><!--end rowNewForm-->
				<div class="floatRight">
					<asp:button runat="server" cssclass="btnNewForm" onClick="saveRequest" text="Send Request" />
				</div>
				<p class="subInfo">According to the copy right laws, all the mateials Dompé area sending you are for free and for persnal use only. Any duplication  is strctly forbidden.</p>
			</div>
			
			</ContentTemplate>
		</asp:UpdatePanel>			
			
			
        </div><!--end colRight-->
       
    </div><!--end container-->
