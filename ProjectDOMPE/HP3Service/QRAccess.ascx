﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Dompe.Extensions"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Dim _strAddQs As String = String.Empty
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        contentId = Me.PageObjectGetContent.Id
    End Sub

    Sub Page_Load()
        'se l'utente è già autenticato
        'If Me.BusinessAccessService.IsAuthenticated Then
            
        If Not String.IsNullOrEmpty(Request("idrcp")) Then
            'se ho il codice in querystring reindirizzo alla pagina dell'rcp 
            Response.Redirect("/rcp/?intRCP=" & Request("idrcp"))
        Else
            'sennò alla default dell'area medico
            Response.Redirect(Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langKey.Id), False)
        End If
            
        'End If
    End Sub

</script>

<div class="body">
    <div class="brandSection" style="background-image: url('/ProjectDOMPE/_slice/bgTopBrand.jpg');">
        <div class="boxCntGeneric">
            <div class="container">
                <h2 style="color: #333333;">Visualizza RCP</h2>
            </div>
        </div>
    </div>
    <div class="container">   
        <div class="colRight">     
        <iframe id="FrameDocCheck" width="100%" height="700" frameborder="0" scrolling="no" src="/ProjectDOMPE/HP3Common/QRLogin.aspx?c=<%=Me.PageObjectGetTheme.Id%><%=_strAddQs%>&idrcp=<%=Request("idrcp")%>" style="margin:0px; padding:0px; border:0px;"></iframe>
            </div>
        <div class="colLeft" style="">
        	<div class="boxBlueDett bigBoxBlueDett" style="min-height: 440px;">
            	<div class="listSide">
                    </div>
                </div>
            </div>
    </div>
</div>
