﻿<%@ Control Language="VB" ClassName="contactUs" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Register TagPrefix="HP3" TagName="Captcha" Src="~/HP3Common/ctlCaptcha.ascx" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
   Private captchaCode As String
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        SubjectMail()
        AgeUserMail()
    End Sub
    
    Sub Page_Load()
      
        If Not Page.IsPostBack Then
            SetMessageErrors()
           
        End If
        boxNote.Text = Me.BusinessDictionaryManager.Read("PLY_NoteContactUS", Me.PageObjectGetLang.Id, "PLY_NoteContactUS")
        CaptchaInfo()
    End Sub
    
    Sub CaptchaInfo()
        If Not Page.IsPostBack Then
            captchaCode = ImageUtility.GetCaptchaCode()
            IDCaptcha.Code = captchaCode
            HiddenCaptcha.Text = captchaCode
        Else
            'IDCaptcha.Code = ViewState("CaptchaValue.Value")
        End If
        IDCaptcha.Code = HiddenCaptcha.Text
        IDCaptcha.Width = "120"
        IDCaptcha.Height = "30"
        IDCaptcha.ForeColor = Drawing.Color.LightGray
        IDCaptcha.BackColor = Drawing.Color.White
        IDCaptcha.TextBackColor = Drawing.Color.DarkSlateGray
        IDCaptcha.TextForeColor = Drawing.Color.DarkSlateGray
        VerifyCaptcha.Text = ImageUtility.DecryptCaptchaCode(HiddenCaptcha.Text)
    End Sub
    
    Sub SentMessage(ByVal s As Object, ByVal e As EventArgs)
        Dim oMailTemplateValue As MailTemplateValue = Nothing
            
        oMailTemplateValue = getMailTemplate("UserContactMail")
        'gestisco lo storage del messaggio
        ' CreateContactMessage()
        'gestisco email di contatto utente 
        Dim res As Boolean = SendContactMail(oMailTemplateValue)
      
        If Not res Then
            messgError.Text = GetLabel("Contact_Error", "For technical reasons, the message has not been sent successfully. Please, try again later. Thank you.")
            DivInvioOK.Visible = False
            divContactForm.Visible = True
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ ContactUsError(" & Me.ObjectTicket.User.Key.Id & ");})", True)
            
        Else
            messgConf.Text = GetLabel("Contact_Conf", "Your message has been sent successfully. The NOTJUSTAMOMENT.COM team will contact you as soon as possible. Thank you.")
            DivInvioOK.Visible = True
            divContactForm.Visible = False
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ ContactUs(" & Me.ObjectTicket.User.Key.Id & ");})", True)
            ' disableDivForm()
               
            Return
            
        End If
        
    End Sub
    
    Sub SubjectMail()
        Dim kk As Integer
        Dim strOpt As String = ""
        For kk = 0 To 3
            strOpt = Me.BusinessDictionaryManager.Read("PLY_MailSubject_" & kk, _langKey.Id, "PLY_MailSubject_" & kk)
            tbSubject.Items.Add(strOpt)
        Next kk
        tbSubject.DataBind()
        
    End Sub
    
    Sub AgeUserMail()
        Dim kk As Integer
        Dim strAge As String = ""
        For kk = 0 To 4
            strAge = Me.BusinessDictionaryManager.Read("PLY_AgeRange_" & kk, _langKey.Id, "PLY_AgeRange_" & kk)
            ddlAge.Items.Add(strAge)
        Next kk
        ddlAge.DataBind()
        
    End Sub
    
    
    
    'nte sul mailtemplate e inviare l'email
    Function SendContactMail(ByVal oMailTemaplate As MailTemplateValue) As Boolean
        Try
            Dim mailValue As New Healthware.HP3.Core.Utility.ObjectValues.MailValue
           
            If (Not oMailTemaplate Is Nothing) Then
               
                mailValue.MailTo.Add(New MailAddress(oMailTemaplate.Recipients, "NOTJUSTAMOMENT"))
                mailValue.MailFrom = New MailAddress(tbEmail.Text, tbEmail.Text)
            
                mailValue.MailBody = RepleceBodyInfo(oMailTemaplate.Body)
                mailValue.MailSubject = oMailTemaplate.Subject
                mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
               
                If (oMailTemaplate.BccRecipients <> String.Empty) Then
                    mailValue.MailBcc.Add(New MailAddress(oMailTemaplate.BccRecipients))
                End If
                    
                If (oMailTemaplate.FormatType = 1) Then
                    mailValue.MailIsBodyHtml = True
                Else
                    mailValue.MailIsBodyHtml = False
                End If
               
                'invio della mail
                Dim systemUtility As New SystemUtility()
                systemUtility.SendMail(mailValue)
                Return True
            End If
                 
              
        Catch ex As Exception
            Return False
            Throw New BaseException("NOTJUSTAMOMENT- Contact Us Form Error : ", ex)
        End Try
        Return False
    End Function
    
   
    'gestisce replace di alcuni parametri nel Body del MailTemplate
    Function RepleceBodyInfo(ByVal body As String) As String
        Dim replecedBody As String = String.Empty
        Try
            If Not String.IsNullOrEmpty(tbEmail.Text) Then
               
            
                replecedBody = body.Replace("[USER]", "").Replace("[EMAIL]", tbEmail.Text)
           
                If Not String.IsNullOrEmpty(tbMessage.Text) Then replecedBody = replecedBody.Replace("[MESSAGE]", tbMessage.Text)
                If Not String.IsNullOrEmpty(tbSubject.SelectedItem.Text) Then replecedBody = replecedBody.Replace("[SUBJECT]", tbSubject.SelectedItem.Text)
                If Not String.IsNullOrEmpty(ddlAge.SelectedItem.Text) Then replecedBody = replecedBody.Replace("[AGERANGE]", ddlAge.SelectedItem.Text)
                replecedBody = replecedBody.Replace("[SEX]", hdnSexVal.Value)
            End If
        Catch ex As Exception
            ' Response.Write(ex.Message) : Response.End()
        End Try
      
        
        Return replecedBody
    End Function
    
    'recupera il Mail Template Corretto
    'dati la Lingua, il Nome e il Sito
    Function getMailTemplate(ByVal templateName As String) As MailTemplateValue
        Dim omailTemplateSearcher As New MailTemplateSearcher
        Dim omailTemplateValue As MailTemplateValue
        
        omailTemplateSearcher.Key.KeyLanguage = Me.PageObjectGetLang
        omailTemplateSearcher.KeySite = Me.PageObjectGetSite
        omailTemplateSearcher.Key.MailTemplateName = templateName
                     
        Dim coll As MailTemplateCollection = Me.BusinessMailTemplateManager.Read(omailTemplateSearcher)
       
        If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
            omailTemplateValue = coll(0)
            Return omailTemplateValue
        End If
                      
        Return Nothing
    End Function
    
    Sub ResetForm(ByVal s As Object, ByVal e As EventArgs)
        '    tbName.Text = "First name *"
        '   tbSurname.Text = "Last name *"
        tbEmail.Text = "Email *"
        tbMessage.Text = "Body *"
     
        messgError.Text = String.Empty
    End Sub
    
    Sub ResetForm()
        '   tbName.Text = "First name *"
        '  tbSurname.Text = "Last name *"
        tbEmail.Text = "Email *"
        tbMessage.Text = "Body *"
     
        messgError.Text = String.Empty
    End Sub
    
    'Sub disableDivForm()
    '    divForm.Visible = False
    'End Sub
    
    Sub SetMessageErrors()
        Dim reqfield As String = GetLabel("RequiredF", "Required field")
        ' reqName.Text = reqfield
        '  reqSurname.Text = reqfield
        reqEmail.Text = reqfield
        reqMessage.Text = reqfield
        
        regexEmail.Text = GetLabel("regexEmail", "The email is not well formed.")
        regexMessage.Text = GetLabel("regexMessage", "Please, limit the message to 400 characters or less.")
    End Sub
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
</script>

<script type = "text/javascript">

   
    function cancellaValue(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).value = "";
        }
    }

    function test2(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valore;
        }
    }

    function ContactUs(usr) {
        
        HP3Piwik.TraceEvent('MyEvent', 'ContactUS: SendMail userId' + usr, 1, 'page', null, true);

    }
    function ContactUsError(usr) {

        HP3Piwik.TraceEvent('MyEvent', 'ContactUS SendMail Error: userId' + usr, 1, 'page', null, true);

    }
    
</script>

    <script type="text/javascript" language="javascript">
        //        $(document).ready(function () {
        //           $(".fieldContactRadio .radio").dgStyle();
        //        });
        function SelectM() {
            $("#singleM").attr("checked", "checked");
            $("#singleF").removeAttr("checked");
            $("#box-singleM").attr("class", "radio selected");
            $("#box-singleF").attr("class", "radio");
            $("#<%= hdnSexVal.ClientId %>").val("M");

        }
        function SelectF() {
            $("#singleM").removeAttr("checked");
            $("#singleF").attr("checked", "checked");
            $("#box-singleM").attr("class", "radio");
            $("#box-singleF").attr("class", "radio selected");
            $("#<%= hdnSexVal.ClientId %>").val("F");
        }

     
    </script>
    	<div id="divContactForm" class="rowInternal"  runat="server" visible="true">
        	
          <div class="leftBlock2">
                  <img height="475" src="/ProjectDOMPE/_slice/imgContact.jpg" />
           </div>
            <div  class="rightBlock2">
            <div class="barTitleInt">a
                        	<div class="title">
                            	 <h2><%= Me.BusinessDictionaryManager.Read("PLY_Contactus_title", _langKey.Id, "PLY_Contactus_title")%></h2>
                           </div>
                           <div class="imgBarTitle">
                            	<img src="/ProjectDOMPE/_slice/overRight.png">
                            </div>
                        </div>
             <div class="contContactUs">
               
                   <div class="rowContact">
                	  <div class="left">
                                <div class="label">
                                       <span>Email*</span>
                                 </div>
                                 <div class="fieldContact">
                    	    <asp:TextBox ID="tbEmail" runat="server" EnableViewState ="false" />
                            <asp:RequiredFieldValidator ID="reqEmail" ControlToValidate="tbEmail" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup ="contGroup" EnableViewState ="false" />
                            <asp:RegularExpressionValidator ID="regexEmail" runat="server"  ControlToValidate="tbEmail" ValidationGroup ="contGroup" EnableViewState ="false"
                              ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}"/>
                            </div>
                   </div>
                
                  <div class="right">
                                	<div class="labelMini">
                                    	<span>Sex</span>
                                    </div>
                                    <div class="labelMiniSpan">
                                    	<span>M</span>
                                    </div>    
                                    <div class="fieldContactRadio">
                                    <div id="box-singleM" class="radio selected" onclick ="SelectM()">
											<input type="radio" checked="checked" value="M" name="marital_status" id="singleM" />
										</div>
                                    </div>
                                    <div class="labelMiniSpan">
                                    	<span>F</span>
                                    </div>  
                                    <div class="fieldContactRadio">
                                        <div id="box-singleF" class="radio" onclick ="SelectF()">
											<input type="radio" checked="" value="F" name="marital_status" id="singleF" />
                                        </div>    
                                     </div>
                                </div>
                  <input id="hdnSexVal" type="hidden" runat="server" value="M" />
                   </div>

                     <div class="rowContact">
                     <div ="left">
                       <div class="label">
                           <span>Subject</span>
                       </div>
                     <div class="fieldContact">
                	   	   <asp:DropDownList ID="tbSubject" runat="server" />
                  </div>
                  </div>
                  <div class="right">
                  <div class="labelMini">
                            <span>Age</span>
                  </div>
                  <div class="fieldContactMini">
                	   	<asp:DropDownList ID="ddlAge" runat="server" />
                  </div>
                  </div>
                  </div>
                   <div class="rowContact">
                            <div class="label">
                                  <span>Text*</span>
                               </div>
                	 <div class="textArea">
                    	    <asp:TextBox  ID="tbMessage" Width="259" Rows="5" runat="server" EnableViewState ="false" TextMode ="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMessage" ControlToValidate="tbMessage" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup ="contGroup" EnableViewState ="false" />
                            
                            <asp:RegularExpressionValidator ID="regexMessage" runat="server" ControlToValidate="tbMessage"  ValidationGroup ="contGroup"
                               ValidationExpression="[\s\S]{1,400}"></asp:RegularExpressionValidator>
                          </div>   
                   </div>
     <div class="rowContact">
                      <div class="label">
                                  <span><%= Me.BusinessDictionaryManager.Read("PrivacyUS", 2, "PrivacyUS")%></span>
                               </div>     
                	 <div class="textArea2">
                    	    <asp:TextBox  ID="boxNote" Enabled="false" runat="server" EnableViewState ="false" TextMode ="MultiLine"></asp:TextBox>
                          </div>   
                   </div>


            <div class="rowContact noMargin">
            	<div class="fieldContactCaptcha ">
                <div class="label">
                 <%= Me.BusinessDictionaryManager.Read("Code", Me.BusinessMasterPageManager.GetLang.Id, "Code")%>*
                 </div>
                    <asp:placeholder ID="CaptchaPanel" runat="server">
                    
                    <HP3:Captcha ID="IDCaptcha" runat="server" />
                                           
                    <asp:TextBox ID="InsertCaptcha" runat="server" CssClass="captchainput" alt="Captcha code" MaxLength="12"></asp:TextBox>
                    <asp:TextBox ID="HiddenCaptcha" Visible="false" runat="server" />
                    <div style="display: none"> <asp:TextBox ID="VerifyCaptcha" runat="server" /></div>
                    <asp:CompareValidator id="CompareValidator1" style="clear:both;float:left;width:auto;margin: 5px 0 0 62px;" ValidationGroup ="contGroup"
                        ControlToCompare="InsertCaptcha" Operator="Equal" ControlToValidate ="VerifyCaptcha" ForeColor="red" Type="String" 
                        runat="server" EnableViewState ="false" ErrorMessage ="The text entered does not match" />
                       
                        </asp:placeholder>
        	            
                </div><!--end sideL-->
               
                <div class="floatRight">
            		 <div class="btnGen marginLeft52">
                    	 <asp:LinkButton id="btnSend"  runat="server" OnClientClick="ContactUs" OnClick="SentMessage" UseSubmitBehavior="false"  ValidationGroup ="contGroup" > <%= GetLabel("SEND", "SEND")%> </asp:LinkButton>
                    </div>
                </div><!--end sideR-->
        </div>
               <div class="rowContact">
                   <div><asp:Label ID="messgError" class="errorMsg" runat="server"></asp:Label></div>
          	  </div>    
     
          </div>   
        </div> 
         <div id="DivInvioOK" class="rowInternal"  runat="server" visible="false">
            <div class="leftBlock2">
                  <img src="/ProjectDOMPE/_slice/imgContact.jpg" />
           </div>
            <div  class="rightBlock2">
            <div class="barTitleInt">
                        	<div class="title">
                            	 <h2><%= Me.BusinessDictionaryManager.Read("PLY_InvioOK", _langKey.Id, "PLY_InvioOK")%></h2>
                           </div>
                           <div class="imgBarTitle">
                            	<img src="/ProjectDOMPE/_slice/overRight.png" />
                            </div>
                        </div>
               <div class="contContactUs">
              <div class="rowContact"> <asp:Label ID="messgConf"  runat="server">
               </asp:Label></div></div>
              
    </div>        
          
  </div>
       <div class="sfumBig marginBott80"> </div>
   
  


  
