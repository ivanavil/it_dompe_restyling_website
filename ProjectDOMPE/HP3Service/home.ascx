﻿<%@ Control Language="VB" ClassName="Home" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _readMore As String = String.Empty
    Protected _langId As Integer = 0
    Protected _atGlanceDescription As String = String.Empty
    Dim _pageTitle As String = String.Empty
    
    Protected Sub Page_Init()
        _siteFolder = Me.ObjectSiteFolder
        _langId=Me.PageObjectGetLang.Id
        _readMore = Me.BusinessDictionaryManager.Read("DMP_readMore", _langId, "DMP_readMore")

        
        Dim pageTitle As String = Me.BusinessMasterPageManager.GetPageTitle()
        _pageTitle = "Dompé Corporate"
        If Not String.IsNullOrEmpty(pageTitle) Then _pageTitle = pageTitle
    End Sub
    
    Protected Sub Page_Load()
        InitView()
    End Sub
    
    Protected Sub InitView()       
        InitHomeSlider()
        InitChiSiamo()               
        InitNewsView()
    End Sub
    
   
    
    Protected Sub InitHomeSlider()
        
        Dim ctrlPath = String.Format("/{0}/HP3Common/HomeSlider.ascx", _siteFolder)
        Me.LoadSubControl(ctrlPath, plhSlider)
        
    End Sub
    
    Protected Sub InitChiSiamo()
        
        Dim ctrlPath = String.Format("/{0}/HP3Common/HomeChiSiamo.ascx", _siteFolder)
        Me.LoadSubControl(ctrlPath, plhChiSiamo)
        
    End Sub
      
  
    Protected Sub InitNewsView()
        
        Dim ctrlPath = String.Format("/{0}/HP3Common/HomeEvents.ascx", _siteFolder)
        Me.LoadSubControl(ctrlPath, plhEventi)
        
    End Sub
    
</script>


<div class="body home">
    <div class="titleHp"><h1><%=_pageTitle%></h1></div>
    
    <!--slider-->
    <asp:PlaceHolder runat="server" ID="plhSlider"></asp:PlaceHolder>
    <!--Box News-->
    <asp:PlaceHolder runat="server" ID="plhEventi"></asp:PlaceHolder>        
    <!--chi siamo-->
    <asp:PlaceHolder runat="server" ID="plhChiSiamo"></asp:PlaceHolder>    
    
      

</div>

<div id="dialog-video-home"><iframe width="853" height="480" src="//www.youtube.com/embed/PKbDWZpbG5g" frameborder="0" allowfullscreen></iframe></div>



 <script>
     $(function () {
         $("#dialog-video-home").dialog({
             height:530,
             width:882,
             modal: true,
             autoOpen: false,
             draggable:false,
             resizable: false
         });
     });

     $("#opener-video-home").click(function () {
         $("#dialog-video-home").dialog("open");
     });
</script>
<style>
     .ui-dialog .ui-dialog-titlebar
    {
        background-color: #fff;
        border: 0;
        padding: 0 0;
        position: relative;
    }
    .ui-dialog
    {
         background-color: #fff;
    }

</style>