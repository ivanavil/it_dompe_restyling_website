﻿<%@ Control Language="VB" ClassName="MediaEventi" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _readMore As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _newsTemplate As String = String.Empty
    Protected _ajaxNewsPage As String = String.Empty
    Protected _siteAreaKey As SiteAreaIdentificator = Nothing
    Protected _text As String = String.Empty
    Protected _title As String = String.Empty
    Protected _titleClass As String = String.Empty
    Protected _titleStyle As String = String.Empty
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
	Private _cacheKey As String = String.Empty
	Private _cacheKeyHeader As String = String.Empty
    Dim _headerText As String = String.Empty
    Protected _topKeysNews As String = String.Empty
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _langKey = Me.PageObjectGetLang
        _readMore = Me.BusinessDictionaryManager.Read("DMP_readMore", _langKey.Id, "DMP_readMore")
        _newsTemplate = String.Format("/{0}/Templates/MediaNews.htm", _siteFolder)
        _ajaxNewsPage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _siteAreaKey = Me.PageObjectGetSiteArea
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        'Media
        Dim thval As New ThemeValue
        thval = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNews))
        If Not thval Is Nothing AndAlso thval.KeyContent.Id > 0 Then
            _topKeysNews = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(thval.KeyContent.Id, _langKey.Id)
            
        End If
    End Sub
    Dim _CheckApproval As Integer = 1
    Protected Sub Page_Load()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
        '_cacheKey = "_cacheMedia1Livello:" & _langKey.Id & "|" & _themeKey.Id & "|" & _siteAreaKey.Id & "|"
        '_cacheKeyHeader = "_cacheHeaderMedia1Livello:" & _langKey.Id & "|" & _themeKey.Id & "|" & _siteAreaKey.Id & "|"
        'If Request("cache") = "false" Then
        'CacheManager.RemoveByKey(_cacheKey)
        'CacheManager.RemoveByKey(_cacheKeyHeader)
        'End If
		
        'ltlLinkNews.Text = getLinkToNews()
		
        'Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
        'Dim _strCachedContentHeader As String = CacheManager.Read(_cacheKeyHeader, 1)
        'If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
        '_text = _strCachedContent
        'Else
        'InitView()
        'End If
		
        'If Not (_strCachedContentHeader Is Nothing) AndAlso (_strCachedContentHeader.Length > 0) Then
        '_headerText = _strCachedContentHeader
        'Else
        'InitView()
        'End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Function getLinkToNews() As String
        Dim oNewTh As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNews), _langKey)
        Dim _linkToNews As String = String.Empty
        If Not oNewTh Is Nothing And oNewTh.Key.Id > 0 Then
            Return "<a href='" & Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idNews, _langKey.Id) & "' title='" & oNewTh.Description & "'>" & oNewTh.Description & "</a>"
        End If

        Return String.Format("<a href='{0}' title='{1}'>{2}</a>", "/news", "News", "News")
    End Function	
    
    Protected Sub InitView()
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key = _themeKey
        soTheme.KeyLanguage.Id = _langKey.Id
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentExtraSearcher
            so.KeySite = _siteKey
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = theme.KeyContent.Id
            so.CalculatePagerTotalCount = False
            so.SetMaxRow = 1
            Dim coll = Me.BusinessContentExtraManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                If Not cont Is Nothing Then
                    _text = cont.FullText
                    _title = cont.TitleContentExtra
                    _titleClass = cont.Code
                    _titleStyle = cont.TitleOriginal
                    Dim replacing = GetPlaceHolders()
                    If Not replacing Is Nothing AndAlso replacing.Any() Then
                        For Each item As KeyValuePair(Of String, String) In replacing
                            _text = _text.Replace(item.Key, item.Value)
                        Next
                    End If
                    _headerText = "<div class='box-container titleSection'><div class='boxCnt'><div class='container'><h1 class='" & _titleClass & "' " & _titleStyle & ">" & _title & "</h1></div></div></div>"
                End If
				
				
				'Cache Content and send to the oputput-------
				CacheManager.Insert(_cacheKey, _text, 1, 120)
				CacheManager.Insert(_cacheKeyHeader, _headerText, 1, 120)
				'--------------------------------------------				
            End If
        End If
               
    End Sub
    
    Protected Function GetPlaceHolders() As Dictionary(Of String, String)
        
        Dim replacing As New Dictionary(Of String, String)
        If Me.BusinessAccessService.IsAuthenticated AndAlso Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleMediaPress) Then
            replacing.Add("[SEIGIORNALISTA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langKey.Id))
        Else
            replacing.Add("[SEIGIORNALISTA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langKey.Id))
        End If
        replacing.Add("[UFFICIOSTAMPA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idContattiMediaPress, _langKey.Id))
        Return replacing
        
    End Function
    
</script>


<section>
 	<div class="cont-img-top">
    	<div class="layer">&nbsp;</div>
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/media.jpg');"></div>
        <div class="txt">
            <h1><%=Me.BusinessDictionaryManager.Read("theme_" & _themeKey.Id, _langKey.Id, "Comunicazione e Media")%></h1>    
        </div>    
    </div><!--end cont-img-top-->
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

<section class="com-media proj-section">
    <div class="container">
        <h1>News</h1>
        <div class="sep">&nbsp;</div>
        <div id="newsLoader" style="text-align:center; display:none;"><img src="/<%=_siteFolder%>/_slice/ajax-loader.gif" alt="loading" /></div>
        <div class="generic" id="newsContainer">                	
           
		</div>
    </div>
</section>  


<section class="proj-section">
    <div class="container">
     <h1><%=Me.BusinessDictionaryManager.Read("theme_" & Dompe.ThemeConstants.idProgettiSpeciali, _langKey.Id, "Progetti Speciali")%></h1>
     <div class="sep">&nbsp;</div>
        <div class="generic">
         <div class="proj-box">
                <div class="imgs"><img src="/ProjectDOMPE/_slice/progSpeciali_StopContratture.jpg" /> </div>
                <div class="bottom">
                 <a href="http://www.stopcontratture.it/" title="Stop contratture" target="_blank">
                    Stop Contratture</a>
                    <span></span>
                </div>
         </div><!--end man-box-->
         
  </div>
    </div>
</section>


<%--<section>
	<div class="box-full" style="background-image:url('/ProjectDOMPE/_slice/progSpeciali_StopContratture.jpg');">
    	<div class="cont">
    	
        <p style="margin:0 auto;position:relative;
width:59px;padding: 300px 0 0 0;"><a style="color:#1a181b;" href="http://www.stopcontratture.it/" target="_blank" ><img src="/ProjectDOMPE/_slice/go.png" /></a></p>
        </div>
	</div>
</section>  --%>

<section>
	<div class="box-full" style="background-image:url('/ProjectDOMPE/_slice/contatti.jpg');">
    	<div class="cont">
    	<h1 style="text-shadow: 3px 3px 10px #000;"><%=Me.BusinessDictionaryManager.Read("theme_" & Dompe.ThemeConstants.idContattiMediaPress, _langKey.Id, "Contatti ufficio stampa")%></h1>	    	
        <p><a style="color:#1a181b;" href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idContattiMediaPress, _langKey.Id)%>" ><img src="/ProjectDOMPE/_slice/go.png" /></a></p>
        </div>
	</div>
</section>  

<%--<div class="body media">
	<%=_headerText %>
	<div class="box-container item1">
		<div class="boxCnt">            
		 
			<div class="container">
				<div class="cnt">
					<h3><asp:Literal ID="ltlLinkNews" runat="server" /></h3>
					<div></div>  
				</div>
			</div>
		</div>
	</div>
	<%=_text %>
</div>--%>




<script type="text/javascript">
    var _templateUrl = "<%=_newsTemplate%>";
    var _ajaxNewsPage = "<%=_ajaxNewsPage%>";
    var _defaultKeysToExclude = "<%=_topKeysNews%>";
    var LoadNewsCallback = function (data) {
    var container = $("#newsContainer");
        if (data) {
            if (data.Success) {
                if (data.StatusCode == "<%=CInt(Keys.EnumeratorKeys.AsyncStatusCode.FillItem)%>") {                    
                    $.tmpl("ArchiveTemplate", data.Result.Collection).appendTo(container);
                    container.fadeIn('fast');
                    $("#newsLoader").hide();
                }
            }
        }
    };

    var OnLoadNewsCallback = function () {
        $(".viewNews").hover(
	        function () {
	            var temp = $(this).parent();
	            $(temp).children(".viewMoreNews").removeClass('displayNone');
	        }, function () {
	            var temp = $(this).parent();
	            $(temp).children(".viewMoreNews").addClass('displayNone');
	        }
	    );
    }

    $(document).ready(function () {

        var searcher = GetSearcher();

        LoadTemplate(_templateUrl, LoadNews, searcher, LoadNewsCallback, OnLoadNewsCallback);

    });

    

    function GetSearcher()
    {
        var searcher = {
            SiteareId: '<%=Dompe.SiteAreaConstants.IDSiteareaNews%>',
            ContenTypeId: '<%=Dompe.ContentTypeConstants.Id.ArchivioCtype%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=Dompe.ThemeConstants.idNews %>',            
            Display: true,
            KeysToExclude: _defaultKeysToExclude,
            Active: true,
            ApprovalStatus: '<%=_CheckApproval%>',
            Delete: false,
            PageNumber: 1,
            PageSize: 3
        };

        recordOutboundFull("Archivio <%= Helper.GetSectionName(_siteAreaKey) %>", "Caricamento", "page: 1")
        
        return searcher;
    }



    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {
            $.template("ArchiveTemplate", template);

            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    function LoadNews(contentSearcher, callback, onCompleteCallback) 
    {
        $('#newsLoader').fadeIn('fast');
        $('#newsContainer').hide();

        var postData = {
            op: '<%=Cint(Keys.EnumeratorKeys.LiteratureViewType.FeaturedBox)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: _ajaxNewsPage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {
                
                if (callback && typeof (callback) == 'function') callback(data);
            },
            error: function (data) {
                
            }
        }).complete(function () {
            if (onCompleteCallback && typeof (onCompleteCallback) == 'function') onCompleteCallback();
            
        });
    }

    

</script>