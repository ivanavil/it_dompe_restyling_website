﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentVal As New ContentValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Dim ContenutoStoria As String = ""
    Private langContent As Integer = 0
    Private imgPath As String = "/Dompe/_slice/imgCenter.jpg"
    Protected _TitlePage As String = ""
    Protected userVal As UserHCPValue
    Private _classSection As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
	Private _isMobile as boolean = false
		
	Dim	_UserName As String = String.Empty
	Dim	_UserLastName As String = String.Empty
	Dim	_UserEmail As String = String.Empty
	Dim	_DocCheckId As String = String.Empty
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
	
	Public Function IsMobileDevice() As Boolean
		Dim userAgent = Request.UserAgent.ToString().ToLower()
		If Not String.IsNullOrEmpty(userAgent) Then
			if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
			userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
				Return true
			End If
		End if
		Return False
	End Function
        
    
    Sub Page_Load()
		_isMobile = IsMobileDevice()
        contentId = Me.PageObjectGetContent.Id
		btmReq.Attributes.Add("style", "font-size:180%;")
        Dim serchcont As New ContentSearcher
        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/MenuAreaMedico.ascx", _siteFolder)
        LoadSubControl(pathPressMenu, PHL_Med_Menu)
        
        If Not Me.ObjectTicket Is Nothing AndAlso Me.ObjectTicket.User.Key.Id > 0 Then
            userVal = Me.BusinessUserHCPManager.Read(Me.ObjectTicket.User.Key)
			With userVal
				_UserName = userVal.Name
				_UserLastName = userVal.Surname
				_UserEmail = userVal.Email.ToString
				_DocCheckId = userVal.HCPRegistration
			End With
        End If
        
        If contentId > 0 Then 'controllo se è presente il content
            contentVal = Me.BusinessContentManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
            If Not contentVal Is Nothing AndAlso contentVal.Key.Id > 0 Then
                _TitlePage = contentVal.Title
                titolo_sezione.Text = contentVal.Title
                testo_sezione.Text = contentVal.Launch
                _classSection = contentVal.Code
                
            End If                                            
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Function GetContentPKbyID(ByVal id As Integer) As Integer
        Dim serchct As New ContentExtraSearcher
        Dim serchColl As ContentExtraCollection
        Dim PK As Integer = 0
        serchct.key.Id = id
        'serchct.Display = SelectOperation.Enabled
        'serchct.Active = SelectOperation.Enabled
        'serchct.Delete = SelectOperation.Disabled
        serchct.Properties.Add("Key.PrimaryKey")
        
        serchColl = Me.BusinessContentExtraManager.Read(serchct)
        
        If Not serchColl Is Nothing AndAlso serchColl.Count > 0 Then
            PK = serchColl(0).Key.PrimaryKey
        End If
        
        Return PK
    End Function
      
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = contentId
      
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
           
    Function GetSearchLink(ByVal domain As String, ByVal name As String, ByVal tag As Integer) As String
        If tag > 0 Then
            Dim so As New ThemeSearcher
       
            so.KeyLanguage = _langKey
            so.Key.Domain = domain
            so.Key.Name = name
            so.KeySite = Me.PageObjectGetSite
            so.LoadHasSon = False
            so.LoadRoles = False
            so.Display = SelectOperation.Disabled
      
            Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
            If (Not coll Is Nothing AndAlso coll.Any) And (Not String.IsNullOrEmpty(tag)) Then
                Dim link As String = Me.BusinessMasterPageManager.GetLink(coll(0), False)
                link = link & "?tag=" & tag.ToString
            
                Return link
            End If
        End If
               
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
	
	Sub doWebrequest(sender as Object, e as eventargs)
		'Dim field1 as string  = hdnDocCheckId.Value
		Dim POSTdata as string = String.Empty '"userDocCheckID=" & hdnDocCheckId.Value _
			'& "&userEmail=" & hdnUserEmail.Value _
			'& "&userFirstName=" & hdnUserName.Value _
			'& "&userLastName=" & hdnUserLastName.Value
		
		Dim responseData As String = ""
		Try
			Dim _url As String = "http://msvweb13.dompe.it:85/rivisteonline/medicocheck.asp"
			Dim hwrequest As Net.HttpWebRequest = Net.Webrequest.Create(_url)
			hwrequest.Accept = "*/*"
			hwrequest.AllowAutoRedirect = true
			hwrequest.UserAgent = "http_requester/0.1"
			hwrequest.Timeout = 60000
			hwrequest.Method = "POST"
			If hwrequest.Method = "POST" Then
				hwrequest.ContentType = "application/x-www-form-urlencoded"
				Dim encoding As New Text.ASCIIEncoding()
				Dim postByteArray() As Byte = encoding.GetBytes(POSTdata)
				hwrequest.ContentLength = postByteArray.Length
				Dim postStream As IO.Stream = hwrequest.GetRequestStream()
				postStream.Write(postByteArray, 0, postByteArray.Length)
				postStream.Close()
			End If
			
			Dim hwresponse As Net.HttpWebResponse = hwrequest.GetResponse()
			If hwresponse.StatusCode = Net.HttpStatusCode.OK Then
				'Dim responseStream As IO.StreamReader = _
				'New IO.StreamReader(hwresponse.GetResponseStream())
				'responseData = responseStream.ReadToEnd()
			End If
			hwresponse.Close()
		Catch ex As Exception
			responseData = "An error occurred: " & ex.Message
		End Try
		
		'response.write(responseData)
    End Sub
    
    Function GetImageContent(ByVal IDcont As Integer, ByVal ImgWidth As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, Me.PageObjectGetLang.Id), ImgWidth, title)
        Return res
    End Function
    
    Function GetImageContent() As String
        Dim res As String = ""
        Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High

        
        res = Me.BusinessContentManager.GetCover(Me.PageObjectGetContent, 1280, 323, imgqual, 1)
        
        If Not String.IsNullOrEmpty(res) Then
            
            res = "/HP3Image/cover/" & res
        Else
            div_banner.Visible = False
        End If
        Return res
    End Function
</script>
<script type="text/javascript">
	var _DocCheckId = '<%=_DocCheckId%>';
	var _UserName = '<%=_UserName%>';
	var _UserLastName = '<%=_UserLastName%>';
	var _UserEmail = '<%=_UserEmail%>';
	
	function goToRequestForm() {
		//construct htmlForm string
		var htmlForm = "<form action='http://msvweb13.dompe.it:85/rivisteonline/medicocheck.asp' id='richiediartform' target='_blank' method='post'>" +
			"<input type='hidden' name='uniquekey' id='uniquekey' value='" + _DocCheckId + "' />" +
			"<input type='hidden' name='firstname' id='firstname' value='" + _UserName + "' />" +
			"<input type='hidden' name='lastname' id='lastname' value='" + _UserLastName + "' />" +
			"<input type='hidden' name='emailaddress' id='emailaddress' value='" + _UserEmail + "' />" +
		"</form>";

		//Submit the form
		$(htmlForm).appendTo("body").submit();
	};
	
    $(function () {
        $("#<%=btmReq.ClientId%>").click(function (event) {
            event.preventDefault();
        });
    });
	
	$(function () {
		$(".richiediArticolo").click(function(event){
			event.preventDefault();
			goToRequestForm();
			//document.location.href = $(this).find("a").attr("href");
		});	
	});
</script>




    <asp:PlaceHolder id="div_banner" runat="server" >
    <section>
        <div class="cont-img-top">
            <div class="bg-top" style="background-image:url('<%=GetImageContent()%>')">    	                                          
            </div><!--end titleSection-->
            <div class="txt">
                       <h1 class="<%=_classSection%>" <%=_inlineStyleTitle %> style="text-align:center;"><asp:Literal ID="titolo_sezione" runat="server" /></h1>
            </div>  
            <div class="layer">&nbsp;</div>
        </div>
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder> 
    </section>
    </asp:PlaceHolder>
<section>
	<div class="container">
        <div class="colLeft">        	
            	<div class="listSide">
                   <asp:PlaceHolder ID="PHL_Med_Menu" runat="server" Visible="true" />

                </div>                
        </div><!--end colLeft-->
         <div class="colRight">
             <div class="content">
            <asp:Literal ID="testo_optional" runat="server" />
            <asp:literal id="ltlContentDetail" runat="server" EnableViewState="False" />
                
            <asp:Literal ID="testo_sezione" runat="server" />
            <div style="margin:10px 0;border:dashed 1px red;padding:5px;padding-left:35px;background-image:url('/ProjectDompe/_slice/alert.png');background-repeat:no-repeat;background-position:3px 8px;">
	            <%= Me.BusinessDictionaryManager.Read("DMP_RADiscl", 1, "DMP_RADiscl")%>
	            <div style="font-style:italic;font-weight:bold;margin-top:5px;"><%= Me.BusinessDictionaryManager.Read("DMP_RADiscl2", 1, "DMP_RADiscl2")%></div>
            </div>
						
            <div class="readAll richiediArticolo" style="cursor:pointer;">
                <span class="spantxt"><%= Me.BusinessDictionaryManager.Read("DMP_ClickHereRA", 1, "DMP_ClickHereRA")%></span>
	            <asp:linkbutton id="btmReq" PostBackUrl="/test.aspx" OnClientClick="aspnetForm.target = '_blank'" runat="server" CssClass="linkimg viewMore viewMoreWhite">
		            Leggi tutto
	            </asp:LinkButton>
                
            </div>
            </div>
        </div><!--end colRight-->
         
    </div><!--end container-->
</section>
