﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
	Private _isMobile as boolean = false
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
  
    End Sub
	
	Public Function IsMobileDevice() As Boolean
		Dim userAgent = Request.UserAgent.ToString().ToLower()
		If Not String.IsNullOrEmpty(userAgent) Then
			if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
			userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
				Return true
			End If
		End if
		Return False
	End Function	
        
    Dim _canCheckApproval As Boolean = False
    Sub Page_Load()
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        Dim contentId As Integer = Me.PageObjectGetContent.Id

        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/MenuAreaMedico.ascx", _siteFolder)
        LoadSubControl(pathPressMenu, PHL_Med_Menu)
        Dim pathRichiediArticolo As String = String.Format("/{0}/HP3Common/BoxRichiediArticolo.ascx", _siteFolder)
        LoadSubControl(pathRichiediArticolo, phRichiediArticolo)
        
        Dim so As New ThemeSearcher()
        so.Key = Me.PageObjectGetTheme
        so.KeyLanguage.Id = 1'_langId
        so.LoadHasSon = False
        so.LoadRoles = False
        so.Properties.Add("Description")
     
        Dim coll = Me.BusinessThemeManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any() Then
            titolo_sezione.Text = coll(0).Description
        End If
        
        
        If Me.PageObjectGetContent.Id > 0 Then
            
            repContenuti.Visible = False
            archivio.Visible = False
            Dim oStringBuilder As StringBuilder = New StringBuilder()
            Dim serchcont As New ContentExtraSearcher
            
            Dim soEx As New ContentExtraSearcher()
            soEx.key.Id = contentId
            soEx.key.IdLanguage = _langKey.Id
            soEx.CalculatePagerTotalCount = False
            soEx.SetMaxRow = 1
            soEx.KeySite = Me.PageObjectGetSite
            If Not _canCheckApproval Then soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
            If Not contentextraColl Is Nothing AndAlso contentextraColl.Any() Then
                contentextraVal = contentextraColl.FirstOrDefault
                'contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, 1))
                'If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                fulltext.Text = "<h3>" & contentextraVal.Title & Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus) & "</h3>"
                fulltext.Text = fulltext.Text & contentextraVal.FullText
                  
            End If
        
            
        Else
               
            Dim cntSrc As New ContentExtraSearcher
       
            Dim cntcoll As New ContentExtraCollection
            Dim cntsearch As New ContentExtraSearcher
            Dim cntVal As New ContentExtraValue
            cntsearch.key.IdLanguage = 1 '_langId
            cntsearch.KeySiteArea.Domain = Me.BusinessMasterPageManager.GetSite.Id
            cntsearch.KeySiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
            cntsearch.KeyContentType.Id = Me.BusinessMasterPageManager.GetContentType.Id
      
            cntsearch.SortType = ContentGenericComparer.SortType.ByData
            cntsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
		 
            If Not _canCheckApproval Then
                cntsearch.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            Else
                 Me.BusinessContentExtraManager.Cache=False 
            End If
            
            
            cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
			
            If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
                repContenuti.DataSource = cntcoll
                repContenuti.DataBind()
                repContenuti.Visible = True
            Else
                repContenuti.Visible = False
            End If
           
            fulltext.Visible = False
            archivio.Visible = True
        End If
             
     
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
  
        
    End Sub
     
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 140, title, title)
        Return res
    End Function
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
</script>


<section>
    <div class="cont-img-top">
        <div class="bg-top focusSection">
        </div>    	
            <div class="txt">
                <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>
            </div>
        <div class="layer">&nbsp;</div>
    </div>

    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

<section>
	<div class="container">
        <div class="colLeft">        	
            	<div class="listSide">
                  <asp:PlaceHolder ID="PHL_Med_Menu" runat="server" Visible="true" />

                </div>                
        </div><!--end colLeft-->


         <div class="colRight">  
             <div class="content">              
				<asp:literal id="fulltext" runat="server" EnableViewState="False" />       
             

             <asp:PlaceHolder ID="archivio" runat="server" EnableViewState="False" >  
                      <div class="eventiContInt marginTop10">
            	
                          <asp:Repeater ID="repContenuti" runat="server" EnableViewState="False">
                              <HeaderTemplate></HeaderTemplate>
                              <ItemTemplate>
                                    <div id="singleEventsInt" class="singleEventsInt">
                   <!-- <div class="img" style="background-image:url('/ProjectDompe/_slice/arrowEvSx.png');">&nbsp;</div> -->
                    <div class="dateInt" >
                        <!--<span><%# Day(DataBinder.Eval(Container.DataItem, "DatePublish"))%></span>
                        <span class="mese"><%# Left(MonthName(Month(DataBinder.Eval(Container.DataItem, "DatePublish"))), 3)%></span>
                        <span class="anno"><%# Right(Year(DataBinder.Eval(Container.DataItem, "DatePublish")), 2)%></span>-->
                    </div><!--end date-->
                    <div class="txtEvContInt" style="width:459px;">
                        <div class="txtEvInt">
                            <h3><%# DataBinder.Eval(Container.DataItem, "title")%> <%#Healthware.Dompe.Helper.Helper.checkApproval(DataBinder.Eval(Container.DataItem, "ApprovalStatus")) %></h3>
                            <p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>
                        </div><!--end txtEv-->  
                        <div class="goEv" style="display:none;">
                            <a href="<%# GetLink(Container.DataItem) %>" title=" <%=Me.BusinessDictionaryManager.Read(" Vai all'evento", Me.PageObjectGetLang.Id, " Vai all'evento")%>">
                                <%=Me.BusinessDictionaryManager.Read(" Vai all'evento", Me.PageObjectGetLang.Id, " Vai all'evento")%>
                            </a>
                        </div>
                    </div><!--end txtEvContInt-->
            	</div><!--end singleEventsInt-->
                                  
                                  
                                  
                                  </ItemTemplate>
                              <FooterTemplate></FooterTemplate>

                          </asp:Repeater>

          
            
            </div><!--end eventiContInt-->
            
            <asp:PlaceHolder ID="phRichiediArticolo" runat="server" Visible="true" />


            <%--<div class="chiediArticoloBanner">
            	<a href="#">140 riviste.</a>
            </div>--%>
             
             </asp:PlaceHolder>     			
              </div>                     
        </div><!--end colRight-->

     


    </div><!--end container-->
   </section> 

		
	
 <script type="text/javascript">
     $(document).ready(function () {
         $('.singleEventsInt').hover(
             function () {
                 $(this).find('.txtEvInt').css('display', 'none');
                 $(this).find('.goEv').css('display', 'block');
                 /*$(this).find('.img').children().css('width','500px');
                 $(this).find('.img').children().css('left','-90px');
                 $(this).find('.img').children().css('top','-20px');*/
                 $(this).find('.img').children().animate({ width: "180px", left: "-90px", top: "-20px" }, 300);

             },
             function () {
                 $(this).find('.txtEvInt').css('display', 'block');
                 $(this).find('.goEv').css('display', 'none');
                 /*$(this).find('.img').children().css('width','323px');
                 $(this).find('.img').children().css('left','0px');
                 $(this).find('.img').children().css('top','0px');*/
                 $(this).find('.img').children().animate({ width: "140px", left: "0px", top: "0px" }, 300);
             }
     );
     });
</script>
	<script type="text/javascript">
		<%' If Not _isMobile Then %>
		//$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	   <%' End If %>
	</script>