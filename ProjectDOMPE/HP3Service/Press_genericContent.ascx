﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        contentId = Me.PageObjectGetContent.Id
    End Sub
        
    
    Sub Page_Load()
      
        If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            idContentTheme = valthem.KeyContent.Id
          
           ' verifico se il content è quello associato al tema allora carico il contenuto generico
            If contentId = idContentTheme Then
                Dim src As New ContentExtraSearcher
                Dim sCol As New ContentExtraCollection
                
                src.KeySiteArea.Id = _siteArea.Id
                src.KeyContentType.Id = Me.PageObjectGetContentType.Id
                src.key.IdLanguage = _langKey.Id
                src.key.Id = contentId
                sCol = Me.BusinessContentExtraManager.Read(src)
                
             
                If Not sCol Is Nothing AndAlso sCol.Count > 0 Then
                    titolo_sezione.Text = sCol(0).Title
                    testo_sezione.Text = sCol(0).FullText
                    testo_subtitle.Text = sCol(0).Abstract
                    LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxDownload.ascx", _siteFolder), PhlDownload)
                    LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxLinks.ascx", _siteFolder), PhlLinks)
                    LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxComunicatiStampa.ascx", _siteFolder), PhlComunicati)
                    
                End If
                plhDetail.Visible = False
                plhContentGeneric.Visible = True
           
            Else 'se il content non è quello associato al tema allora carico il dettaglio
                Dim path As String = String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder)
                LoadSubControl(path, plhDetail)
                plhDetail.Visible = True
                plhContentGeneric.Visible = False
            
            End If
                               
            Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
            LoadSubControl(pathPressMenu, PHL_Press_Menu)
            
        Else
            titolo_sezione.Text = ""
            testo_sezione.Text = Dict.Read("Il Contenuto non è più disponibile", _langKey.Id, "Il Contenuto non è più disponibile")
            testo_subtitle.Text = ""
            PHL_Press_Menu.Visible = False
        End If
                  
    End Sub
    

    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
</script>
<div id="DivImagesTop" class="" runat="server">
&nbsp;
</div>


<asp:PlaceHolder ID="PHL_Press_Menu" runat="server" Visible="true" />


<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
 <div class="rowInternal">
    
        
          <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>
          <asp:Literal ID="testo_subtitle" runat="server" />
          <p><asp:Literal ID="testo_sezione" runat="server" /></p>
          <asp:PlaceHolder runat="server" ID="PhlDownload" />      
          <asp:PlaceHolder runat="server" ID="PhlLinks" />  
             <asp:PlaceHolder runat="server" ID="PhlComunicati" />  
          
 </div><!--block-->
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">
&nbsp;
</asp:PlaceHolder>
