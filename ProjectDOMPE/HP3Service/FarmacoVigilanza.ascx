﻿<%@ Control Language="VB" ClassName="FarmacoVigilanza" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dmp.Helpers" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Register TagPrefix="HP3" TagName="Captcha" Src="~/HP3Common/ctlCaptcha.ascx" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _token As String = String.Empty
    Protected _langKey as LanguageIdentificator = Nothing
    Private captchaCode As String
    Protected _themeKey As ThemeIdentificator = Nothing
    
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _token = Guid.NewGuid().ToString
        _langKey = Me.PageObjectGetLang
        _themeKey = Me.PageObjectGetTheme
    End Sub
    
    Protected Sub Page_Load()
        
        If Not Page.IsPostBack Then
            InitView()
            CaptchaInfo()
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        Dim _ctrlPath As String = String.Format("/{0}/HP3Common/BoxRelatedContent.ascx", _siteFolder)
        Me.LoadSubControl(_ctrlPath, ph_RelatedContent)
    End Sub
    
    'Captcha system is used to protect the system by BOT automatic registration
    Sub CaptchaInfo()
        'CaptchaPanel.Visible = True
        'If Not Page.IsPostBack Then
        captchaCode = ImageUtility.GetCaptchaCode()
        IDCaptcha.Code = captchaCode
        HiddenCaptcha.Text = captchaCode
        'Else
        'IDCaptcha.Code = ViewState("CaptchaValue.Value")
        'End If
        IDCaptcha.Code = HiddenCaptcha.Text
        IDCaptcha.Width = "120"
        IDCaptcha.Height = "30"
        IDCaptcha.ForeColor = Drawing.Color.LightGray
        IDCaptcha.BackColor = Drawing.Color.White
        IDCaptcha.TextBackColor = Drawing.Color.DarkSlateGray
        IDCaptcha.TextForeColor = Drawing.Color.DarkSlateGray
    End Sub
    Function CheckCaptcha() As Boolean
        Dim result As Boolean = True
        success_txt.text = ""
        If (textCaptcha.Text <> ImageUtility.DecryptCaptchaCode(HiddenCaptcha.Text)) Then
            success_txt.Text = GetLabel("captcha_Error_")
            ', idLang, "The numeric code you have entered for the captcha protection is incorrect. Please retry."
            result = False
        End If
        
        Return result
    End Function
    
    Protected Sub InitView()
        hdnToken.Value = _token
        Session("token") = _token
        btnFarmacoV.Text = Me.BusinessDictionaryManager.Read("DMP_FarmaFormSend", _langKey.Id, "DMP_FarmaFormSend")
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        If contentId > 0 Then
            Dim contentextraVal As New ContentExtraValue
            contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
            If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.PrimaryKey > 0 Then
                'ltlTitle.Text = contentextraVal.Title
                ltlFulltext.Text = contentextraVal.FullText
            End If
        End If
       
    End Sub
    
    Protected Sub btnFarmacoV_Click(ByVal sender As Object, ByVal ev As EventArgs)
        If CheckCaptcha() Then
            Dim sessionToken As String = Session("token")
            If hdnToken.Value.Equals(sessionToken) Then
                Session.Remove("token")
            
                Dim replacing As New Dictionary(Of String, String)
                replacing.Add("[EMAIL]", txtEmail.Text)
                replacing.Add("[SUBJECT]", txtObject.Text)
                replacing.Add("[MESSAGE]", txtMessage.Text)
                replacing.Add("[TELEPHONE]", txtTel.Text)
                replacing.Add("[DRUG]", txtFarmaco.Text)
                replacing.Add("[FOOTER]", String.Empty)
            
                Dim htmlTemplate As String = Me.BusinessDictionaryManager.Read("DMP_htmlTemplate", _langKey.Id, "DMP_htmlTemplate")
            
                MailHelper.SendMessage(New MailTemplateIdentificator("Farmacovigilanza"), _langKey.Id, htmlTemplate, replacing, False)
            
                plhDone.Visible = True
                plhContactForm.Visible = False
                ltlFulltext.Visible = False
            End If
        End If
       CaptchaInfo()
        
    End Sub
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id, label)
    End Function
</script>
<style type="text/css">
    .form-container textarea
    {
        line-height: 20px !important;
        height:120px !important;
    }
</style>
<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/farmacovigilanza.jpg');">
            
        </div>    
         <div class="txt">
                <h1><%=Me.BusinessDictionaryManager.Read("theme_" & _themeKey.Id, _langKey.Id, "Farmacovigilanza")%></h1>          
         </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

<section>
    <div class="container">
         <div class="colLeft"><div class="listSide"><asp:PlaceHolder runat="server" ID="ph_RelatedContent"></asp:PlaceHolder></div> </div>
        <div class="colRight">
             <div class="content">
                 <div class="generic">
                 <asp:Literal ID="ltlFulltext" runat="server"></asp:Literal>
                      <div class="form-container">

                           <asp:PlaceHolder runat="server" ID="plhContactForm">
                        <div id="formCnt">
                	        <h3 style="margin-bottom:20px !important;"><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormTitle", _langKey.Id, "DMP_FarmaFormTitle")%></h3>


                            <div class="left-form">
                                <div class="generic">
                                    <span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormEmail", _langKey.Id, "DMP_FarmaFormEmail")%><span class="red">*</span></span>
                                    <asp:TextBox runat="server" ID="txtEmail" TextMode="SingleLine" ValidationGroup="farmacoV" TabIndex="1" />
                                </div>
                                <div class="generic">
                                    <span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormSubject", _langKey.Id, "DMP_FarmaFormSubject")%></span>
                                    <asp:TextBox runat="server" ID="txtObject" TextMode="SingleLine" ValidationGroup="farmacoV" TabIndex="2" />
                                </div>
                                <div class="generic">
                                    <span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormTel", _langKey.Id, "DMP_FarmaFormTel")%></span>
                                    <asp:TextBox runat="server" ID="txtTel" TextMode="SingleLine" ValidationGroup="farmacoV" TabIndex="3" />
                                </div>
                                 
                            </div>


                            <div class="right-form">
                                <div class="generic">
                                    <span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormFarmaco", _langKey.Id, "DMP_FarmaFormFarmaco")%><span class="red">*</span></span>
                                    <asp:TextBox runat="server" ID="txtFarmaco" TextMode="SingleLine" ValidationGroup="farmacoV" TabIndex="3" />
                                 </div>
                                <div class="generic">
                                    <span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormDesc", _langKey.Id, "DMP_FarmaFormDesc")%></span>
                                    <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" ValidationGroup="farmacoV" TabIndex="5" />
                                </div>
                            
                            </div>
                            <div class="generic">
                            <span class="generic noMargin"><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormPrivacy", _langKey.Id, "DMP_FarmaFormPrivacy")%><span class="red">*</span></span>

                            <textarea class="informativa" style="width:100% !important;" readonly="readonly"><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormPrivacyTxt", _langKey.Id, "DMP_FarmaFormPrivacyTxt")%> </textarea>

                            </div>
                            

                            <div class="generic marginBott20">
                                <p class="size14">
                    	        <input type="checkbox" class="contactCheck" style="width:auto;line-height:10px;margin:3px 3px 0;padding:0;float:left;height:auto;" id="chkPrivacy"/><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormPrivacyChk", _langKey.Id, "DMP_FarmaFormPrivacyChk")%>                                
                                </p>
                            </div>

                            <div class="generic size14">
                                <p style="padding-top:0px !important;"><span class="red">*</span><%=Me.BusinessDictionaryManager.Read("DMP_FarmaFormRequired", _langKey.Id, "DMP_FarmaFormRequired")%></p>
                                </div>

                            <div class="right-form" style="margin:0px;"> 
                                <div class="generic">                             
                                    <asp:placeholder ID="CaptchaPanel" runat="server">
                                    <div class="captcha">
                                    <span><%=GetLabel("Captcha")%> <span class="red">*</span></span>
                                        <br /><HP3:Captcha ID="IDCaptcha" runat="server" />
                                    <asp:TextBox ID="textCaptcha" runat="server" Width="165px" style="margin-right:10px;"  alt="Captcha code"></asp:TextBox>                	
                                    <asp:TextBox ID="HiddenCaptcha" Visible="false" runat="server" alt="Captcha code" />
                                    <div style="clear:both;float:left;width:100%; color:Red"> 
                                        <asp:Literal  ID="success_txt" runat="server"   >
                                        </asp:Literal>
                                    </div> 
                                    
                                    <asp:RequiredFieldValidator ID="ReqtextCaptcha" runat="server" ErrorMessage="*" ControlToValidate="textCaptcha" Font-Bold="True"></asp:RequiredFieldValidator>
                                    
                                    </div>
                                </asp:placeholder>  
                                    </div>                                
                            </div>

                            <div class="generic size14">                                
                                <div class="btn-big"><asp:LinkButton CssClass="btn" runat="server" ID="btnFarmacoV" OnClick="btnFarmacoV_Click"  CausesValidation="true"  TabIndex="6" Text="Invia" /></div>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="plhDone" Visible="false">
                        <h3><%=Me.BusinessDictionaryManager.Read("DMP_FarmacoVMailSent", _langKey.Id, "DMP_FarmacoVMailSent") %></h3>
                    </asp:PlaceHolder>
                    <asp:HiddenField runat="server" ID="hdnToken" />


                       </div>
                 </div>
             </div>
        </div>
    </div>
</section>
    
  

<script type="text/javascript">

    $(document).ready(function () {

        var button = $("#formCnt .btn");

        var email = $("#<%=txtEmail.ClientID %>");
        var farmaco = $("#<%=txtFarmaco.ClientID %>");
        var chkprivacy = $("#chkPrivacy");
        var emailPattern = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var _langId = '<%=_langKey.Id%>';
        var captcha = $("#<%=textCaptcha.ClientID%>");


        if (_langId == 1) {
            _strCheck = '\n- L\'accettazione del disclaimer è obbligatoria.';
            _strEmail = '\n- Il campo Email è obbligatorio.';
            _strEmail2 = '\n- Controllare il valore immesso nel campo email.';
            _strFarmaco = '\n- Il campo Farmaco è obbligatorio.';
            _strCaptcha = '\n- Il campo Captcha è obbligatorio.';
        }
        if (_langId == 2) {
            _strCheck = '\n- Disclaimer acceptance is mandatory.';
            _strEmail = '\n- Email is mandatory.';
            _strEmail2 = '\n- Please check Email format.';
            _strFarmaco = '\n- Drug is mandatory.';
            _strCaptcha = '\n- Captcha is mandatory';
        }
        if (_langId == 3) {
            _strCheck = '- Pranimi i përjashtimit nga përgjegjësia është i detyrueshëm.';
            _strEmail = '\n- Elementi Email është i detyrueshëm.';
            _strEmail2 = '\n- Kontrollo saktësinë e email-it të futur.';
            _strFarmaco = '\n- Elementi drogë është i detyrueshëm.';
            _strCaptcha = '\n- Elementi Captcha është i detyrueshëm.';
        }		

		if (_langId == 4) {
            _strCheck = '- Es obligatorio aceptar el descargo de responsabilidad.';
            _strEmail = '\n- El campo Correo electrónico es obligatorio.';
            _strEmail2 = '\n- Compruebe el dato indicado en el campo Correo electrónico.';
            _strFarmaco = '\n- El campo fármaco es obligatorio.';
            _strCaptcha = '\n- El campo Captcha es obligatorio.';
        }


        button.click(function () {
            _outMessage = '';
          /*  if ($.trim(email.val()) == '') {
                alert("email empty");
                return false;
            }

            if (!emailPattern.test(email.val())) {
                alert("invalid email format");
                return false;
            }

            if ($.trim(farmaco.val()) == '') {
                alert("error farmaco");
                return false;
            }

        
            if (!chkprivacy.val()) {
                alert("error privacy");
                return false;
            }*/



           

            if ($.trim(email.val()) == '') {
                _outMessage = _strEmail;
            }

            if (!emailPattern.test(email.val())) {
                _outMessage += _strEmail2;
            }

            if ($.trim(farmaco.val()) == '') {
                _outMessage += _strFarmaco;
            }

            if ($.trim(captcha.val()) == '') {
                _outMessage += _strCaptcha;
            }
            if (!chkprivacy.attr("checked")) {
                _outMessage += _strCheck;
            }

            if (_outMessage != '') {
                alert(_outMessage);
                return false;
            }


        });

    });

</script>