﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Dompe.Extensions"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Dim _strAddQs As String = String.Empty
    Dim titolo As String = ""
    Dim testoRcp As String = ""
    Dim cover As String = ""
    Dim abstract As String = ""
    Dim productDocument As String = ""
    Dim productMecAz As String = ""
    Dim nomeBrand As String = ""
    Dim _contColor As String = ""
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        
    End Sub

    Sub Page_Load()
		
        If Not Integer.TryParse(Request("intRCP"), contentId) Then
            plhContentGeneric.Visible = False
            plh_noaccess.Visible = True
            ltl_msg.Text = "Contenuto inesistente!"
            ltl_msg2.Text = "Il link utilizzato non è corretto."
        Else
        
            'If Me.BusinessAccessService.IsAuthenticated Then
            'visualizza rcp
           
            Dim man As New ContentExtraManager()
            Dim cntRCP As ContentExtraValue
            Dim cntRCPColl As ContentExtraCollection
            Dim cntRCPSrc As New ContentExtraSearcher
            cntRCPSrc.key.PrimaryKey = contentId
            cntRCPSrc.KeyContentType.Id = 152
            cntRCPColl = man.Read(cntRCPSrc)
            
            If Not cntRCPColl Is Nothing AndAlso cntRCPColl.Count > 0 Then
                cntRCP = cntRCPColl(0)
                Dim RcpRelType As Integer = 10
                Dim cntRelSrc As New ContentRelationSearcher
                cntRelSrc.KeyContentRelated.PrimaryKey = cntRCP.Key.PrimaryKey
                cntRelSrc.KeyRelationType.Id = RcpRelType
               
                Dim coll = Me.BusinessContentManager.ReadContentRelation(cntRelSrc)
                
                If Not coll Is Nothing AndAlso coll.Count > 0 Then
                    Dim brand As New ContentExtraValue
                    brand = Me.BusinessContentExtraManager.Read(coll(0).KeyContent)
                    
                    Dim styleElements = brand.BookReferer.Split("|")
                    If Not styleElements Is Nothing AndAlso styleElements.Any() Then
                        _contColor = styleElements.FirstOrDefault()
                    End If
                    
                    
                    nomeBrand = brand.Title
                End If
                
                
                titolo = cntRCP.Title
                testoRcp = cntRCP.FullText
                abstract = cntRCP.Abstract
                cover = Healthware.Dompe.Helper.Helper.GetCover(cntRCP.Key, 0, 0)
                productDocument = getProductDocument(cntRCP)
                productMecAz = getProductMecAz(cntRCP)
                
                plh_noaccess.Visible = False
                plhContentGeneric.Visible = True
                
                TraceData(Me.ObjectTicket.User.Key.Id, 56, cntRCP.Key.Id)
            Else
                plhContentGeneric.Visible = False
                plh_noaccess.Visible = True
                ltl_msg.Text = "Contenuto inesistente!"
                ltl_msg2.Text = "Il link utilizzato non è corretto."
            End If
            
            '    Else
            '    plhContentGeneric.Visible = False
            '    plh_noaccess.Visible = True
            '    ltl_msg.Text = "Contenuto non disponibile"
            '    ltl_msg2.Text = "Per accedere al contenuto richiesto è necessario <a href='/QRAccess/?idrcp=" & Request("intRCP") & "'>loggarsi</a>."
            'End If
            
        End If
        
    End Sub
  
    Sub TraceData(ByVal idU As Integer, evID As Integer, ByVal cntID As Integer)
        Dim trman As New TraceEventManager
       
        Dim tv As New TraceValue
        tv.DateInsert = DateTime.Now
        tv.Description = "Accesso diretto a RCP" 'Me.BusinessDictionaryManager.Read("profile_traceDescription", idLang, "Modify Data")
        tv.KeyEvent.Id = evID
        tv.KeyUser.Id = idU
        tv.KeyContent.Id = cntID
        tv.DateInsert = DateTime.Now
        tv.KeySite.Id = Me.PageObjectGetSite.Id
        tv.KeyContentType = Me.PageObjectGetContentType
        tv.KeySiteArea = Me.PageObjectGetSiteArea
        tv.KeyLanguage.Id = Me.PageObjectGetLang.Id
        tv.IdSession = Me.ObjectTicket.IdSession
        tv.CustomField = "intRCP=" & Request("intRCP")
        
        Me.BusinessTraceService.Create(tv)
        
       
    End Sub
    
    Function getProductMecAz(ByVal cnt As ContentExtraValue) As String
        Dim man As New ContentExtraManager()
        Dim so As New ContentExtraSearcher()
        Dim collMecAz = man.ReadContentExtraRelation(New ContentIdentificator(cnt.Key.Primarykey), New ContentRelationTypeIdentificator(Dompe.RelationType.MeccanismoDazione))
        If Not collMecAz Is Nothing AndAlso collMecAz.Any() Then Return String.Format("<h4>{0}</h4><p>{1}</p>", collMecAz(0).Launch, collMecAz(0).FullText)

        Return String.Empty
    End Function
	
    Function getProductDocument(ByVal cnt As ContentExtraValue) As String
        Dim so As New DownloadSearcher()
        so.key.Id = cnt.Key.Id
        so.key.IdLanguage = 1 'Me.PageObjectGetLang.Id
        so.ApprovalStatus = 1
		
        Dim collMaterial As DownloadCollection = Me.BusinessContentManager.ReadDownloadRelation(so)
        If Not collMaterial Is Nothing And collMaterial.Count() > 0 Then
            For Each element As DownloadTypeValue In collMaterial(0).DownloadTypeCollection
                Return "<a title='Download " & cnt.Title & "' target='_blank' href='/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(element.KeyContent.PrimaryKey) & "' style=""font-size: 20px;font-weight: bold;"">" & cnt.Title & Healthware.Dompe.Helper.Helper.checkApproval(cnt.ApprovalStatus) & "</a><p>&nbsp;</p>"
            Next
        Else
            Return String.Empty
        End If
        Return String.Empty
    End Function
</script>
<div id="DivImagesTop" class="" runat="server"></div>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
<div class="body">
    <div class="box-container titleSection" style="background-image: url('/ProjectDOMPE/_slice/bgTopBrand.jpg');">
        <div class="boxCnt">
            <div class="container">
                <h2 style="color: #333333;"><%=nomeBrand%></h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="colRight">
        <div class="bgAccBrand" style="background-color:<%=_contColor%>">
            <h3 style="font-size:25px !important;padding:25px 2px 0 95px !important;"><%=titolo%></h3>

        </div>

        <p style='text-align:center;'><img src='/HP3Image/cover/<%=cover%>'/></p>        
        <%=abstract%>
        <%=testoRcp%>
        <%=productDocument%>
        <%=productMecAz%>
        </div>
        <div class="colLeft">
        	<div class="boxBlueDett bigBoxBlueDett" style="min-height: 440px;">
            	<div class="listSide">
                    </div>
                </div>
            </div>
    </div>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="plh_noaccess" runat="server" Visible="false">
    <div class="body">
    <div class="box-container titleSection" style="background-image: url('/ProjectDOMPE/_slice/bgTopBrand.jpg');">
        <div class="boxCnt">
            <div class="container">
                <h2 style="color: #333333;"><asp:Literal ID="ltl_msg" runat="server"></asp:Literal></h2>
            </div>
        </div>
    </div>   
    <div class="container">
        <div class="colRight">
            <p style='text-align:center;'><asp:Literal ID="ltl_msg2" runat="server"/></p>
        </div>
        <div class="colLeft">
        	<div class="boxBlueDett bigBoxBlueDett">
            	<div class="listSide">
                    </div>
                </div>
         </div>
    </div>
</div>

</asp:PlaceHolder>
