﻿<%@ Control Language="VB" ClassName="Literature" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.DMP.BL" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>


<script runat="server">
    'TODO: Generalizzare gli errori a livello di dictionary
    ' Aggiungere anche Approval
    
    Dim operazione As Integer
    Protected _siteFolder As String = String.Empty
    Protected _emptyLiteratureArchiveTemplate As String = String.Empty
    Protected _ajaxLiteraturePage As String = String.Empty
    Protected _ajaxTagsPage As String = String.Empty
    Protected _siteArea As Integer = 0
    Protected _brand As Integer = 0
    Protected _bitMask As String = String.Empty
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _userKey As UserIdentificator = Nothing
    Protected _topKeys As String = String.Empty
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _geo As Integer = 0
    Dim _MaxItem As Integer
    Protected strSezione As String = ""
    Protected _notAvaible As String = ""
    Dim phG As New ContentExtraValue
    Dim pkLast As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _MaxItem = 8
        _siteFolder = Me.ObjectSiteFolder
        '  _bitMask = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BitMaskAree 'dm Global Variable
        '  _brand = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BrandCurrent
        _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
        _ajaxLiteraturePage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _ajaxTagsPage = String.Format("/{0}/HP3Handler/Tags.ashx", _siteFolder)
        _siteArea = Me.PageObjectGetSiteArea.Id
        _keyContentType = Me.PageObjectGetContentType
        _userKey = Me.ObjectTicket.User.Key
        _langKey = Me.PageObjectGetLang
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        _contentKey = Me.PageObjectGetContent
    End Sub
    
    Dim _CheckApproval As Integer = 1
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
        
        operazione = CInt(Keys.EnumeratorKeys.LiteratureViewType.List)
        If _siteArea = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress Then operazione = CInt(Keys.EnumeratorKeys.LiteratureViewType.ViewSliderPhotoGallery)
        
        Dim idContentTheme As Integer = 0
        Dim idContent As Integer = Me.PageObjectGetContent.Id
        
        plhArchive.Visible = True
        phG = ReadLastPhotoG()
        If Not phG Is Nothing Then            
            pkLast = phG.Key.PrimaryKey
            phG.LinkUrl = "/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l=" & getCodeLanguage(phG.Key.IdLanguage) & "&inpage=1&c=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(phG.Key.Id)
                
        End If
            
        If _siteArea = Dompe.SiteAreaConstants.IDComunicatiStampaAreaMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDSiteareaNewsMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDRassegnaStampaAreaMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDSchedeIstituzionaliMediaPress Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressComunicati.htm", _siteFolder)
            strSezione = "PressComunicati"
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDSiteareaLinksMediaPress Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressLink.htm", _siteFolder)
            strSezione = "PressLink"
        ElseIf _siteArea = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressPhotoGallery.htm", _siteFolder)
            strSezione = "PressPhotoGallery"
            _MaxItem = 8
        Else
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
            strSezione = "PressArchive"
        End If
        
        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        
        _notAvaible = Me.BusinessDictionaryManager.Read("DMP_notavaible", _langKey.Id, "DMP_notavaible")
        
        
        Dim so As New ThemeSearcher
        Dim coll As ThemeCollection = Nothing
        so.KeySite = Me.PageObjectGetSite
        so.KeyFather.Id = Dompe.ThemeConstants.idAreaPress
        so.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        coll = Me.BusinessThemeManager.Read(so)
           
        rptSiteMenu.DataSource = coll
        rptSiteMenu.DataBind()
        'End If
    End Sub
    
    Public Shared Function getCodeLanguage(ByVal idL As Integer) As String
        Dim strFinal As String = "it"
        If idL = 2 Then strFinal = "en"
		If idL = 3 Then strFinal = "alb"
		If idL = 4 Then strFinal = "es"
        Return strFinal
    End Function
    
    Private Function ReadLastPhotoG() As ContentExtraValue
        Dim so As New ContentExtraSearcher()
        
        Dim phGColl As New ContentExtraCollection
        '     Dim strBitmask = data.BitMask.Split(Healthware.DMP.Keys.LabelKeys.DEFAULT_SEPARATOR_COMMA)

        Dim sum As Integer = 0
        Dim tmpInt As Integer = 0

        so.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        so.KeyContentType.Id = Me.PageObjectGetContentType.Id
        so.key.IdLanguage = Me.PageObjectGetLang.Id
     
        If Me.PageObjectGetContent.Id > 0 Then
            so.key = Me.PageObjectGetContent
        End If
        'gestione geo solo per utenti con ruolo Geo
        Dim manager As New AccessService()
        'Dim ticket = manager.GetTicket()
        'Dim userKey = ticket.User.Key

        'dm comuni a tutte le chiamate , nel caso generalizzare
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SortOrder = ContentGenericComparer.SortOrder.DESC


        so.PageNumber = 1
        so.PageSize = 1

        so.Properties.Add("Title")
        so.Properties.Add("Launch")
        so.Properties.Add("DatePublish")
        so.Properties.Add("ContentType.Key.Id")
        so.Properties.Add("ContentType.Key.Name")
        so.Properties.Add("ContentType.Key.Domain")
        so.Properties.Add("FullText")
        so.Properties.Add("LinkUrl")
        so.Properties.Add("DateExpiry")
        so.Properties.Add("LinkLabel")
        so.Properties.Add("Code")
        so.Properties.Add("Copyright")
        so.Properties.Add("SiteArea.Key.Id")
        so.Properties.Add("BookReferer")
        so.Properties.Add("FullTextComment")


        phGColl = Me.BusinessContentExtraManager.Read(so)
        
        If Not phGColl Is Nothing AndAlso phGColl.Count > 0 Then
            Return phGColl(0)
        Else
            Return Nothing
        End If
        
        
    End Function
    
  
    
    Protected Sub InitDetailView()
       
        Dim path As String = String.Format("/{0}/HP3Service/Press_DetailContent.ascx", _siteFolder)
        'LoadSubControl(path, plhDetail)
        ' plhArchive.Visible = False
        ' plhDetail.Visible = True
              
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub

    Protected Sub InitView()
        
        ' _topKeys = GetTopItemsKeys()
        
        ' plhDetail.Visible = False
        ' plhArchive.Visible = True
        
          
        '  InitAreaFilters()
    End Sub
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
  
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
  
</script>

<asp:PlaceHolder ID="plhArchive" runat="server">
<script language="javascript" type="text/javascript">
    var templateUrl = "<%=_emptyLiteratureArchiveTemplate%>";
    var ajaxLiteraturePage = "<%=_ajaxLiteraturePage%>";
    var nPage = 1;
    var _loadingLibrary = false; 
    var _pageSize = <%=_MaxItem %>;
    var _defaultBitmask = '<%=_bitMask%>';
    var _currentBitmask = _defaultBitmask;
    var _defaultKeysToExclude = "<%=_topKeys%>";
    var _loadingStatus = false;
   
   $(document).ready(function () {
        FilterApply();

      });

   var yearsList=$('.yearHidden');

  function FilterApply(){
    nPage = 1;
    _defaultKeysToExclude = "<%=_topKeys%>";
    //alert("--- key " + _defaultKeysToExclude)
    $(".notavaible").remove();
    $(".archiveCnt").remove();
   
     var so = prepareSearcherLiterature('', _defaultBitmask, _defaultKeysToExclude);
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
     InitAutoCompleater();
      InitSearch();

   }

    function yearsOpen(){
        
		if(yearsList==0){
			$('.yearsList').css('display','block');
			yearsList=1;
		}else{
			//$('.yearsList').css('display','none');
			yearsList=0;
		}
	}
	
    function selYear(id){
        
		//$('.yearsList').css('display','none');
		yearsList=0;
		$('.yearSearch').html(id);
		$('.yearHidden').val(id);
	}


    function GetTags()
    {
        var tags = '';             
        $("ul#tagsView li").each(function(){
            tags += $(this).attr('data-tag') + ',' || '';
        });

        return tags;
    }

    function InitSearch(){
        $('.filterBtn #btnTagSearch').click(function(){
            DoSearch();
        });
    }

    function DoSearch()
    {
        _loadingStatus=false;
        nPage=1;
        $("#literatureArchive").empty();            
        var tags = GetTags();                                    
        var so = prepareSearcherLiterature(tags, _currentBitmask, '')
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
    }

    function InitAutoCompleater(){      

        var searchField = $('.bgFilter #tagSearch');
        searchField.val('');
        searchField.keyup(function (e) {                    
           ReadTags();        
        });      

        searchField.focusout(function(e){
            searchField.val('');    
        });
        
    }

    function ReadTags(){
        
        var postData = {
                    op: '<%=Keys.EnumeratorKeys.TagOperationType.List %>',
                    search: $('.bgFilter #tagSearch').val(),
                    limit: '50'  
                };

        $('.bgFilter #tagSearch').autocomplete({
            minLength:2,
            focus: function(event, ui){                
                return false; 
            },
            select: function(event, ui){                           

                $("#tagsView").append("<li data-tag=" + ui.item.value + ">" + ui.item.label + "<a id='removeTag' title='Delete'>X</a></li>");
                $("ul#tagsView li[data-tag="+ ui.item.value +"] a").click(function(){
                    $(this).closest('li[data-tag='+ ui.item.value +']').remove();
                    DoSearch();
                });              
               
               $("ul#tagsView li[data-tag="+ ui.item.value +"]").hover(
                function(){
                    $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").removeClass("hidden");
                },
                function(){
                    $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").addClass("hidden");
                }
               );

                DoSearch();

                $("#tagSearch").val('');
                return false;            
                    
            },
            source: function(request, response){

             $.ajax({
                type: "POST",
                url: '<%=_ajaxTagsPage %>',            
                data: postData,
                success: function( data ) {
                    if (data){
                        if (data.Success) {        
                            if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %>')
                            {
                                response( $.map( data.Result, function( item ) {
                                    return {
                                        label: item.TagName,
                                        value: item.TagKey

                                    }
                                }));                            
                            } else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %>')
                            {
                                response('','');
                            }
                        }
                        else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                            document.location.reload();
                        }
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //alert(textStatus);
                }
     
            });
        }
        });
    }

    function InitReadMore()
    {
    
//        $('#moreInfo').on('click', function () {            
            ++nPage;
            var keysToExclude = _defaultKeysToExclude;
            var tags = GetTags();            
            var contentSearcher = prepareSearcherLiterature(tags, _currentBitmask, keysToExclude);
            Literature.read(contentSearcher);
            return false;
//        });  
    }

    var ReadLiteratureCallback = function (data) {  
        
        if(data){
            if (data.Success) {        
                if (data.StatusCode == "<%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %>" )
                {     
                    _loadingStatus = true;
                    var container = $("#literatureArchive");
                    container.html('');
                    $.tmpl("ArchiveTemplate", data.Result.Collection).appendTo(container);
                
                    if(data.Result.PageNumber == data.Result.PageTotalNumber)
                       {  
                       $('#moreInfo').hide();
                       $('#moreInfoBar').hide();
                       }
                       
                    else
                       { 
                       $('#moreInfo').show();
                       $('#moreInfoBar').show();
                       }
                    

                } else if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %> )
                {
                    $('#moreInfo').hide();
                    $('#moreInfoBar').hide();
                   // if(!_loadingStatus)
                        $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
                }
            }
            else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                document.location.reload();
            }
        }
        else
            {
                $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
            }      
       
    };

    var OnReadLiteratureArchiveCompleteCallback = function () {
             
        $('#LiteratureArchiveLoader').hide();
        _loadingLibrary=false;
        $("#literatureArchive").fadeIn('fast');        
        
        $(".singleMiniGallery a").each(function(index) {
            
            $(this).click(function(){
                $("#contGallery").attr('src', $(this).attr('data-gallery'));
                var parentContainer = $(".singleMiniGallery[id=" + $(this).attr('data-cont') + "]");
                parentContainer.hide();
                
                var swapCont = $(".singleMiniGallery[id=" + $("iframe#contGallery").attr("data-cont") + "]");
                swapCont.fadeIn('fast');
                
                $("iframe#contGallery").attr("data-cont", $(this).attr('data-cont'));

            });
           

        });
        
        $(".singleMiniGallery[id=" + $("iframe#contGallery").attr("data-cont") + "]").hide();
    };


    function prepareSearcherLiterature(TagsP, bitmask, keysToExclude) {
        var year = $(".yearSearch").html();
        var word = $("#tagSearchArchivio").val();
        var searcher = {          
            SiteareId: '<%=_siteArea%>',
            ContenTypeId: '<%=_keyContentType.Id%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=_themeKey.Id %>',
            Brand: '',
            BitMask: bitmask,
            Display: true,
            Active: true,
            Delete: false,
            ApprovalStatus:'<%=_CheckApproval%>',
            Year: year,
            Word: word,
            Tags:'',
            PageNumber:nPage,
            PageSize:_pageSize            
        };

        recordOutboundFull("Archivio <%= strSezione %>","Caricamento","page: "+ nPage)
        if(TagsP != '')
             searcher.Tags =  TagsP;


//        if(CountriesP != '')
//            searcher.Countries =  CountriesP;
                    
        return searcher;

    };

    function LoadLiteratureArchive(contentSearcher, callback, onCompleteCallback)   //old LoadProfile
    {
        
        $('#LiteratureArchiveLoader').show();
             $('#moreInfo').hide();
           $('#moreInfoBar').hide();     
        var postData = {
            op: '<%=Cint(operazione)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: ajaxLiteraturePage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {
                if (typeof callback === "function") {
                callback(data);
                }
                   $(".archiveCnt").hover(
			function () {
				$(this).children('.effectHover').css('display','none');
			},
			function () {
				$(this).children('.effectHover').css('display','block');
			}
		);
       },
            error: function (data) {
               //alert("error")
               //$("#literatureArchive").fadeIn('fast');
            }
        }).complete(function () {            
            if(onCompleteCallback && typeof(onCompleteCallback) == 'function') onCompleteCallback();
            //if (typeof onCompleteCallback === "function") {
           // onCompleteCallback();
           // }
        }); 
    }


    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {

            $.template("ArchiveTemplate", template);

            //se non è LoaderFunction nothing
            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    var Literature = Literature || new function (){
           

        this.read = function (contentSearcher) {
            if (true == _loadingLibrary) return;

            _loadingLibrary = true;

            $('#LiteratureArchiveLoader').show();
            $('#moreInfo').hide();
            $('#moreInfoBar').hide();
            //var container = jQuery('#literatureArchive');  
            
            LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        };
    }

    function FilterArea(item){
        
        _loadingStatus=false;
        $('.contentHome .filter .aree a[data-mask='+ item +']').toggleClass('sel');
        nPage=1;

         var bitmask = GetBitmask();
         _currentBitmask = bitmask;
        
        var tags = GetTags();  

        var keysToExclude = '';
        var contentSearcher = prepareSearcherLiterature(tags, bitmask, keysToExclude);
        $("#literatureArchive").empty();
        LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback)
     }

    function GetBitmask()
    {
       var mask = '';
       $('.contentHome .filter .aree a.sel').each(function(){
            mask += $(this).attr('data-mask') + ',' || '';
       });

       if(mask == '' ) {
           mask = _defaultBitmask;
        }

        return mask;
    }
        
          function SdwbOK(urlLink,wdt,hig) 
    {
          
        //recordOutboundLink(urlLink, "Press Gallery", "Open", "Press articoli")
         Shadowbox.open({
         content:    urlLink,
        player:     "iframe",      
        height: hig,
        width:  wdt
        });


        }
</script>

    <section>
        <div class="cont-img-top">
    	    <div class="bg-top gallerySection">                
            </div>    
            <div class="txt">
                   <h1 class="grey">Photogallery</h1>       
            </div>
            <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    </section>

 	<section>
    <div class="container">

        <div class="colLeft">
        	
            	<div class="listSide">
                	
                 <asp:Repeater ID="rptSiteMenu" runat="server">
                    <HeaderTemplate><ul></HeaderTemplate> 
                    <ItemTemplate>
                        <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# DataBinder.Eval(Container.DataItem, "Description")%></a></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>  
                      
                </div><!--end listSide-->
            
        </div><!--end colLeft-->


        <div class="colRight" style="padding-top:0px;">
            <% If Not phG Is Nothing Then%>
            <iframe data-cont="<%=phG.Key.Id %>" src="<%=phG.LinkUrl%>" id="contGallery" frameborder="0" width="600" height="610" style="border:0px;" ></iframe>
           <%--<p><%=phG.Launch%></p>--%>
            <% End If %>
        
            <div class="colAllFixed gallery">
        	    <h3><%=Me.BusinessDictionaryManager.Read("Altre gallery", _langKey.Id)%></h3>
                <div class="generic">
                     <div id="literatureArchive"></div>                
                </div>
            </div>
        </div><!--end colRight-->
        
    </div><!--end container-->
    </section>


</asp:PlaceHolder>

<asp:PlaceHolder ID="plhDetail" runat="server" />


<script type="text/javascript">
    $(document).ready(function(){


            $('.singleEventsInt').live('mouseover mouseout', function(event) {
                if (event.type == 'mouseover') {
                    $(this).find('.txtEvInt').css('display','none');
                    $(this).find('.goEv').css('display','block');                    
                    $(this).find('.img').children().animate({width: "500px", left:"-90px", top:"-20px"}, 300);
                } else {
                    $(this).find('.txtEvInt').css('display','block');
                    $(this).find('.goEv').css('display','none');                   
                    $(this).find('.img').children().animate({width: "140px", left:"0px", top:"0px"}, 300);
                }
            });

    });
    </script>