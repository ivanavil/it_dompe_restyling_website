﻿<%@ Control Language="VB" ClassName="videoTutorials" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _site As SiteIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentType As ContentTypeIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Dim ltlimage As String = "/Dompe/_swf/image/video.jpg"
    Dim hasApple As Boolean = False
    Dim videoPK As Integer = 4
    Dim pageSize As Integer = 4
    Dim _strFollowUs As String = String.Empty
   
    Dim _defSiteLanguage As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _langKey = Me.PageObjectGetLang
        _site = Me.PageObjectGetSite
        _siteArea = Me.PageObjectGetSiteArea
        _contentType = Me.PageObjectGetContentType
        _currentTheme = Me.PageObjectGetTheme
        _defSiteLanguage = Me.ObjectSiteDomain.DefLanguage.Id
    End Sub
    
    Sub Page_Load()
        Dim userAgent As String = HttpContext.Current.Request.UserAgent.ToLower()
        If (userAgent.Contains("iphone") Or userAgent.Contains("ipad")) Then
            hasApple = True
        End If
     
        If Me.PageObjectGetContent.Id > 0 Then
            Dim collCont As New ContentValue
            videoPK = Me.PageObjectGetContent.Id
            Try
                collCont = Me.BusinessContentManager.Read(New ContentIdentificator(videoPK, Me.PageObjectGetLang.Id))
                urlYT.Value = collCont.LinkUrl
            Catch ex As Exception
            End Try
        End If
		
        Select Case Me.PageObjectGetLang.Id
            Case 1
                _strFollowUs = "Seguici su "
            
            Case 2
                _strFollowUs = "Follow Us on "
            
            Case 3
                _strFollowUs = "Ndiqni ne në "
            Case 4
                _strFollowUs = "Siganos en "				
        End Select
    End Sub
     
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
   
    Function GetLink(ByVal video As ContentValue) As String
        If Not video Is Nothing AndAlso video.Key.Id > 0 Then
            Dim theme As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            If Not theme Is Nothing AndAlso theme.Key.Id > 0 Then
                'theme.KeyContent = video.Key 'BUG
                Return Me.BusinessMasterPageManager.GetLink(video.Key, theme, False)
            End If
        End If

        Return String.Empty
    End Function
       
    
    Function GetLink(ByVal link As String) As String
        If Not String.IsNullOrEmpty(link) Then
            If Not link.ToLower.Contains("http://") Then
                Return "http://" & link
            End If
           
            Return link
        End If
      
        Return String.Empty
    End Function
  
    Function GetLink1(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(200), Me.PageObjectGetMasterPage, False)
        'End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    
</script>

<style type="text/css">
#listVideos
    {
        clear:both;float:left;width:100%;
    }
.Bg .Qo
{
 	clear:both !important;
	float:left !important;
	width:20px !important;
	height:20px !important;
	padding:0;
    background-image: url("../_slice/google.gif") !important;
    background-position: left top !important;
    background-repeat: no-repeat !important;   
}
</style>

 
<%--<script type="text/javascript" src='http://connect.facebook.net/en_US/all.js'></script>--%>
  
<script type="text/javascript">
    
    function fbs_click() {
        var url = $("#<%= urlYT.ClientId %>").val();
        if (url != "") {
          u = url
        }else{
            u = location.href;
        }
        t = document.title;
        window.open('http://www.facebook.com/sharer.php?u=' +
             encodeURIComponent(u) + '?pk_campaign=fb' +
             '&t=' +
             encodeURIComponent(t),
             ' sharer', 'toolbar=0, status=0, width=626, height=436');
        return false;
    }

    function twt_click() {
        var url = $("#<%= urlYT.ClientId %>").val();
        if (url != "") {
            u = url
        } else {
            u = location.href;
        }

        t = document.title;
        window.open('https://twitter.com/intent/tweet?url=' +
             encodeURIComponent(u) + '?pk_campaign=twitter' +
             '&text=' +
             encodeURIComponent(t) + '&hashtags=dompe',
             ' sharer', 'toolbar=0, status=0, width=626, height=436');
        return false;
    }

//    function mail_click() {
//        u = location.href;
//        window.open('/ProjectDOMPE/_async/asyncMail.aspx?link=' +
//                   encodeURIComponent(u),
//             ' sharer','toolbar=0, status=0, width=602, height=540');
//        return false;
//    }

   
</script>
  <div class="followUs">
    <span><%= Me.BusinessDictionaryManager.Read("DMP_Seguici", _langKey.Id, "DMP_Seguici")%></span>
    <input type="hidden" id="urlYT" value="" runat="server" />
    <a class="linkedin" href="http://www.linkedin.com/company/68249?trk=tyah&trkInfo=tas%3Adomp%2Cidx%3A1-1-1"  target="_blank" title="<%=_strFollowUs%> LinkedIn">LinkedIn</a>                   
    <a class="youtube" href="https://www.youtube.com/channel/UC2tH5SZVHBo-j5_lCmghncA"  target="_blank" title="<%=_strFollowUs%> YouTube">YouTube</a>                   
    <script type="text/javascript">    var addthis_config = { "data_track_addressbar": false };</script>                
</div>
