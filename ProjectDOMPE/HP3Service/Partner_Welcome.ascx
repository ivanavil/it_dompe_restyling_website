﻿<%@ Control  Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<script runat="server">
 Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
    Private Dict As New DictionaryManager
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
  
    End Sub
        
    
    Sub Page_Load()
    end sub

    Function GetLink(ByVal id As Integer) As String
        
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, id, _langKey.Id)
       
    End Function
    </script>


<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/areaMedica.jpg');">            
        </div>    
         <div class="txt">
                <h1 class="grey"><%=Dict.Read("AreaPrtnr", _langKey.Id)%></h1>          
            </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <div class="breadcrumbs">
        <ul>
        	<li><a href="#" title="Home">Home</a><span><img src="/ProjectDOMPE/_slice/arr-bc.jpg" /></span></li>
        	<li><a href="#" title="<%=Dict.Read("AreaPrtnr", _langKey.Id)%>>"><%=Dict.Read("AreaPrtnr", _langKey.Id)%></a></li>
        </ul>
    </div><!--end breadcrumbs--> 
</section>
<section>
    <div class="container">
        <div class="box-level">            
        	<img src="/ProjectDOMPE/_slice/bg-on1.jpg" />
            <a href="<%=GetLink(Dompe.ThemeConstants.partnerAboutUs)%>">
                <div class="cont-box">
                    <h2><%=Dict.Read("thm_" & Dompe.ThemeConstants.partnerAboutUs, _langKey.Id)%></h2>
                    <p></p>
                    <p class="go"><img src="/ProjectDOMPE/_slice/go-small.png" /></p>
                </div>
             </a>   
        </div><!--end box-level-->

        <div class="box-level">            
        	<img src="/ProjectDOMPE/_slice/bg-on1.jpg" />
            <a href="<%=GetLink(Dompe.ThemeConstants.partnerFocusOn)%>">
                <div class="cont-box">
                    <h2><%=Dict.Read("thm_" & Dompe.ThemeConstants.partnerFocusOn, _langKey.Id)%></h2>
                    <p></p>
                    <p class="go"><img src="/ProjectDOMPE/_slice/go-small.png" /></p>
                </div>
             </a>   
        </div><!--end box-level-->

        <div class="box-level">            
        	<img src="/ProjectDOMPE/_slice/bg-on1.jpg" />
            <a href="/news-en/">
                <div class="cont-box">
                    <h2>News</h2>
                    <p></p>
                    <p class="go"><img src="/ProjectDOMPE/_slice/go-small.png" /></p>
                </div>
             </a>   
        </div><!--end box-level-->

        <div class="box-level">            
        	<img src="/ProjectDOMPE/_slice/bg-on1.jpg" />
            <a href="<%=GetLink(Dompe.ThemeConstants.partnerMedicalInfo)%>">
                <div class="cont-box">
                    <h2><%=Dict.Read("thm_" & Dompe.ThemeConstants.partnerMedicalInfo, _langKey.Id)%></h2>
                    <p></p>
                    <p class="go"><img src="/ProjectDOMPE/_slice/go-small.png" /></p>
                </div>
             </a>   
        </div><!--end box-level-->

    </div>
</section>

