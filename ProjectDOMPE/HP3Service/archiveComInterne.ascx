<%@ Control Language="VB" ClassName="Literature" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.DMP.BL" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>


<script runat="server">
    'TODO: Generalizzare gli errori a livello di dictionary
    ' Aggiungere anche Approval
    
    Protected _siteFolder As String = String.Empty
    Protected _emptyLiteratureArchiveTemplate As String = String.Empty
    Protected _ajaxLiteraturePage As String = String.Empty
    Protected _ajaxTagsPage As String = String.Empty
    Protected _siteArea As Integer = 0
    Protected _brand As Integer = 0
    Protected _bitMask As String = String.Empty
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _userKey As UserIdentificator = Nothing
    Protected _topKeys As String = String.Empty
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _geo As Integer = 0
    Const _MaxItem As Integer = 10
    Protected _lancioThema As String = ""
    Protected strSezione As String = ""
    Protected _cssInlineCode As String = String.Empty
    Protected _notAvaible As String = ""
    Private _isMobile As Boolean = False
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        
        _siteFolder = Me.ObjectSiteFolder
        '  _bitMask = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BitMaskAree 'dm Global Variable
        '  _brand = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BrandCurrent
        _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/Archive.htm", _siteFolder)
        _ajaxLiteraturePage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _ajaxTagsPage = String.Format("/{0}/HP3Handler/Tags.ashx", _siteFolder)
        _siteArea = Me.PageObjectGetSiteArea.Id
        _keyContentType = Me.PageObjectGetContentType
        _userKey = Me.ObjectTicket.User.Key
        _langKey = Me.PageObjectGetLang
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        _contentKey = Me.PageObjectGetContent
    End Sub
	
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    Dim _CheckApproval As Integer = 1
	
	
	Private Function CheckIpAddress() As Boolean
		Dim _iIP as String = HttpContext.Current.Request.ServerVariables("remote_addr")
		Dim iIp As System.Net.IPAddress = System.Net.IPAddress.Parse(_iIP) 
		'For Each s As String In Request.ServerVariables
		'   Response.write("<strong>" & s & "</strong>: " & Request.ServerVariables(s) & "<hr>")
		'Next

		'Range n. 1 Domp�----------------------------------------
		'Dim _iStart as String = "94.80.95.161"
		'Dim _iEnd as String = "94.80.95.174"
		'Dim iStart As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart)
		'Dim iEnd As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd)
		'if (iStart.Address <= iIp.Address And iEnd.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range n. 2 Domp�----------------------------------------		
		'Dim _iStart2 as String = "212.31.245.241"
		'Dim _iEnd2 as String = "212.31.245.254"
		'Dim iStart2 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart2)
		'Dim iEnd2 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd2)		
		'if (iStart2.Address <= iIp.Address And iEnd2.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------
		
		'Range n. 1 Domp� MI-------------------------------------
		Dim _iStart as String = "192.168.1.1"
		Dim _iEnd as String = "192.168.1.254"
		Dim iStart As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart)
		Dim iEnd As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd)
		if (iStart.Address <= iIp.Address And iEnd.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range n. 2 Domp� MI-------------------------------------
		Dim _iStart2 as String = "192.168.2.1"
		Dim _iEnd2 as String = "192.168.2.254"
		Dim iStart2 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart2)
		Dim iEnd2 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd2)
		if (iStart2.Address <= iIp.Address And iEnd2.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range WIFI Domp� MI-------------------------------------
		Dim _iStart3 as String = "172.30.108.1"
		Dim _iEnd3 as String = "172.30.108.254"
		Dim iStart3 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart3)
		Dim iEnd3 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd3)
		if (iStart3.Address <= iIp.Address And iEnd3.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------		

		'Range n. 1 Domp� AQ-------------------------------------
		Dim _iStart4 as String = "192.168.6.1"
		Dim _iEnd4 as String = "192.168.6.254"
		Dim iStart4 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart4)
		Dim iEnd4 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd4)
		if (iStart4.Address <= iIp.Address And iEnd4.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range WIFI Domp� AQ-------------------------------------
		Dim _iStart5 as String = "172.30.129.1"
		Dim _iEnd5 as String = "172.30.129.254"
		Dim iStart5 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart5)
		Dim iEnd5 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd5)
		if (iStart5.Address <= iIp.Address And iEnd5.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range LAN/WIFI Domp� NY---------------------------------
		Dim _iStart6 as String = "172.30.118.1"
		Dim _iEnd6 as String = "172.30.118.254"
		Dim iStart6 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart6)
		Dim iEnd6 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd6)
		if (iStart6.Address <= iIp.Address And iEnd6.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range Domp� Tirana---------------------------------
		Dim _iStart7 as String = "172.30.142.1"
		Dim _iEnd7 as String = "172.30.142.254"
		Dim iStart7 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart7)
		Dim iEnd7 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd7)
		if (iStart7.Address <= iIp.Address And iEnd7.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'Range WIFI Domp� Tirana---------------------------------
		Dim _iStart8 as String = "172.30.141.1"
		Dim _iEnd8 as String = "172.30.141.254"
		Dim iStart8 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iStart8)
		Dim iEnd8 As System.Net.IPAddress = System.Net.IPAddress.Parse(_iEnd8)
		if (iStart8.Address <= iIp.Address And iEnd8.Address >= iIp.Address) Then Return True
		'--------------------------------------------------------

		'RHW-----------------------------------------------------		
		Dim iRhWAddress As System.Net.IPAddress = System.Net.IPAddress.Parse("212.239.80.134")
		if (iRhWAddress.Address = iIp.Address) Then Return True
		'--------------------------------------------------------
		
		Return False
    End Function
    
    Protected Sub Page_Load(sender As Object, e As System.EventArgs)
        If _siteArea = Dompe.SiteAreaConstants.IDComunicazioneInterna Then
            _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/News.htm", _siteFolder)
			Dim labPage as String = "Comunicazione Interna"
			If _langKey.Id = 2 Then labPage = "Internal Communication"
			If _langKey.Id = 3 Then labPage = "Komunikimi i brendsh�m"
			If _langKey.Id = 4 Then labPage = "Comunicaci�n interna"
			
            strSezione = "<h2 class='red'>" & labPage & "</h2>"
            _cssInlineCode = String.Format("background-image: url('/{0}/_slice/comunicazioneInterna.jpg');", _siteFolder)
        End If
        _notAvaible = Me.BusinessDictionaryManager.Read("DMP_notavaible", _langKey.Id, "DMP_notavaible")
		_isMobile = IsMobileDevice()
		If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
		
		
		
		If Not CheckIpAddress() Then
			plhDetail.Visible = False
			plhArchive.Visible = False
			plhNotAuthorized.Visible = True
		Else
			Dim idContentTheme As Integer = 0
			Dim idContent As Integer = Me.PageObjectGetContent.Id
			If idContent > 0 Then
				Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme, _langKey)
				_lancioThema = valthem.Launch
				If Not String.IsNullOrEmpty(_lancioThema) Then
					divThemelaunch.Visible = True
				End If
				idContentTheme = valthem.KeyContent.Id
				_topKeys = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _langKey.Id)
				Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent = _topKeys
				If idContentTheme <> idContent Then
					InitDetailView()
				Else
					InitView()
				End If
			Else
				InitView()
			End If
			
			Dim _ctrlPath As String = String.Format("/{0}/HP3Common/BoxRelatedContent.ascx", _siteFolder)
			Me.LoadSubControl(_ctrlPath, ph_RelatedContent)
			div_RelatedContent.Visible = True
			ph_RelatedContent.Visible = True
		End If
    End Sub
    
    
    Protected Sub InitDetailView()
        Dim path As String = String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder)
        If Me.BusinessMasterPageManager.GetSiteArea.Id = Dompe.SiteAreaConstants.IDEventiConferenze Then
            path = String.Format("/{0}/HP3Service/DetailContentEventi.ascx", _siteFolder)
        End If
        LoadSubControl(path, plhDetail)
        plhArchive.Visible = False
        plhDetail.Visible = True
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub

    Protected Sub InitView()
        plhDetail.Visible = False
        plhArchive.Visible = True
    End Sub
</script>

<script language="javascript" type="text/javascript">
    var templateUrl = "<%=_emptyLiteratureArchiveTemplate%>";
    var ajaxLiteraturePage = "<%=_ajaxLiteraturePage%>";
    var nPage = 1;
    var _loadingLibrary = false; 
    var _pageSize = <%=_MaxItem %>;
    var _defaultBitmask = '<%=_bitMask%>';
    var _currentBitmask = _defaultBitmask;
    var _defaultKeysToExclude = "<%=_topKeys%>";
    var _loadingStatus = false;
    var yearsList=0;

    $(document).ready(function () {
        FilterApply();
    });

  

    function FilterApply(){
        nPage = 1;
        $(".notavaible").remove();
        $(".archiveCnt").remove();
   
        var so = prepareSearcherLiterature('', _defaultBitmask, _defaultKeysToExclude);
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        
        InitAutoCompleater();
        InitSearch();
    
    }

    function yearsOpen(){
        if(yearsList==0){
            $('.yearsList').css('display','block');
            yearsList=1;
        }else{
            $('.yearsList').css('display','none');
            yearsList=0;
        }
    }
	
    function selYear(id){
        $('.yearsList').css('display','none');
        yearsList=0;
        $('.yearSearch').html(id);
        $('.yearHidden').val(id);
    }


    function GetTags()
    {
        var tags = '';             
        $("ul#tagsView li").each(function(){
            tags += $(this).attr('data-tag') + ',' || '';
        });

        return tags;
    }

    function InitSearch(){
        $('.filterBtn #btnTagSearch').click(function(){
            DoSearch();
        });
    }

    function DoSearch()
    {
        _loadingStatus=false;
        $("#literatureArchive").empty();            
        var tags = GetTags();                                    
        var so = prepareSearcherLiterature(tags, _currentBitmask, '')
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
    }

    function InitAutoCompleater(){      

        var searchField = $('.bgFilter #tagSearch');
        searchField.val('');
        searchField.keyup(function (e) {                    
            ReadTags();        
        });      

        searchField.focusout(function(e){
            searchField.val('');    
        });
        
    }

    function ReadTags(){
        
        var postData = {
            op: '<%=Keys.EnumeratorKeys.TagOperationType.List %>',
            search: $('.bgFilter #tagSearch').val(),
            limit: '50'  
        };

        $('.bgFilter #tagSearch').autocomplete({
            minLength:2,
            focus: function(event, ui){                
                return false; 
            },
            select: function(event, ui){                           

                $("#tagsView").append("<li data-tag=" + ui.item.value + ">" + ui.item.label + "<a id='removeTag' title='Delete'>X</a></li>");
                $("ul#tagsView li[data-tag="+ ui.item.value +"] a").click(function(){
                    $(this).closest('li[data-tag='+ ui.item.value +']').remove();
                    DoSearch();
                });              
               
                $("ul#tagsView li[data-tag="+ ui.item.value +"]").hover(
                 function(){
                     $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").removeClass("hidden");
                 },
                 function(){
                     $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").addClass("hidden");
                 }
                );

                DoSearch();

                $("#tagSearch").val('');
                return false;            
                    
            },
            source: function(request, response){

                $.ajax({
                    type: "POST",
                    url: '<%=_ajaxTagsPage %>',            
                data: postData,
                success: function( data ) {
                    if (data){
                        if (data.Success) {        
                            if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %>')
                            {
                                response( $.map( data.Result, function( item ) {
                                    return {
                                        label: item.TagName,
                                        value: item.TagKey

                                    }
                                }));                            
                            } else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %>')
                            {
                                response('','');
                            }
                        }
                        else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                            document.location.reload();
                        }
                    }
                },
                 error: function(XMLHttpRequest, textStatus, errorThrown) {
                     //alert(textStatus);
                 }
     
             });
            }
        });
}

function InitReadMore()
{
    //            $("#moreInfo").on('click', function () {            
    var keysToExclude = _defaultKeysToExclude;
    var tags = GetTags(); 
    nPage = nPage + 1; 
    var contentSearcher = prepareSearcherLiterature(tags, _currentBitmask, keysToExclude);
    Literature.read(contentSearcher);
    return false;
    //        });  
}

var ReadLiteratureCallback = function (data) {  
    if(data){
        if (data.Success) {        
            if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %> )
                {     
                    _loadingStatus = true;
                var container = $("#literatureArchive");
                $.tmpl("ArchiveTemplate", data.Result.Collection).appendTo(container);
                
                if(data.Result.PageNumber == data.Result.PageTotalNumber)
                {  
                    $('#moreInfo').hide();
                    $('#moreInfoBar').hide();
                }
                else
                { 
                    $('#moreInfo').show();
                    $('#moreInfoBar').show();
                }
                    

            } else if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %> )
                {
                  $('#moreInfo').hide();
            $('#moreInfoBar').hide();
                    
            // if(!_loadingStatus)
            $("#literatureArchive").html("<div class='singleArchive positionRel' style='cursor:default;'><div class='titleArchive' style='background-color:transparent!important;padding:20px 0 250px 0;'><div class='container'><div class='colArchive'><%= _notAvaible %></div></div></div></div>");
                }
    }
            else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
        document.location.reload();
    }
    }
    else
    {
        $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
            }      
       
            };

            var OnReadLiteratureArchiveCompleteCallback = function () {
             
                $('#LiteratureArchiveLoader').hide();
                _loadingLibrary=false;
                $("#literatureArchive").fadeIn('fast');        

            };


            function prepareSearcherLiterature(TagsP, bitmask, keysToExclude) {
                var year = $(".yearSearch").html();
                var word = $("#tagSearchArchivio").val();
                var searcher = {          
                    SiteareId: '<%=_siteArea%>',
            ContenTypeId: '<%=_keyContentType.Id%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=_themeKey.Id %>',
            Brand: '',
            BitMask: bitmask,
            Display: true,
            Active: true,
            Delete: false,
            ApprovalStatus:'<%=_CheckApproval%>',
            Year: year,
            Word: word,
            Tags:'',
            PageNumber:nPage,
            PageSize:_pageSize,
            KeysToExclude: keysToExclude
        };

        recordOutboundFull("Archivio <%= strSezione %>","Caricamento","page: "+ nPage)

        if(TagsP != '')
            searcher.Tags =  TagsP;


        //        if(CountriesP != '')
        //            searcher.Countries =  CountriesP;
                    
        return searcher;

    };

    function LoadLiteratureArchive(contentSearcher, callback, onCompleteCallback)   //old LoadProfile
    {
        $('#LiteratureArchiveLoader').show();
        $('#moreInfo').hide();
        $('#moreInfoBar').hide();  
      
        var postData = {
            op: '<%=Cint(Keys.EnumeratorKeys.LiteratureViewType.List)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: ajaxLiteraturePage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {
                if (typeof callback === "function") {
                    callback(data);
                }
                $(".archiveCnt").hover(
         function () {
             $(this).children('.effectHover').css('display','none');
         },
         function () {
             $(this).children('.effectHover').css('display','block');
         }
     );
            },
            error: function (data) {
                //alert("error")
                //$("#literatureArchive").fadeIn('fast');
            }
        }).complete(function () {            
            if(onCompleteCallback && typeof(onCompleteCallback) == 'function') onCompleteCallback();
            //if (typeof onCompleteCallback === "function") {
            // onCompleteCallback();
            // }
        }); 
    }


    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {
            $.template("ArchiveTemplate", template);

            //se non � LoaderFunction nothing
            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    var Literature = Literature || new function (){
           

        this.read = function (contentSearcher) {
            if (true == _loadingLibrary) return;

            _loadingLibrary = true;

            $('#LiteratureArchiveLoader').show();
            $('#moreInfo').hide();
            $('#moreInfoBar').hide();
            //var container = jQuery('#literatureArchive');  
            
            LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        };
    }

    function FilterArea(item){
        
        _loadingStatus=false;
        $('.contentHome .filter .aree a[data-mask='+ item +']').toggleClass('sel');
        nPage=1;

        var bitmask = GetBitmask();
        _currentBitmask = bitmask;
        
        var tags = GetTags();  

        var keysToExclude = '';
        var contentSearcher = prepareSearcherLiterature(tags, bitmask, keysToExclude);
        $("#literatureArchive").empty();
        LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback)
    }

    function GetBitmask()
    {
        var mask = '';
        $('.contentHome .filter .aree a.sel').each(function(){
            mask += $(this).attr('data-mask') + ',' || '';
        });

        if(mask == '' ) {
            mask = _defaultBitmask;
        }

        return mask;
    }
        
   
</script>

<asp:PlaceHolder runat="server" ID="plhNotAuthorized" visible="false">
<div class="body">
    <div class="box-container titleSection" style="<%=_cssInlineCode %>">
        <div class="boxCnt">
            <div class="container">
                <%= strSezione%>
            </div>
        </div>
    </div>
    <div class="containerArchiveTot" style="margin: 0 0 220px 0;">
	<div id="literatureArchive2"></div>
        <div class='singleArchive positionRel' style='cursor:default;'><div class='titleArchive' style='background-color:transparent!important;padding:20px 0 250px 0;'><div class='container'><div class='colArchive'>Non sei autorizzato a visualizzare i contenuti di questa pagina.</div></div></div></div>
    </div>	</div>	
    <div class="colLeft">
        <div class="boxBlueDett bigBoxBlueDett">
           <div class="listSide">                	   
                &nbsp;
            </div>
        </div><!--end boxBlueDett-->
        <div class="imgSideBlue">&nbsp;</div>
    </div><!--end colLeft-->
</div>
</asp:PlaceHolder>



<asp:PlaceHolder runat="server" ID="plhArchive">
<asp:PlaceHolder ID="plhBoxLiterature" runat="server"></asp:PlaceHolder>
<div class="body">
    <div class="box-container titleSection" style="<%=_cssInlineCode %>">
        <div class="boxCnt">
            <div class="container">
                <%= strSezione%>
            </div>
        </div>
    </div>
    <div class="containerArchiveTot" style="margin: 0 0 220px 0;">
        <div class="positionRel" id="disclaimerNews" runat="server" visible="false">
                <div class="container">
                    <div class="colArchive">
                        <asp:Literal ID="ltlDisclaimer" runat="server"></asp:Literal>
                    </div> 
                </div> 
        </div> 
        <div id="literatureArchive"></div>
        <div id="LiteratureArchiveLoader" style="clear:both; float:left; width:100%; height:200px;" class="positionRel"><div style="text-align:center; clear:both; float:left; position:absolute; left:50%; top:50px; margin-left:-16px; height:200px;"><img src="/<%=_siteFolder%>/_slice/ajax-loader.gif" alt="loading" /></div></div>

        <div id="moreInfoBar" class="bar" style="clear: both; width:100%;margin: 0 auto;">
            <div class="bar2" style="width:600px; margin:0 0 0 375px; background-color:#ECECEC; clear:both; height:35px; float:left;">
            <a onclick="InitReadMore();" href="javascript:void(0);" id="moreInfo" style="text-align: center; padding-top: 10px; float: left; clear: both; width: 100%; color: #333333; font-weight: bold;"><span><%=Me.BusinessDictionaryManager.Read("DMP_MORE_INFO", _langKey.Id, "Altri Risultati")%></span></a>
            </div>
        </div>
    </div>
    <div class="colLeft">
        <div class="boxBlueDett bigBoxBlueDett">
           <div class="listSide" id="div_RelatedContent" runat="server" visible="false">                	   
                <asp:PlaceHolder runat="server" ID="ph_RelatedContent" Visible="false"></asp:PlaceHolder>               
            </div>
        </div><!--end boxBlueDett-->
        <div class="imgSideBlue">&nbsp;</div>
    </div><!--end colLeft-->

   <div class="container" style="display:none;">
     <div class="bodyBox">
    <div id="divThemelaunch" class="themeLaunch" runat="server" visible="false"><%= _lancioThema%></div>
       <div class="searchBox">
            <h4 class="searchTitle"><%=Me.BusinessDictionaryManager.Read("DMP_Search", _langKey.Id, "DMP_Search")%></h4>	
                  
            <div class="searchForm">
                  
                     <input type="text"  id="tagSearchArchivio" />
                 
                 <div class="yearSearch" onclick="javascript:yearsOpen();"></div>

                <ul class="yearsList" style="display: none;">
                    <li><a title="" href="javascript:void(0);" onclick="javascript:selYear('');">&nbsp;</a></li>
                    <li><a title="2013" href="javascript:void(0);" onclick="javascript:selYear('2013');">2013</a></li>
                    <li><a title="2012" href="javascript:void(0);" onclick="javascript:selYear('2012');">2012</a></li>
                    <li><a title="2011" href="javascript:void(0);" onclick="javascript:selYear('2011');">2011</a></li>
                </ul>
                   <input id="hdnyear" class="yearHidden" type="hidden" value="-1" />
                   <a id="A1" href="javascript:void(0);" onclick="FilterApply()" runat="server" class="btnSearch"><%= Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch")%></a>
            </div>      
        </div>   <!--end filter--> 
        </div> <!--End body box-->
    </div> <!--End container-->
   
</div> <!--End body-->
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">
</asp:PlaceHolder>

<script type="text/javascript">
	<% If Not _isMobile Then %>
    $(document).scroll(function () {
        var hDoc = $(document).height();
        var colLeftH = $('.colLeft').height();
        var ctrlEnd = hDoc - 110;
        var hWin = $(window).scrollTop() + colLeftH;

        if (hWin >= ctrlEnd) {
		
            $('.colLeft').css("position", "fixed");
            $('.colLeft').css("top", "auto");
            $('.colLeft').css("bottom", "110px");
        }else{
            if ($(window).scrollTop() > 431) {
                $('.colLeft').css("position", "fixed");
                $('.colLeft').css("top", "115px");
                $('.colLeft').css("bottom", "auto");
            } else {
                $('.colLeft').css("position", "absolute");
                $('.colLeft').css("top", "546px");
                $('.colLeft').css("bottom", "auto");
            }
        }
    });
    <% End If %>
</script>
