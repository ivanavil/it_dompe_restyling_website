﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private linkArchive As String
    Private classSection As String
    Private classBoxLeft As String
    Private classImgSide As String
    Private Dict As New DictionaryManager
	
	Private _isMobile as boolean = false
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguageFromUrl()  'Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        
    End Sub
        
	Public Function IsMobileDevice() As Boolean
        
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
             if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
                 userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
                Return true
            End If
        End if
            
        Return False
        
    End Function
	
    Dim _canCheckApproval As Boolean = False
    
    Sub Page_Load()
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        InitMenu()
        InitView()
    End Sub
    
    Protected Sub InitView()
        
        Dim contentId As Integer = Me.PageObjectGetContent.Id
		
        _cacheKey = "cacheKeyArc:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId
        If Request("cache") = "false" Or _canCheckApproval Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		
		Dim _isApproved as Boolean = False        		
        Dim arrData(4) As String
        Dim _strCachedContent() As String = CacheManager.Read(_cacheKey, 1)
        If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
            arrData = _strCachedContent
            titolo.Text = arrData(0)
            'titBig.Text = arrData(0)
            ltlContentDetail.Text = arrData(1)
			'_classSection = arrData(2)
			_isApproved = arrData(3)
			If Not _isApproved Then Response.Redirect(Request.Url.Scheme.ToString() & "://" & Me.ObjectSiteDomain.Domain, False)
			If arrData(1).IndexOf("addthis_toolbox") > -1 Then phSocialSharing.visible = true
        Else
			 'Response.write("not cached")
            Dim oStringBuilder As StringBuilder = New StringBuilder()
            Dim serchcont As New ContentExtraSearcher
            If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
                'Response.Write("_langKey:" & _langKey.Id)
                Dim soEx As New ContentExtraSearcher()
                soEx.key.Id = contentId
                soEx.key.IdLanguage = _langKey.Id
                soEx.CalculatePagerTotalCount = False
                soEx.SetMaxRow = 1
                soEx.KeySite = Me.PageObjectGetSite
				soEx.Display = SelectOperation.All
                If Not _canCheckApproval Then 
					'Controllo su Focus On Partner mirato a leggere i contenuti solo per associazione diretta con gli utenti
					If _currentTheme.Id = Dompe.ThemeConstants.partnerFocusOn Then
						soEx.RelatedTo.ContentUser.KeyUser.Id = Me.ObjectTicket.User.Key.Id
						soEx.RelatedTo.ContentUser.KeyRelationType.Id = Dompe.RelationType.PartnerContent
					End If				
					soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
				End If

                Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
                If Not contentextraColl Is Nothing AndAlso contentextraColl.Any() Then
                    contentextraVal = contentextraColl.FirstOrDefault
                    
                    'contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
                    'If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                    
                    'titBig.Text = contentextraVal.Title '& Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus)
                    titolo.Text = contentextraVal.Title & Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus)
                    oStringBuilder.Append("<div class='content'>")
                    ' oStringBuilder.Append("<h3>" & contentextraVal.Title & "</h3>")
                    If Not String.IsNullOrEmpty(contentextraVal.Abstract) Then oStringBuilder.Append("<h4>" & contentextraVal.Abstract & "</h4>")
                    'If Not String.IsNullOrEmpty(contentextraVal.BookReferer) Then oStringBuilder.Append("<p><strong>" & Me.BusinessDictionaryManager.Read("DMP_labelbook", _langKey.Id, "DMP_labelbook") & ":</strong><i>" & contentextraVal.BookReferer & "</i></p>")
                    'If Not String.IsNullOrEmpty(contentextraVal.FullTextComment) Then oStringBuilder.Append("<p><strong>" & Me.BusinessDictionaryManager.Read("DMP_labelComment", _langKey.Id, "DMP_labelComment") & ":</strong><i>" & contentextraVal.FullTextComment & "</i></p>")

					Dim strSocialShare as String = getSocialShare()
					If Not String.IsNullOrEmpty(strSocialShare) Then oStringBuilder.Append(strSocialShare)					
					
                    Dim strFltxt As String = contentextraVal.FullText
                    If String.IsNullOrEmpty(strFltxt) Then
                        oStringBuilder.Append(contentextraVal.Launch)
                    Else
                        oStringBuilder.Append(strFltxt)
                    End If
				  
					Dim _strHeaderSedi As String = String.Empty
					Dim _strGooglePinSedi As String = String.Empty
					
					If _currentTheme.Id = Dompe.ThemeConstants.idSedi Then
                        _strHeaderSedi = "<h1 style='margin-top:40px;'>" & Me.BusinessDictionaryManager.Read("DMP_headerSedi", _langKey.Id) & "</h1>"

						If Not String.IsNullOrEmpty(contentextraVal.LinkContentExtra) Then
							_strGooglePinSedi = "<div style='float:left;padding-top:3px;'><a title='" & Me.BusinessDictionaryManager.Read("DMP_headerSedi", _langKey.Id) & "' href='" & contentextraVal.LinkContentExtra & "'><img src='/ProjectDOMPE/_slice/google-maps-pin.svg' style='width:40px;' /></a></div>"
						End If
					End If
					
					If Not String.IsNullOrEmpty(contentextraVal.FullTextOriginal) Then oStringBuilder.Append(_strHeaderSedi & _strGooglePinSedi & "<div style='float:left;'>" & contentextraVal.FullTextOriginal & "</div><div style='clear:both;'></div>")
                    
                    If _siteArea.Id = Dompe.SiteAreaConstants.IDSiteareaPosizioniAperte Then
                        Dim jsClick As String = "recordOutboundLink(""mailto:" & contentextraVal.LinkUrl & "?subject=" & contentextraVal.LinkLabel & """,""Mail to"",""Send"",""" & contentextraVal.Title & """);return false;"
                        oStringBuilder.Append("<div class='dwnBox'><h5>" & Me.BusinessDictionaryManager.Read("HeaderCandidatura", _langKey.Id, "HeaderCandidatura") & "</h5>" _
                            & "<a onclick='" & jsClick & "' href='mailto:" & contentextraVal.LinkUrl & "?subject=" & contentextraVal.LinkLabel & "'>" _
                            & "<span class='txt'><span class='title'>" & Me.BusinessDictionaryManager.Read("TitleCandidatura", _langKey.Id, "TitleCandidatura") & "</span>" _
                            & " </span> <span class='readMore2'>" & Me.BusinessDictionaryManager.Read("LinkMailToCandidatura", _langKey.Id, "LinkMailToCandidatura") & "</span>" _
                            & "</a></div>")
                    End If
                    oStringBuilder.Append("</div>")
					
                    If _siteArea.Name = Dompe.SiteAreaConstants.Management Then imgPath = GetImage(contentextraVal.Key, contentextraVal.Title)
                    _classSection = contentextraVal.Code
                    'oStringBuilder.Append(LoadContentAssociati(contentextraVal.Key.PrimaryKey))
                    oStringBuilder.Append(loadLink(contentextraVal.Key.PrimaryKey))
					oStringBuilder.Append(LoadVideo(contentextraVal.Key.PrimaryKey))
					oStringBuilder.Append(LoadGallery(contentextraVal.Key.PrimaryKey))
                    
                    
					_isApproved = iif(contentextraVal.ApprovalStatus = 1, True, False)
					
                    arrData(0) = contentextraVal.Title
                    arrData(1) = oStringBuilder.ToString()
                    arrData(2) = String.Empty 'contentextraVal.Code
                    arrData(3) = _isApproved
                    
                    'Cache Content and send to the oputput-------
                    If contentextraVal.ApprovalStatus = 1 Then CacheManager.Insert(_cacheKey, arrData, 1, 60)
                    ltlContentDetail.Text = oStringBuilder.ToString()
                    '--------------------------------------------
                Else
				
					Response.Redirect(Request.Url.Scheme.ToString() & "://" & Me.ObjectSiteDomain.Domain, False)
				End If
            End If
        End If
        getArchiveData()
        '-------------------------
        'Additional Controls
        '-------------------------
        'plhSideRed.Visible = (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAPeople)
        'plhBoxRedDett.Visible = ((Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAPeople) Or (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSASergioDmp) Or (Me.PageObjectGetSiteArea.Id = Dompe.SiteAreaConstants.IDSAAringhieri))
        If _siteArea.Name = Dompe.SiteAreaConstants.Management Then LoadSubControl(String.Format("/{0}/HP3Common/BoxManagement.ascx", _siteFolder), phlManagement)
        'LoadSubControl(String.Format("/{0}/HP3Common/BoxNews.ascx", _siteFolder), Phlnews)
        If Dompe.ContentBoxImage.IdContentList.Contains("{" & Me.PageObjectGetContent.Id.ToString & "}") Then LoadSubControl(String.Format("/{0}/HP3Common/BoxImage.ascx", _siteFolder), PhlBoxImage)
        LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
        'LoadSubControl(String.Format("/{0}/HP3Common/BoxLinks.ascx", _siteFolder), PhlLinks)
        
    End Sub
	
	Function getSocialShare() As String
		Dim octxS as new ContentContextSearcher()
		With octxS
			.KeyContent = Me.PageObjectGetContent
			.KeyCOntext = New ContextIdentificator(44)
		End With
		Dim octxC as contentcontextCollection = Me.BusinessContentManager.ReadContentContext(octxS)
		If Not octxC Is Nothing AndAlso octxC.Any Then
			phSocialSharing.visible = true
			Return "<div class='addthis_toolbox addthis_default_style addthis_32x32_style'><a class='addthis_button_preferred_1'></a><a class='addthis_button_preferred_2'></a><a class='addthis_button_google_plusone_share'></a><a class='addthis_button_linkedin'></a></div>"
		End IF
		
		Return String.Empty
	End Function	
    
    Sub getArchiveData()
       
        
        Dim tV As ThemeValue = Me.BusinessThemeManager.Read(_currentTheme)
      
        If (Not tV Is Nothing) Then
            linkArchive = Me.BusinessMasterPageManager.GetLink(tV, False)
            
            Select Case tV.KeyFather.Id
                Case Dompe.ThemeConstants.idAreaPress
                    classBoxLeft = "boxGreenDett"
                    classImgSide = "imgSideGreen"
                Case Dompe.ThemeConstants.idAreaPartner
                    classSection = "networkSection"
                    classBoxLeft = "boxGreenDett"
                    classImgSide = "imgSideGreen"
				
                Case Else
                    classBoxLeft = "boxRedDett bigBoxRedDett"
                    classImgSide = "imgSideRed"
            End Select
        End If
                


    End Sub
    
    Protected Sub InitMenu()
        
        If _currentTheme.Id = Dompe.ThemeConstants.idSedi Then
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuSedi.ascx", _siteFolder)
            LoadSubControl(ctrlPath, div_menu)
            'menu.Visible = True
            div_menu.Visible = True
        ElseIf _currentTheme.Id = Dompe.ThemeConstants.partnerFocusOn Then
            Dim ctrlPath As String = String.Format("/{0}/HP3Common/MenuFocusOn.ascx", _siteFolder)
            LoadSubControl(ctrlPath, div_menu)
            ' menu.Visible = True
            div_menu.Visible = True
        End If
    End Sub
    
    Function LoadContentAssociati(ByRef PK As Integer) as String
		Dim _out As String = String.Empty
		
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        If PK > 0 Then
            soRel.Content.Key.PrimaryKey = PK
            soRel.Content.KeyRelationType = New RelationTypeIdentificator(4)
            soRel.Content.RelationSide = RelationTypeSide.Right
        End If
        
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
      
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
			_out = "<div class='dwnBox'><h5>" & Me.BusinessDictionaryManager.Read("DMP_RelatedMaterial", _langKey.Id, "DMP_RelatedMaterial") & "</h5>"
			For Each oContent as ContentValue in coll
				_out += "<a href='/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(oContent.Key.PrimaryKey) & "' onclick=""recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(oContent.Key.PrimaryKey) & "','Download Dettaglio del contenuto','Download','" & oContent.Title & "','" & oContent.Key.PrimaryKey & "');return false;"">" & _
					"<span class='txt'><span class='title'>" & oContent.Title & "</span>" & _
					"<span>" & IIf(oContent.Launch.Length > 120, Left(oContent.Launch, 120) & "...", oContent.Launch) & "</span>" & _
					"</span><span class='readMore2'>" & Me.BusinessDictionaryManager.Read("Download", _langKey.Id, "Download") & "</span></a>"
			Next
			_out += "</div>"
		
            'rpdDownloadRelated.DataSource = coll
            'rpdDownloadRelated.DataBind()
        End If
		
		Return _out
    End Function

    	
	Function loadLink(ByRef PK As Integer) as String
		Dim _sbLink as New StringBuilder
		Dim _strLinkItem as String = "<li style='list-style-type: square;'><a href='{0}' title='{1}'>{2}</a></li>"
		
		Dim oColl as contentCollection = me.businesscontentmanager.ReadContentRelation(New ContentIdentificator(PK), New ContentRelationTypeIdentificator(Dompe.RelationType.LinkRelation))
		If Not oColl Is Nothing AndAlso oColl.Any Then
			For Each oCon as contentValue in oColl
				_sbLink.AppendFormat(_strLinkItem, oCon.LinkUrl, oCon.Title, oCon.Title)
			Next
			
			Return "<ul style='margin-top:20px;'>" & _sbLink.ToString() & "</ul>"
		End If
		Return String.Empty
	End Function	

    Function LoadGallery(ByRef PK As Integer) As String
		Dim soRel As New ContentRelatedSearcher
        Dim _out As String = String.Empty
        Dim coll As ContentCollection = Me.BusinessContentManager.ReadContentRelation(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id), New ContentRelationTypeIdentificator(Dompe.RelationType.GalleryRelation))
        If Not coll Is Nothing AndAlso coll.Any Then
            For Each oCont As ContentValue In coll
                _out = "<iframe width='600' frameborder='0' height='610' style='border:0px;' id='contGallery' src='/ProjectDOMPE/HP3Popup/PhotoGalleryPublic.aspx?l=" & _langKey.Code & "&inpage=1&c=" & SimpleHash.UrlEncryptRijndaelAdvanced(oCont.Key.Id) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & _langKey.Id & "&cS=" & Me.PageObjectGetContent.Id & "' data-cont='188'></iframe>"
            Next
        End If
        Return _out
    End Function
    
    Function LoadVideo(ByRef PK As Integer) As String
        Dim _out As String = String.Empty
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
        If PK > 0 Then
            soRel.Content.Key.PrimaryKey = PK
            soRel.Content.KeyRelationType = New RelationTypeIdentificator(1)
            soRel.Content.RelationSide = RelationTypeSide.Left
        End If
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDVideoGallery
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            If coll.Count = 1 Then
                _out += "<h3>" & coll(0).Title & "</h3>"
                _out += "<p>" & coll(0).Launch & "</p>"
                Dim w As Integer = 0
                Dim h As Integer = 0
                If Not String.IsNullOrEmpty(coll(0).Copyright) Then
                    w = coll(0).Copyright.Split("|")(0)
                    h = coll(0).Copyright.Split("|")(1)
                End If
                
                _out += String.Format("<iframe width='600' height='370' style='border:0px;' id='contGallery' src='/ProjectDOMPE/HP3Popup/VideoGallery.aspx?l={0}&c={1}&v=2&w={2}&h={3}'></iframe>", Me.PageObjectGetLang.Code, Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(coll(0).Key.PrimaryKey),w,h)
            Else
                _out = "<h4 class='photoTitle' style='float:left; width:100%;'>" & Me.BusinessDictionaryManager.Read("DMP_TitleAlbumVideoDettaglio", _langKey.Id, "DMP_TitleAlbumVideoDettaglio") & "</h4>"
                _out += "<div class='boxPhotoGallery noMargin'>"
                For Each oCont As ContentValue In coll
                    _out += "<div class='cntVideoGallery'>" & _
                 "<a title='" & oCont.Title & "' href='/ProjectDOMPE/HP3Popup/VideoGallery.aspx?l=" & Me.PageObjectGetLang.Code & "&c=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(oCont.Key.PrimaryKey) & "&v=1' rel='shadowbox;height=585;width=800'><img src='/HP3Image/Cover/" & GetImageContent(oCont.Key.Id, 150, oCont.Title) & "' alt='" & oCont.Title & "' /></a>" & _
                 "</div>"
                Next
                _out += "</div>"
            End If
        End If
        
        Return _out
    End Function
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = idContent
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
     
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
    Function GetImageContent(ByVal IDcont As Integer, ByVal ImgWidth As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, Me.PageObjectGetLang.Id), ImgWidth, title)
        Return res
    End Function
    
    Function GetImageContent() As String
        Dim res As String = ""
        Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High
        res = Me.BusinessContentManager.GetCover(Me.PageObjectGetContent, 1280, 323, imgqual, 1)

        If Not String.IsNullOrEmpty(res) Then
            res = "/HP3Image/cover/" & res
        End If		
		
        Return res
    End Function
</script>


<section>
   

    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    
</section>
   
    <section>
	<div class="container">
       <div class="colLeft">        	   
            	   
                       <asp:PlaceHolder runat="server" id="div_menu" visible="false"></asp:PlaceHolder>
            	   
        </div><!--end colLeft-->
         <div class="colRight">
             <%' If Not String.IsNullOrEmpty(classSection) Then%>
	           <%-- <div class="<%=classSection%>" style="background-image:url('<%=GetImageContent()%>');">
    	            <div class="boxCntGeneric">
                        <div class="container">
                            <h2 class="white"><asp:literal ID="titBig" runat="server" /></h2>
                        </div>
                    </div>
                </div>--%>
             <%'Else%>
             <h1><asp:Label ID="titolo" runat="server" /></h1>
             <%' End If%>
        	
                
                <asp:Literal ID="testo_optional" runat="server" />
				<asp:literal id="ltlContentDetail" runat="server" EnableViewState="False" />
				
                <div class="generic">		
               
                <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>

				<asp:Repeater ID="rpdDownloadRelated" ViewStateMode="Disabled" runat="server"  visible="false">
				<HeaderTemplate>
					<div class="dwnBox"><h5><%= Me.BusinessDictionaryManager.Read("DMP_RelatedMaterial", _langKey.Id, "DMP_RelatedMaterial")%></h5>
				</HeaderTemplate>
				<ItemTemplate>
					<a href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>" onclick="recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>','Download Dettaglio del contenuto','Download','<%# CType(Container.DataItem, ContentValue).Title%>','<%# CType(Container.DataItem, ContentValue).Key.PrimaryKey %>');return false;">
						<span class="txt">
							<span class="title"><%# CType(Container.DataItem, ContentValue).Title%></span>
							<span><%# IIf(CType(Container.DataItem, ContentValue).Launch.Length > 120, Left(CType(Container.DataItem, ContentValue).Launch, 120) & "...", CType(Container.DataItem, ContentValue).Launch)%> </span>
							<%--                 
							<span><%# getSizeFile(CType(Container.DataItem, DownloadValue).DownloadTypeCollection(0).Weight)%></span>
							--%>
						</span> 
						<span class="readMore2"><%= Me.BusinessDictionaryManager.Read("Download", _langKey.Id, "Download")%></span>
					</a>
				</ItemTemplate>
				<FooterTemplate>
					</div>
				</FooterTemplate>
				</asp:Repeater>
				

				<asp:Repeater ID="rptPhotoGallery" runat="server" EnableViewState="false"  visible="false">
				<HeaderTemplate>
					<h4 class="photoTitle" style="float:left; width:100%;"><%= Me.BusinessDictionaryManager.Read("DMP_TitleAlbumDettaglio", _langKey.Id, "DMP_TitleAlbumDettaglio")%></h4>              
				</HeaderTemplate> 
				<ItemTemplate>
					<div class="boxPhotoGallery noMargin">
						<div class="cnt">
							<a class="effectHover" title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundFull('PhotoGallery','Open Album','<%# CType(Container.DataItem, ContentValue).Title%>')"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
							<a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800"><%# GetImageContent(DataBinder.Eval(Container.DataItem, "Key.Id"), 150, DataBinder.Eval(Container.DataItem, "Title"))%></a>
							<h3><a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></h3>
							<p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>    
						</div>
					</div><!--end boxPhotoGallery-->               
				</ItemTemplate>
				<FooterTemplate></FooterTemplate>
				</asp:Repeater>
                
            	<asp:PlaceHolder ID="phlManagement" runat="server"></asp:PlaceHolder>
				<asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
				<asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
				
					
					<asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            
                
                </div>
                         
             
        </div>		      
    </div><!--end container-->
    </section>



<asp:placeholder id="phSocialSharing" runat="server" visible="false">			
	<script type='text/javascript'>var addthis_config = {'data_track_addressbar':true};</script>
	<script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-533ef6f344e9cf07'></script>	
</asp:placeholder>
<script type="text/javascript">
	$(document).ready(function () {
		$(".infoAcc1").first().css("display", "block");
		$(".bgAccBrand").first().attr("val", "o");
		$(".bgAccBrand").children("div:first").removeClass("closeAcc").addClass("openAcc");
	});

    <%' If Not _isMobile Then %>
			<%' If _currentTheme.Id = Dompe.ThemeConstants.partnerFocusOn Then %>
 //   $(document).scroll(function () {
    //       if ($(window).scrollTop() > 223) {
    //           $('.colLeft').css("position", "fixed");
    //          $('.colLeft').css("top", "115px");
    //     } else {
    //        $('.colLeft').css("position", "absolute");
    //        $('.colLeft').css("top", "222px");
    //    }
    // });
			<%' else %>
    // $(document).scroll(function () {
    //    if ($(window).scrollTop() > 223) {
    //        $('.colLeft').css("position", "fixed");
    //        $('.colLeft').css("top", "115px");
    //     } else {
    //         $('.colLeft').css("position", "absolute");
    //        $('.colLeft').css("top", "222px");
    //    }
    // });
    <%' end if %>
    <%' end if %>	
    $('.breadcrumbs').css("margin", "0");
    $('.colRight h1').css("margin-top", "0");
    $('.content ul').css("padding", "0 0 5px 20px");
    $('.content h3').css("margin", "0 0 10px 0");

	$(".bgAccBrand").click(function () {	        
		var valore = $(this).attr("val");
		if (valore == "c" || valore == undefined) {	            
			$(this).children("div:first").removeClass("closeAcc").addClass("openAcc");
			$(this).attr("val", "o");	            	            	            
			$(this).parents('.singleAccFocus').find('.infoAcc1').css("display", "block");
		} else {	            
			$(this).children("div:first").removeClass("openAcc").addClass("closeAcc");
			$(this).attr("val", "c");	            
			$(this).parents('.singleAccFocus').find('.infoAcc1').css("display", "none");
		}
	});
</script>
