﻿<%@ Control Language="VB" ClassName="registrationForm" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>


<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Private systemUtility As New SystemUtility()
    Private linkHome As String = ""
    Dim userID As UserIdentificator
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
    End Sub
    
   
        
    Sub Page_Load()

        linkHome = Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
        If Not Me.BusinessAccessService.IsAuthenticated Then ' se l'utente non è  autenticato, rimandiamo alla HOMEPage
            Response.Redirect(Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home), False)
        Else
            userID = Me.ObjectTicket.User.Key
           
            If Not Page.IsPostBack Then
                ' SetMessageErrors()
                BindAllCountry()
                BindUserData()
              
            End If
         
        End If
           
    End Sub
    
    Sub BindAllCountry()
        Dim geoType As GeoSearcher
        Dim geoColl As GeoCollection
        Dim utility As New WebUtility
        
        geoType = New GeoSearcher
        geoType.geoTypeId = 2
               
        geoColl = New GeoCollection
        geoColl = Me.BusinessGeoManager.Read(geoType)
       
        ddl_country.DataSource = geoColl
        ddl_country.DataTextField = "Description"
        ddl_country.DataValueField = "Key.Id"
                 
        If Not (geoColl Is Nothing) Then
            geoColl.Sort(GeoGenericComparer.SortType.ByDescription)
            utility.LoadListControl(ddl_country, geoColl, "Description", "Key.Id")
        End If
        ddl_country.Items.Insert(0, New ListItem(Me.BusinessDictionaryManager.Read("DMP_Country*", _langKey.Id, "Country *"), 0))
    End Sub
    
    Sub BindUserData()
        Try
           
            Dim usV As New UserHCPValue
            usV = Me.BusinessUserHCPManager.Read(userID)
          
            If Not usV Is Nothing Then
                ddlTitle.SelectedItem.Text = usV.Title
                TxtFirstName.Text = usV.Name
                TxtLastName.Text = usV.Surname
                TxtEmail.Text = usV.Email(0)
                ddl_country.SelectedItem.Text = usV.HCPWard
                TxtHospital.Text = usV.HCPStructure
                TxtWorkAddress.Text = usV.Address(0)
                Txtcap.Text = usV.Cap
                Txtcity.Text = usV.City
                'txtPassword.Text = usV.Password
                'txtConfermaPwd.Text = usV.Password
                
                If usV.HCPMoreInfo.ToString = "phone" Then
                    ChkPhone.Checked = True
                    TxtPhone.Value = usV.Telephone.GetString
                Else
                    ChkEmail.Checked = True
                End If
              
             
            End If
            
            
        Catch ex As Exception

        End Try
    End Sub
    
    Function ExistEmail(ByVal email_address As String) As Boolean
        Dim hlpman As New Helper
        Dim idRet As Integer
        idRet = hlpman.existUserByEmail(email_address.ToLower.Trim)
        If idRet > 0 Then
            Return True
        ElseIf idRet = 0 Then
            Return False
        ElseIf idRet = -1 Then
            Return True
        End If
    End Function
    'esegue tutti gli step necessari per la registrazione utente
    'effettua i controlli necessari e gestisce i messaggi di errore
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = _langKey
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
       
    Sub UpdateRegistrazione(ByVal s As Object, ByVal e As EventArgs)
        Dim userval As UserHCPValue = Me.BusinessUserHCPManager.Read(userID)
        Dim hlpman As New Helper
        If (Page.IsValid()) Then
            Dim VerificaMail As Boolean = True
           
            Try
                If userval.Email.ToString().Trim <> TxtEmail.Text.Trim Then
                    VerificaMail = ExistEmail(TxtEmail.Text.Trim)
                Else
                  
                    VerificaMail = False
                End If
              
            Catch ex As Exception

            End Try
           
            If Not VerificaMail Then
                userval.Title = ddlTitle.SelectedItem.Text
                userval.Name = TxtFirstName.Text
                userval.Surname = TxtLastName.Text
                'userval.Email.Item(0) = TxtEmail.Text
                userval.Email = New MultiFieldType()
                userval.Email.Add(TxtEmail.Text)
                userval.Username = TxtEmail.Text
                ' userval.Password = txtPassword.Text
                userval.HCPWard = ddl_country.SelectedItem.Text
                userval.Address = New MultiFieldType()
                userval.Address.Add(TxtWorkAddress.Text)
                userval.Cap = Txtcap.Text
                userval.City = Txtcity.Text
                userval.HCPStructure = TxtHospital.Text
                Dim ContactPreferer As String = IIf(ChkPhone.Checked, "phone", "email")
                userval.HCPMoreInfo = New MultiFieldType()
                userval.HCPMoreInfo.Add(ContactPreferer)
                If Not String.IsNullOrEmpty(TxtPhone.Value) Then
                    userval.Telephone = New MultiFieldType()
                    userval.Telephone.Add(TxtPhone.Value)
                End If
               
                Me.BusinessUserHCPManager.Update(userval)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){MessageOk();ModificaDati();})", True)
               
            Else
                
                ltlErrMail.Text = Me.BusinessDictionaryManager.Read("DMP_ErrorMailExist", _langKey.Id, "DMP_ErrorMailExist")
                ErrEmailPresente.Attributes("style") = "display: block;color: #FF0000;"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){MessageErrorCode();})", True)
            
            End If
            
        End If
        
    End Sub
</script>
    <style type="text/css">
    .dspNone
    {
        display:none;
    }
     .dspVisible
    {
        display: block;
    }
    </style>
 <script type = "text/javascript">
     function ValidateCheckBox() {
       
         if (document.getElementById("<%=ChkPhone.ClientID %>").checked == true) {
             var testo = $("#<%=TxtPhone.ClientID %>").val();
             if (testo == "" || testo == "Telephone") {
                 $("#NumeroObbligatorio").attr("style", "display:block; color: #FF0000;");
             } else {
                 $("#NumeroObbligatorio").attr("style", "display: none;");
            }
         } else {
             $("#NumeroObbligatorio").attr("style", "display: none;");

         }
     }

     function MessageOk() {
         $("#contentForm").attr("style", "display: none");
         $("#MessageOK1").attr("style", "display: block");
      }

   
     function MessageErrorCode() {
         $("#contentForm").attr("style", "display: block");
         $("#MessageOK1").attr("style", "display: none");
         $("#content").attr("style", "display: none");
         $("#RecoveryPassw").attr("style", "display: none");
     }

     function Close() {
         location.href = '<%= linkHome %>'
     }

   
     function cancellaValue(nomeControllo, valore) {


         if (document.getElementById(nomeControllo).value == valore) {
             document.getElementById(nomeControllo).value = "";
         }
     }

     function test2(nomeControllo, valore) {

         if (document.getElementById(nomeControllo).value == "") {
             document.getElementById(nomeControllo).value = valore;
         }
     }

     function ModificaDati() {

         HP3Piwik.TraceEvent('MyEvent', 'Modify Personal Date', 1, 'page', null, true);

     } 


</script>
 <script type = "text/javascript">
     function ValidateCheckBox() {
        
            if (document.getElementById("<%=ChkPhone.ClientID %>").checked == true) {
                var testo = $("#<%=TxtPhone.ClientID %>").val();
                if (testo == "" || testo == "Telephone") {
                    $("#NumeroObbligatorio").attr("style", "display:block; color: #FF0000;");
                    return false;
                 }else{
                     $("#NumeroObbligatorio").attr("style", "display: none;");
                     return true;
                 }
            } else {
                 $("#NumeroObbligatorio").attr("style", "display: none;");
                 return true;
             }

         }

         function cancellaValue(nomeControllo, valore) {


             if (document.getElementById(nomeControllo).value == valore) {
                 document.getElementById(nomeControllo).value = "";
             }
         }

          function test2(nomeControllo, valore) {

             if (document.getElementById(nomeControllo).value == "") {
                 document.getElementById(nomeControllo).value = valore;
             }
         }

</script>

<div class="contInternal">
    
    	<div class="left">
         </div><!--end left-->
         <div class="centerVideo">
          <div id="contentForm" style="display:block;">

  <div class="containerForm">
        	
  <div class="center">
  <h3><%= Me.BusinessDictionaryManager.Read("DMP_TitleRegistrationForm", _langKey.Id, "DMP_TitleRegistrationForm")%></h3>
                
         <div class="contFormFields">
                	<div class="left">
                        <div class="styled-select0">
                               <asp:DropDownList ID="ddlTitle" CssClass="title" runat="server">
                               <asp:ListItem Selected="True" Text="Dr." Value="Dr."></asp:ListItem>
                               <asp:ListItem Text="Dr.ssa" Value="Dr.ssa"></asp:ListItem>
                               </asp:DropDownList>
                        </div>
                        <div class="row">
                            <asp:TextBox ID="TxtFirstName" onfocus ="javascript:cancellaValue(this.id,'First Name*');" onblur="javascript:test2(this.id,'First Name*');" Text="First Name*" runat="server" CssClass="textfield" MaxLength="20"></asp:TextBox>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="First Name*" ControlToValidate="TxtFirstName" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>
                        <div class="row">
                           <asp:TextBox ID="TxtEmail" onfocus ="javascript:cancellaValue(this.id,'Email *');" onblur="javascript:test2(this.id,'Email *');" Text="Email *" runat="server" CssClass="textfield" MaxLength="60" />
                        <asp:RequiredFieldValidator ID="reqEmail" InitialValue="Email *" ErrorMessage="Required" ControlToValidate="TxtEmail" Display="dynamic" runat="server" ValidationGroup ="RegistrationForm" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ErrorMessage="Format incorrect" runat="server" Display="dynamic" ControlToValidate="TxtEmail" ValidationGroup ="RegistrationForm" ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}"/>
                        <div id="ErrEmailPresente" runat="server" style="display: none"><asp:Literal ID="ltlErrMail" runat="server" Text=""></asp:Literal></div>
                        </div>
                        <div class="row">
                            <asp:TextBox ID="TxtHospital" onfocus ="javascript:cancellaValue(this.id,'Hospital name *');" onblur="javascript:test2(this.id,'Hospital name *');" Text="Hospital name *" runat="server" CssClass="textfield" MaxLength="30"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" InitialValue="Hospital name *" ControlToValidate="TxtHospital" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>
                        <div class="row">
                           <asp:TextBox ID="Txtcap" onfocus ="javascript:cancellaValue(this.id,'Post code *');" onblur="javascript:test2(this.id,'Post code *');" Text="Post code *" runat="server" CssClass="textfield" MaxLength="10"></asp:TextBox>
                           <asp:RequiredFieldValidator InitialValue="Post code *" ID="RequiredFieldValidator5" ControlToValidate="Txtcap" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>
                    <%--    <div class="row">
                           <asp:TextBox CssClass="dspVisible" ID="txtPassword" runat="server" TextMode="Password" onblur="javascript:test2PwdForm(this.id,'Password *');"  MaxLength="30"></asp:TextBox>
                           <asp:TextBox CssClass="dspNone" ID="TextPwdForm" Text="Password *"  onfocus ="javascript:cancellaValuePwdForm(this.id,'Password *');" runat="server" ValidationGroup="RegistrationForm" EnableViewState ="false"/>
                          <asp:RequiredFieldValidator ID="passwordValidator" ControlToValidate="txtPassword" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>--%>
                        
                   </div><!--end left-->
                  <div class="right">
                    	 <div class="row">
                            <asp:TextBox ID="TxtLastName" onfocus ="javascript:cancellaValue(this.id,'Last name *');" onblur="javascript:test2(this.id,'Last name *');" Text="Last name *" runat="server" CssClass="textfield" MaxLength="20"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" InitialValue="Last name *" ControlToValidate="TxtLastName" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>
                        <div class="row">
                           <%-- <asp:textBox ID="ddlCountry" Text="Country *" runat="server" CssClass="textfield" MaxLength="20" />--%>
                          <div class="styled-select1">
                           <asp:DropDownList ID="ddl_country" runat="server" CssClass="title">
                           </asp:DropDownList>
                           </div>
                          <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" InitialValue="Country *" ControlToValidate="ddlCountry" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        --%></div>
                        <div class="row">
                             <asp:TextBox ID="TxtWorkAddress" onfocus ="javascript:cancellaValue(this.id,'Work address name *');" onblur="javascript:test2(this.id,'Work address name *');" Text="Work address name *" runat="server" CssClass="textfield" MaxLength="30"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" InitialValue="Work address name *" ControlToValidate="TxtWorkAddress" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
     
                        </div>
                        <div class="row">
                             <asp:TextBox ID="Txtcity" onfocus ="javascript:cancellaValue(this.id,'City *');" onblur="javascript:test2(this.id,'City *');" runat="server" Text="City *" CssClass="textfield" MaxLength="20"></asp:TextBox>
                               <asp:RequiredFieldValidator ID="RequiredFieldValidator6" InitialValue="City *" ControlToValidate="Txtcity" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        </div>
                    <%--    <div class="row">
                            <asp:TextBox CssClass="dspVisible" ID="txtConfermaPwd" onblur="javascript:test2ConfPwdForm(this.id,'Password *');" runat="server" TextMode="Password" MaxLength="30"></asp:TextBox>
                            <asp:TextBox CssClass="dspNone" ID="TextConfPwdLab" Text="Password *"  onfocus ="javascript:cancellaValueConfPwdForm(this.id,'Password *');" runat="server" ValidationGroup="logGroup" EnableViewState ="false"/>
                        
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtConfermaPwd" ErrorMessage="Required" Display="Dynamic" runat="server" ValidationGroup="RegistrationForm"></asp:RequiredFieldValidator>
                        <asp:CompareValidator id="compPass" Display="dynamic" ValidationGroup ="RegistrationForm"
                        ControlToCompare="txtPassword" Operator="Equal" ControlToValidate ="txtConfermaPwd" EnableViewState ="false"
                        ForeColor="red" Type="String" 
                        runat="server" ErrorMessage="Le password non coincidono" />
                        </div> --%>
                        <div class="row">
                        	<p> <%= Me.BusinessDictionaryManager.Read("DMP_HowCanWeContactYou", _langKey.Id, "DMP_HowCanWeContactYou")%></p>
                        	<div class="contCheckForm">
                            	<div class="checkLeft">
                                	<div class="check">
                                     <input type="radio" id="ChkEmail" class="ch" runat="server" name="RegistrationForm" /> <%--<input type="password"  id="txtPwd"   runat="server" name="user" class="textfield"/>--%>
                                     </div>
                                    <div class="label">    
                                    	<span><%= Me.BusinessDictionaryManager.Read("DMP_CheckEmail", _langKey.Id, "Email")%></span>
                                    </div>    
                                </div>
                                <div class="checkRight">
                                	<div class="check">
                                    	 <input type="radio" id="ChkPhone" class="ch" runat="server" name="RegistrationForm" />
                                     <div id="Div1" style="display:none; color:#FF0000;">  <%= Me.BusinessDictionaryManager.Read("DMP_InsertNumber", _langKey.Id, "DMP_InsertNumber")%></div>
                                    </div>
                                    <div class="inpTel">
                                    	 <input ID="TxtPhone" value="Telephone" onfocus ="javascript:cancellaValue(this.id,'Telephone');" onblur="javascript:test2(this.id,'Telephone');" runat="server" class="textfield" maxlength="15" />
                                         <div id="NumeroObbligatorio" style="display:none; color:#FF0000;">  <%= Me.BusinessDictionaryManager.Read("DMP_InsertNumber", _langKey.Id, "DMP_InsertNumber")%></div>
                                         </div>
                                    </div>
                                </div>
                            </div><!--end contCheckForm-->
                     
                    </div><!--end right-->
              </div><!--end contFormFields-->         
  </div>
   
  
             <%-- <asp:Button ID="Button1" OnClientClick="Close();return false;" runat="server" Text="Annulla" class="button2"/>--%>
            	<div class="floatRight">
                	<div class="regFormBtn">
                		<asp:Button ID="Button2" OnClientClick="return ValidateCheckBox();" OnClick="UpdateRegistrazione" runat="server" Text="Submit" class="buttonOK" ValidationGroup="RegistrationForm" />	
                    </div>
                </div>
           
</div>
</div>
<div id="MessageOK1" style="display:none">
          	<div class="center">
             <h3><%= Me.BusinessDictionaryManager.Read("DMP_TitleModifyOK", _langKey.Id, "DMP_TitleModifyOK")%></h3>
            <div class="regCodeCont"> 
            <%= Me.BusinessDictionaryManager.Read("DMP_TxtModifyOK", _langKey.Id, "DMP_TxtModifyOK")%>
            </div>
            <div class="floatRight">
                	<a href="javascript:void(0)" onclick = "Close()"><%= Me.BusinessDictionaryManager.Read("DMP_BackHome", _langKey.Id, "Back Home")%></a> 
                </div>
             </div> 
          
             <%-- <asp:Button ID="Button1" OnClientClick="Close();return false;" runat="server" Text="Annulla" class="button2"/>--%>
            	
           
       
       </div>
 </div>
 
 </div>
