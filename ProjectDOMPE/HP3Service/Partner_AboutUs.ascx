﻿<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Linq"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
    Dim abstract As String
    Dim FullText As String
    Dim FullTextOriginal As String
	Private _isMobile as boolean = false
    
	Dim strQuery As String = "readPartners"
	Dim _objCachedDataSet As DataSet = Nothing
	Dim oDt as DataTable = Nothing
	
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
	End Sub

	        
	Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
             if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
                 userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
                Return true
            End If
        End if
        Return False
    End Function
	
	Function LoadPartner() As DataTable
		Dim _cacheKey as String = "partnerDataSet"
		Dim oDt as DataTable = Nothing
		If request("cache") = "false" Then cacheManager.RemoveByKey(_cacheKey)
		
		_objCachedDataSet = cacheManager.Read(_cacheKey, 1)
		If _objCachedDataSet Is Nothing Then
			If Not String.IsNullOrEmpty(strQuery) Then
				Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQuery, False)
			
				If Not String.IsNullOrEmpty(query) Then
					Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, Nothing) 'Al posto di Nothing vanno  passati gli SqlParams
					If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
						_objCachedDataSet = ds
						'Cache Content and send to the oputput-------
						CacheManager.Insert(_cacheKey, _objCachedDataSet, 1, 120)
						'--------------------------------------------							
					Else
						Return Nothing
					End If
				End If
			End If
		End If	

		Return  _objCachedDataSet.Tables(0)
	End Function	
    
    
    Sub Page_Load()
       _isMobile = IsMobileDevice()
        Dim cntSrc As New ContentExtraSearcher
        Dim cntcoll As New ContentExtraCollection
        Dim cntsearch As New ContentExtraSearcher
        Dim cntVal As New ContentExtraValue
        cntsearch.key.IdLanguage = 2'_langKey.Id
        cntsearch.key.Id = Me.PageObjectGetContent.Id
        cntsearch.KeySite = Me.PageObjectGetSite
        cntsearch.KeySiteArea.Id = _siteArea.Id
        cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
       
        If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
            abstract = cntcoll(0).Abstract
            FullText = cntcoll(0).FullText
			oDt = LoadPartner()
			
			Dim sb as New StringBuilder()
			If Not oDt Is Nothing AndAlso oDt.Rows.Count > 0 Then
				sb.Append("<table cellpadding='0' cellpsacing='0'><thead style='padding:0;width:600px;'><tr><th>Country</th><th>Direct Partner</th><th>Indirect Partner</th><th>Brand</th></tr></thead><tbody>")
				Dim _strCountry As String = String.Empty
				Dim _strPartner As String = String.Empty
				Dim _strRowColor As String = String.Empty
				Dim _count as Integer = 1
				For Each row As DataRow In oDt.Rows
					If _strCountry <> row("partner_country") Then 
						if String.IsNullOrEmpty(_strRowColor) then
							_strRowColor = " class='grey'"
						else
							_strRowColor = String.Empty
						end if					
						sb.Append("<tr" & _strRowColor & ">")
						_strCountry = row("partner_country")
						_strPartner = String.Empty
						sb.Append("<td>" & row("partner_country") & "</td>")
					Else
						sb.Append("<tr" & _strRowColor & ">")
						sb.Append("<td>&nbsp;</td>")
					End If

					If _strPartner <> row("partner_directPartner") Then
						sb.Append("<td>" & row("partner_directPartner") & "&nbsp;</td>")
						_strPartner = row("partner_directPartner")
					Else
						sb.Append("<td>&nbsp;</td>")
					End If

					sb.Append("<td>" & row("partner_indirectPartner") & "&nbsp;</td>")
					sb.Append("<td>" & row("partner_brand") & "</td>")
					sb.Append("</tr>")
					_count += 1
				Next
				sb.Append("</tbody></table>")
			End If			
			FullText = FullText.replace("[PARTNERTABLE]", sb.ToString())

            FullTextOriginal = cntcoll(0).FullTextOriginal
            ' fulltext.Text = cntcoll(0).FullText
        End If
            
        Dim soT As New ThemeSearcher
        Dim collT As ThemeCollection = Nothing
        soT.KeySite = Me.PageObjectGetSite
        soT.KeyFather.Id = Dompe.ThemeConstants.idAreaPartner
        soT.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        collT = Me.BusinessThemeManager.Read(soT)
           
        If Not collT Is Nothing AndAlso collT.Count > 0 Then
            rptSiteMenu.DataSource = collT
            rptSiteMenu.DataBind()
        End If
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Function GetLink(ByVal vo As ThemeValue) As String
        
        If vo.Key.Id = Dompe.ThemeConstants.partnerNews Then
            Return Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idNews, _langKey.Id)
        End If
        
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, vo.Key.Id, _langKey.Id)
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
</script>



<section>
    <div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDompe/_slice/aboutUsHeader.jpg');">            
        </div>    
        <div class="txt">
                <h1 class="grey">About us</h1>
        </div>
        <div class="layer">&nbsp;</div>
    </div><!--end slider-->
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
 	<%--<div class="networkSection" style="background-image: url('/ProjectDompe/_slice/aboutUsHeader.jpg') !important;background-position:center top;">
    	<div class="boxCntGeneric">
            <div class="container">
                <h2 class="grey">About us</h2>
            </div>
        </div>
    </div><!--end networkSection-->--%>
</section>
<section>

    <div class="container">
        <div class="colLeft">        	
                <div class="listSide">                	
                     <asp:Repeater ID="rptSiteMenu" runat="server">
                        <HeaderTemplate><ul></HeaderTemplate> 
                        <ItemTemplate>
                            <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# Me.BusinessDictionaryManager.Read("thm_" & DataBinder.Eval(Container.DataItem, "key.id"), _langKey.Id)%></a></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>  
                </div>            
        </div><!--end colLeft-->
        <div class="colRight">
            <div class="content">
                <div class="singleAcc" val="c">
                    <div class="about1 aboutAcc">
                    	<h3 style="padding-left:30px;">Domp&eacute; products</h3>
                    </div>
                    <div class="infoAcc1" style="display:none;">
                        <%=abstract%>
                    </div><!--end infoAcc-->
                </div><!--end singleAcc-->
                <div class="singleAcc" val="c">
                    <div class="about2 aboutAcc">
                    	<h3 style="padding-left:30px;">Business Opportunity</h3>
                    </div>
                    <div class="infoAcc1" style="display:none;"><%=Fulltext%></div><!--end infoAcc-->
                </div><!--end singleAcc-->
                <div class="singleAcc" val="c">
                    <div class="about3 aboutAcc">
                    	<h3 style="line-height: 40px;padding-top:20px;padding-left:30px;">Organization<br /> and management</h3>
                    </div>
                    <div class="infoAcc1" style="display:none;">
                        <%=FulltextOriginal%>
                    </div><!--end infoAcc-->
                </div><!--end singleAcc-->
            </div><!--end accCont-->
        </div><!--end colRight-->
        
    </div><!--end container-->

</section>
<script type="text/javascript">
	$(document).ready(function () {
		$(".infoAcc1").first().css("display", "block");
		$(".singleAcc").first().attr("val", "o");            
	});

	$(".aboutAcc").click(function () {
		var valore = $(this).parent().attr("val");
		if (valore == "c" || valore == undefined) {
			$(this).parent().attr("val", "o");
			$(this).next().css("display", "block");
		} else {
			$(this).parent().attr("val", "c");
			$(this).next().css("display", "none");
		}
	});
	
	<% 'If Not _isMobile Then %>
		//	$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	<% 'end if %>	
</script>