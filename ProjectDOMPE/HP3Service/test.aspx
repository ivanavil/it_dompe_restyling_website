﻿<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Content" %>

<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>

<!DOCTYPE html>

<script runat="server">

    Protected Sub Page_load()
        InitView()
    End Sub
    
    Protected Sub InitView()
        
        Dim res As String = "#"
        Dim pk As Integer = 181
        If pk > 0 Then
            Dim dwncoll As New ContentCollection
            Dim dwnsearch As New ContentSearcher
            Dim soRel As New ContentRelatedSearcher
            Dim coll As ContentCollection = Nothing
            Dim manCont As New ContentManager

            ' 
            soRel.Content.Key.PrimaryKey = pk
            soRel.Content.KeyRelationType.Id = 4
            soRel.Content.RelationSide = RelationTypeSide.Right
            soRel.Content.RelationTypeSearcher = RelationTypeSearch.SelectedType
            ' 

            'dwnsearch.KeySiteArea.Id = 94 'Dompe.SiteAreaConstants.IDDownloadArea
            dwnsearch.SortType = ContentGenericComparer.SortType.ByData
            dwnsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
           ' dwnsearch.KeyContentType.Id = 135
            dwnsearch.RelatedTo = soRel

            dwncoll = manCont.Read(dwnsearch)

            If Not dwncoll Is Nothing AndAlso dwncoll.Count > 0 Then
                res = "/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(dwncoll(0).Key.PrimaryKey)
            End If
        End If
        
        Response.Write(res)
        
    End Sub
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
