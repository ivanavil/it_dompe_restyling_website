﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        contentId = Me.PageObjectGetContent.Id
    End Sub
        
    
    Sub Page_Load()
      
        If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            idContentTheme = valthem.KeyContent.Id
          
           ' verifico se il content è quello associato al tema allora carico il contenuto generico
            If contentId = idContentTheme Then
                Dim src As New ContentExtraSearcher
                Dim sCol As New ContentExtraCollection
                
                src.KeySiteArea.Id = _siteArea.Id
                src.KeyContentType.Id = Me.PageObjectGetContentType.Id
                src.key.IdLanguage = _langKey.Id
                src.key.Id = contentId
                sCol = Me.BusinessContentExtraManager.Read(src)
                
             
                If Not sCol Is Nothing AndAlso sCol.Count > 0 Then
                    titolo_sezione.Text = sCol(0).Title
                    testo_sezione.Text = sCol(0).FullText
                    testo_subtitle.Text = sCol(0).Abstract
            
                    
                End If
           
                plhContentGeneric.Visible = True
                rptGallery.DataSource = LoadContent()
                rptGallery.DataBind()
                                 
            
            End If
                               
            Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
            LoadSubControl(pathPressMenu, PHL_Press_Menu)
            
        End If
                  
    End Sub
    
    
    Function LoadContent() As ContentCollection
        
        Dim idContentKeyTheme As Integer = GetPKThema()
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        so.key.IdLanguage = Me.PageObjectGetLang.Id
   
        
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.KeysToExclude = idContentKeyTheme
     
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        Else
            Return Nothing
        End If
        
    End Function

    Function GetPKThema() As Integer
        Dim idContentKeyTheme As Integer = 0
        Dim idContentTheme As Integer
        Dim contentextraVal As New ContentExtraValue
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Me.BusinessMasterPageManager.GetTheme.Id))
        idContentTheme = valthem.KeyContent.Id
        
        contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(idContentTheme, Me.PageObjectGetLang.Id))
        If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
            idContentKeyTheme = contentextraVal.Key.PrimaryKey
        End If
        Return idContentKeyTheme
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetImageContent(ByVal IDcont As Integer, ByVal ImgWidth As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, Me.PageObjectGetLang.Id), ImgWidth, title, title)
        Return res
    End Function
    
</script>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
<div class="presentation">
	<div class="presentationBox">
                <div class="presentationBox2">
                    <div class="presentationBox3">
                     	<div class="container">
                          	<h2><asp:Literal ID="titolo_sezione" runat="server" /></h2>
                       </div>
                   </div>
              </div>
    </div>
</div>
	<div class="body">
    	<div class="container">
        	<div class="bodyBox">
            	
                <div class="colLeft">
                	
                    	<asp:PlaceHolder ID="PHL_Press_Menu" runat="server" Visible="true" />
                    
                </div><!--end colLeft-->
                
                <div class="colRight">
                    <div class="searchBox">
                        <div class="searchFormGallery">
                    		<input type="text" name="cerca2" value="Cerca">
                        </div>
                    </div><!--end searchBox-->
                    <div class="archiveBox">
                        
                        <asp:Literal ID="testo_subtitle" runat="server" />
                        <asp:Literal ID="testo_sezione" runat="server" />


                  <asp:Repeater ID="rptGallery" runat="server">
                <HeaderTemplate>              
               </HeaderTemplate> 
                <ItemTemplate>
                <div class="boxPhotoGallery noMargin">
                            <div class="cnt">
                            	<a class="effectHover" title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundLink('/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l='<%= Me.PageObjectGetLang.Code %>'c='<%# Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>','Gallery Press','Open','<%# CType(Container.DataItem, ContentValue).Title%>')"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                            	 <a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundLink('/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l='<%= Me.PageObjectGetLang.Code %>'c='<%# Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>','Gallery Press','Open','<%# CType(Container.DataItem, ContentValue).Title%>')"><%# GetImageContent(DataBinder.Eval(Container.DataItem, "Key.Id"), 150, DataBinder.Eval(Container.DataItem, "Title"))%></a>
                                <h3><a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundLink('/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l='<%= Me.PageObjectGetLang.Code %>'c='<%# Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>','Gallery Press','Open','<%# CType(Container.DataItem, ContentValue).Title%>')"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></h3>
                                <p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>    
                            </div>
                        </div><!--end boxPhotoGallery-->               
                 
                </ItemTemplate>
                <FooterTemplate></FooterTemplate>
            </asp:Repeater>


                        
                      
                        
                        <div class="floatRight clearBoth">
                        	<div class="paginator">
                            	<ul>
                                	<li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="sel" href="#">4</a></li>
                                    <li><a href="#">...</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        
                    </div><!--end archiveBox-->
                </div><!--end colRight-->	
                
                
            </div><!--end bodyBox-->
        </div><!--end container-->
    </div><!--end body-->













</asp:PlaceHolder>

