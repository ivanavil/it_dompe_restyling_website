﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
    Private _isMobile As Boolean = False

		
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
    End Sub

	Public Function IsMobileDevice() As Boolean
		Dim userAgent = Request.UserAgent.ToString().ToLower()
		If Not String.IsNullOrEmpty(userAgent) Then
			if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
			userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
				Return true
			End If
		End if
		Return False
	End Function		
    
    Dim _canCheckApproval As Boolean = False
    Sub Page_Load()
        _isMobile = IsMobileDevice()
      '  readXmlFilter()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
	
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/MenuAreaMedico.ascx", _siteFolder)
        LoadSubControl(pathPressMenu, PHL_Med_Menu)
        Dim pathRichiediArticolo As String = String.Format("/{0}/HP3Common/BoxRichiediArticolo.ascx", _siteFolder)
        LoadSubControl(pathRichiediArticolo, phRichiediArticolo)
               
        Dim so As New ThemeSearcher()
        so.Key = Me.PageObjectGetTheme
        so.KeyLanguage.Id = 1'_langId
        so.LoadHasSon = False
        so.LoadRoles = False
        so.Properties.Add("Description")
     
        Dim coll = Me.BusinessThemeManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any() Then
            titolo_sezione.Text = coll(0).Description
        End If
        
        
        If Me.PageObjectGetContent.Id > 0 Then
            
            repContenuti.Visible = False
            archivio.Visible = False
            
            
            Dim soEx As New ContentExtraSearcher()
            soEx.key.Id = contentId
            soEx.key.IdLanguage = _langKey.Id
            soEx.CalculatePagerTotalCount = False
            soEx.SetMaxRow = 1
            soEx.KeySite = Me.PageObjectGetSite
            If Not _canCheckApproval Then soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
            If Not contentextraColl Is Nothing AndAlso contentextraColl.Any() Then
                contentextraVal = contentextraColl.FirstOrDefault
            
                'contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, 1))
                'If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                fulltext.Text = "<h3>" & contentextraVal.Title & Healthware.Dompe.Helper.Helper.checkApproval(contentextraVal.ApprovalStatus) &"</h3>"
                fulltext.Text = fulltext.Text & contentextraVal.FullText
                Dettaglio.Visible = True
                fulltext.Visible = True
            End If
        
            
        Else
            'If Not Page.IsPostBack Then
            '    Dim cntSrc As New ContentExtraSearcher
          
            '    Dim cntsearch As New ContentExtraSearcher
            '    Dim cntVal As New ContentExtraValue
            '    cntsearch.key.IdLanguage = _langId
            '    cntsearch.KeySite.Id = Me.BusinessMasterPageManager.GetSite.Id
            '    cntsearch.KeySiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
            '    cntsearch.KeyContentType.Id = Me.BusinessMasterPageManager.GetContentType.Id
      
            '    cntsearch.SortType = ContentGenericComparer.SortType.ByData
            '    cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
            '    getContent(cntsearch)
            'End If
        End If
             
     
        ' loadBrand()
        'If Not Page.IsPostBack Then
        '    selAree.InnerText = Me.BusinessDictionaryManager.Read("DMP_SelAreeTh", 1, "Tutte le aree terapeutiche")
        '    selPrincipi.InnerText = Me.BusinessDictionaryManager.Read("DMP_SelPrincipi", 1, "Tutti i Principi attivi")
        '    selBrand.InnerText = Me.BusinessDictionaryManager.Read("DMP_SelBrand", 1, "Tutti i Brand")
        'End If
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    'Sub FilroCntx(obj As Object, ev As EventArgs)
    '    If Page.IsPostBack Then
    '        'Dim strItem As RepeaterItem
    '        Dim cntSrc As New ContentExtraSearcher
    '        Dim cntsearchx As New ContentExtraSearcher
    '        Dim stringaContext As String = String.Empty
            
    '        'For Each strItem In rpContext.Items
            
    '        '    Dim chkCaratteristiche As DropDownList = strItem.FindControl("repCaratteristiche")
    '        '    If chkCaratteristiche.SelectedValue <> String.Empty Then
    '        '        stringaContext = stringaContext & chkCaratteristiche.SelectedValue & ","
    '        '    End If
            
    '        'Next strItem
            
    '        If Not String.IsNullOrEmpty(hiddenareetxt.Value) Then
    '            selAree.InnerText = hiddenareetxt.Value
    '        End If
    '        If Not String.IsNullOrEmpty(hiddenprincipitxt.Value) Then
    '            selPrincipi.InnerText = hiddenprincipitxt.Value
    '        End If
    '        If Not String.IsNullOrEmpty(hiddenbrandtxt.Value) Then
    '            selBrand.InnerText = hiddenbrandtxt.Value
    '        End If
            
    '        If Not _canCheckApproval Then cntsearchx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            
    '        Dim tmpVal As Integer = 0
    '        If Not String.IsNullOrEmpty(hiddenaree.Value) AndAlso Integer.TryParse(hiddenaree.Value, tmpVal) Then
    '            cntsearchx.KeysContext += tmpVal.ToString() & ","
    '        End If
    '        If Not String.IsNullOrEmpty(hiddenprincipi.Value) AndAlso Integer.TryParse(hiddenprincipi.Value, tmpVal) Then
    '            cntsearchx.KeysContext += tmpVal.ToString() & ","
    '        End If
    '        If Not String.IsNullOrEmpty(hiddenbrand.Value) AndAlso Integer.TryParse(hiddenbrand.Value, tmpVal) Then
    '            cntsearchx.KeysContext += tmpVal.ToString() & ","
    '        End If
            
    '         Response.Write("KeysContext:" & cntsearchx.KeysContext)
    '        'selAree.InnerText=cntsearchx.KeysContext
            
    '        'cntsearchx.KeysContext = stringaContext
    '        cntsearchx.key.IdLanguage = 1'Me.BusinessMasterPageManager.GetLanguage.Id
    '        cntsearchx.KeySite.Id = Me.BusinessMasterPageManager.GetSite.Id
    '        cntsearchx.KeySiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
    '        cntsearchx.KeyContentType.Id = Me.BusinessMasterPageManager.GetContentType.Id
      
    '        cntsearchx.SortType = ContentGenericComparer.SortType.ByData
    '        cntsearchx.SortOrder = ContentGenericComparer.SortOrder.ASC
    '        getContent(cntsearchx)
    '    End If
    'End Sub
    
    
    Sub getContent(srcContenuti As ContentExtraSearcher)
        Dim cntscoll As New ContentExtraCollection
        Dim man As New ContentExtraManager
        cntscoll = man.Read(srcContenuti)
        ' Response.Write(
        ' "l:"& srcContenuti.key.IdLanguage &"<br>"&        "sid:"& srcContenuti.KeySite.id &"<br>" &       "said:"&  srcContenuti.KeySiteArea.Id &"<br>"& _
           ' "sCtypeid:"& srcContenuti.KeyContentType.Id &"<br>" & _
        '"sContexts:"& srcContenuti.KeysContext &"<br>"  & _
      '  "-" & cntscoll.Count)
        If Not cntscoll Is Nothing AndAlso cntscoll.Count > 0 Then
         
            repContenuti.DataSource = cntscoll
            repContenuti.DataBind()
            repContenuti.Visible = True
            NoContent.Visible = False
        Else
            NoContent.Visible = True
            NoContent.Text = Me.BusinessDictionaryManager.Read("Nessun risultato in archivio", Me.PageObjectGetLang.Id, "Nessun risultato in archivio")
            repContenuti.Visible = False
        End If
        Dettaglio.Visible = False
        fulltext.Visible = False
        archivio.Visible = True
        'getCategorie()
        
    End Sub
    
    
    Sub getCategorie()
        If Not Page.IsPostBack Then
            Dim oContextSearcher As New ContextSearcher
            Dim oContextCollection As New ContextCollection
            oContextSearcher.KeyContextGroup.Id = Dompe.ContextGroup.CaratteristicheProdotti 'INSERIRE IL GROUP CONTEXT ID
            oContextSearcher.Key.Language.Id = 1'_langId
       
            oContextCollection = Me.BusinessContextManager.Read(oContextSearcher)
                
            If Not oContextCollection Is Nothing AndAlso oContextCollection.Count > 0 Then
                rpContext.DataSource = oContextCollection
                rpContext.DataBind()
                If rpContext.Items.Count() > 0 Then rpContext.Visible = True
            End If
        
            oContextCollection = Nothing
        End If
    End Sub
     
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""

        'Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        'imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High
        'res = Me.BusinessContentManager.GetCover(Me.PageObjectGetContent, 205, 0, imgqual)		
		'response.write(res2)
		'response.end
		
		
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 205, title, title)
        Return res
    End Function
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
       
    
    Function getProductContext(ByVal cntx As ContextValue) As ListItem()
       
        Dim soc As New ContextSearcher()
        soc.KeyFather.Id = cntx.Key.Id
        soc.Key.Language.Id = 1'Me.PageObjectGetLang.Id
        Dim collcx As ContextCollection = Me.BusinessContextManager.Read(soc)
        Dim list1 As New List(Of ListItem)
        If Not collcx Is Nothing And collcx.Count() > 0 Then
            Dim ex As ContextValue
            Dim cont As Integer = 0
            For Each ex In collcx
                Dim newItem As New ListItem()
                newItem.Text = ex.Description
                newItem.Value = ex.Key.Id.ToString()
                newItem.Attributes.Add("value", ex.Key.Id.ToString())
                list1.Insert(cont, newItem)
                cont = cont + 1
            Next
        End If
        
        Return list1.ToArray()
     
    End Function
    
    Function getProductDocument(ByVal cnt As ContentExtraValue) As DownloadCollection
        Dim so As New DownloadSearcher()
        so.key.Id = cnt.Key.Id
        so.key.IdLanguage = 1'Me.PageObjectGetLang.Id
        Dim collMaterial As DownloadCollection = Me.BusinessContentManager.ReadDownloadRelation(so)
        If Not collMaterial Is Nothing And collMaterial.Count() > 0 Then
            Return collMaterial
        Else
            Return Nothing
        End If
    End Function
    
    Sub TakeDownload(ByVal s As Object, ByVal e As System.EventArgs)
       
        Dim HeaderValue As String = ""
        Dim findString As String = " ,/,?,&,#,:"
        Dim findArray() As String = findString.Split(",")

        For Each find As String In findArray
            HeaderValue = s.commandargument.ToString.Split(";")(1).Replace(find, "-")
        Next
             
        Dim oDownManager As New DownloadManager
        Dim oDownloadValue As DownloadValue = oDownManager.Read(New ContentIdentificator(HeaderValue), New DownloadTypeIdentificator(s.commandargument.ToString.Split(";")(0)))
        Dim filename As String = oDownloadValue.Title.ToLower

		response.write(filename)
		response.end
		
        Response.ContentType = "application/octet-stream"
        Response.AddHeader("Content-Type", "application/octet-stream")
        
        If Not oDownloadValue Is Nothing Then
            'TRACCIAMENTO?
            DownloadManager.TakeDownload(oDownloadValue, oDownloadValue.Title.ToLower.Replace(" ", "-"))
        End If
                 
    End Sub
    
    Protected Sub rpContext_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If Not Page.IsPostBack Then
            Dim currContext As ContextValue = CType(e.Item.DataItem, ContextValue)
        
            Dim repCaratteristiche As DropDownList = CType(e.Item.FindControl("repCaratteristiche"), DropDownList)
            repCaratteristiche.Items.AddRange(getProductContext(currContext))
            repCaratteristiche.DataBind()
        End If
    End Sub
    

    
    
    Function getContextByidContent(ByVal conttId As Integer, Rty As Integer) As String
        Dim result As String = String.Empty
        
        Dim contextCollControl As New ContextCollection
        Dim ctnx As New ContextSearcher
        Dim soRel As New ContextRelatedSearcher
        ctnx.KeyContent.Id = conttId
        ctnx.Key.Language.Id = Me.PageObjectGetLang.Id
       
        soRel.Context.KeyRelationType = New RelationTypeIdentificator(Rty)
        ctnx.KeySiteArea.Id = Dompe.SiteAreaConstants.IDVideoGallery
        ctnx.RelatedTo = soRel
        
        
        contextCollControl = Me.BusinessContentManager.ReadContentContext(ctnx)
        If Not contextCollControl Is Nothing AndAlso contextCollControl.Any Then
            result = contextCollControl(0).Description
        End If
         
        
        Return result
    End Function

    'function loadAree()As String 
    '    Dim ctxArea = 6
    '    Dim tmpval As String = String.Empty
        
    '    Dim tmpstr As String = String.Empty
    '    tmpstr=String.Format("<li><a href='#' rel=''>{0}</a></li>",Me.BusinessDictionaryManager.Read("DMP_SelAreeTh", 1, "Tutte le aree terapeutiche"))
    '    tmpstr = tmpstr + getProductContext(ctxArea, tmpval)
    '    return tmpstr 
    'End Function
    'Function loadPrincipi(Optional ByVal idFhater As Integer = 7) As String
    '    Dim ctxArea = idFhater
    '    Dim tmpval As String = String.Empty
        
    '    Dim tmpstr As String = String.Empty
    '    tmpstr = String.Format("<li><a href=""#"" rel="""">{0}</a></li>", Me.BusinessDictionaryManager.Read("DMP_SelPrincipi", 1, "Tutti i Principi attivi"))
    '    If Not String.IsNullOrEmpty(tmpval) Then
    '        selPrincipi.InnerText = tmpval
    '    End If
    '    tmpstr = tmpstr + getProductContext(ctxArea, tmpval)
        
    '    Return tmpstr
    'End Function
    'Dim strBrand As String = String.Empty
    'Function loadBrand() As String
    '    Dim ctxArea = 8
    '    Dim tmpval As String = String.Empty
        
    '    Dim tmpstr As String = String.Empty
    '    tmpstr = String.Format("<li><a href='#' rel=''>{0}</a></li>", Me.BusinessDictionaryManager.Read("DMP_SelBrand", 1, "Tutti i Brand"))
    '    tmpstr = tmpstr + getProductContext(ctxArea, tmpval)
    '    If Not String.IsNullOrEmpty(tmpval) Then
    '        selBrand.InnerText = tmpval
    '    End If
    '    Return tmpstr
    'End Function
    
    '  Function getProductContext(ByVal cntxFather As Integer, ByRef val As String  ) As String
    'Dim res As String = String.Empty
    'Dim _cacheMenuCtx = "_cacheRcpDropDownMenu:" & cntxFather & "|" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id
    'If Request("cache") = "false" Then
    '          CacheManager.RemoveByKey(_cacheMenuCtx)
    '      End If
	
    'Dim _strCachedDDlMenu as String = CacheManager.Read(_cacheMenuCtx, 1)
    'If Not (_strCachedDDlMenu Is Nothing) AndAlso (_strCachedDDlMenu.Length > 0) Then
    '	res = _strCachedDDlMenu
    'Else
    '       Dim soc As New ContextSearcher()
    '	soc.KeyFather.Id = cntxFather
    '	soc.Key.Language.Id = 1'Me.PageObjectGetLang.Id
    '	Dim collcx As ContextCollection = Me.BusinessContextManager.Read(soc)

    '	Dim sBuilder As New StringBuilder()
    '	If Not collcx Is Nothing And collcx.Count() > 0 Then
    '	collcx.sort(2,1)
    '		Dim ex As ContextValue
    '		Dim cont As Integer = 0
    '		For Each ex In collcx
    '			If Not String.IsNullOrEmpty(hiddenaree.Value) AndAlso CInt(hiddenaree.Value) = ex.Key.Id Then
    '				val = ex.Description
    '			End If
    '			If Not String.IsNullOrEmpty(hiddenprincipi.Value) AndAlso CInt(hiddenprincipi.Value) = ex.Key.Id Then
    '				val = ex.Description
    '			End If
    '			If Not String.IsNullOrEmpty(hiddenbrand.Value) AndAlso CInt(hiddenbrand.Value) = ex.Key.Id Then
    '				val = ex.Description
    '			End If
    '                  sBuilder.Append(String.Format("<li><a href=""#"" rel=""{0}"">{1}</a></li>", ex.Key.Id, ex.Description))
    '		Next
    '	End If
    '	res = sBuilder.ToString()	

    '	'Cache Content and send to the oputput-------
    '	CacheManager.Insert(_cacheMenuCtx, res, 1, 120)
    '	'--------------------------------------------			
    'End If

    '      Return res
        
        
    '  End Function
    
        
</script>
<style>
    .LnkReset
    {
        clear:both;
        float:left;
        width:251px;
        margin: 20px 0 0 250px;
        font-weight:bold;
    }
    #ulFilterAree li
    {
        padding: 0;
        background-image: none;
    }
    #ulFilterPrincipi li
    {
        padding: 0;
        background-image: none;
    }
    #ulFilterBrand li
    {
        padding: 0;
        background-image: none;
    }
</style>

<section>
    <div class="cont-img-top">
        <div class="bg-top archiveSection"></div><!--end brandSection-->
    	    
                <div class="txt">
                    <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>
                </div>
           <div class="layer">&nbsp;</div>        
    </div>
     <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

<section>
	<div class="container">
         <div class="colLeft">        	
            	<div class="listSide">
                  <asp:PlaceHolder ID="PHL_Med_Menu" runat="server" Visible="true" />
                </div>                
        </div><!--end colLeft-->

         <div class="colRight">
             <div class="content">
                  <%-- DETTAGLIO --%> 
                <asp:PlaceHolder ID="Dettaglio" runat="server" EnableViewState="False" >
			            <asp:literal id="fulltext" runat="server" EnableViewState="False" />
                </asp:PlaceHolder>

                <%-- ARCHIVIO --%> 
                <asp:PlaceHolder ID="archivio" runat="server" >
                <%-- CATEGORIE--%>
                <div class="searchRCP">
                    <div class="searchRCPLeft">

                        <asp:Repeater ID="rpContext" runat="server" OnItemDataBound="rpContext_ItemDataBound"  Visible="false">                           
                            <ItemTemplate>
                                <div style="clear:both;"></div>
                                <a href="#" title='<%# DataBinder.Eval(Container.DataItem, "Description")%>'><%# DataBinder.Eval(Container.DataItem, "Description")%></a>
                                <asp:DropDownList ID="repCaratteristiche" runat="server">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>   
                                                   
                            </ItemTemplate>
                        </asp:Repeater>
                 
                     <div id="FilterDiv">
                          <%--<div class="selMed">
                          <a href="#" title='' id="selAree" runat="server" rel="ulFilterAree" class="selectddl"></a>
                   <input type="hidden" ID="hiddenaree" runat="server" Value="" />
                             <input type="hidden" ID="hiddenareetxt" runat="server" Value="" />
                           <a href="#" title=' ' id="selPrincipi" runat="server" rel="ulFilterPrincipi" class="selectddl"></a>
                        
                             <input type="hidden" ID="hiddenprincipi" runat="server" Value="" />
                             <input type="hidden" ID="hiddenprincipitxt" runat="server" Value="" />
                         <a href="#" title='' id="selBrand" rel="ulFilterBrand" runat="server" class="selectddl"></a>
                    
                         <input type="hidden" ID="hiddenbrand" runat="server" Value="" />
                         <input type="hidden" ID="hiddenbrandtxt" runat="server" Value="" />
                              </div>--%>
                   </div>
                              </div>
                    <div class="searchRCPRight">
    <%--                    <asp:LinkButton runat="server" cssclass="viewMore" OnClick="FilroCntx"><%'=Me.BusinessDictionaryManager.Read("Cerca", Me.PageObjectGetLang.Id, "Cerca")%></asp:LinkButton>--%>
                        <a id="searchLink" class="viewMore" style="cursor:pointer;" >Cerca</a>
                    </div>
                    <%--<asp:LinkButton ID="LinkButton1" CssClass="LnkReset" Visible="false" runat="server" OnClientClick="return ResetForm();"><%=Me.BusinessDictionaryManager.Read("Reset", Me.PageObjectGetLang.Id, "Reset")%></asp:LinkButton>--%>
                     </div>
                          <div class="eventiContInt marginTop10"> 
                          
                             <%-- ELENCO PRODOTTI --%>       	
                              <asp:Repeater ID="repContenuti" runat="server" EnableViewState="False">
                                  <HeaderTemplate>
                                      <div class="archiveRCP">
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                     <div class="itemRCP">
                                        <h3><%# DataBinder.Eval(Container.DataItem, "title")%> <%#Healthware.Dompe.Helper.Helper.checkApproval(DataBinder.Eval(Container.DataItem, "ApprovalStatus")) %></h3>
                                        <div class="cnt">
                                            <div class="cover"><%#GetImage(DataBinder.Eval(Container.DataItem, "key.id"), DataBinder.Eval(Container.DataItem, "title"))%></div>
                                            <div class="cntTxt">
                                                <p><strong><%=Me.BusinessDictionaryManager.Read("Principio attivo", Me.PageObjectGetLang.Id, "Principio attivo")%></strong>
                                                    <br />
												    <%# DataBinder.Eval(Container.DataItem, "TitleContentExtra")%>
												    <%'#getContextByidContent(DataBinder.Eval(Container.DataItem, "key.id"),Dompe.RelationType.PrincipiAttivi) %>
                                                </p>
                                                <p><strong><%=Me.BusinessDictionaryManager.Read("Area", Me.PageObjectGetLang.Id, "Area")%></strong>
                                                <br /><%# DataBinder.Eval(Container.DataItem, "SubTitle")%>
                                                    <%'#getContextByidContent(DataBinder.Eval(Container.DataItem, "key.id"),Dompe.RelationType.AreaTerapeutica) %>
                                                </p>
                                            </div>
                                                <%--DOCUMENTI CORRELATI--%>
											     <asp:Repeater ID="rpRelatedMaterial" runat="server" DataSource='<%#getProductDocument(Container.DataItem)%>'>     
                                                     <HeaderTemplate>
                                                         <div style="clear:both;float:left;">
                                                         <span style="font-weight: bold; padding-left: 25px;">DOWNLOAD</span>
                                                             <ul>
                                                     </HeaderTemplate>
												        <ItemTemplate>
                                                            <li><a class="pdfRCP" title="<%#Container.DataItem.Title%>" href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(Container.DataItem.DownloadTypeCollection(0).KeyContent.PrimaryKey)%>"><%#Container.DataItem.Title%></a></li>																													
												        </ItemTemplate>
                                                     <FooterTemplate>
                                                            </ul></div>
                                                     </FooterTemplate>
											    </asp:Repeater>
                                        
                                        </div>
                                    </div>
                                    </ItemTemplate>
                                  <FooterTemplate></div></FooterTemplate>
                              </asp:Repeater>          
                <asp:Literal ID="NoContent" runat="server" Visible="false"></asp:Literal>
                </div><!--end eventiContInt-->
                 <asp:PlaceHolder ID="phRichiediArticolo" runat="server" Visible="true" />
             
                 </asp:PlaceHolder>   
             </div>
        </div><!--end colRight-->
       
    </div><!--end container--> 
</section>

<script>
    var brandId = ""
    var brandTxt = ""
    var principioId = ""
    var principioTxt = ""
    var AreaId = ""
    var AreaTxt = ""
    var selClick = ""
    $(document).ready(function () {
        AddFilter();
    });

    function removeAddlink() {
        $(".selMed").remove();
    }

    function AddFilter() {
            
      
        $.ajax({
            contentType: "text/html; charset=utf-8",
            url: '/ProjectDOMPE/_async/loadFilterList.aspx',
            dataType: "html",
            data: { 'selClick': selClick, 'AreaVal': AreaId, 'AreaText': AreaTxt, 'principioVal': principioId, 'principioText': principioTxt, 'brandVal': brandId, 'brandText': brandTxt },
            success: function (data) {
                removeAddlink();
                $('#FilterDiv').append(data);
            }
        });
        // 
    }
</script>
	
    <script type="text/javascript">

     //   function ResetForm() {
     //       $("#<%'=hiddenaree.ClientID%>").val("");
     //       $("#<%'=hiddenareetxt.ClientID%>").val("");
     //       $("#<%'=hiddenprincipi.ClientID%>").val("");
     //       $("#<%'=hiddenprincipitxt.ClientID%>").val("");
     //       $("#<%'=hiddenbrand.ClientID%>").val("");
     //       $("#<%'=hiddenbrandtxt.ClientID%>").val("");
     //       brandId = ""
    //        brandTxt = ""
     //      principioId = ""
     //        principioTxt = ""
     //      AreaId = ""
     //       AreaTxt = ""
    //        selClick = ""
    //        AddFilter();
    //        return false;
    //    }


        //$(".itemRCP").hover(
        //    function () {
        //        $(this).css('background', '#fbfbfb');
        //        $(this).children('.cnt').children('.cntTxt').children('.pdfRCP').addClass('selPdfRCP');
        //    }, function () {
        //        $(this).css('background', '#fff');
        //        $(this).children('.cnt').children('.cntTxt').children('.pdfRCP').removeClass('selPdfRCP');
        //    }
        //);
		
		//$(".itemRCP").click(
		//	function(){
		//		document.location.href = $(this).find("a.pdfRCP").attr("href");
		//	}
		//);

		$(".selectddl").click(function (event) {
		    event.preventDefault();
		    var myddl = $(this).attr("rel");
		    $("#" + myddl).toggle();
		    if (myddl == "ulFilterAree") {
		        $('#ulFilterBrand').hide();
		        $('#ulFilterPrincipi').hide();
		    } else if (myddl == "ulFilterPrincipi") {
		        $('#ulFilterBrand').hide();
		        $('#ulFilterAree').hide();
		    } else if (myddl == "ulFilterBrand") {
		        $('#ulFilterPrincipi').hide();
		        $('#ulFilterAree').hide();
		    }
        });

      

       

        function hideDDL()
        {
           $('#ulFilterBrand').hide();
           $('#ulFilterPrincipi').hide();
           $('#ulFilterAree').hide();
        }

        
        $( "#searchLink" ).click(function() {
            var brandId = $("#hiddenbrand").val();              
              var principioId = $("#hiddenprincipi").val();             
            var AreaId = $("#hiddenaree").val();     
            
            $('.eventiContInt').html('');

              $.ajax({
                  contentType: "text/html; charset=utf-8",
                  url: '/ProjectDOMPE/_async/loadFilteredRcp.aspx',
                  dataType: "html",
                  data: { 'AreaVal': AreaId, 'principioVal': principioId, 'brandVal': brandId, 'si': <%=Me.BusinessMasterPageManager.GetSite.Id%>, 'sa': <%=Me.BusinessMasterPageManager.GetSiteArea.Id%>, 'ct': <%=Me.BusinessMasterPageManager.GetContentType.Id%> },
                  success: function (data) {
                      
                      $('.eventiContInt').append(data);
                  }
              });
              // 
        });

	</script>
	
	<script type="text/javascript">
		<%' If Not _isMobile Then %>
		//$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	   <%' End If %>
	</script>	
