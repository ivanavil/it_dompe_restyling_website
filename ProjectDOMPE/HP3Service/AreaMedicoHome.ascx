﻿<%@ Control Language="VB" ClassName="ChiSiamo" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace=" Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _sedi As String = String.Empty
    Protected _langId As Integer = 0
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _readMore As String = String.Empty
    
    Protected Sub Page_Init()
        _siteFolder = Me.ObjectSiteFolder
        _langId = Me.PageObjectGetLang.Id
        _siteKey = Me.PageObjectGetSite
        _readMore = Me.BusinessDictionaryManager.Read("DMP_readMore", _langId, "DMP_readMore")
    End Sub
    
    Protected Sub Page_Load()
        
        InitView()
        
    End Sub
    
    Protected Sub InitView()
        
    
        
        pnlIT.Visible = (_langId = Dompe.Languages.ITALIAN)
        pnlEN.Visible = (_langId = Dompe.Languages.ENGLISH)
       
               
    End Sub
    
   
    
    
    
    Protected Function GetContentLink(idContent As Integer) As String
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key.Id = Dompe.ThemeConstants.idManagement
        soTheme.KeyLanguage.Id = _langId
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentSearcher
            Dim idDompe As Integer = idContent
            so.KeySite = Me.PageObjectGetSite
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = idDompe
            Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                Return Me.BusinessMasterPageManager.GetLink(cont.Key, theme, False)
            End If
        End If
        
        Return String.Empty
        
    End Function
    
</script>

<asp:Panel runat="server" ID="pnlIT" Visible="false">
    <div class="body areaMedica">
        <div class="box-container titleSection">
        <div class="boxCnt">
            <div class="container">
                <h1>Area medico</h1>
            </div>
        </div>
    </div>
          <div class="box-container rightCnt item1">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Brand.</h3>
                       
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idBrand, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-container leftCnt item2">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Focus on.</h3>                        
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idFocuson, _langId)%>" title="<%=_readMore %>" class="viewMore viewMoreWhite"><%=_readMore %></a>
                    
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="box-container rightCnt item3">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Link utili.</h3>
                       
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idLinksUsefulMediaPress, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                    </div>
                </div>
            </div>
        </div>
           <div class="box-container leftCnt item4">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt1">
                        <h3 class="red">Archivio RCP.</h3>
                      
                         <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArchivioRCP, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                                       
                    </div>
                    
                </div>
            </div>
        </div>      
    </div>
</asp:Panel>

<asp:Panel runat="server" ID="pnlEN" Visible="false">
   <div class="body areaMedica">
        <div class="box-container titleSection">
        <div class="boxCnt">
            <div class="container">
                <h2>Medical area</h2>
            </div>
        </div>
    </div>
          <div class="box-container rightCnt item1">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Brand.</h3>
                       
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idBrand, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-container leftCnt item2">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Focus on.</h3>                        
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idFocuson, _langId)%>" title="<%=_readMore %>" class="viewMore viewMoreWhite"><%=_readMore %></a>
                     
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="box-container rightCnt item3">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt">
                        <h3>Useful link.</h3>
                       
                        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idLinksUsefulMediaPress, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                    </div>
                </div>
            </div>
        </div>
           <div class="box-container leftCnt item4">
            <div class="boxCnt">
                <div class="container">
                    <div class="cnt1">
                        <h3 class="red">RCP Archive.</h3>
                      
                         <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArchivioRCP, _langId)%>" title="<%=_readMore %>" class="viewMore"><%=_readMore %></a>
                                       
                    </div>
                    
                </div>
            </div>
        </div>      
    </div>
</asp:Panel>
