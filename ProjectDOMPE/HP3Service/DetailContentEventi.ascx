﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<script runat="server">
    Dim dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
        
    
    Sub Page_Load()
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        Dim serchcont As New ContentExtraSearcher
      
        If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
          
            contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
           
            If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                '   titolo_sezione.Text = contentextraVal.Title
                testo_title.Text = contentextraVal.Title
                imgPath = GetImage(contentextraVal.Key, contentextraVal.Title)
                testo_sezione.Text = contentextraVal.FullText
                testo_subtitle.Text = contentextraVal.Abstract
                testo_optional.Text = contentextraVal.FullTextOriginal
                LoadContentAssociati(contentextraVal.Key.PrimaryKey)
                If CDate(contentextraVal.DatePublish).ToShortDateString <> CDate(contentextraVal.DateExpiry).ToShortDateString Then
                    DateEvento.Text = dict.Read("Dmp_DateDal", _langKey.Id, "Dmp_DateDal") & ": " & CDate(contentextraVal.DatePublish).ToShortDateString & " " & dict.Read("Dmp_DateAl", _langKey.Id, "Dmp_DateAl") & ": " & CDate(contentextraVal.DateExpiry).ToShortDateString
                Else
                    DateEvento.Text = dict.Read("Dmp_DateEvento", _langKey.Id, "Dmp_DateEvento") & ": " & CDate(contentextraVal.DatePublish).ToShortDateString
                End If
                
                LuogoEvento.Text = dict.Read("Dmp_Luogo", _langKey.Id, "Dmp_Luogo") & ": <strong>" & contentextraVal.Copyright &"</strong>"
                
            End If
                
            End If
        
            If Not IsPostBack() Then
                If _siteArea.Name = Dompe.SiteAreaConstants.Management Then
                    LoadSubControl(String.Format("/{0}/HP3Common/BoxManagement.ascx", _siteFolder), phlManagement)
                End If
         
                LoadSubControl(String.Format("/{0}/HP3Common/BoxNews.ascx", _siteFolder), Phlnews)
                If Dompe.ContentBoxImage.IdContentList.Contains("{" & Me.PageObjectGetContent.Id.ToString & "}") Then
                    LoadSubControl(String.Format("/{0}/HP3Common/BoxImage.ascx", _siteFolder), PhlBoxImage)
                End If
                LoadSubControl(String.Format("/{0}/HP3Common/BoxDownload.ascx", _siteFolder), PhlDownload)
                LoadSubControl(String.Format("/{0}/HP3Common/BoxLinks.ascx", _siteFolder), PhlLinks)
            
            End If
       
                  
    End Sub
    
    Sub LoadContentAssociati(ByRef PK As Integer)
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        If PK > 0 Then
            soRel.Content.Key.PrimaryKey = PK
            soRel.Content.KeyRelationType = New RelationTypeIdentificator(4)
            soRel.Content.RelationSide = RelationTypeSide.Right
        End If
        
      
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
      
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            rpdDownloadRelated.DataSource = coll
            rpdDownloadRelated.DataBind()
        End If
    End Sub
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = idContent
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
     
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
</script>


<%--  <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>--%>
<%--<div class="presentation">
                <div class="container">
                    <div class="presentationBox">
                        <div class="presentationBox2">
                           <div class="presentationBox3">
                                   <h2><asp:Literal ID="titolo_sezione" runat="server" /></h2>                          
                           </div>
                         </div>
                     </div>
                </div>
</div>--%>
<div class="body">
  <div class="container">
       <div class="bodyBox">
        <asp:Literal ID="testo_optional" runat="server" />
          <div class="leftBody">
            <div class="content">
            <h3><asp:Literal ID="testo_title" runat="server" /></h3>
              <h4><asp:Literal ID="testo_subtitle" runat="server" /></h4>
              <p><asp:Literal ID="DateEvento" runat="server" /></p>
               <p><asp:Literal ID="LuogoEvento" runat="server" /></p>
              <asp:Literal ID="testo_sezione" runat="server" />
              </div>
       <asp:Repeater ID="rpdDownloadRelated" runat="server">
       <HeaderTemplate></HeaderTemplate>
       <ItemTemplate>
        <a href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>" onclick="recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>','Download Dettaglio del contenuto','Download','<%# CType(Container.DataItem, ContentValue).Title%>','<%# CType(Container.DataItem, ContentValue).Key.PrimaryKey %>');return false;">
         <span class="title"><%# CType(Container.DataItem, ContentValue).Title%></span>
                <span><%# IIf(CType(Container.DataItem, ContentValue).Launch.Length > 120, Left(CType(Container.DataItem, ContentValue).Launch, 120) & "...", CType(Container.DataItem, ContentValue).Launch)%> </span>
 <%--                 
                <span> <%# getSizeFile(CType(Container.DataItem, DownloadValue).DownloadTypeCollection(0).Weight)%></span>--%>
               <span class="readMore"><%= Me.BusinessDictionaryManager.Read("Download", _langKey.Id, "Download")%></span>
       </ItemTemplate>
       <FooterTemplate></FooterTemplate>
       </asp:Repeater>
          </div>
        <div class="rightBody">
              <asp:PlaceHolder ID="phlManagement" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
            <div class="rightBodyRow">
              <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            </div>
        </div>
        </div>
   </div>
</div>
          


