﻿<%@ Control Language="VB" ClassName="detailContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
    Protected _brands As ContentExtraCollection = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _text As String = String.Empty
    Protected _mainText As String = String.Empty
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _contColor As String = String.Empty
	Private _isMobile as boolean = false
	
	Dim _finalBrandDetailText As String = String.Empty
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.PageObjectGetLang
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _themeKey = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
        _siteKey = Me.PageObjectGetSite
      End Sub

	Public Function IsMobileDevice() As Boolean
		Dim userAgent = Request.UserAgent.ToString().ToLower()
		If Not String.IsNullOrEmpty(userAgent) Then
			if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
			userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
				Return true
			End If
		End if
		Return False
	End Function	

	Dim _canCheckApproval as Boolean = false	
    
    Protected Sub Page_Load()
		_isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
		'_canCheckApproval = True
		
        InitView()
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub InitView()
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        Dim so As New ThemeSearcher()
        so.LoadHasSon = False
        so.LoadRoles = False
        so.KeyLanguage = _langKey
        so.Key = _themeKey
        _currTheme = Helper.GetTheme(so)
        
        If Not _currTheme Is Nothing Then
            titolo_sezione.Text = _currTheme.Description
        End If

        Dim pathPressMenu As String = String.Format("/{0}/HP3Common/MenuAreaMedico.ascx", _siteFolder)
        LoadSubControl(pathPressMenu, PHL_Med_Menu)
        Dim pathRichiediArticolo As String = String.Format("/{0}/HP3Common/BoxRichiediArticolo.ascx", _siteFolder)
        LoadSubControl(pathRichiediArticolo, phRichiediArticolo)
        
		'Legge il singolo brand dato dalla somma del meccanismo di azione piu gli RCP
        If contentId > 0 Then
			 _cacheKey = "_cacheAreaMedicoDettaglioBrand:" & _langKey.id & "|" & _themeKey.Id & "|" & _siteArea.Id & "|" & Me.PageObjectGetContent.Id
			If Request("cache") = "false" Or _canCheckApproval Then
				CacheManager.RemoveByKey(_cacheKey)
			End If
			Dim _strCachedContent As String = ""' CacheManager.Read(_cacheKey, 1)
			If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
				_finalBrandDetailText = _strCachedContent
			Else
				Dim soEx As New ContentExtraSearcher()
				soEx.key.Id = contentId
				soEx.key.IdLanguage = 1'_langId
				soEx.CalculatePagerTotalCount = False
				soEx.SetMaxRow = 1
				soEx.KeySite = Me.PageObjectGetSite
				If Not _canCheckApproval Then soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
				soEx.Properties.Add("Title")
				'soEx.Properties.Add("Launch")
				'soEx.Properties.Add("FullText")
				soEx.Properties.Add("ApprovalStatus")
				soEx.Properties.Add("BookReferer")
				
				Dim man As New ContentExtraManager()
				Dim prodColl = man.Read(soEx)
				If Not prodColl Is Nothing AndAlso prodColl.Any() Then
					Dim prod = prodColl.FirstOrDefault()
					If Not prod Is Nothing Then
						titolo_sezione.Text =prod.Title 
						Dim styleElements = prod.BookReferer.Split("|")
						If Not styleElements Is Nothing AndAlso styleElements.Any() Then
							_contColor = styleElements.FirstOrDefault()
						End If
						
						'Legge il meccanismo di azione
						Dim sbMecAz As New StringBuilder()
						Dim collMecAz = man.ReadContentExtraRelation(New ContentIdentificator(contentId, 1), New ContentRelationTypeIdentificator(Dompe.RelationType.MeccanismoDazione))
						If Not collMecAz Is Nothing AndAlso collMecAz.Any() Then
							sbMecAz.AppendFormat("<style type='text/css'>.infoAcc h4{{ color:{0}; font-size:16px; }}</style>", _contColor)
							sbMecAz.Append("<div class='singleAcc' val='o'>")
							sbMecAz.AppendFormat("<div class='bgAccBrand' style='background-color:{0}'>", _contColor)
							sbMecAz.Append("<div id='imgs' class='openAcc' style=""top:32px;"">close</div>")
                            sbMecAz.AppendFormat("<h3>{0}</h3>", Healthware.Dompe.Helper.Helper.checkApproval(prod.ApprovalStatus) & collMecAz(0).Launch & Healthware.Dompe.Helper.Helper.checkApproval(prod.ApprovalStatus))
							sbMecAz.Append("</div>")
							sbMecAz.AppendFormat("<div class='infoAcc'>{0}</div>", collMecAz(0).FullText)
							sbMecAz.Append("</div>")
							_mainText = sbMecAz.ToString
						End If
						
						'Dim sb As New StringBuilder()
						'If Not String.IsNullorEmpty(prod.FullText) Then
						'	sb.AppendFormat("<style type='text/css'>.infoAcc h4{{ color:{0}; font-size:16px; }}</style>", _contColor)
						'	sb.Append("<div class='singleAcc' val='o'>")
						'	sb.AppendFormat("<div class='bgAccBrand' style='background-color:{0}'>", _contColor)
						'	sb.Append("<div id='imgs' class='openAcc'>close</div>")
                        '   sb.AppendFormat("<h3>{0}</h3>", Healthware.Dompe.Helper.Helper.checkApproval(prod.ApprovalStatus) & prod.Launch & Healthware.Dompe.Helper.Helper.checkApproval(prod.ApprovalStatus))
						'	sb.Append("</div>")
						'	sb.AppendFormat("<div class='infoAcc'>{0}</div>", prod.FullText)
						'	sb.Append("</div>")
						'	_mainText = sb.ToString						
						'End if
						pnlDetail.Visible = True
					End If
				End If

				'Legge gli RCP associati al singolo Brand
				Dim RcpRelType As Integer = 10
				Dim coll = man.ReadContentExtraRelation(New ContentIdentificator(contentId, 1), New ContentRelationTypeIdentificator(RcpRelType))
				If Not coll Is Nothing AndAlso coll.Any() Then
					coll.Sort(2,1)
					_text = GetFullBrandContent(coll)
					div_brands.Visible = False
					pnlDetail.Visible = True
				End If
				_finalBrandDetailText = _mainText & _text
			End If
        
        Else
			'Lista dei Brands
            Dim cntSrc As New ContentExtraSearcher
            Dim cntsearch As New ContentExtraSearcher()
            cntsearch.key.IdLanguage = 1'_langId
            cntsearch.KeySiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
            cntsearch.KeyContentType.Id = Me.BusinessMasterPageManager.GetContentType.Id
            cntsearch.CalculatePagerTotalCount = False
            cntsearch.SortType = ContentGenericComparer.SortType.ByData
            cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
            If Not _canCheckApproval Then cntsearch.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved

            _brands = Me.BusinessContentExtraManager.Read(cntsearch)
            div_brands.Visible = True
        End If
    End Sub
	
    'Function checkApproval(ByVal approvalStatus as integer) as String
    '	If approvalStatus > 1 Then Return "<span style='color:red;font-weight:bold;'> - NON APPROVATO</span>"
    '	Return String.Empty
    'End Function
    
    Protected Function GetFullBrandContent(ByVal coll As ContentExtraCollection) As String
        Dim sb As New StringBuilder()
        Dim toggleClass As String = String.Empty
        Dim toggleParentClass As String = String.Empty
        
        For i As Integer = 0 To coll.Count - 1
            Dim item = coll(i)
            sb.Append("<div val='c' class='singleAcc'>")
            sb.AppendFormat("<div class='bgAccBrand' style='background-color:{0}'><div class='closeAcc' id='imgs' style=""top:32px !important;"">close</div><h3 style=""font-size:25px !important;padding:25px 2px 0 95px !important;"">{1}</h3></div>", _contColor, item.Title & Healthware.Dompe.Helper.Helper.checkApproval(item.ApprovalStatus))
            sb.AppendFormat("<div class='infoAcc'><p style='text-align:center;'><img src='/HP3Image/cover/{0}'/></p>{1}{4}{2}{3}</div>",  Healthware.Dompe.Helper.Helper.GetCover(item.Key, 0, 0), item.Abstract, item.FullText, getProductDocument(item), getProductMecAz(item))
            sb.Append("</div>")
        Next
        Return sb.ToString() 
    End Function
	
	Function getProductMecAz(ByVal cnt As ContentExtraValue) As String
		Dim man As New ContentExtraManager()
        Dim so As New ContentExtraSearcher()
		Dim collMecAz = man.ReadContentExtraRelation(New ContentIdentificator(cnt.Key.Primarykey), New ContentRelationTypeIdentificator(Dompe.RelationType.MeccanismoDazione))
		If Not collMecAz Is Nothing AndAlso collMecAz.Any() Then Return String.Format("<h4>{0}</h4><p>{1}</p>", collMecAz(0).Launch, collMecAz(0).FullText)

		Return String.Empty
    End Function
	
    Function getProductDocument(ByVal cnt As ContentExtraValue) As String
        Dim so As New DownloadSearcher()
        so.key.Id = cnt.Key.Id
        so.key.IdLanguage = 1'Me.PageObjectGetLang.Id
		If Not _canCheckApproval Then so.ApprovalStatus = 1
		
        Dim collMaterial As DownloadCollection = Me.BusinessContentManager.ReadDownloadRelation(so)
        If Not collMaterial Is Nothing And collMaterial.Count() > 0 Then
			Dim _strReturn as New StringBuilder()
		
			For Each oDownVal as DownloadValue in collMaterial
				_strReturn.Append("<li style='list-style-type:none;'><a title='Download " & oDownVal.Title & "' href='/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(oDownVal.DownloadTypeCollection(0).KeyContent.PrimaryKey) & "' style=""font-size: 20px;font-weight: bold;"">" & oDownVal.Title & Healthware.Dompe.Helper.Helper.checkApproval(cnt.ApprovalStatus) & "</a></li>")
				'For Each element as DownloadTypeValue in oDownVal.DownloadTypeCollection 'collMaterial(0).DownloadTypeCollection
				'	Return 
				'Next
			Next
			
			Return "<ul style='margin:0;padding:0;'>" & _strReturn.ToString() & "</ul><p>&nbsp;</p>"
        Else
            Return String.Empty
        End If
		Return String.Empty
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
</script>


<section>
    <div class="cont-img-top">
        <div class="bg-top brandSection">
        </div><!--end brandSection-->     	    
                <div class="txt">
                    <h1><asp:Literal ID="titolo_sezione" runat="server" /></h1>
                </div>
            <div class="layer">&nbsp;</div>         
     </div>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>

<section>
	<div class="container">
        <div class="colLeft">        	
            	<div class="listSide">
                   <asp:PlaceHolder ID="PHL_Med_Menu" runat="server" Visible="true" />
                </div>                
        </div><!--end colLeft-->


         <div class="colRight">	
             <div class="content">
             <asp:Panel runat="server" ID="pnlDetail" Visible="false">
			 <style>
				.infoAcc h4 {margin:0 !important;padding:0 !important;}
				.singleAcc p {padding:0 0 10px 0 !important;}
			</style>
				 <%=_finalBrandDetailText%>
            </asp:Panel>
			 
             <asp:PlaceHolder runat="server" id="div_brands">
             <div class="contMedBar">
             <%
                 If Not _brands Is Nothing AndAlso _brands.Any() Then
                     Dim brandColor As String = String.Empty
                     Dim brandClass As String = String.Empty
                     Dim arrowImage As String = String.Empty
                 
                     For Each item In _brands
                         brandClass=String.Empty
                         arrowImage = "goEv_2.png"
                         
                         Dim styleElements = item.BookReferer.Split("|")
                         If Not styleElements Is Nothing AndAlso styleElements.Any() Then
                             brandClass = styleElements.LastOrDefault()
                             'If brandClass.Equals("arialBar") orelse brandClass.Equals("sostenilBar") then arrowImage="viewMore.png"
							 If brandClass.Equals("whitebg") then arrowImage="viewMore.png"
                         End If
                         %>
						<div class="<%=String.Format("{0} barMed", brandClass)%>">
						<a style="background-image:url('/HP3Image/cover/<%=item.Key.Id%>_gen.gif');" title="<%= Server.HtmlEncode(item.Title)%>" href="<%=Me.BusinessMasterPageManager.GetLink(item.Key, _currTheme, true) %>"><%= item.Title%> <%=Healthware.Dompe.Helper.Helper.checkApproval(item.ApprovalStatus) %><img src="<%=String.Format("/{0}/_slice/{1}", _siteFolder, arrowImage)%>" class="imgGo displayNone" /></a>
						</div>
                 <%
                    Next
                 end if
                %>
              </div>                 			
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phRichiediArticolo" runat="server" Visible="true" />
             </div>
        </div><!--end colRight-->

        

    </div><!--end container-->
</section>	
	
    <script type="text/javascript">

        $(document).ready(function () {

            $(".barMed").mouseover(function () {
                $(this).find(".imgGo").removeClass('displayNone');

            }).mouseout(function () {
                $(this).find(".imgGo").addClass('displayNone');
            });


            $(".singleAcc").each(function (index) {
                var valore = $(this).attr("val");
                if (valore == "c" || valore == undefined) {
                    $('.infoAcc', this).hide();
                }
            });
        });			
	

        $(".bgAccBrand").click(function () {
            var valore = $(this).parent().attr("val");
            if (valore == "c" || valore == undefined) {
                $(this).parent().attr("val", "o");
                $(this).parent().find(".infoAcc").css("display", "block");
                $(this).parent().find("#imgs").removeClass("closeAcc").addClass("openAcc");

            } else {
                $(this).parent().attr("val", "c");
                $(this).parent().find(".infoAcc").css("display", "none");
                $(this).parent().find("#imgs").removeClass("openAcc").addClass("closeAcc");
            }


        });
	</script>
	<script type="text/javascript">
		<%' If Not _isMobile Then %>
		//$(document).scroll(function () {
	    //    var hDoc = $(document).height();
	    //    var colLeftH = $('.colLeft').height();
	    //    var ctrlEnd = hDoc - 110;
	    //    var hWin = $(window).scrollTop() + colLeftH;

	    //    if (hWin >= ctrlEnd) {
	    //        $('.colLeft').css("position", "fixed");
	    //        $('.colLeft').css("top", "auto");
	    //        $('.colLeft').css("bottom", "110px");
        //    }else{
	    //        if ($(window).scrollTop() > 431) {
	    //            $('.colLeft').css("position", "fixed");
	    //            $('.colLeft').css("top", "115px");
	    //            $('.colLeft').css("bottom", "auto");
        //        } else {
	    //            $('.colLeft').css("position", "absolute");
	    //            $('.colLeft').css("top", "546px");
	    //            $('.colLeft').css("bottom", "auto");
        //        }
	    //    }
	    //});
	   <%' End If %>
	</script>	