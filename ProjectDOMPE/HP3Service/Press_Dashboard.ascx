﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>



<script runat="server">
    
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager()
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteKey As SiteIdentificator=Nothing
    Protected _text As String = String.Empty
    Protected _title As String = String.Empty
    Protected _titleClass As String = String.Empty
    Protected _titleStyle As String = String.Empty
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        _siteKey = Me.PageObjectGetSite
        
    End Sub
        
    
    Protected Sub Page_Load()
      
        InitView()
                  
    End Sub
    
    Protected Sub InitView()
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key = _currentTheme
        soTheme.KeyLanguage.Id = _langKey.Id
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentExtraSearcher
            so.KeySite = _siteKey
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = theme.KeyContent.Id
            so.CalculatePagerTotalCount = False
            so.SetMaxRow = 1
            Dim coll = Me.BusinessContentExtraManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                
                If Not cont Is Nothing Then
                    
                    _text = cont.FullText
                    _title = cont.TitleContentExtra
                    _titleClass = cont.Code
                    _titleStyle = cont.TitleOriginal
                    Dim replacing = GetPlaceHolders()
                    If Not replacing Is Nothing AndAlso replacing.Any() Then
                        For Each item As KeyValuePair(Of String, String) In replacing
                            _text = _text.Replace(item.Key, item.Value)
                        Next
                    End If
                    
                End If
            End If
        End If
               
    End Sub
    
    Protected Function GetPlaceHolders() As Dictionary(Of String, String)
        
        Dim replacing As New Dictionary(Of String, String)
        replacing.Add("[TITLECOMUNICATISTAMPA]", Dict.Read("thm_" & Dompe.ThemeConstants.idComunicatiStampaMediaPress, _langKey.Id))
        replacing.Add("[COMUNICATISTAMPA]", GetLink(Dompe.ThemeConstants.idComunicatiStampaMediaPress))
        replacing.Add("[TITLESCHEDEISTITUZIONALI]", Dict.Read("thm_" & Dompe.ThemeConstants.idSchedeIstituzionaliMediaPress, _langKey.Id))
        replacing.Add("[SCHEDEISTITUZIONALI]", GetLink(Dompe.ThemeConstants.idSchedeIstituzionaliMediaPress))
        
        replacing.Add("[TITLEPHOTOGALLERY]", Dict.Read("thm_" & Dompe.ThemeConstants.idPhotoGalleryMediaPress, _langKey.Id))
        replacing.Add("[PHOTOGALLERY]", GetLink(Dompe.ThemeConstants.idPhotoGalleryMediaPress))
        replacing.Add("[TITLECONTATTI]", Dict.Read("thm_" & Dompe.ThemeConstants.idPressContatti, _langKey.Id))
        replacing.Add("[CONTATTI]", GetLink(Dompe.ThemeConstants.idPressContatti))
        
        Return replacing
        
    End Function
    
    Function GetLink(ByVal id As Integer) As String
        
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, id, _langKey.Id)
       
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
</script>

<%--<div class="body press">
    <div class="box-container titleSection">
        <div class="boxCnt">
            <div class="container">
                <h2 class="<%=_titleClass %>" <%=_titleStyle%>><%=_title %></h2>
            </div>
        </div>
    </div>
    <%=_text %>    
</div>--%>

<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/press.jpg');">
            
        </div>    
         <div class="txt">
                <h1 class="<%=_titleClass %>" <%=_titleStyle%>><%=_title %></h1>          
            </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <div class="breadcrumbs">
        <ul>
        	<li><a href="#" title="Home">Home</a><span><img src="/ProjectDOMPE/_slice/arr-bc.jpg" /></span></li>
        	<li><a href="#" title="<%=_title %>"><%=_title %></a></li>
        </ul>
    </div><!--end breadcrumbs--> 
</section>
<section>
    <div class="container"><%= _text%></div>
</section>