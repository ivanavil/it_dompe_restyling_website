﻿<%@ Control Language="VB" ClassName="Literature" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.DMP.BL" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<script runat="server">
    'TODO: Generalizzare gli errori a livello di dictionary
    ' Aggiungere anche Approval
    
    Dim operazione As Integer
    Protected _siteFolder As String = String.Empty
    Protected _emptyLiteratureArchiveTemplate As String = String.Empty
    Protected _ajaxLiteraturePage As String = String.Empty
    Protected _ajaxTagsPage As String = String.Empty
    Protected _siteArea As Integer = 0
    Protected _brand As Integer = 0
    Protected _bitMask As String = String.Empty
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _userKey As UserIdentificator = Nothing
    Protected _topKeys As String = String.Empty
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _geo As Integer = 0
    Dim _MaxItem As Integer
    Protected strSezione As String = ""
    Protected _notAvaible As String = ""
    Dim classSection As String = ""
	Private _isMobile as boolean = false
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _MaxItem = 10
        _siteFolder = Me.ObjectSiteFolder
        '  _bitMask = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BitMaskAree 'dm Global Variable
        '  _brand = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).BrandCurrent
        _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
        _ajaxLiteraturePage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _ajaxTagsPage = String.Format("/{0}/HP3Handler/Tags.ashx", _siteFolder)
        _siteArea = Me.PageObjectGetSiteArea.Id
        _keyContentType = Me.PageObjectGetContentType
        _userKey = Me.ObjectTicket.User.Key
        _langKey = Me.PageObjectGetLang
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        _contentKey = Me.PageObjectGetContent
    End Sub
	
	Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
             if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
                 userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
                Return true
            End If
        End if
            
        Return False
    End Function	
    
    Dim _CheckApproval As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
        operazione = CInt(Keys.EnumeratorKeys.LiteratureViewType.List)
        If _siteArea = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress Then operazione = CInt(Keys.EnumeratorKeys.LiteratureViewType.ViewSliderPhotoGallery)
        
        PopulateYear()
        
        Dim idContentTheme As Integer = 0
        Dim idContent As Integer = Me.PageObjectGetContent.Id
        If idContent > 0 Then
            'Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            'idContentTheme = valthem.KeyContent.Id
            '_topKeys = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _langKey.Id)
            'Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent = _topKeys
            'If idContentTheme <> idContent Then
            '    InitDetailView()
            'Else
            '    InitView()
            'End If
            Dim path As String = String.Format("/{0}/HP3Service/DetailArchiveContent.ascx", _siteFolder)
            LoadSubControl(path, plhDetail)
            plhDetail.Visible = True
            plhArchive.Visible = False
            
        Else
            plhArchive.Visible = True
        
        
            'If _siteArea = Dompe.SiteAreaConstants.IDSiteareaNewsMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDRassegnaStampaAreaMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDSchedeIstituzionaliMediaPress Then
            '    _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
            '    strSezione = "PressArchive"
            'Else
            
            If _siteArea = Dompe.SiteAreaConstants.IDComunicatiStampaAreaMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDSiteareaNewsMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDRassegnaStampaAreaMediaPress Or _siteArea = Dompe.SiteAreaConstants.IDSchedeIstituzionaliMediaPress Then
                _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressComunicati.htm", _siteFolder)
                strSezione = "PressComunicati"
            ElseIf _siteArea = Dompe.SiteAreaConstants.IDSiteareaLinksMediaPress Then
                _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressLink.htm", _siteFolder)
                strSezione = "PressLink"
            ElseIf _siteArea = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress Then
                _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressPhotoGallery.htm", _siteFolder)
                strSezione = "PressPhotoGallery"
                _MaxItem = 8
            Else
                _emptyLiteratureArchiveTemplate = String.Format("/{0}/Templates/PressArchive.htm", _siteFolder)
                strSezione = "PressArchive"
            End If
        
            Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
            '  LoadSubControl(pathPressMenu, plhMenu)
        
            _notAvaible = Me.BusinessDictionaryManager.Read("DMP_notavaible", _langKey.Id, "DMP_notavaible")
        
			Dim _langId As Integer = Me.BusinessMasterPageManager.GetLanguage.Id
		
            Dim so As New ThemeSearcher
            Dim coll As ThemeCollection = Nothing
            so.KeySite = Me.PageObjectGetSite
            so.KeyFather.Id = Dompe.ThemeConstants.idAreaPress
            so.KeyLanguage.Id = IIf(_langId >= 2, 2, _langId) 
            coll = Me.BusinessThemeManager.Read(so)
           
            rptSiteMenu.DataSource = coll
            rptSiteMenu.DataBind()
        End If
        
        Select Case _themeKey.Id
            Case Dompe.ThemeConstants.idComunicatiStampaMediaPress
                classSection = "comunicatiSection"
            Case Dompe.ThemeConstants.idSchedeIstituzionaliMediaPress
                classSection = "comunicatiSection pressSchedeSection"
            Case Dompe.ThemeConstants.idRassegnaStampaMediaPress
                classSection = "comunicatiSection pressRassegnaStampaSection"
            Case Else
                classSection = "comunicatiSection"
        End Select
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub InitDetailView()
       
        Dim path As String = String.Format("/{0}/HP3Service/Press_DetailContent.ascx", _siteFolder)
        'LoadSubControl(path, plhDetail)
        ' plhArchive.Visible = False
       ' plhDetail.Visible = True
              
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub

    Protected Sub InitView()
        
        ' _topKeys = GetTopItemsKeys()
        
        ' plhDetail.Visible = False
       ' plhArchive.Visible = True
        
          
        '  InitAreaFilters()
    End Sub
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, vo.Key.Id, _langKey.Id)
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
  
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
  
    Protected yearlist As String = String.Empty
    Sub PopulateYear()
        Dim sb As New StringBuilder()
        Dim i As Integer = DateTime.Now.Year
        sb.AppendFormat("<li><a class='aPosizione' data-pos='{0}' href='#' title='{1}'>{2}</a></li>","", Me.BusinessDictionaryManager.Read("DMP_SelAnno", Me.PageObjectGetLang.Id, "DMP_SelAnno"), Me.BusinessDictionaryManager.Read("DMP_SelAnno", Me.PageObjectGetLang.Id, "DMP_SelAnno"))
        While i >= 2012
            sb.AppendFormat("<li><a class='aPosizione' data-pos='{0}' href='#' title='{1}'>{2}</a></li>", i, i, i)
            i -= 1
        End While
        yearlist = sb.ToString()
    End Sub
    Dim sb As New StringBuilder()
</script>

<asp:PlaceHolder ID="plhArchive" runat="server">
<script language="javascript" type="text/javascript">
    var templateUrl = "<%=_emptyLiteratureArchiveTemplate%>";
    var ajaxLiteraturePage = "<%=_ajaxLiteraturePage%>";
    var nPage = 1;
    var _loadingLibrary = false; 
    var _pageSize = <%=_MaxItem %>;
    var _defaultBitmask = '<%=_bitMask%>';
    var _currentBitmask = _defaultBitmask;
    var _defaultKeysToExclude = "<%=_topKeys%>";
    var _loadingStatus = false;
   
   $(document).ready(function () {
        FilterApply(0);

        $('#moreInfo').on('click', function () {            
            ++nPage;
            var keysToExclude = _defaultKeysToExclude;
            var tags = GetTags();            
            var contentSearcher = prepareSearcherLiterature(tags, _currentBitmask, keysToExclude);
            Literature.read(contentSearcher);
            return false;
        });  
      });

   var yearsList=$('.yearHidden');

  function FilterApply(type){
    nPage = 1;
    _defaultKeysToExclude = "<%=_topKeys%>";
    //alert("--- key " + _defaultKeysToExclude)
    $(".notavaible").remove();
    $(".archiveCnt").remove();
    
    if (type == 1){
        var container = $("#literatureArchive");
        container.html('');
    }

    var so = prepareSearcherLiterature('', _defaultBitmask, _defaultKeysToExclude);
    LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
    InitAutoCompleater();
    InitSearch();

   }

    function yearsOpen(){
		if(yearsList==0){
			$('.yearsList').css('display','block');
			yearsList=1;
		}else{
			//$('.yearsList').css('display','none');
			yearsList=0;
		}
	}
	
    function selYear(){
		var id= $(".yearsList").val();
		//$('.yearsList').css('display','none');
		yearsList=0;
		$('.yearSearch').html(id);
		$('.yearHidden').val(id);
	}


    function GetTags()
    {
        var tags = '';             
        $("ul#tagsView li").each(function(){
            tags += $(this).attr('data-tag') + ',' || '';
        });

        return tags;
    }

    function InitSearch(){
        $('.filterBtn #btnTagSearch').click(function(){
            DoSearch();
        });
    }

    function DoSearch()
    {
        _loadingStatus=false;
        nPage=1;
        $("#literatureArchive").empty();            
        var tags = GetTags();                                    
        var so = prepareSearcherLiterature(tags, _currentBitmask, '')
        LoadTemplate(templateUrl, LoadLiteratureArchive, so, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
    }

    function InitAutoCompleater(){      

        var searchField = $('.bgFilter #tagSearch');
        searchField.val('');
        searchField.keyup(function (e) {                    
           ReadTags();        
        });      

        searchField.focusout(function(e){
            searchField.val('');    
        });
        
    }

    function ReadTags(){
        
        var postData = {
                    op: '<%=Keys.EnumeratorKeys.TagOperationType.List %>',
                    search: $('.bgFilter #tagSearch').val(),
                    limit: '50'  
                };

        $('.bgFilter #tagSearch').autocomplete({
            minLength:2,
            focus: function(event, ui){                
                return false; 
            },
            select: function(event, ui){                           

                $("#tagsView").append("<li data-tag=" + ui.item.value + ">" + ui.item.label + "<a id='removeTag' title='Delete'>X</a></li>");
                $("ul#tagsView li[data-tag="+ ui.item.value +"] a").click(function(){
                    $(this).closest('li[data-tag='+ ui.item.value +']').remove();
                    DoSearch();
                });              
               
               $("ul#tagsView li[data-tag="+ ui.item.value +"]").hover(
                function(){
                    $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").removeClass("hidden");
                },
                function(){
                    $("ul#tagsView li[data-tag="+ ui.item.value +"] a#removeTag").addClass("hidden");
                }
               );

                DoSearch();

                $("#tagSearch").val('');
                return false;            
                    
            },
            source: function(request, response){

             $.ajax({
                type: "POST",
                url: '<%=_ajaxTagsPage %>',            
                data: postData,
                success: function( data ) {
                    if (data){
                        if (data.Success) {        
                            if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %>')
                            {
                                response( $.map( data.Result, function( item ) {
                                    return {
                                        label: item.TagName,
                                        value: item.TagKey

                                    }
                                }));                            
                            } else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %>')
                            {
                                response('','');
                            }
                        }
                        else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                            document.location.reload();
                        }
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //alert(textStatus);
                }
     
            });
        }
        });
    }

   
    

   

    var ReadLiteratureCallback = function (data) {  
        
        if(data){
            if (data.Success) {        
                if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.FillItem %> )
                {     
                    _loadingStatus = true;

                var container = $("#literatureArchive");
               
                //se è sttaa vviata una ricerca da FilterApply()
                //container.html('');

                    $.tmpl("ArchiveTemplate", data.Result.Collection).appendTo(container);
                
                    if(data.Result.PageNumber == data.Result.PageTotalNumber)
                    {                          
                       $('#moreInfo').hide();
                       $('#moreInfoBar').hide();
                       }
                       
                    else
                    {                         
                       $('#moreInfo').show();
                       $('#moreInfoBar').show();
                       }
                    

                } else if (data.StatusCode == <%=Keys.EnumeratorKeys.AsyncStatusCode.NoItem %> )
                {
                    $('#moreInfo').hide();
                    $('#moreInfoBar').hide();
                   // if(!_loadingStatus)
                        $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
                }
            }
            else if (data.StatusCode == '<%=Keys.EnumeratorKeys.AsyncStatusCode.SessionTimeout %>'){                                                  
                document.location.reload();
            }
        }
        else
            {
                $("#literatureArchive").html("<p class='notavaible'><%= _notAvaible %></p>");
            }      
       
    };

    var OnReadLiteratureArchiveCompleteCallback = function () {
             
        $('#LiteratureArchiveLoader').hide();
        _loadingLibrary=false;
        $("#literatureArchive").fadeIn('fast');        

    };


    function prepareSearcherLiterature(TagsP, bitmask, keysToExclude) {
        //var year = $(".yearSearch").html();
        var year =$('#hiddenyear').val();
        var word = $("#tagSearchArchivio").val();
        var searcher = {          
            SiteareId: '<%=_siteArea%>',
            ContenTypeId: '<%=_keyContentType.Id%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=_themeKey.Id %>',
            Brand: '',
            BitMask: bitmask,
            Display: true,
            Active: true,
            Delete: false,
            ApprovalStatus:'<%=_CheckApproval%>',
            Year: year,
            Word: word,
            Tags:'',
            PageNumber:nPage,
            PageSize:_pageSize,
            KeysToExclude: keysToExclude
        };

        recordOutboundFull("Archivio <%= strSezione %>","Caricamento","page: "+ nPage)
        if(TagsP != '')
             searcher.Tags =  TagsP;


//        if(CountriesP != '')
//            searcher.Countries =  CountriesP;
                    
        return searcher;

    };

    function LoadLiteratureArchive(contentSearcher, callback, onCompleteCallback)   //old LoadProfile
    {
        
        $('#LiteratureArchiveLoader').show();
             $('#moreInfo').hide();
           $('#moreInfoBar').hide();     
        var postData = {
            op: '<%=Cint(operazione)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: ajaxLiteraturePage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {
                if (typeof callback === "function") {
                callback(data);
                }
                   $(".archiveCnt").hover(
			function () {
				$(this).children('.effectHover').css('display','none');
			},
			function () {
				$(this).children('.effectHover').css('display','block');
			}
		);
       },
            error: function (data) {
               //alert("error")
               //$("#literatureArchive").fadeIn('fast');
            }
        }).complete(function () {            
            if(onCompleteCallback && typeof(onCompleteCallback) == 'function') onCompleteCallback();
            //if (typeof onCompleteCallback === "function") {
           // onCompleteCallback();
           // }
        }); 
    }


    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {

            $.template("ArchiveTemplate", template);

            //se non è LoaderFunction nothing
            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    var Literature = Literature || new function (){
           

        this.read = function (contentSearcher) {
            if (true == _loadingLibrary) return;

            _loadingLibrary = true;

            $('#LiteratureArchiveLoader').show();
            $('#moreInfo').hide();
            $('#moreInfoBar').hide();
            //var container = jQuery('#literatureArchive');  
            
            LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback);
        };
    }

    function FilterArea(item){
        
        _loadingStatus=false;
        $('.contentHome .filter .aree a[data-mask='+ item +']').toggleClass('sel');
        nPage=1;

         var bitmask = GetBitmask();
         _currentBitmask = bitmask;
        
        var tags = GetTags();  

        var keysToExclude = '';
        var contentSearcher = prepareSearcherLiterature(tags, bitmask, keysToExclude);
        $("#literatureArchive").empty();
        LoadTemplate(templateUrl, LoadLiteratureArchive, contentSearcher, ReadLiteratureCallback, OnReadLiteratureArchiveCompleteCallback)
     }

    function GetBitmask()
    {
       var mask = '';
       $('.contentHome .filter .aree a.sel').each(function(){
            mask += $(this).attr('data-mask') + ',' || '';
       });

       if(mask == '' ) {
           mask = _defaultBitmask;
        }

        return mask;
    }
        
          function SdwbOK(urlLink,wdt,hig) 
    {
          
        //recordOutboundLink(urlLink, "Press Gallery", "Open", "Press articoli")
         Shadowbox.open({
         content:    urlLink,
        player:     "iframe",      
        height: hig,
        width:  wdt
        });


        }
</script>

    <section>
        <div class="cont-img-top">
    	<div class="bg-top <%=classSection%>">
            
        </div>    
            <div class="txt">
                <h1 class="grey"><%=Me.BusinessDictionaryManager.Read(Me.PageObjectGetTheme.Name, Me.PageObjectGetLang.Id)%></h1>         
            </div>
            <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <%--<div class="<%=classSection%>">
    	<div class="boxCntGeneric">
            <div class="container">
                <h2 class="grey"><%=Me.BusinessDictionaryManager.Read(Me.PageObjectGetTheme.Name, Me.PageObjectGetLang.Id)%></h2>
            </div>
        </div>
        </div><!--end comunicatiSection-->--%>
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
    </section>

<section>
 	
    <div class="container">
        <div class="colLeft">
        	
            	<div class="listSide">
                	
                 <asp:Repeater ID="rptSiteMenu" runat="server">
                    <HeaderTemplate><ul></HeaderTemplate> 
                    <ItemTemplate>
                        <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# Me.BusinessDictionaryManager.Read("thm_" & DataBinder.Eval(Container.DataItem, "key.id"), _langKey.Id)%></a></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>  
                    
                </div><!--end listSide-->
            
        </div><!--end colLeft-->
        <div class="colRight" style="padding-top:0px;">
        	<div class="barTool">


            	<div class="inpRound">
                	<input type="text"  id="tagSearchArchivio" />
                </div>
                <div class="styled-select">
                    <div class="yearSearch" onclick="javascript:yearsOpen();"></div>
					

                    <a href='javascript:void(0);' title='<%=Me.BusinessDictionaryManager.Read("DMP_SelAnno",Me.PageObjectGetLang.Id,"DMP_SelAnno") %>' class='selDrop' id='selPosizione'><%=Me.BusinessDictionaryManager.Read("DMP_SelAnno",Me.PageObjectGetLang.Id,"DMP_SelAnno") %></a>
                    <ul id='ulPosizione'>
                        <%=yearlist %>
                        <%--<li><a class='aPosizione' data-pos='' href='#' title='<%=Me.BusinessDictionaryManager.Read("DMP_SelAnno",Me.PageObjectGetLang.Id,"DMP_SelAnno") %>'><%=Me.BusinessDictionaryManager.Read("DMP_SelAnno",Me.PageObjectGetLang.Id,"DMP_SelAnno") %></a></li>
                        <li><a class='aPosizione' data-pos='2014' href='#' title='2014'>2014</a></li>
                        <li><a class='aPosizione' data-pos='2013' href='#' title='2013'>2013</a></li>
                        <li><a class='aPosizione' data-pos='2012' href='#' title='2012'>2012</a></li>--%>
                    </ul>
                    <input type="hidden" id="hiddenyear" />



					<select class="yearsList" onChange="javascript:selYear();"></select>
                </div>
                <div class="vaiMini">
                	
                    <input id="hdnyear" class="yearHidden" type="hidden" value="0" />
                   <a id="A1" href="javascript:void(0);" onclick="FilterApply(1)" runat="server" ><%= Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch")%></a>

                </div>
            </div><!--end barTool-->
            <div class="eventiContInt">             	
                 <div id="literatureArchive"></div>
                <div id="LiteratureArchiveLoader" style="display:none;"><img src="/ProjectDOMPE/_slice/loading.gif"  /> </div>
                <%--<div id="moreInfo" style="display:none; text-align:right;"><img src="/ProjectDOMPE/_slice/viewMore.png" style="cursor:pointer" /> </div>--%>
                <div id="moreInfoBar" class="bar" style="clear: both; width:100%;margin: 0 auto;">
                    <div class="bar2" style="width:600px; margin:0 0 0 0; background-color:#ECECEC; clear:both; height:35px; float:left;">
                    <a  href="javascript:void(0);" id="moreInfo" style="text-align: center; padding-top: 10px; float: left; clear: both; width: 100%; color: #333333; font-weight: bold;"><span><%=Me.BusinessDictionaryManager.Read("DMP_MORE_INFO", _langKey.Id, "Altri Risultati")%></span></a>
                </div>
        </div>
            </div><!--end eventiContInt-->
        </div><!--end colRight-->
        
    </div><!--end container-->
    
</section><!--end body-->

</asp:PlaceHolder>

<asp:PlaceHolder ID="plhDetail" runat="server" />


<script type="text/javascript">

    $("#selPosizione").click(function (event) {
        event.preventDefault();
        $('#ulPosizione').toggle();
    });

    $(".aPosizione").click(function (event) {
        event.preventDefault();
        $('#selPosizione').html($(this).html());
        var element = $(this).attr("data-pos");

        $('#hiddenyear').val(element);

        $('#ulPosizione').hide();
        return false;
    });

    $(".body").click(function (e) {

        if (e.target.id != 'selPosizione') {
            $('#ulPosizione').hide();
        }
    });

    $(document).ready(function(){
			$(".yearsList").html("");
			var _ddlItem = '<option value="{0}">{1}</option>';
			var _curYear = '<%=dateTime.Now.Year.ToString()%>';
			$(".yearsList").append('<option value="">&nbsp;</option>');
			for (var i = _curYear;  i >= 2012; i--) {
				$(".yearsList").append(_ddlItem.replace("{0}", i).replace("{1}", i))
			}

		
			//$('.singleEventsInt').live('mouseover mouseout', function(event) {
        //    event.preventDefault();
        //        if (event.type == 'mouseover') {
        //            //alert($(this).find('#chEf').html());
        //            $(this).find('.txtEvInt').css('display','none');
        //            $(this).find('.goEv').css('display','block');                    
        //            if($(this).find('#chEf').html() == '0'){
        //                //alert($(this).find('#chEf').html());
        //                $(this).find('#chEf').html('1');
        //                $(this).find('.img').children().animate({width: "500px", left:"-90px", top:"-20px"}, 300);                        
        //            }
        //        } else {
        //            $(this).find('.txtEvInt').css('display','block');
        //            $(this).find('.goEv').css('display','none');  
        //            if($(this).find('#chEf').html() == '1'){
        //                $(this).find('#chEf').html('0');
        //                $(this).find('.img').children().animate({width: "140px", left:"0px", top:"0px"}, 300);                        
        //            }
        //        }
        //    });

        $('.singleEventsInt').live({
            mouseenter: function () {
                $(this).find('.txtEvInt').css('display','none');
                $(this).find('.goEv').css('display','block');                    
                $(this).find('.imgCls').animate({width: "200px", left:"-90px", top:"-20px"}, 300);                        
                
            },
            mouseleave: function () {
                $(this).find('.txtEvInt').css('display','block');
                $(this).find('.goEv').css('display','none');                  
                $(this).find('.imgCls').animate({width: "140px", left:"0px", top:"0px"}, 300);                                        
            }
        });                          
    });

    </script>

<!--Menu scroll-->
	<script type="text/javascript">
		<%' If Not _isMobile Then %>
			//$(document).scroll(function () {
	        //var hDoc = $(document).height();
	        //var colLeftH = $('.colLeft').height();
	        //var ctrlEnd = hDoc - 110;
	        //var hWin = $(window).scrollTop() + colLeftH;

	        //if (hWin >= ctrlEnd) {
			
	        //    $('.colLeft').css("position", "fixed");
	        //    $('.colLeft').css("top", "auto");
	        //    $('.colLeft').css("bottom", "110px");
            //}else{
	        //    if ($(window).scrollTop() > 431) {
	        //        $('.colLeft').css("position", "fixed");
	        //        $('.colLeft').css("top", "115px");
	        //        $('.colLeft').css("bottom", "auto");
            //    } else {
	        //        $('.colLeft').css("position", "absolute");
	        //        $('.colLeft').css("top", "546px");
	        //        $('.colLeft').css("bottom", "auto");
            //    }
	        //}
	    //});
	   <%' End If %>
	</script>