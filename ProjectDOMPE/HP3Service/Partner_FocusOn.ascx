﻿<%@ Control  Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<script runat="server">
 Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    Private _cacheKey As String = String.Empty
    Private _cacheKeyTit As String = String.Empty
    Private _classSection As String = String.Empty
    Private _classInfo As String = String.Empty
    Private _inlineStyleTitle As String = String.Empty
    Protected _langId As Integer = 0
	Private _isMobile as boolean = false
		
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _siteFolder = Me.ObjectSiteFolder
        _currentTheme = Me.PageObjectGetTheme
        _langId = Me.PageObjectGetLang.Id
    End Sub
	
        
	Public Function IsMobileDevice() As Boolean
        
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
             if (Request.Browser.IsMobileDevice orelse userAgent.Contains("iphone") orelse userAgent.Contains("blackberry") orelse _
                 userAgent.Contains("mobile") orelse userAgent.Contains("windows ce") orelse userAgent.Contains("opera mini") orelse userAgent.Contains("palm") orelse userAgent.Contains("android")) then
                Return true
            End If
        End if
            
        Return False
        
    End Function	
        
    Dim _canCheckApproval As Boolean = False
    Sub Page_Load()
        _isMobile = IsMobileDevice()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        If Me.PageObjectGetContent.Id > 0 Then
            
            Dim path As String = String.Format("/{0}/HP3Service/DetailArchiveContent.ascx", _siteFolder)
            LoadSubControl(path, plhDetail)
            plhDetail.Visible = True
            plhArchive.Visible = False
        Else
            
            Dim so As New ContentSearcher()
            Dim sum As Integer = 0
            Dim tmpInt As Integer = 0
            so.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
            so.KeyContentType.Id = Me.PageObjectGetContentType.Id
            so.key.IdLanguage = 2'Me.PageObjectGetLang.Id
            If Not _canCheckApproval Then 
				so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
				so.RelatedTo.ContentUser.KeyUser.Id = Me.ObjectTicket.User.Key.Id
				so.RelatedTo.ContentUser.KeyRelationType.Id = Dompe.RelationType.PartnerContent
			End If
            'aggungere filtro su associazione a persone
        
            'dm comuni a tutte le chiamate , nel caso generalizzare
            so.SortType = ContentGenericComparer.SortType.ByData
            so.SortOrder = ContentGenericComparer.SortOrder.DESC
            so.Properties.Add("Title")
            so.Properties.Add("Key.Id")
            so.Properties.Add("ApprovalStatus")
        
            If _canCheckApproval Then Me.BusinessContentManager.Cache = False
            
            Dim coll As New ContentCollection
        
            coll = Me.BusinessContentManager.Read(so)
        
            If Not coll Is Nothing AndAlso coll.Count > 0 Then
                rp_cont.DataSource = coll
                rp_cont.DataBind()
            End If
            
            
            Dim soT As New ThemeSearcher
            Dim collT As ThemeCollection = Nothing
            soT.KeySite = Me.PageObjectGetSite
            soT.KeyFather.Id = Dompe.ThemeConstants.idAreaPartner
            soT.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
            collT = Me.BusinessThemeManager.Read(soT)
           
            If Not collT Is Nothing AndAlso collT.Count > 0 Then
                rptSiteMenu.DataSource = collT
                rptSiteMenu.DataBind()
            End If
            
            plhDetail.Visible = False
            plhArchive.Visible = True
            LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
        End If
    End Sub
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetLink(ByVal vo As ThemeValue) As String
        If vo.Key.Id = Dompe.ThemeConstants.partnerNews Then
            Return Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idNews, _langKey.Id)
        End If
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, vo.Key.Id, _langKey.Id)
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Dim so As New ThemeSearcher()
        so.KeySiteArea = _siteArea
        so.KeyContentType = Me.PageObjectGetContentType
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        Dim theme_value = Helper.GetTheme(so)
        
        If Not theme_value Is Nothing AndAlso theme_value.Key.Id > 0 Then
            Return Me.BusinessMasterPageManager.GetLink(vo.Key, theme_value, False)
        End If
        
        Return String.Empty	
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
  
</script>
<style>
    h3 a {color:#FFF;}
	.bgAccBrand {margin: 0 0 1px;}
</style>

<asp:PlaceHolder ID="plhArchive" runat="server">
    <section>
        <div class="cont-img-top">
    	    <div class="bg-top" style="background-image:url('/ProjectDompe/_slice/focusOnHeader.jpg');">
                
            </div>    
            <div class="txt">
                    <h1 class="white">Focus on</h1>
                </div>
            <div class="layer">&nbsp;</div>
        </div><!--end slider-->
        <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder> 	   
    </section>
    <section>
    <div class="container">
         <div class="colLeft">        	
                <div class="listSide">                	
                     <asp:Repeater ID="rptSiteMenu" runat="server">
                        <HeaderTemplate><ul></HeaderTemplate> 
                        <ItemTemplate>
                            <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# Me.BusinessDictionaryManager.Read("thm_" & DataBinder.Eval(Container.DataItem, "key.id"), _langKey.Id)%></a></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>  
                </div>                        
        </div><!--end colLeft-->
        <div class="colRight">
            <div class="content">
                <asp:Repeater ID="rp_cont" runat="server">
                    <ItemTemplate>
                        <div class="singleAcc" val="c">
                            <div class="about<%#Container.ItemIndex + 1%>">
                    	        <h3 style="padding-left:30px;"><a href="<%#getlink(Container.DataItem)%>"><%#DataBinder.Eval(Container.DataItem, "title") %></a></h3>
                                <%#Healthware.Dompe.Helper.Helper.checkApproval(DataBinder.Eval(Container.DataItem, "ApprovalStatus")) %>
                            </div>                    
                        </div><!--end singleAcc-->
                    </ItemTemplate>
                </asp:Repeater>
            </div><!--end accCont-->
        </div><!--end colRight-->
       
    </div><!--end container-->
    </section>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plhDetail" runat="server" />
<script type="text/javascript">
	$(".singleAcc").click(function () {
		var valore = $(this).attr("val");
		if (valore == "c" || valore == undefined) {
			$(this).attr("val", "o");
			$(this).find(".infoAcc1").css("display", "block");
		} else {
			$(this).attr("val", "c");
			$(this).find(".infoAcc1").css("display", "none");
		}
	});
	
	<% 'If Not _isMobile Then %>
			//$(document).scroll(function () {
	        //var hDoc = $(document).height();
	        //var colLeftH = $('.colLeft').height();
	        //var ctrlEnd = hDoc - 110;
	        //var hWin = $(window).scrollTop() + colLeftH;

	        //if (hWin >= ctrlEnd) {
	        //    $('.colLeft').css("position", "fixed");
	        //    $('.colLeft').css("top", "auto");
	        //    $('.colLeft').css("bottom", "110px");
            //}else{
	        //    if ($(window).scrollTop() > 431) {
	        //        $('.colLeft').css("position", "fixed");
	        //        $('.colLeft').css("top", "115px");
	        //        $('.colLeft').css("bottom", "auto");
            //    } else {
	        //        $('.colLeft').css("position", "absolute");
	        //        $('.colLeft').css("top", "546px");
	        //        $('.colLeft').css("bottom", "auto");
            //    }
	        //}
	    //});
	<% 'end if %>	
	
</script>