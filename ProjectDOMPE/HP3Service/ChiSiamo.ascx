﻿<%@ Control Language="VB" ClassName="ChiSiamo" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" EnableViewState="false" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace=" Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.Dompe.Extensions" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _sedi As String = String.Empty
    Protected _langId As Integer = 0
    Protected _siteKey As SiteIdentificator = Nothing
    Protected _themeKey As ThemeIdentificator = Nothing
    Protected _text As String = String.Empty
    Protected _title As String = String.Empty
    Protected _titleClass As String = String.Empty
    Protected _titleStyle As String = String.Empty
	Private _cacheKey As String = String.Empty
	Protected _siteArea As SiteAreaIdentificator = Nothing
	 
    Protected Sub Page_Init()
        _siteFolder = Me.ObjectSiteFolder
        _langId = Me.PageObjectGetLang.Id
        _siteKey = Me.PageObjectGetSite
        _themeKey = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
    End Sub
    
    Protected Sub Page_Load()
		_cacheKey = "_cacheChiSiamo1Livello:" & _langId & "|" & _themeKey.Id & "|" & _siteArea.Id & "|"
        If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		
		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_text = _strCachedContent
		Else
			InitView()
        End If
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
    End Sub
    
    Protected Sub InitView()
		Dim _finalText as String = String.Empty
	
        Dim soTheme As New ThemeSearcher()
        soTheme.Key = _themeKey
        soTheme.KeyLanguage.Id = _langId
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentExtraSearcher
            so.KeySite = _siteKey
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = theme.KeyContent.Id
            so.CalculatePagerTotalCount = False
            so.SetMaxRow=1
            Dim coll = Me.BusinessContentExtraManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                If Not cont Is Nothing Then
                    _text = cont.FullText
                    _title = cont.TitleContentExtra
                    _titleClass = cont.Code
                    _titleStyle = cont.TitleOriginal
                    Dim replacing = GetPlaceHolders()
                    If Not replacing Is Nothing AndAlso replacing.Any() Then
                        For Each item As KeyValuePair(Of String, String) In replacing
                            _text = _text.Replace(item.Key, item.Value)
                        Next
                    End If
                End If

                '            Dim _headerText As String = "<div class='box-container titleSection'><div class='boxCnt'><div class='container'><h1 class='" & _titleClass & "' " & _titleStyle & ">" & _title & "</h1></div></div></div>"
                '_text = _headerText & _text
            End If
			
			'Cache Content and send to the oputput-------
			CacheManager.Insert(_cacheKey, _text, 1, 120)
			'--------------------------------------------	
        End If
               
    End Sub
    
    Protected Function GetPlaceHolders() As Dictionary(Of String, String)
        Dim replacing As New Dictionary(Of String, String)
        replacing.Add("[STORIA]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idStoria, _langId))
        replacing.Add("[DOMPEATGLANCE]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idAtGlance, _langId))
        replacing.Add("[VIDEOLINK]", GetVideoLink(0))
        replacing.Add("[MISSION]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMission, _langId))
        replacing.Add("[SERGIODOMPE]", GetContentLink(7))
        replacing.Add("[MANAGEMENT]", GetContentLink(627))
        replacing.Add("[SEDI]", GetSedi(New SiteAreaIdentificator(Dompe.SiteAreaConstants.IDSiteareaSedi)))
        replacing.Add("[LINKSEDI]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idSedi, _langId))
        replacing.Add("[RARESTONES]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idRarestOnes, _langId))
		replacing.Add("[WECARE]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idWeCare, _langId))

        Return replacing
    End Function
    
    Protected Function GetSedi(ByVal siteAreaKey As SiteAreaIdentificator) As String
        Dim so As New ContentExtraSearcher()
        so.KeySiteArea = siteAreaKey
        so.Active = SelectOperation.Enabled
        so.Display = SelectOperation.Enabled
        so.Delete=SelectOperation.Enabled
        so.KeyContentType.Id = Dompe.ContentTypeConstants.Id.SediCtype
        so.key.IdLanguage = _langId
        so.KeySite = _siteKey
        so.CalculatePagerTotalCount = False
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SortOrder = ContentGenericComparer.SortOrder.ASC
        
        Dim coll = Me.BusinessContentExtraManager.Read(so)
        If coll Is Nothing OrElse Not coll.Any() Then Return String.Empty
        
        Dim sb As New StringBuilder()
        Dim i As Integer = 1
        Dim link As String = String.Empty
        'sb.Append("<ul>")
        For Each item In coll
            link = Me.BusinessMasterPageManager.GetLink(item, New ThemeIdentificator(Dompe.ThemeConstants.idSedi), New Healthware.HP3.Core.Web.ObjectValues.TraceEventIdentificator(1), False)
            sb.AppendFormat("<a title='{0}' href='{1}' style='color: #5E96A6; font-weight: bold;'>{2}</a>" & IIf(i = coll.Count, "", "&nbsp; | &nbsp;"), item.LinkLabel, link, item.SubTitle)
            i += 1
        Next
        'sb.Append("</ul>")
        
        Return sb.ToString()
    End Function
    
    Function GetVideoLink(ByVal PK As Integer) As String
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        If PK > 0 Then
            soRel.Content.Key.PrimaryKey = PK
            soRel.Content.KeyRelationType = New RelationTypeIdentificator(1)
            soRel.Content.RelationSide = RelationTypeSide.Right
        End If
        
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDVideoGallery
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
      
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Dim cont = coll.FirstOrDefault()
            If Not cont Is Nothing Then
               return String.Format("<a class='viewPicture' href=""/ProjectDOMPE/HP3Popup/VideoGallery.aspx?l={0}&c={1}&v=1"" rel='shadowbox;height=585;width=800' onclick=""recordOutboundFull('VideoGallery','Open Video','{2}')"">{3}</a>", Me.PageObjectGetLang.Code, Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(cont.Key.Id), cont.Title, "Vedi")
                 
            End If
        End If
        
            Return String.Empty
    End Function
    
    Protected Function GetContentLink(idContent As Integer) As String
        
        Dim soTheme As New ThemeSearcher()
        soTheme.Key.Id = Dompe.ThemeConstants.idManagement
        soTheme.KeyLanguage.Id = _langId
        Dim theme = Helper.GetTheme(soTheme)
        If Not theme Is Nothing Then
            Dim so As New ContentSearcher
            Dim idDompe As Integer = idContent
            so.KeySite = Me.PageObjectGetSite
            so.KeySiteArea.Id = theme.KeySiteArea.Id
            so.key.IdLanguage = theme.KeyLanguage.Id
            so.key.Id = idDompe
            Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim cont = coll.FirstOrDefault()
                Return Me.BusinessMasterPageManager.GetLink(cont.Key, theme, False)
            End If
        End If
        
        Return String.Empty
        
    End Function
    
</script>

<section>
 	<div class="cont-img-top">
    	<div class="bg-top" style="background-image:url('/ProjectDOMPE/_slice/chiSiamo.jpg');">
            
        </div>    
         <div class="txt">
                <h1><%=Me.BusinessDictionaryManager.Read("theme_" & _themeKey.Id, _langId, "Chi siamo")%></h1>          
         </div>
         <div class="layer">&nbsp;</div>
    </div><!--end cont-img-top-->
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>



<section>
    <div class="container"><%= _text%></div>
</section>