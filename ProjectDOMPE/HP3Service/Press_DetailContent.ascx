﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContent As Integer = 0
    Private pkContent As Integer = 0
    Private langContent As Integer = 0
    Private imgPath As String = ""
    Private _siteFolder As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
        
    
    Sub Page_Load()
        Dim contentId As Integer = Me.PageObjectGetContent.Id
        Dim serchcont As New ContentExtraSearcher
      
        If Me.PageObjectGetContent.Id > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
          
            contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
           
            If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                titolo_sezione.Text = contentextraVal.Title
                testo_title.Text = contentextraVal.Title
                'imgPath = GetImage(contentextraVal.Key, contentextraVal.Title)
                testo_sezione.Text = contentextraVal.FullText
                testo_subtitle.Text = contentextraVal.Abstract
                testo_optional.Text = contentextraVal.FullTextOriginal
                LoadContentAssociati(contentextraVal.Key.PrimaryKey)
            End If
                
        End If
        
        If Not IsPostBack() Then
         
            LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxDetail_News.ascx", _siteFolder), Phlnews)
            LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxDownload.ascx", _siteFolder), PhlDownload)
            LoadSubControl(String.Format("/{0}/HP3Common/Press_BoxLinks.ascx", _siteFolder), PhlLinks)
            Dim pathPressMenu As String = String.Format("/{0}/HP3Common/Press_BoxLeftMenu.ascx", _siteFolder)
            LoadSubControl(pathPressMenu, PHL_Press_Menu)
        End If
       
                  
    End Sub
    
    Sub LoadContentAssociati(ByRef PK As Integer)
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        'If PK > 0 Then
        soRel.Content.Key.PrimaryKey = PK
        soRel.Content.KeyRelationType.Id = Dompe.RelationType.DocumentDedicateRelation
        soRel.Content.RelationSide = RelationTypeSide.Right
        soRel.Content.RelationTypeSearcher = RelationTypeSearch.SelectedType
        ' End If
        
      
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadContenutiAreaMediaPress
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
      
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            rpdDownloadRelated.DataSource = coll
            rpdDownloadRelated.DataBind()
        End If
    End Sub
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = idContent
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
     
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Function GetImage(ByVal IDcont As ContentIdentificator, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(IDcont, 900, title, title)
        Return res
    End Function
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
</script>
<div class="presentation">
      <div class="presentationBox">
             <div class="presentationBox2">
                    <div class="presentationBox3">
                          <div class="container">
                             <h2><asp:Literal ID="titolo_sezione" runat="server" /></h2>                          
                          </div>
                    </div>
             </div>
      </div>
</div>
<div class="body">
  <div class="container">
       <div class="bodyBox">
        <div class="detailPress">
        <div class="colLeft">
        	<asp:PlaceHolder ID="PHL_Press_Menu" runat="server" Visible="true" />
        </div>
          <div class="leftBody">
            <div class="content">
            <asp:Literal ID="testo_optional" runat="server" />
            <h3><asp:Literal ID="testo_title" runat="server" /></h3>
              <h4><asp:Literal ID="testo_subtitle" runat="server" /></h4>
              <asp:Literal ID="testo_sezione" runat="server" />
            </div>
      
            <asp:Repeater ID="rpdDownloadRelated" ViewStateMode="Disabled" runat="server">
       <HeaderTemplate><div class="dwnBox"><h5><%= Me.BusinessDictionaryManager.Read("DMP_RelatedMaterial", _langKey.Id, "DMP_RelatedMaterial")%></h5></HeaderTemplate>
       <ItemTemplate>
        <a href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>" onclick="recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, ContentValue).Key.PrimaryKey)%>','Download DetailPress','Click','<%# CType(Container.DataItem, ContentValue).Title%>')">
         <span class="txt"><span class="title"><%# CType(Container.DataItem, ContentValue).Title%></span>
                <span><%# IIf(CType(Container.DataItem, ContentValue).Launch.Length > 120, Left(CType(Container.DataItem, ContentValue).Launch, 120) & "...", CType(Container.DataItem, ContentValue).Launch)%> </span>
 <%--                 
                <span> <%# getSizeFile(CType(Container.DataItem, DownloadValue).DownloadTypeCollection(0).Weight)%></span>--%>
              </span> <span class="readMore2">Download</span>
       </ItemTemplate>
       <FooterTemplate></div></FooterTemplate>
       </asp:Repeater>
          </div>
        <div class="rightBody">
              <asp:PlaceHolder ID="phlManagement" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlBoxImage" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="Phlnews" runat="server"></asp:PlaceHolder>
            <div class="rightBodyRow">
              <asp:PlaceHolder ID="PhlDownload" runat="server"></asp:PlaceHolder>
              <asp:PlaceHolder ID="PhlLinks" runat="server"></asp:PlaceHolder>
            </div>
        </div>
        </div>
        </div>
   </div>
</div>
          


