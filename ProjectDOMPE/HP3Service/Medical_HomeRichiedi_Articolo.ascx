﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Dompe.Extensions"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Private Dict As New DictionaryManager
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Private langContent As Integer = 0
    Dim _strAddQs As String = String.Empty
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
        contentId = Me.PageObjectGetContent.Id
    End Sub

    Sub Page_Load()
		If Not String.IsNullOrEmpty(Request("skipdc")) AndAlso Request("skipdc") = "skip" Then _strAddQs = "&skipdc=skip"
        If Me.BusinessAccessService.IsAuthenticated Then
            InitView()
        Else
            If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
                Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
                idContentTheme = valthem.KeyContent.Id
                'verifico se il content è quello associato al tema allora carico il contenuto generico
                If contentId = idContentTheme Then
                Else
                    plhContentGeneric.Visible = False
                End If
            Else
                plhContentGeneric.Visible = False
            End If
		End If
    End Sub
    
    Protected Sub InitView()
		Dim urlred As String = String.Empty
       If Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleMediaPress) Then
            urlred = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langKey.Id)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & urlred & "','Login','OK','Media Press');return false;})", True)
        
        Else If Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleDoctor) Then
			urlred = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langKey.Id)
			'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & urlred & "','Login','OK','role doctor');return false;})", True)
        End If
		Response.Redirect(urlred, False)
    End Sub
</script>

<div id="DivImagesTop" class="" runat="server"></div>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">

    <section>
        <div class="cont-img-top">
            <div class="bg-top" style="background-image: url('/HP3Image/cover/112-1_gen_1280.jpg');">                            
            </div>
            <div class="txt">
                    <h1>Richiedi l'articolo</h1>
                </div>
            <div class="layer">&nbsp;</div>
        </div>
    </section>
    <section>
    <div class="container">
        <asp:PlaceHolder runat="server" ID="PhlLogin" />
        <iframe id="FrameDocCheck" width="100%" height="700" frameborder="0" scrolling="no" src="/ProjectDOMPE/HP3Common/HCPLogin.aspx?c=<%=Me.PageObjectGetTheme.Id%><%=_strAddQs%>" style="margin:0px; padding:0px; border:0px;"></iframe>
    </div>
    </section>



</asp:PlaceHolder>