﻿<%@ Control Language="VB" ClassName="whatsNew" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected contentextraVal As New ContentExtraValue
    Private idContentTheme As Integer = 0
    Private pkContent As Integer = 0
    Dim contentId As Integer = 0
    Dim ContenutoStoria As String = ""
    Private langContent As Integer = 0
    Private imgPath As String = "/Dompe/_slice/imgCenter.jpg"
    Protected _TitlePage As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
        _siteArea = Me.PageObjectGetSiteArea
        _currentTheme = Me.PageObjectGetTheme
        _siteFolder = Me.ObjectSiteFolder
    End Sub
        
    
    Dim _canCheckApproval As Boolean = False
    
    Sub Page_Load()
        contentId = Me.PageObjectGetContent.Id
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
     
        Dim serchcont As New ContentExtraSearcher
        If contentId > 0 Then 'controllo se è presente il content altrimenti segnalo il problema
            Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            idContentTheme = valthem.KeyContent.Id
            Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _langKey.Id)
           
           ' verifico se il content è quello associato al tema allora carico il contenuto generico
            If contentId = idContentTheme Then
                
                
                Dim soEx As New ContentExtraSearcher()
                soEx.key.Id = contentId
                soEx.key.IdLanguage = _langKey.Id
                soEx.CalculatePagerTotalCount = False
                soEx.SetMaxRow = 1
                soEx.KeySite = Me.PageObjectGetSite
                If Not _canCheckApproval Then soEx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
                Dim contentextraColl = Me.BusinessContentExtraManager.Read(soEx)
                If Not contentextraColl Is Nothing AndAlso contentextraColl.Any() Then
                
                    contentextraVal = contentextraColl.FirstOrDefault
                    
                    'contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, _langKey.Id))
                    'If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
                    _TitlePage = contentextraVal.Title
                    '  titolo_sezione.Text = contentextraVal.Title
                    testo_sezione.Text = contentextraVal.FullText
                    ContenutoStoria = contentextraVal.FullText
                    testo_subtitle.Text = contentextraVal.Abstract
                    '   ReferenceLink(contentextraVal.FullTextOriginal)
                    'imgPath = IIf(String.IsNullOrEmpty(contentextraVal.LinkContentExtra), "/Dompe/_slice/imgCenter.jpg", contentextraVal.LinkContentExtra)
                End If
                plhDetail.Visible = False
                plhContentGeneric.Visible = True
                
                If _currentTheme.Id = Dompe.ThemeConstants.idStoria Then
                    plhDetail.Visible = False
                    plhContentGeneric.Visible = False
                    phlStoria.Visible = True
                End If
           
            Else 'se il content non è quello associato al tema allora carico il dettaglio
                Dim path As String = String.Format("/{0}/HP3Service/DetailContent.ascx", _siteFolder)
                LoadSubControl(path, plhDetail)
                plhDetail.Visible = True
                plhContentGeneric.Visible = False
            
            End If
                               
        
        Else
            titolo_sezione.Text = ""
            testo_sezione.Text = "Nessun content associato al tema"
            testo_subtitle.Text = ""
            '   ReferenceLink(contentextraVal.FullTextOriginal)
            'imgPath = IIf(String.IsNullOrEmpty(contentextraVal.LinkContentExtra), "/Dompe/_slice/imgCenter.jpg", contentextraVal.LinkContentExtra)
         
        End If
        
        LoadSubControl(String.Format("/{0}/HP3Common/BreadCrumbs.ascx", _siteFolder), plh_breadcrumbs)
                  
    End Sub
    
    Function GetContentPKbyID(ByVal id As Integer) As Integer
        Dim serchct As New ContentExtraSearcher
        Dim serchColl As ContentExtraCollection
        Dim PK As Integer = 0
        serchct.key.Id = id
        'serchct.Display = SelectOperation.Enabled
        'serchct.Active = SelectOperation.Enabled
        'serchct.Delete = SelectOperation.Disabled
        serchct.Properties.Add("Key.PrimaryKey")
        
        serchColl = Me.BusinessContentExtraManager.Read(serchct)
        
        If Not serchColl Is Nothing AndAlso serchColl.Count > 0 Then
            PK = serchColl(0).Key.PrimaryKey
        End If
        
        Return PK
    End Function
      
    
    Function GetLink() As String
        Dim keyContent As New ContentIdentificator
        keyContent.Id = contentId
      
        keyContent.PrimaryKey = pkContent
        keyContent.IdLanguage = langContent
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        
        Return Me.BusinessMasterPageManager.GetLink(keyContent, GetTheme(), False)
    End Function
    
     
    Function GetLink(ByVal keyContent As ContentIdentificator, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "Downlaod|" & ClassUtility.PropertyValueToString(keyContent)
        cachekey += "|" & Me.PageObjectGetContentType.Id.ToString
        cachekey += "|" & Me.PageObjectGetSiteArea.Id.ToString
        cachekey += "|" & Me.PageObjectGetTheme.Id.ToString
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
                    
        If (String.IsNullOrEmpty(cachedLink)) Then
            
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"

            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
      
            If (Not keyContent Is Nothing AndAlso keyContent.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", keyContent.PrimaryKey, keyContent.Id, keyContent.IdLanguage, Me.PageObjectGetContentType.Id, Me.PageObjectGetSiteArea.Id, Me.PageObjectGetTheme.Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
                          
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
 
        End If
        
        Return cachedLink
    End Function
    
       
    Function GetSearchLink(ByVal domain As String, ByVal name As String, ByVal tag As Integer) As String
        
        If tag > 0 Then
            Dim so As New ThemeSearcher
       
            so.KeyLanguage = _langKey
            so.Key.Domain = domain
            so.Key.Name = name
            so.KeySite = Me.PageObjectGetSite
            so.LoadHasSon = False
            so.LoadRoles = False
            so.Display = SelectOperation.Disabled
      
            Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
            If (Not coll Is Nothing AndAlso coll.Any) And (Not String.IsNullOrEmpty(tag)) Then
                Dim link As String = Me.BusinessMasterPageManager.GetLink(coll(0), False)
                link = link & "?tag=" & tag.ToString
            
                Return link
            End If
        
        End If
               
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
       
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
</script>
<asp:PlaceHolder ID="plhContentGeneric" runat="server" Visible="true">
<div class="presentation">
       <div class="presentationBox">
             <div class="presentationBox2">
                 <div class="presentationBox3">
                      <div class="container">
        	             	<h1><%= _TitlePage%></h1>
                    </div>
                </div>
            </div>
        </div>
</div>
    <div class="body">
        <div class="container">
            <div class="bodyBox">
                <asp:Literal ID="titolo_sezione" runat="server" />
                <asp:Literal ID="testo_subtitle" runat="server" />
                <asp:Literal ID="testo_sezione" runat="server" />
            </div>
        </div>
    </div><!--block-->
</asp:PlaceHolder>
<asp:PlaceHolder ID="phlStoria" runat="server" Visible="false">

<script type="text/javascript">
    var currLang = '<%=_langKey.Code.ToLower()%>';
</script>

    <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/storyline.css">
    <script language="JavaScript" type="text/javascript" src="/Js/swiper.min.js"></script> 
    <script language="JavaScript" type="text/javascript" src="/Js/storyline.js"></script> 
<section>
    <div class="cont-img-top">
        <div class="bg-top" style="background-image: url('<%=String.Format("/{0}/_slice/storia.jpg", _siteFolder)%>');">                                       
        </div>
        <div class="txt">
                    <h1 class="white"><%= _TitlePage%></h1>
        </div> 
        <div class="layer">&nbsp;</div>
    </div>
    <asp:PlaceHolder ID="plh_breadcrumbs" runat="server"></asp:PlaceHolder>
</section>


    <section id="section-story" class="contentFull">
    <div class="firstContainer contentFull">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div id="swiperWidth" class="swiper-slide">
                    <ul id="storyLine"></ul>
                </div>
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
    <div style="height:30px;">&nbsp;</div>
    <div class="dragger-row">
        <div class="dragger-row-wrapper">
            <div class="dragger-image"></div>
        </div>
    </div>
</section>
        <%--<div class="container">
            <%= ContenutoStoria%>
        </div>--%>
    
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plhDetail" Visible="false">
&nbsp;
</asp:PlaceHolder>