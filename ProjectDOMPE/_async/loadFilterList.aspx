﻿<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>


<script runat="server" language="VB">
    Private languageId As Integer = 0
    Private defsiteLang As Integer = 0
    Private currentSAId As Integer = 0
    Private strsearch As String = ""
    Private _maskfilt As String = ""
    Private _strDDlAreeTeraupetiche As String = ""
    Private _strDDlPrincipiAttivi As String = ""
    Private _strDDlBrand As String = ""
    Dim areaT As String = "" 'hiddenaree.Value
    Dim principiAtt As String = "" 'hiddenprincipi.Value
    Dim brand As String = "" 'hiddenbrand.Value
    Dim areaTText As String = ""
    Dim principiAttText As String = ""
    Dim brandText As String = ""
    
    Dim selclk As String = ""
    
    Sub page_init()
    
    End Sub
    
    Sub page_load()
        ltlDDLPrincipi.Text = ""
        ltlDDLPrincipi.DataBind()
        ltlDDLArea.Text = ""
        ltlDDLArea.DataBind()
        ltlDDLBrand.Text = ""
        ltlDDLBrand.DataBind()
        If Not String.IsNullOrEmpty(Request("selClick")) Then
            selclk = Request("selClick")
        End If
       
        If Not String.IsNullOrEmpty(Request("AreaVal")) Then
            areaT = Request("AreaVal")
        End If
        
        If Not String.IsNullOrEmpty(Request("AreaText")) Then
            areaTText = Request("AreaText")
        End If
        
        If Not String.IsNullOrEmpty(Request("principioVal")) Then
            principiAtt = Request("principioVal")
        End If
       
        If Not String.IsNullOrEmpty(Request("principioText")) Then
            principiAttText = Request("principioText")
        End If
        
        If Not String.IsNullOrEmpty(Request("lang")) Then
            languageId = Integer.Parse(Request("lang"))
        End If
        
        If Not String.IsNullOrEmpty(Request("brandVal")) Then
            brand = Request("brandVal")
        End If
        
        If Not String.IsNullOrEmpty(Request("brandText")) Then
            brandText = Request("brandText")
        End If
        
        If String.IsNullOrEmpty(selclk) Or (selclk = "AREA" And String.IsNullOrEmpty(areaT)) Then
            readXmlFilter()
        Else
            readXmlFilterClick()
        End If
       
        'Response.Write("selclk:" & selclk)
            
    End Sub

    Sub readXmlFilter()
        
         
        Dim doc As XDocument = XDocument.Load(Server.MapPath("/ProjectDOMPE/_xml/infoBrand.xml"))
        If Request("cache") = "false" Then
            CacheManager.RemoveByKey("CacheDDLAllAree")
            CacheManager.RemoveByKey("CacheDDLAllPrincipi")
            CacheManager.RemoveByKey("CacheDDLAllBrand")
        End If
        
        'Dim _strCachedDDlMenu As String = CacheManager.Read("CacheDDLAllAree", 1)
        'If Not (_strCachedDDlMenu Is Nothing) AndAlso (_strCachedDDlMenu.Length > 0) Then
        '    _strDDlAreeTeraupetiche = _strCachedDDlMenu
        'Else
        Dim areenames = doc.Elements("RCP").Elements("AREATERAPEUTICHE").Elements("AREA")
        For Each elemento In areenames
            _strDDlAreeTeraupetiche = _strDDlAreeTeraupetiche & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
        Next
        'CacheManager.Insert("CacheDDLAllAree", _strDDlAreeTeraupetiche, 1, 120)
        'End If
        
          
        ''Dim _strCachedDDlPrincipi As String = CacheManager.Read("CacheDDLAllPrincipi", 1)
        ''If Not (_strCachedDDlPrincipi Is Nothing) AndAlso (_strCachedDDlPrincipi.Length > 0) Then
        ''    _strDDlPrincipiAttivi = _strCachedDDlPrincipi
        ''Else
        'Dim principinames = doc.Elements("RCP").Elements("PRINCIPIATTIVI").Elements("PRINCIPIO")
        'For Each elemento In principinames
        '    _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
        'Next
        ''CacheManager.Insert("CacheDDLAllPrincipi", _strDDlPrincipiAttivi, 1, 120)
        ''End If
        
        ''Dim _strCachedDDlBrand As String = CacheManager.Read("CacheDDLAllBrand", 1)
        ''If Not (_strCachedDDlBrand Is Nothing) AndAlso (_strCachedDDlBrand.Length > 0) Then
        ''    _strDDlBrand = _strCachedDDlBrand
        ''Else
        'Dim brandnames = doc.Elements("RCP").Elements("BRANDS").Elements("BRAND")
        'For Each elemento In brandnames
        '    _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
        'Next
        ''CacheManager.Insert("CacheDDLAllBrand", _strDDlBrand, 1, 120)
        ''End If
      
     
        ltlDDLArea.Text = _strDDlAreeTeraupetiche
        ltlDDLArea.DataBind()
        ltlDDLBrand.Text = _strDDlBrand
        ltlDDLBrand.DataBind()
        ltlDDLPrincipi.Text = _strDDlPrincipiAttivi
        ltlDDLPrincipi.DataBind()
        hiddenaree.Value = areaT
        hiddenareetxt.Value = areaTText
        hiddenbrand.Value = brand
        hiddenbrandtxt.Value = brandText
        hiddenprincipi.Value = principiAtt
        hiddenprincipitxt.Value = principiAttText
        selAree.InnerHtml = IIf(String.IsNullOrEmpty(areaTText), "Tutte le aree terapeutiche", areaTText)
        selPrincipi.InnerHtml = IIf(String.IsNullOrEmpty(principiAttText), "Tutti i Principi attivi", principiAttText)
        selBrand.InnerHtml = IIf(String.IsNullOrEmpty(brandText), "Tutti i brand", brandText)
        
        If (selclk = "AREA" And String.IsNullOrEmpty(areaT)) Then
            selPrincipi.InnerHtml = "Tutti i Principi attivi"
            selBrand.InnerHtml = "Tutti i brand"
            hiddenbrand.Value = ""
            hiddenbrandtxt.Value = "Tutti i brand"
            hiddenprincipi.Value = ""
            hiddenprincipitxt.Value = "Tutti i Principi attivi"
        End If
    End Sub
    
    Sub readXmlFilterClick()
     
        Dim doc As XDocument = XDocument.Load(Server.MapPath("/ProjectDOMPE/_xml/infoBrand.xml"))
      
     
       
        If selclk = "AREA" Then
            Dim areenames = doc.Elements("RCP").Elements("AREATERAPEUTICHE").Elements("AREA")
            For Each elemento In areenames
                If elemento.Attribute("id").Value = areaT Then
                    SplitInfo(elemento.Attribute("brands"), elemento.Attribute("principi"), "{" & areaT & "}")
                End If
            Next
        ElseIf selclk = "PRINCIPIO" Then
            Dim principinames = doc.Elements("RCP").Elements("PRINCIPIATTIVI").Elements("PRINCIPIO")
            For Each elemento In principinames
                If elemento.Attribute("id").Value = principiAtt Or String.IsNullOrEmpty(principiAtt) Then
                    'Response.Write(elemento.Attribute("brands").Value & " " & "{" & principiAtt & "}" & " " & elemento.Attribute("aree").Value)
                    SplitInfo(elemento.Attribute("brands"), "{" & principiAtt & "}", elemento.Attribute("aree"))
                End If
            Next
        ElseIf selclk = "BRAND" Then
            'Response.Write("brand")
            Dim brandnames = doc.Elements("RCP").Elements("BRANDS").Elements("BRAND")
            For Each elemento In brandnames
                If elemento.Attribute("id").Value = brand Or String.IsNullOrEmpty(brand) Then
                    SplitInfo("{" & brand & "}", elemento.Attribute("principi"), elemento.Attribute("aree"))
                End If
            Next
        End If
        
              
        ltlDDLArea.Text = _strDDlAreeTeraupetiche
        ltlDDLArea.DataBind()
        ltlDDLBrand.Text = _strDDlBrand
        ltlDDLBrand.DataBind()
        ltlDDLPrincipi.Text = _strDDlPrincipiAttivi
        ltlDDLPrincipi.DataBind()
        
        hiddenaree.Value = areaT
        hiddenareetxt.Value = areaTText
        selAree.InnerHtml = IIf(String.IsNullOrEmpty(areaTText), "Tutte le aree terapeutiche", areaTText)
        
        If selclk <> "AREA" Then
            hiddenbrand.Value = brand
            hiddenbrandtxt.Value = brandText
            hiddenprincipi.Value = principiAtt
            hiddenprincipitxt.Value = principiAttText            
            selPrincipi.InnerHtml = IIf(String.IsNullOrEmpty(principiAttText), "Tutti i Principi attivi", principiAttText)
            selBrand.InnerHtml = IIf(String.IsNullOrEmpty(brandText), "Tutti i brand", brandText)
        End If
    End Sub
     

  
    Sub SplitInfo(Optional ByVal strIdBrand As String = "", Optional ByVal stringPrinc As String = "", Optional ByVal stringAree As String = "")
        
        Dim doc As XDocument = XDocument.Load(Server.MapPath("/ProjectDOMPE/_xml/infoBrand.xml"))
        
        _strDDlAreeTeraupetiche = ""
        _strDDlBrand = ""
        _strDDlPrincipiAttivi = ""
       
      '  Response.Write("string Brand " & strIdBrand & "<br /> string princ " & stringPrinc & "<br /> string aree " & stringAree)
        
        'Dim _strCachedDDlAreeSplit As String = CacheManager.Read("CacheSplitAree" & stringAree, 1)
        'If Not (_strCachedDDlAreeSplit Is Nothing) AndAlso (_strCachedDDlAreeSplit.Length > 0) Then
        '    _strDDlAreeTeraupetiche = _strCachedDDlAreeSplit
        'Else
        Dim areenames = doc.Elements("RCP").Elements("AREATERAPEUTICHE").Elements("AREA")
        For Each elemento In areenames
            'If stringAree.Contains("{" & elemento.Attribute("id").Value & "}") Then
            _strDDlAreeTeraupetiche = _strDDlAreeTeraupetiche & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
            'End If
        Next
        If Not String.IsNullOrEmpty(areaT) Then
            _strDDlAreeTeraupetiche = _strDDlAreeTeraupetiche & "  <li><a rel="""" href=""#"">Tutte le aree terapeutiche</a></li>"
        End If
        'CacheManager.Insert("CacheSplitAree" & stringAree, _strDDlAreeTeraupetiche, 1, 120)
        'End If
        
        
        If selclk = "AREA" Then
            
            Dim principinames = doc.Elements("RCP").Elements("PRINCIPIATTIVI").Elements("PRINCIPIO")
            _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel="""" href=""#"">Tutti i Principi attivi</a></li>"
            For Each elemento In principinames            
               If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}")) Then
                    _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                End If
            Next
            
            Dim brandnames = doc.Elements("RCP").Elements("BRANDS").Elements("BRAND")
            _strDDlBrand = _strDDlBrand & "  <li><a rel="""" href=""#"">Tutti i brand</a></li>"
            For Each elemento In brandnames
                If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}")) Then
                    _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                End If
            Next
            
            selPrincipi.InnerHtml = "Tutti i Principi attivi"
            hiddenprincipi.Value = ""
            hiddenprincipitxt.Value = "Tutti i Principi attivi"
            selBrand.InnerHtml = "Tutti i brand"
            hiddenbrand.Value = ""
            hiddenbrandtxt.Value = "Tutti i brand"
            
            
            'Response.Write("test " & selBrand.InnerHtml)
        Else
            'Response.Write("test2")
         
            ' Dim _strCachedDDlPrincipiSplit As String = CacheManager.Read("CacheSplitPrincipi" & stringPrinc, 1)
            'If Not (_strCachedDDlPrincipiSplit Is Nothing) AndAlso (_strCachedDDlPrincipiSplit.Length > 0) Then
            '_strDDlPrincipiAttivi = _strCachedDDlPrincipiSplit
            ' Else
            Dim principinames = doc.Elements("RCP").Elements("PRINCIPIATTIVI").Elements("PRINCIPIO")
            If (principiAtt <> "") Then _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel="""" href=""#"">Tutti i Principi attivi</a></li>"
            For Each elemento In principinames
            
                If (Not String.IsNullOrEmpty(areaT) And Not String.IsNullOrEmpty(brand)) Then
                    'Response.Write("test1")
                    If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}") And (elemento.Attribute("brands").Value).Contains("{" & brand & "}")) Then
                        If elemento.Attribute("id").Value <> principiAtt Then _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                ElseIf (Not String.IsNullOrEmpty(areaT) And String.IsNullOrEmpty(brand)) Then
                    'Response.Write("test2")
                    If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}")) Then
                        If elemento.Attribute("id").Value <> principiAtt Then _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                ElseIf (String.IsNullOrEmpty(areaT) And Not String.IsNullOrEmpty(brand)) Then
                    'Response.Write("test3")
                    If ((elemento.Attribute("brands").Value).Contains("{" & brand & "}")) Then
                        If elemento.Attribute("id").Value <> principiAtt Then _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                Else
                    If elemento.Attribute("id").Value <> principiAtt Then
                        _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                End If
            
                'If (String.IsNullOrEmpty(areaT) Or (Not (String.IsNullOrEmpty(areaT)) And (elemento.Attribute("aree").Value).Contains("{" & areaT & "}"))) And (String.IsNullOrEmpty(brand) Or (Not (String.IsNullOrEmpty(brand)) And (elemento.Attribute("brands").Value).Contains("{" & brand & "}"))) Then 'stringPrinc.Contains("{" & elemento.Attribute("id").Value & "}") Then
                '    'Response.Write("sì")
                '    _strDDlPrincipiAttivi = _strDDlPrincipiAttivi & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                'End If
            Next
       
            'CacheManager.Insert("CacheSplitPrincipi" & stringPrinc, _strDDlPrincipiAttivi, 1, 120)
            'End If
        
            'Dim _strCachedDDlBrandSplit As String = CacheManager.Read("CacheSplitBrand" & strIdBrand, 1)
            'If Not (_strCachedDDlBrandSplit Is Nothing) AndAlso (_strCachedDDlBrandSplit.Length > 0) Then
            '_strDDlBrand = _strCachedDDlBrandSplit
            'Else
            Dim brandnames = doc.Elements("RCP").Elements("BRANDS").Elements("BRAND")
            If (brand <> "") Then _strDDlBrand = _strDDlBrand & "  <li><a rel="""" href=""#"">Tutti i brand</a></li>"
            For Each elemento In brandnames
            
                If (Not String.IsNullOrEmpty(areaT) And Not String.IsNullOrEmpty(principiAtt)) Then
                    If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}") And (elemento.Attribute("principi").Value).Contains("{" & principiAtt & "}")) Then
                        If elemento.Attribute("id").Value <> brand Then _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                ElseIf (Not String.IsNullOrEmpty(areaT) And String.IsNullOrEmpty(principiAtt)) Then
                    If ((elemento.Attribute("aree").Value).Contains("{" & areaT & "}")) Then
                        If elemento.Attribute("id").Value <> brand Then _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                ElseIf (String.IsNullOrEmpty(areaT) And Not String.IsNullOrEmpty(principiAtt)) Then
                    If ((elemento.Attribute("principi").Value).Contains("{" & principiAtt & "}")) Then
                        If elemento.Attribute("id").Value <> brand Then _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                Else
                    If elemento.Attribute("id").Value <> brand Then
                        _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                    End If
                End If
            
                'If strIdBrand.Contains("{" & elemento.Attribute("id").Value & "}") Then
                'If (String.IsNullOrEmpty(areaT) Or (Not (String.IsNullOrEmpty(areaT)) And (elemento.Attribute("aree").Value).Contains("{" & areaT & "}"))) And (String.IsNullOrEmpty(principiAtt) Or (Not (String.IsNullOrEmpty(principiAtt)) And (elemento.Attribute("principi").Value).Contains("{" & principiAtt & "}"))) Then 'stringPrinc.Contains("{" & elemento.Attribute("id").Value & "}") Then
                '    _strDDlBrand = _strDDlBrand & "  <li><a rel=""" & elemento.Attribute("id").Value & """ href=""#"">" & elemento.Attribute("name").Value & "</a></li>"
                'End If
            Next
       
            'CacheManager.Insert("CacheSplitBrand" & strIdBrand, _strDDlBrand, 1, 120)
            'End If
        End If
    End Sub
   

    
</script>

                  <div class="selMed">
                        <a href="#" title='' id="selAree" runat="server" rel="ulFilterAree" class="selectddl"></a>
                        <ul id='ulFilterAree'>
                          <%# Eval("NAME") %>
                          <asp:Literal ID="ltlDDLArea" runat="server"></asp:Literal>
                        </ul>
                      <input type="hidden" ID="hiddenaree" runat="server" />
                      <input type="hidden" ID="hiddenareetxt" runat="server" />
                    </div>
                    <div class="selMed">
                        <a href="#" title=' ' id="selPrincipi" runat="server" rel="ulFilterPrincipi" class="selectddl"></a>
                        <ul id='ulFilterPrincipi'>
                         <asp:Literal ID="ltlDDLPrincipi" runat="server"></asp:Literal>
                        </ul>
                         <input type="hidden" ID="hiddenprincipi" runat="server" />
                         <input type="hidden" ID="hiddenprincipitxt" runat="server" />
                    </div>
                    <div class="selMed">
                    <a href="#" title='' id="selBrand" rel="ulFilterBrand" runat="server" class="selectddl"></a>
                    <ul id='ulFilterBrand'>
                       <asp:Literal ID="ltlDDLBrand" runat="server"></asp:Literal>
                    </ul>
                   <input type="hidden" ID="hiddenbrand" runat="server" />
                    <input type="hidden" ID="hiddenbrandtxt" runat="server" />
   </div>
 <script>
    var selClick = ""
    function removeAddlink() {
        $(".selMed").remove();
    }

    function AddFilter() {
        var brandId = $("#<%=hiddenbrand.ClientID%>").val();
        var brandTxt = $("#<%=hiddenbrandtxt.ClientID%>").val();
        var principioId = $("#<%=hiddenprincipi.ClientID%>").val();
        var principioTxt = $("#<%=hiddenprincipitxt.ClientID%>").val();
        var AreaId = $("#<%=hiddenaree.ClientID%>").val();
        var AreaTxt = $("#<%=hiddenareetxt.ClientID%>").val();
        removeAddlink();
        $.ajax({
            contentType: "text/html; charset=utf-8",
            url: '/ProjectDOMPE/_async/loadFilterList.aspx',
            dataType: "html",
            data: { 'selClick': selClick, 'AreaVal': AreaId, 'AreaText': AreaTxt, 'principioVal': principioId, 'principioText': principioTxt, 'brandVal': brandId, 'brandText': brandTxt },
            success: function (data) {
                $('#FilterDiv').append(data);
            }
        });
        // 
    }
</script>
    <script type="text/javascript">

        function ResetForm() {
            $("#<%=hiddenaree.ClientID%>").val("");
                    $("#<%=hiddenareetxt.ClientID%>").val("");
                    $("#<%=hiddenprincipi.ClientID%>").val("");
                    $("#<%=hiddenprincipitxt.ClientID%>").val("");
                    $("#<%=hiddenbrand.ClientID%>").val("");
                    $("#<%=hiddenbrandtxt.ClientID%>").val("");
                    brandId = ""
                    brandTxt = ""
                    principioId = ""
                    principioTxt = ""
                    AreaId = ""
                    AreaTxt = ""
                    selClick = ""
                    AddFilter();
                    return false;
                }
        

        $(".selectddl").click(function (event) {
            event.preventDefault();
            var myddl = $(this).attr("rel")
            $("#" + myddl).toggle();
            if (myddl == "ulFilterAree") {
                $('#ulFilterBrand').hide();
                $('#ulFilterPrincipi').hide();
           } else if (myddl == "ulFilterPrincipi") {
                $('#ulFilterBrand').hide();
                $('#ulFilterAree').hide();
           } else if (myddl == "ulFilterBrand") {               
                $('#ulFilterPrincipi').hide();
                $('#ulFilterAree').hide();
            }
        });

        $("#ulFilterAree li a").click(function (event) {
            event.preventDefault();
             $('#<%=selAree.ClientID%>').html($(this).html());
            var rel = $(this).attr("rel");
            $("#<%=hiddenaree.ClientID%>").val(rel)
            $("#<%=hiddenareetxt.ClientID%>").val($(this).html())
            selClick = "AREA"
            AddFilter();
        });
        $("#ulFilterPrincipi li a").click(function (event) {
            event.preventDefault();
            $('#<%=selPrincipi.ClientID%>').html($(this).html());
            var rel = $(this).attr("rel");
            $("#<%=hiddenprincipi.ClientID%>").val(rel)
            $("#<%=hiddenprincipitxt.ClientID%>").val($(this).html())
            selClick = "PRINCIPIO"
            AddFilter();
        });
        $("#ulFilterBrand li a").click(function (event) {            
            event.preventDefault();
            $('#<%=selBrand.ClientID%>').html($(this).html());
            var rel = $(this).attr("rel");
            $("#<%=hiddenbrand.ClientID%>").val(rel)
            $("#<%=hiddenbrandtxt.ClientID%>").val($(this).html())
            selClick = "BRAND"
            AddFilter();
        });


        $(".body").click(function (e) {
            if (e.target.id != '<%=selAree.ClientID %>' && e.target.id != '<%=selPrincipi.ClientID%>' && e.target.id != '<%=selBrand.ClientID%>') {
                // hideDDL();
            }
        });

        function hideDDL() {
            $('#ulFilterBrand').hide();
            $('#ulFilterPrincipi').hide();
            $('#ulFilterAree').hide();
        }

        $(document).ready(function () {
            if ($("#<%=hiddenaree.ClientID%>").val() == '') {
                $('#<%=selPrincipi.ClientID%>').attr("style", "color:#CCC");
                $('#<%=selBrand.ClientID%>').attr("style", "color:#CCC");
            }
        });

        $('html').click(function (e) {
            //alert(e.target.id);
            if (e.target.id != 'selPrincipi' && e.target.id != 'selAree' && e.target.id != 'selBrand') {
                $('#ulFilterBrand').hide();
                $('#ulFilterPrincipi').hide();
                $('#ulFilterAree').hide();
            }
        });
	</script>