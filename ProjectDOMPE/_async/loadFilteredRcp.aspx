﻿<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>


<script runat="server" language="VB">
    Private languageId As Integer = 0
    Private defsiteLang As Integer = 0
    Private currentSAId As Integer = 0
    Private strsearch As String = ""
    Private _maskfilt As String = ""
    Private _strDDlAreeTeraupetiche As String = ""
    Private _strDDlPrincipiAttivi As String = ""
    Private _strDDlBrand As String = ""
    Dim areaT As String = "" 'hiddenaree.Value
    Dim principiAtt As String = "" 'hiddenprincipi.Value
    Dim brand As String = "" 'hiddenbrand.Value
    Dim areaTText As String = ""
    Dim principiAttText As String = ""
    Dim brandText As String = ""
    Dim sa As Integer
    Dim ct As Integer
    Dim si As Integer
    Dim selclk As String = ""
    
    Sub page_init()
        If Not String.IsNullOrEmpty(Request("AreaVal")) Then
            areaT = Request("AreaVal")
        End If
                        
        If Not String.IsNullOrEmpty(Request("principioVal")) Then
            principiAtt = Request("principioVal")
        End If             
                        
        If Not String.IsNullOrEmpty(Request("brandVal")) Then
            brand = Request("brandVal")
        End If
        
        If Not String.IsNullOrEmpty(Request("sa")) Then
            sa = Request("sa")
        End If
        
        If Not String.IsNullOrEmpty(Request("ct")) Then
            ct = Request("ct")
        End If
                
        If Not String.IsNullOrEmpty(Request("si")) Then
            si = Request("si")
        End If
              
    End Sub
    
    Sub page_load()
        
        
        Dim cntSrc As New ContentExtraSearcher
        Dim cntsearchx As New ContentExtraSearcher
        Dim stringaContext As String = String.Empty
        Dim _canCheckApproval As Boolean = False
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        
        Dim logicEspr As New LogicExpression(Of ContextIdentificator)
        Dim lOperation1 As New LogicalOperation(Of ContextIdentificator)
        Dim lOperation2 As New LogicalOperation(Of ContextIdentificator)
        Dim lOperation3 As New LogicalOperation(Of ContextIdentificator)
        If Not _canCheckApproval Then cntsearchx.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            
        Dim tmpVal As Integer = 0
        If Not String.IsNullOrEmpty(areaT) AndAlso Integer.TryParse(areaT, tmpVal) Then
            'cntsearchx.KeysContext += tmpVal.ToString() & ","            
            lOperation1.AddItem(New ContextIdentificator(tmpVal))
            lOperation1.LogicOperator = LogicalOperators.ANDOperator
            logicEspr.AddItem(lOperation1)
        End If
        If Not String.IsNullOrEmpty(principiAtt) AndAlso Integer.TryParse(principiAtt, tmpVal) Then
            'cntsearchx.KeysContext += tmpVal.ToString() & ","
            lOperation2.AddItem(New ContextIdentificator(tmpVal))
            lOperation2.LogicOperator = LogicalOperators.ANDOperator
            logicEspr.AddItem(lOperation2)
        End If
        If Not String.IsNullOrEmpty(brand) AndAlso Integer.TryParse(brand, tmpVal) Then
            'cntsearchx.KeysContext += tmpVal.ToString() & ","
            lOperation3.AddItem(New ContextIdentificator(tmpVal))
            lOperation3.LogicOperator = LogicalOperators.ANDOperator
            logicEspr.AddItem(lOperation3)
        End If
            
        ' If cntsearchx.KeysContext <> "" Then cntsearchx.KeysContext = Left(cntsearchx.KeysContext, Len(cntsearchx.KeysContext) - 1)
        logicEspr.LogicOperator = LogicalOperators.ANDOperator
        cntsearchx.LogicExprOfContext = logicEspr
        cntsearchx.key.IdLanguage = 1 'Me.BusinessMasterPageManager.GetLanguage.Id
        cntsearchx.KeySite.Id = si
        cntsearchx.KeySiteArea.Id = sa
        cntsearchx.KeyContentType.Id = ct
      
        cntsearchx.SortType = ContentGenericComparer.SortType.ByData
        cntsearchx.SortOrder = ContentGenericComparer.SortOrder.ASC
        getContent(cntsearchx)
    End Sub
    
    Sub getContent(srcContenuti As ContentExtraSearcher)
        Dim cntscoll As New ContentExtraCollection
        Dim man As New ContentExtraManager
        cntscoll = man.Read(srcContenuti)
        
        If Not cntscoll Is Nothing AndAlso cntscoll.Count > 0 Then
         
            repContenuti.DataSource = cntscoll
            repContenuti.DataBind()
            repContenuti.Visible = True
            NoContent.Visible = False
        Else
            NoContent.Visible = True
            NoContent.Text = Me.BusinessDictionaryManager.Read("Nessun risultato in archivio", Me.PageObjectGetLang.Id, "Nessun risultato in archivio")
            repContenuti.Visible = False
        End If
     
        
    End Sub
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
		Dim imgqual As New Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue
        imgqual.ImageQuality = Healthware.HP3.Core.Utility.ObjectValues.ImageQualityValue.EnumImageQuality.High
		
        Dim res As String = ""
        'res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 205, title, title)
		res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 205, 0, title, title, imgqual)

		'Dim res2 As String = ""
		
        
		
		'res = "<img description='" & title & "' alt='" & title & "' src='/HP3Image/cover/" & Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, 1), 205, 0, imgqual) & "' />"


		'"<img description='" & title & "' alt='" & titlet & "' src='/HP3Image/cover/" & Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, 1), 205, 0, imgqual) & "' />"
		'res = Me.BusinessContentManager.GetCover(New ContentIdentificator(IDcont, 1), 205, 0, imgqual)
		'response.write("<span style='color:white;'><img description='" & title & "' alt='" & title & "' src='/HP3Image/cover/" & res2 & "' /></span>")
		
		
		
		
		'response.write(res2)
		
        Return res
    End Function
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
       
    
    Function getProductContext(ByVal cntx As ContextValue) As ListItem()
       
        Dim soc As New ContextSearcher()
        soc.KeyFather.Id = cntx.Key.Id
        soc.Key.Language.Id = 1 'Me.PageObjectGetLang.Id
        Dim collcx As ContextCollection = Me.BusinessContextManager.Read(soc)
        Dim list1 As New List(Of ListItem)
        If Not collcx Is Nothing And collcx.Count() > 0 Then
            Dim ex As ContextValue
            Dim cont As Integer = 0
            For Each ex In collcx
                Dim newItem As New ListItem()
                newItem.Text = ex.Description
                newItem.Value = ex.Key.Id.ToString()
                newItem.Attributes.Add("value", ex.Key.Id.ToString())
                list1.Insert(cont, newItem)
                cont = cont + 1
            Next
        End If
        
        Return list1.ToArray()
     
    End Function
    
    Function getProductDocument(ByVal cnt As ContentExtraValue) As DownloadCollection
        Dim so As New DownloadSearcher()
        so.key.Id = cnt.Key.Id
        so.key.IdLanguage = 1 'Me.PageObjectGetLang.Id
        Dim collMaterial As DownloadCollection = Me.BusinessContentManager.ReadDownloadRelation(so)
        If Not collMaterial Is Nothing And collMaterial.Count() > 0 Then
            Return collMaterial
        Else
            Return Nothing
        End If
    End Function
</script>


 <%-- ELENCO PRODOTTI --%>       	
                          <asp:Repeater ID="repContenuti" runat="server" EnableViewState="False">
                              <HeaderTemplate>
                                  <div class="archiveRCP">
                              </HeaderTemplate>
                              <ItemTemplate>
                                 <div class="itemRCP">
                                    <h3><%# DataBinder.Eval(Container.DataItem, "title")%> <%#Healthware.Dompe.Helper.Helper.checkApproval(DataBinder.Eval(Container.DataItem, "ApprovalStatus")) %></h3>
                                    <div class="cnt">
                                        <div class="cover"><%#GetImage(DataBinder.Eval(Container.DataItem, "key.id"), DataBinder.Eval(Container.DataItem, "title"))%></div>
                                        <div class="cntTxt">
                                            <p><strong><%=Me.BusinessDictionaryManager.Read("Principio attivo", Me.PageObjectGetLang.Id, "Principio attivo")%></strong>
                                                <br />
												<%# DataBinder.Eval(Container.DataItem, "TitleContentExtra")%>
												<%'#getContextByidContent(DataBinder.Eval(Container.DataItem, "key.id"),Dompe.RelationType.PrincipiAttivi) %>
                                            </p>
                                            <p><strong><%=Me.BusinessDictionaryManager.Read("Area", Me.PageObjectGetLang.Id, "Area")%></strong>
                                            <br /><%# DataBinder.Eval(Container.DataItem, "SubTitle")%>
                                                <%'#getContextByidContent(DataBinder.Eval(Container.DataItem, "key.id"),Dompe.RelationType.AreaTerapeutica) %>
                                            </p>
                                        </div>
                                            <%--DOCUMENTI CORRELATI--%>
											 <asp:Repeater ID="rpRelatedMaterial" runat="server" DataSource='<%#getProductDocument(Container.DataItem)%>'>     
                                                 <HeaderTemplate>
                                                     <div style="clear:both;float:left;">
                                                     <span style="font-weight: bold; padding-left: 25px;">DOWNLOAD</span>
                                                         <ul>
                                                 </HeaderTemplate>
												    <ItemTemplate>
                                                        <li><a class="pdfRCP" title="<%#Container.DataItem.Title%>" href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(Container.DataItem.DownloadTypeCollection(0).KeyContent.PrimaryKey)%>"><%#Container.DataItem.Title%></a></li>																													
												    </ItemTemplate>
                                                 <FooterTemplate>
                                                        </ul></div>
                                                 </FooterTemplate>
											</asp:Repeater>
                                        
                                    </div>
                                </div>
                                </ItemTemplate>
                              <FooterTemplate></div></FooterTemplate>
                          </asp:Repeater>          
            <asp:Literal ID="NoContent" runat="server" Visible="false"></asp:Literal>