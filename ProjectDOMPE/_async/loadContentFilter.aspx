﻿<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.SearchEngine" %>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server" language="VB">
    Private languageId As Integer = 0
    Private defsiteLang As Integer = 0
    Private currentThemeId As Integer = 0
    Private Rolelist As String = ""
    Dim pageSize As Integer = 8
    Private _ltNumRecords As Integer = 0
    Protected _numResult As Integer = 0
    Protected word As String = ""
    Dim pageNum As Integer = 0
    Sub page_init()
        
    End Sub
    
    Sub page_load()
        pageNum = Request("pageNum")
        pageSize = Request("pageSize")
        Dim keyToExclude As Integer = Request("KeyToExclude")
        Rolelist = Request("Rolelist")
        languageId = Request("languageId")
        word = Request("word")
        word = Uri.UnescapeDataString(word)
        '   Response.Write(word)
        DoSearchByWord(word)
    End Sub
    
       
    Sub DoSearchByWord(ByVal word As String)
       
        Dim pos As New PositionValue
        pos.Site = Me.PageObjectGetSite
        pos.Language = Me.PageObjectGetLang
        Dim strRuoli As String = ""
        Dim pageNumber As Integer = 1
     
        Dim so As New ContentSearcher
        so.SearchString = word
        so.key.IdLanguage = languageId
        so.ApprovalStatus = ContentValue.ApprovalWFStatus.Approved
        pageNumber = pageNum
        so.PageNumber = pageNumber
        so.PageSize = pageSize
        
                
        'se non ha il ruolo press escludo la sezione press dalla ricerca
        If Not Rolelist.Contains("{" & Dompe.Role.RoleMediaPress & "}") Then
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PressArchivio))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PressDashboard))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PressHome))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PressPhotoGallery))            
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PressContatti))            
        End If
        
        If Not Rolelist.Contains("{" & Dompe.Role.RoleDoctor & "}") Then
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.IDMedicalArchivioRCPCtype))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.IDMedicalBrandCtype))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.IDMedicalInfoUtiliCtype))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.IDMedicalFocusOn))
        End If
        
        If Not Rolelist.Contains("{" & Dompe.Role.RolePartner & "}") Then
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PartnerAboutUs))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PartnerFocusOn))
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.PartnerMedicalInfo))            
        End If
       
        
        so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Dompe.ContentTypeConstants.Id.DMPDownload))
        so.KeysSiteAreaToExclude.Add(New SiteAreaIdentificator(47))
      
        Dim search As New SearchEngine
        Dim Type As TypeResearch = TypeResearch.OROperator
        Dim coll As ContentCollection = Nothing
     
        If (word.ElementAt(0) = "'" And word.ElementAt(word.Length - 1) = "'") Or (word.ElementAt(0) = """" And word.ElementAt(word.Length - 1) = """") Then
            word = word.Remove(0, 1)
            word = word.Remove(word.Length - 1, 1)
            so.SearchString = word
            Type = TypeResearch.Exact
            coll = search.AdvancedSearch(so, pos, Type, Nothing)
        ElseIf (word.Contains("+")) Then
            '   Response.Write("and")
            Type = TypeResearch.ANDOperator
            coll = search.Search(so, pos, Type)
        Else
            coll = search.Search(so, pos, Type)
        End If
                 
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            _numResult = coll.PagerTotalNumber
            _ltNumRecords = coll.PagerTotalCount
            rptSearchResults.DataSource = coll
            rptSearchResults.DataBind()
        End If
    
    End Sub
   
    Public Shared Function DownloadRelated(ByVal pk As Integer) As String
        Dim res As String = ""
        If pk > 0 Then
            Dim dwncoll As New ContentCollection
            Dim dwnsearch As New ContentSearcher
            Dim soRel As New ContentRelatedSearcher
            Dim coll As ContentCollection = Nothing
            Dim cman As New ContentManager
            ' 
            soRel.Content.Key.PrimaryKey = pk
            soRel.Content.KeyRelationType.Id = 4
            'soRel.Content.RelationSide = RelationTypeSide.Right
            soRel.Content.RelationSide = RelationTypeSide.Left
            soRel.Content.RelationTypeSearcher = RelationTypeSearch.SelectedType
            ' 

            'dwnsearch.KeySiteArea.Id = 94 'Dompe.SiteAreaConstants.IDDownloadArea
            dwnsearch.SortType = ContentGenericComparer.SortType.ByData
            dwnsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
            dwnsearch.KeyContentType.Id = 135
            dwnsearch.RelatedTo = soRel

            dwncoll = cman.Read(dwnsearch)

            If Not dwncoll Is Nothing AndAlso dwncoll.Count > 0 Then
                res = "/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(dwncoll(0).Key.PrimaryKey)
            End If
        End If
        Return res
    End Function
    
    Function GetLink(ByVal content As ContentValue) As String
	
		Dim so As New ThemeSearcher()
        so.KeySiteArea = content.SiteArea.Key
        so.KeyContentType =content.ContentType.Key
        so.KeyLanguage.Id = languageId
        so.KeySite = New SiteIdentificator(46)
        so.LoadHasSon = False
        so.LoadRoles = False
		Dim theme_value As ThemeValue = Helper.GetTheme(so)
            
        'Dim theme_value As ThemeValue = Me.BusinessThemeManager.Read(New SiteIdentificator(46), New SiteAreaIdentificator(content.SiteArea.Key.Id), New ContentTypeIdentificator(content.ContentType.Key.Id))
       
        If Not content Is Nothing AndAlso Not content.ContentType.Key Is Nothing Then
            If content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDSiteareaProfessionisti Then
                Return Me.BusinessMasterPageManager.GetLink(content.Key, New ThemeValue(Dompe.ThemeConstants.IDProfessionisti), False)
            End If
            
            If content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDSiteareaStudenti Then
                Return Me.BusinessMasterPageManager.GetLink(content.Key, New ThemeValue(Dompe.ThemeConstants.IDStudenti), False)
            End If

            If content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDApparatorespiratorio Then
                Return Me.BusinessMasterPageManager.GetLink(New ThemeValue(Dompe.ThemeConstants.idApparatoRespiratorio), False)
            End If
                  
            If content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDSiteareaPosizioniAperte Then
                Return Me.BusinessMasterPageManager.GetLink(New ThemeValue(Dompe.ThemeConstants.IDPosizioniAperte), False)
            End If

            If content.LinkUrl <> "" Then
                Return "javascript:ExternalLinkPopupit('" & content.LinkUrl & "');"
            End If
            
            'se il contenttype è tra quelli che potrebbero avere un download
            If content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDComunicatiStampaAreaMediaPress Or content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDSchedeIstituzionaliMediaPress Or content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDMedicalArea_focuson Or content.SiteArea.Key.Id = Dompe.SiteAreaConstants.IDMedicalArea_ArchivioRCP Then
                Dim linkDwnl As String
                linkDwnl = DownloadRelated(content.Key.PrimaryKey)
                If linkDwnl <> "" Then
                    Return linkDwnl
                Else
                    Return Me.BusinessMasterPageManager.GetLink(content.Key, theme_value, False)
                End If
            End If
			
            If Not theme_value Is Nothing Then
                Return Me.BusinessMasterPageManager.GetLink(content.Key, theme_value, False)
            Else
                Return "#"
            End If
                                   
        End If
        
        Return "#"
    End Function
    
    'taglia il lancio a maxLen caratteri
    Function GetShortLaunch(ByVal launch As String, ByVal maxLen As Integer) As String
        Dim shortLaunch As String = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(launch)
        
        If Len(shortLaunch) > maxLen Then
            If InStr(maxLen, shortLaunch, " ") > 0 Then
                shortLaunch = Left(shortLaunch, InStr(maxLen, shortLaunch, " ")) & "..."
            End If
        End If

        Return shortLaunch
    End Function
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, languageId, defaultLabel)
    End Function
    Function GetLabelNores(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, languageId, defaultLabel).Replace("[label]", word)
    End Function
</script>
<style>
	.searchItem {font-size:20px;margin-bottom: 5px;padding-bottom: 0;}
	.searchItemp {padding-top:0;}
	.searchItem a:link, .searchItem a:active {color:#000;} 
	.searchItem a:hover, .searchItem a:visited {color:#000;text-decoration:none;} 
</style>

   <div class="archiveBox">
   <% If _ltNumRecords = 0 Then%>
    <div id="Div1">
     <div class="numResults">
     <p><%= GetLabelNores("DMP_InitNoResult", "DMP_InitNoResult")%></p>
    </div>
    </div>
   <% Else%>
    <div id="literatureArchive">
      <%If pageNum = 1 Then%>
        <div class="numResults">
            <p><b> <%= _ltNumRecords%> </b> <%= GetLabel("DMP_results_for", "DMP_results_for")%> <b><i><%= word%></i></b></p>
        </div>
      <%End If%> 
	  <asp:Repeater ID="rptSearchResults" runat="server"  EnableViewState="false">
		<ItemTemplate> 
			<div class="archiveCnt">
				<div class="cnt">
					<h3 class="searchItem"><a href="<%#GetLink(Container.DataItem) %>"><%#DataBinder.Eval(Container.DataItem, "Title")%></a> </h3>
					<p class="searchItemp"><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>
				</div>
				<%-- 
				<div class="tools">
					<a href="<%#GetLink(Container.DataItem) %>"> <%= Me.BusinessDictionaryManager.Read("DMP_Leggi", languageId, "DMP_Leggi")%></a>
				</div>
				--%>
			</div>
		</ItemTemplate>
	</asp:Repeater>
      <%If _ltNumRecords > 0 And pageNum < _numResult Then%>
        <div id="moreInfoBar" class="bar" style="clear: both; width: 975px; margin: 0 auto;">
            <div class="bar2" style="width:100%; margin:0; background-color:#ECECEC; clear:both; height:35px; float:left;">
            <a onclick="AddMoreContent('<%= pageNum +1 %>');$('.bar').hide();" href="javascript:void(0);" id="moreVideo" style="text-align: center; padding-top: 10px; float: left; clear: both; width: 100%; color: #333333; font-weight: bold;">
			<span><%=Me.BusinessDictionaryManager.Read("DMP_MORE_INFO", languageId, "Altri Risultati")%></span></a>
            </div>
        </div>	  

		
       <%End If%>    
          </div>
   <% End If%>
     </div>       