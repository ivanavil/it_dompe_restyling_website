<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.SearchEngine" %>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>

<script runat="server" language="VB">
	Sub Page_Init
		If (Request.ServerVariables("HTTP_X_REQUESTED_WITH") Is Nothing Or Request.ServerVariables("HTTP_X_REQUESTED_WITH") <> "XMLHttpRequest") Then
            Response.End()
        End If
	End Sub

	Sub Page_Load
		Dim reqType As String = Request("type")
		Dim reqLang As Integer = Request("lang")
		Dim _cacheKey As String = String.Empty
		
        If reqType.ToLower() = "header" Then _cacheKey = "cacheKeyMenu:46|" & reqLang & "|191"
		If reqType.ToLower() = "footer" Then _cacheKey = "cacheKey:Footerlinks_" & reqLang
		If reqType.ToLower() = "lgt" Then doLogOut(reqLang)
		
		If reqType.ToLower() = "setpopup1" Then setpopup1(reqLang)
		If reqType.ToLower() = "setpopup2" Then setpopup2(reqLang)
		
		Response.Write(CacheManager.Read(_cacheKey, 1))
	End Sub
	
	Sub doLogOut(ByVal _reqLang as String)
		Dim _uriBuilder = new UriBuilder(Request.Url.Scheme, Request.Url.Host, Request.Url.Port)
		Dim _strLang as String = "/"
		If _reqLang = 2 Then _strLang = _strLang & "/en/"
		If _reqLang = 3 Then _strLang = _strLang & "/shq/"
		If _reqLang = 3 Then _strLang = _strLang & "/en/"
		Try
			Me.BusinessAccessService.Logout()
		Catch
			Response.Write("ko")
		End Try
		Response.Write("ok")
	End Sub
	
	Sub setpopup1(ByVal _reqLang as Integer)
		Try
			Response.Write(Me.BusinessDictionaryManager.Read("PLY_TitleBoxLinkExternal", _reqLang, "PLY_TitleBoxLinkExternal"))
		Catch
			Response.Write("ko")
		End Try
	End Sub
	
	Sub setpopup2(ByVal _reqLang as Integer)
		Try
			Response.Write(Me.BusinessDictionaryManager.Read("PLY_MessageBoxLinkExternal", _reqLang, "PLY_MessageBoxLinkExternal"))
		Catch
			Response.Write("ko")
		End Try
	End Sub	
</script>