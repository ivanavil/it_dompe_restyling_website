﻿<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Register TagPrefix="HP3" TagName="Captcha" Src="~/HP3Common/ctlCaptcha.ascx" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>


<script runat="server" language="VB">
    Private languageId As Integer = 0
    Private defsiteLang As Integer = 0
    Private currentThemeId As Integer = 0
    Private _siteFolder As String = ""
    Private _strLink As String = ""
    Private captchaCode As String
    Sub page_init()
        
    End Sub
    
    Sub page_load()
        _siteFolder = Me.ObjectSiteFolder
    
        If Not String.IsNullOrEmpty(Request.QueryString("Link")) Then
            _strLink = Request.QueryString("Link")
            TxtLink.Text = _strLink
        End If
       
        CaptchaInfo()
    End Sub
    
    Sub CaptchaInfo()
        If Not Page.IsPostBack Then
            captchaCode = ImageUtility.GetCaptchaCode()
            IDCaptcha.Code = captchaCode
            HiddenCaptcha.Text = captchaCode
        Else
            'IDCaptcha.Code = ViewState("CaptchaValue.Value")
        End If
        IDCaptcha.Code = HiddenCaptcha.Text
        IDCaptcha.Width = "120"
        IDCaptcha.Height = "30"
        IDCaptcha.ForeColor = Drawing.Color.LightGray
        IDCaptcha.BackColor = Drawing.Color.White
        IDCaptcha.TextBackColor = Drawing.Color.DarkSlateGray
        IDCaptcha.TextForeColor = Drawing.Color.DarkSlateGray
        VerifyCaptcha.Text = ImageUtility.DecryptCaptchaCode(HiddenCaptcha.Text)
    End Sub
    
    Function GetLink(ByVal video As ContentValue) As String
        If Not video Is Nothing AndAlso video.Key.Id > 0 Then
            
            Dim theme As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(currentThemeId))
                       
            If Not theme Is Nothing AndAlso theme.Key.Id > 0 Then
                'theme.KeyContent = video.Key 'BUG
            
                Return Me.BusinessMasterPageManager.GetLink(video.Key, theme, False)
            End If
        End If
           
        
        Return String.Empty
    End Function
    
    'taglia il lancio a maxLen caratteri
    Function GetShortLaunch(ByVal launch As String, ByVal maxLen As Integer) As String
        Dim shortLaunch As String = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(launch)
        
        If Len(shortLaunch) > maxLen Then
            If InStr(maxLen, shortLaunch, " ") > 0 Then
                shortLaunch = Left(shortLaunch, InStr(maxLen, shortLaunch, " ")) & "..."
            End If
        End If

        Return shortLaunch
    End Function
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, languageId, defaultLabel)
    End Function
    
    Sub SentMessage(ByVal s As Object, ByVal e As EventArgs)
        Dim oMailTemplateValue As MailTemplateValue = Nothing
            
        oMailTemplateValue = getMailTemplate("SendLink")
        
        Dim res As Boolean = SendContactMail(oMailTemplateValue)
        
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ chiudi();})", True)
       
        
    End Sub
    
    Function SendContactMail(ByVal oMailTemaplate As MailTemplateValue) As Boolean
        Try
            Dim mailValue As New Healthware.HP3.Core.Utility.ObjectValues.MailValue
           
            If (Not oMailTemaplate Is Nothing) Then
               
                mailValue.MailTo.Add(New MailAddress(TxtMailDest.Text))
                mailValue.MailFrom = New MailAddress(TxtMailMitt.Text, TextNomeMitt.Text & " " & TxtCognomeMitt.Text)
            
                mailValue.MailBody = ReplaceBodyInfo(oMailTemaplate.Body)
                mailValue.MailSubject = oMailTemaplate.Subject
                mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
               
                If (oMailTemaplate.BccRecipients <> String.Empty) Then
                    mailValue.MailBcc.Add(New MailAddress(oMailTemaplate.BccRecipients))
                End If
                    
                If (oMailTemaplate.FormatType = 1) Then
                    mailValue.MailIsBodyHtml = True
                Else
                    mailValue.MailIsBodyHtml = False
                End If
              
                'invio della mail
                Dim systemUtility As New SystemUtility()
                systemUtility.SendMail(mailValue)
                
                Return True
            End If
                 
              
        Catch ex As Exception
            Return False
            Throw New BaseException("Dompe Dac - Mail Share Link Error : ", ex)
        End Try
              
        Return False
    End Function
  
    Function ReplaceBodyInfo(ByVal body As String) As String
        Dim replecedBody As String = String.Empty
        Try
            If (Not String.IsNullOrEmpty(TxtMailDest.Text)) Then
                replecedBody = body.Replace("[LINK]", TxtLink.Text).Replace("[myname]", TextNomeMitt.Text).Replace("[mysurname]", TxtCognomeMitt.Text).Replace("[message]", tbMessage.Text)
            End If
        Catch ex As Exception
            Response.Write(ex.Message) : Response.End()
        End Try
            
        Return replecedBody
    End Function
    
    Function getMailTemplate(ByVal templateName As String) As MailTemplateValue
        Dim omailTemplateSearcher As New MailTemplateSearcher
        Dim omailTemplateValue As MailTemplateValue
        
        omailTemplateSearcher.KeySite = Me.PageObjectGetSite
        omailTemplateSearcher.Key.MailTemplateName = templateName
                     
        Dim coll As MailTemplateCollection = Me.BusinessMailTemplateManager.Read(omailTemplateSearcher)
       
        If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
            omailTemplateValue = coll(0)
            Return omailTemplateValue
        End If
                      
        Return Nothing
    End Function
    
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>  
    <meta name="google-site-verification" content="LT0kjCXld7kByc_qajTa2bc7nnRmCddgUwOwNE4zVFs" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
    <meta name="metadescription" content='<asp:literal ID="ltlMetaDescription" EnableViewState="false" runat="server" />' />
    <meta name="metakeywords" content='<asp:literal ID="ltlMetaKeywords" EnableViewState="false" runat="server" />' />  
   
    <title>
            <asp:Literal ID="ltlPageTitle" runat="server"></asp:Literal>
    </title>

    <script language="JavaScript" type="text/JavaScript" src="/<%=_siteFolder %>/_js/jquery-1.8.0.min.js"></script>	
     <script language="JavaScript" type="text/JavaScript" src="/<%=_siteFolder %>/_js/jquery-ui-1.8.23.custom.min.js"></script> 
        
    <link type="text/css" rel="stylesheet" href="/<%=_siteFolder %>/_css/style.css" media="screen"/>
     <link rel="stylesheet" title="Standard" type="text/css" media="print" href="/<%=_siteFolder %>/_css/print.css" /> 
     <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
  
    <link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.gif" />
    <link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.gif" />
  
  <style>
 .styled-select{
    height: 26px;
    overflow: hidden;
	clear:both;
	float:left;
    width: 240px;
	z-index:100;
	margin:0 0 0 0;
	background: url("/ProjectDOMPE/_slice/bgSelect.gif") no-repeat scroll right center ;
	border:1px solid #999999;
}
.styled-select select{
    background: none repeat scroll 0 0 transparent;
    border: 0 none;
    border-radius: 0 0 0 0;
    font-size: 14px;
    height: 26px;
    line-height: 1;
	color:#999999;
	-webkit-appearance: none;
    padding: 0 ;
    width: 268px;
}
  
  </style>
  <script type="text/javascript">
      function chiudi() {
          self.close()
      }
  </script>
 </head>
   
<body>
       <form id="myForm" runat="server" enctype="multipart/form-data">
                <div class="popupFields">
    
    	     <h2 style="color:#ffffff;"><%= Me.BusinessDictionaryManager.Read("PLY_DESTINATARIO", 2, "PLY_DESTINATARIO")%></h2>
           
           <%--	 <div class="rowContactFields">
                  <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Name", 2, "PLY_Name")%>   :</div>
                  <div class="field">
                        <asp:TextBox  ID="TextNomeDest" Width="250px"  runat="server" ValidationGroup="MailGroup" EnableViewState ="false" MaxLength="40" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="TextNomeDest" Display="Dynamic" runat="server" ValidationGroup ="MailGroup" EnableViewState ="false" />
                  </div> 
             </div>
           
              	 <div class="rowContactFields">
                      <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Surname", 2, "PLY_Surname")%>: </div>
                      <div class="field">	
                        <asp:TextBox  ID="TxtCognomeDest" Width="250px"  runat="server" ValidationGroup="MailGroup" EnableViewState ="false" MaxLength="40" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ControlToValidate="TxtCognomeDest" Display="Dynamic" runat="server" ValidationGroup ="MailGroup" EnableViewState ="false" />
                        </div>
                    </div>
--%>
                <div class="rowContactFields">
                     <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Link", 2, "PLY_Link")%>: </div>
                      <div class="field"> <asp:TextBox  ID="TxtLink" Width="400px"  runat="server" ValidationGroup="MailGroup" EnableViewState ="false" MaxLength="40" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  ControlToValidate="TxtLink" Display="Dynamic" runat="server" ValidationGroup ="MailGroup" EnableViewState ="false" />
                        </div>
                    </div>

              <div class="rowContactFields">
                        <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Email", 2, "PLY_Email")%> : </div>
                    	<div class="field">
                            <asp:TextBox ID="TxtMailDest" runat="server" Width="250px" EnableViewState ="false" ValidationGroup="logGroup" MaxLength="60" />
                            <asp:RequiredFieldValidator ID="reqLog" ErrorMessage="Required" ControlToValidate="TxtMailDest" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup="MailGroup" EnableViewState ="false" />
                            <asp:RegularExpressionValidator ID="regexEmail" ErrorMessage="Incorrect Format" runat="server" Display="dynamic" ControlToValidate="TxtMailDest" ValidationGroup="MailGroup" EnableViewState ="false"
                          ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}"/>
                        </div>
                    </div>
                <div class="rowContactFields">
                        <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Message", 2, "PLY_Message")%> : </div>
                        <div class="textArea">
                            <asp:TextBox  ID="tbMessage" Width="539" Rows="5" runat="server" EnableViewState ="false" TextMode ="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMessage" ControlToValidate="tbMessage" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup ="contGroup" EnableViewState ="false" />
                            
                            <asp:RegularExpressionValidator ID="regexMessage" runat="server" ControlToValidate="tbMessage"  ValidationGroup ="contGroup"
                               ValidationExpression="[\s\S]{1,400}"></asp:RegularExpressionValidator>
                            
                        </div>
                </div>

             <h2 style="color:#ffffff;"><%= Me.BusinessDictionaryManager.Read("PLY_MITTENTE", 2, "PLY_MITTENTE")%></h2>     
                	
                	<div class="rowContactFields">
                        <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Name", 2, "PLY_Name")%>: </div>
                        <div class="field">
                        	    <asp:TextBox  ID="TextNomeMitt" Width="250px"  runat="server" ValidationGroup="MailGroup" EnableViewState ="false" MaxLength="40" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  ControlToValidate="TextNomeMitt" Display="Dynamic" runat="server" ValidationGroup ="MailGroup" EnableViewState ="false" />
                        </div>
                    </div>
               
                <div class="rowContactFields">
                     <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Surname", 2, "PLY_Surname")%>:</div>
                    	<div class="field">
                            <asp:TextBox  ID="TxtCognomeMitt" Width="250px"  runat="server" ValidationGroup="MailGroup" EnableViewState ="false" MaxLength="40" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  ControlToValidate="TxtCognomeMitt" Display="Dynamic" runat="server" ValidationGroup ="MailGroup" EnableViewState ="false" />
                        </div>
                    </div>

                     <div class="rowContactFields">
                       <div class="label"><%= Me.BusinessDictionaryManager.Read("PLY_Email", 2, "PLY_Email")%> : </div>
                    	<div class="field"><asp:TextBox ID="TxtMailMitt" runat="server" Width="250px" EnableViewState ="false" ValidationGroup="logGroup" MaxLength="60" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Required" ControlToValidate="TxtMailMitt" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup="MailGroup" EnableViewState ="false" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ErrorMessage="Incorrect Format" runat="server" Display="dynamic" ControlToValidate="TxtMailMitt" ValidationGroup="MailGroup" EnableViewState ="false"
                          ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}"/>
                          </div>
                    </div>
                      <div class="rowContactFields">
                    	<div class="fieldContactCaptcha ">
                <div class="label">
                 <%= Me.BusinessDictionaryManager.Read("Code", Me.BusinessMasterPageManager.GetLang.Id, "Code")%>*
                 </div>
                    <asp:placeholder ID="CaptchaPanel" runat="server">
                    
                    <HP3:Captcha ID="IDCaptcha" runat="server" />
                                           
                    <asp:TextBox ID="InsertCaptcha" runat="server" CssClass="captchainput" alt="Captcha code" MaxLength="12"></asp:TextBox>
                    <asp:TextBox ID="HiddenCaptcha" Visible="false" runat="server" />
                    <div style="display: none"> <asp:TextBox ID="VerifyCaptcha" runat="server" /></div>
                    <asp:CompareValidator id="CompareValidator1" style="clear:both;float:left;width:auto;margin: 5px 0 0 62px;" ValidationGroup ="contGroup"
                        ControlToCompare="InsertCaptcha" Operator="Equal" ControlToValidate ="VerifyCaptcha" ForeColor="red" Type="String" 
                        runat="server" EnableViewState ="false" ErrorMessage ="The text entered does not match" />
                       
                        </asp:placeholder>
        	            
                </div><!--end sideL-->
                </div>
               <div class="row">
                <div class="btnGen">
                      <asp:LinkButton ID="EmailGrp" ValidationGroup="MailGroup" OnClick="SentMessage" runat="server"> Send </asp:LinkButton> 
               </div>          
                 


           
       
</div>
</div>
 
       </form>
            
   </body>

</html>