<%@ Page Language="VB" ClassName="Efpia2015" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<script runat="server">
    Sub Page_Load()
        If CheckBrowserCaps() Then Response.End()
    End Sub
    
    Function CheckBrowserCaps() As Boolean
        Dim _strUserAgent As String = Request.ServerVariables("HTTP_USER_AGENT").ToLower
        If _strUserAgent.Contains("googlebot") Then Return True
        If _strUserAgent.Contains("yahoocrawler") Then Return True
        If _strUserAgent.Contains("msnbot") Then Return True
        If _strUserAgent.Contains("slurp") Then Return True
        If _strUserAgent.Contains("altavista") Then Return True
		
        Return False
    End Function
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>www.dompe.com | Trasferimento di valori</title>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/jquery.js"></script>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/jquery.tablesorter.widgets.js"></script>
	<script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/ui/jquery-ui.js.axd"></script>
	<script type="text/javascript" src="http://www.dompe.com/ProjectDOMPE/_js/efpia.js"></script>
<script>
		$(function () {
			initializeBtns(2015);
		});
		//HCP
		var _filterHcpOn = 0;

		var _prependHtmlHCP = "<tr style='background-color: #E6E3DF;' class='headerData odd'><td colspan='14' data-sorter='false' style='text-align: center;padding:10px 0;'><strong>INDIVIDUAL NAMED DISCLOSURE</strong> - one line per HCP (i.e. all transfers of value during a year for an individual HCP will be summed up: itemization should be available for the individual Recipient or public authorities' consultation only, as appropriate)<br /><br /><span style='color:red;'><strong>DATI SU BASE INDIVIDUALE</strong> - una riga per ciascun operatore sanitario (ossia sar� indicato l'importo complessivo di tutti i trasferimenti di valore effettuati nell�arco dell�anno a favore di ciascun operatore sanitario: il dettaglio sar� reso disponibile solo per il singolo Destinatario o per le Autorit� competenti, su richiesta)</span></td></tr>"

		var _appendHtmlHCP = "<tr class='eofData even'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI  ? nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData odd'> <td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>N/A</td><td>N/A</td><td>59.144,29</td><td>217.483,29</td><td>18.000</td><td>0</td><td>&nbsp;</td><td>294.627,58</td></tr><tr class='eofData even'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata ?  Punto 5.5 CD</span></td><td>N/A</td><td>N/A</td><td>87</td><td>210</td><td>4</td><td>0</td><td>&nbsp;</td><td>301</td></tr><tr class='eofData odd'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari - Punto 5.5 CD </span></em></td><td>N/A</td><td>N/A</td><td>39,55</td><td>50,72</td><td>57,14</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>";
		
        $(function () {
            $(".tablesorter1").tablesorter({
                theme: 'blue',
                widgets: ['zebra', 'filter']
            }).bind('filterStart filterEnd', function (e, filter) {
                if (e.type === 'filterStart') {
					$('.tablesorter1 tr.eofData').remove();
					$('.tablesorter1 tr.headerData').remove();
					
                } else if (e.type === 'filterEnd') {
					//value of search input box
					if ( $('.tablesorter1 input.tablesorter-filter[data-column="1"]').val().length == 0 ) {
						_filterHcpOn = 0;
						$(".tablesorter1").prepend(_prependHtmlHCP).append(_appendHtmlHCP);
					} else {
						_filterHcpOn = 1;
						if ($('.tablesorter1 tbody tr:visible[role="row"]').length >= 1) {
							$(".tablesorter1").prepend(_prependHtmlHCP);
						} else {
							$(".tablesorter1").append(_appendHtmlHCP);
						}
					}
                } else {
                    return;
                }
            }).bind("sortStart sortEnd",function(e, t){
				if (e.type === 'sortStart') {
					$('.tablesorter1 tr.eofData').remove();
					$('.tablesorter1 tr.headerData').remove();
				}
				else { //sortEnd
					if (_filterHcpOn == 1)  {
						if ($('.tablesorter1 tbody tr:visible[role="row"]').length >= 1) {
							//no eof
							$(".tablesorter1").prepend(_prependHtmlHCP);
						} else {
							//eof
							$(".tablesorter1").append(_appendHtmlHCP);
						}
					}
					else {
						$(".tablesorter1").prepend(_prependHtmlHCP).append(_appendHtmlHCP);
					}
				}
			});
			//END HCP
			
			//HCO
			
		var _filterHcoOn = 0;

		var _prependHtmlHCO = "<tr style='background-color: #E6E3DF;' class='headerData odd'><td colspan='14' data-sorter='false' style='text-align: center;padding:10px 0;'><strong>INDIVIDUAL NAMED DISCLOSURE</strong> - one line per HCO (i.e. all transfers of value during a year for an individual HCO will be summed up: itemization should be available for the individual Recipient or public authorities' consultation only, as appropriate)<br /><br /><span style='color:red;'><strong>DATI SU BASE INDIVIDUALE</strong> - una riga per organizzazione sanitaria (ossia saranno indicati in aggregato tutti i trasferimenti di valore effettuati nell�arco dell�anno a favore di ciascuna organizzazione sanitaria: il dettaglio sar� reso disponibile solo per la  singola Organizzazione sanitaria o per le autorit� competenti) </span></td></tr>"

		var _appendHtmlHCO = "<tr class='eofData'><td colspan='14' data-sorter='false' style='text-align: center;'>OTHER, NOT INCLUDED ABOVE - where information cannot be disclosed on an individual basis for legal reasons<br /><br /><span style='color:red;'>ALTRO, NON INCLUSO NELLE VOCI PRECEDENTI � nei casi in cui i dati non possono essere pubblicati su base individuale per motivi normativi (privacy)</span></td></tr><tr class='eofData'><td colspan='6'>Aggregate amount attributable to transfers of value to such Recipients -- Art. 3.02 CE<br /><br /><span style='color:red;'>Dato aggregato attribuibile a trasferimenti di valore a tali Destinatari - Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'>Number of Recipients in aggregate disclosure - Art. 3.02 CE<br /><br /><span style='color:red;'>Numero dei Destinatari i cui dati sono pubblicati in forma aggregata �  Punto 5.5 CD</span></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>0</td></tr><tr class='eofData'><td colspan='6'><em>% of the number of Recipients included in the aggregate disclosure in the total number of Recipients disclosed - Art. 3.02 CE<br /><br /><span style='color:red;'>% del numero di Destinatari inclusi nel dato aggregato sul numero complessivo dei Destinatari  �  Punto 5.5 CD </span></em></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>&nbsp;</td><td>N/A</td></tr>"
		
		
            $(".tablesorter3").tablesorter({
                theme: 'blue',
                widgets: ['zebra', 'filter']
            }).bind('filterStart filterEnd', function (e, filter) {
                if (e.type === 'filterStart') {
					$('.tablesorter3 tr.eofData').remove();
					$('.tablesorter3 tr.headerData').remove();
					
                } else if (e.type === 'filterEnd') {
					//value of search input box
					if ( $('.tablesorter3 input.tablesorter-filter[data-column="1"]').val().length == 0 ) {
						_filterHcoOn = 0;
						$(".tablesorter3").prepend(_prependHtmlHCO).append(_appendHtmlHCO);
					} else {
						_filterHcoOn = 1;
						if ($('.tablesorter3 tbody tr:visible[role="row"]').length >= 1) {
							$(".tablesorter3").prepend(_prependHtmlHCO);
						} else {
							$(".tablesorter3").append(_appendHtmlHCO);
						}
					}
                } else {
                    return;
                }
            }).bind("sortStart sortEnd",function(e, t){
				if (e.type === 'sortStart') {
					$('.tablesorter3 tr.eofData').remove();
					$('.tablesorter3 tr.headerData').remove();
				}
				else { //sortEnd
					if (_filterHcoOn == 1)  {
						if ($('.tablesorter3 tbody tr:visible[role="row"]').length >= 1) {
							//no eof
							$(".tablesorter3").prepend(_prependHtmlHCO);
						} else {
							//eof
							$(".tablesorter3").append(_appendHtmlHCO);
						}
					}
					else {
						$(".tablesorter3").prepend(_prependHtmlHCO).append(_appendHtmlHCO);
					}
				}
			});

			/*
            $(".tablesorter3").tablesorter({
                theme: 'blue',
                widgets: ['zebra', 'filter']
            }).bind('filterStart filterEnd', function (e, filter) {
                if (e.type === 'filterStart') {
                } else if (e.type === 'filterEnd') {
					$('.tablesorter3 tr.eofData').hide();
                    if ($('.tablesorter3 input.tablesorter-filter[data-column="1"]').val().length == 0) {
                        $('.tablesorter3 tr.eofData').show();
                    } else {
                        if ($('.tablesorter3 tbody tr:visible').length >= 1) {
                            $('.tablesorter3 tr.eofData').hide();
                        } else {
                           $('.tablesorter3 tr.eofData').show();
                        }
                    }
                } else {
                    return;
                }
            });
			*/
			//END HCO
			
			
			//R&D
            $(".tablesorter4").tablesorter({
                theme: 'blue',
                widgets: ['zebra']
            });
			//END R&D
        });
    </script>

	<link rel="stylesheet" href="http://www.dompe.com/ProjectDOMPE/_js/css/jquery-ui.css" />
    <link rel="stylesheet" href="http://www.dompe.com/projectdompe/_css/style.css" />
    <link rel="stylesheet" href="http://www.dompe.com/ProjectDOMPE/_js/tablesorter/css/theme.blue.css" />
	<link rel="stylesheet" href="http://www.dompe.com/projectdompe/_css/efpia.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a href="http://www.dompe.com" title="Domp� Corporate website - Home Page">
                <img alt="Domp� Corporate website - Home Page" src="http://www.dompe.com/projectdompe/_slice/logo.gif" /></a>
        </div>
		<div class="widget">
			<a class="ui-button ui-widget ui-corner-all" id="btn2016" href="/projectdompe/efpia.aspx">Trasferimenti di valore 2016</a>
			<a class="ui-button ui-widget ui-corner-all" id="btn2015" href="/projectdompe/efpia_2015.aspx">Trasferimenti di valore 2015</a>
		</div>
        
	<div class="blockwi">
		<table class="tablesorter1">
            <thead>
                <tr>
                    <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2015<br />Pubblished on 30 June 2016</td>
                </tr>
                <tr>
                    <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                    <th valign="top" style="padding-top:10px;">
						Full Name<br /><br />
						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
						<span style="color:red;">
							<strong>Operatori sanitari</strong>: Citt�  dove si svolge  prevalentemente la professione<br />
							<strong>Organizzazioni sanitarie</strong>: Sede Legale
						</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Country of Principal Practice<br /><br />
						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit�</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Principal Practice Address<br /><br />
						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit�</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Unique country identifier<br /><br />
						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Donations and Grants to HCOs<br /><br />
						<span style="color:red;">Donazioni e contributi a organizzazioni sanitarie</span>
					</th>
                    <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche)
( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
					</th>
                    <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
						Fee for service and consultancy<br /><br />
						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
					</th>
                    <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						TOTAL OPTIONAL<br /><br />
						<span style="color:red;">TOTALE Facoltativo</span>
					</th>
                </tr>
                <tr>
					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3 CE)<br /><br />
						<span style="color:red;">da Punto 5,5 a 5.7 CD</span>
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Modulo 1CE)<br /><br />
						<span style="color:red;">Allegato 2(CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3 CE)<br /><br />
						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3CE)<br /><br />
						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3.01.1.a CE)<br /><br />
						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
					</th>
					
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
						<span style="color:red;">Accordi di sponsorizzazione con organizzazioni sanitarie/soggetti terzi nominati da organizzazioni sanitarie per la realizzazione di eventi</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Registration Fees<br /><br />
						<span style="color:red;">Quote di iscrizione</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Travel & Accomodation<br /><br />
						<span style="color:red;">Viaggi e  ospitalit�</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Fees<br /><br />
						<span style="color:red;">Corrispettivi</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
						<span style="color:red;">Spese riferibili ad attivit� di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit�</span>
					</th>
					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>

<tr>
  <td>HCP</td>
  <td>ABBATE VITO</td>
  <td>Partinico</td>
  <td>IT</td>
  <td>V.LE P.SSA
  ELENA 71</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>AFSHAR
  ZAHRA</td>
  <td>Osimo</td>
  <td>IT</td>
  <td>VIA PARINI 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>AGRESTI
  LEONARDA</td>
  <td>Bolognetta</td>
  <td>IT</td>
  <td>VIA ROMA 71</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ALBAN
  ORFEO</td>
  <td>Teolo</td>
  <td>IT</td>
  <td>VIA DALLA CHIESA 23</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>794,90</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>794,90</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ANGELINI
  CATERINA</td>
  <td>Albignasego</td>
  <td>IT</td>
  <td>VIA C. COLOMBO 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>707,50</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>707,50</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ARANCIO
  ROSA SANTA</td>
  <td>Aci
  Castello</td>
  <td>IT</td>
  <td>VIA FIRENZE 58</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ARDITA
  ANGIOLA MARIA</td>
  <td>Lentini</td>
  <td>IT</td>
  <td>VIA GRAMSCI 12</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>650,00</td>
  <td>350,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ATTINA'
  ANTONINO</td>
  <td>Crotone</td>
  <td>IT</td>
  <td>VIA XXV APRILE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BACCA
  DINO</td>
  <td>Genova</td>
  <td>IT</td>
  <td>VIA D'ALBERTIS 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BAIOCCHI
  SILVIA</td>
  <td>Pavia</td>
  <td>IT</td>
  <td>VIA SALVATORE MAUGERI 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>426,32</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>426,32</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BALDASSINI
  SIMONETTA</td>
  <td>Prato</td>
  <td>IT</td>
  <td>VIA GHERADDI 16</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>650,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>650,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BARBANO
  LUCA</td>
  <td>Lumezzane</td>
  <td>IT</td>
  <td>VIA MAZZINI 122</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>843,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.540,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BARBERIS
  FRANCA</td>
  <td>Alessandria</td>
  <td>IT</td>
  <td>C.SO
  ACQUI 157</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BARISON
  ANNA</td>
  <td>Conselve</td>
  <td>IT</td>
  <td>VIA PUCCINI 14</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>331,13</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>331,13</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BASCHIERI
  STEFANO</td>
  <td>Bernareggio</td>
  <td>IT</td>
  <td>VIA KENNEDI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>220,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>220,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BATTILANA
  MICHELE</td>
  <td>Budrio</td>
  <td>IT</td>
  <td>VIA BENNI 44</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BAVETTA
  ANDREA</td>
  <td>Carini</td>
  <td>IT</td>
  <td>VIA PONTICELLI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BELLUOMINI
  ROBERTO</td>
  <td>Capannori</td>
  <td>IT</td>
  <td>VIA DI TIGLIO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BELLUOMO
  GIUSEPPE</td>
  <td>Militello
  in Val di Catania</td>
  <td>IT</td>
  <td>PIAZZA
  S.AGATA 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BELLUZZO
  CALOGERO</td>
  <td>Agrigento</td>
  <td>IT</td>
  <td>VIA GIOENI 75</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BELTRAME
  FEDERICO</td>
  <td>Verona</td>
  <td>IT</td>
  <td>P.LE
  STEFANI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>265,30</td>
  <td>170,47</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>435,77</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BENASSI
  FULVIO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA PORTUENSE 332</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>792,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>792,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BENINCASA
  ANTONIO</td>
  <td>Aversa</td>
  <td>IT</td>
  <td>VIA GRAMSCI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>152,88</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>152,88</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BERTOCCO
  ELISABETTA</td>
  <td>Lonigo</td>
  <td>IT</td>
  <td>VIA SISANA 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>1.319,44</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.319,44</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BEZZI
  MICHELA</td>
  <td>Brescia</td>
  <td>IT</td>
  <td>P.LE
  SPEDALI CIVILI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>220,18</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>220,18</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BIANCO
  DANIELA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>P.ZZA
  VITTORIO EMANUELE II 39</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>592,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BINDI
  LUCIA</td>
  <td>Montevarchi</td>
  <td>IT</td>
  <td>VIA CENNANO 80</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>421,42</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>421,42</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BO
  ROBERTO</td>
  <td>Giarole</td>
  <td>IT</td>
  <td>VIA OCCIMIANO 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>167,80</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>167,80</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BOERI
  PATRIZIA</td>
  <td>Sant'Angelo
  Lodigiano</td>
  <td>IT</td>
  <td>VIA UMBERTO PRIMO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>505,02</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>505,02</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BOI
  MARIA GRAZIA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>V.LE
  MICHELANGELO 41</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>1.758,45</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.108,45</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BOMBI
  CLAUDIO</td>
  <td>Bologna</td>
  <td>IT</td>
  <td>VIA CASTIGLIONE 45</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>272,32</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>272,32</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BONAIUTO
  M. GRAZIA</td>
  <td>Noto</td>
  <td>IT</td>
  <td>VIA TRIGONA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>1.490,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.490,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BONANZINGA
  SALVATORE</td>
  <td>Giarre</td>
  <td>IT</td>
  <td>V.LE
  DON MINZONI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>1.490,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.490,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BORGHESAN
  AGOSTINO</td>
  <td>Montagnana</td>
  <td>IT</td>
  <td>VIA CESARE BATTISTI 28</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BORGOGNO
  FULVIO</td>
  <td>Sanremo</td>
  <td>IT</td>
  <td>VIA CAVALLOTTI 4</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BORTOLI
  MICHELA</td>
  <td>Lonigo</td>
  <td>IT</td>
  <td>VIA SISANA 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>270,90</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>270,90</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BOSSONE
  VITO</td>
  <td>Montagnana</td>
  <td>IT</td>
  <td>VIA MATTEOTTI 77</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>58,68</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>58,68</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BRAIDO
  FULVIO</td>
  <td>Genova</td>
  <td>IT</td>
  <td>L.GO
  ROSANNA BENZI 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>1.532,26</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.532,26</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BRUSASCO
  VITO</td>
  <td>Genova</td>
  <td>IT</td>
  <td>L.GO
  ROSANNA BENZI 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>43,08</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>43,08</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BRUZZESE
  DOMENICO</td>
  <td>Roccella
  Ionica</td>
  <td>IT</td>
  <td>VIA UMBERTO PRIMO 89</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BUGEA
  SALVATORE</td>
  <td>Porto
  Empedocle</td>
  <td>IT</td>
  <td>VIA NAPOLI 33</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BUGGIO
  GIUSEPPE</td>
  <td>Camposampiero</td>
  <td>IT</td>
  <td>C/O
  OSPEDALE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>472,68</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>472,68</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>BURGIO
  ANDREA</td>
  <td>Porto
  Empedocle</td>
  <td>IT</td>
  <td>VIA TRENTO 14</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CACIAGLI
  LUCIANO</td>
  <td>Montopoli
  in Val d'Arno</td>
  <td>IT</td>
  <td>VIA MARTIRI LIBERTA 14</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CACICI
  GIUSEPPE</td>
  <td>Verona</td>
  <td>IT</td>
  <td>P.LE
  STEFANI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>265,30</td>
  <td>210,12</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>475,42</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAGGIANO
  ELIO</td>
  <td>Avellino</td>
  <td>IT</td>
  <td>VIA LUIGI AMABILE 27C</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>630,33</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.130,33</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CALCAGNO
  LORENZO</td>
  <td>Aidone</td>
  <td>IT</td>
  <td>VIA MARCONI 15</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CALDAROLA
  GERMANA ALBINA ANGELA</td>
  <td>Bisceglie</td>
  <td>IT</td>
  <td>VIA CISTERNA BUFIS</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>966,25</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>966,25</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CALLANDRONE
  ROBERTO</td>
  <td>Varazze</td>
  <td>IT</td>
  <td>VIA BATTISTI 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CALVO
  CALLEJA MARIA VICTORIA</td>
  <td>Rovigo</td>
  <td>IT</td>
  <td>VIA TRE MARTIRI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>813,19</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.510,19</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAMERINO
  ROSARIO</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>P.ZA
  FEDELE CALARCO 7</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>400,00</td>
  <td>290,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>690,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAMILLERI
  VINCENZO</td>
  <td>Agrigento</td>
  <td>IT</td>
  <td>VIA PLEBIS REA 66</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CANGIOTTI
  CESARE</td>
  <td>Santarcangelo
  di Romagna</td>
  <td>IT</td>
  <td>VIA PEDRIGNONE 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CANTILE
  ROSA</td>
  <td>Casapesenna</td>
  <td>IT</td>
  <td>VIA CAVOUR 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAPATO
  SILVIA</td>
  <td>Rho</td>
  <td>IT</td>
  <td>VIA PASSIRANA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CARACCIOLO
  PIETRO</td>
  <td>Messina</td>
  <td>IT</td>
  <td>C.SO
  DA PAPARDO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>650,00</td>
  <td>350,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CARBUCICCHIO
  ENRICO</td>
  <td>Trieste</td>
  <td>IT</td>
  <td>VIA SISTIANA 16/L</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>332,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>332,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CARROZZI
  FRANCESCO</td>
  <td>Pesaro</td>
  <td>IT</td>
  <td>INAIL
  P.ZZALE I MAGGIO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>728,55</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.078,55</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CARVALHO
  MICHAEL PAUL</td>
  <td>Roma</td>
  <td>IT</td>
  <td>P.ZA
  S.MARIA DELLA PIETA'</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CASTRICA
  CARLA</td>
  <td>Todi</td>
  <td>IT</td>
  <td>VIA DELLE CASELLE 17</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>320,00</td>
  <td>289,14</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>609,14</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAVARRA
  PIETRO</td>
  <td>Giardini-Naxos</td>
  <td>IT</td>
  <td>VIA VITT.EMANULE 182</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>800,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>800,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CAVENAGHI
  MASSIMILIANO</td>
  <td>Vicenza</td>
  <td>IT</td>
  <td>VIA RODOLFI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>457,95</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>457,95</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CERIANA
  PIERO</td>
  <td>Pavia</td>
  <td>IT</td>
  <td>VIA SALVATORE MAUGERI 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>391,64</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>391,64</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CHESSARI
  ROSARIA MARIA</td>
  <td>Tropea</td>
  <td>IT</td>
  <td>VIA LIBERTA'</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CIOFFO
  SALVATORE</td>
  <td>Casal
  di Principe</td>
  <td>IT</td>
  <td>C.SO
  UMBERTO I</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CIRILLO
  GIUSEPPE</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA ELIO LAMPRIDIO CERVA 24</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>407,52</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>407,52</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CITRO
  AMODIO</td>
  <td>Salerno</td>
  <td>IT</td>
  <td>VIA F. LA FRENCESCA 4</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>COLUCCI
  ANDREA</td>
  <td>Cortina
  d'Ampezzo</td>
  <td>IT</td>
  <td>VIA FRANCHETTI 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>468,73</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>468,73</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CONFALONIERI
  MARCO</td>
  <td>Trieste</td>
  <td>IT</td>
  <td>ST.A
  DI FIUME 447</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>508,82</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>508,82</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CONTE
  LUCA</td>
  <td>Rovigo</td>
  <td>IT</td>
  <td>V.LE
  DELLA PACE 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>379,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>379,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CORDA
  MARCO</td>
  <td>Cagliari</td>
  <td>IT</td>
  <td>VIA PERETTI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>265,30</td>
  <td>598,01</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>863,31</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>COSTANTINI
  MARIA GABRIELLA</td>
  <td>Belluno</td>
  <td>IT</td>
  <td>VIA FELTRE 50</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>COSTANZA
  BIANCA MARIA</td>
  <td>Bagheria</td>
  <td>IT</td>
  <td>VIA DI PASQUALE 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>535,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>535,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>COSTIGLIOLA
  ADRIANO</td>
  <td>Bacoli</td>
  <td>IT</td>
  <td>VIA CUPA DELLA TORRETTA 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>421,42</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>421,42</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>COSTIGLIOLA
  ANTONIO</td>
  <td>Pozzuoli</td>
  <td>IT</td>
  <td>VIA CAMPANA 250</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>461,58</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>461,58</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CREPALDI
  MONICA</td>
  <td>Lodi</td>
  <td>IT</td>
  <td>P.ZA
  OSPEDALE 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>338,73</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>338,73</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CUCUCCIO
  ANTONINO</td>
  <td>Acireale</td>
  <td>IT</td>
  <td>VIA REGINA MARGHERITA 7</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>CUFFARI
  BIAGIO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA DI TOR SAPIENZA 30/A</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>306,64</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>306,64</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>D'ADDIO
  FRANCESCO</td>
  <td>Marcianise</td>
  <td>IT</td>
  <td>P.ZA
  CARITA'</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>469,90</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>619,90</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DAINELLI
  ALBA</td>
  <td>Poggibonsi</td>
  <td>IT</td>
  <td>LOCALITA'
  CAMPOSTAGGIA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>D'AMATO
  GENNARO</td>
  <td>Napoli</td>
  <td>IT</td>
  <td>RIONE
  SIRIGNANO 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>419,98</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>419,98</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DANESI
  GIANLUCA</td>
  <td>Ravenna</td>
  <td>IT</td>
  <td>VIA MISSIROLI 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>776,56</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.126,56</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>D'AQUILA
  FRANCESCO</td>
  <td>Piedimonte
  Etneo</td>
  <td>IT</td>
  <td>VIA NUOVA DEL CONVENTO 63</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DE
  ANGELIS MASSIMO</td>
  <td>Sant'Elpidio
  a Mare</td>
  <td>IT</td>
  <td>VIA ANGELI 33-A</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>715,33</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.215,33</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DE
  MARZI MARIA</td>
  <td>Cinto
  Euganeo</td>
  <td>IT</td>
  <td>VIA ROMA 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>339,14</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>339,14</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DE
  SIMONE GIUSEPPE</td>
  <td>Benevento</td>
  <td>IT</td>
  <td>C/O
  PIANO CAPPELLE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>949,98</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.646,98</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DEL
  DONNO MARIO</td>
  <td>Benevento</td>
  <td>IT</td>
  <td>VIA DELL'ANGELO 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>522,82</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>522,82</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DELBONO
  GIUSEPPINA</td>
  <td>Varazze</td>
  <td>IT</td>
  <td>VIA CAIROLI 20</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>276,56</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>276,56</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DELICATI
  ORIANA</td>
  <td>Monfalcone</td>
  <td>IT</td>
  <td>VIA ROSSINI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>869,72</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.566,72</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DELL'AQUILA
  ANNA MARIA</td>
  <td>Caserta</td>
  <td>IT</td>
  <td>VIA ELEUTERIO RUGGIERO 107</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DEMURTAS
  ANTONIO</td>
  <td>Milano</td>
  <td>IT</td>
  <td>V.LE
  CERTOSA 278</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>331,13</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>331,13</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DENI
  LAURA</td>
  <td>Aci
  Castello</td>
  <td>IT</td>
  <td>VIA DANTE 18</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DENNETTA
  DONATELLA</td>
  <td>Pesaro</td>
  <td>IT</td>
  <td>VIA SABBATINI 22</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>475,42</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>475,42</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DENTE
  FEDERICO</td>
  <td>Pisa</td>
  <td>IT</td>
  <td>VIA ZAMENHOF 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>174,92</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>174,92</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DETOTTO
  EUGENIO</td>
  <td>Budrio</td>
  <td>IT</td>
  <td>VIA BENNI 44</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>291,02</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>291,02</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>D'ETTORE
  NICOLINA</td>
  <td>Sant'Arpino</td>
  <td>IT</td>
  <td>C/O
  ASL</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>419,98</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>419,98</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DI
  GIACOMO GIOVANNI</td>
  <td>Biancavilla</td>
  <td>IT</td>
  <td>S.P.
  STRADALE PROVINCIALE 80</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>1.033,18</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.033,18</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DI
  MITA FRANCESCO</td>
  <td>Gaiarine</td>
  <td>IT</td>
  <td>VIA FRACASSI 39</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>402,10</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>402,10</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DI
  PALMA FRANCESCO</td>
  <td>Scafati</td>
  <td>IT</td>
  <td>VIA L. STURZO 73</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>555,33</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.055,33</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DI
  SAVERIO PIERO</td>
  <td>Teramo</td>
  <td>IT</td>
  <td>VIA F. MASCI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>630,33</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.130,33</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DI
  TRANI RUGGIERO</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA P.PE DI PANTELLERIA 37</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>DIMITRI
  FEDOR</td>
  <td>Frosinone</td>
  <td>IT</td>
  <td>VIA MARITTIMA 192</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>419,98</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>419,98</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>EBBLI
  ANTONIO</td>
  <td>Savona</td>
  <td>IT</td>
  <td>VIA GENOVA 30</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>254,34</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>254,34</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ERCOLI
  ANGELAMARIA</td>
  <td>Lodi</td>
  <td>IT</td>
  <td>P.ZA
  OSPEDALE 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>760,18</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>760,18</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ERMETI
  MASSIMO</td>
  <td>Morciano
  di Romagna</td>
  <td>IT</td>
  <td>VIA MONTESI 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>406,20</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>906,20</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ESLAMI
  VARZANEH AMIR HOSSEIN</td>
  <td>Perugia</td>
  <td>IT</td>
  <td>OSP.SILVESTRINI
  REP.</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>682,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.032,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FACCINI
  ENZO</td>
  <td>Dolo</td>
  <td>IT</td>
  <td>VIA RIVIERA XXIX APRILE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>707,50</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>707,50</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FALCHINI
  GIUSEPPE</td>
  <td>Civitavecchia</td>
  <td>IT</td>
  <td>VIA DONATORI DEL SANGUE 98</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FARAONE
  STANISLAO</td>
  <td>Casoria</td>
  <td>IT</td>
  <td>VIA S.ROCCO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FERRANTE
  LORENZO</td>
  <td>Pioltello</td>
  <td>IT</td>
  <td>VIA MARCONI 18</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FERRANTE
  MARIA CONCETTA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA INGEGNEROS 33</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FERRARESSO
  ALBERTO</td>
  <td>Feltre</td>
  <td>IT</td>
  <td>VIA BAGNOLS SUR CEZE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>807,26</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.504,26</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FINAZZI
  ALFREDO</td>
  <td>Grumello
  del Monte</td>
  <td>IT</td>
  <td>VIA MART.LIBERTA' 45</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FORGIARINI
  GIOVANNI</td>
  <td>Novara</td>
  <td>IT</td>
  <td>VIA DEI MILLE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>440,00</td>
  <td>910,68</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.350,68</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FOSCHI
  CLAUDIO</td>
  <td>Cesena</td>
  <td>IT</td>
  <td>VIA SAUB C.SO CAVOUR 25</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>408,39</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>408,39</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FRANCHIGNONE
  PIER LUIGI</td>
  <td>Cambiasca</td>
  <td>IT</td>
  <td>VIA ORTI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>331,13</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>331,13</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FRANCIONI
  SANDRA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA DELLA LOGGETTA 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>299,85</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>299,85</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FRANZESE
  ANTONIO</td>
  <td>San
  Gennaro Vesuviano</td>
  <td>IT</td>
  <td>P.ZZA
  MARGHERITA 4</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FRAUSINI
  GIAMPAOLO</td>
  <td>Pesaro</td>
  <td>IT</td>
  <td>VIA SABBATTINI 21</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>429,75</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>429,75</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>FUMAGALLI
  RENATO</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA CARMELO LAZZARO 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GAMBARDELLA
  PAOLO</td>
  <td>Lamezia
  Terme</td>
  <td>IT</td>
  <td>VIA OSPEDALE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GAROFALO
  MARIA ANTONELLA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA CATALDO PARISIO 95</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GATTA
  DIEGO</td>
  <td>Esine</td>
  <td>IT</td>
  <td>VIA ALESSANDRO MANZONI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>388,44</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>388,44</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GAUDIOSI
  CARLO</td>
  <td>Telese
  Terme</td>
  <td>IT</td>
  <td>VIA NINO BIXIO 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>517,46</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.214,46</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GIANNI'
  GIANVITO</td>
  <td>Siena</td>
  <td>IT</td>
  <td>VIA DI COLLEVERDE 19</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>525,53</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>525,53</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GOTI
  PATRIZIO</td>
  <td>Prato</td>
  <td>IT</td>
  <td>P.ZA
  OSPEDALE 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>463,35</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>463,35</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GRAMAGLIA
  MASSIMO</td>
  <td>Cecina</td>
  <td>IT</td>
  <td>P.ZZA
  GUERRAZZI 12</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>498,08</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>498,08</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GRAVINO
  GENNARO</td>
  <td>Pollena
  Trocchia</td>
  <td>IT</td>
  <td>VIA MASSA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>726,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.076,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GRECO
  ALESSANDRA</td>
  <td>Pavia</td>
  <td>IT</td>
  <td>V.LE
  C. GOLGI 19</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>265,30</td>
  <td>296,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>561,36</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GRECO
  NATALE</td>
  <td>San
  Lazzaro di Savena</td>
  <td>IT</td>
  <td>VIA JUSSI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>326,07</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>326,07</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GUERRA
  GIOVANNI</td>
  <td>Sant'Angelo
  Lodigiano</td>
  <td>IT</td>
  <td>ST.A
  PROVINCIALE 19</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>310,79</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>310,79</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>GUIDA
  STEFANIA</td>
  <td>Pavia</td>
  <td>IT</td>
  <td>V.LE
  C. GOLGI 19</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>265,30</td>
  <td>296,66</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>561,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>IACOACCI
  CORRADO</td>
  <td>Ancona</td>
  <td>IT</td>
  <td>VIA DELLA MONTAGNOLA 164</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>472,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>472,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>IMERI
  GIANLUCA</td>
  <td>Milano</td>
  <td>IT</td>
  <td>VIA A.DI RUDINI' 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>322,44</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>322,44</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>INCARBONE
  MARISA MARIA</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>VIA S.LA MALFA 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>INSINGA
  ANTONINO</td>
  <td>Isola
  delle Femmine</td>
  <td>IT</td>
  <td>VIA CUTINO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>KLOTZNER
  MARKUS</td>
  <td>Bolzano</td>
  <td>IT</td>
  <td>PIAZZA
  W. A. CADONNA 12</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>254,64</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>254,64</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LA
  MANTIA S.RE GIUSEPPE</td>
  <td>Bagheria</td>
  <td>IT</td>
  <td>VIA CITTA' DI PALERMO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LA
  PORTA GIANFRANCO</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>VIA MANZONI S.N.C.</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LA
  ROCCA MARIO</td>
  <td>Vazzola</td>
  <td>IT</td>
  <td>VIA S FRANCESCO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>335,74</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>335,74</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LA
  SETA FRANCESCO</td>
  <td>Corleone</td>
  <td>IT</td>
  <td>VIA DON G.COLLETTO 15</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LA
  VILLA LISA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA RONDINELLA 66</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LANZILLOTTI
  ROSSANA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA STARNINA 34</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LAREGINA
  MARILENA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA APPIA NUOVA 225</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>407,52</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>407,52</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LISCIANDRANO
  ANTONINO</td>
  <td>Militello
  in Val di Catania</td>
  <td>IT</td>
  <td>PIAZZA
  S.AGATA 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LO
  CONTE CINZIA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>V.LE
  PIERACCINI 17</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>299,86</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>299,86</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LONETTI
  NICOLA</td>
  <td>Crotone</td>
  <td>IT</td>
  <td>C.SO
  MESSINA 25</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LONGO
  ANNA MARIA</td>
  <td>Catania</td>
  <td>IT</td>
  <td>VIA MESSINA 829</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>736,03</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>736,03</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>LUCENTE
  ANNIBALE</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA DELLE VIGNE 149</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MACCIOCCHI
  BRUNO</td>
  <td>Cassino</td>
  <td>IT</td>
  <td>VIA CASILINA NORD</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>899,61</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>899,61</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MADASCHI
  CLAUDIA</td>
  <td>Bergamo</td>
  <td>IT</td>
  <td>VIA GAVAZZENI 6</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>344,46</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>344,46</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MAGGIO
  VITO</td>
  <td>Agrigento</td>
  <td>IT</td>
  <td>VIA FICANI 30</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MANNINI
  CLAUDIA</td>
  <td>Livorno</td>
  <td>IT</td>
  <td>C/O
  POSTAZ.GUA.TUR.</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>599,70</td>
  <td>2.888,02</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.487,72</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MANNINO
  ANTONINO</td>
  <td>Capaci</td>
  <td>IT</td>
  <td>VIA KENNEDY 85</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARCHESE
  RAGONA ANTONIO</td>
  <td>Bolzano</td>
  <td>IT</td>
  <td>VIA FIRENZE 56</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARCHETTI
  MARIA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIALE
  VEGA 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARCHIONNE
  MAURIZIO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA PARASACCHI 206</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>592,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARIANI
  MARIO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA FRANCESCO TOVAGLIERI 379</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>584,20</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>584,20</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARINELLI
  MASSIMILIANO</td>
  <td>Ancona</td>
  <td>IT</td>
  <td>CORSO
  GARIBALDI 101</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARINO
  MAURO ANTONIO</td>
  <td>Termini
  Imerese</td>
  <td>IT</td>
  <td>VIA S. CIMINO 18</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARRAZZO
  GIUSEPPINA</td>
  <td>Crotone</td>
  <td>IT</td>
  <td>VIA XXV APRILE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARTUCCI
  MICHELE</td>
  <td>Telese
  Terme</td>
  <td>IT</td>
  <td>VIA NINO BIXIO 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MARZULLO
  MANLIO</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>VIA MONS.PALERMO 40</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MATRAXIA
  PIERA MARIA</td>
  <td>Gela</td>
  <td>IT</td>
  <td>VIA DEL CALVARIO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MAURO
  UMBERTO</td>
  <td>Fiumefreddo
  di Sicilia</td>
  <td>IT</td>
  <td>VIA BELLINI 42</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MAZID
  MAHMOUD</td>
  <td>Albano
  Sant'Alessandro</td>
  <td>IT</td>
  <td>VIA PAPA GIOVANNI XXIII 27/9</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>293,93</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>293,93</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MAZZA
  GIUSEPPA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA DANTE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MEAZZA
  LUIGI</td>
  <td>Lissone</td>
  <td>IT</td>
  <td>VIA ALFIERI 22</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>333,32</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>333,32</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MEINI
  GIAMPAOLO</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA STARNINA 52</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MENCONI
  DANIELA</td>
  <td>Porto
  Sandt'Elpidio</td>
  <td>IT</td>
  <td>VIA ELPIDIENSE 17</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MENEGHELLI
  LISA SERENA</td>
  <td>Nogara</td>
  <td>IT</td>
  <td>VIA LEONE 11</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MENNA
  PATRIZIA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA DELLE CALASANZIANE 25</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>592,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MESSINA
  GASPARE</td>
  <td>Realmonte</td>
  <td>IT</td>
  <td>VIA FARO 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MESSINA
  VINCENZO</td>
  <td>Caserta</td>
  <td>IT</td>
  <td>VIA TESCIONE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>421,42</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>421,42</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MICHELETTO
  CLAUDIO</td>
  <td>Bussolengo</td>
  <td>IT</td>
  <td>VIA OSPEDALE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>43,89</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>43,89</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MICHIELETTO
  LUCIO</td>
  <td>Chioggia</td>
  <td>IT</td>
  <td>VIA OSPEDALE CHIOGGIA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>420,90</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>420,90</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MINCARINI
  MARCELLO</td>
  <td>Genova</td>
  <td>IT</td>
  <td>LARGO
  ROSANNA BENZI 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>256,38</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>256,38</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MINCHIO
  ELISA</td>
  <td>Trento</td>
  <td>IT</td>
  <td>L.GO
  MEDAGLIE D'ORO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>495,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>495,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MINELLI
  FERDINANDO</td>
  <td>Sarezzo</td>
  <td>IT</td>
  <td>VIA IV NOVEMBRE 38</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>220,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>220,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MINORE
  GIANCARLO</td>
  <td>Bologna</td>
  <td>IT</td>
  <td>VIA EMILIA LEVANTE 81</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>776,56</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.126,56</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MIRABELLA
  FRANCO</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA MESSINA MARINA 197</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MIRODDI
  SALVATORE</td>
  <td>Aidone</td>
  <td>IT</td>
  <td>VIA GANGI 9</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MONACO
  ILENIA</td>
  <td>Foggia</td>
  <td>IT</td>
  <td>V.LE
  PINTO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>377,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>377,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MONDINO
  GERMANO GIUSEPPE</td>
  <td>Sanremo</td>
  <td>IT</td>
  <td>C/O
  CASINO C.SO INGLESI 26</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>367,83</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>367,83</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MONTERA
  MADDALENA</td>
  <td>Vietri
  sul Mare</td>
  <td>IT</td>
  <td>C.SO
  UMBERTO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MONTI
  PAOLO</td>
  <td>Larciano</td>
  <td>IT</td>
  <td>VIA GRAMSCI 901</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MUNGO
  FRANCESCA</td>
  <td>Maser�
  di Padova</td>
  <td>IT</td>
  <td>VIA CONSELVANA 110</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>396,76</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>396,76</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MURATORE
  MAURIZIO</td>
  <td>LECCE</td>
  <td>IT</td>
  <td>VIA SCARAMBONE 36</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>0</td>
  <td>3.000,00</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MUSOLINO
  BEATRICE</td>
  <td>Genova</td>
  <td>IT</td>
  <td>VIA DELLE ROVARE 18</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>95,60</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>95,60</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MUSOLINO
  TULLIA</td>
  <td>Lamezia
  Terme</td>
  <td>IT</td>
  <td>VIA OSPEDALE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>MUZZIOLI
  GIOVANNI LUIGI</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA TOMASSINI 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>592,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>NATALE
  MARIARITA</td>
  <td>Frosinone</td>
  <td>IT</td>
  <td>VIA FABI A.S.L. AMB.</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>NOCERA
  GIOVANNI</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>VIA MONS.LA VACCARA 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ONIDA
  ROSA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>STA
  STATALE 113 31/DE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ORRU'
  ANTONELLO</td>
  <td>Tortol�</td>
  <td>IT</td>
  <td>C/O
  AMB.COMUNALE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>624,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>624,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PACE
  ELISABETTA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA UGO LA MALFA 122</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>475,00</td>
  <td>2.080,92</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.555,92</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PACE
  NADIA</td>
  <td>Aci
  Catena</td>
  <td>IT</td>
  <td>VIA GUGLIELMINO 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PAGANI
  MATTEO</td>
  <td>Lodi</td>
  <td>IT</td>
  <td>P.ZA
  OSPEDALE 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>691,31</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>691,31</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PALADINI
  ILARIA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA LUNGO L'AFRICO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PALADINI
  LUCIANA</td>
  <td>Padova</td>
  <td>IT</td>
  <td>VIA GIUSTINIANI 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>708,31</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.058,31</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PANACCIONE
  ALESSIA</td>
  <td>Ardea</td>
  <td>IT</td>
  <td>VIA PRATICA DI MARE 12B</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PANZERA
  FULVIO</td>
  <td>Rogliano</td>
  <td>IT</td>
  <td>C/O
  ASL 13-POLIAMB.</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>653,66</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>653,66</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PAOLETTI
  GIANCARLO</td>
  <td>Trieste</td>
  <td>IT</td>
  <td>VIA TOR SAN PIERO 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>467,14</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>467,14</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PAPALE
  MARIA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA ELIO CHIANESI 33</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>561,31</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>561,31</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PAPOTTO
  GIUSEPPINA</td>
  <td>Lentini</td>
  <td>IT</td>
  <td>VIA TERMINI 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>800,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>800,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PASSERA
  ALBERTO</td>
  <td>Vizzolo
  Predabissi</td>
  <td>IT</td>
  <td>H
  PREDABISSI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PASTORE
  UMBERTO</td>
  <td>Bassano
  del Grappa</td>
  <td>IT</td>
  <td>VIA DEI LOTTI SNC</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>844,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.541,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PECORARO
  MARCO</td>
  <td>Tortol�</td>
  <td>IT</td>
  <td>VIA STAZIONE 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>624,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>624,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PEDICELLI
  ILARIA</td>
  <td>Casoria</td>
  <td>IT</td>
  <td>VIA S.ROCCO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>727,21</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.077,21</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PELAIA
  GIROLAMO</td>
  <td>Catanzaro</td>
  <td>IT</td>
  <td>VIA TOMMASO CAMPANELLA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>597,34</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>597,34</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PENNINO
  ANTONINO</td>
  <td>Terme
  Vigliatore</td>
  <td>IT</td>
  <td>VIA NAZIONALE 256</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>650,00</td>
  <td>350,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PERZIANO
  FRANCESCO</td>
  <td>Cir�
  Marina</td>
  <td>IT</td>
  <td>C/O
  SERVIZIO PNEUMOLOGIA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>591,86</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>591,86</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PESCE
  PAOLA</td>
  <td>Noale</td>
  <td>IT</td>
  <td>PIAZZA
  DEL CARNIO 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PETRICCA
  DANIELA</td>
  <td>Battaglia
  Terme</td>
  <td>IT</td>
  <td>PIAZZA
  LIBERTA' 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>264,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>264,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PIAZZA
  INNOCENTE</td>
  <td>Portogruaro</td>
  <td>IT</td>
  <td>VIA ZAPPETTI 58</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>459,37</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>459,37</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PIERALLINI
  FEDERICO</td>
  <td>Ponte
  Buggianese</td>
  <td>IT</td>
  <td>VIA MATTEOTTI 36</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PIRARI
  FRANCESCA</td>
  <td>Tortol�</td>
  <td>IT</td>
  <td>VIA DELEDDA 40</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>576,27</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>576,27</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>POGGI
  ROBERTA</td>
  <td>Verona</td>
  <td>IT</td>
  <td>OSP.
  B.TN.PNEUMOLOGIA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>152,88</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>152,88</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>POMERRI
  ANTONELLA</td>
  <td>Padova</td>
  <td>IT</td>
  <td>VIA DEI COLLI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>264,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>264,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>POMPILI
  TERESA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>P.ZA
  S.MARIA DELLA PIETA'</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PONTONE
  FILIPPO</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA VALDERA 54</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PORTO
  VITTORIO</td>
  <td>Termini
  Imerese</td>
  <td>IT</td>
  <td>VIA PANDOLFINI 19</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PRIVITERA
  GAETANO MARIA</td>
  <td>Aci
  Catena</td>
  <td>IT</td>
  <td>VIA V. EMANUELE 101</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>PUMA
  SALVATORE</td>
  <td>Favara</td>
  <td>IT</td>
  <td>CORSO
  VITTORIO VENETO 94</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>QALQILI
  ALFRED DAOUD</td>
  <td>Alessandria</td>
  <td>IT</td>
  <td>VIA BUOZZI 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>707,50</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>707,50</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RAMPONI
  ALESSANDRO CESARE</td>
  <td>Pioltello</td>
  <td>IT</td>
  <td>VIA VERONESI 14</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>220,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>220,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RAPUZZI
  FABRIZIO</td>
  <td>Cinisello
  Balsamo</td>
  <td>IT</td>
  <td>VIA MAZZINI 4</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RAVIELE
  ANTONIO</td>
  <td>San
  Salvatore Telesino</td>
  <td>IT</td>
  <td>VIA CORTE NOCERA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RIBOLLA
  FULVIA</td>
  <td>Milano</td>
  <td>IT</td>
  <td>VIA A.DI RUDINI' 8</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>264,08</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>264,08</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RICCI
  QUIRINO MASSIMO</td>
  <td>Ancona</td>
  <td>IT</td>
  <td>V.LE
  VOLTERRA 16</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RICCIARDI
  ORLANDO</td>
  <td>Godega
  di Sant'Urbano</td>
  <td>IT</td>
  <td>VIA BRUSCHE 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>220,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>220,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RICCUCCI
  ANDREA</td>
  <td>Firenze</td>
  <td>IT</td>
  <td>VIA STARNINA 78</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>92,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>92,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RIGHETTI
  STEFANO</td>
  <td>Verona</td>
  <td>IT</td>
  <td>P.LE
  STEFANI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>58,68</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>58,68</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RINALDI
  ROBERTA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA CRUILLAS 310</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>250,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RINALDI
  SANDRO</td>
  <td>Bolzano</td>
  <td>IT</td>
  <td>VIA PALERMO 11</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>309,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>309,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RIVA
  ROBERTO</td>
  <td>Merano</td>
  <td>IT</td>
  <td>VIA GOETHE 50</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>152,88</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>152,88</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RIZZO
  FRANCESCO OSVALDO</td>
  <td>Aci
  San Filippo</td>
  <td>IT</td>
  <td>VIA VIA CROCE 140</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RIZZO
  L. C.</td>
  <td>Campi
  Bisenzio</td>
  <td>IT</td>
  <td>VIA S.STEFANO 13</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>650,11</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>650,11</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ROCCO
  PAOLA ROSARIA ANTONIA</td>
  <td>Caserta</td>
  <td>IT</td>
  <td>VIA DEI VECCHI PINI 17</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ROMEO
  DOMENICO</td>
  <td>Matera</td>
  <td>IT</td>
  <td>VIA MONTESCAGLIOSO 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>102,23</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>102,23</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ROSATI
  YURI</td>
  <td>Macerata</td>
  <td>IT</td>
  <td>VIA S.LUCIA 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>640,13</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>640,13</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ROSSI
  ANTONELLA</td>
  <td>Cecina</td>
  <td>IT</td>
  <td>P.ZZA
  GUERRAZZI 12</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>95,60</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>95,60</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ROVEDA
  PAOLO</td>
  <td>Melegnano</td>
  <td>IT</td>
  <td>VIA PANDINA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>332,17</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>332,17</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>RUSSO
  MAURIZIO</td>
  <td>Crotone</td>
  <td>IT</td>
  <td>C.SO
  MESSINA 25</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SALATTO
  MARIA PINA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>V.LE
  DEI ROMAGNOLI 12</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>892,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SALVUCCI
  MARIA CECILIA</td>
  <td>Morrovalle</td>
  <td>IT</td>
  <td>VIA ROSSINI 32</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>319,25</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>319,25</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SANGUIGNI
  LUCIANO</td>
  <td>Sabaudia</td>
  <td>IT</td>
  <td>B.GO
  VODICE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>415,84</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>415,84</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SANNA
  NADIA</td>
  <td>Cagliari</td>
  <td>IT</td>
  <td>VIA PERETTI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>841,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>841,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SANTOLI
  GIOVAN BATTISTA</td>
  <td>Montopoli
  in Val d'Arno</td>
  <td>IT</td>
  <td>VIA MATTEI 4</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SAPIENTE
  VITTORIO</td>
  <td>Cosenza</td>
  <td>IT</td>
  <td>VIA F. MIGLIORI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCADUTO
  VINCENZO</td>
  <td>Bagheria</td>
  <td>IT</td>
  <td>VIA FUMAGALLI 25</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCAGLIONE
  FRANCESCO</td>
  <td>Cesano
  Maderno</td>
  <td>&nbsp;</td>
  <td>VIA BENACO 10</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>0</td>
  <td>2.000,00</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.000,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCARPELLI
  ENRICO</td>
  <td>Cosenza</td>
  <td>IT</td>
  <td>C.SO
  DA MUOIO PICCOLO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>939,35</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>939,35</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCARTABELLATI
  ALESSANDRO</td>
  <td>Crema</td>
  <td>IT</td>
  <td>VIA MACOLLE'14</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>160,87</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>160,87</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCARZO
  EMANUELE</td>
  <td>Alessandria</td>
  <td>IT</td>
  <td>P.ZZA
  GARIBALDI 48</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCAVALLI
  PATRIZIA</td>
  <td>Civita
  Castellana</td>
  <td>IT</td>
  <td>VIA FERRETTI 169</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>617,28</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>617,28</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCEVOLA
  MORENO</td>
  <td>Mirano</td>
  <td>IT</td>
  <td>OSP.
  MIRANO MEDICINA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>440,64</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>440,64</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCHENONE
  LUCA</td>
  <td>Genova</td>
  <td>IT</td>
  <td>VIA MILANO C.MARITTIMA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>167,80</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>167,80</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCHIAPPOLI
  MICHELE</td>
  <td>Verona</td>
  <td>IT</td>
  <td>P.LE
  STEFANI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>414,37</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>414,37</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCIARRA
  FEDERICO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>P.ZA
  LORENZINI 30</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>596,78</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>596,78</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SCICHILONE
  NICOLA</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA UGO LA MALFA 122</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>3.038,37</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.038,37</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SERGIO
  MARCELLO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA ROBERTO MALATESTA 124</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>592,62</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>592,62</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SOELVA
  BERND</td>
  <td>Bolzano</td>
  <td>IT</td>
  <td>VIA LORENZ BOHLER 5</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>839,56</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.189,56</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SPADARO
  SALVATORE</td>
  <td>Catania</td>
  <td>IT</td>
  <td>VIA MESSINA 829</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>1.490,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.490,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SPADOTTO
  VERONICA</td>
  <td>Padova</td>
  <td>IT</td>
  <td>VIA GIUSTINIANI 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>403,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>403,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>SPINA
  MARIA GIUSEPPA</td>
  <td>Acireale</td>
  <td>IT</td>
  <td>VIA V. SARDELLA 9</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>STORCHI
  MASSIMO</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA GIOVAGNOLI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TERRANA
  BALDASSARE</td>
  <td>Porto
  Empedocle</td>
  <td>IT</td>
  <td>VIA DELLO SPORT POLIAMB. SN</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TERRANOVA
  SALVATORE</td>
  <td>Palermo</td>
  <td>IT</td>
  <td>VIA INSERRA 4 L</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>285,71</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,71</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TESTA
  DAVIDE</td>
  <td>Pietra
  Marazzi</td>
  <td>IT</td>
  <td>P.ZA
  UMBERTO I</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TINAZZI
  ELISA</td>
  <td>Verona</td>
  <td>IT</td>
  <td>P.LE
  STEFANI 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>405,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>405,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TOMA
  GIOVANNI</td>
  <td>Pisa</td>
  <td>IT</td>
  <td>VIA ZAMENHOF 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TORRELLI
  LAURA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA EMILIO PRAGA 26</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>300,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TORRISI
  ANGELO</td>
  <td>Fiumefreddo
  di Sicilia</td>
  <td>IT</td>
  <td>CORSO
  VINCENZO BELLINI 78</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TOSCANO
  GIUSEPPE</td>
  <td>Pomigliano
  d'Arco</td>
  <td>IT</td>
  <td>VIA P DI PIEMONTE 215</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>356,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>356,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TREVISAN
  FIORENZA</td>
  <td>Bussolengo</td>
  <td>IT</td>
  <td>VIA OSPEDALE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>1.129,29</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.479,29</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TROPIANO
  DANIELA</td>
  <td>Catanzaro</td>
  <td>IT</td>
  <td>VIA OSPEDALE 2</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TUCI
  STEFANO</td>
  <td>Monsummano
  Terme</td>
  <td>IT</td>
  <td>VIA MISERICORDIA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>TUTTOLOMONDO
  RAFFAELE</td>
  <td>Raffadali</td>
  <td>IT</td>
  <td>VIA NAZIONALE 147</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>UBALDI
  ENZO</td>
  <td>Viterbo</td>
  <td>IT</td>
  <td>C/O
  ASL</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>513,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>863,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>UGOLINI
  MARCELLO</td>
  <td>Pesaro</td>
  <td>IT</td>
  <td>VIA SABBATINI 22</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>350,00</td>
  <td>703,06</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.053,06</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VALSECCHI
  ELEONORA</td>
  <td>Lecco</td>
  <td>IT</td>
  <td>C.SO
  MARTIRI 94</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VANNUCCI
  FRANCO</td>
  <td>Pistoia</td>
  <td>IT</td>
  <td>P.ZA
  GIOVANNI XXIII</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>463,35</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>463,35</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VELLA
  CANNELLA PIO GIOACHINO</td>
  <td>Canicatt�</td>
  <td>IT</td>
  <td>VIA G. GIANGRECO 6</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>150,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>150,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VENTURINI
  MARIO RICCARDO FRANCESCO</td>
  <td>Colle
  di Compito</td>
  <td>IT</td>
  <td>VIA DI TIGLIO 783</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>188,16</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>188,16</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VENTURINI
  ROBERTO ERCOLINO</td>
  <td>Mortara</td>
  <td>IT</td>
  <td>ST.A
  PAVESE 1013</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>473,09</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>473,09</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VIGNALE
  ALBERTO</td>
  <td>Alessandria</td>
  <td>IT</td>
  <td>P.ZA
  GARIBALDI 48</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>298,81</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>298,81</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VILLA
  GIUSEPPE PAOLO</td>
  <td>Gioiosa
  Ionica</td>
  <td>IT</td>
  <td>V.LE
  RIMEMBRANZE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VILLAREALE
  GIUSEPPE</td>
  <td>Piazza
  Armerina</td>
  <td>IT</td>
  <td>VIA NINO BIXIO</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VINCENTI
  CRISTINA</td>
  <td>Passoscuro</td>
  <td>IT</td>
  <td>VIA S. D'AMICO 110</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>459,82</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>459,82</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VINCENZI
  ANTONELLA</td>
  <td>Monza</td>
  <td>IT</td>
  <td>VIA DONIZETTI 106</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>437,00</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>437,00</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>VITA
  PIERPAOLO</td>
  <td>Verona</td>
  <td>IT</td>
  <td>VIA VILLAFRANCA</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>285,01</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>285,01</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZAMPOGNA
  MARIA TERESA</td>
  <td>Roma</td>
  <td>IT</td>
  <td>VIA ARMELLINI 30</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>120,00</td>
  <td>1.068,50</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.188,50</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZANASI
  ALESSANDRO</td>
  <td>Bologna</td>
  <td>IT</td>
  <td>VIA MASSARENTI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>645,00</td>
  <td>41,40</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>686,40</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZANATTA
  ALBERTO</td>
  <td>Vicenza</td>
  <td>IT</td>
  <td>VIA RODOLFI</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>697,00</td>
  <td>805,15</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.502,15</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZANNINI
  GUIDO</td>
  <td>Verona</td>
  <td>IT</td>
  <td>OSP.
  CIVILE MAGGIORE</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>58,68</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>58,68</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZARANTONELLO
  VITTORIANO</td>
  <td>Montagnana</td>
  <td>IT</td>
  <td>VIA BERGHETTA II 3</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>195,96</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>195,96</td>
 </tr>
<tr>
  <td>HCP</td>
  <td>ZECCHINATO
  FABRIZIO</td>
  <td>Legnago</td>
  <td>IT</td>
  <td>VIA GIANELLA 1</td>
  <td>&nbsp;</td>
  <td>N/A</td>
  <td>N/A</td>
  <td>0</td>
  <td>707,50</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>707,50</td>
 </tr>
 			
            </tbody>
        </table>

        <p>&nbsp;</p>
        <table class="tablesorter3">
            <thead>
               <tr>
                    <td colspan="14" data-sorter="false" style="text-align: center; color: #E20025;">Period: 2015<br />Pubblished on 30 June 2016</td>
                </tr>
                <tr>
                    <th rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                    <th valign="top" style="padding-top:10px;">
						Full Name<br /><br />
						<span style="color:red;">Nome e Cognome/ Denominazione</span>						
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						<strong>HCPs</strong>: City of Principal Practice<br /><strong>HCOs</strong>: city where registered<br /><br />
						<span style="color:red;">
							<strong>Operatori sanitari</strong>: Citt�  dove si svolge  prevalentemente la professione<br />
							<strong>Organizzazioni sanitarie</strong>: Sede Legale
						</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Country of Principal Practice<br /><br />
						<span style="color:red;">Stato dove si svolge prevalentemente la professione/attivit�</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Principal Practice Address<br /><br />
						<span style="color:red;">Indirizzo dove si svolge prevalentemente la professione/attivit�</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Unique country identifier<br /><br />
						<span style="color:red;">Codice di identificazione del  Paese (Facoltativo)</span>
					</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						Donations and Grants to HCOs<br /><br />
						<span style="color:red;">Donazioni e contributi a organizzazioni sanitarie</span>
					</th>
                    <th valign="top" style="padding-top:10px;" colspan="3" data-filter="false" data-sorter="false">
						Contribution to costs of Events<br /><span style="font-size:80%;">(Art. 3.01.1.b and  3.01.2.a CD)</span><br /><br />
						<span style="color:red;">Contributo per il finanziamento di eventi (es.convegni, congressi e riunioni scientifiche)
( Punto 5.5, lettera a) e Punto 5.6, lettera b)  e allegato 2 CE</span>
					</th>
                    <th valign="top" style="padding-top:10px;" colspan="2" data-filter="false" data-sorter="false">
						Fee for service and consultancy<br /><br />
						<span style="color:red;">Corrispettivi per prestazioni professionali e consulenze (punto 5.5 lettera b e punto 5.6 lettera c) CE</span>
					</th>
                    <th valign="top" style="padding-top:10px;" rowspan="2" data-filter="false" data-sorter="false">&nbsp;</th>
                    <th valign="top" style="padding-top:10px;" data-filter="false" data-sorter="false">
						TOTAL OPTIONAL<br /><br />
						<span style="color:red;">TOTALE Facoltativo</span>
					</th>
                </tr>
                <tr>
					<th data-sorter="false" style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 1.01 ) Codice EFPIA Disclosure(CE)<br /><br />
						<span style="color:red;">Punto 5.1 Codice deontologico Farmindustria (CD)</span>
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3 CE)<br /><br />
						<span style="color:red;">da Punto 5,5 a 5.7 CD</span>
					</th>

					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Modulo 1CE)<br /><br />
						<span style="color:red;">Allegato 2(CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3 CE)<br /><br />
						<span style="color:red;">(da Punto 5.5 a 5.7CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3CE)<br /><br />
						<span style="color:red;">(da Punto 5.5 a 5.7 CD)</span>					
					</th>
					<th data-sorter="false" data-filter="false"  style="font-style:italic;font-weight:normal;color:grey;padding-top:10px;" valign="top">
						(Art. 3.01.1.a CE)<br /><br />
						<span style="color:red;">(Punto 5.6, lettera a CD)</span>					
					</th>
					
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an EventL<br /><br />
						<span style="color:red;">Accordi di sponsorizzazione con organizzazioni sanitarie/soggetti terzi nominati da organizzazioni sanitarie per la realizzazione di eventi</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Registration Fees<br /><br />
						<span style="color:red;">Quote di iscrizione</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Travel & Accomodation<br /><br />
						<span style="color:red;">Viaggi e  ospitalit�</span>
					</th>
                    <th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Fees<br /><br />
						<span style="color:red;">Corrispettivi</span>
					</th>
					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">
						Related expenses agreed in the fee for service or consultancy contract, including travel & accomodation relevant to the contract<br /><br />
						<span style="color:red;">Spese riferibili ad attivit� di consulenza e prestazioni professionali risultanti da uno specifico contratto, comprendenti  le relative spese di viaggio e ospitalit�</span>
					</th>
					<th data-filter="false" data-sorter="false" valign="top" style="padding-top:10px;">&nbsp;</th>
                </tr>
            </thead>
			
			
            <tbody>
	
		
  <tr>
  <td>HCO</td>
  <td>ACADEMY
  SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA
  AOSTA, 4</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>9.100,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>9.100,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ACROSS SARDINIA SAS</td>
  <td>SASSARI</td>
  <td>IT</td>
  <td>VIALE ITALIA 12</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>3.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>AIM CONGRESS SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA RIPAMONTI, 129</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>19.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>19.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>AIM EDUCATION SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA G. RIPAMONTI, 129</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>15.091,95</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>15.091,95</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>AIPO RICERCHE</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA FRUA, 15</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>19.010,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>19.010,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ASS AMICI DEL CENTRO DINO FERRARI</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA FRANCESCO SFORZA 35</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ASS CULT LABORATORIO ADOLESCENZA</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA TROILO 4</td>
  <td>&nbsp;</td>
  <td>6.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>6.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>AZIENDA POLICLINICO UMBERTO I ROMA</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIALE DEL POLICLINICO, 155</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
 </tr>
 <tr>
  <td>HCO</td>
  <td>C.LABMEETING COMMUNICATION LABORATORY</td>
  <td>BARI</td>
  <td>IT</td>
  <td>Strada Statale Bari-Modugno-Toriotto 65</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>3.980,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.980,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CAPRIMED SRL</td>
  <td>NAPOLI</td>
  <td>IT</td>
  <td>VIA SELLA ORTA 3</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>18.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>18.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CARLSON WAGONLIT ITALIA SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA VESPUCCI 2</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.489,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.489,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CENTER COMUNICAZIONE E CONGRESSI</td>
  <td>NAPOLI</td>
  <td>IT</td>
  <td>VIA G. QUAGLIARELLO, 27</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>16.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>24.000,00</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>40.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CENTRO ITALIANO CONGRESSI CIC SUD S</td>
  <td>BARI</td>
  <td>IT</td>
  <td>VIALE ESCRIVA' 28</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CICALA CONFERENCES EVENTS DI A. CIC</td>
  <td>NAPOLI</td>
  <td>IT</td>
  <td>VIA G. FILANGIERI 11</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>COGEST M. &amp; C</td>
  <td>VERONA</td>
  <td>IT</td>
  <td>VICOLO SAN SILVESTRO 6</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>COLLAGE SRL</td>
  <td>PALERMO</td>
  <td>IT</td>
  <td>VIA UMBERTO GIORDANO 37/A</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>180,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>180,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CONGRESSI MEDICI OCULISTI S.R.L.</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>CORSO MONFORTE 15</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>45.310,84</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>45.310,84</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CONSORZIO FUTURO IN RICERCA</td>
  <td>FERRARA</td>
  <td>IT</td>
  <td>VIA SARAGAT 1</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>20.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>20.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>CPA SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA DELLA FARNESINA, 224</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>D.G.M.P. srl</td>
  <td>LA FONTINA</td>
  <td>IT</td>
  <td>VIA CARDUCCI, 62/E</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>DEFLA DI CLAUDIA FORTUNATO</td>
  <td>NAPOLI</td>
  <td>IT</td>
  <td>VIA DEL PARCO MARGHERITA, 49/3</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>9.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>9.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>DYNAMICOM SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA SAN GREGORIO 12</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>36.808,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>36.808,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>E MEETING&amp;CONSULTING SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA MICHELE MERCATI 33</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>E20ECONVEGNI</td>
  <td>TRANI</td>
  <td>IT</td>
  <td>VIA TASSELGRADO 68</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>EDRA LSWR S.P.A.</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA SPADOLINI 7</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>13.680,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>13.680,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ETRUSCA CONVENTIONS S.N.C.</td>
  <td>PERUGIA</td>
  <td>IT</td>
  <td>VIA BONCIARIO 6D</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>4.370,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>4.370,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FONDAZIONE GIORGIO BRUNELLI</td>
  <td>BRESCIA</td>
  <td>IT</td>
  <td>VIA CAMPIANI CELLATICA 77</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FONDAZIONE IRCCS CA' GRANDA</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA FRANCESCO SFORZA 28</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FONDAZIONE RITA LEVI MONTALCINI</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA CATANZARO 9</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FONDAZIONE TELETHON</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA G. SALICETO 5/A</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>10.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FONDAZIONE UMBERTO VERONESI</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>PIAZZA VELASCA 5</td>
  <td>&nbsp;</td>
  <td>12.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>12.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>FORMAZIONE ED EVENTI SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA FRANCO SACCHETTI 127</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>18.300,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>18.300,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>GIAVA SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA FLAMINIA 71</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>I&amp;C srl</td>
  <td>BOLOGNA</td>
  <td>IT</td>
  <td>VIA ANDREA COSTA, 202</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>16.285,80</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>16.285,80</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>IDEA CONGRESS SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA DELLA FARNESINA 224</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>13.100,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>13.100,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>IDEA-Z PROJECT IN PROGRESS SRL</td>
  <td>ABBIATEGRASSO</td>
  <td>IT</td>
  <td>VIALE GIAN GALEAZZO SFORZA 62</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>6.448,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>6.448,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>IMR SRL</td>
  <td>BENEVENTO</td>
  <td>IT</td>
  <td>C. DA BADESSA SNC</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>3.760,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.760,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>INTERPROGRAM ORGANIZER S.A.S. DI MASSIMO CATTANEO &amp; C.</td>
  <td>BARI</td>
  <td>IT</td>
  <td>VIA CALEFATI 89</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ISTITUTO EUROPEO DI ONCOLOGIA SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA RIPAMONTI 435</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>ISTITUTO RICERCHE FARM.MARIO NEGRI</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA GIUSEPPE LA MASA 19</td>
  <td>&nbsp;</td>
  <td>1.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MED STAGE SRL</td>
  <td>CASSANO
  D'ADDA</td>
  <td>IT</td>
  <td>VIA ROSSINI 10</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MEDI K S.R.L.</td>
  <td>PADOVA</td>
  <td>IT</td>
  <td>VIA ALSAZIA 3/1</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>65.872,06</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>65.872,06</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MEETING PLANNER srl</td>
  <td>BARI</td>
  <td>IT</td>
  <td>Via S. Matarrese, 2/4</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>4.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>4.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MGM CONGRESS&nbsp; SRL</td>
  <td>NAPOLI</td>
  <td>IT</td>
  <td>TRAVERSA PIETRAVALLE 8</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MIDI 2007 SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA GERMANICO 42</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>3.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>MOMEDA EVENTI SRL</td>
  <td>BOLOGNA</td>
  <td>IT</td>
  <td>VIA SAN FELICE, 26</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>NADIREX INTERNATIONAL SRL</td>
  <td>PAVIA</td>
  <td>IT</td>
  <td>VIA RIVIERA, 39</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>200,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>200,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>NEW EVENTS DI PERRI LUCIA</td>
  <td>AVERSA</td>
  <td>IT</td>
  <td>VIA FILIPPO SAPORITO 76</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>NIA CONGRESSI SRL</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>VIA CICERONE, 28</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>4.945,22</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>4.945,22</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>OLYMPIA CONGRESSI SRL</td>
  <td>FIUMICINO</td>
  <td>IT</td>
  <td>Via Copenhagen 18/F</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>OSPEDALE PEDIATRICO BAMBIN GESU'</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>PIAZZA S. ONOFRIO, 4</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>4.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>4.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>PLANNING CONGRESSI SRL</td>
  <td>BOLOGNA</td>
  <td>IT</td>
  <td>VIA GUELFA, 9</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>27.207,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>27.207,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>PREX S.p.A.</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA FAVA 25</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>28.908,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>28.908,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>QUICKLINE SAS</td>
  <td>TRIESTE</td>
  <td>IT</td>
  <td>Via Santa Caterina da Siena, 3</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>11.331,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>11.331,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>REGIA CONGRESSI SRL</td>
  <td>FIRENZE</td>
  <td>IT</td>
  <td>VIA A. CESALPINO 5/B</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>REUNION SOCIETA' CONSORTILE A R.L.</td>
  <td>BOLOGNA</td>
  <td>IT</td>
  <td>VIA ANTONIO CANOVA 16/20</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>5.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>SIDO-SOC ITALIANA DI ORTODONZIA</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA PIETRO GIAGGIA 1</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.400,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.400,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>SUMMEET SRL</td>
  <td>VARESE</td>
  <td>IT</td>
  <td>VIA P. MASPERO 5</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>TOP CONGRESS &amp; INCENTIVE TRAVEL</td>
  <td>SALERNO</td>
  <td>IT</td>
  <td>Via Luigi Guercio, 58</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UNIV. DEGLI STUDI LA SAPIENZA</td>
  <td>ROMA</td>
  <td>IT</td>
  <td>P.ZLE ALDO MORO, 5</td>
  <td>&nbsp;</td>
  <td>3.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UNIV.STUDI DI URBINO CARLO BO</td>
  <td>URBINO</td>
  <td>IT</td>
  <td>VIA SAFFI 2</td>
  <td>&nbsp;</td>
  <td>18.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>18.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UNIVERSITA' DEGLI STUDI DI L'AQUILA</td>
  <td>L'AQUILA</td>
  <td>IT</td>
  <td>VIA GIOVANNI FALCONE 25</td>
  <td>&nbsp;</td>
  <td>2.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UPDATE INTERNATIONAL CONGRESS SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA DEI CONTARINI 7</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>2.250,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>2.250,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UPDATE INTERNATIONAL CONGRESS SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA DEI CONTARINI 7</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.000,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>UVET AMERICAN EXPRESS SPA</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>BASTIONI DI PORTA VOLTA 10</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.044,42</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.044,42</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>VIAGGI PANDOSIA SNC</td>
  <td>MENDICINO</td>
  <td>IT</td>
  <td>VIA PASQUALI 177/179</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>1.500,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>1.500,00</td>
 </tr>
<tr>
  <td>HCO</td>
  <td>VICTORY PROJECT CONGRESSI SRL</td>
  <td>MILANO</td>
  <td>IT</td>
  <td>VIA CARLO POMA 2</td>
  <td>&nbsp;</td>
  <td>0</td>
  <td>3.000,00</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>0</td>
  <td>&nbsp;</td>
  <td>3.000,00</td>
 </tr>

		   
            </tbody>
        </table>

        <p>&nbsp;</p>
        <table class="tablesorter4">
		
            <thead>
                <tr>
                    <td colspan="14" data-sorter="false" style="text-align: center;">AGGREGATE DISCLOSURE - <span style="color:red;">PUBBLICAZIONE SU BASE AGGREGATA</span></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>R&D</td>
                    <td colspan="11">
						Transfers of Value re Research & Development as defined - Article 3.04 and Schedule 1 CE<br /><br />
						<span style="color:red;">Trasferimenti di valore per Ricerca&Sviluppo come da definizione � Punto 5.8 e 5.9 e Allegato 2 CD</span>
					</td>
                    <td>1.549.815,31</td>
                    <td>0</td>
                </tr>
            </tbody>
        </table>
    </form>
	</div>
	
	</body>
</html>
