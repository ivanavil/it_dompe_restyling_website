<%@ Page Language="VB" ClassName="myDefault" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>

<script runat="server">
    Sub Page_Load()
        If CheckBrowserCaps() Then Response.End()
    End Sub
    
    Function CheckBrowserCaps() As Boolean
        Dim _strUserAgent As String = Request.ServerVariables("HTTP_USER_AGENT").ToLower
        If _strUserAgent.Contains("googlebot") Then Return True
        If _strUserAgent.Contains("yahoocrawler") Then Return True
        If _strUserAgent.Contains("msnbot") Then Return True
        If _strUserAgent.Contains("slurp") Then Return True
        If _strUserAgent.Contains("altavista") Then Return True
		
        Return False
    End Function
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>www.dompe.com | Trasferimento di valori | Nota Metodologica</title>
    <script type="text/javascript" src="http://www.dompe.com//ProjectDOMPE/_js/jquery.js"></script>
    <link rel="stylesheet" href="http://www.dompe.com/projectdompe/_css/style.css" />
    <style>
        body
        {
            padding: 30px;
            color: #000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a href="http://www.dompe.com" title="Domp� Corporate website - Home Page">
                <img alt="Domp� Corporate website - Home Page" src="http://www.dompe.com/projectdompe/_slice/logo.gif" /></a>
        </div>
        <h1 style="text-align: center; font-size: 20px;">Pubblicazione dei trasferimenti di valore ad operatori sanitari e organizzazioni sanitarie da parte di Domp� farmaceutici S.p.A.<br />
            <br />
            Nota Metodologica
        </h1>
        <p>
            I dati relativi ai trasferimenti di valore a Operatori Sanitari e Organizzazioni Sanitarie presenti nella sezione �<a href="http://www.dompe.com/trasferimenti-di-valore/" title="www.dompe.com">Trasferimenti di valore</a>� del sito <a href="http://www.dompe.com" title="www.dompe.com">www.dompe.com</a> sono stati pubblicati nel rispetto delle regole e secondo le definizioni contenute nel Codice Deontologico di Farmindustria in materia di Trasparenza<sup>1</sup>.
            <br />
        </p>
        <p>
            Tale Codice ha recepito per l�Italia il �<em>Codice EFPIA sulla pubblicazione dei trasferimenti di valore da parte delle aziende farmaceutiche a favore di Operatori Sanitari e Organizzazioni Sanitarie</em>�<sup>2</sup>.
        </p>
        <p>
            Il Gruppo Domp�, impegnato nello sviluppo di soluzioni terapeutiche innovative per i pazienti in tutto il mondo, persegue la propria focalizzazione in ricerca relazionandosi in maniera trasparente con gli stakeholders e i pazienti, secondo un�identit� distintiva fatta di cultura, storia e valori che guidano l�agire aziendale.
        </p>
        <p>
            Per questo tutti i trasferimenti di valore da parte di Domp� a Operatori Sanitari e Organizzazioni Sanitarie - in particolare quelli effettuati nell�ambito dell�attivit� di Ricerca e Sviluppo, di corsi ed eventi scientifici ECM e delle diverse iniziative di collaborazione scientifica � hanno come scopo primario la possibilit� di offrire ai Pazienti soluzione terapeutiche sempre pi� sicure ed efficaci.
        </p>
        <h3>Modalit� di pubblicazione dei dati</h3>
        <p>
		Gli importi pubblicati si riferiscono ai trasferimenti di valore a Operatori Sanitari e Organizzazioni Sanitarie che operano o che siano domiciliati in Italia e negli altri paesi europei in cui trova applicazione il �<em>Codice EFPIA sulla pubblicazione dei trasferimenti di valore da parte delle aziende farmaceutiche a favore di Operatori Sanitari e Organizzazioni Sanitarie</em>� sopra menzionato.
        </p>
        <p>
            Per la pubblicazione dei suddetti dati Domp� ha scelto di adottare un criterio di competenza<sup>3</sup>.
        </p>
        <p>
            Gli importi relativi ai trasferimenti di valore sono espressi in Euro, devono intendersi al netto dell�IVA (o dell�equivalente tassazione del paese di appartenenza degli Operatori Sanitari/Organizzazioni Sanitarie), salvo che per le transazioni per le quali l�IVA sia indetraibile in base alla normativa fiscale applicabile e comprendono invece eventuali ritenute d�acconto.  
        </p>
        <p>
            Per i trasferimenti di valore ad Operatori Sanitari pubblicati su base individuale (ove sia presente il nominativo dell�Operatore Sanitario oltre all�importo) Domp� ha preventivamente acquisito, operazione per operazione, il consenso alla pubblicazione dei dati personali da parte dei soggetti interessati secondo quanto prescritto dalla normativa applicabile in materia di tutela dei dati personali.
        </p>
        <p>
            I trasferimenti di valore ad Operatori Sanitari e Organizzazioni Sanitarie effettuati nell�ambito di attivit� di Ricerca e Sviluppo sono stati pubblicati, come previsto dalle regole EFPIA e Farmindustria, in forma aggregata<sup>4</sup>. 
        </p>
        <p style="text-align: right;">Milano, giugno 2017.</p>
        <p>&nbsp;</p>
        <ol>
            <li>Farmindustria � Par. 5 del "<em>Codice Deonologico Farmindustria</em>" (<a target="_blank" href="http://www.farmindustria.it" title="www.farmindustria.it">www.farmindustria.it</a>).</li>
            <li>EFPIA (European Federation of Pharmaceutical Industries and Associations) - "<em>EFPIA Code on Disclosure of Transfers of Value from Pharmaceutical Companies to Healthcare Professionals and Healthcare Organisations</em>" - (<a target="_blank" href="http://transparency.efpia.eu" title="transparency.efpia.eu">transparency.efpia.eu</a>).</li>
            <li>Vedasi il Paragrafo 5.3 del Codice Deontologico di Farmindustria.</li>
            <li>Vedasi il Paragrafo 5.8 del Codice Deontologico di Farmindustria.</li>
        </ol>
    </form>
</body>
</html>
