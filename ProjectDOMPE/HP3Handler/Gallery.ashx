﻿<%@ WebHandler Language="VB" Class="Download" %>

Imports System
Imports System.Web
Imports Healthware.HP3.Core.Utility
Imports Healthware
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues

Public Class Download : Implements IHttpHandler
    Dim pKey As Integer = 0
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pk = context.Request.QueryString("pk")
        
        If String.IsNullOrWhiteSpace(pk) Then context.Response.Write("") : context.Response.End()
        
        context.Response.ContentType = "text/plain"
        DoDownload(context)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Protected Sub DoDownload(ByRef context As HttpContext)
        pKey = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(context.Request.QueryString("pk"))
        Dim galleryId As Integer = 0
        Integer.TryParse(pKey, galleryId)
        If galleryId > 0 Then
            Dim filePath = context.Server.MapPath(String.Format("~/HP3image/Photogallery/{0}/gallery.zip", pKey.ToString))
            If System.IO.File.Exists(filePath) Then
                context.Response.Clear()
                context.Response.ContentType = "application/octet-stream"
                context.Response.AddHeader("Content-Disposition", "attachment; filename=gallery.zip")
                context.Response.TransmitFile(filePath)
            Else
                context.Response.Write("") : context.Response.End()
            End If
        End If
    End Sub
End Class