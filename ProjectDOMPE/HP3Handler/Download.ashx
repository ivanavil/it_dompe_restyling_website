﻿<%@ WebHandler Language="VB" Class="Download"   %>

Imports System
Imports System.Web
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.Web
Imports Healthware.HP3.Core.Web.ObjectValues
Imports Healthware
Imports Healthware.HP3.Core.User
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues

Public Class Download : Implements IHttpHandler
    Dim pKey As Integer = 0
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
    
        Dim pk = context.Request.QueryString("pk")
        context.Response.ContentType = "text/plain"
        DoDownload(context)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Protected Sub DoDownload(ByRef context As HttpContext)
        pKey = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(context.Request.QueryString("pk"))
        'System.Web.HttpContext.Current.Response.Write(pKey)
        ' System.Web.HttpContext.Current.Response.end()
        'Dim strUserId As String = context.Request(Healthware.HSP.Keys.LabelKeys.USERID_PARAMETER)
        'If String.IsNullOrWhiteSpace(strUserId) Then Return
        'strUserId = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(strUserId)

        'Dim userId As Integer = 0
        'If Not Integer.TryParse(strUserId, userId) Then Return

        'Dim strKey As String = context.Request(Healthware.HSP.Keys.LabelKeys.PRIMARYKEY_PARAMETER)
        'If String.IsNullOrWhiteSpace(strKey) Then Return
        'strKey = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(strKey)

        'Dim pKey As Integer = 0
        'If Not Integer.TryParse(strKey, pKey) Then Return

        Dim dwnlMan As New DownloadManager()
        Dim _accMan = New AccessService()
        Dim _ticket = _accMan.GetTicket()
        Dim dwnlVal = dwnlMan.Read(New ContentIdentificator(pKey))
        If Not dwnlVal Is Nothing Then

            'If Healthware.HSP.Helper.UserHelper.CanAccessContent(dwnlVal) Then

            Dim filename = String.Format("{0}.{1}", dwnlVal.Key.PrimaryKey, dwnlVal.DownloadTypeCollection(0).DownloadType.Label)
            Dim filePath = String.Format("~/hp3download/{0}", filename)
            filePath = context.Server.MapPath(filePath)

            If System.IO.File.Exists(filePath) Then
                If Not _ticket Is Nothing Then
                    Dim trc As New TraceService
                    Dim traceVal As New TraceValue
                    traceVal.KeySite.Id = 46
                    traceVal.KeyContent.Id = dwnlVal.Key.Id
                    traceVal.KeySiteArea.Id = context.Request("sa")
                    traceVal.KeyContentType.Id = context.Request("ct")
                    traceVal.KeyEvent.Id = 26
                    traceVal.KeyUser.Id = _ticket.User.Key.Id
                    traceVal.KeyLanguage.Id = context.Request("lk")
                    traceVal.CustomField = context.Request("cS")
                    traceVal.DateInsert = System.DateTime.Now
                    traceVal.IdSession = _ticket.IdSession
                    trc.Create(traceVal)
                End If

                'Dim siteAreaId As Integer = 0
                'If Not String.IsNullOrWhiteSpace(context.Request(Keys.LabelKeys.SITEAREA_PARAMETER)) Then
                '    Integer.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(context.Request(Keys.LabelKeys.SITEAREA_PARAMETER)), siteAreaId)
                'End If

                'Dim contentTypeId As Integer = 0
                'If Not String.IsNullOrWhiteSpace(context.Request(Keys.LabelKeys.CONTENTYPE_PARAMETER)) Then
                '    Integer.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(context.Request(Keys.LabelKeys.CONTENTYPE_PARAMETER)), contentTypeId)
                'End If

                'Dim themeId As Integer = 0
                'If Not String.IsNullOrWhiteSpace(context.Request(Keys.LabelKeys.THEMEID_PARAMETER)) Then
                '    Integer.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(context.Request(Keys.LabelKeys.THEMEID_PARAMETER)), themeId)
                'End If

                'TraceDownload(dwnlVal.Key, Healthware.HSP.Keys.EventKeys.Download, "download file", userId, contentTypeId, siteAreaId, themeId)

                Context.Response.Clear()
                Context.Response.ContentType = "application/octet-stream"
                Context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}.{1}", _
                                                                                HttpUtility.HtmlDecode(dwnlVal.Title.Replace(" ", "_").Replace(",", "")), _
                                                                                  dwnlVal.DownloadTypeCollection(0).DownloadType.Label))
																				

                context.Response.TransmitFile(filePath)
            End If
            'End If
        End If

    End Sub
  
    
End Class