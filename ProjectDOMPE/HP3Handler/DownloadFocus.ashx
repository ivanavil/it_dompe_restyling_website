﻿<%@ WebHandler Language="VB" Class="Download" %>
Imports Healthware
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.User
Imports Healthware.HP3.Core.User.ObjectValues
Imports Healthware.HP3.Core.User.AccessService
Imports Healthware.HP3.Core.Web
Imports Healthware.HP3.Core.Web.ObjectValues
Imports System
Imports System.Web

Public Class Download : Implements IHttpHandler
    Dim fileN As String = String.Empty
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pk = context.Request.QueryString("pk")
        context.Response.ContentType = "text/plain"
        DoDownload(context)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
	
	Dim _accMan = New AccessService()
	Dim _ticket = _accMan.GetTicket()
    
    Protected Sub DoDownload(ByRef context As HttpContext)
        fileN = context.Request("f")
       
        'context.Response.Write("Requested File: " & fileN)
        Dim accSrv As New AccessService
		
		Dim _isPrivate as Boolean = False
		
		Dim filePath as String = String.Empty			
		If fileN.ToLower.IndexOf("downloadables") > -1 Then
			filePath = "~/hp3Image/" & fileN
		Else
			filePath = "~/hp3download/focusonf/" & fileN
			_isPrivate = True
		End If
		filePath = context.Server.MapPath(filePath)
		
		
		If _isPrivate Then
			If Not accSrv.HasUserRole(New RoleIdentificator(65)) Then context.Response.Write("File not found.") : context.Response.End
		End If
		
		If System.IO.File.Exists(filePath) Then
			Dim trc As New TraceService
			Dim traceVal As New TraceValue
			traceVal.KeySite.Id = 46
			traceVal.KeyContent.Id = 0
			traceVal.KeySiteArea.Id = context.Request("sa")
			traceVal.KeyContentType.Id = context.Request("ct")
			traceVal.KeyEvent.Id = 26
			traceVal.KeyUser.Id = _ticket.User.Key.Id
			traceVal.KeyLanguage.Id = context.Request("lk")
			traceVal.CustomField = fileN
			traceVal.DateInsert = System.DateTime.Now
			traceVal.IdSession = _ticket.IdSession
			trc.Create(traceVal)

			context.Response.Clear()
			context.Response.ContentType = "application/octet-stream"
			context.Response.AddHeader("Content-Disposition", "attachment; filename=" & fileN.Replace(" ", "-"))

			context.Response.TransmitFile(filePath)
		Else
			context.Response.Write("Required file not available!")
			context.Response.End
		End If		
			
        
        'If accSrv.HasUserRole(New RoleIdentificator(65)) Then
        'Else
        '    context.Response.Write("")
        'End If
    End Sub
End Class