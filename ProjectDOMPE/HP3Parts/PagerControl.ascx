﻿<%@ Control Language="VB" 
    EnableViewState="false"
    ClassName="PagerControlPriligy" %>
<%@ Import Namespace="System.ComponentModel" %>

<script runat="server">
#Region "Constants"
    Private Const DEFAULT_CURRENT_CSS_CLASS As String = "current"
    Private Const DEFAULT_PAGER_SIZE As Integer = 8
    Private Const DEFAULT_CONTAINER_CSS_CLASS As String = "pager"
    Private Const DEFAULT_FIRST_ITEM_TEXT As String = "«"
    Private Const DEFAULT_LAST_ITEM_TEXT As String = "»"
    Private Const DEFAULT_NEXT_ITEM_TEXT As String = "Next Page"
    Private Const DEFAULT_PREVIOUS_ITEM_TEXT As String = "Previous Page"
    Private Const DEFAULT_START_PAGE As Integer = 1
    Private Const DEFAULT_TOTAL_PAGES As Integer = 1
    Private Const DEFAULT_PAGE_LINK_PARAMETER As String = "pg"
    Private Const DEFAULT_PAGE_LINK_FORMAT As String = ""
    
    Private Const DEFAULT_SHOW_LEGEND As Boolean = False
    Private Const DEFAULT_LEGEND_FORMAT As String = "Page {0} of {1}"
#End Region
    
#Region "Enums"
    Public Enum Modes
        Postback
        Link
    End Enum
#End Region
    
#Region "Members"
    Protected _mode As Modes = Modes.Postback
    
    Protected _currentPageIndex As Integer = DEFAULT_START_PAGE
    Protected _pagerSize As Integer = DEFAULT_PAGER_SIZE
    Protected _containerCssClass As String = DEFAULT_CONTAINER_CSS_CLASS
    Protected _currentItemCssClass As String = DEFAULT_CURRENT_CSS_CLASS
    Protected _totalPages As Integer = DEFAULT_TOTAL_PAGES
    
    Protected _goToFirstItemText As String = DEFAULT_FIRST_ITEM_TEXT
    Protected _goToLastItemText As String = DEFAULT_LAST_ITEM_TEXT
    Protected _showGoToFirstItemButton As Boolean = False
    Protected _showGoToLastItemButton As Boolean = False
    
    Protected _goToNextItemText As String = DEFAULT_NEXT_ITEM_TEXT
    Protected _goToPreviousItemText As String = DEFAULT_PREVIOUS_ITEM_TEXT
    Protected _showGoToNextItemButton As Boolean = True
    Protected _showGoToPreviousItemButton As Boolean = True
    
    Protected _goToFirstItemImageUrl As String = String.Empty
    Protected _goToLastItemImageUrl As String = String.Empty
    Protected _goToNextItemImageUrl As String = String.Empty
    Protected _goToPreviousItemImageUrl As String = String.Empty
    
    Protected _pageLinkParameter As String = DEFAULT_PAGE_LINK_PARAMETER
    Protected _pageLinkFormat As String = DEFAULT_PAGE_LINK_FORMAT
    
    Protected _firstTooltip As String = String.Empty
    Protected _lastTooltip As String = String.Empty
    Protected _previousTooltip As String = String.Empty
    Protected _nextTooltip As String = String.Empty
    Protected _showTooltip As Boolean = True
    
    Protected _disableLinkOnCurrentPage As Boolean = True
    Protected _useStrongTagForCurrentPage As Boolean = True
    Protected _showLegend As Boolean = DEFAULT_SHOW_LEGEND
    Protected _legendFormat As String = DEFAULT_LEGEND_FORMAT
    
    Protected _isFirstMoreVisible As Boolean = False
    Protected _isLastMoreVisible As Boolean = False
    Protected _showGoToLastMore As Boolean = True
    Protected _showGoToFirstMore As Boolean = True
#End Region

#Region "Properties"
    Public Property Mode() As Modes
        Get
            Return _mode
        End Get
        Set(ByVal value As Modes)
            _mode = value
        End Set
    End Property
    
    Public Property CurrentPageIndex() As Integer
        Get
            Return _currentPageIndex
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then
                _currentPageIndex = 1
            Else
                _currentPageIndex = value
            End If
        End Set
    End Property
    
    Public Property TotalPages() As Integer
        Get
            Return _totalPages
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then
                _totalPages = 1
            Else
                _totalPages = value
            End If
        End Set
    End Property
    
    ''' <summary>
    ''' Pager size. Minimum admitted value is 3.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PagerSize() As Integer
        Get
            Return _pagerSize
        End Get
        Set(ByVal value As Integer)
            _pagerSize = value
        End Set
    End Property
    
    Public Property ContainerCssClass() As String
        Get
            Return _containerCssClass
        End Get
        Set(ByVal value As String)
            _containerCssClass = value
        End Set
    End Property
    
    Public Property CurrentItemCssClass() As String
        Get
            Return _currentItemCssClass
        End Get
        Set(ByVal value As String)
            _currentItemCssClass = value
        End Set
    End Property
    
    Public Property GoToFirstItemText() As String
        Get
            Return _goToFirstItemText
        End Get
        Set(ByVal value As String)
            _goToFirstItemText = value
        End Set
    End Property
    
    Public Property GotoLastItemText() As String
        Get
            Return _goToLastItemText
        End Get
        Set(ByVal value As String)
            _goToLastItemText = value
        End Set
    End Property
    
    Public Property DisableLinkOnCurrentPage() As Boolean
        Get
            Return _disableLinkOnCurrentPage
        End Get
        Set(ByVal value As Boolean)
            _disableLinkOnCurrentPage = value
        End Set
    End Property
    
    Public Property PageLinkParameter() As String
        Get
            Return _pageLinkParameter
        End Get
        Set(ByVal value As String)
            _pageLinkParameter = value
        End Set
    End Property
    Public Property PageLinkFormat() As String
        Get
            Return _pageLinkFormat
        End Get
        Set(ByVal value As String)
            _pageLinkFormat = value
        End Set
    End Property
    
    Public Property FirstTooltip() As String
        Get
            Return _firstTooltip
        End Get
        Set(ByVal value As String)
            _firstTooltip = value
        End Set
    End Property
    Public Property LastTooltip() As String
        Get
            Return _lastTooltip
        End Get
        Set(ByVal value As String)
            _lastTooltip = value
        End Set
    End Property
    Public Property PreviousTooltip() As String
        Get
            Return _previousTooltip
        End Get
        Set(ByVal value As String)
            _previousTooltip = value
        End Set
    End Property
    Public Property NextTooltip() As String
        Get
            Return _nextTooltip
        End Get
        Set(ByVal value As String)
            _nextTooltip = value
        End Set
    End Property
    Public Property ShowTooltip() As Boolean
        Get
            Return _showTooltip
        End Get
        Set(ByVal value As Boolean)
            _showTooltip = value
        End Set
    End Property
    
    Public Property GoToFirstItemImageUrl() As String
        Get
            Return _goToFirstItemImageUrl
        End Get
        Set(ByVal value As String)
            _goToFirstItemImageUrl = value
        End Set
    End Property
    Public Property GoToLastItemImageUrl() As String
        Get
            Return _goToLastItemImageUrl
        End Get
        Set(ByVal value As String)
            _goToLastItemImageUrl = value
        End Set
    End Property
    Public Property GoToNextItemImageUrl() As String
        Get
            Return _goToNextItemImageUrl
        End Get
        Set(ByVal value As String)
            _goToNextItemImageUrl = value
        End Set
    End Property
    Public Property GoToPreviousItemImageUrl() As String
        Get
            Return _goToPreviousItemImageUrl
        End Get
        Set(ByVal value As String)
            _goToPreviousItemImageUrl = value
        End Set
    End Property
        
    Public Property ShowLegend() As Boolean
        Get
            Return _showLegend
        End Get
        Set(ByVal value As Boolean)
            _showLegend = value
        End Set
    End Property
    Public Property LegendFormat() As String
        Get
            Return _legendFormat
        End Get
        Set(ByVal value As String)
            _legendFormat = value
        End Set
    End Property
#End Region
    
#Region "Events"
    Public Event PageIndexChanged(ByVal sender As Object, ByVal e As PagerIndexChangedEventArgs)
#End Region
    
#Region "Public methods"
    Public Sub Clear()
        TotalPages = 1
        CurrentPageIndex = 1
        Visible = False
    End Sub
    
    Public Function SetTotalPages(ByVal itemsCount As Integer, ByVal pageSize As Integer) As Integer
        TotalPages = Math.Ceiling(itemsCount / pageSize)
    End Function
#End Region
    
#Region "Control life-cycle"
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Page.RegisterRequiresControlState(Me)
        MyBase.OnInit(e)
    End Sub
    
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        
   
        '13/02/2012
        'If TotalPages <= 1 Then
        '    Me.Visible = False
        '    pdr.Visible = False
        'End If
        If Me.Visible Then LoadData()
    End Sub
    
    Protected Sub Page_DataBinding(ByVal sender As Object, ByVal e As EventArgs) Handles Me.DataBinding
        LoadData()
    End Sub
    
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
    '    If TotalPages <= 1 Then
    '        Me.Visible = False
    '    End If
    'End Sub
        
    Protected Sub PagerDataRepeater_ItemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        If Not String.IsNullOrEmpty(e.CommandName) Then
            Select Case e.CommandName
                Case "FIRST_PAGE"
                    Dim eventArg As PagerIndexChangedEventArgs = New PagerIndexChangedEventArgs(1)
                    RaiseEvent PageIndexChanged(Me, eventArg)
                Case "PREVIOUS_PAGE"
                    Dim argument As Integer = CurrentPageIndex - 1
                    If argument < 1 Then argument = 1
                    Dim eventArg As PagerIndexChangedEventArgs = New PagerIndexChangedEventArgs(argument)
                    RaiseEvent PageIndexChanged(Me, eventArg)
                Case "CHANGE_PAGE"
                    Dim argument As Integer = 1
                    Integer.TryParse(e.CommandArgument, argument)
                    Dim eventArg As PagerIndexChangedEventArgs = New PagerIndexChangedEventArgs(argument)
                    RaiseEvent PageIndexChanged(Me, eventArg)
                Case "NEXT_PAGE"
                    Dim argument As Integer = CurrentPageIndex + 1
                    If argument > TotalPages Then argument = TotalPages
                    Dim eventArg As PagerIndexChangedEventArgs = New PagerIndexChangedEventArgs(argument)
                    RaiseEvent PageIndexChanged(Me, eventArg)
                Case "LAST_PAGE"
                    Dim eventArg As PagerIndexChangedEventArgs = New PagerIndexChangedEventArgs(TotalPages)
                    RaiseEvent PageIndexChanged(Me, eventArg)
            End Select
        End If
    End Sub
#End Region
    
#Region "Private support methods"
    Protected Function GetSelectedItemCssClass(ByVal value As Integer) As String
        If value = CurrentPageIndex Then Return _currentItemCssClass
        Return String.Empty
    End Function

    Private Sub LoadData()
        If _pagerSize <= 2 Then _pagerSize = 3
        If _pagerSize <= 1 Then
            Clear()
        Else
            pdr.DataSource = ComputePagerPage()
            pdr.DataBind()
        End If
        
    End Sub
    
    Private Function ComputePagerPage() As HashSet(Of Integer)
        Dim retCol As HashSet(Of Integer) = New HashSet(Of Integer)
        Dim startPosition As Integer = 1
        Dim pagerOffset As Integer = Math.Floor(_pagerSize / 2)
        Dim pivotPosition As Integer = CurrentPageIndex - pagerOffset
        
        ' Case 1: negative start position
        If pivotPosition > 1 Then startPosition = pivotPosition
        
        ' Case 2: pivot position exceeds total pages number
        If startPosition + _pagerSize - 1 > TotalPages Then
            ' Shift positions
            While startPosition > 1 AndAlso startPosition + _pagerSize - 1 > TotalPages
                startPosition = startPosition - 1
            End While
        End If
        
        For i As Integer = startPosition To startPosition + _pagerSize - 1 Step 1
            If i > TotalPages Then Exit For
            retCol.Add(i)
        Next
        
        _isFirstMoreVisible = Not retCol.Contains(1)
        _isLastMoreVisible = Not retCol.Contains(_totalPages)
        
        Return retCol
    End Function
    
    Protected Overrides Sub LoadControlState(ByVal savedState As Object)
        Dim rawState() As Object = CType(savedState, Object())
        MyBase.LoadControlState(rawState(0))
        _currentPageIndex = 1
        _totalPages = 1
        Integer.TryParse(rawState(1), _currentPageIndex)
        Integer.TryParse(rawState(2), _totalPages)
    End Sub

    Protected Overrides Function SaveControlState() As Object
        Dim rawState() As Object = New Object(3) {}
        rawState(0) = MyBase.SaveControlState()
        rawState(1) = _currentPageIndex
        rawState(2) = _totalPages
        Return rawState
    End Function
    
    Protected Function GetPageLink(ByVal index As Integer) As String
        If index > 0 Then
            If Not String.IsNullOrEmpty(_pageLinkFormat) Then
                Return String.Format(_pageLinkFormat, index)
            Else
                Dim url As String = HttpContext.Current.Request.Url.ToString()
                Dim sb As StringBuilder = New StringBuilder()
                sb.Append(url)
                If Not url.Contains("?") Then
                    sb.Append("?")
                Else
                    sb.Append("&")
                End If
                sb.Append(_pageLinkParameter)
                sb.Append("=")
                sb.Append(Server.UrlEncode(index))
                Return sb.ToString()
            End If
        End If
        Return "javascript:;"
    End Function
#End Region
</script>

<asp:Repeater ID="pdr" OnItemCommand="PagerDataRepeater_ItemCommand" runat="server">
    <HeaderTemplate>
        <div class="<%# _containerCssClass %>">
            <asp:Hyperlink ID="gfl"
                Text='<%# Server.HtmlEncode(_goToFirstItemText) %>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToFirstItemImageUrl), _goToFirstItemImageUrl, String.Empty) %>'
                NavigateUrl='<%# GetPageLink(1)%>'
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                Visible='<%# _showGoToFirstItemButton AndAlso Not (CurrentPageIndex = 1) AndAlso _mode = Modes.Link %>' 
                runat="server" />
            <asp:LinkButton ID="gfb"  CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToFirstItemText) %>'
                Visible='<%# String.IsNullOrEmpty(_goToFirstItemImageUrl) Andalso _showGoToFirstItemButton AndAlso Not (CurrentPageIndex = 1)  AndAlso _mode = Modes.Postback %>' 
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                CommandName="FIRST_PAGE"
                runat="server" />  
            <asp:ImageButton ID="gfi" CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToFirstItemText) %>'
                Visible='<%# Not String.IsNullOrEmpty(_goToFirstItemImageUrl) Andalso _showGoToFirstItemButton AndAlso Not (CurrentPageIndex = 1)  AndAlso _mode = Modes.Postback %>' 
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                CommandName="FIRST_PAGE"
                runat="server" />  
                 
            <asp:Hyperlink ID="gpl"
                Text='<%# Server.HtmlEncode(_goToPreviousItemText) %>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToPreviousItemImageUrl), _goToPreviousItemImageUrl, String.Empty) %>'
                NavigateUrl='<%# GetPageLink(CurrentPageIndex - 1)%>'
                Tooltip='<%# IIf(_showTooltip, _previousTooltip , string.empty) %>'
                Visible='<%# _showGoToPreviousItemButton AndAlso (CurrentPageIndex > 1) AndAlso _mode = Modes.Link %>'
                runat="server" />
            <asp:LinkButton ID="gpb" CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToPreviousItemText) %>'
                Tooltip='<%# IIf(_showTooltip, _previousTooltip , string.empty) %>'
                Visible='<%# String.IsNullOrEmpty(_goToPreviousItemImageUrl) AndAlso _showGoToPreviousItemButton AndAlso (CurrentPageIndex > 1) AndAlso _mode = Modes.Postback %>'
                CommandName="PREVIOUS_PAGE"
                runat="server" />
            <asp:ImageButton ID="gpi" CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToFirstItemText) %>'
                Visible='<%# Not String.IsNullOrEmpty(_goToPreviousItemImageUrl) Andalso _showGoToPreviousItemButton AndAlso Not (CurrentPageIndex = 1)  AndAlso _mode = Modes.Postback %>' 
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToPreviousItemImageUrl), _goToPreviousItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                CommandName="PREVIOUS_PAGE"
                runat="server" />  
                
            <asp:PlaceHolder ID="gfmSection" Visible='<%# _showGoToFirstMore AndAlso _isFirstMoreVisible %>' runat="server">
            <asp:Hyperlink ID="gfml"
                Text='1'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToFirstItemImageUrl), _goToFirstItemImageUrl, String.Empty) %>'
                NavigateUrl='<%# GetPageLink(1)%>'
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                Visible='<%# _isFirstMoreVisible AndAlso _showGoToFirstMore AndAlso Not (CurrentPageIndex = 1)  AndAlso _mode = Modes.Link %>' 
                runat="server" />
            <asp:LinkButton ID="gfmb" CausesValidation ="false"
                Text='1'
                Visible='<%# _isFirstMoreVisible AndAlso _showGoToFirstMore AndAlso Not (CurrentPageIndex = 1)  AndAlso _mode = Modes.Postback %>' 
                Tooltip='<%# IIf(_showTooltip, _firstTooltip , string.empty) %>'
                CommandName="FIRST_PAGE"
                runat="server" />              
                <span>...</span>
            </asp:PlaceHolder>
    </HeaderTemplate>
    <ItemTemplate>
        <%--<%#IIf(txtActPage2.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>--%>
        <asp:LinkButton ID="ilbt" CausesValidation ="false"
            CssClass='<%# GetSelectedItemCssClass(DirectCast(Container.DataItem, Integer)) %>'
            Text='<%# DirectCast(Container.DataItem, Integer) %>'
            Tooltip='<%# IIf(_showTooltip, DirectCast(Container.DataItem, Integer).ToString() , string.empty) %>'
            CommandName="CHANGE_PAGE" 
            CommandArgument='<%# DirectCast(Container.DataItem, Integer) %>'
            Visible='<%# Not (_disableLinkOnCurrentPage AndAlso (DirectCast(Container.DataItem, Integer) = CurrentPageIndex)) AndAlso _mode = Modes.Postback%>'
            runat="server" />
            
        <asp:Hyperlink ID="ilnk"
            CssClass='<%# GetSelectedItemCssClass(DirectCast(Container.DataItem, Integer)) %>'
            Text='<%# DirectCast(Container.DataItem, Integer) %>'
            Tooltip='<%# IIf(_showTooltip, DirectCast(Container.DataItem, Integer).ToString() , string.empty) %>'
            NavigateUrl='<%# GetPageLink(DirectCast(Container.DataItem, Integer))%>'
            Visible='<%# Not (_disableLinkOnCurrentPage AndAlso (DirectCast(Container.DataItem, Integer) = CurrentPageIndex))  AndAlso _mode = Modes.Link %>'
            runat="server" />
            
        <asp:Label ID="ilbl"
            CssClass='<%# GetSelectedItemCssClass(DirectCast(Container.DataItem, Integer)) %>'
            Text='<%# IIf(_useStrongTagForCurrentPage, String.Format("<strong>{0}</strong>", DirectCast(Container.DataItem, Integer)),DirectCast(Container.DataItem, Integer)) %>'
            Visible='<%# Not _useStrongTagForCurrentPage AndAlso _disableLinkOnCurrentPage AndAlso (DirectCast(Container.DataItem, Integer) = CurrentPageIndex) %>'
            runat="server" />
            
        <asp:Literal ID="iltl" 
            Mode="PassThrough"
            Text='<%# String.Format("<strong>{0}</strong>", DirectCast(Container.DataItem, Integer)) %>'
            Visible='<%# _useStrongTagForCurrentPage AndAlso (DirectCast(Container.DataItem, Integer) = CurrentPageIndex) %>'
            runat="server" />
    </ItemTemplate>
    <FooterTemplate>
        <asp:PlaceHolder ID="glmSection" Visible='<%# _showGoToLastMore AndAlso _isLastMoreVisible %>' runat="server">
            <span>...</span>
            <asp:Hyperlink ID="glml"
                Text='<%# TotalPages %>'
                NavigateUrl='<%# GetPageLink(TotalPages)%>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToLastItemImageUrl), _goToLastItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _lastTooltip , string.empty) %>'
                Visible='<%# _isLastMoreVisible AndAlso _showGoToLastMore AndAlso Not (CurrentPageIndex = TotalPages) AndAlso _mode = Modes.Link %>'
                runat="server" />                
            <asp:LinkButton ID="glmb" CausesValidation ="false"
                Text='<%# TotalPages %>'
                Tooltip='<%# IIf(_showTooltip, _lastTooltip , string.empty) %>'
                Visible='<%# _isLastMoreVisible AndAlso _showGoToLastMore AndAlso Not (CurrentPageIndex = TotalPages) AndAlso _mode = Modes.Postback %>'
                CommandName="LAST_PAGE"
                runat="server" />
        </asp:PlaceHolder>
    
            <asp:Hyperlink ID="gnl"
                Text='<%# Server.HtmlEncode(_goToNextItemText) %>'
                NavigateUrl='<%# GetPageLink(CurrentPageIndex + 1)%>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToNextItemImageUrl), _goToNextItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _nextTooltip , string.empty) %>'
                Visible='<%# _showGoToNextItemButton AndAlso (CurrentPageIndex < TotalPages) AndAlso _mode = Modes.Link %>' 
                runat="server" />
            <asp:LinkButton ID="gnb" CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToNextItemText) %>'
                Tooltip='<%# IIf(_showTooltip, _nextTooltip , string.empty) %>'
                Visible='<%# String.IsNullOrEmpty(_goToNextItemImageUrl) AndAlso _showGoToNextItemButton AndAlso (CurrentPageIndex < TotalPages) AndAlso _mode = Modes.Postback %>'
                CommandName="NEXT_PAGE"
                runat="server" />
            <asp:ImageButton ID="gni" CausesValidation ="false"
                AlternateText='<%# Server.HtmlEncode(_goToNextItemText) %>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToNextItemImageUrl), _goToNextItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _nextTooltip , string.empty) %>'
                Visible='<%# Not String.IsNullOrEmpty(_goToNextItemImageUrl) AndAlso _showGoToNextItemButton AndAlso (CurrentPageIndex < TotalPages) AndAlso _mode = Modes.Postback %>'
                CommandName="NEXT_PAGE"
                runat="server" />
                
            <asp:Hyperlink ID="gll"
                Text='<%# Server.HtmlEncode(_goToLastItemText) %>'
                NavigateUrl='<%# GetPageLink(TotalPages)%>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToLastItemImageUrl), _goToLastItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _lastTooltip , string.empty) %>'
                Visible='<%# _showGoToLastItemButton AndAlso Not (CurrentPageIndex = TotalPages) AndAlso _mode = Modes.Link %>'
                runat="server" />                
            <asp:LinkButton ID="glb" CausesValidation ="false"
                Text='<%# Server.HtmlEncode(_goToLastItemText) %>'
                Tooltip='<%# IIf(_showTooltip, _lastTooltip , string.empty) %>'
                Visible='<%# String.IsNullOrEmpty(_goToLastItemImageUrl) AndAlso _showGoToLastItemButton AndAlso Not (CurrentPageIndex = TotalPages) AndAlso _mode = Modes.Postback %>'
                CommandName="LAST_PAGE"
                runat="server" />
            <asp:ImageButton ID="gli"  CausesValidation ="false"
                AlternateText='<%# Server.HtmlEncode(_goToLastItemText) %>'
                ImageUrl='<%# IIf(Not String.IsNullOrEmpty(_goToLastItemImageUrl), _goToLastItemImageUrl, String.Empty) %>'
                Tooltip='<%# IIf(_showTooltip, _lastTooltip , string.empty) %>'
                Visible='<%# Not String.IsNullOrEmpty(_goToLastItemImageUrl) AndAlso _showGoToLastItemButton AndAlso Not (CurrentPageIndex = TotalPages) AndAlso _mode = Modes.Postback %>'
                CommandName="LAST_PAGE"
                runat="server" />
                
            <asp:PlaceHolder ID="legendPlaceholder" Visible='<%# _showLegend %>' runat="server">
                <span><asp:Literal ID="legendLiterla" Text='<%# String.Format(_legendFormat, _currentPageIndex, _totalPages) %>' runat="server" /></span>
            </asp:PlaceHolder>
        </div>
    </FooterTemplate>
</asp:Repeater>
