﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Dim targetlnk As String = ""
    Dim idLingua As Integer

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        idLingua = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        If Me.BusinessMasterPageManager.GetContent.Id > 0 Then
            If Not Page.IsPostBack Then
           
                Dim so As New ContentExtraSearcher
                Dim coll As ContentExtraCollection = Nothing
                so.key.Id = Me.BusinessMasterPageManager.GetContent.Id
                so.KeySite = Me.PageObjectGetSite
                so.key.IdLanguage = Me.PageObjectGetLang.Id
                so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress
                so.SortOrder = ContentGenericComparer.SortOrder.DESC
                so.SortType = ContentGenericComparer.SortType.ByData

     
                          
                coll = Me.BusinessContentExtraManager.Read(so)
                If Not coll Is Nothing AndAlso coll.Any Then
                    SliderImmagini.Text = coll(0).FullTextComment
               
                End If
            End If
        End If
    End Sub

        
</script>
        <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/Dompe.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/elastislide.css" />

        <noscript>
			<style>
				.es-carousel ul{
					display:block;
				}		
			</style>
		    </noscript>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>



            <div class="container">
			
			<div class="content">
				
				<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
                                <asp:Literal ID="SliderImmagini" runat="server"></asp:Literal>
									
								</ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->
			
			</div><!-- content -->
		</div><!-- container -->



        <script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
    	<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--%>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.elastislide.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/gallery.js"></script>   