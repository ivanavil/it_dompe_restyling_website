<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Dompe" %>

<script language="vb" runat="server"> 
    
    Protected _siteFolder As String = String.Empty
    'Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _currentTheme As ThemeValue = Nothing
    Protected _currentCT As ContentTypeIdentificator = Nothing
    Protected _urlHome As String = ""
    Protected _totItem As Integer = 0
    Private _siteId As Integer = 46
    Private totMenu As Integer = 0
    Private _Father As Integer = 0
    Private CSSClass As String = "subMenu"
    Private _langCode As String = String.Empty
    Private _langId As Integer = 0
    Protected strClose As String = ""
    Public accessService As New AccessService()
    Protected strLogin As String = ""
    Protected _stringOpenlog As String = ""
    Private _isMobile As Boolean = False
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        '_currentTheme = Me.PageObjectGetTheme
        _currentCT = Me.PageObjectGetContentType
        '_langCode = Me.PageObjectGetLang.Code.ToUpper
        _langId = Me.PageObjectGetLang.Id
        _urlHome = Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idHomeTheme, _langId)
        _siteId = Me.BusinessMasterPageManager.GetSite.Id
        
        strClose = Me.BusinessDictionaryManager.Read("DMP_Close", _langId, "DMP_Close")
        strLogin = Me.BusinessDictionaryManager.Read("DMP_Login", _langId, "DMP_Login")
    End Sub
	
    Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
        Return False
    End Function
	
	
    Sub manageReservedArea(sender As Object, e As EventArgs)
        Dim strUrl As String = ddlReservedArea.SelectedItem.Value
        If Not String.IsNullOrEmpty(strUrl) Then Response.Redirect(strUrl, False)
    End Sub
    
    Sub page_load()
        _isMobile = IsMobileDevice()
        
        Dim thmV As New ThemeValue
        
        thmV = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
        
        If thmV Is Nothing Then
            Response.Redirect("/", True)
        End If
        
        
        _currentTheme = thmV
        _stringOpenlog = Request.QueryString("act")
		
        Dim ctrlPath As String = String.Format("/{0}/HP3Common/search.ascx", Me._siteFolder)
        LoadSubControl(ctrlPath, Me.phSearch)
        LoadSubControl("/ProjectDOMPE/HP3Service/SocialNetwork.ascx", Me.plhSocialNetwork)
        Dim ctrlLog As String = String.Format("/{0}/HP3Common/login.ascx", Me._siteFolder)
        LoadSubControl(ctrlLog, Me.phlLogin)
		
        phlLogin.Visible = True
        linkLogin.Visible = True
        linkLogout.Visible = False
        If Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Or _
                Me.BusinessAccessService.IsAuthenticated Or _
                Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idMedicalHome Or _
                Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idArticoloMedical _
            Then
            phlLogin.Visible = False
            linkLogin.Visible = False
        End If
        If Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idMedicalHome And Not Me.BusinessAccessService.IsAuthenticated Then
            phlLogin.Visible = True
            linkLogin.Visible = True
        End If
		
      
        If accessService.IsAuthenticated() Then
            If Me.ObjectTicket.User.Key.Id > 0 Then
                linkLogout.Visible = True
                linkLogout.Text = Me.BusinessDictionaryManager.Read("DMP_LogOut", _langId, "Logout")
                phlLogin.Visible = False
                linkLogin.Visible = False
				
					
                If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(78)) Then
                    ltlPrivateArea.Visible = False
                    ddlReservedArea.Visible = True
                    ddlReservedArea.Items.Clear()
                    
                    Select Case _langId
                        Case 1 'Italiano
                            ddlReservedArea.Items.Add(New ListItem("Aree riservate", ""))
                            ddlReservedArea.Items.Add(New ListItem("Area riservata alla stampa", "/area-press-welcome-page/"))
                            ddlReservedArea.Items.Add(New ListItem("Area riservata al medico", "/area-medico-welcome-page/"))
                            ddlReservedArea.Items.Add(New ListItem("Area riservata ai partner", "/partner-area-welcome-page/"))
                            
                        Case 2 'Inglese
                            ddlReservedArea.Items.Add(New ListItem("Reserved Areas", ""))
                            ddlReservedArea.Items.Add(New ListItem("Press reserved area", "/press-area-welcome-page/"))
                            ddlReservedArea.Items.Add(New ListItem("HCP reserved area", "/hcp-area-welcome-page/"))
                            ddlReservedArea.Items.Add(New ListItem("Partner reserved area", "/partner-area-welcome-page/"))
                            
                        Case 3 'Albanian
                            ddlReservedArea.Items.Add(New ListItem("Seksione t� rezervuara", ""))
                            ddlReservedArea.Items.Add(New ListItem("Seksion i rezervuar p�r shtypin", "/seksion-per-shtypin/"))
                            ddlReservedArea.Items.Add(New ListItem("Seksion i rezervuar p�r mjekun", "/seksion-per-mjekun/"))
                            ddlReservedArea.Items.Add(New ListItem("Seksion i rezervuar p�r partner�t", "/seksion-per-partneret/"))
							
                        Case 4 'Spanish
                            ddlReservedArea.Items.Add(New ListItem("�reas reservadas", ""))
                            ddlReservedArea.Items.Add(New ListItem("�rea reservada para la prensa", "/p�gina-de-bienvenida-prensa/"))
                            ddlReservedArea.Items.Add(New ListItem("�rea reservada para los medicos", "/p�gina-bienvenida-area-medico/"))
                            ddlReservedArea.Items.Add(New ListItem("�rea reservada para los socios", "/p�gina-de-bienvenida-socios/"))							
                            
                    End Select
					
                    'If _langId = 1 Then 'Italiano
                    '	ddlReservedArea.Items.Add(New ListItem("Aree riservate", ""))
                    '	ddlReservedArea.Items.Add(New ListItem("Area riservata alla stampa", "/area-press-welcome-page/"))
                    '	ddlReservedArea.Items.Add(New ListItem("Area riservata al medico", "/area-medico-welcome-page/"))
                    '	ddlReservedArea.Items.Add(New ListItem("Area riservata ai partner", "/partner-area-welcome-page/"))
						
                    'ElseIf _langId = 2 Then 'Inglese
                    '	ddlReservedArea.Items.Add(New ListItem("Reserved Areas", ""))
                    '	ddlReservedArea.Items.Add(New ListItem("Press reserved area", "/press-area-welcome-page/"))
                    '	ddlReservedArea.Items.Add(New ListItem("HCP reserved area", "/hcp-area-welcome-page/"))
                    '                   ddlReservedArea.Items.Add(New ListItem("Partner reserved area", "/partner-area-welcome-page/"))
                    '               ElseIf _langId = 3 Then 'Inglese
                    '                   ddlReservedArea.Items.Add(New ListItem("Reserved Areas", ""))
                    '                   ddlReservedArea.Items.Add(New ListItem("Press reserved area", "/press-area-welcome-page/"))
                    '                   ddlReservedArea.Items.Add(New ListItem("HCP reserved area", "/hcp-area-welcome-page/"))
                    '                   ddlReservedArea.Items.Add(New ListItem("Partner reserved area", "/partner-area-welcome-page/"))
                    '               End If
                    ddlReservedArea.SelectedIndex = 0
					
                Else
                    ltlPrivateArea.Visible = True
                    ddlReservedArea.Visible = False
					
                    Dim idThmAP As Integer
                    If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(Dompe.Role.RoleMediaPress)) Then idThmAP = Dompe.ThemeConstants.idThemaSezioneGiornalista
                    If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(Dompe.Role.RoleDoctor)) Then idThmAP = Dompe.ThemeConstants.idMedicalHome
                    If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(Dompe.Role.RolePartner)) Then idThmAP = Dompe.ThemeConstants.partnerWelcome
					
                    'private area link in header
                    Dim vo As New ThemeValue
                    vo = Me.BusinessThemeManager.Read(New ThemeIdentificator(idThmAP))
                    If Not vo Is Nothing AndAlso vo.Key.Id > 0 Then
                        ltlPrivateArea.Text = String.Format("<a href='{0}' class='link-reserved' title='{1}'>{2} </a> ", GetLinkSL(vo), Me.BusinessDictionaryManager.Read("DMP_areaprivata", _langId, "DMP_areaprivata"), Me.BusinessDictionaryManager.Read("DMP_areaprivata", _langId, "DMP_areaprivata"))
                    End If
                End If
            End If
        End If
        
        'If (_currentTheme.Name <> Dompe.ThemeConstants.Name.Home) Then
        '    Dim ctrlPath1 As String = String.Format("/{0}/HP3Common/BreadCrumbs.ascx", Me._siteFolder)            
        '    LoadSubControl(ctrlPath1, Me.plhBreadCrumbs)
        'End If
        
        If Not (Me.PageObjectGetTheme Is Nothing) AndAlso Me.PageObjectGetTheme.Id > 0 Then
            'LoadHeaderMenu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Header)
            LoadHeader2Menu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
            LoadHeader2HamMenu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
        End If
        'ctrlPath = String.Format("/{0}/HP3Common/top.ascx", Me._siteFolder)
        'LoadSubControl(ctrlPath, Me.phtop)
    
    End Sub
    
    Protected Sub LoadHeader2Menu(ByVal domain As String, ByVal name As String)
        Dim _cacheKey As String = "cacheKeyHeaderMenu:" & Me.PageObjectGetSite.Id & "|" & Me.PageObjectGetLang.Id & "|" & Me.PageObjectGetTheme.Id
        Dim strHeaderMenu As String = CacheManager.Read(_cacheKey, 1)
        If Not String.IsNullOrEmpty(strHeaderMenu) Then
            ltlMenu.Text = strHeaderMenu
        Else
            Dim so As New ThemeSearcher
            so.KeySite = Me.PageObjectGetSite
            so.KeyLanguage = Me.PageObjectGetLang
            so.KeyFather.Domain = domain
            so.KeyFather.Name = name
            so.LoadHasSon = False
            so.LoadRoles = False
            Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any Then
                
                Dim _intCounter As Integer = 1
				
                Dim ssssbb As StringBuilder = New StringBuilder()
                ssssbb.Append("<ul class='sf-menu headerMenu'>")
                Dim currentclass As String = String.Empty
                
                'verifico chi � il padre
                Dim tmpvo As New ThemeValue
                Dim currFatherid As Integer = 0
                tmpvo = _currentTheme 'Me.BusinessThemeManager.Read(_currentTheme)
                If Not tmpvo Is Nothing AndAlso tmpvo.Key.Id > 0 Then
                    currFatherid = tmpvo.KeyFather.Id
                End If
                
                For Each thm As ThemeValue In coll
                    If _currentTheme.Key.Id = thm.Key.Id Or thm.Key.Id = currFatherid Or (currFatherid = Dompe.ThemeConstants.idAreaPress And thm.Key.Id = Dompe.ThemeConstants.idMedia) Then
                        currentclass = "current"
                    Else
                        currentclass = String.Empty
                    End If
                                        
                    ssssbb.AppendFormat("<li id=""d{0}"" class=""{1} {2}"">", _intCounter, GetClass(thm.Key.Id, _intCounter), currentclass)
                    ssssbb.Append("<a title='" & thm.Description & "' href='" & Me.BusinessMasterPageManager.GetLink(thm, False) & "'>" & thm.Description & "</a>")
                    ssssbb.Append("<ul class='submenu'>")
                    
                    Dim _internalThemColl As ThemeCollection = LoadRptSubMenu(thm)
                    
                    For Each internalThemeValue As ThemeValue In _internalThemColl
                        'If _currentTheme.Key.Id = internalThemeValue.Key.Id Then
                        If _currentTheme.Key.Id = internalThemeValue.Key.Id Or (internalThemeValue.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista And _currentTheme.KeyFather.Id = Dompe.ThemeConstants.idAreaPress) Then
                            currentclass = "current"
                        Else
                            currentclass = String.Empty
                        End If
                        ssssbb.Append("<li id='s" & internalThemeValue.Key.Id & "' class='" & currentclass & "'>")
                        ssssbb.Append("<a title='" & internalThemeValue.Description & "' href='" & GetLinkSL(internalThemeValue) & "'><span><span>" & internalThemeValue.Description & "</span></span></a>")
                        ssssbb.Append("</li>")
                    Next
                    ssssbb.Append("</ul>")
                    'End If
                    
                    ssssbb.Append("</li>")
                    _intCounter += 1
                Next
                ssssbb.Append("</ul>")
                CacheManager.Insert(_cacheKey, ssssbb.ToString(), 1, 60)
                ltlMenu.Text = ssssbb.ToString()
				
            End If
        End If
    End Sub
    
    Protected Sub LoadHeader2HamMenu(ByVal domain As String, ByVal name As String)
        Dim _cacheKey As String = "cacheKeyHeaderHamMenu:" & Me.PageObjectGetSite.Id & "|" & Me.PageObjectGetLang.Id & "|" & Me.PageObjectGetTheme.Id
        Dim strHeaderMenu As String = CacheManager.Read(_cacheKey, 1)
        If Not String.IsNullOrEmpty(strHeaderMenu) Then
            ltlHamMenu.Text = strHeaderMenu
        Else
            Dim so As New ThemeSearcher
            so.KeySite = Me.PageObjectGetSite
            so.KeyLanguage = Me.PageObjectGetLang
            so.KeyFather.Domain = domain
            so.KeyFather.Name = name
            so.LoadHasSon = False
            so.LoadRoles = False
            Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any Then
                
                Dim _intCounter As Integer = 1
				
                Dim ssssbb As StringBuilder = New StringBuilder()
                ssssbb.Append("<div class='ham-menu'><ul>")
                Dim currentclass As String = String.Empty
                
                'verifico chi � il padre
                Dim tmpvo As New ThemeValue
                Dim currFatherid As Integer = 0
                tmpvo = _currentTheme 'Me.BusinessThemeManager.Read(_currentTheme)
                If Not tmpvo Is Nothing AndAlso tmpvo.Key.Id > 0 Then
                    currFatherid = tmpvo.KeyFather.Id
                End If
                
                For Each thm As ThemeValue In coll
                    If _currentTheme.Key.Id = thm.Key.Id Or thm.Key.Id = currFatherid Or (currFatherid = Dompe.ThemeConstants.idAreaPress And thm.Key.Id = Dompe.ThemeConstants.idMedia) Then
                        currentclass = "current"
                    Else
                        currentclass = String.Empty
                    End If
                                        
                    ssssbb.AppendFormat("<li id=""dh{0}"" class=""{1} {2}"">", _intCounter, GetClass(thm.Key.Id, _intCounter), currentclass)
                    ssssbb.Append("<a title='" & thm.Description & "' href='" & Me.BusinessMasterPageManager.GetLink(thm, False) & "'>" & thm.Description & "</a>")
                    ssssbb.Append("<ul>")
                    
                    Dim _internalThemColl As ThemeCollection = LoadRptSubMenu(thm)
                    
                    For Each internalThemeValue As ThemeValue In _internalThemColl
                        'If _currentTheme.Key.Id = internalThemeValue.Key.Id Then
                        If _currentTheme.Key.Id = internalThemeValue.Key.Id Or (internalThemeValue.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista And _currentTheme.KeyFather.Id = Dompe.ThemeConstants.idAreaPress) Then
                            currentclass = "current"
                        Else
                            currentclass = String.Empty
                        End If
                        ssssbb.Append("<li id='sh" & internalThemeValue.Key.Id & "' class='" & currentclass & "'>")
                        ssssbb.Append("<a title='" & internalThemeValue.Description & "' href='" & GetLinkSL(internalThemeValue) & "'><span><span>" & internalThemeValue.Description & "</span></span></a>")
                        ssssbb.Append("</li>")
                    Next
                    ssssbb.Append("</ul>")
                    'End If
                    
                    ssssbb.Append("</li>")
                    _intCounter += 1
                Next
                ssssbb.Append("</ul></div>")
                CacheManager.Insert(_cacheKey, ssssbb.ToString(), 1, 60)
                ltlHamMenu.Text = ssssbb.ToString()
				
            End If
        End If
    End Sub
    
    Function GetLinkSL(ByVal vo As ThemeValue) As String
        Dim linkFinale As String = Me.BusinessMasterPageManager.GetLink(vo, False)
        
        If vo.Key.Id = Dompe.ThemeConstants.partnerWelcome Then
            linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.partnerWelcome, 2)
        End If
               
        If vo.Key.Id = Dompe.ThemeConstants.idManagement Then
           ' Response.Write(linkFinale ) ': Response.End()
            'prendo il primo contenuto con relazione al context 1
          '  Dim so As New ContentSearcher
         '   so.KeySite = Me.PageObjectGetSite
         '   so.KeySiteArea.Id = vo.KeySiteArea.Id
         '   so.key.IdLanguage = vo.KeyLanguage.Id
         '   so.KeyContext.Id = Dompe.ContextCostant.idMainContent
          '  Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
         '   If Not coll Is Nothing AndAlso coll.Any() Then
                'linkFinale = Me.BusinessMasterPageManager.GetLink(coll(0).Key, vo, False)
        '    End If
        End If
        
        If vo.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Then
		
            linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
		
            'If Me.BusinessAccessService.IsAuthenticated Then
            '    If Me.ObjectTicket.User.Key.Id > 0 Then
            '        If Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleMediaPress) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langId)
                      
            '        ElseIf Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleDoctor) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langId)
                        
            '        ElseIf Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RolePartner) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.partnerWelcome, _langId)
            '        End If
                    
            '    Else
            '        linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
            '    End If
            'Else
            '    linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
            'linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista)
            'End If

        End If
       ' Response.Write(linkFinale )
        Return linkFinale
    End Function
    
    Function LoadRptSubMenu(ByVal thmval As ThemeValue) As ThemeCollection
        Dim themes As ThemeCollection = Nothing
        Dim Roles As Boolean = False
                  
        If Not thmval Is Nothing Then
                                  
            themes = ReadMyTHChildren(thmval.Key)
      
        End If
        Return themes
    End Function
    
    Function ReadMyTHChildren(ByVal key As ThemeIdentificator) As ThemeCollection
        Dim coll As ThemeCollection = Nothing
        
        If Not key Is Nothing AndAlso key.Id > 0 Then
            Dim soTheme As New ThemeSearcher
            
            soTheme.KeyFather.Id = key.Id
            soTheme.KeySite = Me.PageObjectGetSite
            soTheme.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
            soTheme.LoadHasSon = False
            soTheme.LoadRoles = False
           
            coll = Me.BusinessThemeManager.Read(soTheme)
                    
        End If
          
        Return coll
    End Function
    
    'Function GetClass(ByVal idTheme As String, ByVal item As Integer) As String
    '    Dim res As String = String.Empty
    '    res = String.Format("menu{0}", item)
    '    Dim manHElp As New Healthware.Dompe.Helper.Helper
                     
    '    Select Case Me.PageObjectGetTheme.Id
    '        Case idTheme
    '            res = String.Format("current {0}", res)
    '        Case Dompe.ThemeConstants.idAreaMedico
    '            If idTheme = Dompe.ThemeConstants.idAreaMedico Then
    '                res = String.Format("current {0}", res)
    '            End If
    '    End Select
            
    '    Return res
    'End Function
    
    
    Function GetClass(ByVal idTheme As String, ByVal item As Integer) As String
        Dim res As String = String.Empty
        'verifico chi � il padre
        Dim tmpvo As New ThemeValue
        Dim currFatherid As Integer = 0
        tmpvo = _currentTheme
        If Not tmpvo Is Nothing AndAlso tmpvo.Key.Id > 0 Then
            currFatherid = tmpvo.KeyFather.Id
        End If
        Dim currentclass As String = String.Empty
        'Response.Write("- _currentTheme" & _currentTheme.Id & " idTheme" & idTheme & " currFatherid" & currFatherid)
        If _currentTheme.Key.Id = idTheme Or CInt(idTheme) = currFatherid Or (currFatherid = Dompe.ThemeConstants.idAreaPress And idTheme = Dompe.ThemeConstants.idMedia) Then
            currentclass = "current"
        Else
            currentclass = String.Empty
        End If
        res = String.Format("class='{0}'", currentclass)
            
        Return res
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
  
 
    Sub LoadHeaderMenu(ByVal domain As String, ByVal name As String)
        Dim so As New ThemeSearcher
       
        so.KeySite = Me.PageObjectGetSite
        so.KeyLanguage = Me.PageObjectGetLang
        so.KeyFather.Domain = domain
        so.KeyFather.Name = name
              
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            'RptHeaderMenu.DataSource = coll
            'RptHeaderMenu.DataBind()
        End If
    End Sub
    
    
   
  
    Function getLink(ByVal langValue As LanguageValue) As String
        Return Me.BusinessMasterPageManager.GetLink(langValue.Key, True)
    End Function
    
    Function GetLink() As String
        Return Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
    End Function
    
    'GetLink by Theme Domain/Name
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
        
    Function GetLink(ByVal vo As MenuItemValue) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.PageObjectGetLang
        so.KeySite = Me.PageObjectGetSite
        so.Key = vo.KeyTheme
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
       
        Return Nothing
    End Function
    
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id)
    End Function
    
    Function LoadLanguagesDMP() As String
        If Me.PageObjectGetTheme.Id = Dompe.ThemeConstants.partnerWelcome Then
            Return "<a class='lang'>&nbsp;</a>"
        End If
        
        Dim CacheLngkey As String = String.Empty
        Dim CurrentLang As String = Me.PageObjectGetLang.Code
        Dim langColl As New LanguageCollection
        langColl = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(_siteId))
              
        Dim allLangs As String = String.Empty
        Dim curLang As String = String.Empty
        Dim otherLangs As String = String.Empty
		
        For Each elem As LanguageValue In langColl
            If Not elem Is Nothing AndAlso elem.Key.Id > 0 AndAlso elem.Key.Id < 3 Then
                If Not CurrentLang.ToLower.Equals(elem.Key.Code.ToLower) Then
                    'otherLangs = otherLangs & "<a class='lang' href=" & getLink(elem) & " title=" & elem.Description & ">" & elem.Description & "</a>"
                    otherLangs += "<li><a class='lang' href='" & getLink(elem) & "' title='" & elem.Description & "'>" & elem.Key.Code & "</a></li>"
                Else
                    curLang = "<a class='lang' href='" & getLink(elem) & "' title='" & elem.Description & "'>" & elem.Key.Code & "</a>"
                End If
            End If
        Next
        Return "<ul class='' id='lang'><li class='current'>" & curLang & "</li></ul>"
        
        'TODO: da ripristinare e cancellare il precedente
        'Return "<ul class='' id='lang'><li class='current'>" & curLang & "<ul id='subMenuLang'>" & otherLangs & "</ul></li></ul>"
    End Function
    
    Sub SizeText(ByVal s As Object, ByVal e As CommandEventArgs)
        CacheManager.RemoveByKey("BodySize")
        CacheManager.Insert("BodySize", e.CommandArgument, 1, 5)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ SizeBody('" & e.CommandArgument & "');})", True)
    End Sub
    
    Sub DoLogout(ByVal s As Object, ByVal e As EventArgs)
       
        accessService.Logout()
        CacheManager.RemoveByKey("cacheKeyMenu:" & Me.PageObjectGetSite.Id & "|" & Me.PageObjectGetLang.Id & "|" & Dompe.ThemeConstants.idMedia)
        Response.Redirect("/")
    End Sub

</script>
<script type="text/javascript">
    var loginStatus = 0;
    var selectStatus = 0;

    $(document).ready(function () {
        var ass = '<%= _stringOpenlog %>'
        if (ass == "lgn") {
            loginBox(1);
        }

        $('#lang').superfish({});
    });

    function loginBox(val) {
        if (loginStatus == 0 || val == 1) {
            loginStatus = 1;
            $('.loginBox').css('display', 'block');
            $('.logout').html('<%= strClose %>');
            $('.logout').addClass('closeLogin');

        } else {
            $('.loginBox').css('display', 'none');
            $('.logout').html('<%= strLogin %>');
            $('.logout').removeClass('closeLogin');
            loginStatus = 0;

        }
    }

    function SizeBody(Bodyclass) {
        if (Bodyclass != "") {
            $(".body").removeClass("smallFont");
            $(".body").removeClass("bigFont");
            $(".body").addClass(Bodyclass);
        }
    }
</script>



<header>
	<div class="first-block">  
        <asp:PlaceHolder ID="phlLogin" runat="server"></asp:PlaceHolder>
    	<div class="logo">
        	<a href="<%=_urlHome %>" title="Dompe">Domp&eacute;</a>
        </div>
        <div class="floatRight">
        	<asp:PlaceHolder ID="plhSocialNetwork" runat="server" EnableViewState="false" />
            <div class="log">
                <asp:LinkButton runat="server" CssClass="logout" ID="linkLogout" OnClick="dologOut" Text="Logout"></asp:LinkButton>
                <a title="login" href="javascript:void(0);" onclick="javascript:loginBox(0);" class="logout" runat="server" id="linkLogin"><%= Me.BusinessDictionaryManager.Read("Accedi", _langId, "Accedi")%> </a>
            	<%--<a href="#" title="Accedi">Accedi</a>--%>
            </div>
        </div>
    </div><!--end first-block -->
    <div class="second-block">

        <asp:Literal ID="ltlHamMenu" runat="server"></asp:Literal>
        <asp:Literal ID="ltlMenu" runat="server"></asp:Literal>


         <div class="change-lang">
        	<%=LoadLanguagesDMP()%>
        </div>
        <div class="search">
            <asp:PlaceHolder ID="phSearch" runat="server" EnableViewState="false" />        	
        </div>
        <asp:Literal ID="ltlPrivateArea" runat="server"></asp:Literal>
        <asp:DropDownList ID="ddlReservedArea" CssClass="sel-area" Visible="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="manageReservedArea" />
       
       
    </div><!--end second-block -->
       
</header><!--end header-->