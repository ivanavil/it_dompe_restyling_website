﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Dim targetlnk As String = ""
    Dim idLingua As Integer
    Dim conta As Integer
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        idLingua = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        conta = 1
        If Not Page.IsPostBack Then
            rptSiteMenu.DataSource = LoadMenu()
            rptSiteMenu.DataBind()
           
        End If
      
    End Sub
    
    Function LoadMenu() As ContentCollection
       
        Dim idContentKeyTheme As Integer = GetPKThema()
      
     
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        so.KeySite = Me.PageObjectGetSite
        so.key.IdLanguage = Me.PageObjectGetLang.Id
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDPhotoGalleryAreaMediaPress
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.KeysToExclude = idContentKeyTheme
        so.SetMaxRow = 3
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        Else
            Return Nothing
        End If
        
    End Function
    
    Function GetPKThema() As Integer
        Dim idContentKeyTheme As Integer = 0
        Dim idContentTheme As Integer
        Dim contentextraVal As New ContentExtraValue
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idPhotoGalleryMediaPress))
        idContentTheme = valthem.KeyContent.Id
        
        contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(idContentTheme, Me.PageObjectGetLang.Id))
        If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
            idContentKeyTheme = contentextraVal.Key.PrimaryKey
        End If
        Return idContentKeyTheme
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 120, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
       
        Return "javascript:alert('gallery: " & vo.Key.Id & "');"
       
    End Function
    
    'Function getTarget(ByVal vo As ContentValue) As String
    '    targetlnk = ""
    '    If Not String.IsNullOrEmpty(vo.LinkUrl) Then
    '        targetlnk = "target=""_blank"""
    '        Return targetlnk
    '    Else
    '        Return targetlnk
    '    End If
    'End Function
        
</script>
   
           <asp:Repeater ID="rptSiteMenu" runat="server">
                <HeaderTemplate>
                 <div class="archiveBox">
                        <div class="searchBox1">
                            <h4 class="photoTitle"><%= Me.BusinessDictionaryManager.Read("Press_Title_PhotoGallery", Me.PageObjectGetLang.Id, "Press_Title_PhotoGallery")%></h4>
                        </div>            
               </HeaderTemplate> 
                <ItemTemplate>

                <div class="boxPhotoGallery<%if conta=1 then %> noMargin<%end if %>">
                                <div class="cnt">                                    
                                    <a  class="effectHover" href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundFull('Press PhotoGallery','Press Open Album','<%# DataBinder.Eval(Container.DataItem, "Title") %>')">              
                 <%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                                    <a href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundFull('Press PhotoGallery','Press Open Album','<%# DataBinder.Eval(Container.DataItem, "Title") %>');"><%# GetImage(DataBinder.Eval(Container.DataItem, "Key.Id"), DataBinder.Eval(Container.DataItem, "Title"))%></a>
                                    <h3><a  href="<%#"/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?l="& Me.PageObjectGetLang.Code & "&c="& Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(DataBinder.Eval(Container.DataItem, "Key.Id"))%>" rel="shadowbox;height=625;width=800" onclick="recordOutboundFull('Press PhotoGallery','Press Open Album','<%# DataBinder.Eval(Container.DataItem, "Title") %>');">              
                                        <%# DataBinder.Eval(Container.DataItem, "Title")%>
                                    </a></h3>
                                    <p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>    
                                </div>
                            </div><!--end boxPhotoGallery-->       
                 <%conta = conta + 1%>
                </ItemTemplate>
                <FooterTemplate>
                </div>                
                </FooterTemplate>
            </asp:Repeater>      