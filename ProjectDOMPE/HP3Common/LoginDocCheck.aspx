﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<script runat="server">
    Dim _lang As Integer = 1
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
       
    End Sub
    
    Private Enum OperationMail As Integer
        Recovery = 1
        Activation = 2
    End Enum
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _lang, defaultLabel)
    End Function
    
    Sub ForgotPassword(ByVal s As Object, ByVal e As EventArgs)
        
        
        If (Page.IsValid()) Then
          
            
            Dim oUser As UserValue = ReadUserByEmail(TxtForgotMail.Value.Trim())
          
            If (Not oUser Is Nothing AndAlso oUser.Key.Id > 0) AndAlso oUser.Status = 1 Then ' UTENTE ATTIVO
                Dim ck As Boolean = checkEmail(oUser.Email.GetString)
                Dim userS As String = IIf(oUser Is Nothing, "0", oUser.Key.Id)
                If ck Then
                    Dim conf As Boolean = RecoveryPassword(oUser)
                    If conf Then
                        'Reset()
                        'DisableDivForm()
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryOk();})", True)
                        Return
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryError();})", True)
                        Return
                    End If
                    ' messg.Text = GetLabel("Recovery_Error1", "Sorry, no match has been found with the inserted mail. Thank you.")
                End If
            Else ' UTENTE NON ATTIVO
                Dim userStat As String = IIf(IsNothing(oUser), "0", "-1")
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryError();})", True)
                Return
            End If
                      
            ' messgError.Text = GetLabel("Recovery_Error1", "Sorry, no match has been found with the inserted email. Thank you.")
            Return
        Else
            Response.Redirect("http://" & Request.Url.Host)
        End If
        
    End Sub
    
    Function RecoveryPassword(ByVal oUser As UserValue) As Boolean
        Dim oMailTemplateValue As MailTemplateValue = Nothing
        Dim oUserValue As UserValue = Nothing
        
        If Not oUser Is Nothing AndAlso oUser.Key.Id > 0 Then
            oMailTemplateValue = getMailTemplate("UserRecoveryPassword")
                        
            'gestisco email di recovery password
            Dim res As Boolean = SendMail(oMailTemplateValue, oUser, OperationMail.Recovery)
            Return res
        End If
        
        Return False
    End Function
    
    Function ReadUserByEmail(ByVal email As String) As UserValue
        Dim so As New UserSearcher
        ' so.Properties.Add("Email.GetString")
        so.SetMaxRow = 1
        so.Status = 1 ' Attivo
        so.Email = email
        so.RegistrationSite.Id = 46
        ' so.Username = email
        
        Me.BusinessUserManager.Cache = False
        Dim coll As UserCollection = Me.BusinessUserManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
    
    Function getMailTemplate(ByVal strMailtemplate As String) As MailTemplateValue
        Dim nameTemplate As String = ""
     
        nameTemplate = strMailtemplate
        
        Dim omailTemplateValue As New MailTemplateValue
        Dim omailTemplateColl As New MailTemplateCollection
        Dim omailTemplateSearcher As New MailTemplateSearcher
        Dim omailTemplateManager As New MailTemplateManager
        
        omailTemplateSearcher.Key.KeyLanguage.Id = _lang
        'omailTemplateSearcher.KeySite.Id = idsite
        omailTemplateSearcher.Key.MailTemplateName = nameTemplate
        omailTemplateColl = Me.BusinessMailTemplateManager.Read(omailTemplateSearcher)
        If Not omailTemplateColl Is Nothing AndAlso omailTemplateColl.Count > 0 Then
            Return omailTemplateColl(0)
        End If
        Return Nothing
    End Function
    
    Function checkEmail(ByVal userEmail As String) As Boolean
       
        If Not String.IsNullOrEmpty(userEmail) Then
            
            If (userEmail = TxtForgotMail.Value) Then
                Return True
            End If
        End If
        
        Return False
    End Function
    Function SendMail(ByVal oMailTemaplate As MailTemplateValue, ByVal oUser As UserValue, ByVal operation As Integer) As Boolean
        Try
            Dim mailValue As New MailValue
            
            If (Not oMailTemaplate Is Nothing) And (Not oUser Is Nothing) Then
               
                mailValue.MailTo.Add(New MailAddress(oUser.Email.GetString))
                mailValue.MailFrom = New MailAddress(oMailTemaplate.SenderAddress, oMailTemaplate.SenderName)
            
                mailValue.MailBody = ReplaceBodyInfo(oMailTemaplate.Body, oUser, operation)
                mailValue.MailSubject = oMailTemaplate.Subject
                mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
               
                If (oMailTemaplate.BccRecipients <> String.Empty) Then
                    mailValue.MailBcc.Add(New MailAddress(oMailTemaplate.BccRecipients))
                End If
                    
                If (oMailTemaplate.FormatType = 1) Then
                    mailValue.MailIsBodyHtml = True
                Else
                    mailValue.MailIsBodyHtml = False
                End If
               
                'invio della mail
                Dim systemUtility As New SystemUtility()
                systemUtility.SendMail(mailValue)
              
                Return True
            End If
                 
              
        Catch ex As Exception
            Return False
            Throw New BaseException("DOMPE - Recovery Password Error : ", ex)
        End Try
              
        Return False
    End Function
    
    Function ReplaceBodyInfo(ByVal body As String, ByVal userValue As UserValue, ByVal operation As Integer) As String
        Dim replecedBody As String = String.Empty
        
        Select Case operation
            Case OperationMail.Recovery
       
                If Not userValue Is Nothing Then
                    Dim completeName As String = userValue.CompleteName
            
                    If Not String.IsNullOrEmpty(completeName) Then
                        replecedBody = body.Replace("[USER]", completeName)
                    Else
                        replecedBody = body.Replace("[USER]", "User")
                    End If
            
                    Return replecedBody.Replace("[PASSWORD]", userValue.Password).Replace("[USERID]", userValue.Username)
         
                End If
            Case OperationMail.Activation
                                
                'If Not userValue Is Nothing Then
                '    replecedBody = body.Replace("[LINK]", getActivationLink(userValue))
                'End If
        
                Return replecedBody
                
        End Select
                
        Return replecedBody
    End Function
</script>    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login DocCheck</title>
   <%-- <link href="../_css/it/font.css" rel="stylesheet" media="screen" type="text/css" />
<link href="../_css/style.css" rel="stylesheet" media="screen" type="text/css" />--%>
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
		<script type="text/javascript" language="javascript">
		    function submitDocCheck(ff) {
		        if (ff == '0') {
		           document.form_doccheckPress.submit();
		        }else{
		            document.form_doccheckHcp.submit();
		        }
            };

		    function changeAccount() {
		        var itemval = $('#selAccounts').val();
		        if (itemval == '') {
		            $('#name').val('');
		            $('#password').val('');
		        }
		        else {
		            $('#password').val(itemval);
		            $('#name').val($('#selAccounts option:selected').text());
		        }
		    };

		    function test2login(nomeControllo, valore) {

		        if (document.getElementById(nomeControllo).value == "") {
		            document.getElementById(nomeControllo).value = valore;
		        } else {
		            var pwd = $("#password").val();
		            if (pwd != "") {
		                $("#password").attr("class", "dspVisible");
		                $("#TxtPwdLabel").attr("class", "dspNone");
		            }
		        }
		        $("#ErrorUser").attr("style", "display:none");
		        $("#ErrorPwd").attr("style", "display:none");
		    }
		    function cancellaValue(nomeControllo, valore) {


		        if (document.getElementById(nomeControllo).value == valore) {
		            document.getElementById(nomeControllo).value = "";
		        }
		        $("#ErrorUser").attr("style", "display:none");
		        $("#ErrorPwd").attr("style", "display:none");
		    }
		    function test2Pwd(nomeControllo, valore) {

		        if (document.getElementById(nomeControllo).value == "") {
		            $("#TxtPwdLabel").attr("class", "dspVisible");
		            $("#password").attr("class", "dspNone");
		        }
		        $("#ErrorUser").attr("style", "display:none");
		        $("#ErrorPwd").attr("style", "display:none");
		    }
		    function cancellaValuePwd(nomeControllo, valore) {


		        if (document.getElementById(nomeControllo).value == valore) {
		            document.getElementById(nomeControllo).setAttribute('class', 'dspNone');
		            $("#password").attr("class", "dspVisible");
		            $("#password").val("");
		            $("#password").focus();
		        }
		        $("#ErrorUser").attr("style", "display:none");
		        $("#ErrorPwd").attr("style", "display:none");
		    }

		 
		    function ValidationCheck() {
		      
                var user = $("#name").val();
		        var pass = $("#password").val();
		        $("#ErrorUser").attr("style", "display:none");
		        $("#ErrorPwd").attr("style", "display:none");
                var esito = 1;
                if (user == "" || user == "UserID *"){
                    esito = 0;
                    $("#ErrorUser").attr("style", "display:block");
                }
                if (pass == ""){
                esito = 0;
                $("#ErrorPwd").attr("style", "display:block");
                }
            if (esito > 0) {
                 submitDocCheck(0);
                }
               return false;
           }


           function OpenRecoveryPwd() {
               $("#Divlogin").attr("style", "display:none");
               $("#boxForgot").attr("style", "display:block");
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");
           }
           function CloseRecoveryPwd() {
               $("#Divlogin").attr("style", "display:block");
               $("#boxForgot").attr("style", "display:none");
               $("#MessageOK").attr("style", "display:none");
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");

           }
           function test2(nomeControllo, valore) {

               if (document.getElementById(nomeControllo).value == "") {
                   document.getElementById(nomeControllo).value = valore;
               }
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");
           }
           function cancellaValue(nomeControllo, valore) {


               if (document.getElementById(nomeControllo).value == valore) {
                   document.getElementById(nomeControllo).value = "";
               }
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");
           }

           function RecoveryOk() {
               $("#Divlogin").attr("style", "display:none");
               $("#boxForgot").attr("style", "display:none");
               $("#MessageOK").attr("style", "display:block");
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");
           }
           function RecoveryError() {
               $("#MessageOK").attr("style", "display: none");
               $("#ErrorForgot").attr("style", "display: block");
               $("#Divlogin").attr("style", "display:none");
               $("#boxForgot").attr("style", "display:none");
               $("#ErrorUser").attr("style", "display:none");
               $("#ErrorPwd").attr("style", "display:none");
           }
     
     
     </script>
   <style type="text/css">
       
.loginBoxCnt p.psswDim a {
    color: #FFFFFF;
    text-decoration: underline;
}
.loginBoxCnt p.psswDim {
    text-align: left;
    font-size: 11px;
    padding: 0 0 0 10px;
    clear: both;
    }
.loginBox {
    color: #FFFFFF;
}
.loginBoxCnt input {
    background-image: url("../_slice/loginInput.gif");
    background-position: left top;
    background-repeat: no-repeat;
    border: 0 none;
    color: #C30505;
    float: left;
    height: 22px;
    line-height: 22px;
    margin: 0 0 5px;
    padding: 0 10px;
    width: 246px;
}
.loginBoxCnt .loginBoxLeft .btn {
    width: 85px;
}
.loginBoxCnt .btn {
    background-color: #FFFFFF;
    bottom: 0;
    color: #C30505;
    left: 0;
    padding: 10px 0;
    position: absolute;
    text-align: center;
    text-decoration: none;
}

body{margin:0; padding:0; font-size: 13px;font-family: Tahoma,Geneva,sans-serif;}
p {margin:0; padding:0;}
form {margin:0; padding:0;}
 .dspNone
    {
        display:none;
    }
     .dspVisible
    {
        display: block;
    }
    input.buttonOKlogin
    {
    background-color: #FFFFFF;
    color: #C30505;
    width:85px;
    border:0;
    margin:0 0 0 10px;
    padding: 10px 0;
    float:left;
    background-image:none;
    height:auto;
    text-align: center;
    }
    a.null
    {
        clear:both;float:left;width:80px;
    }
    
 .tooltipError
    {
        position:absolute;
        z-index : 9999;
        right: 1px;
        border: 1 solid #000;
        width: 200px;
        height:24px;
        background-image: url('/ProjectDOMPE/_slice/tooltip.png');
        background-repeat:no-repeat;
        color: #C30505;
        text-align: center;
        top:26px;
         line-height:22px;
  }
  
      .tooltipErrorUser
    {
        position:absolute;
        z-index : 9999;
        top: 0px;
        line-height:22px;
        right : 1px;
        width: 200px;
        height:24px;
        background-image: url('/ProjectDOMPE/_slice/tooltip.png');
        background-repeat:no-repeat;
        color: #C30505;
        text-align:center;
        padding: 0;
    }
    </style>
 </head>
<body>
 <form id="form_doccheckPress" name="form_doccheckPress" target="_parent" action="https://login.doccheck.com/" method="post">
     
			<%--	<select id="selAccounts" onchange="changeAccount()" style=" width: 150px;">
					<option value="0" selected="selected">Select a user</option>
					<option value="pierdomenico1">pierdomenico.romano@publicishealthware.com</option>
					<option value="valter1">valter.santoro@publicishealthware.com</option>
					<option value="bbicio1">bbicio@outlook.com</option>
				</select> 
				 <input class="btn" type="button" id="btnPress1" value="Press" onClick="submitDocCheck(0);" />--%>

	<div id="Divlogin" style="display:block;" class="loginBox">
    	<div class="container">
        	<div class="loginBoxCnt">
                <div class="loginBoxLeft">
                    <p><input type="text" name="name" id="name" value="UserID *" title="UserID" onfocus ="javascript:cancellaValue(this.id,'UserID *');" onblur="javascript:test2login(this.id,'UserID *');" />
                    </p>
                     <div id="ErrorUser" class="tooltipErrorUser"  style=" display:none"><%= GetLabel("RequiredUser", "RequiredUser")%></div>
                    <p><input class="dspNone" type="password" title="Password" name="password" id="password"  onblur="javascript:test2Pwd(this.id,'Password *');" />
                    <input class="dspVisible" type="text" title="Password" id="TxtPwdLabel"  onfocus ="javascript:cancellaValuePwd(this.id,'Password *');" value="Password *" />
                    </p>
                    <div id="ErrorPwd" class="tooltipError" style=" display:none"><%= GetLabel("RequiredPassword", "RequiredPassword")%></div>
                    <p class="psswDim">
               <%--      <%= GetLabel("ForgotPasswordPartner", "ForgotPasswordPartner")%> <a href="javascript:void(0)" onclick = "OpenRecoveryPwd()"><i><%= GetLabel("DMP_ClickHere", "DMP_ClickHere")%></i></a>--%>
                     <%= GetLabel("ForgotPasswordMed", "ForgotPasswordMed")%><a href="https://login.doccheck.com/" title="Recupera password" target="_blank">&nbsp;<i><%= GetLabel("DMP_ClickHere", "DMP_ClickHere")%></i></a>
                     </p>
					 <%-- Doc Check LOGIN STAGING 2000000004780--%>
					 <%-- Doc Check LOGIN PROD 2000000004778--%>
                   <input type="hidden" id="dc_key" name="keynummer" value="2000000004778" /> 
				   <input type="hidden" value="it" name="strDcLanguage" />
				   <input type="hidden" value="196" name="intDcLanguageId" />  
                 <a href="javascript:void(0)" class="btn" id="btnPress" onclick="ValidationCheck();"><%= GetLabel("LOGIN", "LOGIN")%></a>
                </div>
            </div>
        </div>
    </div>
</form> 
<form id="frm1" runat="server">
     
<div id="boxForgot" style="display:none">
<div class="container">
        	<div class="loginBoxCnt">
                <div class="loginBoxLeft">
                    <p>
<input id="TxtForgotMail" runat="server" type="text" onfocus ="javascript:cancellaValue(this.id,'Email *');" onblur="javascript:test2(this.id,'Email *');" value="Email *" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="Email *" runat="server" ErrorMessage="Required" ControlToValidate="txtForgotMail"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="regexEmail" ErrorMessage="Incorrect Format" runat="server" Display="dynamic" ControlToValidate="txtForgotMail" EnableViewState ="false" ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}" />
</p>
<a href="javascript:void(0)" class="null" onclick = "CloseRecoveryPwd()"><span style=" color:White;"><i><%= GetLabel("Annulla", "Annulla")%></i></span></a>
<asp:Button ID="Button2" CssClass="buttonOKlogin" OnClick="ForgotPassword" runat="server" Text="Submit" class="buttonOKlogin" />
</div>
</div>
</div>
</div>

 <div id="MessageOK" style=" display:none;"> 
      <%= GetLabel("DMP_ForgotOK", "DMP_ForgotOK")%> 
      <a href="javascript:void(0)" onclick = "CloseRecoveryPwd()"><span style=" color:White;"><i><%= GetLabel("Close", "Close")%></i></span></a>
</div>
</form>
</body>
</html>
