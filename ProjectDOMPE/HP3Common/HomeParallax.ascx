﻿<%@ Control Language="VB" ClassName="HomeParallax" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Dompe" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _diabetologia As String = String.Empty
    Protected _themes As ThemeCollection = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _readMore As String = String.Empty
    Protected _lblDiabete As String = String.Empty
    Protected _oftalmologia As String = String.Empty
    Protected _oncologia As String = String.Empty
    Protected _trapianti As String = String.Empty
    
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _langKey = Me.PageObjectGetLang
        _readMore = Me.BusinessDictionaryManager.Read("DMP_readMore", _langKey.Id, "DMP_readMore")
        _lblDiabete = Me.BusinessDictionaryManager.Read("DMP_HDiab", _langKey.Id, "DMP_HDiab")
        _oftalmologia = Me.BusinessDictionaryManager.Read("DMP_HOft", _langKey.Id, "DMP_HOft")
        _oncologia = Me.BusinessDictionaryManager.Read("DMP_Onco", _langKey.Id, "DMP_Onco")
        _trapianti = Me.BusinessDictionaryManager.Read("DMP_HTrap", _langKey.Id, "DMP_HTrap")
        
        
    End Sub

    Protected Sub Page_Load()
        
        InitView()
        
    End Sub
    
    Protected Sub InitView()
        
        Dim so As New ThemeSearcher()
        
        so.Properties.Add("Description")
        so.LoadRoles = False
        so.LoadHasSon = False
        so.KeyLanguage = _langKey
        so.KeyFather.Id = ThemeConstants.idRicerca
        _themes = Me.BusinessThemeManager.Read(so)
        
    End Sub
    
    Protected Function GetLabel(ByVal themeId As Integer) As String
        
        Dim theme = _themes.Where(Function(th) th.Key.Id = themeId).FirstOrDefault()
        If Not theme Is Nothing Then
            Return theme.Description
        End If
        
        Return String.Empty
        
    End Function
    
</script>


<div class="imgParallax">
    <div class="parallax">
        <div class="parallax2">
            <div class="paritem1 paritem">
                <div class="paritems">
	                <img src="<%=String.Format("/{0}/_slice/parallax1_1.jpg", _siteFolder)%>" alt="">
	                <img src="<%=String.Format("/{0}/_slice/parallax1_2.jpg", _siteFolder)%>" alt="">
                </div>
                <div class="parTxt">
                    <span class="op">&nbsp;</span>
                    <h3><%=GetLabel(ThemeConstants.idDiabetologia) %></h3>
                    <p><%=_lblDiabete%></p>
                    <span class="viewMore viewMoreWhite"><%=_readMore %></span>
                </div>
                <span class="sha">&nbsp;</span>
                <span class="title"><%=GetLabel(ThemeConstants.idDiabetologia) %></span>
                <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idDiabetologia, _langKey.Id)%>" title="<%=GetLabel(ThemeConstants.idDiabetologia) %>" class="link"><%=GetLabel(ThemeConstants.idDiabetologia) %></a>
            </div>
            <div class="paritem2 paritem">
                <div class="paritems">
	                <img src="<%=String.Format("/{0}/_slice/parallax2_1.jpg", _siteFolder)%>" alt="">
	                <img src="<%=String.Format("/{0}/_slice/parallax2_2.jpg", _siteFolder)%>" alt="">
                </div>
                <div class="parTxt">
                    <span class="op">&nbsp;</span>
                    <h3><%=GetLabel(ThemeConstants.idOftalmologia)%></h3>
                    <p><%=_oftalmologia %></p>
                    <span class="viewMore viewMoreWhite"><%=_readMore %></span>
                </div>
                <span class="sha">&nbsp;</span>
                <span class="title"><%=GetLabel(ThemeConstants.idOftalmologia)%></span>
                <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idOftalmologia, _langKey.Id)%>" title="<%=GetLabel(ThemeConstants.idOftalmologia)%>" class="link"><%=GetLabel(ThemeConstants.idOftalmologia)%></a>
            </div>
            <div class="paritem3 paritem">
                <div class="paritems">
	                <img src="<%=String.Format("/{0}/_slice/parallax3_1.jpg", _siteFolder)%>" alt="">
	                <img src="<%=String.Format("/{0}/_slice/parallax3_2.jpg", _siteFolder)%>" alt="">
                </div>
                <div class="parTxt">
                    <span class="op">&nbsp;</span>
                    <h3><%=GetLabel(ThemeConstants.idOncologia)%></h3>
                    <p><%=_oncologia %></p>
                    <span class="viewMore viewMoreWhite"><%=_readMore %></span>
                </div>
                <span class="sha">&nbsp;</span>
                <span class="title"><%=GetLabel(ThemeConstants.idOncologia)%></span>
                <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idOncologia, _langKey.Id)%>" title="<%=GetLabel(ThemeConstants.idOncologia)%>" class="link"><%=GetLabel(ThemeConstants.idOncologia)%></a>
            </div>
            <div class="paritem4 paritem">
                <div class="paritems">
	                <img src="<%=String.Format("/{0}/_slice/parallax4_1.jpg", _siteFolder)%>" alt="">
	                <img src="<%=String.Format("/{0}/_slice/parallax4_2.jpg", _siteFolder)%>" alt="">
                </div>
                <div class="parTxt">
                    <span class="op">&nbsp;</span>
                    <h3><%=GetLabel(ThemeConstants.idTrapianti)%></h3>
                    <p style="top:242px !important;"><%=_trapianti %></p>
                    <span class="viewMore viewMoreWhite"><%=_readMore %></span>
                </div>
                <span class="sha">&nbsp;</span>
                <span class="title"><%=GetLabel(ThemeConstants.idTrapianti)%></span>
                <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idTrapianti, _langKey.Id)%>" title="<%=GetLabel(ThemeConstants.idTrapianti)%>" class="link"><%=GetLabel(ThemeConstants.idTrapianti)%></a>
            </div>
            <div class="bgPar">&nbsp;</div>
        </div>
    </div>
</div>




<script type="text/javascript">

    var parallaxCnt = 0

    $(document).ready(function () {

        parallaxHome();

        $(".paritem .link").hover(
           function () {
               $(this).parent().children('.parTxt').css('display', 'block');
               $(this).parent().children('.title').css('display', 'none');
           }, function () {
               $(this).parent().children('.parTxt').css('display', 'none');
               $(this).parent().children('.title').css('display', 'block');
           }
       );

    });

    function parallaxHome() {
        var cntCtrl = parallaxCnt % 4;
        if (cntCtrl == 0) {
            if (parallaxCnt == 4) {
                $(".paritem1 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem1 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else if (cntCtrl == 1) {
            if (parallaxCnt == 5) {
                $(".paritem2 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem2 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else if (cntCtrl == 2) {
            if (parallaxCnt == 6) {
                $(".paritem3 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem3 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else {
            if (parallaxCnt == 7) {
                $(".paritem4 .paritems").animate({ left: 0 }, 1000, function () { });
                parallaxCnt = -1;
            } else {
                $(".paritem4 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        }
        var tt2 = setTimeout(function () { parallaxHome() }, 2500);
    }
</script>


