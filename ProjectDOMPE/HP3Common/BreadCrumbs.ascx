<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<script language="VB" runat="server">
    Private Dict As New DictionaryManager
    Private PageUrl As String = ""
    Private id_Tema As Integer
    Private id_content As Integer = 0
    Private id_sito As Integer = 0
    Private id_Lingua As Integer = 0
    Protected _urlHome As String = ""
    Protected _siteareaMenu As Integer = 0
    Protected _siteThememenu As Integer = 0
    
    Sub Page_Init()
        id_sito = Me.PageObjectGetSite.Id
        id_Lingua = Me.PageObjectGetLang.Id
        id_Tema = Me.PageObjectGetTheme.Id
        If Me.PageObjectGetContent.Id > 0 Then id_content = Me.PageObjectGetContent.Id
        _urlHome = "http://" & Request.Url.Host
    End Sub
    
    
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        
        Dim dimstr_qstring As String = ""
        Dim manHElp As New Healthware.Dompe.Helper.Helper
        If Request.QueryString().ToString <> "" Then
            dimstr_qstring = "?" & Request.QueryString().ToString
        End If
        PageUrl = "http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("SCRIPT_NAME") & dimstr_qstring
        
        If Me.BusinessMasterPageManager.GetTheme.Id > Dompe.ThemeConstants.idHomeTheme Then
            Dim oThemeCollection As New ThemeCollection
            oThemeCollection = Me.BusinessThemeManager.GetPath(Me.BusinessMasterPageManager.GetTheme)
            If Not oThemeCollection Is Nothing AndAlso oThemeCollection.Count > 0 Then
              
                rpBreadcrumbs.DataSource = oThemeCollection
                rpBreadcrumbs.DataBind()
            
                Dim cThemeSearcher As New ThemeSearcher()
                cThemeSearcher.Key = Me.BusinessMasterPageManager.GetTheme
                cThemeSearcher.Display = SelectOperation.All
                cThemeSearcher.KeyLanguage.Id = id_Lingua
                oThemeCollection = Me.BusinessThemeManager.Read(cThemeSearcher)
            
                If Not oThemeCollection Is Nothing AndAlso oThemeCollection.Count > 0 Then
                    'litTheme.Text = "<a href=""#"" title=""" & oThemeCollection(0).Description & """>" & Dict.Read("theme_" & oThemeCollection(0).Key.Id, Me.BusinessMasterPageManager.GetLang.Id, oThemeCollection(0).Description).ToString() & "</a>"
                    
                    If Request.Url.AbsolutePath.Contains(".html") Then
                        'Response.Write("dettaglio:" & Me.PageObjectGetContent.Id)
                        
                        Dim strTheme As String = ""
                        Dim webRequest As New WebRequestValue
                        webRequest.KeycontentType = oThemeCollection(0).KeyContentType
                        webRequest.KeysiteArea = oThemeCollection(0).KeySiteArea
                        webRequest.Keycontent = oThemeCollection(0).KeyContent
                        webRequest.KeyEvent = oThemeCollection(0).KeyTraceEvent
                        webRequest.KeyTheme.Id = oThemeCollection(0).Key.Id
                        
                        litTheme.Text = "<a href=""" & Me.BusinessMasterPageManager.FormatRequest(webRequest) & """ style=""pointer-events: inherit; cursor: pointer;"" title=""" & oThemeCollection(0).Description & """>" & Dict.Read("theme_" & oThemeCollection(0).Key.Id, Me.BusinessMasterPageManager.GetLang.Id, oThemeCollection(0).Description).ToString() & "</a>"
                        
                        litTheme.Text += "<span><img src=""/ProjectDOMPE/_slice/arr-bc.jpg"" /></span><span class=""currentCnt"" style>" & Me.BusinessContentManager.Read(Me.PageObjectGetContent).Title
                    Else
                        litTheme.Text = "<a href=""#"" title=""" & oThemeCollection(0).Description & """>" & Dict.Read("theme_" & oThemeCollection(0).Key.Id, Me.BusinessMasterPageManager.GetLang.Id, oThemeCollection(0).Description).ToString() & "</a>"
                    End If
                    'If oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.Advisory And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.HDContact And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.Kolinterviews And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.Search And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.ASK And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.Header And oThemeCollection(0).Key.Name <> Dompe.ThemeConstants.Name.Footer And oThemeCollection(0).KeyFather.Name <> Dompe.ThemeConstants.Name.Footer Then
                    '    litTheme.Text = getLink(oThemeCollection(0)).Replace("<span><img src=""/ProjectDOMPE/_slice/arr-bc.jpg"" /></span>", "")
                    '    If Me.PageObjectGetContent.Id > 0 Then
                    '        Dim contentv As New ContentValue
                    '        contentv = Me.BusinessContentManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, 2))
                    '        If Not contentv Is Nothing Then
                    '            litContent.Text = "b<span><img src=""/ProjectDOMPE/_slice/arr-bc.jpg"" /></span>" & contentv.Title
                    '        End If
                                           
                    '    End If
                    'Else
                    '    If Me.PageObjectGetContent.Id > 0 Then
                    '        Dim contentv As New ContentValue
                    '        contentv = Me.BusinessContentManager.Read(New ContentIdentificator(Me.PageObjectGetContent.Id, 2))
                    '        If Not contentv Is Nothing Then
                    '            litContent.Text = contentv.Title
                    '        End If
                    '    Else
                    '        litTheme.Text = oThemeCollection(0).Description
                    '    End If
                    'End If
                   
                       
   
                End If
            Else
           
            End If
        
        Else
           
        End If
End Sub
    
    Function getLink(ByVal theme As ThemeValue) As String
        
        Dim strTheme As String = ""
        Dim webRequest As New WebRequestValue
        webRequest.KeycontentType = theme.KeyContentType
        webRequest.KeysiteArea = theme.KeySiteArea
        webRequest.Keycontent = theme.KeyContent
        webRequest.KeyEvent = theme.KeyTraceEvent             
        webRequest.KeyTheme.Id = theme.Key.Id
        webRequest.KeyLang.Id = id_Lingua
       
        If theme.KeyFather.Id = 0 Then Return ""
        If theme.KeyContentType.Id = 0 Then
            Return Dict.Read("theme_" & theme.Key.Id, Me.BusinessMasterPageManager.GetLang.Id, theme.Description).ToString() & "&nbsp;/&nbsp;"
        End If
      
        If theme.Key.Name = Dompe.ThemeConstants.Name.Header Or theme.Key.Name = Dompe.ThemeConstants.Name.Footer Or theme.KeyFather.Name = Dompe.ThemeConstants.Name.Footer Then
            
        Else
            strTheme = "<li><a href='" & Me.BusinessMasterPageManager.FormatRequest(webRequest) & "'>"
            strTheme += Dict.Read("theme_" & theme.Key.Id, Me.BusinessMasterPageManager.GetLang.Id, theme.Description).ToString() & "</a>"
            If theme.Key.Id <> Me.BusinessMasterPageManager.GetTheme.Id Then
                strTheme += "<span><img src=""/ProjectDOMPE/_slice/arr-bc.jpg"" /></span></li>"
            End If
        End If
                      
        Return strTheme
    End Function
</script>


<%--<div class="breadcrumbs">
        <ul>
        	<li><a href="#" title="Home">Home</a><span><img src="/ProjectDOMPE/_slice/arr-bc.jpg" /></span></li>
        	<li><a href="#" title="ResponsabilitÓ Sociale">Responsabilit&agrave; Sociale</a><span><img src="_slice/arr-bc.jpg" /></span></li>
        	<li><a href="#" title="Progetti e Iniziative">Progetti e Iniziative</a></li>
        </ul>
    </div><!--end breadcrumbs--> --%>

<div class="breadcrumbs"> 
	  <ul>
      <li><a href="<%= _urlHome %>">Home</a><span><img src="/ProjectDOMPE/_slice/arr-bc.jpg" /></span></li>
        <asp:Repeater ID="rpBreadcrumbs" runat="server">
        <HeaderTemplate></HeaderTemplate>
		    <ItemTemplate><%#getLink(Container.DataItem)%></ItemTemplate>
	  <FooterTemplate></FooterTemplate>
        </asp:Repeater>
	   <li> <asp:literal ID="litTheme" runat="server" /></li>
       
    </ul>
 </div>
	
