<%@ Control Language="VB" ClassName="logOut" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected Usertitle As String = ""
    Protected UserName As String = ""
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
    End Sub
    
    Sub Page_Load()
     
        If Me.BusinessAccessService.IsAuthenticated Then ' se l'utente �  autenticato, gestiamo il Logout
            Usertitle = Me.ObjectTicket.User.Title
            UserName = Me.ObjectTicket.User.Name & " " & Me.ObjectTicket.User.Surname
            'logout
            '  DoLogout()
        End If
        
        '  Response.Redirect(Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home), False)
    End Sub
    
    Sub DoLogout(ByVal s As Object, ByVal e As System.EventArgs)
        Try
            Dim bool As Boolean = Me.BusinessAccessService.Logout()
            Response.Redirect(Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home), False)
        Catch Err As Exception
            Throw New BusinessException("Dompe Logout Error: " & Err.ToString())
        End Try
      
    End Sub
    
    Function GetLink(ByVal Domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = _langKey
        so.Key.Domain = Domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    'costruisce il link passando domain e name del thema
    Function getlinkModify() As String
                     
        Return Me.BusinessMasterPageManager.GetLink(New ThemeIdentificator(Dompe.ThemeConstants.idHomeTheme), Nothing, False)
     
   
    End Function
</script>


<div id="top">
  <div id="userInfo">
     <p><%= Me.BusinessDictionaryManager.Read("DMP_Welcome", _langKey.Id, "DMP_Welcome")%> <a href="<%= getlinkModify() %>"><span class="hcpName"><%= Usertitle %> &nbsp; <%= UserName %></span></a></p> 
   </div>
  <div id="logOut">
    <asp:LinkButton ID="lnkbtnOut" OnClick="DoLogout" runat="server">logout</asp:LinkButton>
   </div>
</div>

	
    
  

