﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>

<script runat="server">
   
    Protected _siteFolder As String = String.Empty
    Dim targetlnk As String = ""
    Dim idLingua As Integer
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        idLingua = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        
        If Not Page.IsPostBack Then
            rptContent.DataSource = LoadMenu()
            rptContent.DataBind()
           
        End If
      
    End Sub
    
    Function LoadMenu() As ContentCollection
       
        Dim idContentKeyTheme As Integer = GetPKThema()
      
     
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        so.KeySite = Me.PageObjectGetSite
        so.key.IdLanguage = Me.PageObjectGetLang.Id
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDMedicalArea_NewsEventi
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.KeysToExclude = idContentKeyTheme
        so.SetMaxRow = 3
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        Else
            Return Nothing
        End If
        
    End Function
    
    Function GetPKThema() As Integer
        Dim idContentKeyTheme As Integer = 0
        Dim idContentTheme As Integer
        Dim contentextraVal As New ContentExtraValue
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNewsMediaPress))
        idContentTheme = valthem.KeyContent.Id
        
        contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(idContentTheme, Me.PageObjectGetLang.Id))
        If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
            idContentKeyTheme = contentextraVal.Key.PrimaryKey
        End If
        Return idContentKeyTheme
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 50, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = ""
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "javascript:ExternalLinkPopup('" & vo.LinkUrl & "');"
        Else
            Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idNewsMediaPress), Me.PageObjectGetMasterPage, False)
        End If
    End Function
    
    Function GetImageContent(ByVal IDcont As Integer, ByVal ImgWidth As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, Me.PageObjectGetLang.Id), ImgWidth, title, title)
        Return res
    End Function
    
    Function VerifyDownloadAcc(ByVal PkId As Integer) As String
        Dim stringaFinale As String = String.Empty
        stringaFinale = " style=""display:none;"" "
        Dim strlink As String = Healthware.DMP.Adapters.LiteratureModelAdapter.DownloadRelated(PkId)
        If strlink = "#" Then
            stringaFinale = String.Empty
        Else
            Dim titleEtichetta As String = Me.BusinessDictionaryManager.Read("DMP_EticheattadownloadArchive", Me.PageObjectGetLang.Id, "Download")
            stringaFinale = String.Format("<a title=""{0}"" href=""{1}"">{0}</a>", titleEtichetta, strlink)
            
        End If
        Return stringaFinale
    End Function

</script>
  <script language="JavaScript" type="text/JavaScript" src="/ProjectDOMPE/_js/OverArchive.js"></script>

           <asp:Repeater ID="rptContent" runat="server">
           
                <HeaderTemplate>
                 <div class="searchBox1">
                                <h4 class="newsTitle"><%= Me.BusinessDictionaryManager.Read("Press_Title_News", Me.PageObjectGetLang.Id, "Press_Title_News")%></h4> 
                            </div><!--end searchBox1-->
                    
                        
               </HeaderTemplate> 
                <ItemTemplate>

                        <div class="archiveBox">
                            <div class="archiveCnt">
                                <div class="effectHover" style="display: block;"><%# DataBinder.Eval(Container.DataItem, "Title")%></div>
                                <div class="cover"><a title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%# getlinkCont(Container.DataItem) %>"><%# GetImageContent(DataBinder.Eval(Container.DataItem, "Key.Id"), 135, DataBinder.Eval(Container.DataItem, "Title"))%></a>
                                </div>
                                <div class="cnt">
                                    <h3><a title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%# getlinkCont(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></h3>
                                    <p class="date"><%# Healthware.Dompe.Helper.Helper.get_FormatData(CDate(DataBinder.Eval(Container.DataItem, "DatePublish")), idLingua)%></p>
                                    <p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>
                                </div>
                                <%If 1 > 0 Then%>
                                <div class="tools">
                                    <a title="<%# DataBinder.Eval(Container.DataItem, "Title")%>" href="<%# getlinkCont(Container.DataItem) %>"><%= Me.BusinessDictionaryManager.Read("DMP_Detail", idLingua, "DMP_Detail")%></a>
                                   <%#VerifyDownloadAcc(DataBinder.Eval(Container.DataItem, "Key.Primarykey"))%> 
                                </div>
                                <%End If%>
                            </div>
                        </div><!--end archiveBox-->    



           
               
                </ItemTemplate>
                <FooterTemplate>
                       
                </FooterTemplate>
            </asp:Repeater>
                              
       
  

  

