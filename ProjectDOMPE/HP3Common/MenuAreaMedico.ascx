﻿<%@ Control Language="VB" ClassName="MenuSedi"  Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    Private _cacheKey As String = String.Empty
	Protected _contentId As Integer = 0
    Public Property ContentId As Integer
        Get
            Return _contentId
        End Get
        Set(value As Integer)
            _contentId = value
        End Set
    End Property	
	
    Protected Sub Page_Init()
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
        _contentKey = Me.PageObjectGetContent
		If ContentId = 0 Then ContentId = Me.PageObjectGetContent.Id
    End Sub
    
    Protected Sub Page_Load()
		_cacheKey = "_cacheBoxManuAreaMedico:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId
        If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If

		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_strMenu = _strCachedContent
		Else
			InitView()
		End If
    End Sub
	

    
    Protected Sub InitView()
        Dim idAreamedica As Integer = Dompe.ThemeConstants.idAreaMedico
        Dim so As New ThemeSearcher()
        so.KeyFather.Id = idAreamedica
        so.KeyLanguage.Id = _langKey.Id
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim sel As String = ""
        Dim collT As New ThemeCollection

        collT = BusinessThemeManager.Read(so)
       ' collT.Sort(ThemeGenericComparer.SortType.ByOrder, ThemeGenericComparer.SortOrder.ASC)
        If Not collT Is Nothing AndAlso collT.Any() Then
            Dim sBuilder As New StringBuilder()
            sBuilder.Append("<ul>")
            Dim el As New ThemeValue
            
            For Each el In collT
                If (el.Key.Id = _currentTheme.Id) OrElse (el.Key.Id = Request("c")) Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If
                sBuilder.AppendFormat("<li> <a class='{0}' title='{1}' href='{2}'>{3}</a></li>", sel, Server.HtmlEncode(el.Description), Helper.GetThemeLink(Me.BusinessMasterPageManager, el.Key.Id, _langKey.Id), el.Description)
            Next
            sBuilder.Append("</ul>")
            _strMenu = sBuilder.ToString()
            sBuilder.Clear()
			
			'Cache Content and send to the oputput-------
			CacheManager.Insert(_cacheKey, _strMenu, 1, 120)
			'--------------------------------------------
        End If
    End Sub
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(_currTheme, False)
    End Function
</script>
<%=_strMenu %>