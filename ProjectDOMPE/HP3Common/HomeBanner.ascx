﻿<%@ Control Language="VB" ClassName="HomeBanner" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing

    Protected Sub Page_Init()
        _langKey = Me.PageObjectGetLang
    End Sub
   
    Sub page_load()
    End Sub
    
    Function boxBanner() As String
        Dim res As String = String.Empty
        Dim _boxBannerKey As String = "hpBoxBannerWide:" & _langKey.Id
        If Request("cache") = "false" Then CacheManager.RemoveByKey(_boxBannerKey)
        res = CacheManager.Read(_boxBannerKey, 1)
        If String.IsNullOrEmpty(res) Then
            res = Me.BusinessDictionaryManager.Read("hpBoxBannerWide", _langKey.Id, "")
            
            If String.IsNullOrEmpty(res) Then Return String.Empty
            CacheManager.Insert(_boxBannerKey, res, 1, 60)
        End If
        Return res
    End Function
</script>
<%=boxBanner()%>