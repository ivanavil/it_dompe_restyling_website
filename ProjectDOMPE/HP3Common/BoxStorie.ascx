﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _idCont As Integer = 0
	Private _cacheKey As String = String.Empty
	Protected _secondMenu As String = String.Empty

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
        _idCont = Me.PageObjectGetContent.Id
    End Sub
    
    Sub Page_Load()
		_cacheKey = "_cacheBoxStorie:" & _langKey.Id & "|" & _currTheme.Id & "|" & _siteArea.Id & "|" & contentId
		If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_secondMenu = _strCachedContent
		Else
			LoadMenu()
		End If		
		
		
        If Not Page.IsPostBack Then
            LoadMenu()
        End If
    End Sub
    
    Sub LoadMenu()
		Dim sb As New StringBuilder()
	
        Dim cntcoll As New ContentExtraCollection
        Dim cntsearch As New ContentExtraSearcher
        Dim cntVal As New ContentExtraValue
        cntsearch.key.IdLanguage = _lang
        cntsearch.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        cntsearch.KeyContentType.Id = Dompe.ContentTypeConstants.Id.ProcessoSelezioneCtype
        cntsearch.SortType = ContentGenericComparer.SortType.ByData
        cntsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
        'cntsearch.KeysToExclude = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(_idCont, _lang)
        cntsearch.Properties.Add("Key.Id")
        cntsearch.Properties.Add("Key.PrimaryKey")
        cntsearch.Properties.Add("Launch")
        cntsearch.Properties.Add("Title")
        cntsearch.Properties.Add("SubTitle")
        cntsearch.Properties.Add("TitleContentExtra")
           
        
        cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
        
        If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
			sb.Append("<h4 class='newsTitle'>" & Me.BusinessDictionaryManager.Read("PLY_TitleBoxManagement", _lang, "PLY_TitleBoxManagement") & "</h4>")
			sb.Append("<div class='boxComunicati managementG'>")

			For Each el As ContentValue In coll
			
				sb.Append("<div " & getClass(el.Key.Id) & ">")
					sb.Append("<div class='img'>")
						sb.Append("<a href='" & GetLink(el) & "'><img src='/ProjectDOMPE/_slice/plus.png' alt='plus'></a>")
					sb.Append("</div>")
					sb.Append("<div class='info'>")
					
					sb.Append("<p><a href='" & GetLink(el)  & "'>" & el.TitleContentExtra & "<span class='dateCom' style='font-size:14px !important;'>" &  el.SubTitle & "</span> </a></p>")
					
					sb.Append("</div>")
				sb.Append("</div>")
			
			Next
		
		
			sb.Append("</div>")
		
            'rptSiteMenu.DataSource = cntcoll
            'rptSiteMenu.DataBind()
        End If
		
		_secondMenu = sb.ToString()
		
		'Cache Content and send to the oputput-------
		CacheManager.Insert(_cacheKey, _secondMenu, 1, 120)
		'--------------------------------------------		
      
    End Sub
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 72, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
       
    'GetLink by Theme Domain/Name
    'Function GetLink1(ByVal vo As ContentValue) As String
       
    '    Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(200), Me.PageObjectGetMasterPage, False)
    '    'End If
        
    '    Return String.Empty 'potrebbe restiture URL SiteDominio
    'End Function
    Function getClass(ByVal idContent As Integer) As String
        Dim res As String = "class=""singleCom"""
        
        If idContent = _idCont Then
            res = "class=""singleCom singleComContent"""
        End If
        Return res
    End Function
       
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $(".singleCom ").hover(
			function () {
			    $(this).addClass("singleComBg");
			},
			function () {
			    $(this).removeClass("singleComBg");
			}
		);
    });
</script>
<div class="rightBodyRow">
	<%=_secondMenu%>
	
           <asp:Repeater ID="rptSiteMenu" visible="false" runat="server">
                <HeaderTemplate>
                <h4 class="newsTitle"><%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxManagement", _lang, "PLY_TitleBoxManagement")%></h4>
                <div class="boxComunicati managementG">
                </HeaderTemplate> 
                <ItemTemplate>
					<div <%# getClass(DataBinder.Eval(Container.DataItem, "Key.Id")) %>>
						<div class="img">
							<a href="<%# GetLink(Container.DataItem) %>"><img src="/ProjectDOMPE/_slice/plus.png" alt="plus"></a>
						</div>
						<div class="info">
							<p><a href="<%# GetLink(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "TitleContentExtra")%> <span class="dateCom" style=" font-size:14px !important;"> <%# DataBinder.Eval(Container.DataItem, "SubTitle") %> </span> </a>
							<%--  <%# Left(DataBinder.Eval(Container.DataItem, "FullText"), 50)%> <%# IIf(DataBinder.Eval(Container.DataItem, "FullText").ToString.Length > 50, "...", "")%> <br />--%> 
							</p>
						</div>
					</div>
               </ItemTemplate>
             <FooterTemplate></div></FooterTemplate>
     </asp:Repeater>
</div>