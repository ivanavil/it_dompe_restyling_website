<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Private _count As Integer = 0
    Private _urlHome As String = ""
    Private _lang As Integer = 1
    Private _codeLang As String = "it"
    Private _footerLinks As StringBuilder = New StringBuilder
	Private _cacheKey As String = String.Empty
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
        _codeLang = Me.PageObjectGetLang.Code
    End Sub
    
    Sub page_load()
        LoadFooter(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Footer)
        _urlHome = "http://" & Request.Url.Host
    End Sub
    
    Sub LoadFooter(ByVal domain As String, ByVal name As String)
		_cacheKey = "cacheKey:Footerlinks_" & _lang
		If Request("cache") = "false" Then CacheManager.RemoveByKey(_cacheKey)
        Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not String.IsnullOrEmpty(_strCachedContent) Then 
			LtlfooterLinks.Text = _strCachedContent
		Else
			Dim so As New ThemeSearcher		
            so.KeySite = Me.PageObjectGetSite
            so.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
            so.KeyFather.Domain = domain
            so.KeyFather.Name = name
              
            so.LoadHasSon = False
            so.LoadRoles = False
        
            Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any Then
                Dim ii As Integer = 0
                _footerLinks.Append("<ul>")
                For Each cntVal As ThemeValue In coll
				
				_footerLinks.Append("<li style=""font-size:12px;""><a href=""" & GetLink(cntVal) & """>" & cntVal.Description & "</a></li>")
                    ii = ii + 1
                Next
                '_footerLinks.Append("<li><a href=""javascript:ExternalLinkPopup" & _codeLang.ToLower & "('http://www.philogen.com/');"">Philogen</a></li><li><a href=""javascript:ExternalLinkPopup" & _codeLang.ToLower & "('http://www.amgendompe.it/');"">Amgen Domp�</a></li>")
                _footerLinks.Append("</ul>")
            End If
            LtlfooterLinks.Text = _footerLinks.ToString
            CacheManager.Insert(_cacheKey, _footerLinks.ToString, 1, 60)
		End If
    End Sub

    Function GetLink(ByVal theme As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(theme, False)
    End Function
</script>


<footer>
    <asp:Literal ID="LtlfooterLinks" runat="server"></asp:Literal>
    <div class="txt-footer">
    	<%= Me.BusinessDictionaryManager.Read("PLY_copyrightText", Me.PageObjectGetLang.Id, "PLY_copyrightText").Replace("[YEAR]", DateTime.Now.ToString("yyyy")).Replace("[SEDI]", Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idSedi, _lang))%>
        <%= Me.BusinessDictionaryManager.Read("MicrodatiFooter", Me.PageObjectGetLang.Id, "")%>
    </div>

</footer><!--end footer-->



<%--                   <div class="footer">
                       <div class="container">
<div class="footerBox">
<div class="txtFooter">

</div>
</div>
</div>
</div>

<div class="copyright">
<p><%= Me.BusinessDictionaryManager.Read("PLY_copyrightText", Me.PageObjectGetLang.Id, "PLY_copyrightText").Replace("[YEAR]", DateTime.Now.ToString("yyyy"))%></p>
</div>
<%= Me.BusinessDictionaryManager.Read("MicrodatiFooter", Me.PageObjectGetLang.Id, "")%>--%>
