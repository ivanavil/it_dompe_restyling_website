﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _contID As Integer = 0
    Protected _literalNews As StringBuilder = New StringBuilder
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        Dim collectNews As ContentCollection = Nothing
        _contID = Me.PageObjectGetContent.Id
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("BoxNews_" & _lang & "_" & _contID)
        If useCache Then
            TxtListNews.Text = CacheManager.Read("BoxNews_" & _lang & "_" & _contID, 1).ToString
        Else
            
            If _contID > 0 Then
                collectNews = LoadContentAssociati(_contID)
            Else
                collectNews = LoadMenu()
            End If
            
            If Not collectNews Is Nothing AndAlso collectNews.Count > 0 Then
                Dim i As Integer = 0
                For Each cntVal As ContentValue In collectNews
                    Dim classext As String = IIf(i Mod 2 = 0, "rightBodyLeft", "rightBodyRight")
                    _literalNews.Append("<div class=""" & classext & """><div class=""cnt""><a title=""" & cntVal.Title & """ class=""effectHover"" href=""" & getlinkCont(cntVal) & """>" & cntVal.Title & "</a><a title=""" & cntVal.Title & """ href=""" & getlinkCont(cntVal) & """>" & GetImage(cntVal.Key.Id, cntVal.Title) & "</a><p class=""date"">" & Format(cntVal.DateInsert.Value, "short date").Replace("/", ".") & "</p><h3><a title=""" & cntVal.Title & """ href=""" & getlinkCont(cntVal) & """>" & cntVal.Title & "</a></h3><p>" & IIf(cntVal.Launch.Length > 120, Left(cntVal.Launch, 120) & "...", cntVal.Launch) & "</p> <p class=""readMore""><a href=""" & getlinkCont(cntVal) & """ title=""" & Me.BusinessDictionaryManager.Read("DMP_goto", _lang, "DMP_goto") & """>" & Me.BusinessDictionaryManager.Read("DMP_goto", _lang, "DMP_goto") & "</a></p></div></div>")
                    i = i + 1
                Next
            End If
            TxtListNews.Text = _literalNews.ToString
            CacheManager.Insert("BoxNews_" & _lang & "_" & _contID, _literalNews.ToString, 1, 60)
        End If
           
        
    End Sub
    
    Function LoadMenu(Optional ByVal top As Integer = 2, Optional ByVal pkexclude As Integer = 0) As ContentCollection
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
        Dim iDPkTheme As Integer = 0
        Dim idContentTheme As Integer = 0
       
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNews))
        idContentTheme = valthem.KeyContent.Id
        iDPkTheme = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _lang)
        
        so.key.IdLanguage = _lang
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
        so.KeySiteArea.Name = Dompe.SiteAreaConstants.News
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SetMaxRow = top
        
        If pkexclude > 0 Then
            so.KeysToExclude = pkexclude & "," & iDPkTheme
        End If
                           
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        End If
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, _lang), 185, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
      
        Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idNews), Me.PageObjectGetMasterPage, False)
        
    End Function
    
    Function LoadContentAssociati(ByRef IDCont As Integer) As ContentCollection
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        
        Dim pk As Integer = Me.BusinessContentManager.ReadPrimaryKey(IDCont, _lang)
        
        If pk > 0 Then
            soRel.Content.Key.PrimaryKey = pk
            soRel.Content.KeyRelationType.Id = 1
            soRel.Content.RelationSide = RelationTypeSide.Right
        End If
        so.key.IdLanguage = _lang
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
        so.KeySiteArea.Name = Dompe.SiteAreaConstants.News
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
        so.SetMaxRow = 2
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            If coll.Count = 1 Then
                Dim collTmp As ContentCollection = LoadMenu(1, coll(0).Key.PrimaryKey)
                If Not collTmp Is Nothing AndAlso collTmp.Count > 0 Then
                    coll.Add(LoadMenu(1)(0))
                End If
                Return coll
            Else
                Return coll
            End If
        Else
            Return LoadMenu()
        End If
    End Function
    
        
</script>

<%--<%#iif(Container.ItemIndex = 0,"singleRelVideo noMargin","singleRelVideo") %>--%>

        <h4 class="newsTitle"><%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxNews", Me.PageObjectGetLang.Id, "PLY_TitleBoxNews")%></h4>	

        <div class="rightBodyRow">
        <asp:Literal ID="TxtListNews" runat="server" Text=""></asp:Literal>
        </div>
  
  
    
  

