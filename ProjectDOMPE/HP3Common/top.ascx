<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Protected _currentTheme As ThemeValue = Nothing
    Protected _currentCT As ContentTypeIdentificator = Nothing
    Protected _urlHome As String = ""
    Protected _totItem As Integer = 0
    Private totMenu As Integer = 0
    Private _Father As Integer = 0
   
    Private _langCode As String = String.Empty
    Private _langId As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        
        _siteFolder = Me.ObjectSiteFolder()
        _currentCT = Me.PageObjectGetContentType
        '_langCode = Me.PageObjectGetLang.Code.ToUpper
        _langId = Me.PageObjectGetLang.Id
        _urlHome = Helper.GetThemeLink(Me.BusinessMasterPageManager, ThemeConstants.idHomeTheme, _langId)
        
    End Sub
    
    Sub page_load()
	

        If Not (Me.PageObjectGetTheme Is Nothing) AndAlso Me.PageObjectGetTheme.Id > 0 Then
            _currentTheme = Me.BusinessThemeManager.Read(Me.PageObjectGetTheme)
            'Dim userAgent As String = HttpContext.Current.Request.UserAgent.ToLower()
            'If (userAgent.Contains("iphone") Or userAgent.Contains("ipad")) Then
            'End If
        
            hdntheme.Value = _currentTheme.Key.Id

            LoadMainMenu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
		End If 
            'If (_currentTheme.Name <> Dompe.ThemeConstants.Name.Home) Then
            'Dim ctrlPath1 As String = String.Format("/{0}/HP3Common/BreadCrumbs.ascx", Me._siteFolder)
            'LoadSubControl(ctrlPath1, Me.plhBreadCrumbs)
            'End If
            'openByTheme(_currentTheme.Id)
    End Sub
    
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Sub openByTheme(ByVal idtheme As Integer)
        Dim themeVal As New ThemeValue
        themeVal = Me.BusinessThemeManager.Read(New ThemeIdentificator(idtheme))
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ subMenuOpen(" & themeVal.KeyFather.Id & ");})", True)
    End Sub
    
    Sub LoadMainMenu(ByVal domain As String, ByVal name As String)
		Dim _cacheKey As String = "cacheKeyMenu:" & Me.PageObjectGetSite.Id & "|" & Me.PageObjectGetLang.Id & "|" &  Me.PageObjectGetTheme.Id
		
		
		'Temporaneo per gestire il logo del web health awards. Nel caso decommentare.
        'Dim _strWhaUrl as String = String.Empty
        '      Dim _strWhaTitle As String = String.Empty
        
        '      Select Case Me.PageObjectGetLang.Id
        '          Case 1
        '              _strWhaUrl = "/news/413_per-dompe-una-vittoria-a-stelle-e-strisce.html"
        '              _strWhaTitle = "Per Domp� una vittoria a stelle e strisce"
        '          Case 2
        '              _strWhaUrl = "/news-en/413_2014-web-health-award-for-dompe.html"
        '              _strWhaTitle = "Web Health Awards for Domp�"
        '          Case 3
        '              _strWhaUrl = "/te-rejat/413_per-dompe-nje-fitore-me-yje-dhe-vija.html"
        '              _strWhaTitle = "P�r Domp�-nj� fitore me yje dhe vija"
        '      End Select
        '-------------------------------------
		
        Dim _strMenu As String = CacheManager.Read(_cacheKey, 1)
		If Not String.IsnullOrEmpty(_strMenu) Then 
			ltlMenu.Text = _strMenu
		Else
			Dim so As New ThemeSearcher
			so.KeySite = Me.PageObjectGetSite
			so.KeyLanguage = Me.PageObjectGetLang
			so.KeyFather.Domain = domain
			so.KeyFather.Name = name
			so.LoadHasSon = False
			so.LoadRoles = False
			Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
			If Not coll Is Nothing AndAlso coll.Any Then
				'For Each thmVal As ThemeValue In coll
				'    LoadSubMenu(thmVal)
				'Next
				
				Dim _intCounter As Integer = 1
				
				Dim ssssbb as StringBuilder = New StringBuilder()
				ssssbb.Append("<ul class='sf-menu'>")
                Dim currentclass As String = String.Empty
                
                For Each thm As ThemeValue In coll
                    ssssbb.AppendFormat("<li id=""d{0}"" {1}>", _intCounter, GetClass(thm.Key.Id, _intCounter))
                    ssssbb.Append("<a title='" & thm.Description & "' href='" & Me.BusinessMasterPageManager.GetLink(thm, False) & "'>" & thm.Description & "</a>")
                    
                    
                        ssssbb.Append("<ul>")
                    
                    
                    Dim _internalThemColl As ThemeCollection = LoadRptSubMenu(thm)
                   
                    For Each internalThemeValue As ThemeValue In _internalThemColl
                        
                        If _currentTheme.Key.Id = internalThemeValue.Key.Id Or (internalThemeValue.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista And _currentTheme.KeyFather.Id = Dompe.ThemeConstants.idAreaPress) Then
                            currentclass = "current"
                        Else
                            currentclass = String.Empty
                        End If
                        ssssbb.Append("<li id='s" & internalThemeValue.Key.Id & "' class='" & currentclass & "'>")
                        ssssbb.Append("<a title='" & internalThemeValue.Description & "' href='" & GetLinkSL(internalThemeValue) & "'><span><span>" & internalThemeValue.Description & "</span></span></a>")
                        ssssbb.Append("</li>")
                    Next
                    ssssbb.Append("</ul>")
                    'End If
                    
                    ssssbb.Append("</li>")
                    _intCounter += 1
                Next
                'ssssbb.Append("</ul><a style='position:absolute;right:8px;bottom:5px;' href='" & _strWhaUrl & "' title='" & _strWhaTitle & "'><img src='/ProjectDompe/_slice/ws2014.jpg' alt='" & _strWhaTitle & "' style='width:70px;' /></a>")
                ssssbb.Append("</ul>")
                CacheManager.Insert(_cacheKey, ssssbb.ToString(), 1, 60)
                ltlMenu.Text = ssssbb.ToString()
				
                'rptMenu.DataSource = coll
                'rptMenu.DataBind()
                
            End If
        End If
    End Sub

    
    Function LoadRptSubMenu(ByVal thmval As ThemeValue) As ThemeCollection
        Dim themes As ThemeCollection = Nothing
        Dim Roles As Boolean = False
                  
        If Not thmval Is Nothing Then
                                  
            themes = ReadMyTHChildren(thmval.Key)
      
        End If
        Return themes
    End Function
    
    Function ReadMyTHChildren(ByVal key As ThemeIdentificator) As ThemeCollection
        Dim coll As ThemeCollection = Nothing
        
        If Not key Is Nothing AndAlso key.Id > 0 Then
            Dim soTheme As New ThemeSearcher
            
            soTheme.KeyFather.Id = key.Id
            soTheme.KeySite = Me.PageObjectGetSite
            soTheme.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
            soTheme.LoadHasSon = False
            soTheme.LoadRoles = False
           
            coll = Me.BusinessThemeManager.Read(soTheme)
                    
        End If
          
        Return coll
    End Function
    
    
    'It loads the languages related to the site
    Function LoadLanguages() As String
        Dim CacheLngkey As String = String.Empty
          
        Dim CurrentLang As String = Me.PageObjectGetLang.Code
        
        Dim langSearcher As New LanguageSearcher
        Dim langColl As New LanguageCollection

        langColl = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(Me.PageObjectGetSite.Id))
        langSearcher = Nothing
        
        ' Dim str As String = String.Empty
        Dim i As Integer = 0
        
        Dim allLangs As String = String.Empty
        allLangs = "<ul>"
        
        For Each elem As LanguageValue In langColl
                        
            allLangs = allLangs & "<li>"
            
            If Not CurrentLang.ToLower.Equals(elem.Key.Code.ToLower) Then allLangs = allLangs & "<a href=" & getLink(elem) & " title=" & elem.Description & ">" & elem.Description & "</a>"
            If CurrentLang.ToLower.Equals(elem.Key.Code.ToLower) Then allLangs = allLangs & "<span>" & elem.Description & "</span>"
            
            If (i <> langColl.Count - 1) Then allLangs = allLangs & "<span> | </span>"
                                 
            allLangs = allLangs & "</li>"
           
                
            i = i + 1
        Next
        
        allLangs = allLangs & "</ul>"
        
        Return allLangs
    End Function
    
    Function getLink(ByVal langValue As LanguageValue) As String
        Return Me.BusinessMasterPageManager.GetLink(langValue.Key, False)
    End Function
    
    Function GetLink() As String
        Return Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
    End Function
    
    'GetLink by Theme Domain/Name
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
        
    Function GetLink(ByVal vo As MenuItemValue) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.PageObjectGetLang
        so.KeySite = Me.PageObjectGetSite
        so.Key = vo.KeyTheme
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
       
        Return Nothing
    End Function
    
    Function GetLink(ByVal vo As ThemeValue) As String
   
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
         
    End Function
    
    Function getLinkFirstTheme(ByVal vo As ThemeValue) As String
        Dim strLink As String = "javascript:void(0);"
        Dim themSearch As New ThemeSearcher
        Dim themColl As New ThemeCollection
        
        themSearch.KeyFather.Id = vo.Key.Id
        themSearch.KeyLanguage.Id = vo.KeyLanguage.Id
        themSearch.Properties.Add("Key.Id")
        themSearch.Properties.Add("KeyContent.Id")
              
        themColl = Me.BusinessThemeManager.Read(themSearch)
        
        If Not themColl Is Nothing AndAlso themColl.Count > 0 Then
            strLink = Me.BusinessMasterPageManager.GetLink(New ContentIdentificator(themColl(0).KeyContent.Id, _langId), themColl(0), False)
        End If
        Return strLink
  
    End Function
    
    
    Function GetLinkSL(ByVal vo As ThemeValue) As String
        Dim linkFinale As String = Me.BusinessMasterPageManager.GetLink(vo, False)
        
               
        If vo.Key.Id = Dompe.ThemeConstants.idManagement Then
            'Response.Write("test") : Response.End()
            'prendo il primo contenuto con relazione al context 1
            Dim so As New ContentSearcher
            so.KeySite = Me.PageObjectGetSite
            so.KeySiteArea.Id = vo.KeySiteArea.Id
            so.key.IdLanguage = vo.KeyLanguage.Id
            so.KeyContext.Id = Dompe.ContextCostant.idMainContent
            Me.BusinessContentManager.Cache = False
            
            Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Me.BusinessMasterPageManager.Cache = False
                'linkFinale = Me.BusinessMasterPageManager.GetLink(coll(0).Key, vo, False)
            End If
        End If
        
        If vo.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Then
			linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
		
            'If Me.BusinessAccessService.IsAuthenticated Then
            '    If Me.ObjectTicket.User.Key.Id > 0 Then
            '        If Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleMediaPress) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langId) '& "#" & temp
                      
            '        ElseIf Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RoleDoctor) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langId) '& "#" & temp
                        
            '        ElseIf Me.ObjectTicket.Roles.Any(Function(role) role.Key.Id = Dompe.Role.RolePartner) Then
            '            Return Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.partnerWelcome, _langId)
            '        End If
                    
                    
            '    Else
            '        linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
            '    End If
            'Else
            '    linkFinale = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista, _langId)
            'End If
        End If
        
        Return linkFinale
    End Function
    
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id)
    End Function
    
    Function GetClass(ByVal idTheme As String, ByVal item As Integer) As String
        Dim res As String = String.Empty
        
        'verifico chi � il padre
        Dim tmpvo As New ThemeValue
        Dim currFatherid As Integer = 0
        tmpvo = _currentTheme
        If Not tmpvo Is Nothing AndAlso tmpvo.Key.Id > 0 Then
            currFatherid = tmpvo.KeyFather.Id
        End If
        Dim currentclass As String = String.Empty
        'Response.Write("- _currentTheme" & _currentTheme.Id & " idTheme" & idTheme & " currFatherid" & currFatherid)
        If _currentTheme.Key.Id = idTheme Or CInt(idTheme) = currFatherid Or (currFatherid = Dompe.ThemeConstants.idAreaPress And idTheme = Dompe.ThemeConstants.idMedia) Then
            currentclass = "current"
        Else
            currentclass = String.Empty
        End If
        res = String.Format("class='{0}'", currentclass)
        'Select Case Me.PageObjectGetTheme.Id
        '    Case idTheme
        '        res = String.Format("class=""sf-breadcrumb menu{0} sfHover {1}""", item, currentclass)
        '    Case Dompe.ThemeConstants.idAreaMedico
        '        If idTheme = Dompe.ThemeConstants.idAreaMedico Then
        '            res = String.Format("class=""sf-breadcrumb menu{0} sfHover {1}""", item, currentclass)
        '        End If
        '    Case Else
        '        res = String.Format("class=""menu{0} {1}""", item, currentclass)
        'End Select
            
        Return res
    End Function
    
</script>

   
	<div class="headerBottom" style="display:none;position:relative;">
		<asp:literal id="ltlMenu" runat="server" EnableViewState="false" />       
        
           <asp:Repeater ID="rptMenu" runat="server"  EnableViewState="false" visible="false">
            <HeaderTemplate><ul class="menu sf-menu"></HeaderTemplate>
                <ItemTemplate>
                          <li id="d<%#(Container.ItemIndex + 1) %>" <%# getClass(Container.DataItem.Key.id,Container.ItemIndex + 1) %>><a title="<%# DataBinder.Eval(Container.DataItem, "Description")%>"  href="<%# getLinkFirstTheme(Container.DataItem)%>"> <%# DataBinder.Eval(Container.DataItem, "Description")%></a>
               <asp:Repeater ID="rptSummen" runat="server" DataSource="<%# LoadRptSubMenu(Container.DataItem)%>">
            <HeaderTemplate><ul></HeaderTemplate>  
            <ItemTemplate> <li id='s<%# DataBinder.Eval(Container.DataItem, "Key.Id")%>'><a title="<%# DataBinder.Eval(Container.DataItem, "Description")%>" href="<%# getLinkSL(Container.DataItem) %>"> <%# DataBinder.Eval(Container.DataItem, "Description")%></a></li></ItemTemplate> 
              <FooterTemplate></ul></FooterTemplate>
               </asp:Repeater>
               </li>
                  </ItemTemplate>
                 <FooterTemplate>
                 </ul>  
            </FooterTemplate>
         </asp:Repeater><!--end menu-->
     <input type="hidden" id="hdntheme" runat="server" />
     <asp:PlaceHolder ID="plhBreadCrumbs" runat="server"  EnableViewState="false"/>
   
 
  </div>   
          <!--end menu-->


 



