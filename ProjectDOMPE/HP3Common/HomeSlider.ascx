﻿<%@ Control Language="VB" ClassName="HomeSlider" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _researchLbl As String = String.Empty
    Protected _officesLbl As String = String.Empty
    Protected _csrLbl As String = String.Empty
    
    Protected Sub Page_Init()
        _siteFolder = Me.ObjectSiteFolder
        _langKey = Me.PageObjectGetLang
        _researchLbl = Me.BusinessDictionaryManager.Read("DMP_LaRicerca", _langKey.Id, "DMP_LaRicerca")
        _officesLbl = Me.BusinessDictionaryManager.Read("DMP_SediLb", _langKey.Id, "DMP_SediLb")
        _csrLbl = Me.BusinessDictionaryManager.Read("DMP_CSR", _langKey.Id, "DMP_CSR")
    End Sub
   
    Sub page_load()

    End Sub
    
    Function boxSliderCampaign() As String
        Dim res As String = String.Empty
        Dim bannSerch As New BannerSearcher
        Dim bannColl As New BannerCollection
       
        Dim _boxSlideKey As String = "hpBoxSliderNew:" & _langKey.Id
        If Request("cache") = "false" Then CacheManager.RemoveByKey(_boxSlideKey)
        res = CacheManager.Read(_boxSlideKey, 1)
        If String.IsNullOrEmpty(res) Then
            bannSerch.KeyBox.Id = 3
            bannSerch.KeyLanguage.Id = _langKey.Id
            bannSerch.KeySite.Id = 46
  
            bannSerch.SortType = BannerGenericComparer.SortType.ByDateStart
            bannSerch.SortOrder = BannerGenericComparer.SortOrder.ASC
            bannColl = Me.BusinessAdvertisingManager.Read(bannSerch)
            
            If Not bannColl Is Nothing AndAlso bannColl.Count > 0 Then
                Dim _sb As StringBuilder = New StringBuilder()
                Dim _sbUl As StringBuilder = New StringBuilder()
                Dim i As Integer = 1
                _sb.Append("<div class='slider'>")
                _sb.Append("<div class='imgs2'>")
                _sb.Append("<div class='imgs'>")
                _sbUl.Append("<ul>")
                Dim strSel As String = String.Empty
                For Each itm As BannerValue In bannColl
                    
                    _sb.Append(String.Format("<div class='img{0}'>", i))
                    _sb.Append(String.Format("<h3" & IIf(Not String.IsNullOrEmpty(itm.Alt), " " & itm.Alt, String.Empty) & "><a title='{0}' href='{1}'{3}>{2}</a></h3>", itm.Name, itm.Link, itm.Name, getStyle(itm.Alt)))
                    _sb.Append(String.Format("<a title='{0}' href='{1}'><img src='/HP3Banner/{2}' alt='{3}'></a>", itm.Name, itm.Link, itm.Source, itm.Name))
                    _sb.Append("</div>")
                    
                    If i = 1 Then
                        strSel = "selected"
                    Else
                        strSel = String.Empty
                    End If
                    
                    _sbUl.Append(String.Format("<li class='item{0}'><a href='javascript:void(0);' onClick='javascript:slider({1});' class='{2}' title='{3}'>{4}</a>", i, i, strSel, itm.Name, ""))
                    
                    i += 1
                Next
                _sbUl.Append("</ul>")
                _sb.Append("</div>")
                _sb.Append("<div class='meter-wrap'><div class='meter-value'></div></div>")
                _sb.Append(_sbUl.ToString())
                _sb.Append("</div>")
                _sb.Append("</div>")
                
                res = _sb.ToString
                CacheManager.Insert(_boxSlideKey, res, 1, 60)
            End If
        End If
        Return res
    End Function
    
    Function getStyle(ByVal _strLabel As String) As String
        If Not String.IsNullOrEmpty(_strLabel) Then
            Return " style='color:" & _strLabel & ";'"
        End If
        
        Return String.Empty
    End Function
</script>


<%=boxSliderCampaign() %>



<script type="text/javascript">

    var wi = 0;
    var wicnt = 0;
    var items = 0;

    $(document).ready(function () {

        var wi3 = $(window).width() * 3;
        var wi1 = $(window).width();
        $('.imgs').css('width', wi3 + 'px');
        $('.imgs div').css('width', wi1 + 'px');
        $(".imgs .img1").css('left', '0');
        $(".imgs .img2").css('left', $(window).width() + 'px');
        $(".imgs .img3").css('left', $(window).width() * 2 + 'px');

        slideHome();

    });


    function slideHome() {
        wi = wi + 0.1;
        wicnt++;
        //$('.meter-value').css('width', wi + '%');
        if (wicnt == 330) {
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item2 a').addClass('selected');
            items = 1;
            $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                $(".imgs .img1").css('left', $(window).width() * 2 + 'px');
                $(".imgs .img2").css('left', '0');
                $(".imgs .img3").css('left', $(window).width() + 'px');
                $(".imgs").css('left', '0');
            });
        }
        if (wicnt == 666) {
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item3 a').addClass('selected');
            items = 2;
            $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                $(".imgs .img2").css('left', $(window).width() * 2 + 'px');
                $(".imgs .img3").css('left', '0');
                $(".imgs .img1").css('left', $(window).width() + 'px');
                $(".imgs").css('left', '0');
            });
        }
        if (wicnt == 1000) {
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item1 a').addClass('selected');
            items = 0;
            $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                $(".imgs .img3").css('left', $(window).width() * 2 + 'px');
                $(".imgs .img1").css('left', '0');
                $(".imgs .img2").css('left', $(window).width() + 'px');
                $(".imgs").css('left', '0');
            });
            wi = 0;
            wicnt = 0;
        }
        var tt = setTimeout(function () { slideHome() }, 20);
    }



    function slider(id) {
        if (id == 1) {
            wi = 0;
            wicnt = 0;
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item1 a').addClass('selected');
            if (items == 0) {
            } else if (items == 1) {
                $(".imgs").animate({ left: "-=" + $(window).width() * 2 }, 2000, function () {
                    $(".imgs .img3").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img1").css('left', '0');
                    $(".imgs .img2").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            } else {
                $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                    $(".imgs .img3").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img1").css('left', '0');
                    $(".imgs .img2").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            }
            items = 0;
        } else if (id == 2) {
            wi = 33;
            wicnt = 330;
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item2 a').addClass('selected');
            if (items == 0) {
                $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                    $(".imgs .img1").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img2").css('left', '0');
                    $(".imgs .img3").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            } else if (items == 1) {
            } else {
                $(".imgs").animate({ left: "-=" + $(window).width() * 2 }, 2000, function () {
                    $(".imgs .img1").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img2").css('left', '0');
                    $(".imgs .img3").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            }
            items = 1;
        } else {
            wi = 66.6;
            wicnt = 666;
            $('.slider ul li a').removeClass('selected');
            $('.slider ul li.item3 a').addClass('selected');
            if (items == 0) {
                $(".imgs").animate({ left: "-=" + $(window).width() * 2 }, 2000, function () {
                    $(".imgs .img2").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img3").css('left', '0');
                    $(".imgs .img1").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            } else if (items == 1) {
                $(".imgs").animate({ left: "-=" + $(window).width() }, 2000, function () {
                    $(".imgs .img2").css('left', $(window).width() * 2 + 'px');
                    $(".imgs .img3").css('left', '0');
                    $(".imgs .img1").css('left', $(window).width() + 'px');
                    $(".imgs").css('left', '0');
                });
            } else {
            }
            items = 2;
        }
    }
</script>