﻿<%@ Control Language="VB" ClassName="MenuStorie" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>

<script runat="server">
	Protected contentId As Integer = 0
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    Private _cacheKey As String = String.Empty
	
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
        _contentKey = Me.PageObjectGetContent
        _currTheme = GetTheme()
        contentId = Me.PageObjectGetContent.Id
    End Sub
    
    Dim _canCheckApproval As Boolean = False
    
    Protected Sub Page_Load()
        _cacheKey = "_cacheMenuStorie:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        
		If Request("cache") = "false" Or _canCheckApproval Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		
		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_strMenu = _strCachedContent
		Else
			InitView()
		End If	
    End Sub
    
    Protected Sub InitView()
        LoadMenu()
    End Sub
    
    Protected Sub LoadMenu()
        Dim so As New ContentExtraSearcher
        so.KeySiteArea = _siteArea
        so.key.IdLanguage = _langKey.Id
        so.CalculatePagerTotalCount = False
        so.PageSize=1
        If Not _canCheckApproval Then so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
        
        Dim baseSo As New ContentBaseSearcher()
        baseSo.key.Id = _currTheme.KeyContent.Id
        baseSo.key.IdLanguage = _langKey.Id
        
        Dim baseCont = Me.BusinessContentManager.GetContentBaseValue(baseSo)
        If Not baseCont Is Nothing Then
            so.KeysToExclude = baseCont.Key.PrimaryKey.ToString()
        end if
        
        Dim coll As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        Dim sBuilder As New StringBuilder()
        If Not coll Is Nothing AndAlso coll.Any() Then
            Dim currcontent As New ContentIdentificator
            currcontent = Me.BusinessMasterPageManager.GetContent
            'sBuilder.AppendFormat("<div class='generic relatedContent'><p>{0}</p></div>", Me.BusinessDictionaryManager.Read("DMP_Stories", _langKey.Id, "DMP_Stories"))
            sBuilder.Append("<ul>")
            Dim sel As String = String.Empty
            Dim classBig As String = String.Empty
            For Each el As ContentExtraValue In coll
			
				If el.Key.Id <> 561 Then
			
					If el.Key.Id = currcontent.Id Then
						sel = "sel"
					Else
						sel = String.Empty
					End If
					If el.SubTitle.Length > 34 Then
						classBig = "big"
					Else
						classBig = String.Empty
					End If
					sBuilder.AppendFormat("<li class='{0}'> <a class='{1}' title='{2}' href='{3}'>{4} <br /><span style=""color: #000;"">{5}</span></a></li>", classBig, sel, Server.HtmlEncode(el.Title), GetLink(el.Key), el.Title & Healthware.Dompe.Helper.Helper.checkApproval(el.ApprovalStatus,False), el.SubTitle)
                
				End if
            Next
            sBuilder.Append("</ul>")
            
        End If
        _strMenu = sBuilder.ToString()
        sBuilder.Clear()
        		
		'Cache Content and send to the oputput-------
		CacheManager.Insert(_cacheKey, _strMenu, 1, 120)
		'--------------------------------------------	
    End Sub
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher()
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll.FirstOrDefault()
        End If
        
        Return Nothing
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, _currTheme, False)
        
    End Function
    
</script>

<%=_strMenu %>
