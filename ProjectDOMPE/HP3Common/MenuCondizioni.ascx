﻿<%@ Control Language="VB" ClassName="MenuCondizioni" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">

    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _strMenu As String = String.Empty
    
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        
    End Sub
    
    Protected Sub Page_Load()
        
        InitView()
        
    End Sub
    
    Protected Sub InitView()
                
        LoadMenu()
                
    End Sub
    
    Protected Sub LoadMenu()
        
        Dim so As New ThemeSearcher()
        so.KeyFather.Id = Dompe.ThemeConstants.idFooter
        so.LoadHasSon = False
        so.LoadRoles = False
        so.KeyLanguage = _langKey
        Dim coll = Me.BusinessThemeManager.Read(so)
        
        Dim sBuilder As New StringBuilder()
        If Not coll Is Nothing AndAlso coll.Any() Then
                       
            sBuilder.Append("<ul>")
            Dim sel As String = String.Empty
			DIm strAdd As String = String.Empty
            For Each item In coll
				strAdd = String.Empty
				If item.Description.length < 40 Then strAdd = " singleline"
                If item.Key.Id = _currentTheme.Id Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If
                sBuilder.AppendFormat("<li> <a class='{0}' title='{1}' href='{2}'>{3}</a></li>", sel & strAdd, Server.HtmlEncode(item.Description), Me.BusinessMasterPageManager.GetLink(item, False), item.Description)
            Next
            
            sBuilder.Append("</ul>")
        End If
        
        _strMenu = sBuilder.ToString()
        sBuilder.Clear()
        
    End Sub
    
    
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher()
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll.FirstOrDefault()
        End If
        
        Return Nothing
    End Function
    
    
    
</script>

<%=_strMenu %>
