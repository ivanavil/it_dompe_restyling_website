﻿<%@ Control Language="VB" ClassName="BoxRelatedContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<script runat="server">

    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currTheme As ThemeIdentificator = Nothing
    Protected _secondMenu As String = String.Empty
    Protected _contentId As Integer = 0
    Protected _themeId As Integer = 0
    Private _cacheKey As String = String.Empty
	
    Public Property ContentId As Integer
        Get
            Return _contentId
        End Get
        Set(value As Integer)
            _contentId = value
        End Set
    End Property
    
    Public Property ThemeId As Integer
        Get
            Return _themeId
        End Get
        Set(value As Integer)
            _themeId = value
        End Set
    End Property
    
    Protected Sub Page_Init()
        _langKey = Me.PageObjectGetLang
        _siteArea = Me.PageObjectGetSiteArea
        If ContentId = 0 Then ContentId = Me.PageObjectGetContent.Id
        
    End Sub
    
    Protected Sub Page_Load()
        
        _currTheme = IIf(ThemeId > 0, New ThemeIdentificator(ThemeId), Me.PageObjectGetTheme)
        InitView()
    End Sub
    
    Protected Sub InitView()
		_cacheKey = "_cacheBoxRelatedContent:" & _langKey.Id & "|" & _currTheme.Id & "|" & _siteArea.Id & "|" & contentId
        If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If

		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_secondMenu = _strCachedContent
		Else
			LoadMenu()
		End If
    End Sub
    
    Protected Sub LoadMenu()
        
        Dim soRel As New ContentRelatedSearcher
        Dim so As New ContentSearcher()
        Dim sb As New StringBuilder()
        Dim coll As ThemeCollection
        Dim thmSrc As New ThemeSearcher
        Dim thmFthr = GetFatherTheme()
        thmSrc.KeyFather.Id = thmFthr
        thmSrc.KeyLanguage = _langKey
        
        coll = Me.BusinessThemeManager.Read(thmSrc)
               
        If Not coll Is Nothing AndAlso coll.Any() Then
            sb.Append("<ul>")
            
            For Each el As ThemeValue In coll
                sb.Append("<li>")
                sb.Append("<a class='" & IIf(el.Key.Id = _currTheme.Id or (Me.PageObjectGetContent.Id>0 and el.KeyContent.Id = Me.PageObjectGetContent.Id), " current", String.Empty) & IIf(Server.HtmlEncode(el.Description).Length <= 35, " singleline", String.Empty) & "' title='" & Server.HtmlEncode(el.Description) & "' href='" & GetLink(el) & "'>" & el.Description & "</a>")
                'sb.Append(LoadSubMenu(el))
                sb.Append("</li>")
            Next
            sb.Append("</ul>")
        End If
        _secondMenu = sb.ToString()
			
        'Cache Content and send to the oputput-------
        CacheManager.Insert(_cacheKey, _secondMenu, 1, 120)
        '--------------------------------------------
        
    End Sub
    
    Protected Function LoadSubMenu(tv As ThemeValue) As String
        
        Dim soRel As New ContentRelatedSearcher
        Dim so As New ContentSearcher()
        Dim sb As New StringBuilder()
        Dim coll As ThemeCollection
        Dim thmSrc As New ThemeSearcher
        
        thmSrc.KeyFather.Id = tv.Key.Id
        thmSrc.KeyLanguage = _langKey
        'Response.Write(" LoadSubMenu Father:" & tv.Key.Id)
                
        coll = Me.BusinessThemeManager.Read(thmSrc)
               
        If Not coll Is Nothing AndAlso coll.Any() Then
            'Response.Write(" esiste coll temi ")
            sb.Append("<ul class=""third_level"">")
            
            For Each el As ThemeValue In coll
                sb.Append("<li>")
                sb.Append("<a class='" & IIf(el.Key.Id = _currTheme.Id, " current", String.Empty) & IIf(Server.HtmlEncode(el.Description).Length <= 35, " singleline", String.Empty) & "' title='" & Server.HtmlEncode(el.Description) & "' href='" & GetLink(el) & "'>" & el.Description & "</a>")
                sb.Append("</li>")
            Next
            sb.Append("</ul>")
        End If
        Return sb.ToString()
    End Function
    
    Function GetFatherTheme() As Integer
        Dim res As Integer = 0
        
        If _currTheme.Id = Dompe.ThemeConstants.idPipeline Or _currTheme.Id = Dompe.ThemeConstants.idEccellenza Or _currTheme.Id = Dompe.ThemeConstants.idNostraRicerca Then
            res = Dompe.ThemeConstants.idNostraRicerca
        ElseIf _currTheme.Id = Dompe.ThemeConstants.idModello231 Or _currTheme.Id = Dompe.ThemeConstants.idCodiceCondotta Or _currTheme.Id = Dompe.ThemeConstants.idPoliticaQualSicur or _currTheme.Id = Dompe.ThemeConstants.idEtica Then
            res = Dompe.ThemeConstants.idEtica
        Else
            Dim thval As New ThemeValue
            thval = Me.BusinessThemeManager.Read(_currTheme)
            If Not thval Is Nothing Then
                res = thval.KeyFather.Id
            End If
        End If
        
        Return res
    End Function
    
    
    Function GetFatherThemeOld() As Integer
        Dim res As Integer = 0
        
        If _currTheme.Id = Dompe.ThemeConstants.idPipeline Or _currTheme.Id = Dompe.ThemeConstants.idEccellenza Then
            res = Dompe.ThemeConstants.idRicerca
        ElseIf _currTheme.Id = Dompe.ThemeConstants.idModello231 Or _currTheme.Id = Dompe.ThemeConstants.idCodiceCondotta Or _currTheme.Id = Dompe.ThemeConstants.idPoliticaQualSicur Then
            res = Dompe.ThemeConstants.idResponsabilita
        Else
            Dim thval As New ThemeValue
            thval = Me.BusinessThemeManager.Read(_currTheme)
            If Not thval Is Nothing Then
                res = thval.KeyFather.Id
            End If
        End If
        
        Return res
    End Function
    
    Function GetFatherContent() As Integer
        Dim res As Integer = 0
        If _siteArea.Id = Dompe.SiteAreaConstants.IDSiteareaNews And _currTheme.Id = Dompe.ThemeConstants.idNews Then
            Dim thval As New ThemeValue
            thval = Me.BusinessThemeManager.Read(_currTheme)
            If Not thval Is Nothing AndAlso thval.KeyContent.Id > 0 Then
                res= thval.KeyContent.Id
            End If
        End If
        Return res
    End Function
    
    Function GetLink(ByVal keyContent As ContentValue) As String
        
        Dim so As New ThemeSearcher()
        so.KeySiteArea = keyContent.SiteArea.Key
        so.KeyContentType = keyContent.ContentType.Key
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        Dim theme_value = Helper.GetTheme(so)
        
        If Not theme_value Is Nothing AndAlso theme_value.Key.Id > 0 Then
            Return Me.BusinessMasterPageManager.GetLink(keyContent.Key, theme_value, False)
        End If
        
        Return String.Empty
    End Function
    
    Function GetLink(ByVal theme_value As ThemeValue) As String
                       
        If Not theme_value Is Nothing AndAlso theme_value.Key.Id > 0 Then
            Return Me.BusinessMasterPageManager.GetLink(theme_value, False)
        End If
        
        Return String.Empty
    End Function
    
</script>


<%=_secondMenu%>

