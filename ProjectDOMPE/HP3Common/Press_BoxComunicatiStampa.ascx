﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Dim targetlnk As String = ""
    Dim idLingua As Integer
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        idLingua = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        
        If Not Page.IsPostBack Then
            rptSiteMenu.DataSource = LoadMenu()
            rptSiteMenu.DataBind()
           
        End If
      
    End Sub
    
    Function LoadMenu() As ContentCollection
       
        Dim idContentKeyTheme As Integer = GetPKThema()
      
     
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        so.KeySite = Me.PageObjectGetSite
        so.key.IdLanguage = Me.PageObjectGetLang.Id
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDComunicatiStampaAreaMediaPress
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.KeysToExclude = idContentKeyTheme
        so.SetMaxRow = 3
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        Else
            Return Nothing
        End If
        
    End Function
    
    Function GetPKThema() As Integer
        Dim idContentKeyTheme As Integer = 0
        Dim idContentTheme As Integer
        Dim contentextraVal As New ContentExtraValue
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idComunicatiStampaMediaPress))
        idContentTheme = valthem.KeyContent.Id
        
        contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(idContentTheme, Me.PageObjectGetLang.Id))
        If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
            idContentKeyTheme = contentextraVal.Key.PrimaryKey
        End If
        Return idContentKeyTheme
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 50, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = ""
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "javascript:ExternalLinkPopup('" & vo.LinkUrl & "');"
        Else
            Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idLinksUsefulMediaPress), Me.PageObjectGetMasterPage, False)
        End If
    End Function
    
        
</script>
     
           <asp:Repeater ID="rptSiteMenu" runat="server">
           
                <HeaderTemplate>
                <script type="text/javascript" language="javascript">
                    $(document).ready(function () {                       

                            $(".singleCom ").hover(
			                function () {
			                    $(this).addClass("singleComBg");
			                },
			                function () {
			                    $(this).removeClass("singleComBg");
			                }
		                );
                    });   
                </script>
                <div class="rightBodyRightBig">
                    <h4 class="comunicatiTitle"><%= Me.BusinessDictionaryManager.Read("Press_Title_Comunicati", Me.PageObjectGetLang.Id, "Press_Title_Comunicati")%></h4>
                        <div class="boxComunicati">                        
               </HeaderTemplate> 
                <ItemTemplate>
                <div class="singleCom">
                    <div class="img">
                        <img src="<%="/"& _siteFolder &"/_slice/plus.png" %>" />
                    </div>
                    <div class="info">
                        <p class="dateCom"><%# Healthware.Dompe.Helper.Helper.get_FormatData(CDate(DataBinder.Eval(Container.DataItem, "DatePublish")), idLingua)%></p>
                        <p><a href="<%# getlinkCont(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></p>
                      <%--   <p><%# DataBinder.Eval(Container.DataItem, "Launch")%></p>--%>
                    </div>
                </div><!--end singleCom-->               
                </ItemTemplate>
                <FooterTemplate>                        
                    </div>
                </div>
                </FooterTemplate>
            </asp:Repeater>
                              
       
  

  

