<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _tooltip As String = String.Empty
    
         
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _langKey = Me.BusinessMasterPageManager.GetLanguage
    End Sub
    
    Sub page_load()
        Dim word As String = Request("wd")
        tbWord.Value = Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch")
        hdnSearchlabel.Value = Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch")
        _tooltip = Me.BusinessDictionaryManager.Read("DMP_tooltip", _langKey.Id, "DMP_tooltip")
        submitBtnSearch.ToolTip = _tooltip
        tbWord.Attributes("title") = _tooltip
        If Not String.IsNullOrEmpty(word) Then
            tbWord.Value = word 'StringUtility.CleanHtml(word)
        End If
        
    End Sub
    
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, "GO")
    End Function
    
    
    Sub GOSearch(ByVal sender As Object, ByVal e As EventArgs)
       
        If Not String.IsNullOrEmpty(tbWord.Value) AndAlso tbWord.Value <> Me.BusinessDictionaryManager.Read("DMP_btnSearch", _langKey.Id, "DMP_btnSearch") Then
           
            Dim link As String = Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Search)
            link = link & "?wd=" & Server.UrlEncode(tbWord.Value).Replace("+", Uri.EscapeDataString("+"))
            
            Response.Redirect(link, False)
        End If
    End Sub
           
    'GetLink by Theme Domain/Name
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
        
        so.KeyLanguage = _langKey
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        so.Display = SelectOperation.Disabled
      
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
</script>
<style type="text/css">
    .hidden
    {
        display:none;
    }
</style>
<script type="text/javascript">

    function Instesto(nomeControllo, valore) {
        var valDict = $("#<%= hdnSearchlabel.ClientID %>").val();
        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valDict;
        }
        return false;
    }

    function CancellaValue(nomeControllo, valore) {
        var valDictio = $("#<%= hdnSearchlabel.ClientID %>").val();
      if (document.getElementById(nomeControllo).value == valDictio) {
           document.getElementById(nomeControllo).value = "";
       }
       return false;
   }
  

</script>

 <asp:Panel ID="SearchPanel" runat="server" DefaultButton="submitBtnSearch">
    <div class="search">
        <asp:LinkButton id="submitBtnSearch" runat="server" CssClass="hidden" onclick="GOSearch" ValidationGroup="logSearch"> <%= GetLabel("DMP_Search")%></asp:LinkButton>
         <a href="#" onclick="mngSrcBox()" title="Search">Search</a>
        <input type="text" ID="tbWord" title="" onfocus="javascript:CancellaValue(this.id,'Search');" onblur="javascript:Instesto(this.id,'Search');" runat="server" />
        <asp:RequiredFieldValidator ID="reqLog" InitialValue="Search" ControlToValidate="tbWord" Display="None"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup="logSearch" EnableViewState ="false" />
        <input id="hdnSearchlabel" type="hidden" runat="server" value="" />         
    </div>
</asp:Panel>  
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%=submitBtnSearch.ClientID %>").click(function () {
            var tmpTxt = $('#<%=tbWord.ClientID%>').val();
            if (tmpTxt.trim().length < 3) {
                alert('<%=Me.BusinessDictionaryManager.Read("DMP_SearchReq", _langKey.Id, "DMP_SearchReq")%>');
                return false;
            }
        });
    })

    function mngSrcBox() {
        if ($('#<%=tbWord.ClientID%>').css('display') == 'none') {
            $('#<%=tbWord.ClientID%>').show();
            $('.link-reserved').hide();
            $('.sel-area').hide();
        } else {
            $('#<%=tbWord.ClientID%>').hide();
            $('.link-reserved').show();
            $('.sel-area').show();
        }
    }

</script>