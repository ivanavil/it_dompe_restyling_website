﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langCode As String = String.Empty
    Protected _langId As Integer = 0
    Protected _contID As Integer = 0
    Protected _literalLinks As StringBuilder = New StringBuilder
    Protected _MAXLinks As Integer = 6
    Dim targetlnk As String = ""
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _langCode = Me.PageObjectGetLang.Code.ToLower
        _langId = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
       
        Dim collectLinks As ContentCollection = Nothing
        _contID = Me.PageObjectGetContent.Id
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("BoxLinks_" & _langId & "_" & _contID)
        If useCache Then
            TxtListLinks.Text = CacheManager.Read("BoxLinks_" & _langId & "_" & _contID, 1).ToString
        Else
            If _contID > 0 Then
                collectLinks = LoadContentAssociati(_contID)
            Else
                collectLinks = LoadMenu()
            End If
            
            If Not collectLinks Is Nothing AndAlso collectLinks.Count > 0 Then
                Dim i As Integer = 0
                For Each cntVal As ContentValue In collectLinks
                    _literalLinks.Append("<h3><a href=""" & getlinkCont(cntVal) & """ onclick=""" & getTrackCont(cntVal) & """>" & cntVal.Title & "</a></h3>")
                    i = i + 1
                Next
            End If
            TxtListLinks.Text = _literalLinks.ToString
            CacheManager.Insert("BoxLinks_" & _langId & "_" & _contID, _literalLinks.ToString, 1, 60)
        End If
             
    End Sub
    
    Function LoadMenu(Optional ByVal numItem As Integer = 0, Optional ByVal collectItem As ContentCollection = Nothing) As ContentCollection
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
        so.key.IdLanguage = _langId
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaLinks
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        If numItem > 0 Then
            so.SetMaxRow = numItem
        Else
            so.SetMaxRow = _MAXLinks
        End If
     
        If Not collectItem Is Nothing Then
            Dim strExclude As String = ""
            For Each itm As ContentValue In collectItem
                strExclude = strExclude + itm.Key.PrimaryKey + ";"
            Next
            so.KeysToExclude = strExclude
        End If
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        End If
    End Function
    
    
    Function LoadContentAssociati(ByRef IDCont As Integer) As ContentCollection
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
      
        Dim pk As Integer = Me.BusinessContentManager.ReadPrimaryKey(IDCont, _langId)
        
        If pk > 0 Then
            soRel.Content.Key.PrimaryKey = pk
            soRel.Content.KeyRelationType.Id = 1
            soRel.Content.RelationSide = RelationTypeSide.Right
        End If
        
         so.key.IdLanguage = _langId
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaLinks
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
        so.SetMaxRow = _MAXLinks
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            If coll.Count < _MAXLinks Then
                Dim addLink As Integer = _MAXLinks - coll.Count
                Dim collTmp As ContentCollection = LoadMenu(addLink, coll)
                If Not collTmp Is Nothing AndAlso collTmp.Count > 0 Then
                    For Each itmVal As ContentValue In collTmp
                        coll.Add(itmVal)
                    Next
                End If
                Return coll
            Else
                Return coll
            End If
        Else
            Return LoadMenu()
        End If
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, _langId), 50, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = ""
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "javascript:ExternalLinkPopup" & _langCode & "('" & vo.LinkUrl & "');"
        Else
            Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idLinksUseful), Me.PageObjectGetMasterPage, False)
        End If
    End Function
    
    Function getTrackCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idLinksUseful), Me.PageObjectGetMasterPage, False)
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "recordOutboundExternalFull('Leggi Anche','Click','" & vo.Title & "','" & vo.LinkUrl & "','" & _langCode & "');return false;"
        Else
            Return "recordOutboundLink('" & targetlnk & "','Leggi Anche','Click','" & vo.Title & "','" & targetlnk & "');"
        End If
    End Function
    
</script>

 
<%--<%#iif(Container.ItemIndex = 0,"singleRelVideo noMargin","singleRelVideo") %>--%>

     <div class="rightBodyRight">
         <h4 class="linkTitle"><%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxLinks", Me.PageObjectGetLang.Id, "PLY_TitleBoxLinks")%></h4>
       <div class="linkBox">
       <asp:Literal ID="TxtListLinks" runat="server" Text=""></asp:Literal>
       </div>
         
       </div>
  

  

