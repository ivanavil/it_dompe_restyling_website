﻿<%@ Control Language="VB" ClassName="MenuSedi"  Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">
    Protected contentId As Integer = 0
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    Private _cacheKey As String = String.Empty
	
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
        _contentKey = Me.PageObjectGetContent
        contentId = Me.PageObjectGetContent.Id
        
    End Sub
    
    Protected Sub Page_Load()
        _cacheKey = "_cacheMenuSedi:" & _langKey.Id & "|" & _currentTheme.Id & "|" & _siteArea.Id & "|" & contentId
		If Request("cache") = "false" Then
            CacheManager.RemoveByKey(_cacheKey)
        End If
		Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not (_strCachedContent Is Nothing) AndAlso (_strCachedContent.Length > 0) Then
			_strMenu = _strCachedContent
		Else
			InitView()
		End If		
    End Sub
    
    Protected Sub InitView()
    
        Dim so As New ThemeSearcher()
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        _currTheme = Helper.GetTheme(so)
        LoadMenu()
                
    End Sub
    
    Protected Sub LoadMenu()
        
        Dim so As New ContentExtraSearcher
        so.KeySite.Id = Me.PageObjectGetSite.Id
        so.KeySiteArea = _siteArea
        so.key.IdLanguage = _langKey.Id
        so.CalculatePagerTotalCount = False
        so.PageSize = 1
        
        Dim baseSo As New ContentBaseSearcher()
        baseSo.key.Id = _currTheme.KeyContent.Id
        baseSo.key.IdLanguage = _langKey.Id
        Dim baseCont = Me.BusinessContentManager.GetContentBaseValue(baseSo)
        If Not baseCont Is Nothing Then
            so.KeysToExclude = baseCont.Key.PrimaryKey.ToString()
        End If
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SortOrder = ContentGenericComparer.SortOrder.ASC
        
        Dim coll As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        
        Dim sBuilder As New StringBuilder()

        If Not coll Is Nothing AndAlso coll.Any() Then
            
            Dim currcontent As New ContentIdentificator
            currcontent = Me.BusinessMasterPageManager.GetContent
            'sBuilder.AppendFormat("<div class='generic management'><p>{0}</p></div>", Me.BusinessDictionaryManager.Read("DMP_Sedi", _langKey.Id, "DMP_Sedi"))
            sBuilder.Append("<ul>")
            Dim sel As String = String.Empty
            DIm strAdd As String = String.Empty
            For Each el As ContentExtraValue In coll
				strAdd = String.Empty
				If el.Title.length <= 32 Then strAdd = " singleline"			
			
                If el.Key.Id = currcontent.Id Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If
                sBuilder.AppendFormat("<li> <a class='{0}' title='{1}' href='{2}'>{3} <span>{4}</span></a></li>", sel & strAdd, Server.HtmlEncode(el.Title), GetLink(el.Key), el.Title, String.Empty)
				'el.SubTitle
                
            Next
            sBuilder.Append("</ul>")
           
        End If
        _strMenu = sBuilder.ToString()
        sBuilder.Clear()
		
		'Cache Content and send to the oputput-------
		CacheManager.Insert(_cacheKey, _strMenu, 1, 120)
		'--------------------------------------------
    End Sub
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, _currTheme, False)
        
    End Function
    
    
</script>

<%=_strMenu %>