﻿<%@ Control Language="VB" ClassName="HomeChiSiamo" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Dompe" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _atGlanceDescription As String = String.Empty
    Protected _readMore As String = String.Empty
    Protected _txtPrimCare As String = String.Empty
    Protected _txtPipeline As String = String.Empty
    Protected _txtSolTer As String = String.Empty
    Protected _txtCareers As String = String.Empty
    Protected _txtCommitment As String = String.Empty
    
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _langKey = Me.PageObjectGetLang
        _readMore = Me.BusinessDictionaryManager.Read("DMP_readMore", _langKey.Id, "DMP_readMore")
        _txtPrimCare = Me.BusinessDictionaryManager.Read("HP_PrimCare", _langKey.Id, "HP_PrimCare")
        _txtPipeline = Me.BusinessDictionaryManager.Read("HP_Pipeline", _langKey.Id, "HP_Pipeline")
        _txtSolTer = Me.BusinessDictionaryManager.Read("HP_SolTer", _langKey.Id, "HP_SolTer")
        _txtCareers = Me.BusinessDictionaryManager.Read("HP_Careers", _langKey.Id, "HP_Careers")
        _txtCommitment = Me.BusinessDictionaryManager.Read("HP_Commitment", _langKey.Id, "HP_Commitment")
    End Sub
    
    
</script>
<section>
	<div class="box-full" style="background-image:url('/ProjectDOMPE/_slice/bg-1.jpg');">
    	<div class="cont">
    	<h1>Primary Care</h1>	
    	<p><%=_txtPrimCare%></p>
        <p><a href="http://dompeprimary.com/" target="_blank"><img src="/ProjectDOMPE/_slice/go.png" /></a></p>
        </div>
	</div>
</section>


<section class="args">
    <div class="container">
        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idPipeline, _langKey.Id)%>">
    	<div class="boxes">
        	<img src="/ProjectDOMPE/_slice/pipeline.png" />
            <h2>Pipeline</h2>
            <p><%=_txtPipeline%></p>
        </div>
            </a>
        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idSoluzioni, _langKey.Id)%>">
        <div class="boxes">
        	<img src="/ProjectDOMPE/_slice/sol-ter.png" />
            <h2>Soluzioni terapeutiche</h2>
            <p><%=_txtSolTer%></p>
        </div>
            </a>
        <a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idCareers, _langKey.Id)%>">
        <div class="boxes">
        	<img src="/ProjectDOMPE/_slice/careers.png" />
            <h2>Careers</h2>
            <p><%=_txtCareers%></p>
        </div>
            </a>
    </div>
</section>



<section>
	<div class="box-full noMargin" style="background-image:url('/ProjectDOMPE/_slice/bg-2.jpg');">
    	<div class="cont">
    	<h1>A commitment to innovation</h1>	
    	<p><%=_txtCommitment%></p>
        <p><a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idRicerca, _langKey.Id)%>"><img src="/ProjectDOMPE/_slice/go.png" /></a></p>
        </div>
	</div>
</section>

<script type="text/javascript">

    var parallaxCnt = 0

    $(document).ready(function () {

        parallaxHome();

        $(".paritem .link").hover(
           function () {
               $(this).parent().children('.parTxt').css('display', 'block');
               $(this).parent().children('.title').css('display', 'none');
           }, function () {
               $(this).parent().children('.parTxt').css('display', 'none');
               $(this).parent().children('.title').css('display', 'block');
           }
       );

    });

    function parallaxHome() {
        var cntCtrl = parallaxCnt % 4;
        if (cntCtrl == 0) {
            if (parallaxCnt == 4) {
                $(".paritem1 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem1 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else if (cntCtrl == 1) {
            if (parallaxCnt == 5) {
                $(".paritem2 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem2 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else if (cntCtrl == 2) {
            if (parallaxCnt == 6) {
                $(".paritem3 .paritems").animate({ left: 0 }, 1000, function () { });
            } else {
                $(".paritem3 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        } else {
            if (parallaxCnt == 7) {
                $(".paritem4 .paritems").animate({ left: 0 }, 1000, function () { });
                parallaxCnt = -1;
            } else {
                $(".paritem4 .paritems").animate({ left: "-=243" }, 1000, function () { });
            }
            parallaxCnt++;
        }
        var tt2 = setTimeout(function () { parallaxHome() }, 2500);
    }
</script>


