﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ import Namespace="Dompe" %>
<%@ Import Namespace="System.IO"%>
<%@ Import Namespace="System.Xml"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Xml.Linq"%>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Dim immaginiSlider As String
    Dim idPhotogallery As Integer
    Dim SiteAreaID As Integer
    Dim language As String
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        idPhotogallery = 0
        language = Request("l").ToLower
        SiteAreaID = 0
       
        idPhotogallery = Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c")), idPhotogallery)
        If Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c")), idPhotogallery) = True Then
        
            idPhotogallery = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("c"))
          
        Else
            idPhotogallery = 0
            myForm.Visible = False 
        End If
        
        
        If Int32.TryParse(Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("s")), SiteAreaID) = True Then
        
            SiteAreaID = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndael(Request("s"))
          
        Else
            SiteAreaID = 0
            myForm.Visible = False
        End If
     
      
        _siteFolder = Me.ObjectSiteFolder()
       
    End Sub
    
    Sub Page_Load()
        If idPhotogallery > 0 Then
            Dim idl As Integer = 1
            If language = "en" Then
                idl = 2
            End If
           
            Dim so As New ContentExtraSearcher
            Dim coll As ContentExtraCollection = Nothing
            so.key.Id = idPhotogallery
            so.key.IdLanguage = idl
            so.KeySiteArea.Id = SiteAreaID
            so.KeySite.Id = 46
                          
            coll = Me.BusinessContentExtraManager.Read(so)
         
            If Not coll Is Nothing AndAlso coll.Any Then
                SliderImmagini.Text = coll(0).FullTextComment
               
            End If
        End If
        
    End Sub

        
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
    <title>
            DOMPE
    </title>


    <link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
    <link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
         <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/Dompe.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/elastislide.css" />
         
         
</head>
  <body>
       <form id="myForm" runat="server" enctype="multipart/form-data">  
        <link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/DompeInternal.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/style.css" />
		<link rel="stylesheet" type="text/css" href="/ProjectDOMPE/_css/Gallery/elastislide.css" />

        <noscript>
			<style>
				.es-carousel ul{
					display:block;
				}		
			</style>
		    </noscript>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>



            <div class="container">			
			<div class="content">
				
				<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
                                <asp:Literal ID="SliderImmagini" runat="server"></asp:Literal>									
								</ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->
			
			</div><!-- content -->
		</div><!-- container -->
        <script type="text/javascript" src="/ProjectDOMPE/_js/jquery.js"></script>
    	<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--%>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/jquery.elastislide.js"></script>
		<script type="text/javascript" src="/ProjectDOMPE/_js/Gallery/gallery.js"></script>   
  </form>
   </body>

</html>