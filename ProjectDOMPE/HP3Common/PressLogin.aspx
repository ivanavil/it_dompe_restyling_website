﻿<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site" %>
<%@ import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>
<%@ import Namespace="Healthware.Dompe.Extensions" %>

<%@ Register TagPrefix="DMP" TagName="BoxRelatedContent" Src="~/ProjectDOMPE/HP3Common/BoxRelatedContent.ascx" %>

<script runat="server">

    Protected _siteFolder As String = String.Empty
    Protected _contId As Integer = 0
    Dim _lang As Integer = 1
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        
        _siteFolder = Me.ObjectSiteFolder()
        
    End Sub
    
    Protected Sub Page_Load()
        
        InitView()
    
    End Sub
    
    Protected Sub InitView()
        
        'se sono in un iframe non riesco a leggere in contentId da url        
        If Not String.IsNullOrWhiteSpace(Request.QueryString("c")) Then
            _contId = Integer.Parse(Request.QueryString("c"))
            Dim relatedContent = New BoxRelatedContent()
            relatedContent.ContentId = _contId
            div_RelatedContent.Visible = True
            ph_RelatedContent.Controls.Add(relatedContent)
            ph_RelatedContent.Visible = True
        End If
        
    End Sub
    
    Public Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If

        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = Me.LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
</script>
<link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/ui/jquery-ui.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/json2.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jHp3LabelInside.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/superfish.js.axd"></script>
	<link rel="stylesheet" href="/<%=_siteFolder %>/_js/css/jquery-ui.css.axd" />
	<link href="/<%=_siteFolder %>/_css/it/font.css.axd" rel="stylesheet" type="text/css" />
	<link href="/<%=_siteFolder %>/_css/style.css.axd" rel="stylesheet" type="text/css" />
	<link href="/<%=_siteFolder %>/_css/it/style.css.axd" rel="stylesheet" type="text/css" />
	<asp:Literal ID="MySiteareaCss" runat="server"></asp:Literal> 
	<script language="JavaScript" type="text/JavaScript" src="/Js/shadowbox/shadowbox.js.axd"></script>
    <script language="JavaScript" type="text/JavaScript" src="/Js/global.js.axd"></script>
					 <%-- Doc Check LOGIN STAGING 2000000004780--%>
					 <%-- Doc Check LOGIN PROD 2000000004778--%>
<form id="form_doccheckPress" name="form_doccheckPress" target="_parent" action="https://login.doccheck.com/" method="post">
     <input type="hidden" id="dc_key" name="keynummer" value="2000000004778" /> 
				   <input type="hidden" value="it" name="strDcLanguage" />
				   <input type="hidden" value="196" name="intDcLanguageId" />  
    <div class="colRight">
        <p><%= Me.BusinessDictionaryManager.Read("DMP_IntroGiornal", Me.BusinessMasterPageManager.GetLanguage.Id, "DMP_IntroGiornal")%></p>
        <div class="rowForm">
            <span>Username</span>
            <input type="text" name="name" id="name">
        </div>
        <div class="rowForm">
            <span>Password</span>
            <input type="password" name="password" id="password">
        </div>
        <div class="rowForm">
            <a href="javascript:void(0)" class="btn" id="btnPress" onclick="ValidationCheck();"><%= Me.BusinessDictionaryManager.Read("Accedi", Me.BusinessMasterPageManager.GetLanguage.Id)%></a>            
        </div>
        <p><%= Me.BusinessDictionaryManager.Read("DMP_LinkDocCheck", Me.BusinessMasterPageManager.GetLanguage.Id, "DMP_LinkDocCheck")%></p>
    </div>
    <div class="colLeft" style="position: absolute; top: 0px;">
        <div class="boxRedDett bigBoxRedDett">
            <div class="listSide" id="div_RelatedContent" runat="server" visible="false">                	   
                <asp:PlaceHolder runat="server" ID="ph_RelatedContent" Visible="false"></asp:PlaceHolder>               
            </div>
        </div><!--end boxRedDett-->
        <div class="imgSideRed">&nbsp;</div>
    </div>
</form>

<script type="text/javascript" language="javascript">

    $("#password").keydown(function (e) {
        var key = e.which;
        if (key == 13 && $(this).val().trim().length > 2) { submitDocCheck(); }
    });
    $("#name").keydown(function (e) {
        var key = e.which;
        if (key == 13 && $(this).val().trim().length > 2) { submitDocCheck(); }
    });


    function submitDocCheck() {
        //alert($('#form_doccheckPress').attr("id"));
        document.form_doccheckPress.submit();
    };

    function changeAccount() {
        var itemval = $('#selAccounts').val();
        if (itemval == '') {
            $('#name').val('');
            $('#password').val('');
        }
        else {
            $('#password').val(itemval);
            $('#name').val($('#selAccounts option:selected').text());
        }
    };

    function test2login(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valore;
        } else {
            var pwd = $("#password").val();
            if (pwd != "") {
                $("#password").attr("class", "dspVisible");
                $("#TxtPwdLabel").attr("class", "dspNone");
            }
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValue(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).value = "";
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function test2Pwd(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            $("#TxtPwdLabel").attr("class", "dspVisible");
            $("#password").attr("class", "dspNone");
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValuePwd(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).setAttribute('class', 'dspNone');
            $("#password").attr("class", "dspVisible");
            $("#password").val("");
            $("#password").focus();
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }


    function ValidationCheck() {
        //alert('ValidationCheck');
        var user = $("#name").val();
        var pass = $("#password").val();
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
        var esito = 1;
        if (user == "" || user == "UserID *") {
            esito = 0;
            $("#ErrorUser").attr("style", "display:block");
        }
        if (pass == "") {
            esito = 0;
            $("#ErrorPwd").attr("style", "display:block");
        }
        if (esito > 0) {
            //alert('submitDocCheck');
            submitDocCheck();
        } else {
            alert('Dati non validi');
        }
        return false;
    }


    function OpenRecoveryPwd() {
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:block");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function CloseRecoveryPwd() {
        $("#Divlogin").attr("style", "display:block");
        $("#boxForgot").attr("style", "display:none");
        $("#MessageOK").attr("style", "display:none");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");

    }
    function test2(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valore;
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValue(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).value = "";
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }

    function RecoveryOk() {
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:none");
        $("#MessageOK").attr("style", "display:block");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function RecoveryError() {
        $("#MessageOK").attr("style", "display: none");
        $("#ErrorForgot").attr("style", "display: block");
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:none");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }


     </script>

<script type="text/javascript">

    $(document).scroll(function () {

        if ($(window).scrollTop() > 431) {

            $('.colLeft').css("position", "fixed");
            $('.colLeft').css("top", "115px");
        } else {
            $('.colLeft').css("position", "absolute");
            $('.colLeft').css("top", "546px");
        }
    });

    $(document).ready(function () {
        $('.listSide ul a').click(function () {
            var currurl = $(this).attr("href");
            parent.location = currurl;
            return false;
         });
    });

    $(document).ready(function () {
        $('.colRight p a').click(function () {
            var currurl = $(this).attr("href");
            parent.location = currurl;
            return false;
        });
    });


</script>

