﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _contID As Integer = 0
    Protected _literalNews As StringBuilder = New StringBuilder
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        Dim collectNews As ContentCollection = Nothing
        _contID = Me.PageObjectGetContent.Id
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("BoxPressDetailNews_" & _lang & "_" & _contID)
        If useCache Then
            TxtListNews.Text = CacheManager.Read("BoxPressDetailNews_" & _lang & "_" & _contID, 1).ToString
        Else
            
            
                collectNews = LoadMenu(_contID)
           
            
                If Not collectNews Is Nothing AndAlso collectNews.Count > 0 Then
                    Dim i As Integer = 0
                    For Each cntVal As ContentValue In collectNews
                        Dim classext As String = IIf(i Mod 2 = 0, "rightBodyLeft", "rightBodyRight")
                    _literalNews.Append("<div class=""" & classext & """><div class=""cnt""><a title=""" & cntVal.Title & """ class=""effectHover"" href=""" & getlinkCont(cntVal) & """>" & cntVal.Title & "</a><a title=""" & cntVal.Title & """ href=""" & getlinkCont(cntVal) & """>" & GetImage(cntVal.Key.Id, cntVal.Title) & "</a><p class=""date"">" & Format(cntVal.DateInsert.Value, "short date").Replace("/", ".") & "</p><h3><a title=""" & cntVal.Title & """ href=""" & getlinkCont(cntVal) & """>" & cntVal.Title & "</a></h3><p>" & IIf(cntVal.Launch.Length > 120, Left(cntVal.Launch, 120) & "...", cntVal.Launch) & "</p> <p class=""readMore""><a href=""" & getlinkCont(cntVal) & """ title=""" & Me.BusinessDictionaryManager.Read("DMP_goto", _lang, "DMP_goto") & """>" & Me.BusinessDictionaryManager.Read("DMP_goto", _lang, "DMP_goto") & "</a></p></div></div>")
                        i = i + 1
                    Next
                End If
                TxtListNews.Text = _literalNews.ToString
            CacheManager.Insert("BoxPressDetailNews_" & _lang & "_" & _contID, _literalNews.ToString, 1, 60)
            End If
           
        
    End Sub
    
    Function LoadMenu(Optional ByVal idC As Integer = 0) As ContentCollection
        Dim iDPkTheme As Integer = 0
        Dim idContentTheme As Integer = 0
       
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNewsMediaPress))
        idContentTheme = valthem.KeyContent.Id
        iDPkTheme = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(idContentTheme, _lang)
 
        
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        Dim pk As Integer = Me.BusinessContentManager.ReadPrimaryKey(idC, _lang)
        
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaNewsMediaPress
        so.key.IdLanguage = _lang
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SetMaxRow = 1
     
        If pk > 0 Then
            so.KeysToExclude = pk & "," & iDPkTheme
        Else
            so.KeysToExclude = iDPkTheme
        End If
                           
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        End If
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, _lang), 185, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
      
        Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idNewsMediaPress), Me.PageObjectGetMasterPage, False)
        
    End Function
    
   
    
        
</script>

        <h4 class="newsTitle"><%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxNewsPress", Me.PageObjectGetLang.Id, "PLY_TitleBoxNewsPress")%></h4>	

        <div class="rightBodyRow">
        <asp:Literal ID="TxtListNews" runat="server" Text=""></asp:Literal>
        </div>                         
       
  
    
  

