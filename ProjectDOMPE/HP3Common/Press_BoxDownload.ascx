﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace=" Healthware.HP3.Core.Utility" %>
<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
    End Sub
    
    Sub Page_Load()
       
        DownloadableList()
    End Sub
    
     Sub DownloadableList()
        Dim dwncoll As New DownloadCollection
        Dim dwnsearch As New DownloadSearcher
        dwnsearch.SetMaxRow = 3
        dwnsearch.key.IdLanguage = Me.PageObjectGetLang.Id
        dwnsearch.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadAreaMediaPress
        dwnsearch.SortType = ContentGenericComparer.SortType.ByData
        dwnsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
        
        
        dwncoll = Me.BusinessDownloadManager.Read(dwnsearch)
        
        If Not dwncoll Is Nothing AndAlso dwncoll.Count > 0 Then
            rptDwlPdf.DataSource = dwncoll
            rptDwlPdf.DataBind()
        End If
        
        
    End Sub
    
   
      
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
    
    Sub TakeDownload(ByVal s As Object, ByVal e As EventArgs) 'EventArgs)
        'File Format
        Dim oDownloadType As Integer = s.commandargument.ToString.Split(";")(0)
        'Content
     
        Me.BusinessDownloadManager.Cache = False
        Me.BusinessContentManager.Cache = False
        
        Dim oContentKey As Integer = s.commandargument.ToString.Split(";")(1)
      
        Dim oDownloadValue As DownloadValue = Me.BusinessDownloadManager.Read(New ContentIdentificator(oContentKey), New DownloadTypeIdentificator(oDownloadType))
        If Not oDownloadValue Is Nothing Then
            Dim oContentValue As ContentValue = Me.BusinessContentManager.Read(oContentKey)
            If Not oDownloadValue Is Nothing Then
                Me.BusinessContentManager.Update(oContentValue.Key, "Qty", (oContentValue.Qty + 1).ToString())
                'Me.BusinessMasterPageManager.Trace(oContentValue.Key.Id, 5, oDownloadValue.DownloadTypeCollection(0).FileName.Substring(oDownloadValue.DownloadTypeCollection(0).FileName.IndexOf(".") + 1))
                DownloadManager.TakeDownload(oDownloadValue, oDownloadValue.Title)
                   
            End If
        End If
    End Sub
   
 
       
</script>

 <div class="rightBodyLeft">
       <asp:Repeater ID="rptDwlPdf" runat="server">
                <HeaderTemplate>
                <h4 class="relMatTitle"><%= Me.BusinessDictionaryManager.Read("Press_Title_Download", Me.PageObjectGetLang.Id, "Press_Title_Download")%></h4>
                </HeaderTemplate>
                <ItemTemplate>
               <div class="relMatRow">
                <h3><%# CType(Container.DataItem, DownloadValue).Title%></h3>
              
                <a href="/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, DownloadValue).Key.PrimaryKey)%>" onclick="recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=<%#  Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(CType(Container.DataItem, DownloadValue).Key.PrimaryKey)%>','Press Download Materiali correlati','Download','<%# CType(Container.DataItem, DownloadValue).Title%>','<%# CType(Container.DataItem, DownloadValue).Key.PrimaryKey %>');return false;">
                <span class="title"><%# CType(Container.DataItem, DownloadValue).Title%></span>
                <span><%# CType(Container.DataItem, DownloadValue).Launch%> </span>                   
                <span class="readMore"><%= Me.BusinessDictionaryManager.Read("Download_File", Me.PageObjectGetLang.Id, "Download_File")%></span>
                </a>
                 </div>
                </ItemTemplate>
                <FooterTemplate>
               
                </FooterTemplate>
         </asp:Repeater>
        </div>                      
       
  
    
  

