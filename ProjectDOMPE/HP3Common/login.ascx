﻿<%@ Control Language="VB" ClassName="login" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Exceptions" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>
<%@ Register TagPrefix="HP3" TagName="Captcha" Src="~/HP3Common/ctlCaptcha.ascx" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>


<script runat="server">
    Protected _langKey As LanguageIdentificator = Nothing
    Private captchaCode As String
    Private systemUtility As New SystemUtility()
    Private uid As Integer = 0
    Private linkRedirect As String = ""
    Private _siteAreaID As Integer = 0
    Dim AraeaGiornalista As String = ""
    Protected Enum LoginError As Integer
        UserNotFound = -1
        WrongPassword = -2
    End Enum
    
    Private Enum OperationMail As Integer
        Recovery = 1
        Activation = 2
    End Enum
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _langKey = Me.BusinessMasterPageManager.GetLanguage
'Or Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idMedicalHome
        If Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista  Then
            AraeaGiornalista = "AG"
        Else
            AraeaGiornalista = String.Empty
        End If
        _siteAreaID = Me.PageObjectGetSiteArea.Id
        
    End Sub
    
    Sub Page_Load()
      
        '  TxtRegCode.Text = Me.BusinessDictionaryManager.Read("PLY_RegCode", _langKey.Id, "PLY_RegCode")
        'linkRedirect = Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
      
        'If Me.BusinessAccessService.IsAuthenticated Then ' se l'utente è già autenticato, rimando alla HOMEPage
        '    Response.Redirect(Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home), False)
        'Else ' se l'utente non è autenticato
            
        '    If Not Page.IsPostBack Then
        '        SetMessageErrors()
        '    End If
            
        'End If
        reqLog.ErrorMessage = Me.BusinessDictionaryManager.Read("RequiredUser", _langKey.Id, "RequiredUser")
        reqPassword.ErrorMessage = Me.BusinessDictionaryManager.Read("RequiredPassword", _langKey.Id, "RequiredPassword")
    End Sub
    
    Sub TraceEvent(ByVal idEvent As Integer, Optional ByVal idUser As Integer = 0)
        Dim traceVal As New TraceValue
        traceVal.KeySite.Id = 46
        traceVal.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        traceVal.KeyContentType.Id = Me.PageObjectGetContentType.Id
        traceVal.KeyEvent.Id = idEvent
        traceVal.KeyUser.Id = idUser
        traceVal.KeyLanguage.Id = Me.PageObjectGetLang.Id
        traceVal.DateInsert = System.DateTime.Now
        traceVal.IdSession = Session.SessionID  'da fare vedere se è possibile mantenerlo
        Me.BusinessTraceService.Create(traceVal)
    End Sub
    
    Sub DoLogin(ByVal s As Object, ByVal e As EventArgs)
        Try
            'Response.Write("DOLOGIN")      
            If Not Me.BusinessAccessService.IsAuthenticated Then
              
                Dim ticket As TicketValue = Me.BusinessAccessService.Login(tbLogin.Text, tbPassword.Text)
                
                'controllo eventuali errori del ticket
                If ticket Is Nothing Then
                    Return
                End If
                
                'controllo eventuali errori di Login
                If Not ticket Is Nothing AndAlso ticket.ErrorOccurred.Number <> 0 Then
                    Select Case ticket.ErrorOccurred.Number
                        Case LoginError.UserNotFound
                            messgError.Text = GetLabel("LoginError_Generic", "User not found, the username or the password specified is incorrect.")
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);recordOutboundFull('Login','Error','User not found');})", True)
                           Return
                            
                        Case LoginError.WrongPassword
                            messgError.Text = GetLabel("LoginError_Generic", "User not found, the username or the password specified is incorrect.")
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);recordOutboundFull('Login','Error','User not found');})", True)
                            Return
                    End Select
                End If
                
                If (Not ticket Is Nothing) AndAlso ticket.ErrorOccurred.Number = 0 Then
                    
                    TraceEvent(2, ticket.User.Key.Id)
                    Dim genUrl As String = Me.BusinessMasterPageManager.GetLink(PageObjectGetTheme, Nothing, False)
					
                    If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(78)) Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('/','Login','OK','');return false;})", True)
                        Return
                    End If

                    If Me.BusinessAccessService.HasUserRole(New RoleIdentificator(65)) Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.partnerWelcome, 2) & "','Login','OK','');return false;})", True)
                        Return
                    Else

                        If Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Then
                            Dim id_ruolo_utente As Array
                            Dim id_r As Integer
				  
                            id_ruolo_utente = Split("," & Me.ObjectTicket.Roles.ToString & ",", ",")
                            If id_ruolo_utente.Length() - 1 > 0 Then
						
                                For id_r = 0 To id_ruolo_utente.Length() - 1
							  
                                    'Controllo se l'utente loggato è un giornalista
                                    If id_ruolo_utente(id_r).ToString = Dompe.Role.RoleMediaPress Then
                                        'Link Alla sezione Dashboard
                                        Dim urlred As String = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langKey.Id)
                                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & urlred & "','Login','OK','Media Press');return false;})", True)
                                        ' Response.Redirect(Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard))
                                        Exit For
                                    End If
						  
                                Next
                            Else
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & genUrl & "','Login','OK','no role press');return false;})", True)
                                'Response.Redirect(Me.BusinessMasterPageManager.GetLink(PageObjectGetTheme, Nothing, False), False)
                            End If
					
                        ElseIf Me.BusinessMasterPageManager.GetTheme.Id = Dompe.ThemeConstants.idMedicalHome Then
                            Dim id_ruolo_utente As Array
                            Dim id_r As Integer
				  
                            id_ruolo_utente = Split("," & ticket.Roles.ToString & ",", ",")
                            If id_ruolo_utente.Length() - 1 > 0 Then
						
                                For id_r = 0 To id_ruolo_utente.Length() - 1
                                    'Controllo se l'utente loggato è un medical
                                    If id_ruolo_utente(id_r).ToString = Dompe.Role.RoleDoctor Then
                                        'Link Alla sezione Dashboard
                                        Dim urlred As String = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langKey.Id)
                                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & urlred & "','Login','OK','role doctor');return false;})", True)
                                        '  Response.Redirect(Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome))
                                        Exit For
                                    End If
                                    'Controllo se l'utente loggato è un giornalista
													
                                Next
                            Else
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & genUrl & "','Login','OK','no role doctor');return false;})", True)
                                '  Response.Redirect(Me.BusinessMasterPageManager.GetLink(PageObjectGetTheme, Nothing, False), False)
                            End If
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & genUrl & "','Login','OK','');return false;})", True)
                            ' Response.Redirect(Me.BusinessMasterPageManager.GetLink(PageObjectGetTheme, Nothing, False), False)
                        End If
                  
                    End If
                    Return
   
                End If
                
                
            End If 'Else gestito nel Page_Load()
         
        Catch ex As Exception
            Throw New BusinessException("Dompe Login Error: " & ex.ToString())
        End Try
        
    End Sub
    
    Sub DoReset(ByVal s As Object, ByVal e As EventArgs)
        Reset()
    End Sub
    
   
    Function ReadUserByEmail(ByVal email As String) As UserValue
        Dim so As New UserSearcher
        ' so.Properties.Add("Email.GetString")
        so.SetMaxRow = 1
        so.Status = 1 ' Attivo
        so.Email = email
        so.RegistrationSite.Id = 46
        ' so.Username = email
        
        Me.BusinessUserManager.Cache = False
        Dim coll As UserCollection = Me.BusinessUserManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            Return coll(0)
        End If
        
        Return Nothing
    End Function
      
    
    Sub Reset()
        tbLogin.Text = String.Empty
        tbPassword.Text = String.Empty
        
        messgError.Text = String.Empty
    End Sub
    
    Sub SetMessageErrors()
        Dim reqfield As String = GetLabel("RequiredF", "Required field")
        ' regexEmail.Text = GetLabel("regexEmail", "The email is not well formed.")
        
        reqLog.Text = reqfield
        reqPassword.Text = reqfield
    End Sub
    
    'costruisce il link
    'Function GetLinkForgot() As String
    '    Return Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.ForgotPassword)
    'End Function
    
    'costruisce il link passando domain e name del thema
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = _langKey
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, _langKey.Id, defaultLabel)
    End Function
    
       
   
    Function getLinkMail(ByVal idcnt As String, ByVal decisione As Integer) As String
        Dim res As String = ""
        Dim uid As String = ""
        Dim webRequest As New WebRequestValue
     
        'webRequest.KeysiteArea.Id = LocalConstants_BMSTS.AcceptSiteAreaID
        'webRequest.KeycontentType.Id = LocalConstants_BMSTS.AcceptContenTypeID
        webRequest.Keycontent.Id = 0
        webRequest.KeyEvent = New TraceEventIdentificator("HP3", "entry")
        webRequest.KeyLang = Me.PageObjectGetLang
        'webRequest.KeyTheme.Id = LocalConstants_BMSTS.AcceptThemeId
               
        res = Me.BusinessMasterPageManager.FormatRequest(webRequest) & "?usr=" & uid & "&op=" & decisione
                     
        Return res
    End Function
    
    Function ExistEmail(ByVal email_address As String) As Boolean
        Dim hlpman As New Helper
        Dim idRet As Integer
        idRet = hlpman.existUserByEmail(email_address.ToLower.Trim)
        If idRet > 0 Then
            Return True
        ElseIf idRet = 0 Then
            Return False
        ElseIf idRet = -1 Then
            Return True
        End If
    End Function
    
    'Sub UserOptions()
    '    Dim kk As Integer
       
    '    For kk = 0 To 3
    '        Dim lstI As New ListItem
    '        lstI.Text = Me.BusinessDictionaryManager.Read("PLY_optionKnow_" & kk, _langKey.Id, "PLY_optionKnow_" & kk)
    '        lstI.Value = kk
    '        ddluser.Items.Add(lstI)
    '    Next kk
    '    ddluser.DataBind()
        
    'End Sub
    
    Sub ForgotPassword(ByVal s As Object, ByVal e As EventArgs)
        
        
        If (Page.IsValid()) Then
          
            
            Dim oUser As UserValue = ReadUserByEmail(TxtForgotMail.Text.Trim())
          
            If (Not oUser Is Nothing AndAlso oUser.Key.Id > 0) AndAlso oUser.Status = 1 Then ' UTENTE ATTIVO
                Dim ck As Boolean = checkEmail(oUser.Email.GetString)
                Dim userS As String = IIf(oUser Is Nothing, "0", oUser.Key.Id)
                If ck Then
                    Dim conf As Boolean = RecoveryPassword(oUser)
                    If conf Then
                        'Reset()
                        'DisableDivForm()
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryOk();})", True)
                        Return
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryError();})", True)
                        Return
                    End If
                    ' messg.Text = GetLabel("Recovery_Error1", "Sorry, no match has been found with the inserted mail. Thank you.")
                End If
            Else ' UTENTE NON ATTIVO
                Dim userStat As String = IIf(IsNothing(oUser), "0", "-1")
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){loginBox(1);RecoveryError();})", True)
                Return
            End If
                      
            ' messgError.Text = GetLabel("Recovery_Error1", "Sorry, no match has been found with the inserted email. Thank you.")
            Return
        Else
            Response.Redirect("http://" & Request.Url.Host)
        End If
        
    End Sub
    
    Function RecoveryPassword(ByVal oUser As UserValue) As Boolean
        Dim oMailTemplateValue As MailTemplateValue = Nothing
        Dim oUserValue As UserValue = Nothing
        
        If Not oUser Is Nothing AndAlso oUser.Key.Id > 0 Then
            oMailTemplateValue = getMailTemplate("UserRecoveryPassword")
                        
            'gestisco email di recovery password
            Dim res As Boolean = SendMail(oMailTemplateValue, oUser, OperationMail.Recovery)
            Return res
        End If
        
        Return False
    End Function
    
    Function SendMail(ByVal oMailTemaplate As MailTemplateValue, ByVal oUser As UserValue, ByVal operation As Integer) As Boolean
        Try
            Dim mailValue As New MailValue
            
            If (Not oMailTemaplate Is Nothing) And (Not oUser Is Nothing) Then
               
                mailValue.MailTo.Add(New MailAddress(oUser.Email.GetString))
                mailValue.MailFrom = New MailAddress(oMailTemaplate.SenderAddress, oMailTemaplate.SenderName)
            
                mailValue.MailBody = ReplaceBodyInfo(oMailTemaplate.Body, oUser, operation)
                mailValue.MailSubject = oMailTemaplate.Subject
                mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
               
                If (oMailTemaplate.BccRecipients <> String.Empty) Then
                    mailValue.MailBcc.Add(New MailAddress(oMailTemaplate.BccRecipients))
                End If
                    
                If (oMailTemaplate.FormatType = 1) Then
                    mailValue.MailIsBodyHtml = True
                Else
                    mailValue.MailIsBodyHtml = False
                End If
               
                'invio della mail
                Dim systemUtility As New SystemUtility()
                systemUtility.SendMail(mailValue)
              
                Return True
            End If
                 
              
        Catch ex As Exception
            Return False
            Throw New BaseException("DOMPE - Recovery Password Error : ", ex)
        End Try
              
        Return False
    End Function
   
    Function ReplaceBodyInfo(ByVal body As String, ByVal userValue As UserValue, ByVal operation As Integer) As String
        Dim replecedBody As String = String.Empty
        
        Select Case operation
            Case OperationMail.Recovery
       
                If Not userValue Is Nothing Then
                    Dim completeName As String = userValue.CompleteName
            
                    If Not String.IsNullOrEmpty(completeName) Then
                        replecedBody = body.Replace("[USER]", completeName)
                    Else
                        replecedBody = body.Replace("[USER]", "User")
                    End If
            
                    Return replecedBody.Replace("[PASSWORD]", userValue.Password).Replace("[USERID]", userValue.Username)
         
                End If
            Case OperationMail.Activation
                                
                'If Not userValue Is Nothing Then
                '    replecedBody = body.Replace("[LINK]", getActivationLink(userValue))
                'End If
        
                Return replecedBody
                
        End Select
                
        Return replecedBody
    End Function
    Function getMailTemplate(ByVal strMailtemplate As String) As MailTemplateValue
        Dim nameTemplate As String = ""
     
        nameTemplate = strMailtemplate
        
        Dim omailTemplateValue As New MailTemplateValue
        Dim omailTemplateColl As New MailTemplateCollection
        Dim omailTemplateSearcher As New MailTemplateSearcher
        Dim omailTemplateManager As New MailTemplateManager
        
        omailTemplateSearcher.Key.KeyLanguage.Id = _langKey.Id
        'omailTemplateSearcher.KeySite.Id = idsite
        omailTemplateSearcher.Key.MailTemplateName = nameTemplate
        omailTemplateColl = Me.BusinessMailTemplateManager.Read(omailTemplateSearcher)
        If Not omailTemplateColl Is Nothing AndAlso omailTemplateColl.Count > 0 Then
            Return omailTemplateColl(0)
        End If
        Return Nothing
    End Function
    
    Function checkEmail(ByVal userEmail As String) As Boolean
       
        If Not String.IsNullOrEmpty(userEmail) Then
            
            If (userEmail = TxtForgotMail.Text) Then
                Return True
            End If
        End If
        
        Return False
    End Function
</script>

<style type="text/css">
		.black_overlay{
			display:none;
			position: absolute;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			background-color:black;
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.80;
			filter: alpha(opacity=80);
		}
		.white_content {
			display:none;
			position: absolute;
			top: 25%;
			left: 35%;
			width: 35%;
			
			padding: 0px;
			border: 0px solid #a6c25c;
			background-color: white;
			z-index:1002;
			overflow: auto;
		}
		.headertext{
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#f19a19;
font-weight:bold;
}
.textfield
{
	border:1px solid #a6c25c;
	width:120px; 
}

</style>
<script type = "text/javascript">
    function alertPopLogin() {
        alert('<%= Me.BusinessDictionaryManager.Read("LoginError", _langKey.Id, "LoginError")%>');
    }


          function cancellaValue(nomeControllo, valore) {


              if (document.getElementById(nomeControllo).value == valore) {
                  document.getElementById(nomeControllo).value = "";
            }
        }

        function test2login(nomeControllo, valore) {

            if (document.getElementById(nomeControllo).value == "") {
                document.getElementById(nomeControllo).value = valore;
            } else {
                var pwd = $("#<%= tbPassword.ClientID %>").val();
                if (pwd != "") {
                    $("#<%= tbPassword.ClientID %>").attr("class", "dspVisible");
                    $("#<%= TextPwdLabel.ClientID %>").attr("class", "dspNone");
                }
            }
        }



        function test2(nomeControllo, valore) {

            if (document.getElementById(nomeControllo).value == "") {
                document.getElementById(nomeControllo).value = valore;
            }
        }

        function cancellaValuePwd(nomeControllo, valore) {


            if (document.getElementById(nomeControllo).value == valore) {
               document.getElementById(nomeControllo).setAttribute('class', 'dspNone');
               $("#<%= tbPassword.ClientID %>").attr("class", "dspVisible");
               $("#<%= tbPassword.ClientID %>").val("");
              $("#<%= tbPassword.ClientID %>").focus();
            }
        }


        function test2Pwd(nomeControllo, valore) {

            if (document.getElementById(nomeControllo).value == "") {
                $("#<%= TextPwdLabel.ClientID %>").attr("class", "dspVisible");
                $("#<%= tbPassword.ClientID %>").attr("class", "dspNone");
            }
        }

    
       function checkValidatorUser() {
           var usertype = $("#typeLogin").val();
        if (usertype == "-1") {
            $("#ErrorUser").attr("style", "display:block");
            return false;
            
        } else {
        $("#ErrorUser").attr("style", "display:none");
            return true;

        }
      }

      function OpenRecoveryPwd() {
          $("#DivDropUser").attr("style", "display:none");
          $("#boxLogStandard").attr("style", "display:none");
          $("#<%= divForgotPsw.ClientId %>").attr("style", "display:block");

      }
      function CloseRecoveryPwd() {
          $("#MessageOK").attr("style", "display: none");
          $("#ErrorForgot").attr("style", "display: none");
          $("#DivDropUser").attr("style", "display:block");
          $("#boxLogStandard").attr("style", "display:block");
          $("#<%= divForgotPsw.ClientId %>").attr("style", "display:none");

      }

      function RecoveryOk() {
          $("#DivDropUser").attr("style", "display:none");
          $("#boxLogStandard").attr("style", "display:none");
          $("#<%= divForgotPsw.ClientId %>").attr("style", "display:none");
          $("#MessageOK").attr("style", "display: block");
          $("#ErrorForgot").attr("style", "display: none");
      }
      function RecoveryError() {
          $("#MessageOK").attr("style", "display: none");
          $("#ErrorForgot").attr("style", "display: block");
          $("#DivDropUser").attr("style", "display:none");
          $("#boxLogStandard").attr("style", "display:none");
          $("#<%= divForgotPsw.ClientId %>").attr("style", "display:block");
      }

      function submitDocCheck(ff) {
          if (ff == '0')
              document.myForm.submit();
          else
              document.myForm.submit();

          return false;
      };

      function changeAccount() {
          var itemval = $('#selAccounts').val();
          if (itemval == '') {
              $('#name').val('');
              $('#password').val('');
          }
          else {
              $('#password').val(itemval);
              $('#name').val($('#selAccounts option:selected').text());
          }
      };
              
   </script>

  
<style type="text/css">
    .dspNone
    {
        display:none;
    }
     .dspVisible
    {
        display: block;
    }
    .tooltipError
    {
        position:absolute;
        z-index : 9999;
        top: 90px;
        left : 280px;
        width: 200px;
        height:24px;
        background-image: url('/ProjectDOMPE/_slice/tooltip.png');
        background-repeat:no-repeat;
        color: #C30505 !important;;
        text-align:center;
        padding:0;
        line-height:22px;
    }
    
     .tooltipErrorUser
    {
        position:absolute;
        z-index : 9999;
        top: 65px;
        line-height:22px;
        left : 280px;
        width: 200px;
        height:24px;
        background-image: url('/ProjectDOMPE/_slice/tooltip.png');
        background-repeat:no-repeat;
        color: #C30505 !important;
        text-align:center;
        padding: 0;
    }
    
    .tooltipErrorTypeAccess
    {
        position:absolute;
        z-index : 9999;
        top: 36px;
        line-height:22px;
        left : 280px;
        width: 200px;
        height:24px;
        background-image: url('/ProjectDOMPE/_slice/tooltip.png');
        background-repeat:no-repeat;
        color: #C30505;
        text-align:center;
        padding: 0;
        }
 </style>
 
<div class="loginBox<%=AraeaGiornalista %>">
       	<div class="container">
       	   <div class="loginBoxCnt">
          
           <div class="loginBoxLeft">
            <p class="title"><%= GetLabel("DMP_Login", "DMP_Login")%></p>
        
               <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnSend">
                <p><a href="javascript:void(0);" title="Tipo di accesso" class="selecType" onclick="javascript:selectType();"><%= GetLabel("DMP_TypeAccess", "DMP_TypeAccess")%></a></p>
               <input type="hidden" id="typeLogin" value="-1" />
               <div class="typeList">
                    	<ul>
						
						<%--
						
                        	<li><a id="type2" href="javascript:void(0);" onclick="javascript:valType(2);" title="<%= Me.BusinessDictionaryManager.Read("DMP_User2", _langKey.Id, "DMP_User2")%>"><%= Me.BusinessDictionaryManager.Read("DMP_User2", _langKey.Id, "DMP_User2")%></a></li>
							
						--%>	
							
							<li><a id="type3" href="javascript:void(0);" onclick="javascript:valType(3);" title="<%= Me.BusinessDictionaryManager.Read("DMP_User3", _langKey.Id, "DMP_User3")%>"><%= Me.BusinessDictionaryManager.Read("DMP_User3", _langKey.Id, "DMP_User3")%></a></li>
							
							
							
							<li><a id="type1" href="javascript:void(0);" onclick="javascript:valType(1);" title="<%= Me.BusinessDictionaryManager.Read("DMP_User1", _langKey.Id, "DMP_User1")%>"><%= Me.BusinessDictionaryManager.Read("DMP_User1", _langKey.Id, "DMP_User1")%></a></li>
							
							
                        	<% If Not String.IsNullOrEmpty(Request("skipdc")) AndAlso request("skipdc") = "skip" Then %>
								<li><a id="type4" href="javascript:void(0);" onclick="javascript:valType(4);" title="Utente di sede">Utente di Sede</a></li>
							<% End If %>
							</ul>
                </div>
                 <div id="ErrorUser" class="tooltipErrorTypeAccess" style="display:none"> <%= Me.BusinessDictionaryManager.Read("DMP_ErrorSelectUser", _langKey.Id, "DMP_ErrorSelectUser")%></div>
               	<div id="boxLogDocCheck" style=" display:none;" > 
                <iframe id="FrameDocCheck" width="465" height="125" frameborder="0" scrolling="no" src="/ProjectDOMPE/HP3Common/LoginDocCheck.aspx" style="margin:0px; padding:0px; border:0px;"></iframe>
                </div>
                    <div id="boxLogStandard" style=" display: block;">
                       <p>
                    	<asp:TextBox ID="tbLogin" Text="UserID *" onfocus ="javascript:cancellaValue(this.id,'UserID *');" onblur="javascript:test2login(this.id,'UserID *');" runat="server" EnableViewState ="false" ValidationGroup="logGroup" MaxLength="60" />
                        <asp:RequiredFieldValidator ID="reqLog" CssClass="tooltipErrorUser" InitialValue="UserID *" ErrorMessage="UserName Required" ControlToValidate="tbLogin" Display="dynamic"  SetFocusOnError="false" runat="server" ValidationGroup="logGroup" EnableViewState ="false" />
                      <%--  <asp:RegularExpressionValidator ID="regexEmail" ErrorMessage="Incorrect Format" runat="server" Display="dynamic" ControlToValidate="tbLogin" ValidationGroup="logGroup" EnableViewState ="false"
                         ValidationExpression="[a-z._-]+\@[a-z._-]+\.[a-z]{2,4}"/>--%>
                    </p>
                  <p>
                    	<asp:TextBox CssClass="dspNone" ID="tbPassword"  TextMode="Password" onblur="javascript:test2Pwd(this.id,'Password *');" runat="server" ValidationGroup="logGroup" EnableViewState ="false" MaxLength="40" />
                        <asp:TextBox CssClass="dspVisible" ID="TextPwdLabel" Text="Password *"  onfocus ="javascript:cancellaValuePwd(this.id,'Password *');" runat="server" ValidationGroup="logGroup" EnableViewState ="false" MaxLength="40"/>
                        <asp:RequiredFieldValidator ID="reqPassword" CssClass="tooltipError" ControlToValidate="tbPassword" Display="Dynamic" runat="server" ValidationGroup ="logGroup" EnableViewState ="false"  ErrorMessage="Password Required" />
                  </p>
                    <p class="psswDim">
              <%--         <%= GetLabel("ForgotPasswordPartner", "ForgotPasswordPartner")%> <a href="javascript:void(0)" onclick = "OpenRecoveryPwd()"><i><%= GetLabel("DMP_ClickHere", "DMP_ClickHere")%></i></a>--%>
                       <%= GetLabel("ForgotPasswordMed", "ForgotPasswordMed")%><a href="https://www.doccheck.com/it/account/password/reminder/" title="Recupera password">&nbsp;<i><%= GetLabel("DMP_ClickHere", "DMP_ClickHere")%></i></a>
                    </p>
                  	 <asp:LinkButton CssClass="btn" id="btnSend" runat="server" OnClientClick="return checkValidatorUser();" OnClick="DoLogin" UseSubmitBehavior="false" ValidationGroup ="logGroup" > <%= GetLabel("LOGIN", "LOGIN")%> </asp:LinkButton>
                  </div>
                   
                   <div id="divForgotPsw" style=" display:none;"  runat="server">
                   <asp:TextBox ID="TxtForgotMail" Text="Email *" onfocus ="javascript:cancellaValue(this.id,'Email *');" onblur="javascript:test2login(this.id,'Email *');" runat="server" EnableViewState ="false" ValidationGroup="logGroup" MaxLength="60" />
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" InitialValue="Email *" ErrorMessage="Required" ControlToValidate="TxtForgotMail" Display="dynamic"  SetFocusOnError="false" CssClass="validator" runat="server" ValidationGroup="forgotGroup" EnableViewState ="false" />
                   
                    <a href="javascript:void(0)" class="null" onclick = "CloseRecoveryPwd()"><span style=" color:White;"><i><%= GetLabel("Annulla", "Annulla")%></i></span></a>
                    <div class="regFormBtn">
                		<asp:Button ID="Button1" OnClick="ForgotPassword" runat="server" Text="Submit" class="buttonOKlogin" ValidationGroup="forgotGroup" />	
                    </div>
                    <div id="ErrorForgot" style=" display:none;"> <%= Me.BusinessDictionaryManager.Read("DMP_ForgotError", _langKey.Id, "DMP_ForgotError")%> </div>   
              
                   </div>
                       <div id="MessageOK" style=" display:none;"> 
                       <%= Me.BusinessDictionaryManager.Read("DMP_ForgotOK", _langKey.Id, "DMP_ForgotOK")%> 
                         <a href="javascript:void(0)" onclick = "CloseRecoveryPwd()"><span style=" color:White;"><i><%= GetLabel("Close", "Close")%></i></span></a>
                       </div>
                 </asp:Panel>
                  <div class="row" style="margin-left:98px;">
                       <asp:Label ID="messgError" CssClass="errorMsg" runat="server"></asp:Label></div>
                  </div>     
             <div class="loginBoxRight">
                	<p class="title">
                    <%= Me.BusinessDictionaryManager.Read("DMP_RegisterTitle", _langKey.Id, "DMP_RegisterTitle")%>
                    </p>
                    <p style="position:relative;">
                      <%= Me.BusinessDictionaryManager.Read("DMP_RegisterText", _langKey.Id, "DMP_RegisterText")%>
                    </p>
                   <a href="https://www.doccheck.com/it/account/register/" title="<%= Me.BusinessDictionaryManager.Read("DMP_RegisterHere", _langKey.Id, "DMP_RegisterHere")%>" class="btn nopopupexternal" target="_blank">
                   <%= Me.BusinessDictionaryManager.Read("DMP_RegisterHere", _langKey.Id, "DMP_RegisterHere")%>
                   </a>
                          	
            </div>
        
          
             
      </div>
        </div><!--end contForm-->
          
    </div><!--end content-->
    