﻿<%@ Control Language="VB" ClassName="PressLogin" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>

<script runat="server">

</script>
 <form id="form_doccheckPress" name="form_doccheckPress" target="_parent" action="https://login.doccheck.com/" method="post">
        <%-- Doc Check LOGIN STAGING 2000000004780--%>
        <%-- Doc Check LOGIN PROD 2000000004778--%>
     <input type="hidden" id="dc_key" name="keynummer" value="2000000004778" /> 
				   <input type="hidden" value="it" name="strDcLanguage" />
				   <input type="hidden" value="196" name="intDcLanguageId" />  
    <div class="colRight">
        <p><%= Me.BusinessDictionaryManager.Read("DMP_IntroGiornal", Me.BusinessMasterPageManager.GetLanguage.Id, "DMP_IntroGiornal")%></p>
        <div class="rowForm">
            <span>Username</span>
            <input type="text" name="name" id="name">
        </div>
        <div class="rowForm">
            <span>Password</span>
            <input type="password" name="password" id="password">
        </div>
        <div class="rowForm">
            <a href="javascript:void(0)" class="btn" id="btnPress" onclick="ValidationCheck();"><%= Me.BusinessDictionaryManager.Read("Accedi", Me.BusinessMasterPageManager.GetLanguage.Id)%></a>            
        </div>
        <p><%= Me.BusinessDictionaryManager.Read("DMP_LinkDocCheck", Me.BusinessMasterPageManager.GetLanguage.Id, "DMP_LinkDocCheck")%></p>
    </div>
    <div class="colLeft" style="position: absolute; top: 546px;">
        <div class="boxRedDett">
            &nbsp;
        </div><!--end boxRedDett-->
        <div class="imgSideRed">&nbsp;</div>
    </div>
</form>

<script type="text/javascript" language="javascript">
    function submitDocCheck() {
        //alert($('#form_doccheckPress').attr("id"));
        document.form_doccheckPress.submit();
    };

    function changeAccount() {
        var itemval = $('#selAccounts').val();
        if (itemval == '') {
            $('#name').val('');
            $('#password').val('');
        }
        else {
            $('#password').val(itemval);
            $('#name').val($('#selAccounts option:selected').text());
        }
    };

    function test2login(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valore;
        } else {
            var pwd = $("#password").val();
            if (pwd != "") {
                $("#password").attr("class", "dspVisible");
                $("#TxtPwdLabel").attr("class", "dspNone");
            }
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValue(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).value = "";
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function test2Pwd(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            $("#TxtPwdLabel").attr("class", "dspVisible");
            $("#password").attr("class", "dspNone");
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValuePwd(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).setAttribute('class', 'dspNone');
            $("#password").attr("class", "dspVisible");
            $("#password").val("");
            $("#password").focus();
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }


    function ValidationCheck() {
        alert('ValidationCheck');
        var user = $("#name").val();
        var pass = $("#password").val();
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
        var esito = 1;
        if (user == "" || user == "UserID *") {
            esito = 0;
            $("#ErrorUser").attr("style", "display:block");
        }
        if (pass == "") {
            esito = 0;
            $("#ErrorPwd").attr("style", "display:block");
        }
        if (esito > 0) {
            alert('submitDocCheck');
            submitDocCheck();
        } else {
            alert('Dati non validi');
        }
        return false;
    }


    function OpenRecoveryPwd() {
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:block");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function CloseRecoveryPwd() {
        $("#Divlogin").attr("style", "display:block");
        $("#boxForgot").attr("style", "display:none");
        $("#MessageOK").attr("style", "display:none");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");

    }
    function test2(nomeControllo, valore) {

        if (document.getElementById(nomeControllo).value == "") {
            document.getElementById(nomeControllo).value = valore;
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function cancellaValue(nomeControllo, valore) {


        if (document.getElementById(nomeControllo).value == valore) {
            document.getElementById(nomeControllo).value = "";
        }
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }

    function RecoveryOk() {
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:none");
        $("#MessageOK").attr("style", "display:block");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }
    function RecoveryError() {
        $("#MessageOK").attr("style", "display: none");
        $("#ErrorForgot").attr("style", "display: block");
        $("#Divlogin").attr("style", "display:none");
        $("#boxForgot").attr("style", "display:none");
        $("#ErrorUser").attr("style", "display:none");
        $("#ErrorPwd").attr("style", "display:none");
    }


     </script>

<script type="text/javascript">

    $(document).scroll(function () {
        
        if ($(window).scrollTop() > 431) {
            
            $('.colLeft').css("position", "fixed");
            $('.colLeft').css("top", "115px");
        } else {
            $('.colLeft').css("position", "absolute");
            $('.colLeft').css("top", "546px");
        }
    });

</script>