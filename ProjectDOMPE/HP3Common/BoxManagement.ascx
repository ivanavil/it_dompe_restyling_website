﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _IDContent As Integer = 0
	Private _cacheKey As String = String.Empty

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
        _IDContent = Me.PageObjectGetContent.Id
    End Sub
    
    Sub Page_Load()
        'If Not Page.IsPostBack Then LoadMenu()
		LoadMenu()
	End Sub
    
    Sub LoadMenu()
		_cacheKey = "cacheKey:BoxManagement" & _lang & "|" & _IDContent
		
		If Request("cache") = "false" Then CacheManager.RemoveByKey(_cacheKey)
        Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		If Not String.IsnullOrEmpty(_strCachedContent) Then 
			'Response.write("cached Box")
			ltlBoxSubMenu.Text = _strCachedContent
		Else
			'Response.write("Not cached Box")
			
			Dim _sb as StringBuilder = New StringBuilder()
			
			Dim cntcoll As New ContentExtraCollection
			Dim cntsearch As New ContentExtraSearcher
			Dim cntVal As New ContentExtraValue
            cntsearch.key.IdLanguage = _lang
            cntsearch.KeySite.Id = Me.PageObjectGetSite.Id
            cntsearch.KeySiteArea.Domain = Dompe.ThemeConstants.Domain.DMP
			cntsearch.KeySiteArea.Name = Dompe.SiteAreaConstants.Management
			cntsearch.KeyContentType.Domain = Dompe.ThemeConstants.Domain.DMP
			cntsearch.KeyContentType.Name = Dompe.ContentTypeConstants.Name.Management
			cntsearch.SortType = ContentGenericComparer.SortType.ByData
			cntsearch.SortOrder = ContentGenericComparer.SortOrder.ASC
			'cntsearch.KeysToExclude = Healthware.HP3.Core.Utility.WebUtility.MyPage(Me).PKThemeContent()
			cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
			If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
				_sb.Append("<h4 class='newsTitle'>" & Me.BusinessDictionaryManager.Read("PLY_TitleBoxManagement", _lang, "PLY_TitleBoxManagement") & "</h4>")
				_sb.Append("<div class='boxComunicati managementG'>")
				
				For Each oCExtraval as ContentExtraValue in cntColl
					_sb.Append("<div " & getClass(oCExtraval.Key.Id) & "><a class='mngm' href='" & GetLink(oCExtraval) & "' title='" & oCExtraval.Title.Replace("'", "") & "'>" & oCExtraval.Title & "</a>")
					_sb.Append("<div class='img'><a href='" & GetLink(oCExtraval) & "'><img src='/ProjectDOMPE/_slice/plus.png' alt='plus'></a></div>")
					_sb.Append("<div class='info'><p><a title='" & oCExtraval.Title.Replace("'", "") & "' href='" & GetLink(oCExtraval) & "'>" & oCExtraval.Title & "<span class='dateCom' style='font-size:14px !important;'>" & oCExtraval.SubTitle & "</span></a></p></div>")
					_sb.Append("</div>")
				Next
				_sb.Append("</div>")
			
				'rptSiteMenu.DataSource = cntcoll
				'rptSiteMenu.DataBind()
			End If
			
			CacheManager.Insert(_cacheKey, _sb.ToString(), 1, 60)
			ltlBoxSubMenu.Text = _sb.ToString
		End If
    End Sub
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, _lang), 72, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
       
    'GetLink by Theme Domain/Name
    'Function GetLink1(ByVal vo As ContentValue) As String
       
    '    Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(200), Me.PageObjectGetMasterPage, False)
    '    'End If
        
    '    Return String.Empty 'potrebbe restiture URL SiteDominio
    'End Function
    Function getClass(ByVal idCont As Integer) As String
        Dim res As String = "class=""singleCom"""
        
        If idCont = _IDContent Then
            res = "class=""singleCom singleComContent"""
        End If
        Return res
    End Function
       
</script>
<script type="text/javascript" language="javascript">
  $(document).ready(function () {
    $(".singleCom ").hover(
			function () {
			    $(this).addClass("singleComBg");
			},
			function () {
			    $(this).removeClass("singleComBg");
			}
		);
});
</script>

<div class="rightBodyRow">
	<asp:literal id="ltlBoxSubMenu" runat="server" EnableViewState="false" />
	
	
	<asp:Repeater ID="rptSiteMenu" runat="server" visible="false">
	<HeaderTemplate>
		<h4 class="newsTitle"><%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxManagement", _lang, "PLY_TitleBoxManagement")%></h4>
		<div class="boxComunicati managementG">
	</HeaderTemplate> 
	<ItemTemplate>
			<div <%# getClass(DataBinder.Eval(Container.DataItem, "Key.Id")) %>>
				<div class="img">
					<a href="<%# GetLink(Container.DataItem) %>"><img src="/ProjectDOMPE/_slice/plus.png" alt="plus"></a>
				</div>
				<div class="info">
					<p>
					<a href="<%# GetLink(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "Title")%><span class="dateCom" style=" font-size:14px !important;"><%# DataBinder.Eval(Container.DataItem, "SubTitle")%></span></a>
					<%--  <%# Left(DataBinder.Eval(Container.DataItem, "FullText"), 50)%> <%# IIf(DataBinder.Eval(Container.DataItem, "FullText").ToString.Length > 50, "...", "")%> <br />--%> 
					</p>
				</div>
			</div>
	</ItemTemplate>
	<FooterTemplate>
		</div>
	</FooterTemplate>
	</asp:Repeater>
</div>                         
    
  
    
  

