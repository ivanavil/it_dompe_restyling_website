﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _siteareaMenu As Integer = 0
    Protected _siteThememenu As Integer = 0
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
    End Sub
    
    Sub Page_Load()
        Dim contv As ContentCollection = Nothing
              
        
        If Not Page.IsPostBack Then
            ' carica il menu di sinistra
            rptSiteMenu.DataSource = LoadMenu()
            rptSiteMenu.DataBind()
           
            'If Me.PageObjectGetTheme.Id = Priligy.ThemeConstants.idVideoTheme Then
            '    LinkVideo.Text = "<ul><li class=""sel2""><span class=""imgSel2"">&nbsp;</span><a href=""/videos/0/default.html "">" & Me.BusinessDictionaryManager.Read("PLY_Video_Label_boxSX", 2, "PLY_Video_Label_boxSX") & "</li></ul></a>"
            'Else
            '    LinkVideoSection()
            'End If
        End If
      
    End Sub
    
    Function LoadMenu() As ContentExtraCollection
        Dim so As New ContentExtraSearcher
        Dim coll As ContentExtraCollection = Nothing
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        so.SortType = ContentGenericComparer.SortType.ByData
        so.SortOrder = ContentGenericComparer.SortOrder.ASC
        
        coll = Me.BusinessContentExtraManager.Read(so)
       
        Return coll
       
    End Function
    
    Sub LinkVideoSection()
        Dim so As New ThemeSearcher
        
        so.KeyFather.Id = Me.PageObjectGetTheme.Id
                     
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            LinkVideo.Text = "<a class=""BoxLeftlinkVideo"" href=" & GetLinkVideo(coll(0)) & ">" & coll(0).Description & "</a>"
        End If
    End Sub
    
     
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
       
    'GetLink by Theme Domain/Name
    Function GetLink1(ByVal vo As ContentValue) As String
      
        Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(200), Me.PageObjectGetMasterPage, False)
              
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
    
    Function GetLinkVideo(ByVal themeVal As ThemeValue) As String
      
        Return Me.BusinessMasterPageManager.GetLink(themeVal, New TraceEventIdentificator(1), False)
       
           
    End Function
    
    Function getClassSel(ByVal vo As ContentValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetContent.Id = vo.Key.Id Then
            If vo.Title.Length > 25 Then
                strclass = "class=""sel2"""
            Else
                strclass = "class=""sel2"""
            End If
        End If
        Return strclass
    End Function
    
    Function getClassImgSel(ByVal vo As ContentValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetContent.Id = vo.Key.Id Then
            If vo.Title.Length > 25 Then
                strclass = "class=""imgSel2"""
            Else
                strclass = "class=""imgSel2"""
            End If
          
        End If
        Return strclass
    End Function
   
    Function getClassLink(ByVal vo As ContentValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetContent.Id = vo.Key.Id Then
            If vo.Title.Length < 25 Then
                strclass = "class=""padTop16"""
            Else
                strclass = "class=""padTop9"""
            End If
          
        End If
        Return strclass
    End Function
</script>



<div class="ulNav">
       	        	
           <asp:Repeater ID="rptSiteMenu" runat="server">
                <HeaderTemplate><ul></HeaderTemplate> 
                <ItemTemplate>
                    <li <%# getClassSel(Container.DataItem) %>> 
                      <span <%# getClassImgSel(Container.DataItem) %>>&nbsp;</span>
                      <a href="<%# getlink(Container.DataItem) %>" <%# getClassLink(Container.DataItem) %>> <%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                    </li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
              
            <asp:Literal ID="LinkVideo" runat="server"></asp:Literal>
                        
       
    </div><!--end menuSideL-->
    
        <div class="boxUndNav">
                 <div class="titleColumn">
    	          <h2><%= Me.BusinessDictionaryManager.Read("PLY_TitleLink", Me.PageObjectGetLang.Id, "PLY_TitleLink")%></h2>
                  </div>     
                
                <div class="relLink">
                <div class="img">
                     &nbsp;
                 </div>
               <div class="info">
                   <%= Me.BusinessDictionaryManager.Read("PLY_TextLink", Me.PageObjectGetLang.Id, "PLY_TextLink")%>
                     
                     </div>
                  <div class="floatRight">
                      <a href="/essm/95/essm-letter.html"><%= Me.BusinessDictionaryManager.Read("PLY_goto_Essm", Me.PageObjectGetLang.Id, "PLY_goto_Essm")%></a>
                 </div>
              </div>
              </div>     
 
