﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _IDContent As Integer = 0
    Private _cacheKey As String = String.Empty
	Private _currentTheme as Integer = 0
	Protected _siteArea As SiteAreaIdentificator = Nothing
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
		_currentTheme = Me.PageObjectGetTheme.Id
		_siteArea = Me.PageObjectGetSiteArea
        _IDContent = Me.PageObjectGetContent.Id
    End Sub
    
    Sub Page_Load()
		LoadMenu()     
    End Sub
    
    Sub LoadMenu()
		_cacheKey = "cacheKey:BoxContentui" & _lang & "|" & _IDContent & _currentTheme & "|" & _siteArea.Id
		If Request("cache") = "false" Then CacheManager.RemoveByKey(_cacheKey)
        Dim _strCachedContent As String = CacheManager.Read(_cacheKey, 1)
		
		If Not String.IsnullOrEmpty(_strCachedContent) Then 
			ltlBoxSubMenu.Text = _strCachedContent
		Else
			Dim _sb as StringBuilder = New StringBuilder()
			Dim cntcoll As New ContentExtraCollection
			Dim cntsearch As New ContentExtraSearcher
			Dim cntVal As New ContentExtraValue
			cntsearch.key.IdLanguage = _lang
			cntsearch.KeySiteArea.Domain = Me.BusinessMasterPageManager.GetSite.Id
			cntsearch.KeySiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
			cntsearch.KeyContentType.Id = Me.BusinessMasterPageManager.GetContentType.Id
		   
			cntsearch.SortType = ContentGenericComparer.SortType.ByData
			cntsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
		 
			cntcoll = Me.BusinessContentExtraManager.Read(cntsearch)
			
			If Not cntcoll Is Nothing AndAlso cntcoll.Count > 0 Then
				_sb.Append("<div class='rightBodyRightBig'><h4 class='comunicatiTitle'>" & Me.BusinessDictionaryManager.Read("Title_Box" & Me.BusinessMasterPageManager.GetTheme.Id, Me.PageObjectGetLang.Id, "Title_Box" & Me.BusinessMasterPageManager.GetTheme.Id) & "</h4>")
				_sb.Append("<div class='boxComunicati  managementG'>")

				For Each oCExtra as ContentExtraValue in cntColl
					_sb.Append("<div " & getClass(oCExtra.Key.Id) & "><a class='mngm' href='" & GetLink(oCExtra) & "' title='" & oCExtra.Title.Replace("'", "") & "'>" & oCExtra.Title & "</a>")
					_sb.Append("<div class='img'><img src='/"& _siteFolder & "/_slice/plus.png' /></div>")
					_sb.Append("<div class='info'><p><a title='" & oCExtra.Title.Replace("'", "") & "' href='" & GetLink(oCExtra)  & "'>" & oCExtra.Title & "</a></p></div>")
					_sb.Append("</div>")
				Next			
				_sb.Append("</div></div><div style='clear:both; margin-bottom:20px;'></div>")

				'rptSiteMenu.DataSource = cntcoll
				'rptSiteMenu.DataBind()
			End If
			CacheManager.Insert(_cacheKey, _sb.ToString(), 1, 60)
			ltlBoxSubMenu.Text = _sb.ToString
		
		End If
    End Sub
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 72, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getClass(ByVal idCont As Integer) As String
        Dim res As String = "class=""singleCom"""
        
        If idCont = _IDContent Then
            res = "class=""singleCom singleComContent"""
        End If
        Return res
    End Function
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $(".singleCom ").hover(
			function () {
			    $(this).addClass("singleComBg");
			},
			function () {
			    $(this).removeClass("singleComBg");
			}
		);
    });
</script>
<asp:literal id="ltlBoxSubMenu" runat="server" enableviewstate="false" />

  <asp:Repeater ID="rptSiteMenu" runat="server" visible="false">
	<HeaderTemplate>
	<div class="rightBodyRightBig">
		<h4 class="comunicatiTitle"><%= Me.BusinessDictionaryManager.Read("Title_Box" & Me.BusinessMasterPageManager.GetTheme.Id, Me.PageObjectGetLang.Id, "Title_Box" & Me.BusinessMasterPageManager.GetTheme.Id)%></h4>
			<div class="boxComunicati  managementG">
			
   </HeaderTemplate> 
	<ItemTemplate>
	<div <%# getClass(DataBinder.Eval(Container.DataItem, "Key.Id")) %>>
		<div class="img">
			<img src="<%="/"& _siteFolder &"/_slice/plus.png" %>" />
		</div>
		<div class="info">                    
			<p><a href="<%# GetLink(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "Title")%></a></p>                      
		</div>
	</div><!--end singleCom-->
   
	</ItemTemplate>
	<FooterTemplate>
			
		</div>
	</div>
	<div style="clear:both; margin-bottom:20px;"></div>
	</FooterTemplate>
</asp:Repeater>
