﻿<%@ Control Language="VB" ClassName="MenuProgIniziative" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>

<script runat="server">

    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
        _contentKey = Me.PageObjectGetContent
        _currTheme = GetTheme()
        
    End Sub
    
    Dim _canCheckApproval As Boolean = False
    Protected Sub Page_Load()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        InitView()
        
    End Sub
    
    Protected Sub InitView()
                
        LoadMenu()
                
    End Sub
    
    Protected Sub LoadMenu()
        
        Dim so As New ContentExtraSearcher
        
        so.KeySiteArea = _siteArea
        so.key.IdLanguage = _langKey.Id
        so.CalculatePagerTotalCount = False
        so.PageSize = 1
        
        If Not _canCheckApproval Then
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
        Else
            Me.BusinessContentExtraManager.Cache=False 
        End If
        
        
        Dim baseSo As New ContentBaseSearcher()
        baseSo.key.Id = _currTheme.KeyContent.Id
        baseSo.key.IdLanguage = _langKey.Id
        Dim baseCont = Me.BusinessContentManager.GetContentBaseValue(baseSo)
        If Not baseCont Is Nothing Then
            so.KeysToExclude = baseCont.Key.PrimaryKey.ToString()
        End If
            
        Dim coll As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        
        Dim sBuilder As New StringBuilder()
        
        If Not coll Is Nothing AndAlso coll.Any() Then
            
            Dim currcontent As New ContentIdentificator
            currcontent = Me.BusinessMasterPageManager.GetContent
            'sBuilder.AppendFormat("<div class='generic prjiniziative'><p>{0}</p></div>", Me.BusinessDictionaryManager.Read("DMP_Iniziative", _langKey.Id, "DMP_Iniziative"))
            sBuilder.Append("<ul>")
            Dim sel As String = String.Empty
            DIm strAdd As String = String.Empty
            For Each el As ContentExtraValue In coll
			
                'If el.Key.Id <> 459 Then
                strAdd = String.Empty
                If el.Title.Length < 40 Then strAdd = " singleline"

                If el.Key.Id = currcontent.Id Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If
                sBuilder.AppendFormat("<li> <a class='{0}' title='{1}' href='{2}'>{3} <span>{4}</span></a></li>", sel & strAdd, Server.HtmlEncode(el.Title), GetLink(el.Key), el.Title & Healthware.Dompe.Helper.Helper.checkApproval(el.ApprovalStatus, False), el.SubTitle)
				
				
                'End If
			

            Next
            sBuilder.Append("</ul>")
            
        End If
        _strMenu = sBuilder.ToString()
        sBuilder.Clear()
        
    End Sub
    
    Function GetTheme() As ThemeValue
        Dim so As New ThemeSearcher()
        so.Key = _currentTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        
        Dim coll = Me.BusinessThemeManager.Read(so)
      
        If (Not coll Is Nothing AndAlso coll.Any) Then
            Return coll.FirstOrDefault()
        End If
        
        Return Nothing
    End Function
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, _currTheme, False)
        
    End Function
    
</script>


<%=_strMenu %>