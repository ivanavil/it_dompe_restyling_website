﻿<%@ Control Language="VB" ClassName="richiestaarticolo" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _lang As Integer = 2
    Protected _IDContent As Integer = 0
    Private _cacheKey As String = String.Empty
	Private _currentTheme as Integer = 0
	Protected _siteArea As SiteAreaIdentificator = Nothing
	
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _lang = Me.PageObjectGetLang.Id
		_currentTheme = Me.PageObjectGetTheme.Id
		_siteArea = Me.PageObjectGetSiteArea
        _IDContent = Me.PageObjectGetContent.Id
    End Sub
    
    Sub Page_Load()
        
    End Sub
    
   
  
</script>
<p>&nbsp;</p>
<div class="areaMedicaRequest">
    <div class="redBanner" style="cursor:pointer; width: auto;">
		<a href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArticoloMedical, _lang)%>" title="<%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo", _lang, "DMP_RichiediArticolo").Replace("'", "‘") %>">
			<img alt='<%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo", _lang, "DMP_RichiediArticolo").Replace("'", "‘") %>' src="/ProjectDompe/_slice/RichiediArticolo.jpg" />
		</a>
		<!--
			<h3><%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo", _lang, "DMP_RichiediArticolo") %></h3>
			<p><%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo_riviste", _lang, "DMP_RichiediArticolo_riviste") %></p>
			<a class="viewMore viewMoreWhite" title='<%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo", _lang, "DMP_RichiediArticolo") %>' id="aBoxRed" href="<%=Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idArticoloMedical, _lang)%>"><%=Me.BusinessDictionaryManager.Read("DMP_RichiediArticolo", _lang, "DMP_RichiediArticolo") %></a>
		-->
    </div>
</div>
<script>
	$('.redBanner').click(function(){
		document.location.href = $(this).find("a").attr("href");
	});
</script>