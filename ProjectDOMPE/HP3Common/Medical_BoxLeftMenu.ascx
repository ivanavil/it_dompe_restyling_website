﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<script runat="server">
    Protected _siteFolder As String = String.Empty
    Dim _IdThemaMedical As Integer
    Dim _IdLanguage As Integer
    Public accessService As New AccessService()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _IdThemaMedical = Dompe.ThemeConstants.idAreaMedico
        _IdLanguage = Me.BusinessMasterPageManager.GetLanguage.Id
    End Sub
    
    Sub Page_Load()
        Dim contv As ThemeCollection = Nothing
        
      
        
        If Not Page.IsPostBack Then
            rptSiteMenu.DataSource = LoadMenu()
            rptSiteMenu.DataBind()
        End If
      
        If accessService.IsAuthenticated() Then
            If Me.ObjectTicket.User.Key.Id > 0 Then
                divUtente.Visible = True
            
                Dim myuserRead As New UserValue
                Dim User As New UserManager
                User.Cache = False
        
                myuserRead = User.Read(Me.ObjectTicket.User.Key.Id)
                UserNominativo.Text = myuserRead.Name.ToString & " " & myuserRead.Surname.ToString
                
            Else
                divUtente.Visible = False
            End If
        Else
            divUtente.Visible = False
        End If
        
    End Sub
    
    Function LoadMenu() As ThemeCollection
        Dim so As New ThemeSearcher
        Dim coll As ThemeCollection = Nothing
        so.KeySite = Me.PageObjectGetSite
        so.KeyFather.Id = _IdThemaMedical
        so.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        coll = Me.BusinessThemeManager.Read(so)
       
        Return coll
       
    End Function
     
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
  
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
    
  
  
</script>
<div class="ulLeft" id="divUtente" runat="server">
        <div class="ulNav">
            <div class="ContainerNamingUser">
                <div class="DivBenvenutoUser"><%= Me.BusinessDictionaryManager.Read("DMP_Benvenuto", _IdLanguage, "DMP_Benvenuto")%></div>                
                <div class="DivNamingUser"><asp:Literal ID="UserNominativo" runat="server" /></div>
                
            </div>
        </div>
</div>
<div class="separatore">&nbsp;</div>
<div class="ulLeft">
    <div class="ulNav">   	        	
    <asp:Repeater ID="rptSiteMenu" runat="server">
        <HeaderTemplate><ul></HeaderTemplate> 
        <ItemTemplate>
            <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# DataBinder.Eval(Container.DataItem, "Description")%></a></li>
        </ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>              
    </div>      
</div>