﻿<%@ Control Language="VB" ClassName="MenuWeCare" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>

<script runat="server">

    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    
    Protected Sub Page_Init()
      
    End Sub
    

    
</script>



<div id="ctl02_div_weCare" class="listSide">
    <div class="generic prjiniziative">
      <p style="background:none;">Leggi le storie che si sono ispirate a "In the Woods"</p>
    </div>
    <div class="generic">
        <ul>
            <li><a class="singleline" title="La semina di Cristiana Calilli" href="http://centopercentomamma.it/la-semina/" target="_blank">"La semina" di Cristiana Calilli</a></li>            
            <li><a class="singleline" title="Il sonnellino di Noemi Cuffia" href="http://eccomimi.blogspot.it/2017/03/il-sonnellino-un-racconto-in.html?m=1" target="_blank">"Il sonnellino" di Noemi Cuffia</a></li>            
            <li class="big"><a class="singleline" title="Con gli occhi nel sole di Barbara Damiano" href="http://www.mammafelice.it/2017/03/14/con-gli-occhi-nel-sole/" target="_blank" style="line-height:25px;">“Con gli occhi nel sole” di Barbara Damiano</a></li>            
			<li class="big"><a class="singleline" title="Figlio mio, sei pronto a lotttare? di Daniela Poggi" href="http://scuolainsoffitta.com/2017/03/21/figlio-mio-sei-pronto-a-lottare/" target="_blank" style="line-height:25px;">"Figlio mio, sei pronto a lottare?" di Daniela Poggi</a></li>
			<li class="big"><a class="singleline" title="Lorenzo si ricorda i colori di Alessandra Spada" href="http://www.faccioquellocheposso.com/2017/03/lorenzo-si-ricorda-i-colori/" target="_blank" style="line-height:25px;">"Lorenzo si ricorda i colori" di Alessandra Spada</a></li>
			<li class="big"><a class="singleline" title="La storia della bambina che non voleva dormire di Saverio Tommasi" href="http://www.saveriotommasi.it/la-storia-della-bambina-che-non-voleva-dormire" target="_blank" style="line-height:25px;">"La storia della bambina che non voleva dormire" di Saverio Tommasi</a></li>
        </ul>
    </div>
</div>