﻿<%@ Control Language="VB" ClassName="ExagonCloud" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<script runat="server">

    Protected _langKey As LanguageIdentificator = Nothing
    
    Protected Sub Page_Init()
        _langKey = Me.PageObjectGetLang
    End Sub
    
</script>


<div class="canvasTag">
    <div class="container">
        <div class="tagClouds">
		    <h3><%=Me.BusinessDictionaryManager.Read("DMP_mostClicked", _langKey.Id, "DMP_mostClicked")%></h3>
            <div class="canvasBox" id="canvas">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var sizeCanvas = new Array();
    var xCanvas = new Array();
    var yCanvas = new Array();
    var strCanvas = new Array();
    var yTxtCanvas = new Array();
    var fTxtCanvas = new Array();
    var linkCanvas = new Array();
    strCanvas[0] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud0", _langKey.Id, "DMP_txt_Cloud0")%>';
    strCanvas[1] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud10", _langKey.Id, "DMP_txt_Cloud10")%>';
    strCanvas[2] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud20", _langKey.Id, "DMP_txt_Cloud20")%>';
    strCanvas[3] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud30", _langKey.Id, "DMP_txt_Cloud30")%>';
    strCanvas[4] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud40", _langKey.Id, "DMP_txt_Cloud40")%>';
    strCanvas[5] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud50", _langKey.Id, "DMP_txt_Cloud50")%>';
    strCanvas[6] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud60", _langKey.Id, "DMP_txt_Cloud60")%>';
    strCanvas[7] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud70", _langKey.Id, "DMP_txt_Cloud70")%>';
    strCanvas[8] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud80", _langKey.Id, "DMP_txt_Cloud80")%>';
    strCanvas[9] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud90", _langKey.Id, "DMP_txt_Cloud90")%>';
    strCanvas[10] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud100", _langKey.Id, "DMP_txt_Cloud100")%>';
    strCanvas[11] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud110", _langKey.Id, "DMP_txt_Cloud110")%>';
    strCanvas[12] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud120", _langKey.Id, "DMP_txt_Cloud120")%>'; //ladarixin
    strCanvas[13] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud130", _langKey.Id, "DMP_txt_Cloud130")%>';
    strCanvas[14] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud140", _langKey.Id, "DMP_txt_Cloud140")%>';
    strCanvas[15] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud150", _langKey.Id, "DMP_txt_Cloud150")%>'; //Automedicazione
    strCanvas[16] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud160", _langKey.Id, "DMP_txt_Cloud160")%>';
    strCanvas[17] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud170", _langKey.Id, "DMP_txt_Cloud170")%>';
    strCanvas[18] = '<%=Me.BusinessDictionaryManager.Read("DMP_txt_Cloud180", _langKey.Id, "DMP_txt_Cloud180")%>';

    linkCanvas[0] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud0", _langKey.Id, "DMP_lnk_Cloud0")%>';
    linkCanvas[1] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud10", _langKey.Id, "DMP_lnk_Cloud10")%>';
    linkCanvas[2] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud20", _langKey.Id, "DMP_lnk_Cloud20")%>';
    linkCanvas[3] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud30", _langKey.Id, "DMP_lnk_Cloud30")%>';
    linkCanvas[4] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud40", _langKey.Id, "DMP_lnk_Cloud40")%>';
    linkCanvas[5] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud50", _langKey.Id, "DMP_lnk_Cloud50")%>';
    linkCanvas[6] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud60", _langKey.Id, "DMP_lnk_Cloud60")%>';
    linkCanvas[7] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud70", _langKey.Id, "DMP_lnk_Cloud70")%>';
    linkCanvas[8] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud80", _langKey.Id, "DMP_lnk_Cloud80")%>';
    linkCanvas[9] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud90", _langKey.Id, "DMP_lnk_Cloud90")%>';
    linkCanvas[10] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud100", _langKey.Id, "DMP_lnk_Cloud100")%>';
    linkCanvas[11] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud110", _langKey.Id, "DMP_lnk_Cloud110")%>';
    linkCanvas[12] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud120", _langKey.Id, "DMP_lnk_Cloud120")%>';
    linkCanvas[13] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud130", _langKey.Id, "DMP_lnk_Cloud130")%>';
    linkCanvas[14] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud140", _langKey.Id, "DMP_lnk_Cloud140")%>';
    linkCanvas[15] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud150", _langKey.Id, "DMP_lnk_Cloud150")%>';
    linkCanvas[16] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud160", _langKey.Id, "DMP_lnk_Cloud160")%>';
    linkCanvas[17] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud170", _langKey.Id, "DMP_lnk_Cloud170")%>';
    linkCanvas[18] = '<%=Me.BusinessDictionaryManager.Read("DMP_lnk_Cloud180", _langKey.Id, "DMP_lnk_Cloud180")%>';

    sizeCanvas[0] = 140;
    xCanvas[0] = 480;
    yCanvas[0] = 140;
    yTxtCanvas[0] = 130;
    fTxtCanvas[0] = 18;

    sizeCanvas[1] = 110;
    xCanvas[1] = 640;
    yCanvas[1] = 260;
    yTxtCanvas[1] = 245;
    fTxtCanvas[1] = 18;

    sizeCanvas[2] = 90;
    xCanvas[2] = 760;
    yCanvas[2] = 160;
    yTxtCanvas[2] = 145;
    fTxtCanvas[2] = 18;

    sizeCanvas[3] = 90;
    xCanvas[3] = 90;
    yCanvas[3] = 280;
    yTxtCanvas[3] = 265;
    fTxtCanvas[3] = 18;

    sizeCanvas[4] = 70;
    xCanvas[4] = 90;
    yCanvas[4] = 80;
    yTxtCanvas[4] = 70;
    fTxtCanvas[4] = 18;

    sizeCanvas[5] = 70;
    xCanvas[5] = 170;
    yCanvas[5] = 190;
    yTxtCanvas[5] = 180;
    fTxtCanvas[5] = 18;

    sizeCanvas[6] = 60;
    xCanvas[6] = 290;
    yCanvas[6] = 230;
    yTxtCanvas[6] = 220;
    fTxtCanvas[6] = 18;

    sizeCanvas[7] = 60;
    xCanvas[7] = 900;
    yCanvas[7] = 80;
    yTxtCanvas[7] = 70;
    fTxtCanvas[7] = 18;

    sizeCanvas[8] = 40;
    xCanvas[8] = 310;
    yCanvas[8] = 40;
    yTxtCanvas[8] = 30;
    fTxtCanvas[8] = 13;

    sizeCanvas[9] = 40;
    xCanvas[9] = 360;
    yCanvas[9] = 130;
    yTxtCanvas[9] = 120;
    fTxtCanvas[9] = 13;

    sizeCanvas[10] = 50;
    xCanvas[10] = 650;
    yCanvas[10] = 50;
    yTxtCanvas[10] = 40;
    fTxtCanvas[10] = 13;

    sizeCanvas[11] = 40;
    xCanvas[11] = 700;
    yCanvas[11] = 80;
    yTxtCanvas[11] = 70;
    fTxtCanvas[11] = 9;

    //ladarixin
    sizeCanvas[12] = 40;
    xCanvas[12] = 370;
    yCanvas[12] = 380;
    yTxtCanvas[12] = 370;
    fTxtCanvas[12] = 13;

    sizeCanvas[13] = 50;
    xCanvas[13] = 270;
    yCanvas[13] = 140;
    yTxtCanvas[13] = 135;
    fTxtCanvas[13] = 11;

    sizeCanvas[14] = 50;
    xCanvas[14] = 250;
    yCanvas[14] = 350;
    yTxtCanvas[14] = 345;
    fTxtCanvas[14] = 11;

    //automedicazione
    sizeCanvas[15] = 53;
    xCanvas[15] = 455;
    yCanvas[15] = 350;
    yTxtCanvas[15] = 345;
    fTxtCanvas[15] = 11;

    sizeCanvas[16] = 30;
    xCanvas[16] = 650;
    yCanvas[16] = 130;
    yTxtCanvas[16] = 125;
    fTxtCanvas[16] = 11;

    sizeCanvas[17] = 30;
    xCanvas[17] = 740;
    yCanvas[17] = 40;
    yTxtCanvas[17] = 35;
    fTxtCanvas[17] = 11;

    sizeCanvas[18] = 30;
    xCanvas[18] = 810;
    yCanvas[18] = 60;
    yTxtCanvas[18] = 55;
    fTxtCanvas[18] = 11;
	</script>
	<script src="/ProjectDompe/_js/kinetic-v5.0.1.min.js"></script>
    <script defer>
        var stage = new Kinetic.Stage({
            container: 'canvas',
            width: 975,
            height: 475
        });
        var layer = new Kinetic.Layer();
        var hexagon;
        for (var x = 0; x < 19; x++) {
            hexagon = new Kinetic.RegularPolygon({
                x: xCanvas[x],
                y: yCanvas[x],
                sides: 6,
                radius: sizeCanvas[x],
                fill: '#819ea1',
                stroke: '#fff',
                opacity: 0.5,
                strokeWidth: 1
            });
            var tooltip = new Kinetic.Text({
                x: xCanvas[x],
                y: yTxtCanvas[x],
                text: strCanvas[x],
                fontSize: fTxtCanvas[x],
                fill: '#fff'
            });

            // to align text in the middle of the screen, we can set the
            // shape offset to the center of the text shape after instantiating it
            tooltip.offsetX(tooltip.width() / 2);
            hexagon.on('mouseover', function () {
                this.fill('#729296');
                layer.draw();
            });
            hexagon.on('mouseout', function () {
                this.fill('#819ea1');
                layer.draw();
            });
            var linkTemp = linkCanvas[x];
            hexagon.myLink = linkTemp;
            tooltip.myLink = linkTemp;
            hexagon.on('click touchend', function () {
                location.href = this.myLink;
            });
            tooltip.on('click touchend', function () {
                location.href = this.myLink;
            });
            layer.add(hexagon);
            layer.add(tooltip);
            // add the shape to the layer
        }

        // add the layer to the stage
        stage.add(layer);
   </script>

