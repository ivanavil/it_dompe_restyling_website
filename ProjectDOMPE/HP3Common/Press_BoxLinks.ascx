﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _MAXLinks As Integer = 6
    Dim targetlnk As String = ""
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
    End Sub
    
    Sub Page_Load()
        
        If Not Page.IsPostBack Then
            rptSiteMenu.DataSource = LoadMenu()
            rptSiteMenu.DataBind()
           
        End If
      
    End Sub
    
    Function LoadMenu() As ContentCollection
       
        Dim idContentKeyTheme As Integer = GetPKThema()
      
        
        Dim so As New ContentSearcher
        Dim coll As ContentCollection = Nothing
      
        so.KeySite = Me.PageObjectGetSite
        so.key.IdLanguage = Me.PageObjectGetLang.Id
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaLinksMediaPress
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.KeysToExclude = idContentKeyTheme
        so.SetMaxRow = _MAXLinks
                          
        coll = Me.BusinessContentManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll
        Else
            Return Nothing
        End If
        
    End Function
    
    Function GetPKThema() As Integer
        Dim idContentKeyTheme As Integer = 0
        Dim idContentTheme As Integer
        Dim contentextraVal As New ContentExtraValue
        Dim valthem As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idLinksUsefulMediaPress))
        idContentTheme = valthem.KeyContent.Id
        
        contentextraVal = Me.BusinessContentExtraManager.Read(New ContentIdentificator(idContentTheme, Me.PageObjectGetLang.Id))
        If Not contentextraVal Is Nothing AndAlso contentextraVal.Key.Id > 0 Then
            idContentKeyTheme = contentextraVal.Key.PrimaryKey
        End If
        Return idContentKeyTheme
    End Function
    
    Function GetImage(ByVal IDcont As Integer, ByVal title As String) As String
        Dim res As String = ""
        res = Me.BusinessMasterPageManager.GetCover(New ContentIdentificator(IDcont, 1), 50, title, title)
        Return res
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
        
    'GetLink by Theme Domain/Name
   
    Function getlinkCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = ""
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "javascript:ExternalLinkPopup" & Me.PageObjectGetLang.Code.ToLower & "('" & vo.LinkUrl & "');"
        Else
            Return Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idLinksUsefulMediaPress), Me.PageObjectGetMasterPage, False)
        End If
    End Function
    
    'Function getTarget(ByVal vo As ContentValue) As String
    '    targetlnk = ""
    '    If Not String.IsNullOrEmpty(vo.LinkUrl) Then
    '        targetlnk = "target=""_blank"""
    '        Return targetlnk
    '    Else
    '        Return targetlnk
    '    End If
    'End Function
        

    Function getTrackCont(ByVal vo As ContentValue) As String
        Dim themVal As String = ""
        Dim themeId As Integer = 0
        targetlnk = Me.BusinessMasterPageManager.GetLink(vo, New ThemeIdentificator(Dompe.ThemeConstants.idLinksUseful), Me.PageObjectGetMasterPage, False)
        If Not String.IsNullOrEmpty(vo.LinkUrl) Then
            Return "recordOutboundExternalFull('Press Leggi Anche','Click','" & vo.Title & "','" & vo.LinkUrl & "','" & Me.PageObjectGetLang.Code.ToLower & "');return false;"
        Else
            Return "recordOutboundLink('" & targetlnk & "','Press Leggi Anche','Click','" & vo.Title & "','" & targetlnk & "');"
        End If
    End Function
</script>

     <div class="rightBodyRight">
           <asp:Repeater ID="rptSiteMenu" runat="server">
                <HeaderTemplate>
                    <h4 class="linkTitle"><%= Me.BusinessDictionaryManager.Read("Press_Title_Link", Me.PageObjectGetLang.Id, "Press_Title_Link")%></h4>
                <div class="linkBox">
               </HeaderTemplate> 
                <ItemTemplate>
         
                <h3>
                  <a href="<%# getlinkCont(Container.DataItem) %>" onclick="<%# getTrackCont(Container.DataItem) %>"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                </h3>
                 
                </ItemTemplate>
                <FooterTemplate></div></FooterTemplate>
            </asp:Repeater>
                              
       </div>
  

  

