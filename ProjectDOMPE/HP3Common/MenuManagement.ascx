﻿<%@ Control Language="VB" ClassName="MenuManagement" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">

    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currTheme As ThemeIdentificator = Nothing
    Protected _currentTheme As ThemeValue = Nothing
    Protected _firstMenu As String = String.Empty
    Protected _secondMenu As String = String.Empty
    
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _siteArea = Me.PageObjectGetSiteArea
        _currTheme = Me.PageObjectGetTheme
        
    End Sub
    Dim _canCheckApproval As Boolean = False
    Protected Sub Page_Load()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _canCheckApproval = True
        InitView()
        
    End Sub
    
    Protected Sub InitView()
        
        Dim so As New ThemeSearcher()
        so.Key = _currTheme
        so.KeyLanguage = _langKey
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
        _currentTheme = Helper.GetTheme(so)
        LoadMenu()
        
    End Sub
    
    Protected Sub LoadMenu()
        
        
        
        Dim so As New ContentExtraSearcher()
        Dim ssssbb As New StringBuilder()
        so.KeySiteArea = _siteArea
        so.key.IdLanguage = _langKey.Id
        so.KeyContext.Id = Dompe.ContextCostant.idManagementMain
        
        If Not _canCheckApproval Then
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
        Else
            Me.BusinessContentExtraManager.Cache = False
        End If
        
        
        Dim coll As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        If Not coll Is Nothing AndAlso coll.Any() Then
            Dim currcontent As New ContentIdentificator
            currcontent = Me.BusinessMasterPageManager.GetContent
            ssssbb.Append("<ul>")
            Dim sel As String = String.Empty
			Dim classBig As String = String.Empty
            For Each el As ContentExtraValue In coll
                If el.Key.Id = currcontent.Id Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If

                If el.SubTitle.Length > 34 Then
                    classBig = "big"
                Else
                    classBig = String.Empty
                End If	 			
                ssssbb.Append("<li class='" & classBig & "'>")
                ssssbb.Append("<a class='" & sel & "' title='" & Server.HtmlEncode(el.PageTitle) & "' href='" & GetLink(el.Key) & "'>" & el.PageTitle & "<br /><span style=""color: #000;"">" & el.SubTitle & "</span></a>")
                ssssbb.Append("</li>")
            Next
            ssssbb.Append("</ul>")
        End If
        _firstMenu = ssssbb.ToString
        
        'Menu secondario
        ssssbb = New StringBuilder()
        so.KeySiteArea = _siteArea
        so.key.IdLanguage = _langKey.Id
        so.KeyContext.Id = Dompe.ContextCostant.idManagementSecondary
        so.SortOrder = ContentGenericComparer.SortOrder.ASC
        so.SortType = ContentGenericComparer.SortType.ByTitle
        Dim coll2 As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        If Not coll2 Is Nothing AndAlso coll2.Any() Then
            Dim currcontent As New ContentIdentificator
            currcontent = Me.BusinessMasterPageManager.GetContent
            If _langKey.Id = 3 Then
                ssssbb.Append("<div class='generic management'><p>MENAXHIM</p></div>")
            Else
                ssssbb.Append("<div class='generic management'><p>MANAGEMENT</p></div>")
            End If            
            ssssbb.Append("<div class='generic'><ul>")
            Dim sel As String = String.Empty
			Dim classBig As String = String.Empty
            For Each el As ContentExtraValue In coll2
                If el.Key.Id = currcontent.Id Then
                    sel = "sel"
                Else
                    sel = String.Empty
                End If
                If el.SubTitle.Length > 34 Then
                    classBig = "big"
                Else
                    classBig = String.Empty
                End If				
                ssssbb.Append("<li class='" & classBig & "'>")
                ssssbb.Append("<a class='" & sel & "' title='" & Server.HtmlEncode(el.PageTitle) & "' href='" & GetLink(el.Key) & "'>" & el.PageTitle & Healthware.Dompe.Helper.Helper.checkApproval(el.ApprovalStatus, False) & "<br /><span" & IIf(Not String.IsNullOrEmpty(classBig), " style='line-height:15px;'", String.Empty) & " style=""color: #000;"">" & el.SubTitle & "</span></a>")
                ssssbb.Append("</li>")
            Next
            ssssbb.Append("</ul></div>")
        End If
        _secondMenu = ssssbb.ToString()
        
    End Sub
    
    Function GetLink(ByVal keyContent As ContentIdentificator) As String
           
        Return Me.BusinessMasterPageManager.GetLink(keyContent, _currentTheme, False)
        
    End Function
    
</script>
<%=_firstMenu %>
<%=_secondMenu %>