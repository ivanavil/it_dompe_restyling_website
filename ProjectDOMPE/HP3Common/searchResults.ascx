<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.SearchEngine" %>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>
<%@ Import Namespace="Healthware.DMP.BL" %>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Protected word As String = ""
    Protected _langID As Integer = 1
    Protected _siteArea As Integer = 0
    Protected _topKeys As String = ""
    Protected _ajaxLiteraturePage As String = String.Empty
    Protected _numResult As String = ""
    Protected _strRole As String = ""
    Private Const MAX_PAGE_SIZE As Integer = 10
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _langID = Me.PageObjectGetLang.Id
        _siteArea = Me.PageObjectGetSiteArea.Id
        _ajaxLiteraturePage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
    End Sub
    
    Sub page_load()
        Dim tag As Integer = Request("tag")
        word = Request("wd")
        If Not String.IsNullOrEmpty(word) Then
            word = Server.UrlDecode(word).Replace("'", "\'")
        End If
        
        If Not Me.ObjectTicket Is Nothing AndAlso Me.ObjectTicket.User.Key.Id > 0 Then
            Dim roleCol As RoleCollection = Me.ObjectTicket.Roles
            
            For Each itm As RoleValue In roleCol
                _strRole = _strRole + "{" & itm.Key.Id & "}"
            Next
        End If
        
        If Not IsPostBack Then
            '     bottomPager.CurrentPageIndex = 1
           
            'Se TAG � vuoto e Word non � vuoto
            If (Not String.IsNullOrEmpty(word)) And (tag = 0) Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundFull('Search','" & word & "','Result " & _numResult & " records');initPageVideo();})", True)
            End If
        End If
       
           
    
    End Sub
    
    Protected Sub PageIndexChanged(ByVal sender As Object, ByVal e As PagerIndexChangedEventArgs)
        '    bottomPager.CurrentPageIndex = e.NewPageIndex
        '  DoSearchByWord(word)
    End Sub

    
    'taglia il lancio a maxLen caratteri
    Function GetShortLaunch(ByVal launch As String, ByVal maxLen As Integer) As String
        Dim shortLaunch As String = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(launch)
        
        If Len(shortLaunch) > maxLen Then
            If InStr(maxLen, shortLaunch, " ") > 0 Then
                shortLaunch = Left(shortLaunch, InStr(maxLen, shortLaunch, " ")) & "..."
            End If
        End If

        Return shortLaunch
    End Function
    
    Function GetLabel(ByVal label As String, ByVal defaultLabel As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id, defaultLabel)
    End Function
    
    Protected Sub LoadSearchControl(ByVal ctrlPath As String, ByVal usCtrlName As String)
        Dim usCtrl As UserControl = LoadControl(ctrlPath)
      
        If Not usCtrl Is Nothing Then
            usCtrl.ID = usCtrlName
            phSearch.Controls.Add(usCtrl)
        End If
        
    End Sub
    
  
    Sub TakeDownload(ByVal s As Object, ByVal e As EventArgs)
        Response.Write(" take dwn") : Response.End()
    End Sub
    
    Sub TakeDownload(ByVal oContentKey As Integer, ByVal oDownloadType As Integer) 'EventArgs)
            
        Me.BusinessDownloadManager.Cache = False
        Me.BusinessContentManager.Cache = False
        
        Dim oDownloadValue As DownloadValue = Me.BusinessDownloadManager.Read(New ContentIdentificator(oContentKey), New DownloadTypeIdentificator(oDownloadType))
        If Not oDownloadValue Is Nothing Then
            Dim oContentValue As ContentValue = Me.BusinessContentManager.Read(oContentKey)
            If Not oDownloadValue Is Nothing Then
                Me.BusinessContentManager.Update(oContentValue.Key, "Qty", (oContentValue.Qty + 1).ToString())
                '  Dim objTemporaryTrace As New TemporaryTrace()
                '    objTemporaryTrace.Trace(oContentValue.Key.Id, 5, oDownloadValue.DownloadTypeCollection(0).FileName.Substring(oDownloadValue.DownloadTypeCollection(0).FileName.IndexOf(".") + 1))
                'Me.BusinessMasterPageManager.Trace(oContentValue.Key.Id, 5, oDownloadValue.DownloadTypeCollection(0).FileName.Substring(oDownloadValue.DownloadTypeCollection(0).FileName.IndexOf(".") + 1))
                DownloadManager.TakeDownload(oDownloadValue, oDownloadValue.Title)
                   
            End If
        End If
    End Sub
     
    
    Function GetLink(ByVal content As ContentValue, ByVal title As String) As String
        Dim cachekey As String = String.Empty
        Dim cachedLink As String = String.Empty
        
        cachekey = "PLYDownlaod|" & ClassUtility.PropertyValueToString(content.Key)
        cachekey += "|" & title
            
        cachedLink = CacheManager.Read(cachekey, CacheManager.FirstLevelCache)
        
        If (String.IsNullOrEmpty(cachedLink)) Then
            
      
            Dim domain As String = "http://" & Me.ObjectSiteDomain.Domain
            Dim folder As String = "/download/"
       
            title = StringUtility.CleanHtml(title)
            title = StringUtility.StringUrlNormalize(title)
        
            If (Not content Is Nothing AndAlso content.Key.PrimaryKey > 0) And (Not String.IsNullOrEmpty(title)) Then
           
                Dim link As String = domain & folder & title.ToLower.Replace(" ", "-")
           
                Dim customParam As String = String.Empty
                customParam = String.Format("PK:{0}|ID:{1}|LANG:{2}|CTy:{3}|SA:{4}|TH:{5}", content.Key.PrimaryKey, content.Key.Id, content.Key.IdLanguage, content.ContentType.Key.Id, content.SiteArea.Key.Id, GetTheme(content.ContentType.Key, content.SiteArea.Key).Id)
                       
                Dim actEncryptParams As String = SimpleHash.UrlEncryptRijndael(customParam)
                Dim sb As New StringBuilder(link)
                sb.Append(".dw?down=").Append(actEncryptParams)
            
                CacheManager.Insert(cachekey, sb.ToString(), CacheManager.FirstLevelCache)
                
                Return sb.ToString()
            End If
        End If
        
        Return cachedLink
    End Function
  
    
    
    'genero il link per un contenuto data la siteArea,ContentType, KeyContent
    'il metodo recuper il tema di riferimento e imposta il ContentID nel link
    Function GetLink(ByVal keySiteArea As SiteAreaIdentificator, ByVal ContentType As ContentTypeIdentificator, ByVal key As ContentIdentificator) As String
        Dim so As New ThemeSearcher
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea = keySiteArea
        so.KeyContentType = ContentType
        so.KeyLanguage = Me.PageObjectGetLang
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Dim theme As ThemeValue = coll(0)
            ' theme.KeyContent = key 'BUG 
           
            Return Me.BusinessMasterPageManager.GetLink(key, theme, False)
        End If
        
        Return String.Empty
    End Function
       
    Function GetLink(ByVal keySiteArea As SiteAreaIdentificator, ByVal ContentType As ContentTypeIdentificator) As String
        Dim so As New ThemeSearcher
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea = keySiteArea
        so.KeyContentType = ContentType
        so.KeyLanguage = Me.PageObjectGetLang
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Dim theme As ThemeValue = coll(0)
                       
            Return Me.BusinessMasterPageManager.GetLink(theme, False)
        End If
        
        Return String.Empty
    End Function
       
    
    Function GetTheme(ByVal ContentType As ContentTypeIdentificator, ByVal keySiteArea As SiteAreaIdentificator) As ThemeIdentificator
        Dim so As New ThemeSearcher
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea = keySiteArea
        so.KeyContentType = ContentType
        so.KeyLanguage = Me.PageObjectGetLang
        
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return coll(0).Key
            
        End If
        Return Nothing
    End Function
</script>

<script type="text/javascript" language="javascript">

    function initPageVideo() {
        $lastid = 1;
        $(".videosgrp").remove();
        AddMoreContent($lastid);
    }
 
    function AddMoreContent(page) {
        var bytitle = 0;
        var bydate = 0;
        $('#ErrorRes').attr("style", "display:none");
        $(".MoreResult").remove();
        $('#loading').show()//css('display', 'block');

        $.ajax({
            contentType: "text/html; charset=utf-8",
            url: '/ProjectDOMPE/_async/loadContentFilter.aspx',
            dataType: "html",
            data: { 'pageNum': page, 'Rolelist': '<%= _strRole %>', 'languageId': '<%= _langID %>', 'pageSize': '<%= MAX_PAGE_SIZE %>', 'word': '<%= word %>' },
            success: function (data) {
                $('#content').append(data);
                $('#loading').hide(); //css('display', 'none');
                $('#ErrorRes').attr("style", "display:none");
            },
            error: function () {
                $('#ErrorRes').attr("style", "display:block");
            }
        });
        // 
    }
</script>

<div class="body">
 <div class="container">
    <div class="bodyBox">
        <div class="barTitleAdv">
           <div class="title">
          <%--  <h2><%= GetLabel("searchRes", "Search Result")%></h2>--%>
            </div>
             <asp:PlaceHolder ID="phSearch" runat="server"  EnableViewState="false"/>
      </div>
 
    <div id="content">         
      
        </div>
      <div id="ErrorRes" style=" display:none;">
        <h2><%= Me.BusinessDictionaryManager.Read("DMP_TitleErrorResultSearch", _langID, "DMP_TitleErrorResultSearch")%></h2>
         <%= Me.BusinessDictionaryManager.Read("DMP_TextErrorResultSearch", _langID, "DMP_TextErrorResultSearch")%>
      </div>
    </div><%--end searchResCont--%>
   </div>
 </div>


