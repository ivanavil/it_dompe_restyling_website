﻿
<%@ Control Language="VB" ClassName="HomeEvents" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.DMP" %>

<script runat="server">

    Protected _newsTemplate As String = String.Empty
    Protected _ajaxNewsPage As String = String.Empty
    Protected _siteFolder As String = String.Empty
    Protected _langKey As LanguageIdentificator = Nothing
    Protected _topKeysNews As String = String.Empty
    Protected Sub Page_Init()
        
        _siteFolder = Me.ObjectSiteFolder
        _newsTemplate = String.Format("/{0}/Templates/HomeNews.htm", _siteFolder)
        _ajaxNewsPage = String.Format("/{0}/HP3Handler/Literature.ashx", _siteFolder)
        _langKey = Me.PageObjectGetLang
        
        Dim thval As New ThemeValue
        thval = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNews))
        If Not thval Is Nothing AndAlso thval.KeyContent.Id > 0 Then
            _topKeysNews = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(thval.KeyContent.Id, _langKey.Id)
            
        End If
    End Sub
    
    Dim _CheckApproval As Integer = 1
    Sub page_load()
        If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleReview) Then _CheckApproval = 0
    End Sub
    
</script>


<section class="last-news">
    <div class="container">
    	<h1>Ultime notizie</h1>
    	<div class="sep">&nbsp;</div>
        <div id="newsLoader" style="text-align:center; display:none; margin: 150px 0 0 0;"><img src="/<%=_siteFolder%>/_slice/ajax-loader.gif" alt="loading" /></div>
            <div id="eventiCenter" class="generic">
            </div>
    </div>
</section>


<%--<div class="eventiCont">
    <div class="imgEventi">&nbsp;</div>
    <div id="" style="text-align:center; display:none; margin: 150px 0 0 0;"><img src="/<%=_siteFolder%>/_slice/ajax-loader.gif" alt="loading" /></div>
    <div class="eventiCenter"></div><!--end eventiCenter-->    
</div><!--end eventiCont--> --%>

<script type="text/javascript">

    var _templateUrl = "<%=_newsTemplate%>";
    var _ajaxNewsPage = "<%=_ajaxNewsPage%>";
    var _templateName = "HomeNewsTemplate";
    var _defaultKeysToExclude = "<%=_topKeysNews%>";
    var LoadNewsCallback = function (data) {

        var container = $("#eventiCenter");
        if (data) {
            if (data.Success) {
                if (data.StatusCode == "<%=CInt(Keys.EnumeratorKeys.AsyncStatusCode.FillItem)%>") {
                    $.tmpl(_templateName, data.Result.Collection).appendTo(container);
                    container.fadeIn('fast');
                    $("#newsLoader").hide();
                } else if (data.StatusCode == "<%=CInt(Keys.EnumeratorKeys.AsyncStatusCode.NoItem)%>") {
                    container.fadeIn('fast');
                    $("#newsLoader").hide();
                    $(".eventiCont").hide();
                }
            }
        }
    };


    var OnLoadNewsCallback = function () {

        $('.singleEvents').hover(
             function () {
                 $(this).find('.txtEv').css('display', 'none');
                 $(this).find('.goEv').css('display', 'block');
                 /*$(this).find('.img').children().css('width','500px');
                 $(this).find('.img').children().css('left','-90px');
                 $(this).find('.img').children().css('top','-20px');*/
                 $(this).find('.img').children().animate({ width: "360px", left: "-20px", top: "-10px" }, 1000);

             },
             function () {
                 $(this).find('.txtEv').css('display', 'block');
                 $(this).find('.goEv').css('display', 'none');
                 /*$(this).find('.img').children().css('width','323px');
                 $(this).find('.img').children().css('left','0px');
                 $(this).find('.img').children().css('top','0px');*/
                 $(this).find('.img').children().animate({ width: "323px", left: "0px", top: "0px" }, 1000);
             }
         );
    }

    function LoadTemplate(templateUrl, LoaderFunction, searcher, callback, onCompleteCallback) {

        $.get(templateUrl, function (template) {
            $.template(_templateName, template);

            if (LoaderFunction) LoaderFunction(searcher, callback, onCompleteCallback);

        });
    }

    function LoadNews(contentSearcher, callback, onCompleteCallback) {
        $('#newsLoader').fadeIn('fast');
        $('#eventiCenter').hide();
        
        var postData = {
            op: '<%=Cint(Keys.EnumeratorKeys.LiteratureViewType.FeaturedBox)%>',
            data: JSON.stringify(contentSearcher)
        };

        $.ajax({
            url: _ajaxNewsPage,
            type: 'POST',
            data: postData,
            cache: false,
            success: function (data) {

                if (callback && typeof (callback) == 'function') callback(data);
            },
            error: function (data) {

            }
        }).complete(function () {
            if (onCompleteCallback && typeof (onCompleteCallback) == 'function') onCompleteCallback();

        });
    }

   $(document).ready(function () {

       var searcher = GetSearcher();

       LoadTemplate(_templateUrl, LoadNews, searcher, LoadNewsCallback, OnLoadNewsCallback);       

   });

   function GetSearcher() {
       var searcher = {
           SiteareId: '<%=Dompe.SiteAreaConstants.IDSiteareaNews%>',
            ContenTypeId: '<%=Dompe.ContentTypeConstants.Id.ArchivioCtype%>',
            LanguageId: '<%=_langKey.Id%>',
            ThemeID: '<%=Dompe.ThemeConstants.idNews %>',
            KeysToExclude: _defaultKeysToExclude,
            Display: true,
            Active: true,
            ApprovalStatus: '<%=_CheckApproval%>',
            Delete: false,
            PageNumber: 1,
            PageSize: 4
        };

        recordOutboundFull("Home <%= Helper.GetSectionName(New SiteAreaIdentificator(Dompe.SiteAreaConstants.IDSiteareaNews))%>", "Caricamento", "page: 1")

        return searcher;
    }

    

</script>