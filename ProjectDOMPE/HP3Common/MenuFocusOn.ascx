﻿<%@ Control Language="VB" ClassName="MenuSedi"  Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>

<script runat="server">

    Protected _langKey As LanguageIdentificator = Nothing
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _siteArea As SiteAreaIdentificator = Nothing
    Protected _contentKey As ContentIdentificator = Nothing
    Protected _currTheme As ThemeValue = Nothing
    Protected _strMenu As String = String.Empty
    
    Protected Sub Page_Init()
        
        _langKey = Me.PageObjectGetLang
        _currentTheme = Me.PageObjectGetTheme
        _siteArea = Me.PageObjectGetSiteArea
        _contentKey = Me.PageObjectGetContent
        
        
    End Sub
    
    Protected Sub Page_Load()
        
        Dim soT As New ThemeSearcher
        Dim collT As ThemeCollection = Nothing
        soT.KeySite = Me.PageObjectGetSite
        soT.KeyFather.Id = Dompe.ThemeConstants.idAreaPartner
        soT.KeyLanguage.Id = Me.BusinessMasterPageManager.GetLanguage.Id
        collT = Me.BusinessThemeManager.Read(soT)
           
        If Not collT Is Nothing AndAlso collT.Count > 0 Then
            rptSiteMenu.DataSource = collT
            rptSiteMenu.DataBind()
        End If
        
    End Sub
    
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Helper.GetThemeLink(Me.BusinessMasterPageManager, vo.Key.Id, _langKey.Id)
        'Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getClassSel(ByVal vo As ThemeValue) As String
        Dim strclass As String = ""
        If Me.PageObjectGetTheme.Id = vo.Key.Id Then
          
            strclass = "class=""sel"""
            
        End If
        Return strclass
    End Function
    
</script>

                <div class="listSide">                	
                     <asp:Repeater ID="rptSiteMenu" runat="server">
                        <HeaderTemplate><ul></HeaderTemplate> 
                        <ItemTemplate>
                            <li><a href="<%# getlink(Container.DataItem) %>" <%# getClassSel(Container.DataItem) %>><%# Me.BusinessDictionaryManager.Read("thm_" & DataBinder.Eval(Container.DataItem, "key.id"), _langKey.Id)%></a></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>  
                </div>