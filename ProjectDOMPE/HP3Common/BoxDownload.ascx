﻿<%@ Control Language="VB" ClassName="siteMap" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace=" Healthware.HP3.Core.Utility" %>
<script runat="server">
    Protected _siteFolder As String = String.Empty
    Protected _langCode As String = String.Empty
    Protected _langId As Integer = 0
    Protected _contID As Integer = 0
    Protected _literalDwn As StringBuilder = New StringBuilder
    Protected _MAXLinks As Integer = 3
 
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _langCode = Me.PageObjectGetLang.Code.ToLower
        _langId = Me.PageObjectGetLang.Id
    End Sub
    
    Sub Page_Load()
        If Request("cache") = "false" Then CacheManager.RemoveByKey("BoxDownload_" & _langId & "_" & _contID)
        Dim collectDownload As ContentCollection = Nothing
        _contID = Me.PageObjectGetContent.Id
        Dim useCache As Boolean = False
        useCache = CacheManager.Exists("BoxDownload_" & _langId & "_" & _contID)
        
        
        If useCache Then
            TxtListDownload.Text = CacheManager.Read("BoxDownload_" & _langId & "_" & _contID, 1).ToString
            If Not String.IsNullOrEmpty(TxtListDownload.Text) Then
                divDwlRelated.Visible = True
            End If
        Else
            If _contID > 0 Then
                collectDownload = LoadDownloadAssociati(_contID)
                ' Else
                'collectDownload = DownloadableList()
            End If
            
            If Not collectDownload Is Nothing AndAlso collectDownload.Count > 0 Then
                Dim i As Integer = 1
                Dim mfi As New System.Globalization.DateTimeFormatInfo()

                For Each cntVal As ContentValue In collectDownload
                    Dim traceScript As String = "recordOutboundLink('/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(cntVal.Key.PrimaryKey) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & Me.PageObjectGetLang.Id & "&cS=" & Me.PageObjectGetContent.PrimaryKey & "','Download Materiali Correlati','Download','" & cntVal.Title & "','" & cntVal.Key.PrimaryKey & "');return false;"
                    
                    
                    _literalDwn.Append("<li><img src=""/ProjectDOMPE/_slice/ico-file.png"" /><a href=""/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(cntVal.Key.PrimaryKey) & "&sa=" & Me.PageObjectGetSiteArea.Id & "&ct=" & Me.PageObjectGetContentType.Id & "&u=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.User.Key.Id) & "&s=" & SimpleHash.UrlEncryptRijndaelAdvanced(Me.ObjectTicket.IdSession) & "&lk=" & Me.PageObjectGetLang.Id & "&cS=" & Me.PageObjectGetContent.PrimaryKey & """ onclick=""" & traceScript & """>" & cntVal.Title & "</a></li>")
                    
                    i = i + 1
                Next
                'Response.Write("<br>" & collectDownload.Count & "<br>")
                divDwlRelated.Visible = True
            Else
                divDwlRelated.Visible = False
            End If
            TxtListDownload.Text = _literalDwn.ToString
            CacheManager.Insert("BoxDownload_" & _langId & "_" & _contID, _literalDwn.ToString, 1, 60)
        End If
        
        'If Not IsPostBack() Then
        '    DownloadableList()
        'End If
        'DownloadableList()
    End Sub
    
    Function DownloadableList(Optional ByVal numItem As Integer = 0, Optional ByVal collectItem As ContentCollection = Nothing) As ContentCollection
        Dim dwncoll As New ContentCollection
        Dim dwnsearch As New ContentSearcher
       
        dwnsearch.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
        dwnsearch.SortType = ContentGenericComparer.SortType.ByData
        dwnsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
        dwnsearch.key.IdLanguage = _langId

        If numItem > 0 Then
            dwnsearch.SetMaxRow = numItem
        Else
            dwnsearch.SetMaxRow = _MAXLinks
        End If
        
        If Not collectItem Is Nothing Then
            Dim strExclude As String = ""
            Dim count As Integer = 0
            For Each itm As ContentValue In collectItem
                If count > 0 Then strExclude = strExclude & ";"
                strExclude = strExclude & itm.Key.PrimaryKey
            Next
            dwnsearch.KeysToExclude = strExclude
        End If
        
        dwncoll = Me.BusinessContentManager.Read(dwnsearch)
        
        If Not dwncoll Is Nothing AndAlso dwncoll.Any Then
            Return dwncoll
        End If
        
    End Function
    
    Function LoadDownloadAssociati(ByRef IDCont As Integer) As ContentCollection
        Dim so As New ContentSearcher
        Dim cntVal As New ContentValue
        Dim soRel As New ContentRelatedSearcher
        Dim coll As ContentCollection = Nothing
        
        Dim pk As Integer = Me.BusinessContentManager.ReadPrimaryKey(IDCont, _langId)
      
        If pk > 0 Then
            soRel.Content.Key.PrimaryKey = pk
            soRel.Content.KeyRelationType.Id = 1
            soRel.Content.RelationSide = RelationTypeSide.Left
        End If
        
        so.key.IdLanguage = _langId
        so.KeySite = Me.PageObjectGetSite
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDDownloadArea
        so.SortOrder = ContentGenericComparer.SortOrder.DESC
        so.SortType = ContentGenericComparer.SortType.ByData
        so.RelatedTo = soRel
        so.SetMaxRow = _MAXLinks
        coll = Me.BusinessContentManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            ' Response.Write("test " & pk)
            '--- annullata la modifica precendente che aggiungeva al numero di contenuti correlati altri n contenuti
            'If coll.Count < _MAXLinks Then
            '    Dim addLink As Integer = _MAXLinks - coll.Count
            '    Dim collTmp As ContentCollection = DownloadableList(addLink, coll)
            '    If Not collTmp Is Nothing AndAlso collTmp.Count > 0 Then
            '        For Each itmVal As ContentValue In collTmp
            '            coll.Add(itmVal)
            '        Next
            '    End If
            '    Return coll
            'Else
            '    Return coll
            'End If
            '----------------------------------
            
            Return coll
        Else
            Return Nothing 'DownloadableList() 'altri materiali non correlati
        End If
    End Function
      
    Function GetLink(ByVal vo As ContentValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    
    Function getSizeFile(ByVal sizekb As Integer) As String
        Dim manhelp As New Healthware.Dompe.Helper.Helper
        Return manhelp.convertSize(sizekb)
    End Function
    
    Sub TakeDownload(ByVal s As Object, ByVal e As EventArgs) 'EventArgs)
        'File Format
        Dim oDownloadType As Integer = s.commandargument.ToString.Split(";")(0)
        'Content
     
        Me.BusinessDownloadManager.Cache = False
        Me.BusinessContentManager.Cache = False
        
        Dim oContentKey As Integer = s.commandargument.ToString.Split(";")(1)
      
        Dim oDownloadValue As DownloadValue = Me.BusinessDownloadManager.Read(New ContentIdentificator(oContentKey), New DownloadTypeIdentificator(oDownloadType))
        If Not oDownloadValue Is Nothing Then
            Dim oContentValue As ContentValue = Me.BusinessContentManager.Read(oContentKey)
            If Not oDownloadValue Is Nothing Then
                Me.BusinessContentManager.Update(oContentValue.Key, "Qty", (oContentValue.Qty + 1).ToString())
                'Me.BusinessMasterPageManager.Trace(oContentValue.Key.Id, 5, oDownloadValue.DownloadTypeCollection(0).FileName.Substring(oDownloadValue.DownloadTypeCollection(0).FileName.IndexOf(".") + 1))
                DownloadManager.TakeDownload(oDownloadValue, oDownloadValue.Title)
                
            End If
        End If
    End Sub
   
 
       
</script>

                 <div class="sideR" id="divDwlRelated" runat="server" visible="false">
                	<div class="downloads">
                		<h4><%= Me.BusinessDictionaryManager.Read("PLY_Title_DownloadRight", _langId, "PLY_Title_DownloadRight")%></h4>
                        <ul>
                            <asp:Literal ID="TxtListDownload" runat="server" Text=""></asp:Literal>                        	
                        </ul>
                    </div>
                </div><!--end sideR-->
                
 <script type="text/javascript">
     <% If (divDwlRelated.Visible = False) Then%>
     $(document).ready(function () {
         
         $('#cnt_boxlancio').css("width", "100%");
     });
     <% end if%>
     </script>
  
    
  

