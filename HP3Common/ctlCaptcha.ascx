<%@ Control Language="VB" ClassName="ctlCaptcha" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="System.Drawing.Text" %>

<script runat="server">
    Private _width As String = "100"
    Private _height As String = "40"
    Private _code As String = ""
    Private _foreColor As Color
    Private _backColor As Color
    Private _textForeColor As Color
    Private _textBackColor As Color
    
    Public Property Width() As String
        Get
            Return _width
        End Get
        Set(ByVal value As String)
            _width = value
        End Set
    End Property
    
    Public Property Height() As String
        Get
            Return _height
        End Get
        Set(ByVal value As String)
            _height = value
        End Set
    End Property
    
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property
    
    Public Property ForeColor() As Color
        Get
            Return _foreColor
        End Get
        Set(ByVal value As Color)
            _foreColor = value
        End Set
    End Property
    
    Public Property BackColor() As Color
        Get
            Return _backColor
        End Get
        Set(ByVal value As Color)
            _backColor = value
        End Set
    End Property
    
    Public Property TextForeColor() As Color
        Get
            Return _textForeColor
        End Get
        Set(ByVal value As Color)
            _textForeColor = value
        End Set
    End Property
    
    Public Property TextBackColor() As Color
        Get
            Return _textBackColor
        End Get
        Set(ByVal value As Color)
            _textBackColor = value
        End Set
    End Property
    
    Sub page_init()
        CaptchaCodeHidden.Text = Code
        CaptchaCodeHidden.Visible = False
    End Sub
    
</script>
<asp:TextBox ID="CaptchaCodeHidden" Visible="false" runat="server" />
<img src="/HP3Common/captcha.aspx?c=<%=Code%>&w=<%=Width%>&h=<%=Height%>&fc=<%=ForeColor.ToArgb%>&bc=<%=BackColor.ToArgb%>&tfc=<%=TextForeColor.ToArgb%>&tbc=<%=TextBackColor.ToArgb%>" alt="" />