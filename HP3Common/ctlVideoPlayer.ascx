<%@ Control Language="VB" ClassName="VideoPlayer" %>

<script runat="server">
    Private _listControl As ListControlEnum = ListControlEnum.Standard
    Private _sourcePath As String
    Private _buffer As Integer = 5
    Private _modalBuffer As ModalBufferEnum = ModalBufferEnum.Active
    Private _autoPlay As AutoPlayEnum = AutoPlayEnum.Active
    Private _autoRewind As AutoRewindEnum = AutoRewindEnum.Active
    Private _keepRatio As KeepRatioEnum = KeepRatioEnum.Yes
    Private _flashMenu As Boolean = False
    Private _flashAllowFullScreen As Boolean = True
    Private _flashBgColor As String = "#000000"
    Private _idControl As String = "myVideo"
    Private _width As String = "425"
    Private _height As String = "355"
    
    Public Enum ListControlEnum
        Standard = 1
        NoFullScreen = 2
    End Enum
    
    Public Enum ModalBufferEnum
        Active = 1
        Disabled = 0
    End Enum
    
    Public Enum AutoPlayEnum
        Active = 1
        Disabled = 0
    End Enum
    
    Public Enum AutoRewindEnum
        Active = 1
        Disabled = 0
    End Enum
    
    Public Enum KeepRatioEnum
        Yes = 1
        No = 0
    End Enum
    
    ' List control views on player bar
    Public Property HP3ListControl() As ListControlEnum
        Get
            Return _listControl
        End Get
        Set(ByVal value As ListControlEnum)
            _listControl = value
        End Set
    End Property
    
    '  virtual path of the file video
    Public Property HP3SourcePath() As String
        Get
            Return _sourcePath
        End Get
        Set(ByVal value As String)
            _sourcePath = value
        End Set
    End Property
    
    '  buffer
    Public Property HP3Buffer() As Integer
        Get
            Return _buffer
        End Get
        Set(ByVal value As Integer)
            _buffer = value
        End Set
    End Property
    
    '  modal buffer
    Public Property HP3ModalBuffer() As ModalBufferEnum
        Get
            Return _modalBuffer
        End Get
        Set(ByVal value As ModalBufferEnum)
            _modalBuffer = value
        End Set
    End Property
    
    '  autoplay
    Public Property HP3AutoPlay() As AutoPlayEnum
        Get
            Return _autoPlay
        End Get
        Set(ByVal value As AutoPlayEnum)
            _autoPlay = value
        End Set
    End Property
    
    '  autorewind
    Public Property HP3AutoRewind() As AutoRewindEnum
        Get
            Return _autoRewind
        End Get
        Set(ByVal value As AutoRewindEnum)
            _autoRewind = value
        End Set
    End Property
    
    '  keep ratio
    Public Property HP3KeepRatio() As KeepRatioEnum
        Get
            Return _keepRatio
        End Get
        Set(ByVal value As KeepRatioEnum)
            _keepRatio = value
        End Set
    End Property
    
    ' flash men�
    Public Property HP3FlashMenu() As Boolean
        Get
            Return _flashMenu
        End Get
        Set(ByVal value As Boolean)
            _flashMenu = value
        End Set
    End Property
    
    '  Allow Fullscreen
    Public Property HP3FlashAllowFullScreen() As Boolean
        Get
            Return _flashAllowFullScreen
        End Get
        Set(ByVal value As Boolean)
            _flashAllowFullScreen = value
        End Set
    End Property
    
    '  Allow Fullscreen
    Public Property HP3FlashBgColor() As String
        Get
            Return _flashBgColor
        End Get
        Set(ByVal value As String)
            _flashBgColor = value
        End Set
    End Property
    
    '  ID Control
    Public Property HP3IDControl() As String
        Get
            Return _IDControl
        End Get
        Set(ByVal value As String)
            _IDControl = value
        End Set
    End Property
        
    '  Width
    Public Property HP3Width() As String
        Get
            Return _width
        End Get
        Set(ByVal value As String)
            _width = value
        End Set
    End Property
    
    '  Height
    Public Property HP3Height() As String
        Get
            Return _height
        End Get
        Set(ByVal value As String)
            _height = value
        End Set
    End Property
    
    Function GetListControl() As String
        Select Case HP3ListControl
            Case ListControlEnum.Standard
                Return "PlayPauseButton|SeekBar|VolumeBar|fullScreen"
            Case ListControlEnum.NoFullScreen
                Return "PlayPauseButton|SeekBar|VolumeBar"
        End Select
        Return "PlayPauseButton|SeekBar|VolumeBar|fullScreen"
    End Function
    
    Function GetModalBuffer() As String
        Select Case HP3ModalBuffer
            Case ModalBufferEnum.Active
                Return "1"
            Case ModalBufferEnum.Disabled
                Return ""
        End Select
        Return "1"
    End Function
    
    Function GetAutoPlay() As String
        Select Case HP3AutoPlay
            Case AutoPlayEnum.Active
                Return "1"
            Case AutoPlayEnum.Disabled
                Return ""
        End Select
        Return "1"
    End Function
    
    Function GetAutoRewind() As String
        Select Case HP3AutoRewind
            Case AutoRewindEnum.Active
                Return "1"
            Case AutoRewindEnum.Disabled
                Return ""
        End Select
        Return "1"
    End Function
    
    Function GetKeepRatio() As String
        Select Case HP3KeepRatio
            Case KeepRatioEnum.Yes
                Return "1"
            Case KeepRatioEnum.No
                Return ""
        End Select
        Return "1"
    End Function
    
    Function GetFlashMenu() As String
        Select Case HP3FlashMenu
            Case False
                Return "False"
            Case True
                Return "True"
        End Select
        Return "False"
    End Function
    
    Function GetFlashAllowFullScreen() As String
        Select Case HP3FlashAllowFullScreen
            Case False
                Return "False"
            Case True
                Return "True"
        End Select
        Return "True"
    End Function
    
</script>
<div id="myVideo"></div>
<script type="text/javascript" language="javascript" src="/Js/swfobject.js"></script>
<script language="javascript" type="text/javascript">

var flashvars={};
	  
//I controlli da visualizzare nel player
//vengono messi a video nell'orine in cui compiano 
//il testo fullScreen nel caso in cui il player non supporti questa features non vine renderizzato
flashvars.controllList="<%=GetListControl()%>";
//******************************

//Il path del file flv (fare riferimento all'url della pagina che contiene il proiettore presumibilmente la master page del sito in HP3)
flashvars._contentPath="<%=HP3SourcePath()%>";
//******************************

//La grendezza del buffer in sec. "" =0
flashvars._buffer="<%=HP3Buffer()%>";
//******************************


//Indica se durante il buffrening i controlli 	video devono essre attivi o meno
// 1 disattiva durante il buffrering
// '' li lascia attivi
flashvars.modalBuffer="<%=GetModalBuffer()%>";
//******************************

//Se diverso da vuoto stampa alcune info sul video che possono
//facilitare le operazioni di debug da finalizzare! e usare solo in caso di
//necessit�, disattivare sempre in produzione
flashvars.debug="";
//******************************

//Indica al player se far partire in automatico il video aspettare che l'utente clicchi
// "1" ""
flashvars._autoplay="<%=GetAutoPlay() %>";
//******************************

//Indica l'eventuale immagine di preview
//se _autoplay="1" e viene specificato anche un imagine di copertina
//in automatico il palyer ignore la diraettiva autoplay
//flashvars.tumb=""
//******************************

//Indica al player se riavvolgere il movie (tornare all'inizio) 
// alla fine della riproduzione o o no 
// "1" ""
flashvars.autorewind="<%=GetAutoRewind() %>";
//******************************

//Indica al player se nelle operazioni di scaling deve preservare o meno 
// il  rapporto di aspetto del filmato originale
// "1" ""
flashvars.keepRatio="<%=GetKeepRatio() %>";
//******************************

var params={};
params.menu="<%=GetFlashMenu()%>";
params.allowFullScreen="<%=GetFlashAllowFullScreen()%>";
params.bgColor="<%=HP3FlashBgColor()%>";

swfobject.embedSWF("/HP3Media/Object/VideoPlayer.swf", "<%=HP3IDControl()%>",  "<%=HP3Width()%>",  "<%=HP3Height()%>", "8.0.0", "expressInstall.swf", flashvars, params)
</script>