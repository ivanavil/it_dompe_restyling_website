<%@ Page Language="VB" ClassName="myCaptcha"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="System.Drawing" %>
<script runat="server">
    Sub page_init()
        Dim c As String = ImageUtility.DecryptCaptchaCode(System.Web.HttpUtility.UrlEncode(Request("c")))
        Dim w As String = Request("w")
        Dim h As String = Request("h")
        Dim fc As Color = Color.FromArgb(Request("fc"))
        Dim bc As Color = Color.FromArgb(Request("bc"))
        Dim tfc As Color = Color.FromArgb(Request("tfc"))
        Dim tbc As Color = Color.FromArgb(Request("tbc"))
        
        ImageUtility.GetCaptcha(w, h, c, fc, bc, tfc, tbc)
    End Sub
</script>