﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Text
Imports System.Web
Imports Yahoo.Yui.Compressor

Namespace Healthware.HP3.Handlers
    Public Class YUICompressor
        Implements IHttpHandler

        Private Const DEFAULT_EXPIRE_OFFSET As Integer = 7 ' Days
        Private Const CACHE_DURATION As Integer = 30 ' Minutes

        Private useCache As Boolean = False
        Private useCompression As Boolean = False

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return True
            End Get
        End Property

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            Boolean.TryParse(ConfigurationManager.AppSettings.Get("yuiCacheEnabled"), Me.useCache)
            Boolean.TryParse(ConfigurationManager.AppSettings.Get("yuiCompressionEnabled"), Me.useCompression)
            context.Response.ContentType = "text/plain"
            SetCache()
            Dim filePath As String = GetFilePath()
            Dim fileExtension As String = Path.GetExtension(filePath)
            If File.Exists(filePath) Then
                context.Response.AddHeader("Content-Disposition", "filename=" & Path.GetFileName(filePath))
                Select Case fileExtension
                    Case ".css"
                        context.Response.ContentType = "text/css"
                        If Me.useCompression Then
                            CompressCSS(filePath)
                        Else
                            HttpContext.Current.Response.WriteFile(filePath)
                        End If
                        Exit Select
                    Case ".js"
                        context.Response.ContentType = "application/x-javascript"
                        If Me.useCompression Then
                            CompressJavaScript(filePath)
                        Else
                            HttpContext.Current.Response.WriteFile(filePath)
                        End If
                        Exit Select
                    Case Else
                        EmptyFile()
                        Exit Select
                End Select
            Else
                EmptyFile()
            End If
            'context.Response.Flush()
            context.Response.End()
        End Sub

        Private Sub SetCache()
            HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddDays(DEFAULT_EXPIRE_OFFSET))
        End Sub

        Private Function GetFilePath() As String
            Dim filePath As String = HttpContext.Current.Request.Url.AbsolutePath
            filePath = filePath.TrimEnd(".axd".ToCharArray())
            filePath = HttpContext.Current.Server.MapPath(filePath)
            Return filePath
        End Function

        Private Sub EmptyFile()
            HttpContext.Current.Response.Write(String.Empty)
        End Sub

        Private Sub CompressCSS(ByVal filePath As String)
            Dim current As HttpContext = HttpContext.Current
            Dim requestHash As String = current.Request.Url.GetLeftPart(UriPartial.Path)
            If Me.useCache And current.Cache(requestHash) IsNot Nothing Then
                current.Response.Write(DirectCast(current.Cache(requestHash), String))
                Exit Sub
            End If
            Dim fileLock As New Object()
            SyncLock fileLock
                Dim sr As New StreamReader(filePath, True)
                Dim compressed As String = CssCompressor.Compress(sr.ReadToEnd())
                current.Response.Write(compressed)
                If Me.useCache Then
                    current.Cache.Add(requestHash, compressed, Nothing, DateTime.MaxValue, New TimeSpan(0, CACHE_DURATION, 0), System.Web.Caching.CacheItemPriority.Normal, Nothing)
                End If
                sr.Close()
            End SyncLock
        End Sub

        Private Sub CompressJavaScript(ByVal filePath As String)
            Dim current As HttpContext = HttpContext.Current
            Dim requestHash As String = current.Request.Url.GetLeftPart(UriPartial.Path)
            If Me.useCache And current.Cache(requestHash) IsNot Nothing Then
                current.Response.Write(DirectCast(current.Cache(requestHash), String))
                Exit Sub
            End If
            Dim fileLock As New Object()
            SyncLock fileLock
                Dim sr As New StreamReader(filePath, True)
                Dim compressed As String = JavaScriptCompressor.Compress(sr.ReadToEnd())
                current.Response.Write(compressed)
                If Me.useCache Then
                    current.Cache.Add(requestHash, compressed, Nothing, DateTime.MaxValue, New TimeSpan(0, CACHE_DURATION, 0), System.Web.Caching.CacheItemPriority.Normal, Nothing)
                End If
                sr.Close()
            End SyncLock
        End Sub
    End Class
End Namespace
