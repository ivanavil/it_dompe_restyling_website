Imports Microsoft.VisualBasic
Imports System.Linq
Imports System.Xml
Imports System.Xml.Linq

Imports Healthware.HP3.Core.Base
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.Utility.ObjectValues
Imports Healthware.HP3.Core.Web
Imports Healthware.HP3.Core.Web.ObjectValues

Namespace Healthware.Dompe.Helper301
	Public Class Helper301
		Dim _htUrlToBeRedirected as Hashtable = Nothing
		
		Public Sub New()
            getHashTable()
        End Sub
	
		Private Sub getHashTable()
			Dim _cacheKey as String = "CacheKey:301HashTable"
			_htUrlToBeRedirected = Cachemanager.Read(_cacheKey)
			
			If _htUrlToBeRedirected is Nothing OrElse _htUrlToBeRedirected.Count = 0 Then
				_htUrlToBeRedirected = populateUrlsList()
				CacheManager.Insert(_cacheKey, _htUrlToBeRedirected, 1, 600)
			End If
		End Sub
		
		Private function populateUrlsList() as Hashtable
			Dim _ht as Hashtable = New Hashtable

			'Get List from Xml File------------------------
			Dim reader As XmlReader = getListFromXml()
			While (reader.Read())
				If (reader.Name = "urlToBeRedirected") Then
					_ht.Add(reader.GetAttribute("from").ToString, reader.GetAttribute("to").ToString)
				End If
			End While
			'------------------------------------------------
			
			Return _ht
		End Function
		
        Public Function getListFromXml() As XmlReader
            Dim settings As New XmlReaderSettings()
            settings.IgnoreComments = True
            settings.IgnoreWhitespace = True

            Return XmlReader.Create(HttpContext.Current.Server.MapPath("/App_Data/301Redirects2.xml"), settings)
        End Function		
		
		Public Sub is301Url()
			Dim _curUrl as String = HttpContext.Current.Request.Url.ToString
			If _htUrlToBeRedirected(_curUrl) is Nothing Then 
				Exit Sub
			Else
				HttpContext.Current.Response.Clear()
				HttpContext.Current.Response.Status = "301 Moved Permanently"
				HttpContext.Current.Response.AddHeader("Location", _htUrlToBeRedirected(_curUrl))
				HttpContext.Current.Response.End()
			End If
		End Sub
	End Class
End Namespace