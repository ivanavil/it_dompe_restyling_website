﻿Imports Microsoft.VisualBasic
Namespace Healthware.DMP.Models

    Public Class PagedCollection(Of T)
#Region "Members"

        Private _pageNumber As Integer = 0
        Private _pageTotalNumber As Integer = 0
        Private _collection As IEnumerable(Of T) = Nothing
        Private _collectionCount As Integer = 0

#End Region

#Region "Properties"

        Public Property Collection() As IEnumerable(Of T)
            Get
                Return _collection
            End Get

            Set(ByVal Value As IEnumerable(Of T))
                _collection = Value
            End Set
        End Property

        Public Property PageNumber() As Integer
            Get
                Return _pageNumber
            End Get

            Set(ByVal Value As Integer)
                _pageNumber = Value
            End Set
        End Property

        Public Property PageTotalNumber() As Integer
            Get
                Return _pageTotalNumber
            End Get

            Set(ByVal Value As Integer)
                _pageTotalNumber = Value
            End Set
        End Property

        Public Property CollectionCount() As Integer
            Get
                Return _collectionCount
            End Get

            Set(ByVal Value As Integer)
                _collectionCount = Value
            End Set
        End Property

#End Region
    End Class

End Namespace

