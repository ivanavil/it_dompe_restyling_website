﻿Imports Microsoft.VisualBasic

Namespace Healthware.DMP.Models

    Public Class GenericSearcherModel

        Private _primarykey As String = String.Empty
        Private _siteareId As Integer = 0
        Private _contenTypeId As Integer = 0
        Private _siteId As Integer = 0
        Private _year As String = String.Empty
        Private _languageId As Integer = 0
        Private _contextId As Integer = 0
        Private _bitMask As String = String.Empty
        Private _brand As String = String.Empty
        Private _Word As String = String.Empty
        Private _tags As String = String.Empty
        Private _display As Boolean = True
        Private _delete As Boolean = False
        Private _active As Boolean = True
        Private _approvalStatus As Integer = 0
        Private _pageNumber As Integer = 0
        Private _pageSize As Integer = 0
        Private _ThemeID As Integer = 0
        Private _keysToExclude As String = String.Empty
        Private _keysSiteAreas As String = String.Empty
        Private _userId As String = String.Empty


        Public Property PageNumber As Integer
            Get
                Return _pageNumber
            End Get
            Set(value As Integer)
                _pageNumber = value
            End Set
        End Property

        Public Property PageSize As Integer
            Get
                Return _pageSize
            End Get
            Set(value As Integer)
                _pageSize = value
            End Set
        End Property

        Public Property year As String
            Get
                Return _year
            End Get
            Set(value As String)
                _year = value
            End Set
        End Property

        Public Property Primarykey As String
            Get
                Return _primarykey
            End Get
            Set(value As String)
                _primarykey = value
            End Set
        End Property

        Public Property Word As String
            Get
                Return _Word
            End Get
            Set(value As String)
                _Word = value
            End Set
        End Property

        Public Property SiteareId As Integer
            Get
                Return _siteareId
            End Get
            Set(value As Integer)
                _siteareId = value
            End Set
        End Property

        Public Property ContenTypeId As Integer
            Get
                Return _contenTypeId
            End Get
            Set(value As Integer)
                _contenTypeId = value
            End Set
        End Property


        Public Property ThemeID As Integer
            Get
                Return _ThemeID
            End Get
            Set(ByVal value As Integer)
                _ThemeID = value
            End Set
        End Property

        Public Property SiteId As Integer
            Get
                Return _siteId
            End Get
            Set(value As Integer)
                _siteId = value
            End Set
        End Property

        Public Property LanguageId As Integer
            Get
                Return _languageId
            End Get
            Set(value As Integer)
                _languageId = value
            End Set
        End Property

        Public Property BitMask As String
            Get
                Return _bitMask
            End Get
            Set(value As String)
                _bitMask = value
            End Set
        End Property

        Public Property Brand As String
            Get
                Return _brand
            End Get
            Set(value As String)
                _brand = value
            End Set
        End Property

        Public Property Tags As String
            Get
                Return _tags
            End Get
            Set(value As String)
                _tags = value
            End Set
        End Property

        Public Property Display As Boolean
            Get
                Return _display
            End Get
            Set(value As Boolean)
                _display = value
            End Set
        End Property

        Public Property Active As Boolean
            Get
                Return _active
            End Get
            Set(value As Boolean)
                _active = value
            End Set
        End Property

        Public Property ApprovalStatus As Integer
            Get
                Return _approvalStatus
            End Get
            Set(value As Integer)
                _approvalStatus = value
            End Set
        End Property

        Public Property Delete As Boolean
            Get
                Return _delete
            End Get
            Set(value As Boolean)
                _delete = value
            End Set
        End Property

        Public Property KeysToExclude As String
            Get
                Return _keysToExclude
            End Get
            Set(value As String)
                _keysToExclude = value
            End Set
        End Property

        Public Property KeysSiteAreas As String
            Get
                Return _keysSiteAreas
            End Get
            Set(value As String)
                _keysSiteAreas = value
            End Set
        End Property

        Public Property UserId As String
            Get
                Return _userId
            End Get
            Set(value As String)
                _userId = value
            End Set
        End Property

        Sub New()

            If SiteId = 0 Then SiteId = 46 ' Healthware.HSP.Keys.SiteAreaKeys.HelsinnServicePortal.Id
            If LanguageId = 0 Then LanguageId = 1 'Healthware.HSP.Keys.LanguageKeys.English.Id

        End Sub

    End Class

End Namespace
