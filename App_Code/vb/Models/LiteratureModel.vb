﻿Imports Microsoft.VisualBasic
Imports Healthware.HP3.Core.Content.ObjectValues

Namespace Healthware.DMP.Models

    Public Class LiteratureModel

        Private _key As ContentIdentificator = Nothing
        Private _title As String = String.Empty
        Private _launch As String = String.Empty
        Private _fullText As String = String.Empty
        Private _cover As String = String.Empty
        Private _cover2 As String = String.Empty
        Private _link As String = String.Empty
        Private _authors As AuthorCollection = Nothing
        Private _datePublish As String = String.Empty
        Private _LinkDownload As String = String.Empty
        Private _DateExpiry As String = String.Empty
        Private _linkLabel As String = String.Empty
        Private _Code As String = String.Empty
        Private _Copyright As String = String.Empty
        Private _LinkSticker As String = String.Empty
        Private _BookReference As String = String.Empty
        Private _Comment As String = String.Empty
        Private _LabelDownload As String = String.Empty
        Private _day As String = String.Empty
        Private _month As String = String.Empty
        Private _year As String = String.Empty
        Private _linkexternal As String = String.Empty
        Private _hideDateDay As Boolean = False
        Private _rank As Boolean = False


        Public Property LabelDownload As String
            Get
                Return _LabelDownload
            End Get
            Set(value As String)
                _LabelDownload = value
            End Set
        End Property

        Public Property Comment As String
            Get
                Return _Comment
            End Get
            Set(value As String)
                _Comment = value
            End Set
        End Property

        Public Property BookReference As String
            Get
                Return _BookReference
            End Get
            Set(value As String)
                _BookReference = value
            End Set
        End Property

        Public Property Key As ContentIdentificator
            Get
                Return _key
            End Get
            Set(value As ContentIdentificator)
                _key = value
            End Set
        End Property

        Public Property LinkDownload As String
            Get
                Return _LinkDownload
            End Get
            Set(value As String)
                _LinkDownload = value
            End Set
        End Property

        Public Property Title As String
            Get
                Return _title
            End Get
            Set(value As String)
                _title = value
            End Set
        End Property

        Public Property Launch As String
            Get
                Return _launch
            End Get
            Set(value As String)
                _launch = value
            End Set
        End Property

        Public Property FullText As String
            Get
                Return _fullText
            End Get
            Set(value As String)
                _fullText = value
            End Set
        End Property

        Public Property Cover As String
            Get
                Return _cover
            End Get
            Set(value As String)
                _cover = value
            End Set
        End Property

        Public Property Cover2 As String
            Get
                Return _cover2
            End Get
            Set(value As String)
                _cover2 = value
            End Set
        End Property

        Public Property Link As String
            Get
                Return _link
            End Get
            Set(value As String)
                _link = value
            End Set
        End Property

        Public Property Authors As AuthorCollection
            Get
                Return _authors
            End Get
            Set(value As AuthorCollection)
                _authors = value
            End Set
        End Property

        Public Property DatePublish As String
            Get
                Return _datePublish
            End Get
            Set(value As String)
                _datePublish = value
            End Set
        End Property

        Public Property DateExpiry As String
            Get
                Return _DateExpiry
            End Get
            Set(ByVal value As String)
                _DateExpiry = value
            End Set
        End Property

        Public Property LinkLabel As String
            Get
                Return _linkLabel
            End Get
            Set(ByVal value As String)
                _linkLabel = value
            End Set
        End Property

        Public Property Code As String
            Get
                Return _Code
            End Get
            Set(ByVal value As String)
                _Code = value
            End Set
        End Property

        Public Property Copyright As String
            Get
                Return _Copyright
            End Get
            Set(ByVal value As String)
                _Copyright = value
            End Set
        End Property

        Public Property LinkSticker As String
            Get
                Return _LinkSticker
            End Get
            Set(ByVal value As String)
                _LinkSticker = value
            End Set
        End Property

        Public Property Day As String
            Get
                Return _day
            End Get
            Set(value As String)
                _day = value
            End Set
        End Property

        Public Property Month As String
            Get
                Return _month
            End Get
            Set(value As String)
                _month = value
            End Set         
        End Property

            Public Property Year As String
            Get
                Return _year
            End Get
            Set(value As String)
                _year = value
            End Set
        End Property

        Public Property Linkexternal As String
            Get
                Return _linkexternal
            End Get
            Set(value As String)
                _linkexternal = value
            End Set
        End Property

        Public Property HideDateDay As Boolean
            Get
                Return _hideDateDay
            End Get
            Set(value As Boolean)
                _hideDateDay = value
            End Set
        End Property


        Public Property Rank As Boolean
            Get
                Return _rank
            End Get
            Set(value As Boolean)
                _rank = value
            End Set
        End Property

        Sub New()
            Key = New ContentIdentificator()
        End Sub

    End Class



    Public Class LiteratureModelCollection
        Inherits List(Of LiteratureModel)

        Private _PagerTotalNumber As Integer = 0


        Public Property PagerTotalNumber As Integer
            Get
                Return _PagerTotalNumber
            End Get
            Set(value As Integer)
                _PagerTotalNumber = value
            End Set
        End Property


    End Class

End Namespace
