﻿Imports Microsoft.VisualBasic
Namespace Healthware.DMP.Models

    Public Class OperationResult(Of T)
        Private _success As Boolean = False
        Private _statusCode As Healthware.DMP.Keys.EnumeratorKeys.AsyncStatusCode = Keys.EnumeratorKeys.AsyncStatusCode.NoItem
        Private _message As String = String.Empty
        Private _result As T

        Public Property Success() As Boolean
            Get
                Return _success
            End Get

            Set(ByVal Value As Boolean)
                _success = Value
            End Set
        End Property

        Public Property StatusCode() As Healthware.DMP.Keys.EnumeratorKeys.AsyncStatusCode
            Get
                Return _statusCode
            End Get

            Set(ByVal Value As Healthware.DMP.Keys.EnumeratorKeys.AsyncStatusCode)
                _statusCode = Value
            End Set
        End Property

        Public Property Message() As String
            Get
                Return _message
            End Get

            Set(ByVal Value As String)
                _message = Value
            End Set
        End Property

        Public Property Result() As T
            Get
                Return _result
            End Get

            Set(ByVal Value As T)
                _result = Value
            End Set
        End Property

    End Class

End Namespace

