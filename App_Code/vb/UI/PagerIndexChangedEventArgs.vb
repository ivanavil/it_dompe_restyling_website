﻿Imports Microsoft.VisualBasic

Public Class PagerIndexChangedEventArgs
    Inherits EventArgs

    Private _newPageIndex As Integer

    Public Property NewPageIndex() As Integer
        Get
            Return _newPageIndex
        End Get
        Set(ByVal value As Integer)
            _newPageIndex = value
        End Set
    End Property

    Public Sub New(ByVal pageIndex As Integer)
        _newPageIndex = pageIndex
    End Sub
End Class
