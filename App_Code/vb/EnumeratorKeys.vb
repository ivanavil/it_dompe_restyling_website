﻿Imports Microsoft.VisualBasic


Namespace Healthware.DMP.Keys

    Public Class EnumeratorKeys
        'facciamo riferimento alla tabella PP_User_Account_Status
        Public Enum statusUser
            Active = 1
            Pending = 2
            Disable = 4 'come il cancellato
            moreErrorLoginBlocked = 6
        End Enum

        Public Enum typeErrorLogin
            UserNotFound = 1
            UserBlocked = 2
            WrongPassword = 3
            UserPending = 4
        End Enum

        Public Enum typeAlertModal
            ThreeCharsSearch = 1
            SaveCorrectly = 2
            GenericError = 3
        End Enum

        Public Enum ProfileOperations
            Read = 1
            Write = 2
            EmailCheck = 3
            Cover = 4
        End Enum

        Public Enum AsyncStatus
            Succeded = 1
            Failed = -1
        End Enum
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Public Enum AsyncStatusCode
            FillItem = 1
            NoItem = 2
            SessionTimeout = 3
            AlreadyExists = 4
        End Enum

        Public Enum LiteratureViewType
            List = 1
            Detail = 2
            FeaturedBox = 3
            Search = 4
            ViewSliderPhotoGallery = 5
            ListMobile = 6
        End Enum

        Public Enum FoldersOperationType
            List = 1
            Write = 2
            AddPermission = 3
            Update = 4
            Delete = 5
            UpdatePermission = 6
        End Enum

        Public Enum LiteratureAuthorType
            Author = 1
        End Enum

        Public Enum ElearningAuthorType
            Author = 1
        End Enum

        Public Enum ContentRelationType
            ContentDownload = 4
            UsefulLinks = 5
            RelatedMedia = 6
            CoinsDownload = 7
            Lessons = 8
            Company = 9
            DMS = 10
        End Enum

        Public Enum DownloadType
            Download = 1
            Document = 2
        End Enum

        Public Enum EventsViewType
            List = 1
            Detail = 2
            EventsBox = 3
            SliderEventsBox = 4
        End Enum

        Public Enum EventsFilterType
            Upcoming = 1
            Expired = 2
            ForSelectedDate = 3
            All = 4 'Non considera i filtri per data
        End Enum

        Public Enum SearchViewType
            List = 1
            Detail = 2
        End Enum


        Public Enum TagOperationType
            List = 1
            Create = 2
            ContentTags = 3
        End Enum

        Public Enum GenericOperationType
            RelatedContent = 1
            List = 2
            SliderHomePageBox = 3
        End Enum

        Public Enum ElearningViewType
            ListCourses = 1
            DetailCourse = 2
            ListLessons = 3
            DetailLesson = 4
            FeaturedBox = 5
            AskToAuthor = 6
        End Enum

        Public Enum ElearningCourseStatus
            OnGoing = 1
            TestEnabled = 2
            Completed = 3
        End Enum

        Public Enum ElearningLessonStatus
            OnGoing = 1
            TestEnabled = 2
            Completed = 3
        End Enum

        Public Enum ElearningUserStatus
            Course = 1
            Lesson = 2
        End Enum

        Public Enum PdfGeneratorType
            ElearningCertificate = 1
        End Enum

        Public Enum UserViewType
            ProfilePage = 1
            Header = 2
            Create = 3
            Edit = 4
        End Enum

        Public Enum DMSOperationType
            CreateDocument = 1
            UploadFile = 2
            ReadDocuments = 3
            EditDocument = 4
            DeleteDocument = 5
            CheckInDocument = 6
            CheckOutDocument = 7
            AddVersion = 8
            Link = 9
            Move = 10
        End Enum

        Public Enum StudySummaryType
            Minimal = 1
            Extended = 2
            DeleteStudy = 3
        End Enum

        Public Enum GeoType
            Country = 2
        End Enum
    End Class

    Public Class Role
        Public Const RoleMediaPress As String = "63"
        Public Const RoleDoctor As String = "64"
        Public Const RolePartner As String = "65"

    End Class
    Public Class ContentTypeConstants
        Public Class Domain
            Public Const DMP As String = "DMP"
        End Class

        Public Class Id
            Public Const GenericContCType As Integer = 128
            Public Const GenericContCTypeMediaPress As Integer = 137
            Public Const DashboardContCTypeMediaPress As Integer = 139
            Public Const DMPDownload As Integer = 135
            Public Const ContactContType As Integer = 129
            Public Const HeaderCType As Integer = 130
            Public Const FooterCtype As Integer = 131
            Public Const ArchivioCtype As Integer = 132
            Public Const RicercaCtype As Integer = 136
            Public Const ProductCtype As Integer = 140
            Public Const ManagementCtype As Integer = 148
            Public Const Download As Integer = 15
            Public Const ProcessoSelezioneCtype As Integer = 150
            Public Const PosizioniAperteCtype As Integer = 133
            Public Const PressArchivio As Integer = 138
            Public Const PressDashboard As Integer = 139
            Public Const PressHome As Integer = 141
            Public Const PressPhotoGallery As Integer = 142
            Public Const PressDetail As Integer = 143
            Public Const PressContatti As Integer = 146
            Public Const IDProgettiIniziative As Integer = 149
            Public Const IDMedicalDashbordCtype As Integer = 151
            Public Const IDMedicalArchivioRCPCtype As Integer = 152
            Public Const IDMedicalBrandCtype As Integer = 153
            Public Const IDMedicalFocusOnCtype As Integer = 154
            Public Const IDMedicalInfoUtiliCtype As Integer = 155
            Public Const IDMedicalPhotogalleryCtype As Integer = 142
        End Class
    End Class
End Namespace
