﻿Imports Microsoft.VisualBasic
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.DMP

Namespace Healthware.DMP.BL

    Public Class LiteratureManager


        Public Function GetArchive(ByVal so As ContentExtraSearcher) As ContentExtraCollection

            If so Is Nothing Then Return Nothing

            '  Healthware.HSP.Helper.Cache.insertKeyinGroupLiteratureCache(Healthware.HSP.Helper.Cache.getKeyBySearcher(so))

            Dim manager As New ContentExtraManager()
            Return manager.Read(so)

        End Function

        Public Function GetRicerca(ByVal so As ContentSearcher) As ContentCollection
            If so Is Nothing Then Return Nothing

            Dim manager As New ContentManager()
            Return manager.Read(so)

        End Function

        Public Function GetDetail(ByVal so As ContentExtraSearcher) As ContentExtraValue

            If so Is Nothing Then Return Nothing

            '  Healthware.HSP.Helper.Cache.insertKeyinGroupLiteratureCache(Healthware.HSP.Helper.Cache.getKeyBySearcher(so))

            Dim manager As New ContentExtraManager()
            Dim coll = manager.Read(so)
            If coll Is Nothing OrElse Not coll.Any() Then Return Nothing

            Return coll(0)

        End Function

        'da usare solo per letture dirette, senza filtri suls earcher
        Public Function GetDetail(ByVal so As ContentBaseSearcher) As ContentBaseValue

            If so Is Nothing Then Return Nothing

            '  Healthware.HSP.Helper.Cache.insertKeyinGroupLiteratureCache(Healthware.HSP.Helper.Cache.getKeyBySearcher(so))

            Dim manager As New ContentManager()
            Return manager.GetContentBaseValue(so)

        End Function

        Public Function GetTags(ByVal key As ContentIdentificator) As IEnumerable(Of ContextValue)

            '   Return Healthware.HSP.Helper.GenericHelper.GetRelationalContext(key, Keys.ContextKeys.groupContextTags)

        End Function


        Public Function GetAuthors(ByVal so As ContentAuthorSearcher) As AuthorCollection
            If so Is Nothing Then Return Nothing

            '   Healthware.HSP.Helper.Cache.insertKeyinGroupLiteratureCache(Healthware.HSP.Helper.Cache.getKeyBySearcher(so))

            Dim manager As New ContentAuthorManager()
            Return manager.Read(so)

        End Function


    End Class

End Namespace
