﻿Imports Microsoft.VisualBasic
Imports System.Web.Script.Serialization
Imports System.IO
Imports Healthware.DMP.Keys
Imports Healthware.DMP.Models
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Base.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Web.ObjectValues
Imports Healthware.HP3.Core.User.ObjectValues
Imports Healthware.HP3.Core.User
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Dompe
Imports Healthware.HP3.LMS.Search.SearchEngine


Namespace Healthware.DMP.BL

    Public Class Literature : Implements IHttpHandler

        Protected _op As Integer = 0

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

            Dim retVal As Object = Nothing


            If context.Request.HttpMethod = "POST" Then
                Try
                    If (Integer.TryParse(context.Request("op"), _op)) Then
                        Select Case CType(_op, Keys.EnumeratorKeys.LiteratureViewType)

                            Case CInt(EnumeratorKeys.LiteratureViewType.List)
                                retVal = ReadLiterature(context)

                            Case CInt(EnumeratorKeys.LiteratureViewType.ListMobile)
                                retVal = ReadLiterature(context)

                            Case CInt(EnumeratorKeys.LiteratureViewType.Detail)

                                'nothing 

                            Case CInt(EnumeratorKeys.LiteratureViewType.FeaturedBox)

                                retVal = ReadLiterature(context)

                            Case Keys.EnumeratorKeys.LiteratureViewType.Search

                                retVal = ReadSearch(context)

                            Case Keys.EnumeratorKeys.LiteratureViewType.ViewSliderPhotoGallery

                                retVal = ReadPhotoGallery(context)

                        End Select

                    End If

                Catch ex As Exception
                    ' Healthware.HSP.Helper.ErrorNotifier.NotifyByMail(ex)
                End Try
            End If

            Try
                If Not retVal Is Nothing Then
                    Json(context, retVal)
                End If
            Catch ex As Exception
                ' Healthware.HSP.Helper.ErrorNotifier.NotifyByMail(ex)
            End Try

        End Sub

        Private Function ReadSearch(ByRef context As HttpContext) As Object

            Dim data = context.Request("data")
            If String.IsNullOrWhiteSpace(data) Then Return String.Empty

            Dim js As New JavaScriptSerializer()
            Dim obj As New LiteratureSearcherModel()
            obj = js.Deserialize(Of LiteratureSearcherModel)(data)

            Dim result As New OperationResult(Of PagedCollection(Of LiteratureModel))

            Dim accessManager As New AccessService()
            If Not accessManager.HasAccess(New SiteAreaIdentificator(obj.SiteareId)) Then
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
                result.Result = Nothing
                Return result
            End If

            Dim so = GetSearcherRicerca(obj)
            Dim manager As New LiteratureManager()
            Dim coll = manager.GetRicerca(so)
            Dim retVal As Object = ReadSearcherList(coll)




            Dim objLiteratureModelColl As LiteratureModelCollection = Nothing

            objLiteratureModelColl = CType(retVal, LiteratureModelCollection)

            Dim retColl = New PagedCollection(Of LiteratureModel)
            result.Success = True
            result.StatusCode = EnumeratorKeys.AsyncStatus.Failed

            retColl.Collection = retVal
            retColl.PageTotalNumber = retVal.PagerTotalNumber
            retColl.PageNumber = obj.PageNumber
            retColl.CollectionCount = retVal.Count
            result.Result = retColl

            If Not objLiteratureModelColl Is Nothing AndAlso objLiteratureModelColl.Count > 0 Then

                result.StatusCode = EnumeratorKeys.AsyncStatusCode.FillItem
            Else
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
            End If

            Return result

        End Function

        Private Function ReadPhotoGallery(ByRef context As HttpContext) As Object

            Dim data = context.Request("data")
            If String.IsNullOrWhiteSpace(data) Then Return String.Empty

            Dim js As New JavaScriptSerializer()
            Dim obj As New LiteratureSearcherModel()
            obj = js.Deserialize(Of LiteratureSearcherModel)(data)

            Dim result As New OperationResult(Of PagedCollection(Of LiteratureModel))

            Dim accessManager As New AccessService()
            If Not accessManager.HasAccess(New SiteAreaIdentificator(obj.SiteareId)) Then
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
                result.Result = Nothing
                Return result
            End If

            Dim so = GetSearcher(obj)
            Dim manager As New LiteratureManager()
            Dim coll = manager.GetArchive(so)
            Dim retVal As Object = ReadLiteratureList(coll, EnumeratorKeys.LiteratureViewType.ViewSliderPhotoGallery)

            Dim objLiteratureModelColl As LiteratureModelCollection = Nothing

            objLiteratureModelColl = CType(retVal, LiteratureModelCollection)

            Dim retColl = New PagedCollection(Of LiteratureModel)
            result.Success = True
            result.StatusCode = EnumeratorKeys.AsyncStatus.Failed

            retColl.Collection = retVal
            retColl.PageTotalNumber = retVal.PagerTotalNumber
            retColl.PageNumber = obj.PageNumber
            retColl.CollectionCount = retVal.Count
            result.Result = retColl

            If Not objLiteratureModelColl Is Nothing AndAlso objLiteratureModelColl.Count > 0 Then

                result.StatusCode = EnumeratorKeys.AsyncStatusCode.FillItem
            Else
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
            End If

            Return result

        End Function


        Private Function ReadLiterature(ByRef context As HttpContext) As Object

            Dim data = context.Request("data")
            If String.IsNullOrWhiteSpace(data) Then Return String.Empty

            Dim js As New JavaScriptSerializer()
            Dim obj As New LiteratureSearcherModel()
            obj = js.Deserialize(Of LiteratureSearcherModel)(data)

            Dim result As New OperationResult(Of PagedCollection(Of LiteratureModel))

            Dim accessManager As New AccessService()
            If Not accessManager.HasAccess(New SiteAreaIdentificator(obj.SiteareId)) Then
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
                result.Result = Nothing
                Return result
            End If

            Dim so = GetSearcher(obj)
            Dim manager As New LiteratureManager()
            Dim coll = manager.GetArchive(so)
            Dim retVal As Object = ReadLiteratureList(coll, _op, obj.ThemeID)




            Dim objLiteratureModelColl As LiteratureModelCollection = Nothing

            objLiteratureModelColl = CType(retVal, LiteratureModelCollection)

            Dim retColl = New PagedCollection(Of LiteratureModel)
            result.Success = True
            result.StatusCode = EnumeratorKeys.AsyncStatus.Failed

            retColl.Collection = retVal
            retColl.PageTotalNumber = retVal.PagerTotalNumber
            retColl.PageNumber = obj.PageNumber
            retColl.CollectionCount = retVal.Count
            result.Result = retColl

            If Not objLiteratureModelColl Is Nothing AndAlso objLiteratureModelColl.Count > 0 Then

                result.StatusCode = EnumeratorKeys.AsyncStatusCode.FillItem
            Else
                result.StatusCode = EnumeratorKeys.AsyncStatusCode.NoItem
            End If

            Return result

        End Function

        Private Function GetSearcherRicerca(ByVal data As LiteratureSearcherModel) As ContentSearcher
            Dim so As New ContentSearcher()

            Dim sum As Integer = 0
            Dim tmpInt As Integer = 0

            so.SearchString = data.Word
            so.ApprovalStatus = ContentValue.ApprovalWFStatus.Approved

            so.Display = IIf(data.Display, SelectOperation.Enabled, SelectOperation.Disabled)
            so.Active = IIf(data.Active, SelectOperation.Enabled, SelectOperation.Disabled)
            so.Delete = IIf(data.Delete, SelectOperation.Disabled, SelectOperation.Enabled)

            If Not String.IsNullOrEmpty(data.Word) Then
                so.SearchString = data.Word
            End If

            Dim strRuoli As String = data.KeysToExclude
            If Not strRuoli.Contains("{" & Keys.Role.RoleMediaPress & "}") Then
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressArchivio))
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressDashboard))
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressHome))
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressPhotoGallery))
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressDetail))
                so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.PressContatti))
            End If
            so.KeysContentTypeToExclude.Add(New ContentTypeIdentificator(Keys.ContentTypeConstants.Id.DMPDownload))
            so.KeysSiteAreaToExclude.Add(New SiteAreaIdentificator(47))

            'dm comuni a tutte le chiamate , nel caso generalizzare
            so.SortType = ContentGenericComparer.SortType.ByData
            so.SortOrder = ContentGenericComparer.SortOrder.DESC
            Dim Type As TypeResearch = TypeResearch.OROperator


            so.PageNumber = data.PageNumber
            so.PageSize = data.PageSize

            so.Properties.Add("Title")
            so.Properties.Add("Launch")
            so.Properties.Add("DatePublish")
            so.Properties.Add("ContentType.Key.Id")
            so.Properties.Add("ContentType.Key.Name")
            so.Properties.Add("ContentType.Key.Domain")
            so.Properties.Add("LinkUrl")
            so.Properties.Add("LinkLabel")
            so.Properties.Add("SiteArea.Key.Id")

            Return so
        End Function

        Private Function GetSearcher(ByVal data As LiteratureSearcherModel) As ContentExtraSearcher
            Dim so As New ContentExtraSearcher()

            '     Dim strBitmask = data.BitMask.Split(Healthware.DMP.Keys.LabelKeys.DEFAULT_SEPARATOR_COMMA)

            Dim sum As Integer = 0
            Dim tmpInt As Integer = 0

            If data.ApprovalStatus = 1 Then
                so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
            End If

            so.KeySiteArea.Id = data.SiteareId
            so.KeyContentType.Id = data.ContenTypeId
            so.key.IdLanguage = data.LanguageId

            Dim brand As String = Healthware.HP3.Core.Utility.SimpleHash.UrlDecryptRijndaelAdvanced(data.Brand)

            'so.Flags = New BitMaskSearcher()
            'so.Flags.MaskFlags = sum
            'so.Flags.MaskOperator = BitMaskSearcher.Operators.INOperator

            so.Display = IIf(data.Display, SelectOperation.Enabled, SelectOperation.Disabled)
            so.Active = IIf(data.Active, SelectOperation.Enabled, SelectOperation.Disabled)
            so.Delete = IIf(data.Delete, SelectOperation.Disabled, SelectOperation.Enabled)

            If Not String.IsNullOrEmpty(data.Word) Then
                so.SearchString = data.Word
            End If

            If Not String.IsNullOrEmpty(data.year) Then
                Try
                    so.DatePublish.Comparation = DateComparationType.ComparationType.Between
                    so.DatePublish.BoundDate = New DateTime(data.year, 1, 1, 0, 1, 0)
                    '  Date.Parse(data.year & "/01/01")
                    so.DatePublish.BoundDate2 = New DateTime(data.year, 12, 31, 23, 59, 59)
                Catch ex As Exception
                    Throw New Exception(ex.ToString)

                End Try
                
            End If

            If Not String.IsNullOrWhiteSpace(data.KeysToExclude) Then so.KeysToExclude = data.KeysToExclude

            'gestione geo solo per utenti con ruolo Geo
            Dim manager As New AccessService()
            'Dim ticket = manager.GetTicket()
            'Dim userKey = ticket.User.Key

            'dm comuni a tutte le chiamate , nel caso generalizzare
            so.SortingCriteria.Add(New ContentSort(ContentGenericComparer.SortOrder.DESC, ContentGenericComparer.SortType.ByRank))
            so.SortingCriteria.Add(New ContentSort(ContentGenericComparer.SortOrder.DESC, ContentGenericComparer.SortType.ByData))
            'Modificato per gestire la possibilità di effettuare un doppio ordinamento
            'so.SortType = ContentGenericComparer.SortType.ByData
            'so.SortOrder = ContentGenericComparer.SortOrder.DESC

            so.PageNumber = data.PageNumber
            so.PageSize = data.PageSize

            so.Properties.Add("Title")
            so.Properties.Add("Launch")
            so.Properties.Add("DatePublish")
            so.Properties.Add("ContentType.Key.Id")
            so.Properties.Add("ContentType.Key.Name")
            so.Properties.Add("ContentType.Key.Domain")
            so.Properties.Add("FullText")
            so.Properties.Add("LinkUrl")
            so.Properties.Add("DateExpiry")
            so.Properties.Add("LinkLabel")
            so.Properties.Add("Code")
            so.Properties.Add("ApprovalStatus")
            so.Properties.Add("Copyright")
            so.Properties.Add("SiteArea.Key.Id")
            so.Properties.Add("BookReferer")
            so.Properties.Add("FullTextComment")
            so.Properties.Add("Qty")
            so.Properties.Add("Rank")

            Return so
        End Function

        Private Function ReadLiteratureList(ByVal data As ContentExtraCollection, ByVal viewType As Integer, Optional ByVal ThemeID As Integer = 0) As Object

            Dim coll As New LiteratureModelCollection()

            If Not data Is Nothing AndAlso data.Any Then

                Select Case viewType
                    Case CInt(EnumeratorKeys.LiteratureViewType.List)
                        For Each item As ContentExtraValue In data
                            coll.Add(Adapters.LiteratureModelAdapter.Convert(item, ThemeID))
                        Next
                    Case CInt(EnumeratorKeys.LiteratureViewType.ListMobile)
                        For Each item As ContentExtraValue In data
                            coll.Add(Adapters.LiteratureModelAdapter.ConvertMobile(item, ThemeID))
                        Next
                    Case CInt(EnumeratorKeys.LiteratureViewType.FeaturedBox)
                        For Each item As ContentExtraValue In data
                            coll.Add(Adapters.LiteratureModelAdapter.ConvertFeatured(item, ThemeID))
                        Next

                    Case CInt(EnumeratorKeys.LiteratureViewType.ViewSliderPhotoGallery)
                        For Each item As ContentExtraValue In data
                            coll.Add(Adapters.LiteratureModelAdapter.ConvertPhotoGallery(item))
                        Next
                End Select

                coll.PagerTotalNumber = data.PagerTotalNumber

            End If

            Return coll
        End Function

        Private Function ReadSearcherList(ByVal data As ContentCollection) As Object

            Dim coll As New LiteratureModelCollection()

            If Not data Is Nothing AndAlso data.Any Then

                For Each item As ContentValue In data
                    coll.Add(Adapters.LiteratureModelAdapter.ConvertSearch(item))
                Next

                coll.PagerTotalNumber = data.PagerTotalNumber

            End If

            Return coll
        End Function

        Private Sub Json(ByVal context As HttpContext, ByVal data As Object)
            context.Response.ContentType = "application/json"
            Dim serializer = New JavaScriptSerializer()
            context.Response.Write(serializer.Serialize(data))
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class

End Namespace