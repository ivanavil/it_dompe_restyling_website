﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Threading
Imports System.Net.Mail
Imports Healthware.HP3
Imports Healthware.HP3.Core
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.Utility.ObjectValues
Imports Healthware.HP3.PresentationLayer.Share
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Base.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.HP3.Core.Localization
Imports Healthware.HP3.Core.Localization.ObjectValues

Namespace Healthware.Dmp.Helpers

    Public Class MailHelper



        Public Shared Function GetMailAddressCollection(ByVal addressStr As String, Optional ByVal separator As String = ",") As Healthware.HP3.Core.Utility.MailAddressCollection
            Dim retCol As Healthware.HP3.Core.Utility.MailAddressCollection = New Healthware.HP3.Core.Utility.MailAddressCollection()
            Dim addresses() As String = addressStr.Split(separator)
            For Each address As String In addresses
                Try
                    Dim mailAddress As Healthware.HP3.Core.Utility.MailAddress = New Healthware.HP3.Core.Utility.MailAddress(address)
                    retCol.Add(mailAddress)
                Catch
                    Continue For
                End Try
            Next
            Return retCol
        End Function

        Public Shared Function ReadMailTemplate(ByVal templateName As String, ByVal langId As Integer) As MailTemplateValue

            Dim so As New MailTemplateSearcher()
            so.Key.KeyLanguage.Id = langId
            so.Key.MailTemplateName = templateName

            Dim manager As New MailTemplateManager()
            Dim coll = manager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Return coll.FirstOrDefault()
            End If

            Return Nothing

        End Function

        Public Shared Sub SendMessage(ByVal mailTemplateKey As MailTemplateIdentificator, _
                                      ByVal langId As Integer, _
                                      ByVal htmlTemplate As String, _
                                      ByRef replaceDictionary As Dictionary(Of String, String), _
                                      ByVal throwException As Boolean, _
                                    Optional ByVal toAddress As String = "",
                                    Optional ByVal mailFrom As String = "",
                                    Optional ByVal mailSubject As String = "",
                                    Optional ByVal mailAttachment As MailAttachmentsCollection = Nothing)

            Try
                Dim template = ReadMailTemplate(mailTemplateKey.MailTemplateName, langId)

                Dim message As New MailValue()

                If Not String.IsNullOrEmpty(mailFrom) Then
                    message.MailFrom = New Healthware.HP3.Core.Utility.MailAddress(mailFrom)
                Else
                    message.MailFrom = New Healthware.HP3.Core.Utility.MailAddress(template.SenderAddress, template.SenderName)
                End If

                If String.IsNullOrEmpty(toAddress) Then                    
                    message.MailTo = GetMailAddressCollection(template.Recipients, ",")
                Else
                    message.MailTo.Clear()
                    message.MailTo.Add(New Healthware.HP3.Core.Utility.MailAddress(toAddress))
                End If

                message.MailCC = GetMailAddressCollection(template.CcRecipients, ",")
                message.MailBcc = GetMailAddressCollection(template.BccRecipients, ",")

                If Not String.IsNullOrEmpty(mailSubject) Then
                    message.MailSubject = mailSubject
                Else
                    message.MailSubject = template.Subject
                End If

                If Not String.IsNullOrWhiteSpace(htmlTemplate) Then
                    'Dim strHtmlMailTemplate As String = GetHtmlMailTemplate(langId, htmlTemplate)
                    message.MailBody = htmlTemplate.Replace("[TEMPLATEBODY]", template.Body)
                Else
                    message.MailBody = template.Body
                End If
                '--------------------------------------------------------------------
                If Not (replaceDictionary Is Nothing) Then message.MailBody = FieldReplace(message.MailBody, replaceDictionary)

                If template.FormatType = 1 Then
                    message.MailIsBodyHtml = True
                Else
                    message.MailIsBodyHtml = False
                End If

                If Not mailAttachment Is Nothing Then message.MailAttachments = mailAttachment
                message.MailPriority = MailValue.Priority.Normal

                'Save Mesasge in database
                Dim dictmanager = New DictionaryManager()
                Dim dictVal As New DictionaryValue
                dictVal.Active = False
                dictVal.Key.Name = ""
                dictVal.Description = "On:<br>" + DateTime.Now() + "<br>To:<br>" + message.MailTo.ToString() + "<br>Subject:<br>" + message.MailSubject + "<br>Body:<br>" + message.MailBody
                dictVal.KeyLanguage.Id = 1
                dictVal.Label = mailTemplateKey.MailTemplateName & "_Form_Message_Saved"
                dictVal = dictmanager.Create(dictVal)
                'saved

                SendMailMessage(message, throwException)

                If (Not dictVal Is Nothing AndAlso dictVal.Key.Id > 0) Then
                    'per capire i messaggi che sono stati inviati 
                    dictVal.Key.Name = "sent"
                    dictVal = dictmanager.Update(dictVal)
                End If

            Catch ex As Exception
                System.Web.HttpContext.Current.Response.Write("errore invio email:" & ex.ToString())
                'ErrorNotifier.NotifyByMail(ex)
            End Try
        End Sub

        Public Shared Sub SendMailMessage(ByVal message As Object, ByVal throwOn As Boolean, Optional ByVal strSenderName As String = "")
            Try

                Dim oHp3MailValue As MailValue = DirectCast(message, MailValue)
                Dim oMail As New System.Net.Mail.MailMessage

                If Not oHp3MailValue.MailTo Is Nothing AndAlso oHp3MailValue.MailTo.Any() Then
                    For Each objTo As System.Net.Mail.MailAddress In oHp3MailValue.MailTo
                        'System.Web.HttpContext.Current.Response.Write(oHp3MailValue.MailTo.ToString)
                        oMail.To.Add(objTo)
                    Next
                End If
                If Not oHp3MailValue.MailCC Is Nothing AndAlso oHp3MailValue.MailCC.Any() Then
                    For Each objTo As System.Net.Mail.MailAddress In oHp3MailValue.MailCC
                        oMail.CC.Add(objTo)
                    Next
                End If

                If Not oHp3MailValue.MailBcc Is Nothing AndAlso oHp3MailValue.MailBcc.Any() Then
                    For Each objTo As System.Net.Mail.MailAddress In oHp3MailValue.MailBcc
                        'System.Web.HttpContext.Current.Response.Write(oHp3MailValue.MailBcc.ToString)
                        oMail.Bcc.Add(objTo)
                    Next
                End If

                oMail.From = New System.Net.Mail.MailAddress(oHp3MailValue.MailFrom.Address, oHp3MailValue.MailFrom.DisplayName)
                oMail.Subject = oHp3MailValue.MailSubject
                oMail.IsBodyHtml = True
                oMail.Priority = MailPriority.Normal

                Dim htmlView As AlternateView
                htmlView = AlternateView.CreateAlternateViewFromString(DirectCast(message, MailValue).MailBody, Encoding.UTF8, "text/html")
                oMail.AlternateViews.Add(htmlView)
                Try
                    'Attachment collection management--------------------
                    If Not oHp3MailValue.MailAttachments Is Nothing AndAlso oHp3MailValue.MailAttachments.Count() > 0 Then
                        For Each Attachment As MailAttachment In oHp3MailValue.MailAttachments
                            oMail.Attachments.Add(Attachment)
                        Next
                    End If
                    '----------------------------------------------------
                Catch ex As Exception
                    'ErrorNotifier.NotifyByMail(ex)
                End Try

                'TODO: da ripristinare
                'Dim mySmtpMail As New SmtpClient("relay-www.dompe.com", 587)
                '            mySmtpMail.Credentials = New System.Net.NetworkCredential("noreply@dompe.com", "Pass67!5r87!")
                '            mySmtpMail.EnableSsl = True
                '            mySmtpMail.Send(oMail)


                Dim mySmtpMail As New SmtpClient
                mySmtpMail.Host = ConfigurationManager.AppSettings("smtp")
                mySmtpMail.Send(oMail)
                oMail.Dispose()

            Catch ex As Exception
                System.Web.HttpContext.Current.Response.Write("errore invio email2:" & ex.ToString())
                If throwOn Then
                    Throw New ApplicationException(ex.Message, ex)
                Else
                    'ErrorNotifier.NotifyByMail(ex)
                End If
            End Try
        End Sub

        Public Shared Function GetHtmlMailTemplate(ByVal langId As Integer, ByVal htmlMailTemplate As String) As String
            Dim _out As String = String.Empty
            Dim oDictMan As New DictionaryManager()

            Return oDictMan.Read(htmlMailTemplate, langId)

        End Function

        Public Shared Function FieldReplace(ByVal str As String, ByVal fields As Dictionary(Of String, String)) As String
            For Each item As KeyValuePair(Of String, String) In fields
                str = str.Replace(item.Key, item.Value)
            Next
            Return str
        End Function

    End Class

End Namespace