﻿Imports Microsoft.VisualBasic

Namespace Healthware.DMP.Configuration

    Public Class AppSettingsData

        Public Shared ReadOnly Property InternalUrls() As String
            Get
                Return ConfigurationManager.AppSettings("internalUrls").ToString()
            End Get
        End Property

        Public Shared ReadOnly Property InternalUrlsMobile() As String
            Get
                Return ConfigurationManager.AppSettings("internalUrlsMobile").ToString()
            End Get
        End Property

    End Class

End Namespace