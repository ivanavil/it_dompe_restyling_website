﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.IO
Imports System.Web.Hosting
Imports System.Security.Cryptography
Imports System.Net

Namespace HP3ContentPromotion.WebSite

    <WebService(Namespace:="http://schemas.HP3ContentPromotion.org/FileSync")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class RemoteFileSync
        Inherits FileSync

        Protected Overrides ReadOnly Property BaseDir() As String
            Get
                Return Path.Combine(HostingEnvironment.ApplicationPhysicalPath, ConfigurationManager.AppSettings("remotePath"))
            End Get
        End Property
    End Class

End Namespace