﻿Imports Microsoft.VisualBasic
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.HP3.Core.Base
Imports Healthware.HP3.Core.Web.ObjectValues
Imports Healthware.HP3.Core.Localization.ObjectValues
Imports Healthware.HP3.Core.Web
Imports Healthware.HP3.Core.Document.ObjectValues
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.User
Imports System.Data.SqlClient
Imports System.Data
Imports Dompe
Imports Healthware.HP3.Core.Utility.ObjectValues


Namespace Healthware.Dompe.Helper

    Public Class Helper

        Dim oMapMan As Healthware.HP3.Core.Base.MappingManager = Nothing
        Dim strConnessione As String = String.Empty
        Dim strStored As String = String.Empty

        Public Sub New()
            oMapMan = New Healthware.HP3.Core.Base.MappingManager()
            strConnessione = System.Configuration.ConfigurationManager.AppSettings("conn-string")
            strStored = "proc_PLY_codici_invito"
        End Sub



        Public Shared Function getLinkByIdTheme(ByRef objMasterPageManager As MasterPageManager, ByVal idTheme As Integer) As String
            Dim objThemeValue As ThemeValue = Nothing
            Dim objThemeManager As New ThemeManager

            objThemeValue = objThemeManager.Read(New ThemeIdentificator(idTheme))

            If Not objThemeValue Is Nothing Then
                Return objMasterPageManager.GetLink(objThemeValue, False)
            End If

            Return ""
        End Function


        Public Shared Function GetThemeLink(ByRef objMasterPageManager As MasterPageManager, ByVal idTheme As Integer, ByVal langId As Integer) As String

            Dim manager As New ThemeManager
            Dim so As New ThemeSearcher()
            so.KeyLanguage.Id = langId
            so.Key.Id = idTheme
            Dim coll = manager.Read(so)
            If Not coll Is Nothing AndAlso coll.Any() Then
                Dim theme = coll.FirstOrDefault()
                theme.KeyLanguage.Id = langId
                If Not theme Is Nothing Then Return objMasterPageManager.GetLink(theme, False)
            End If

            Return String.Empty

        End Function


        Public Shared Function GetContentPrimaryKey(ByVal idContent As Integer, ByVal idLanguage As Integer) As Integer

            Dim serchct As New ContentSearcher
            Dim serchColl As New ContentCollection
            Dim serchMan As New ContentManager
            Dim PK As Integer = 0
            serchct.key.Id = idContent
            serchct.key.IdLanguage = idLanguage
            serchct.Properties.Add("Key.PrimaryKey")

            serchColl = serchMan.Read(serchct)

            If Not serchColl Is Nothing AndAlso serchColl.Count > 0 Then
                PK = serchColl(0).Key.PrimaryKey
            End If

            Return PK

        End Function

        Public Function existUserByEmail(ByVal userEmail As String) As Integer
            Dim _out As Integer = 0
            Dim conn As SqlConnection = Nothing
            Dim com As SqlCommand = Nothing
            Dim reader As SqlDataReader = Nothing
            Try
                conn = New SqlConnection(strConnessione)
                com = New SqlCommand("proc_PLY_Utility", conn)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.Add("@TypeQuery", SqlDbType.NVarChar).Value = "VerifyMail"
                com.Parameters.AddWithValue("@userEmail", userEmail.Trim)
                conn.Open()

                Return com.ExecuteScalar()

            Catch ex As Exception
                System.Web.HttpContext.Current.Response.Write("Errore:" & ex.Message)
                Return False
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                conn.Dispose()
                com.Dispose()
            End Try

            Return _out
        End Function

        Function convertSize(ByVal intSize As Integer) As String
            Dim _out As String = "{0} KB"
            Dim kbL As Double
            If intSize >= 1048576 Then
                _out = "{0} MB"
                kbL = Math.Round(((Double.Parse(intSize) / 1024) / 1024)) 'Math.Round(((Double.Parse(intSize) / 1024) / 1024), 2)
            Else
                kbL = Math.Round((Double.Parse(intSize) / 1024)) 'Math.Round((Double.Parse(intSize) / 1024))
            End If

            If CType(kbL, Integer) > 0 Then Return String.Format(_out, kbL.ToString)
            Return "0 KB"
        End Function

        Function getThemeBySiteArea(ByVal NameSiteArea As String) As Integer
            Dim risId As Integer = 0

            Select Case NameSiteArea
                Case "WhatIs"
                    risId = 192
                Case "Identify"
                    risId = 193
                Case "Treatment"
                    risId = 194
                Case "CorrectUse"
                    risId = 195
                Case "Informat"
                    risId = 196
                Case "Policy"
                    risId = 197
                Case "Terms"
                    risId = 198
                Case "PLYDown"
                    risId = 199
                Case "PLYVideo"
                    risId = 200
                Case Else
                    risId = 0
            End Select
            Return risId

        End Function


        Public Shared Function ResolveUrl(ByVal data As ContentValue, ByVal themeID As Integer) As String
            Dim codeLang As String = "it"
            If data.Key.IdLanguage > 1 Then codeLang = "en"


            If data Is Nothing Then Return String.Empty

            If Not data.LinkUrl Is Nothing AndAlso data.LinkUrl <> String.Empty AndAlso data.SiteArea.Key.Id <> SiteAreaConstants.IDEventiConferenze AndAlso data.SiteArea.Key.Id <> SiteAreaConstants.IDSiteareaPubblicazione Then
                Return "javascript:ExternalLinkPopup" & codeLang & "('" & data.LinkUrl & "');"
            Else

                Dim manager As New ContentManager()
                'Dim content = manager.Read(key)
                'If content Is Nothing Then Return String.Empty                
                Dim accMan = New AccessService()
                Dim ticket = accMan.GetTicket()

                Dim oWebRequest As New WebRequestValue
                oWebRequest.IdSite = ticket.Travel.KeySite.Id
                oWebRequest.KeyLang = New LanguageIdentificator(data.Key.IdLanguage)
                oWebRequest.ContentTitle = data.Title
                'Helper.getLinks.CreateTravelTicket(oWebRequest)
                Dim contentType = data.ContentType
                oWebRequest.Keycontent = data.Key
                oWebRequest.KeyTheme = New ThemeIdentificator(themeID)
                oWebRequest.KeysiteArea.Id = data.SiteArea.Key.Id

                If data.SiteArea.Key.Id = 99 Then
                    Return "#"
                Else
                    Return New MasterPageManager().FormatRequest(oWebRequest)
                End If

            End If

            Return String.Empty
        End Function


        Public Shared Function GetLinkSearch(ByVal content As ContentValue) As String
            'calcolo il link in base al ContentType del Contenuto
            Dim _LINK As String = String.Empty

            If Not content Is Nothing AndAlso Not content.ContentType.Key Is Nothing Then

                 Dim oWebRequest As New WebRequestValue
                oWebRequest.IdSite = 46
                oWebRequest.KeyLang = New LanguageIdentificator(content.Key.IdLanguage)
                oWebRequest.ContentTitle = content.Title
                'Helper.getLinks.CreateTravelTicket(oWebRequest)

                oWebRequest.Keycontent = content.Key
                oWebRequest.KeycontentType = content.ContentType.Key
                oWebRequest.KeysiteArea.Id = content.SiteArea.Key.Id
                oWebRequest.KeyTheme = New ThemeIdentificator(192) 'BusinessThemeManager.Read(New SiteIdentificator(46), New SiteAreaIdentificator(content.SiteArea.Key.Id), New ContentTypeIdentificator(content.ContentType.Key.Id))
                _LINK = New MasterPageManager().FormatRequest(oWebRequest)
                Return _LINK
            End If

            Return "#"
        End Function

        Public Shared Function get_FormatData(ByVal dataIn As Date, ByVal id_language As Integer) As String
            Dim str_finale As String = Day(dataIn) & "." & Month(dataIn) & "." & Year(dataIn)

            If id_language = 1 Then
                str_finale = Day(dataIn) & "." & Month(dataIn) & "." & Year(dataIn)
            ElseIf id_language = 2 Then
                str_finale = Month(dataIn) & "." & Day(dataIn) & "." & Year(dataIn)
            Else
                str_finale = Day(dataIn) & "." & Month(dataIn) & "." & Year(dataIn)
            End If

            Return str_finale
        End Function

        Public Shared Function GetCover(ByVal contentKey As ContentIdentificator, ByVal width As Integer, ByVal height As Integer, Optional indexCover As Integer = 0) As String

            Dim retVal As String = String.Empty

            Dim quality As New ImageQualityValue()
            quality.CompositingQuality = ImageQualityValue.EnumCompositingQuality.HighQuality
            quality.SmoothingMode = ImageQualityValue.EnumSmoothingMode.HighQuality
            quality.ImageQuality = ImageQualityValue.EnumImageQuality.High
            quality.JPEGImageCompression = ImageQualityValue.EnumJPEGcompression.L0

            Dim manager As New ContentManager()

            If Not contentKey Is Nothing Then
                retVal = manager.GetCover(contentKey, width, height, quality, indexCover)
            End If


            Return retVal

        End Function

        Public Shared Function GetSectionName(ByVal siteareaKey As SiteAreaIdentificator) As String

            Dim defaultSection As String = "Generico"

            If siteareaKey Is Nothing Then Return defaultSection

            If siteareaKey.Id = SiteAreaConstants.IDSiteareaNews Then
                Return "News"
            ElseIf siteareaKey.Id = SiteAreaConstants.IDSiteareaPubblicazione Then
                Return "Pubblicazione"
            ElseIf siteareaKey.Id = SiteAreaConstants.IDSiteareaSedi Then
                Return "sedi"
            ElseIf siteareaKey.Id = SiteAreaConstants.IDSiteareaNewsMediaPress Then
                Return "Press"
            ElseIf siteareaKey.Id = SiteAreaConstants.IDEventiConferenze Then
                Return "Eventi"
            End If

            Return defaultSection

        End Function

        Public Shared Function GetTheme(ByVal so As ThemeSearcher) As ThemeValue

            If so Is Nothing Then Return Nothing

            Dim man As New ThemeManager()
            Dim coll As ThemeCollection = man.Read(so)

            If Not coll Is Nothing AndAlso coll.Any Then Return coll.FirstOrDefault()

            Return Nothing

        End Function

        Public Shared Function DelLengthString(ByVal strTruncate As String, ByVal MaxLength As Integer, Optional ByVal final As String = "...") As String
            Dim newString As String = strTruncate

            If String.IsNullOrWhiteSpace(strTruncate) Then Return newString

            If strTruncate.Length > MaxLength Then
                Dim index As Integer = strTruncate.IndexOf(" ", MaxLength)
                If index >= MaxLength OrElse index <= 0 Then
                    Dim tmp As String = strTruncate.Substring(0, MaxLength)
                    index = tmp.LastIndexOf(" ")
                    If index > 0 Then
                        newString = String.Format("{0} {1}", tmp.Substring(0, index), final)
                    ElseIf index <= 0 Then
                        newString = TruncateString(strTruncate, MaxLength, final)
                    End If
                End If
            End If

            Return newString
        End Function

        Public Shared Function TruncateString(ByVal strTruncate As String, ByVal MaxLength As Integer, Optional ByVal final As String = "...") As String
            Dim newString As String = strTruncate

            If String.IsNullOrWhiteSpace(strTruncate) Then Return newString

            If strTruncate.Length > MaxLength Then

                newString = String.Format("{0} {1}", strTruncate.Substring(0, MaxLength), final)

            End If

            Return newString
        End Function

        Public Shared Function checkApproval(ByVal approvalStatus As Integer, Optional ByVal isHtml As Boolean = True) As String
            If approvalStatus > 1 And isHtml Then Return "<span style='color:red;font-weight:bold;'> - NON APPROVATO</span>"
            If approvalStatus > 1 And Not isHtml Then Return " - NON APPROVATO"
            Return String.Empty
        End Function

    End Class
End Namespace