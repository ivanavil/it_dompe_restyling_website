﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Namespace Healthware.Dompe.Extensions

    Public Module WebControlExtensions

        <Extension()> _
        Public Sub LoadSubControl(ByRef control As Healthware.HP3.PresentationLayer.Share.ControlShare, ByVal path As String, ByRef destPanel As Control)
            If destPanel Is Nothing Then
                Throw New ArgumentException("destPanel")
            End If

            If Not String.IsNullOrEmpty(path) Then
                Dim tmpCtrl As Control = control.LoadControl(path)
                If Not tmpCtrl Is Nothing Then
                    destPanel.Controls.Add(tmpCtrl)
                End If
            End If
        End Sub

        <Extension()> _
        Public Sub ClientLaunchScript(ByRef ctl As System.Web.UI.Control, ByVal strFunction As String)
            ScriptManager.RegisterClientScriptBlock(ctl.Page, ctl.Page.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    String.Format("javascript:{0};", strFunction), _
                                                    True)
        End Sub

    End Module

End Namespace
