Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Data

Namespace Dompe.Helpers
    Public Class Sql

        Public Shared Function ReadQuery(ByVal queryName As String, Optional ByVal useCache As Boolean = True) As String
            Dim retVal As String = String.Empty

            Dim cacheKey As String = String.Empty
            Dim context = HttpContext.Current

            If useCache Then
                cacheKey = String.Format("sqlQuery_{0}", queryName)
                retVal = context.Cache(cacheKey)
            End If

            If String.IsNullOrEmpty(retVal) Then
                Dim queryFullpath = context.Server.MapPath(String.Format("~/App_Data/SqlQueries/{0}.sql", queryName))
                If System.IO.File.Exists(queryFullpath) Then
                    retVal = System.IO.File.ReadAllText(queryFullpath)
                    If Not String.IsNullOrEmpty(retVal) AndAlso useCache Then
                        context.Cache.Insert(cacheKey, retVal, Nothing, DateTime.Now.AddMinutes(60 * 3), TimeSpan.Zero)
                    End If
                End If
            End If
            Return retVal
        End Function

        Public Shared Function ExecuteQuery(ByVal sqlQuery As String, ByVal sqlParams As SqlParameter()) As DataSet
            Return ExecuteQuery(sqlQuery, sqlParams, ConfigurationManager.AppSettings("conn-string"))
        End Function

        Public Shared Function ExecuteQuery(ByVal sqlQuery As String, ByVal sqlParams As SqlParameter(), ByVal connStr As String) As DataSet
            Using sqlConn As New SqlConnection(connStr)
                Using sqlCmd As New SqlCommand(sqlQuery, sqlConn)
                    sqlCmd.CommandTimeout = 0
					sqlCmd.Parameters.Clear()

                    If Not sqlParams Is Nothing AndAlso sqlParams.Any() Then
                        sqlCmd.Parameters.AddRange(sqlParams)
                    End If

                    Using sqlAd As New SqlDataAdapter(sqlCmd)
                        Dim ds As New DataSet
                        If 0 <> sqlAd.Fill(ds) Then
                            Return ds
                        End If
                    End Using
                End Using
            End Using

            Return Nothing
        End Function

    End Class
End Namespace