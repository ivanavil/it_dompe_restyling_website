﻿Imports Microsoft.VisualBasic
Imports Healthware.DMP.Models
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Web
Imports Healthware.DMP.BL
Imports Healthware.Dompe.Helper
Imports Dompe
Imports Healthware.HP3.Core.Base.ObjectValues
Imports System
Imports System.Web
Imports System.IO
Imports Healthware.DMP
Imports Healthware.HP3.Core.Localization
Imports System.Globalization

Namespace Healthware.DMP.Adapters

    Public Class LiteratureModelAdapter

        Public Shared Function Convert(ByVal data As ContentExtraValue, ByVal themeId As Integer) As LiteratureModel

            Dim abbreviate As Boolean = True
            Dim manD As New DictionaryManager
            Dim labelbook As String = manD.Read("DMP_labelbook", data.Key.IdLanguage, "DMP_labelbook")
            Dim labelComment As String = manD.Read("DMP_labelComment", data.Key.IdLanguage, "DMP_labelComment")
            If data Is Nothing Then Return Nothing
            Dim cover As String
            cover = GetCover(data.Key, 0, 0)
            If String.IsNullOrEmpty(cover) Then cover = "cover_default.jpg"

            Dim curInfo As CultureInfo = Nothing

            Select Case data.Key.IdLanguage
                Case 1
                    curInfo = New CultureInfo("it-IT")

                Case 2
                    curInfo = New CultureInfo("en-GB")

                Case 3
                    curInfo = New CultureInfo("sq-AL")
					
                Case 4
                    curInfo = New CultureInfo("es-ES")					

            End Select

            Dim strLocalizedMonthName As String = DateTime.Parse(data.DatePublish).ToString("MMM", curInfo) 'MonthName(Month(DateTime.Parse(data.DatePublish)), abbreviate)             

            Dim model = New LiteratureModel With
                        {
                            .Key = data.Key,
                            .Title = data.Title & Healthware.Dompe.Helper.Helper.checkApproval(data.ApprovalStatus, False),
                            .Launch = data.Launch,
                            .Cover = cover,
                            .Link = Helper.ResolveUrl(data, themeId),
                            .DatePublish = data.DatePublish.Value.ToShortDateString.Replace("/", "."),
                            .Day = Day(data.DatePublish),
                            .Month = strLocalizedMonthName,
                            .Year = Right(Year(data.DatePublish), 2),
                            .LinkDownload = DownloadRelated(data.Key.PrimaryKey),
                            .FullText = IIf(data.FullText.Length > 1, "1", "0"),
                            .DateExpiry = data.DateExpiry.Value.ToShortDateString.Replace("/", "."),
                            .LinkLabel = data.LinkLabel,
                            .Code = data.Code,
                            .Copyright = data.Copyright,
                            .LinkSticker = manD.Read("DMP_Detail", data.Key.IdLanguage, "DMP_Detail"),
                            .BookReference = IIf(Not String.IsNullOrEmpty(data.BookReferer), "<b>" & labelbook & ":</b><i>" & data.BookReferer & "</i>", ""),
                            .Comment = IIf(Not String.IsNullOrEmpty(data.FullTextComment), "<b>" & labelComment & ":</b><i>" & data.FullTextComment & "</i>", ""),
                            .LabelDownload = manD.Read("DMP_LabelDwn", data.Key.IdLanguage, "DMP_LabelDwn"),
                            .Linkexternal = "javascript:ExternalLinkPopupit" & "('" & data.LinkUrl & "')",
                            .HideDateDay = (data.Qty > 0)
            }

            Return model

        End Function


        Public Shared Function ConvertMobile(ByVal data As ContentExtraValue, ByVal themeId As Integer) As LiteratureModel

            Dim abbreviate As Boolean = True
            Dim manD As New DictionaryManager
            Dim labelbook As String = manD.Read("DMP_labelbook", data.Key.IdLanguage, "DMP_labelbook")
            Dim labelComment As String = manD.Read("DMP_labelComment", data.Key.IdLanguage, "DMP_labelComment")
            If data Is Nothing Then Return Nothing
            Dim cover As String
            cover = GetCover(data.Key, 0, 0)
            If String.IsNullOrEmpty(cover) Then cover = "cover_default.jpg"

            Dim cover2 As String
            cover2 = GetCover(data.Key, 0, 0)
            If String.IsNullOrEmpty(cover2) Then cover2 = cover

            Dim curInfo As CultureInfo = Nothing

            Select Case data.Key.IdLanguage
                Case 1
                    curInfo = New CultureInfo("it-IT")

                Case 2
                    curInfo = New CultureInfo("en-GB")

                Case 3
                    curInfo = New CultureInfo("sq-AL")

            End Select

            Dim strLocalizedMonthName As String = DateTime.Parse(data.DatePublish).ToString("MMM", curInfo) 'MonthName(Month(DateTime.Parse(data.DatePublish)), abbreviate)             

            Dim model = New LiteratureModel With
                        {
                            .Key = data.Key,
                            .Title = data.Title & Healthware.Dompe.Helper.Helper.checkApproval(data.ApprovalStatus, False),
                            .Launch = data.Launch,
                            .Cover = cover,
                            .Cover2 = cover2,
                            .Link = Helper.ResolveUrl(data, themeId),
                            .DatePublish = data.DatePublish.Value.ToShortDateString.Replace("/", "."),
                            .Day = Day(data.DatePublish),
                            .Month = strLocalizedMonthName,
                            .Year = Right(Year(data.DatePublish), 2),
                            .LinkDownload = DownloadRelated(data.Key.PrimaryKey),
                            .FullText = IIf(data.FullText.Length > 1, "1", "0"),
                            .DateExpiry = data.DateExpiry.Value.ToShortDateString.Replace("/", "."),
                            .LinkLabel = data.LinkLabel,
                            .Code = data.Code,
                            .Copyright = data.Copyright,
                            .LinkSticker = manD.Read("DMP_Detail", data.Key.IdLanguage, "DMP_Detail"),
                            .BookReference = IIf(Not String.IsNullOrEmpty(data.BookReferer), "<b>" & labelbook & ":</b><i>" & data.BookReferer & "</i>", ""),
                            .Comment = IIf(Not String.IsNullOrEmpty(data.FullTextComment), "<b>" & labelComment & ":</b><i>" & data.FullTextComment & "</i>", ""),
                            .LabelDownload = manD.Read("DMP_LabelDwn", data.Key.IdLanguage, "DMP_LabelDwn"),
                            .Linkexternal = data.LinkUrl,
                            .HideDateDay = (data.Qty > 0)
            }

            Return model

        End Function

        Public Shared Function ConvertSearch(ByVal data As ContentValue) As LiteratureModel
            Dim manD As New DictionaryManager
            Dim labelbook As String = manD.Read("DMP_labelbook", data.Key.IdLanguage, "DMP_labelbook")
            Dim labelComment As String = manD.Read("DMP_labelComment", data.Key.IdLanguage, "DMP_labelComment")
            If data Is Nothing Then Return Nothing
            Dim model = New LiteratureModel With
                        {
                            .Key = data.Key,
                            .Title = data.Title,
                            .Launch = data.Launch,
                         .Link = Helper.GetLinkSearch(data),
                           .LinkSticker = manD.Read("DMP_Detail", data.Key.IdLanguage, "DMP_Detail")
            }

            Return model

        End Function

        Public Shared Function ConvertPhotoGallery(ByVal data As ContentExtraValue) As LiteratureModel

            If data Is Nothing Then Return Nothing
            Dim cover As String
            cover = GetCover(data.Key, 0, 0)
            If String.IsNullOrEmpty(cover) Then cover = "cover_default.jpg"
            Dim model = New LiteratureModel With
                        {
                            .Key = data.Key,
                            .Title = data.Title & Healthware.Dompe.Helper.Helper.checkApproval(data.ApprovalStatus, False),
                            .Launch = data.Launch,
                            .Cover = cover,
                            .Link = "/ProjectDOMPE/HP3Popup/PhotoGallery.aspx?inpage=1&l=" & getCodeLanguage(data.Key.IdLanguage) & "&c=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndael(data.Key.Id),
                            .DatePublish = data.DatePublish.Value.ToShortDateString.Replace("/", "."),
                            .LinkDownload = DownloadRelated(data.Key.PrimaryKey),
                            .FullText = IIf(data.FullText.Length > 1, "1", "0")
            }

            Return model

        End Function

        Public Shared Function getCodeLanguage(ByVal idL As Integer) As String
            Dim strFinal As String = "it"
            If idL = 2 Then strFinal = "en"
            If idL = 3 Then strFinal = "alb"
            Return strFinal
        End Function

        Protected Shared Function GetCover(ByVal contentKey As ContentIdentificator, ByVal width As Integer, ByVal height As Integer, Optional ByVal indexC As Integer = 0) As String
            If contentKey Is Nothing Then Return String.Empty
            Dim retVal As String = Healthware.Dompe.Helper.Helper.GetCover(contentKey, width, height, indexC)
            Return retVal

        End Function

        Public Shared Function ConvertFeatured(ByVal data As ContentExtraValue, ByVal themeId As Integer) As LiteratureModel

            If data Is Nothing Then Return Nothing

            Dim abbreviate As Boolean = True

            Dim textLength As Integer = 230

            Dim man As New DictionaryManager()

            Dim model = New LiteratureModel With
                        {
                            .Key = data.Key,
                            .Title = data.Title & Healthware.Dompe.Helper.Helper.checkApproval(data.ApprovalStatus, False),
                            .Launch = Helper.DelLengthString(data.Launch, textLength),
                            .Cover = GetCover(data.Key, 325, 200),
                            .Link = Helper.ResolveUrl(data, themeId),
                            .Day = Day(data.DatePublish),
                            .Month = MonthName(Month(data.DatePublish), abbreviate),
                            .LinkLabel = GetLabel(data.Key.IdLanguage),
                            .Rank = data.Rank,
                            .LinkSticker = man.Read("lbl_primopiano", data.Key.IdLanguage, "in primo piano").ToUpper()
            }

            Return model

        End Function

        Protected Shared Function GetLabel(ByVal langId As Integer) As String

            Dim man As New DictionaryManager()

            Return man.Read("DMP_readMore", langId, "DMP_readMore")

        End Function

        Public Shared Function DownloadRelated(ByVal pk As Integer) As String
            Dim res As String = "#"
            If pk > 0 Then
                Dim dwncoll As New ContentCollection
                Dim dwnsearch As New ContentSearcher
                Dim soRel As New ContentRelatedSearcher
                Dim coll As ContentCollection = Nothing
                Dim manCont As New ContentManager

                ' 
                soRel.Content.Key.PrimaryKey = pk
                soRel.Content.KeyRelationType.Id = 4
                'soRel.Content.RelationSide = RelationTypeSide.Right
                soRel.Content.RelationSide = RelationTypeSide.Left
                soRel.Content.RelationTypeSearcher = RelationTypeSearch.SelectedType
                ' 

                'dwnsearch.KeySiteArea.Id = 94 'Dompe.SiteAreaConstants.IDDownloadArea
                dwnsearch.SortType = ContentGenericComparer.SortType.ByData
                dwnsearch.SortOrder = ContentGenericComparer.SortOrder.DESC
                dwnsearch.KeyContentType.Id = 135
                dwnsearch.RelatedTo = soRel

                dwncoll = manCont.Read(dwnsearch)

                If Not dwncoll Is Nothing AndAlso dwncoll.Count > 0 Then
                    res = "/ProjectDOMPE/HP3Handler/Download.ashx?pk=" & Healthware.HP3.Core.Utility.SimpleHash.UrlEncryptRijndaelAdvanced(dwncoll(0).Key.PrimaryKey)
                End If
            End If
            Return res
        End Function

        'Protected Shared Function GetAuthors(ByVal key As ContentIdentificator) As AuthorCollection

        '    Dim manager As New LiteratureManager()
        '    Dim so As New ContentAuthorSearcher()
        '    so.KeyContent = key
        '    so.KeyRelationType = New RelationTypeIdentificator(CInt(Healthware.HSP.Keys.EnumeratorKeys.LiteratureAuthorType.Author))
        '    Return manager.GetAuthors(so)

        'End Function

        '    Protected Shared Function GetCover(ByVal contentKey As ContentIdentificator, ByVal width As Integer, ByVal height As Integer) As String
        '        If contentKey Is Nothing Then Return String.Empty

        '        Dim retVal As String = Helper.GenericHelper.GetCover(contentKey, width, height)
        '        If Not String.IsNullOrWhiteSpace(retVal) Then
        '            retVal = String.Format(Keys.LabelKeys.COVER_PATH, retVal)

        '        Else

        '            'Generic Cover
        '            retVal = String.Format(Keys.LabelKeys.COVER_PATH, String.Format(HSP.Keys.LabelKeys.LITERATURE_GENERIC_COVER, width))

        '        End If

        '        Return retVal

        '    End Function

    End Class

End Namespace
