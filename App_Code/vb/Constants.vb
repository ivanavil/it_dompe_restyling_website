﻿Imports System
Imports System.IO
Imports System.Web
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Utility
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.HP3.Core.Web

Namespace Dompe

    Public Class ContentBoxImage
        Public Const IdContentList As String = "" '"{7}{9}"
    End Class

    Public Class ThemeConstants
        'Sito Desktop
        Public Const idHomeTheme As Integer = 191
        Public Const idHeader As Integer = 193
        Public Const idAreaPress As Integer = 194
        Public Const idThemaSezioneGiornalista As Integer = 229
        Public Const idAreaMedico As Integer = 195
        Public Const idAreaPartner As Integer = 196
        Public Const idFooter As Integer = 197
        Public Const idChiSiamo As Integer = 198
        Public Const idRicerca As Integer = 199
        Public Const idSoluzioni As Integer = 200
        Public Const idResponsabilita As Integer = 201
        Public Const idMedia As Integer = 202
        Public Const idStoria As Integer = 203
        Public Const idAtGlance As Integer = 204
        Public Const idMission As Integer = 205
        Public Const idManagement As Integer = 206
        Public Const idSedi As Integer = 207
        Public Const idDiabetologia As Integer = 210
        Public Const idOftalmologia As Integer = 211
        Public Const idOncologia As Integer = 212
        Public Const idNetwork As Integer = 213
        Public Const idInnovazione As Integer = 214
        Public Const idPipeline As Integer = 215
        Public Const idPubblicazioni As Integer = 216
        Public Const idApparatoRespiratorio As Integer = 217
        Public Const idMalattieRare As Integer = 220
        Public Const idNews As Integer = 228
        Public Const idPercheDompe As Integer = 231
        Public Const idPercorsiProfessionali As Integer = 232
        Public Const idDownload As Integer = 238
        Public Const idLinksUseful As Integer = 239
        Public Const idInfiammazione As Integer = 218
        Public Const idTerapiaDolore As Integer = 219
        Public Const idAutomedicazione As Integer = 221
        Public Const idRespSocialeImpresa As Integer = 222
        Public Const idAssociazioniPazienti As Integer = 226
        Public Const IDEventiConferenze As Integer = 227
        Public Const IDProgettiIniziative As Integer = 225
        Public Const IDStudenti As Integer = 233
        Public Const IDProfessionisti As Integer = 234
        Public Const idSelectionRecruiting As Integer = 235
        Public Const IDPosizioniAperte As Integer = 236
        Public Const idThemaSezioneDashboard As Integer = 240
        Public Const idComunicatiStampaMediaPress As Integer = 241
        Public Const idSchedeIstituzionaliMediaPress As Integer = 242
        Public Const idPhotoGalleryMediaPress As Integer = 243
        Public Const idContattiMediaPress As Integer = 244
        Public Const idPressContatti As Integer = 295
        Public Const idRassegnaStampaMediaPress As Integer = 245
        Public Const idLinksUsefulMediaPress As Integer = 246
        Public Const idNewsMediaPress As Integer = 248
        Public Const idDownloadMediaPress As Integer = 247
        Public Const idDownloadContentMediaPress As Integer = 250
        Public Const idComunicatiStampaPubblici As Integer = 254
        Public Const idMedicalHome As Integer = 256
        Public Const idArchivioRCP As Integer = 257
        Public Const idBrand As Integer = 258
        Public Const idFocuson As Integer = 259
        Public Const idInfoutili As Integer = 260
        Public Const idNewsEventiMedical As Integer = 261
        Public Const idArticoloMedical As Integer = 262
        Public Const idDownloadMedical As Integer = 263
        Public Const idPhotogalleryMedical As Integer = 264
        Public Const idTrapianti As Integer = 279
        Public Const idSeiAprile As Integer = 280
        Public Const IdStorie As Integer = 281
        Public Const partnerWelcome As Integer = 296
        Public Const partnerAboutUs As Integer = 297
        Public Const partnerFocusOn As Integer = 298
        Public Const partnerMedicalInfo As Integer = 299
        Public Const partnerNews As Integer = 300
        Public Const idDrugDiscovery As Integer = 307
        Public Const idRarestOnes As Integer = 309
		Public Const idModello231 As Integer = 282
        Public Const idTrasmissioneValori As Integer = 428
        Public Const idWeCare As Integer = 437
        Public Const idSpecialityCare As Integer = 444
        Public Const idNostraRicerca As Integer = 441
        Public Const idBiotecnologie As Integer = 442
        Public Const idTerapieAvanz As Integer = 443
        Public Const idEtica As Integer = 425
        Public Const idTrasparenza As Integer = 224
        Public Const idAmbiente As Integer = 445
        Public Const idPoliticaQualSicur As Integer = 301
        Public Const idCodiceCondotta As Integer = 446
        Public Const idEccellenza As Integer = 454
        Public Const idOpenInnov As Integer = 453
        Public Const idCardilogia As Integer = 456
        Public Const idRespiratorio As Integer = 457
        Public Const idGastroenterologia As Integer = 455
        Public Const idCareers As Integer = 230
        Public Const idProgettiSpeciali As Integer = 448

        'Temi Sito Mobile
        Public Const idHomeThemeMobile As Integer = 310
        Public Const idHeaderMobile As Integer = 312
        Public Const idAreaPressMobile As Integer = 313
        Public Const idThemaSezioneGiornalistaMobile As Integer = 347
        Public Const idAreaMedicoMobile As Integer = 314
        Public Const idAreaPartnerMobile As Integer = 315
        Public Const idFooterMobile As Integer = 316
        Public Const idChiSiamoMobile As Integer = 317
        Public Const idRicercaMobile As Integer = 318
        Public Const idSoluzioniMobile As Integer = 319
        Public Const idResponsabilitaMobile As Integer = 320
        Public Const idMediaMobile As Integer = 321
        Public Const idStoriaMobile As Integer = 322
        Public Const idAtGlanceMobile As Integer = 323
        Public Const idMissionMobile As Integer = 324
        Public Const idManagementMobile As Integer = 325
        Public Const idSediMobile As Integer = 326
        Public Const idDiabetologiaMobile As Integer = 329
        Public Const idOftalmologiaMobile As Integer = 330
        Public Const idOncologiaMobile As Integer = 331
        Public Const idNetworkMobile As Integer = 332
        Public Const idInnovazioneMobile As Integer = 333
        Public Const idPipelineMobile As Integer = 334
        Public Const idPubblicazioniMobile As Integer = 335
        Public Const idApparatoRespiratorioMobile As Integer = 336
        Public Const idMalattieRareMobile As Integer = 338
        Public Const idNewsMobile As Integer = 346
        Public Const idPercheDompeMobile As Integer = 349
        Public Const idPercorsiProfessionaliMobile As Integer = 350
        Public Const idDownloadMobile As Integer = 356
        Public Const idLinksUsefulMobile As Integer = 357
        Public Const idInfiammazioneMobile As Integer = 337
        Public Const idTerapiaDoloreMobile As Integer = 219 'Non esiste nel db
        Public Const idAutomedicazioneMobile As Integer = 339
        Public Const idRespSocialeImpresaMobile As Integer = 340
        Public Const idAssociazioniPazientiMobile As Integer = 344
        Public Const IDEventiConferenzeMobile As Integer = 345
        Public Const IDProgettiIniziativeMobile As Integer = 343
        Public Const IDStudentiMobile As Integer = 351
        Public Const IDProfessionistiMobile As Integer = 352
        Public Const idSelectionRecruitingMobile As Integer = 353
        Public Const IDPosizioniAperteMobile As Integer = 354
        Public Const idThemaSezioneDashboardMobile As Integer = 358
        Public Const idComunicatiStampaMediaPressMobile As Integer = 359
        Public Const idSchedeIstituzionaliMediaPressMobile As Integer = 360
        Public Const idPhotoGalleryMediaPressMobile As Integer = 361
        Public Const idContattiMediaPressMobile As Integer = 362
        Public Const idPressContattiMobile As Integer = 408
        Public Const idRassegnaStampaMediaPressMobile As Integer = 363
        Public Const idLinksUsefulMediaPressMobile As Integer = 364
        Public Const idNewsMediaPressMobile As Integer = 366
        Public Const idDownloadMediaPressMobile As Integer = 365
        Public Const idContattiMobile As Integer = 367
        Public Const idDownloadContentMediaPressMobile As Integer = 368
        Public Const idComunicatiStampaPubbliciMobile As Integer = 372
        Public Const idMedicalHomeMobile As Integer = 374
        Public Const idArchivioRCPMobile As Integer = 375
        Public Const idBrandMobile As Integer = 376
        Public Const idFocusonMobile As Integer = 377
        Public Const idInfoutiliMobile As Integer = 378
        Public Const idNewsEventiMedicalMobile As Integer = 379
        Public Const idArticoloMedicalMobile As Integer = 380
        Public Const idDownloadMedicalMobile As Integer = 381
        Public Const idPhotogalleryMedicalMobile As Integer = 382
        Public Const idCondizioniUtilizzoMobile As Integer = 370
        Public Const idPrivacyMobile As Integer = 371
        Public Const idFarmacovigilanzaMobile As Integer = 396
        Public Const idModello231Mobile As Integer = 395
        Public Const idPoliticaQSMobile As Integer = 414

        Public Const idSDompeMobile As Integer = 384
        Public Const idAringhieriMobile As Integer = 385
        Public Const idAndreanoMobile As Integer = 386
        Public Const idProttiMobile As Integer = 387
        Public Const idDiMarinoMobile As Integer = 388        
        Public Const idAllegrettiMobile As Integer = 390
        Public Const idGiaquintoMobile As Integer = 391
        Public Const idConfaloneMobile As Integer = 420
        Public Const idPolimeniMobile As Integer = 424
		Public Const idAquilioMobile As Integer = 429

        Public Const idTrapiantiMobile As Integer = 392
        Public Const idSeiAprileMobile As Integer = 393
        Public Const IdStorieMobile As Integer = 394

        Public Const Modello231Mobile As Integer = 395
        Public Const partnerWelcomeMobile As Integer = 409
        Public Const partnerAboutUsMobile As Integer = 410
        Public Const partnerFocusOnMobile As Integer = 411
        Public Const partnerMedicalInfoMobile As Integer = 412
        Public Const partnerNewsMobile As Integer = 413
        Public Const idDrugDiscoveryMobile As Integer = 417
        Public Const idRarestOnesMobile As Integer = 419
		Public Const idWeCareMobile As Integer = 439	


        Public Class Domain
            Public Const DMP As String = "DMP"
            Public Const DMPM As String = "DMPM"
        End Class

        Public Class Name
            Public Const Home As String = "home"
            Public Const Footer As String = "footer"
            Public Const Header As String = "Header"

            Public Const Chisiamo As String = "ChiSiamo"
            Public Const Sviluppo As String = "Sviluppo"
            Public Const Soluzioni As String = "Soluzioni"
            Public Const Sociale As String = "Sociale"
            Public Const MediaEvent As String = "MediaEvent"
            Public Const Management As String = "Management"
            Public Const Tecnologie As String = "Tecnologie"
            Public Const News As String = "News"
            Public Const Giornalista As String = "Giornalist"

            Public Const Search As String = "Search"
            Public Const HomeSite As String = "homesite"
            Public Const Kolinterviews As String = "Kolinterws"
            Public Const ASK As String = "ASK"
            Public Const HDContact As String = "HDContact"
            Public Const Advisory As String = "Advisory"
        End Class

    End Class

    Public Class ContextCostant
        Public Const idMainContent = 1
        Public Const idManagementMain = 2
        Public Const idManagementSecondary = 3
    End Class

    Public Class SiteAreaConstants
        Public Const Management As String = "Management"
        Public Const News As String = "News"
        Public Const Women As String = "CorrectUse"
        Public Const Premature As String = "WhatIs"
        Public Const AskExperts As String = "AskDac"
        Public Const PressRoom As String = "PressRoom"
        Public Const PressKit As String = "PLYPresKit"
        Public Const MediaCoverage As String = "PLYMedCov"
        Public Const Download As String = "Download"
        Public Const IDDownloadArea As Integer = 94
        Public Const IDSiteareaLinks As Integer = 95
        Public Const IDSiteareaNews As Integer = 84
        Public Const IDSiteareaGiornalista As Integer = 85
        Public Const IDSiteareaPubblicazione As Integer = 72
        Public Const IDStoria As Integer = 59
        Public Const IDMission As Integer = 61
        Public Const IDManagement As Integer = 62
        Public Const IDSiteareaSedi As Integer = 63
        Public Const IDNetworkRD As Integer = 69
        Public Const IDEventiConferenze As Integer = 83
        Public Const IDProgettiIniziative As Integer = 81
        Public Const IDGalleryImagesPublic As Integer = 110
        Public Const IDRassegnaStampaAreaMediaPress As Integer = 101
        Public Const IDComunicatiStampaAreaMediaPress As Integer = 97
        Public Const IDSchedeIstituzionaliMediaPress As Integer = 98
        Public Const IDPhotoGalleryAreaMediaPress As Integer = 99
        Public Const IDDownloadAreaMediaPress As Integer = 103
        Public Const IDDownloadContenutiAreaMediaPress As Integer = 106
        Public Const IDSiteareaLinksMediaPress As Integer = 102
        Public Const IDSiteareaNewsMediaPress As Integer = 104
        Public Const IDSiteareaStudenti As Integer = 88
        Public Const IDSiteareaProfessionisti As Integer = 89
        Public Const IDSiteareaPosizioniAperte As Integer = 91
        Public Const IDComunicatiStampaAreaPubblica As Integer = 109
        Public Const IDMedicalArea_Dashboard As Integer = 111
        Public Const IDMedicalArea_ArchivioRCP As Integer = 112
        Public Const IDMedicalArea_Brand As Integer = 113
        Public Const IDMedicalArea_focuson As Integer = 114
        Public Const IDMedicalArea_Infoutili As Integer = 115
        Public Const IDMedicalArea_NewsEventi As Integer = 116
        Public Const IDMedicalArea_Download As Integer = 118
        Public Const IDMedicalArea_Photogallery As Integer = 119
        Public Const IDMedicalArea_Articolo As Integer = 117
        Public Const IDApparatorespiratorio As Integer = 73
        Public Const IDAreaPartner As Integer = 51
        Public Const IDInfiammazione As Integer = 74
        Public Const IDTerapiadolore As Integer = 75
        Public Const IDVideoGallery As Integer = 120
        Public Const IDSAPeople As Integer = 121
        Public Const IDSAStorie As Integer = 124
        Public Const IDSAPipeline As Integer = 71
        Public Const IDSASergioDmp As Integer = 132
        Public Const IDSAManagement As Integer = 62
        Public Const IDSAAringhieri As Integer = 133
        Public Const IDComunicazioneInterna As Integer = 136
        Public Const IDSiteareaRarestOnes As Integer = 139
        Public Const IDSiteMobile As Integer = 140
        Public Const IDSiteareaEticaTrasp As Integer = 143
        Public Const IDVideo As Integer = 159
    End Class

    Public Class ContentTypeConstants
        Public Class Domain
            Public Const DMP As String = "DMP"            
        End Class

        Public Class Name
            Public Const GenericCont As String = "GenCont"
            Public Const Generericfooter As String = "footer"
            Public Const Contact As String = "Contact"
            Public Const DownloadMaterials As String = "PLYDown"
            Public Const AdvisorBoard As String = "Advisor"
            Public Const AdvisorVideo As String = "AdrVideo"
            Public Const Archivio As String = "Archivio"
            Public Const Header As String = "Header"
            Public Const Ricerca As String = "Ricerca"
            Public Const Product As String = "Product"
            Public Const Management As String = "Management"
            Public Const Download As String = "Dwnload"



        End Class

        Public Class Id
            Public Const GenericContCType As Integer = 128
            Public Const GenericContCTypeMediaPress As Integer = 137
            Public Const DashboardContCTypeMediaPress As Integer = 139
            Public Const DMPDownload As Integer = 135
            Public Const contentDetailMobile As Integer = 143
            Public Const ContactContType As Integer = 129
            Public Const HeaderCType As Integer = 130
            Public Const FooterCtype As Integer = 131
            Public Const ArchivioCtype As Integer = 132
            Public Const RicercaCtype As Integer = 136
            Public Const ProductCtype As Integer = 140
            Public Const SediCtype As Integer = 147
            Public Const ManagementCtype As Integer = 148
            Public Const Download As Integer = 15
            Public Const ProcessoSelezioneCtype As Integer = 150
            Public Const PosizioniAperteCtype As Integer = 133
            Public Const FarmacoVigilanza As Integer = 168
            Public Const Contatti As Integer = 167
            Public Const Media As Integer = 162
            Public Const PressArchivio As Integer = 138
            Public Const PressDashboard As Integer = 139
            Public Const PressHome As Integer = 141
            Public Const PressPhotoGallery As Integer = 142
            Public Const PressDetail As Integer = 143
            Public Const PressContatti As Integer = 146
            Public Const IDProgettiIniziative As Integer = 149
            Public Const IDMedicalDashbordCtype As Integer = 151
            Public Const IDMedicalArchivioRCPCtype As Integer = 152
            Public Const IDMedicalBrandCtype As Integer = 153
            Public Const IDMedicalFocusOnCtype As Integer = 154
            Public Const IDMedicalInfoUtiliCtype As Integer = 155
            Public Const IDRcstArticoloCtype As Integer = 156
            Public Const IDMedicalPhotogalleryCtype As Integer = 142
            Public Const IDMedicalFocusOn As Integer = 170
			Public Const IDMotoreRicerca As Integer = 16
			
            Public Const PartnerAboutUs As Integer = 173
            Public Const PartnerFocusOn As Integer = 174
            Public Const PartnerMedicalInfo As Integer = 175
            Public Const ifIWere As Integer = 179
            Public Const IDVideo As Integer = 204
        End Class
    End Class

    Public Class Languages

        Public Const ITALIAN As Integer = 1
        Public Const ENGLISH As Integer = 2

    End Class

    Public Class ContextGroup

        Public Shared Jobopportunities As Integer = 56
        Public Shared CaratteristicheProdotti As Integer = 57

        Public Class Domain
            Public Const DMP As String = "DMP"
        End Class

        Public Class Name
            Public Const SearcByTag As String = "searcByTag"
            Public Const EduTools As String = "eduTools"
            Public Const CLCLocalizations As String = "localiz"

            Public Const Profession As String = "profession"
            Public Const Speciality As String = "speciality"
            Public Const CongressType As String = "congType"
        End Class

    End Class

    Public Class Profile
        Public Class Domain
            Public Const DMP As String = "DMP"
        End Class

        Public Class Name
            Public Const RegUser As String = "regUser"
        End Class

        Public Class ID
            Public Const ProfileDoctor As String = "10"
            Public Const ProfileMediaPress As String = "11"
            Public Const ProfilePartner As String = "12"
            Public Const ProfileSede As String = "13"
        End Class
    End Class


    Public Class Role
        Public Const RoleMediaPress As String = "63"
        Public Const RoleDoctor As String = "64"
        Public Const RolePartner As String = "65"
        Public Const RoleReview As String = "79"
        Public Const RoleSede As String = "78"
    End Class

    Public Class RelationType
        Public Const DocumentDedicateRelation As Integer = 4
        Public Const AreaTerapeutica As Integer = 6
        Public Const PrincipiAttivi As Integer = 7
        Public Const Brand As Integer = 8
        Public Const PartnerContent As Integer = 11
        Public Const LinkRelation As Integer = 12
        Public Const GalleryRelation As Integer = 13
        Public Const MeccanismoDazione As Integer = 14
    End Class

    Public Class TraceEvent
        Public Const SilentLoginDocCheck As Integer = 53
        Public Const FailedSilentLoginDocCheck As Integer = -1
    End Class
End Namespace

