﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.IO
Imports System.Web.Hosting
Imports System.Security.Cryptography
Imports System.Net
Imports System.Collections.Generic
Imports System.Configuration

Namespace HP3ContentPromotion.WebSite

    <WebService(Namespace:="http://schemas.HP3ContentPromotion.org/FileSync")> _
    <WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
    Public Class FileSync
        Inherits System.Web.Services.WebService

        Shared fileSyncAllowedIP As IPAddress
        Shared hashAlgorithm As HashAlgorithm = New MD5CryptoServiceProvider()

        Shared Sub New()
            fileSyncAllowedIP = IPAddress.Parse(ConfigurationManager.AppSettings("fileSyncAllowedIP"))
        End Sub

        Protected Overridable ReadOnly Property BaseDir() As String
            Get
                Return Path.Combine(HostingEnvironment.ApplicationPhysicalPath, ConfigurationManager.AppSettings("localPath"))
            End Get
        End Property

        Private Sub CheckCaller()
            Dim remoteIP As IPAddress = IPAddress.Parse(Context.Request.UserHostAddress())
            ' Controllo che l'ip remoto sia uguale a quello consentito
            If Not remoteIP.Equals(fileSyncAllowedIP) Then
                Throw New HttpException(403, "Not permitted")
            End If
        End Sub

        <WebMethod()> _
        Public Function GetFileHash(ByVal fileName As String) As Byte()
            CheckCaller()

            ' Può arrivare assoluto o relativo
            If Not Path.IsPathRooted(fileName) Then
                fileName = Path.Combine(BaseDir, fileName)
            End If

            ' Se non esiste restituisco un hash vuoto
            If Not File.Exists(fileName) Then Return New Byte() {}

            Using stream As Stream = File.OpenRead(fileName)
                ' Calcolo l'hash del file locale
                Dim localHash As Byte() = hashAlgorithm.ComputeHash(stream)
                Return localHash
            End Using

        End Function

        Private Shared Function CompareBytes(ByVal a As Byte(), ByVal b As Byte()) As Boolean
            Dim c As Integer
            For c = 0 To a.Length - 1
                If (b.Length < c OrElse b(c) <> a(c)) Then
                    Return False
                End If
            Next

            Return True
        End Function

        <WebMethod()> _
        Public Sub DeleteFile(ByVal fileName As String)
            CheckCaller()

            ' TODO: implementare check di sicurezza sul file
            If File.Exists(fileName) Then
                File.Delete(fileName)
            End If
        End Sub

        <WebMethod()> _
        Public Function GetFile(ByVal fileName As String) As Byte()
            CheckCaller()

            Return File.ReadAllBytes(fileName)
        End Function

        <WebMethod()> _
        Public Function GetFileNames(ByVal filePattern As String, ByVal includeSubDirectories As Boolean) As String()
            CheckCaller()

            ' TODO: validare nomi dei files
            Dim baseDir As String = Path.Combine(Me.BaseDir, Path.GetDirectoryName(filePattern))

            ' Se non esiste la directory non faccio niente
            If Not Directory.Exists(baseDir) Then Return New List(Of String)(0).ToArray()

            filePattern = Path.GetFileName(filePattern)
            If includeSubDirectories Then
                Return Directory.GetFiles(baseDir, filePattern, SearchOption.AllDirectories)
            Else
                Return Directory.GetFiles(baseDir, filePattern, SearchOption.TopDirectoryOnly)
            End If
        End Function

        <WebMethod()> _
        Public Sub PutFile(ByVal fileName As String, ByVal data() As Byte)
            CheckCaller()

            fileName = Path.Combine(BaseDir, fileName)
            Dim dir As String = Path.GetDirectoryName(fileName)
            ' Mi assicuro che la directory esista
            If Not Directory.Exists(dir) Then
                Directory.CreateDirectory(dir)
            End If

            ' TODO: implementare check di sicurezza sul file
            File.WriteAllBytes(fileName, data)
        End Sub

        <WebMethod()> _
        Public Sub DeleteAllFiles(ByVal filePattern As String, ByVal includeSubDirectories As Boolean)
            ' TODO: validare nomi dei files
            Dim baseDir As String = Path.Combine(Me.BaseDir, Path.GetDirectoryName(filePattern))
            filePattern = Path.GetFileName(filePattern)

            ' Se non esiste la directory non faccio niente
            If Not Directory.Exists(baseDir) Then Return

            Dim files() As String
            If includeSubDirectories Then
                files = Directory.GetFiles(baseDir, filePattern, SearchOption.AllDirectories)
            Else
                files = Directory.GetFiles(baseDir, filePattern, SearchOption.TopDirectoryOnly)
            End If

            ' Elimino ogni file
            Dim f As String
            For Each f In files
                File.Delete(f)
            Next
        End Sub

    End Class

End Namespace