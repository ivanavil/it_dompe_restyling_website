<%@ Import Namespace="Healthware.Dompe.Helper301" %>

<script language="VB" runat="server">
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
	
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        'Fires at the beginning of each request
	
		'Permanent Redirect Status 301----------
		Dim is301Url as New Helper301
		'is301Url.is301Url()
		'---------------------------------------
		
		'Handling of sitemap.xml----------------
		'If HttpContext.Current.Request.Url.ToString().Contains("sitemap.xml") Then  HttpContext.Current.Response.Redirect("/sitemap.aspx", False)
		'---------------------------------------
		
		'Patch for doccheck data 
        If HttpContext.Current.Request.Url.ToString().Contains("doccheck.aspx") Then
            HttpContext.Current.Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1")
        End If
        
        If Healthware.HP3.Core.Web.MasterPageManager.UseDriver("ShortUrlDriver") Then
            Dim man As New Healthware.HP3.Core.Web.MasterPageManager()
            Dim httpApplication As HttpApplication
            httpApplication = sender
            If httpApplication Is Nothing Then Return
			
            Dim url = HttpApplication.Request.Url.Scheme + "://" + HttpApplication.Request.Url.Authority + HttpApplication.Request.RawUrl
            Dim newurl = man.NormalizeUrl(url)
            If (Not String.Equals(newurl, url, StringComparison.OrdinalIgnoreCase) AndAlso Not String.Equals(newurl + "/default.aspx?", url, StringComparison.OrdinalIgnoreCase)) Then
                HttpApplication.Response.Redirect(newurl)
            End If
        End If
        
        'If (Not String.Equals(newurl, url, StringComparison.OrdinalIgnoreCase) AndAlso Not String.Equals(newurl + "/default.aspx?", url, StringComparison.OrdinalIgnoreCase)) Then
        '    httpApplication.Response.Redirect(newurl)
        'End If
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub
</script>