<%@ Page Language="vb" ClassName="mySiteSecurity" ValidateRequest="false" EnableEventValidation="false" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %> 
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script language="vb" runat="server"> 
    Sub page_load()
        If Me.BusinessSiteManager.IsSecurityAuthenticate Then Response.Redirect("/")
    End Sub

    Sub DoLogin(ByVal s As Object, ByVal e As ImageClickEventArgs)
        If ui.Text = String.Empty Or pw.Text = String.Empty Then
            lblMessage.Text = "Error!"
            Exit Sub
        End If
        If Not Me.BusinessSiteManager.CheckSecurityAuthentication(ui.Text, pw.Text) Then
            lblMessage.Text = "Login incorrect!"
        Else
            Response.Redirect("/")
        End If
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>HP3 Office</title>
	<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
</head>

<body>

<form id="form1" runat = "server" enctype="multipart/form-data">	
<div class="dAccessBox1">
    	
	
	
	<div id="dAccessForm" class="dloginBox" runat="server">
		
		<table class="form">
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		    <tr>
		        <td style="color:#577599;padding-left: 20px;">Username:</td>
		        <td><asp:textbox id="ui" cssclass ="tbUsernameLogin" runat="server" Width="120" /></td>
		    </tr>
		    <tr>
		        <td style="color:#577599;padding-left: 20px;">Password:</td>
		        <td><asp:textbox id="pw" textmode="password" cssclass ="tbPasswordLogin" runat="server"  Width="120" /></td>
				<td>&nbsp;</td>
				<td><asp:ImageButton src="/HP3Office/_slice/background/enter.gif" id="btnLogin" runat="server" cssClass="button" onClick="doLogin" />
				</td>
		    </tr>		
		</table>
		<br/>
		
			
	</div>

	<div class="LoginError"><asp:Label ID="lblMessage" runat="server" /></div>
	<div class="LoginError"><asp:literal id="litError" runat="server" visible ="false" /></div>
</div>


</form>	
</body>
</html>