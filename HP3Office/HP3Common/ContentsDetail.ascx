<%@ Control Language="VB" ClassName="ContentsDetail" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="HP3Office.CMS.Utility" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Forms.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="OptionExtra" Src ="~/hp3Office/HP3Parts/ctlOption_Extra.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ContentGeo" Src ="~/hp3Office/HP3Parts/ctlContentGeo.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Importants" Src ="~/hp3Office/HP3Parts/ctlImportants.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlContentEditorial" Src ="~/hp3Office/HP3Parts/ctlContentEditorialList.ascx" %>
<%@ Register TagPrefix="HP3" TagName="ContentsWorkFlow"   Src="~/hp3Office/HP3Common/ContentsWorkFlow.ascx"%> 
<%@ Register tagprefix="HP3" TagName ="ctlExtraField" Src ="~/hp3Office/HP3Parts/ctlExtraField.ascx"%>

<script runat="server">
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private _oContentManager As ContentAccManager
    Private _contentManager As ContentManager
    
    Private contentEditStatusValue As New ContentEditStatusValue
    
    Private _contentSearcher As ContentSearcher
    Private _contentValue As ContentValue    
    Private _contentCollection As ContentCollection
    
    Private objQs As New ObjQueryString
    Private objCMSUtility As New GenericUtility
    Private webRequest As New WebRequestValue
    
    Private hList As Hashtable
    Private strDomain As String
    Private idContent As Integer
    Private idLanguage As Integer
    Private idPrimakey As Integer
    Private _idContentType As Int32
    Private idThemes As Int32
    Private _strDomain As String
    Private _language As String
    Private _contentVersion As ContentValue
    Private isNew As Boolean
    Private strJS As String
        
            
    ''' <summary>
    ''' Controlla se in content service � stata caricato un controllo slave
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ExistSlave() As Boolean
        Get
            Return CType(Parent.Parent, Object).ExistSlave
        End Get
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
        
    Private Property ContentVersion() As Object
        Get
            Return _contentVersion
        End Get
        Set(ByVal value As Object)
            _contentVersion = value
        End Set
    End Property
          
    Private Property ExistWorkFlow() As Boolean
        Get
            Return ViewState("existWF")
        End Get
        Set(ByVal value As Boolean)
            ViewState("existWF") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Gestisce il salvataggio del controllo
    ''' Riempie un buffer per dialogare con il controllo slave   
    ''' Salva il controllo se non esiste il controllo slave nella pagina contentservice    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Save() As ContentStoreValue
        Dim objStoreContent As New ContentStoreValue
        Dim optionExtraColl As New ContentOptionExtraCollection
         
        webRequest.customParam.LoadQueryString()
        Dim versionId As String = webRequest.customParam.item("V")
        Dim idContent = webRequest.customParam.item("C")
        Dim idLang = webRequest.customParam.item("L")
        isNew = webRequest.customParam.item("N")
      
        Try
            'Riempio il buffer
            With objStoreContent
                'Recupero il content dalla pagina   

                .Content = ReadContentValue
                .NewItem = isNew
               
                'Oggetti accoppiati
                .SitesList = ReadSiteCollection
                .AreasList = ReadAreaCollection
            
                
                .ContentEditorialList = ReadContentEditorial
                .OptionExtraList = ReadOptionExtra
                .GeoList = ReadGeo()
                 
 
                'nel caso di ripristino
                'alla property ContentVersion() assegno la versione attuale
                If Not isNew And (versionId <> String.Empty) Then
                    ContentVersion() = ReadContent(idContent, idLang)
                End If
                
                'recupero dalla property ContentVersion il contentValue per il versioning
                If Not isNew Then
                    .ContentVersion = ContentVersion
                End If
            End With

            'Se non esiste il controllo slave
            'Salvo il content
            If Not ExistSlave Then
               
                optionExtraColl = ReadOptionExtra
               
                _contentValue = SaveContent(objStoreContent.Content)
                                
                If Not _contentValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    objStoreContent = _oContentManager.Create(objStoreContent, objStoreContent.Content.Key)
                Else
                    objStoreContent = Nothing
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStoreContent
    End Function
    
    ''' <summary>
    ''' Salvataggio del content
    ''' </summary>
    ''' <param name="_contentValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function SaveContent(ByVal _contentValue As ContentValue) As ContentValue
        Try
 
     
 
            Dim ContentManager As ContentManager = New ContentManager
            If isNew Then
                _contentValue = ContentManager.Create(_contentValue)
                If _contentValue.Key.IdGlobal = String.Empty Then ContentManager.Update(_contentValue.Key, "Key.IdGlobal", _contentValue.Key.Id)
            Else
                _contentValue = ContentManager.Update(_contentValue)
            End If
            
            Return _contentValue
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    ''' <summary>
    ''' Recupera i parametri
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadParam()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        
        'LoadListParam()
        'If hList Is Nothing Then
        '    isNew = WebUtility.MyPage(Me).isNew
        '    idLanguage = WebUtility.MyPage(Me).idCurrentLang
        '    _idContentType = WebUtility.MyPage(Me).idConsolleContentType
        '    idThemes = WebUtility.MyPage(Me).idThemes
        'Else
        '    isNew = hList("isNew")
        '    idLanguage = objCMSUtility.ReadLanguage(hList("idCurrentLang"))
        '    _idContentType = hList("idConsolleContentType")
        '    idThemes = hList("idThemes")
        'End If
        
        webRequest.customParam.LoadQueryString()
        isNew = webRequest.customParam.item("N")
        idLanguage = webRequest.customParam.item("L")
        _idContentType = webRequest.customParam.item("Ct")
        idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
        If idContent > 0 AndAlso idLanguage > 0 Then
            idPrimakey = ContentHelper.GetContentIdentificator(idContent, idLanguage).PrimaryKey
        End If
    End Sub
    
    ''' <summary>
    ''' Recupera i parametri iniziali 
    ''' nel caso di edit di una versione
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadInizialParam()
        txtSubCategory.Disable()
        idLanguage = objQs.Language_id
        idContent = objQs.ContentId
    End Sub
    
    ''' <summary>
    ''' Gestisce la visualizzazione dei controlli nella form
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadControls()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                ctl.style.add("display", strVis)
            End If
        Next
    End Sub
    
    Sub Page_Load()
        'Dim myUc As Control
        'myUc = Me.Parent.Parent.FindControl("SlavePage").Controls(0).EnableAllFormElements()
             
        'controlla se il content da visualizzare 
        '� una versione o il content attuale
        Dim versionId As String = ""
        'Response.Write(Request("page"))
        webRequest.customParam.LoadQueryString()
        
        versionId = webRequest.customParam.item("V")
        'questo metodo imposta idContent e IdLanguage 
        'in modo da recuperare tutti gli accoppiamenti (aree, siti ecc.)
        LoadInizialParam()
        
        'controlla se esiste un un workflow associato al tema
        If Not (Page.IsPostBack) Then CheckWorkFlow()
        
        'controlla se esistono extra fields associati a uno o pi� parmatri (tema/content type/content template)
        If Not (Page.IsPostBack) Then CheckExtraFields()
        
        'se � una versione
        If (versionId <> String.Empty) Then
            Dim obj As Object = ReadVersion(versionId)
            If Not (obj Is Nothing) Then
                'Se ha trovato il content lo carica  
                LoadControl(obj)
            End If
            
            DisableAllFormElements()
            btnRestore.Visible = True
        Else 'se � il content attuale
            LoadParam()
           
            'Ricordo l'ultimo url visitato
            GenericDataContainer.Add("LastUrl", Request.Url, True)
            Domain() = WebUtility.MyPage(Me).DomainInfo
            Language() = masterPageManager.GetLang.Id
            
            'Caricamento di un contenuto
            If idContent <> 0 Then
                'Modifico l'etichetta dei bottoni in base al tipo di salvataggio
                If Not (isNew) Then
                    CheckOutManagement()
                End If
                
                'Se esiste il controllo slave, ovvero il controllo che eredita da content
                'effettuo il caricamento da questo
                If Not ExistSlave Then
                    'Recupero il contentvalue
                    Dim contentValue As ContentValue = ReadContent(idContent, idLanguage)
                    'setta il valore della property ContentVersion
                    'assegnandogli la versione attuale 
                    ContentVersion() = contentValue
 
                    'Se ha trovato il content lo carica   
                    LoadControl(contentValue)
                   
                    'AMa #2310 17/11/2011 
                    oSite.LoadControl()
                    Aree.LoadControl()
                    
                End If
                
                If isNew Then
                    If lSelector.LoadContentLanguage(idContent) = False Then
                        CType(Parent.Parent, Object).GestButton("btnSalva", False, "Update")
                    End If
                    chkDisplay.Checked = True
                    chkActive.Checked = True
                    chkIndexable.Checked = True
                End If
            Else
                chkDisplay.Checked = True
                chkActive.Checked = True
                chkIndexable.Checked = True
                
                'setta la lingua per un nuovo content
                lSelector.IsNew = True
                lSelector.LoadDefaultValue()
                dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
                CType(Parent.Parent, Object).GestButton("btnSalva", True, "Save")
                InitControls()
            End If
                         
            ReadControls()
            'SetToolbar()
        End If
    End Sub
    
    'controlla se esistono dei campi extra
    Sub CheckExtraFields()
        Dim extFieldManager As New ExtraFieldsManager
        Dim extFieldSearcher As ExtraFieldsSearcher
        Dim totalColl As New ExtraFieldsCollection
      
        
        'Caso 1 - associazione a Theme 
        extFieldSearcher = New ExtraFieldsSearcher
        extFieldSearcher.Template = ExtraFieldsValue.ExtraFieldsTemplate.ContentTemplate
        extFieldSearcher.KeyTheme.Id = Me.BusinessMasterPageManager.Request("T")
               
        extFieldManager.Cache = False
        Dim collTCt As ExtraFieldsCollection = extFieldManager.Read(extFieldSearcher)
              
        If Not (collTCt Is Nothing) AndAlso (collTCt.Count > 0) Then
          
            AddExtraFiledToColl(collTCt, totalColl)
        End If
                
        'Caso 2 - associazione a ContentTemplate
        Dim templateId As Integer = 0
        extFieldSearcher = New ExtraFieldsSearcher
        extFieldSearcher.Template = ExtraFieldsValue.ExtraFieldsTemplate.ContentTemplate
        templateId = GetContentTemplate()
        If (templateId <> 0) Then extFieldSearcher.KeyContentTemplate.Id = templateId
        
        extFieldManager.Cache = False
        Dim collCt As ExtraFieldsCollection = extFieldManager.Read(extFieldSearcher)
        
        If Not (collCt Is Nothing) AndAlso (collCt.Count > 0) Then
            AddExtraFiledToColl(collCt, totalColl)
        End If
       
        'Caso 3 - associazione a ContentType
        extFieldSearcher = New ExtraFieldsSearcher
        extFieldSearcher.Template = ExtraFieldsValue.ExtraFieldsTemplate.ContentTemplate
        extFieldSearcher.KeyContentType.Id = Me.BusinessMasterPageManager.Request("Ct")
                       
        extFieldManager.Cache = False
        Dim collTCtt As ExtraFieldsCollection = extFieldManager.Read(extFieldSearcher)
               
        If Not (collTCtt Is Nothing) AndAlso (collTCtt.Count > 0) Then
            
            AddExtraFiledToColl(collTCtt, totalColl)
        End If
    End Sub
    
    'popola la collection di tutti gli extra field e fa il binding del reapeater
    Sub AddExtraFiledToColl(ByRef coll As ExtraFieldsCollection, ByRef totColl As ExtraFieldsCollection)
              
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
                       
            For Each field As ExtraFieldsValue In coll
                totColl.Add(field)
            Next
          
            rp_extf.DataSource = totColl
            rp_extf.DataBind()
               
            rp_extf.Visible = True
                
        End If
                           
    End Sub
    
    Sub LoadExtraFields(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim extraFId As Literal = CType(e.Item.FindControl("extFieldId"), Literal)
        Dim textField As ctlExtraField = CType(e.Item.FindControl("extrFielV"), ctlExtraField)
        
        If Not extraFId Is Nothing AndAlso Not textField Is Nothing Then
                       
            Select Case textField.TypeControl
                Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                    Dim vo As ExtraFieldsStoreValue = ReadExtraFieldValue(extraFId.Text)
                                         
                    If Not vo Is Nothing AndAlso vo.Key.Id Then
                        textField.Text() = vo.Value
                        textField.Indexable = vo.Indexable
                    End If
                  
                Case ctlExtraField.ControlType.FullTextType
                    Dim vo As ExtraFieldsStoreValue = ReadExtraFieldValue(extraFId.Text)
                    
                    If Not vo Is Nothing AndAlso vo.Key.Id Then
                        textField.Text = vo.Value
                        textField.Indexable = vo.Indexable
                    End If
                   
                Case ctlExtraField.ControlType.BooleanType
                    Dim vo As ExtraFieldsStoreValue = ReadExtraFieldValue(extraFId.Text)
                    
                    If Not vo Is Nothing AndAlso vo.Key.Id Then
                        Dim extFcheck As String = vo.Value
                                      
                        If (extFcheck = "True") Then
                            textField.Checked() = True
                        ElseIf (extFcheck = "False") Then
                            textField.Checked() = False
                        End If
                    End If
            End Select

        End If
    End Sub
    
    Function ReadExtraFieldValue(ByVal extFId As String) As ExtraFieldsStoreValue
        Dim extFieldManager As New ExtraFieldsManager
        If (extFId <> String.Empty) And (webRequest.customParam.item("C") <> 0) Then
            Dim extFStoreSearcher As New ExtraFieldsStoreSearcher
        
            extFStoreSearcher.KeyExtraField.Id = CType(extFId, Integer)
            'extFStoreSearcher.KeyContent.PrimaryKey = Me.BusinessContentManager.ReadPrimaryKey(CType(webRequest.customParam.item("C"), Integer), CType(webRequest.customParam.item("L"), Integer))
            extFStoreSearcher.KeyContent.PrimaryKey = ContentHelper.GetContentIdentificator(CType(webRequest.customParam.item("C"), Integer), CType(webRequest.customParam.item("L"), Integer)).PrimaryKey
           
            
            extFieldManager.Cache = False
            Dim extrFColl As ExtraFieldsStoreCollection = extFieldManager.Read(extFStoreSearcher)
            If Not (extrFColl Is Nothing) AndAlso (extrFColl.Count > 0) Then
                Dim extFieldValue As ExtraFieldsStoreValue = extFieldManager.Read(extFStoreSearcher)(0)
                
                
                Return extFieldValue
            End If
        End If
        
        Return Nothing
    End Function
        
    Public Sub SaveExtraFields(ByVal contentKey As Integer)
        If (rp_extf.Items.Count > 0) Then
            Dim extFieldManager As New ExtraFieldsManager
            
            For Each elem As RepeaterItem In rp_extf.Items
                Dim extraFId As Literal = CType(elem.FindControl("extFieldId"), Literal)
                Dim textField As ctlExtraField = CType(elem.FindControl("extrFielV"), ctlExtraField)
                
                Dim extFValue As New ExtraFieldsStoreValue
                extFValue.KeyExtraField.Id = CType(extraFId.Text, Integer)
                extFValue.KeyContent.PrimaryKey = contentKey
                
                Select Case textField.TypeControl
                    Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                        extFValue.Value = textField.Text
                        extFValue.Indexable = textField.Indexable
                        
                    Case ctlExtraField.ControlType.FullTextType
                        extFValue.Value = textField.Text
                        extFValue.Indexable = textField.Indexable
                        
                    Case ctlExtraField.ControlType.BooleanType
                        If (textField.Checked = True) Then
                            extFValue.Value = "True"
                        ElseIf (textField.Checked = False) Then
                            extFValue.Value = "False"
                        End If
                End Select
               
                If isNew Then  ' create user
                    extFieldManager.Create(extFValue)
                                       
                Else 'update user
                    Dim extrFSearcher As New ExtraFieldsStoreSearcher
                    extrFSearcher.KeyExtraField.Id = CType(extraFId.Text, Integer)
                    extrFSearcher.KeyContent.PrimaryKey = contentKey
                        
                    extFieldManager.Cache = False
                    Dim extFielColl As ExtraFieldsStoreCollection = extFieldManager.Read(extrFSearcher)
                    If Not (extFielColl Is Nothing) AndAlso (extFielColl.Count > 0) Then
                        extFValue.Key = extFielColl(0).Key
                        extFieldManager.Update(extFValue)
                    Else
                        extFieldManager.Create(extFValue)
                    End If
                End If
            Next
       
        End If
    End Sub
    
    Function GetContentTemplate() As Integer
        Dim CTypeManager As New ContentTypeManager
                   
        Dim CTypeValue As ContentTypeValue = CTypeManager.Read(Me.BusinessMasterPageManager.GetContentType)
        If Not (CTypeValue Is Nothing) Then
            Return CTypeValue.KeyContentsTemplate.Id
        End If
        
        Return 0
    End Function
    
    'controlla se esiste un content approval workflow 
    'associato ad un tema
    Sub CheckWorkFlow()
        Dim themeId As Integer = Request("T")
        Dim wfId As Integer = 0
        ExistWorkFlow() = False
        
        Dim themeValue As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(themeId))
        If Not (themeValue Is Nothing) Then
            wfId = themeValue.KeyWorkFlow.Id
        End If
        
        If (wfId <> 0) Then
            ExistWorkFlow() = True
        End If
    End Sub
    
    'gestisce il check-out/check-in di un content
    Sub CheckOutManagement()
        'controllo se il content � nello stato di check-in 
        'passandogli la primaryKey del content e l'id dell'utente che utilizza la consolle
        If Not Me.BusinessStatusManager.IsEditCheckOut(idPrimakey, Me.BusinessMasterPageManager.GetTicket.User.Key.Id) Or (Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CMS", "cmsadmin")) Then
            btnUndoCheckOut.Visible = True
            CType(Parent.Parent, Object).GestButton("btnSalva", True, "Update")
        Else
            btnUndoCheckOut.Visible = False
            CType(Parent.Parent, Object).GestButton("btnSalva", False, "Update")
        End If
               
        'se il content � in stato di check-out ed � in edit
        'visualizziamo le informazioni relative al check-out
        If Me.BusinessStatusManager.IsEditCheckOut(idPrimakey) And Not isNew Then
                          
            contentEditStatusValue = Me.BusinessStatusManager.ReadEditStatus(idPrimakey)
            Dim userValue As UserValue = getAuthor(contentEditStatusValue.KeyUser)
                        
            btnCheckOut.Visible = True
            btnCheckOut.Alt = "Checked out to " & userValue.Surname & " " & userValue.Surname & " exclusively"
       
            If Me.BusinessStatusManager.IsEditCheckOut(idPrimakey, Me.BusinessMasterPageManager.GetTicket.User.Key.Id) Then
                DisableAllFormElements()
            End If
        End If
    End Sub
    
    'effettua undo check-out del content
    Sub UndoContentCheckOut(ByVal sender As Object, ByVal envent As ImageClickEventArgs)
        'l'operazione di undo check-out su un content pu� essere fatta solo
        'dall'utente che ha fatto il check-out sul content o da un utente che ha un ruolo di amministratore
        If Not Me.BusinessStatusManager.IsEditCheckOut(idPrimakey, Me.ObjectTicket.User.Key.Id) Or (Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CMS", "cmsadmin")) Then
            Dim contentEditStatus As ContentEditStatusValue = Me.BusinessStatusManager.ReadEditStatus(idPrimakey)
            
            If Not (contentEditStatus Is Nothing) Then
                'elimino il check-out senza fare il check-in
                Me.BusinessStatusManager.Remove(contentEditStatus.Key)
                
                btnCheckOut.Visible = False
                btnUndoCheckOut.Visible = False
                'btnCheckIn.Visible = True
                BackToList()
            End If
        End If
    End Sub
    
    'restituisce uno uservalue dato un UserIdentificator
    Function getAuthor(ByVal userIdent As UserIdentificator) As UserValue
        Return Me.BusinessUserManager.Read(userIdent)
    End Function
    
    'legge una versione in base al suo Identificatore
    'restituisce l'oggetto memorizzato nella versione
    Function ReadVersion(ByVal versionId As Integer) As Object
        Dim versioningManager As New VersioningManager
        Dim versionIdent As New VersionIdentificator
        
        versionIdent.Id = versionId
        Dim versionValue As VersionValue = versioningManager.Read(versionIdent)
        Return versionValue.StoredObject
    End Function
    
    Function ReadContent(ByVal idContent As Int32, ByVal idLanguage As Int32) As ContentValue
        Dim oContentSearcher As New ContentSearcher
        oContentSearcher.key.IdLanguage = idLanguage
        oContentSearcher.key.Id = idContent
        ' oContentSearcher.Delete = SelectOperation.All
        oContentSearcher.Active = SelectOperation.All
        oContentSearcher.Display = SelectOperation.All
        oContentSearcher.LoadOwnerDetails = True 'M1168
   
        Dim oContentCollection As ContentCollection = objCMSUtility.GetContentCollection(oContentSearcher)
        If Not oContentCollection Is Nothing AndAlso oContentCollection.Count > 0 Then
            'oContentCollection = oContentCollection.DistinctBy("Key.PrimaryKey")
            _contentValue = oContentCollection.Item(0)
         
        End If
      
        Return _contentValue
    End Function
    
    'restituisce lo status attuale del content approval workflow
    Function ReadWorkFlowStatus() As Integer
        Me.BusinessContentManager.Cache = False
        Dim contentValue As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(Request("C"), Request("L")))
   
        Return contentValue.ApprovalStatus
    End Function
    
    'recupera lo status attuale del workflow del content
    Function ReadContentWFStatus() As String
        Dim status As String = ""
        
        Me.BusinessContentManager.Cache = False
        Dim contentValue As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(Request("C"), Request("L")))
        
        If (contentValue.ApprovalStatus = Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.ToBeApproved) Then
            status = "<img  src=""/hp3Office/HP3Image/Ico/contentwf_tobeapproval.gif"" alt=""Approval WorkFlow Status: to be approved"" />"
        ElseIf (contentValue.ApprovalStatus = Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.Approved) Then
            status = "<img  src=""/hp3Office/HP3Image/Ico/contentwf_approval.gif"" alt=""Approval WorkFlow Status: approved"" />"
        ElseIf (contentValue.ApprovalStatus = Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.NotApproved) Then
            status = "<img  src=""/hp3Office/HP3Image/Ico/contentwf_notapproval.gif"" alt=""Approval WorkFlow Status: not approved"" />"
        End If
        
        Return status
    End Function
   
    'ripristina una versione 
    'attivando nuovamente tutti gli elementi del form
    Sub RestoreItem(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        CType(Me.Parent.Parent, Object).EnableButton()
        CType(Me.Parent.Parent.FindControl("SlavePage").Controls(0), Object).EnableAllFormElements()
        
        EnableAllFormElements()
        btnRestore.Visible = False
    End Sub
        
    'visualizza il pannello della gestione del approval workflow
    Sub ShowWorkFlow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (divWF.Visible = True) Then
            divWF.Visible = False
        Else
            divWF.Visible = True
        End If
            
    End Sub
    
    'rimanda alla lista dei contents    
    Sub BackToList()
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al lista dei content
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
                
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
       
    ''' <summary>
    ''' Imposta la toolbar
    ''' </summary>
    ''' <remarks></remarks>
    Sub SetToolbar()
        'If idContent <> 0 Then
        WebUtility.MyPage(Me).GetToolbar.AddForVisibilty("ShowControl", True)
        'Else
        '    WebUtility.MyPage(Me).GetToolbar.AddForVisibilty("ShowControl", False)
        'End If
    End Sub
    
    ''' <summary>
    ''' Inizializzo i controlli che gestiscono gli accoppiamenti con il content
    ''' </summary>
    ''' <remarks></remarks>
    Sub InitControls()
        'nel caso dell'edit di una versione
        'bisogner� controllare se l'oggetto serializzato 
        'che contiene le info sugli accoppiamenti esiste - in quel caso caricare le giuste informazioni
        'altrimenti caricare gli accoppiamenti memorizzati per il content attuale (attualmente si comporta cos�!)
        
        'Passaggio di paramentri agli oggetti che gestiscono gli accoppiamenti del content       
       
        oSite.IsNew = isNew
        oSite.idLanguage = idLanguage
        oSite.idRelated = idContent
        
        'Option extra
        ctlOE.idLanguage = idLanguage
        ctlOE.idRelated = idContent
       
        'Aree a cui � associato il content
        Aree.idRelated = idContent
        Aree.IsNew = isNew
        Aree.idLanguage = idLanguage
        
        'Contents Editorial associbili al content
        ctlCE.idRelated = idContent
        ctlCE.LoadControl()
        
        'Contents Geo associbili al content
      
        ctlContentGeo.idLanguage = idLanguage
        ctlContentGeo.idRelated = idContent
 
        ctlContentGeo.LoadGeo()
       
        'Carico esplicitamente i controlli tramite il metodo LoadControl
        'L'evento load non � affidabile per il caricamento di questi
        oSite.LoadControl()
        Aree.LoadControl()
        ctlOE.LoadControl()
    End Sub
    
    ''' <summary>
    ''' Caricamento dei controlli dall'objectvalue
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadControl(ByVal oContentValue As ContentValue, Optional ByRef _isNew As Boolean = False)
      
        If Not (oContentValue Is Nothing) Then
            menuTitle.Text = oContentValue.Title
            menuId.Text = oContentValue.Key.Id
            menuPK.Text = oContentValue.Key.PrimaryKey
            menuGK.Text = oContentValue.Key.IdGlobal
        End If
        
        'Inizializzazione dei controlli per la gestione degli accoppiamenti
        InitControls()
               
        'Il content � nothing
        If oContentValue Is Nothing Then
            'Caso particolare.....
            'L'id content viene passato , ma nel DB non esiste
            'Ci� accade quando cambio la lingua e non esiste il content associato
            'Simulo il comportamento di nuovo content per forzare la InserNew piuttosto che l'Update
            isNew = True
            
            'Selettore della lingua del content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
        Else
                        
            With oContentValue
               
                If Not isNew Then
                    'Caricamento dei controlli dal contentvalue
                    If .DateCreate.HasValue Then
                        DateCreate.Value = .DateCreate.Value
                    Else
                        DateCreate.Clear()
                    End If
                         
                    If .DateExpiry.HasValue Then
                        DateExpiry.Value = .DateExpiry.Value
                    Else
                        DateExpiry.Clear()
                    End If
             
                    If .DateInsert.HasValue Then
                        DateInsert.Value = .DateInsert.Value
                    Else
                        DateInsert.Clear()
                    End If
           
                    If .DateUpdate.HasValue Then
                        DateUpdate.Value = .DateUpdate.Value
                    Else
                        DateUpdate.Clear()
                    End If
                        
                    If .DatePublish.HasValue Then
                        DatePublish.Value = .DatePublish.Value
                    Else
                        DatePublish.Clear()
                    End If
                    
                    If .Key.IdGlobal = String.Empty Then .Key.IdGlobal = .Key.Id
                                                          
                    KeyGlobal.Text = .Key.IdGlobal
                    Title.Text = .Title
                    MetaKeywords.Text = .MetaKeywords
                    MetaDescription.Text = .MetaDescription
                    PageTitle.Text = .PageTitle
                    ' Launch.Value = .Launch
                    Launch.Content = .Launch
                    txtCode.Text = .Code
                    'M1168
                    If Not .Owner Is Nothing AndAlso Not .Owner.Key Is Nothing AndAlso .Owner.Key.Id > 0 Then
                        txtOwnerId.Text = .Owner.Key.Id
                        txtOwner.Text = .Owner.Name + " " + .Owner.Surname
                    End If
                    txtCopyright.Text = .Copyright
                    txtSubCategory.Text = .IdContentSubCategory
                    txtLinkLabel.Text = .LinkLabel
                    txtLinkUrl.Text = .LinkUrl
                    txtQuantit�.Text = .Qty
                    
                    If .BitMaskFlags.HasValue Then
                        txtFlags.Text = .BitMaskFlags
                    Else
                        txtFlags.Text = ""
                    End If
                    
                    ctlImportants.Importants = .Importants
                                            
                    chkDisplay.Checked = .Display
                    chkActive.Checked = .Active
                    chkIndexable.Checked = .Indexable
                    
                    lSelector.LoadLanguageValue(.Key.IdLanguage)
                                                
                    dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(.ApprovalStatus))
                End If
                If isNew Then
                    lSelector.Enabled = True
                Else
                    lSelector.Enabled = False
                End If
            End With
        End If
               
        _isNew = isNew
        
    End Sub
    
    ''' <summary>
    ''' Restituisce il contentvalue relativo alla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ReadContentValue() As Object
        Get
            Dim cValue As New ContentValue
            'nel caso del versioning
            If (idContent = 0) Then
                idContent = objQs.ContentId
            End If
            
            If (_idContentType = 0) Then
                _idContentType = objQs.ContentType_Id
            End If

            cValue.DateExpiry = DateExpiry.Value
            cValue.DateInsert = DateInsert.Value
            cValue.DatePublish = DatePublish.Value
            DateUpdate.Value = DateTime.Now 'aggiorna la data di modifica del content
            cValue.DateUpdate = DateUpdate.Value
            cValue.DateCreate = DateCreate.Value
            If KeyGlobal.Text.Length > 0 Then cValue.Key.IdGlobal = KeyGlobal.Text
            cValue.Title = Title.Text
            cValue.MetaKeywords = MetaKeywords.Text
            cValue.MetaDescription = MetaDescription.Text
            cValue.PageTitle = PageTitle.Text
            'cValue.Launch = Launch.Value
            cValue.Launch = Launch.Content
            cValue.ContentType.Key.Id = _idContentType
            cValue.Active = chkActive.Checked
            cValue.Display = chkDisplay.Checked
            cValue.indexable = chkIndexable.Checked
            'M1168
            cValue.Owner.Key.Id = IIf(txtOwnerId.Text = "", 0, txtOwnerId.Text)
            cValue.Code = txtCode.Text
            cValue.Copyright = txtCopyright.Text
            cValue.Importants = ctlImportants.Importants
            cValue.LinkLabel = txtLinkLabel.Text
            cValue.LinkUrl = txtLinkUrl.Text
            'cValue.BitMaskFlags = IIf(txtQuantit�.Text = "", 0, txtFlags.Text)
            If Not String.IsNullOrEmpty(txtFlags.Text) Then cValue.BitMaskFlags = txtFlags.Text
            cValue.Qty = IIf(txtQuantit�.Text = "", 0, txtQuantit�.Text)
           
            cValue.IdContentSubCategory = IIf(txtSubCategory.Text = "", 0, txtSubCategory.Text)
            cValue.Key.IdLanguage = lSelector.Language
            cValue.Key.Id = idContent
           
           
            If Not (isNew) Then
                  
                cValue.Key.PrimaryKey = ReadContent(idContent, lSelector.Language).Key.PrimaryKey
             
            End If

            'se esiste un content approval workflow associato al tema
            If (ExistWorkFlow()) Then
                If Not (isNew) Then 'update  content
                    cValue.ApprovalStatus = ReadWorkFlowStatus()
                Else 'nuovo content
                    cValue.ApprovalStatus = ContentValue.ApprovalWFStatus.ToBeApproved
                End If
            Else 'non esiste un workflow associato
                Select Case dwlApprovalStatus.SelectedItem.Value
                    Case ContentValue.ApprovalWFStatus.Approved
                        cValue.ApprovalStatus = ContentValue.ApprovalWFStatus.Approved
                    Case ContentValue.ApprovalWFStatus.ToBeApproved
                        cValue.ApprovalStatus = ContentValue.ApprovalWFStatus.ToBeApproved
                    Case ContentValue.ApprovalWFStatus.NotApproved
                        cValue.ApprovalStatus = ContentValue.ApprovalWFStatus.NotApproved
                End Select
            End If

            Return cValue
        End Get
    End Property
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
        
    Public ReadOnly Property ReadContentEditorial() As Object
        
        Get
            Return ctlCE.GetSelection
        End Get
    End Property
    
    Public ReadOnly Property ReadSiteCollection() As Object
        Get
            Return oSite.GetSelection
        End Get
    End Property
            
    Public ReadOnly Property ReadAreaCollection() As Object
        Get
            Return Aree.GetSelection()
        End Get
    End Property
    
    Public ReadOnly Property ReadOptionExtra() As Object
        Get
            Return ctlOE.OptionsExtra
        End Get
    End Property
    
    Public ReadOnly Property ReadGeo() As Object
        Get
            Return ctlContentGeo.GetSelection()
        End Get
    End Property
    
    'abilita tutti gli elementi del form 
    'il bottone Update/Salve e il bottone New
    Sub EnableAllFormElements()
        KeyGlobal.Enable()
        Title.Enable()
        MetaKeywords.Enable()
        MetaDescription.Enable()
        PageTitle.Enable()
        txtSubCategory.Enable()
        txtCode.Enable()
        txtCopyright.Enable()
        txtLinkLabel.Enable()
        txtLinkUrl.Enable()
        dwlApprovalStatus.Enabled = True
        txtQuantit�.Enable()
        txtFlags.Enable()
        
        divTxtEditor.Visible = False
        
        DateInsert.Enable = True
        DateInsert.Enabled()
        DateCreate.Enable = True
        DateCreate.Enabled()
        DateExpiry.Enable = True
        DateExpiry.Enabled()
        DatePublish.Enable = True
        DatePublish.Enabled()
        DateUpdate.Enable = True
        DateUpdate.Enabled()
                
        lSelector.Enabled = True
        chkActive.Enabled = True
        chkDisplay.Enabled = True
        chkIndexable.Enabled = True
        
        oSite.Enable = True
        oSite.Enabled()
        
        Aree.Enable = True
        Aree.Enabled()
        
        ctlCE.Enable = True
        ctlCE.Enabled()
        
        ' ctlContentGeo.Enable = True
        ctlContentGeo.Enabled()
        
        ctlOE.Enabled()
        ctlImportants.Enable()
    End Sub
    
    'disabilita tutti gli elementi del form 
    'il bottone Update/Salve e il bottone New
    Sub DisableAllFormElements()
        KeyGlobal.Disable()
        Title.Disable()
        MetaKeywords.Disable()
        MetaDescription.Disable()
        PageTitle.Disable()
        txtCode.Disable()
        txtSubCategory.Disable()
        txtCopyright.Disable()
        txtLinkLabel.Disable()
        txtLinkUrl.Disable()
        txtQuantit�.Disable()
        txtFlags.Enable()
        
        divTxtEditor.Visible = True
        
        DateInsert.Enable = False
        DateInsert.Disable()
        DateCreate.Enable = False
        DateCreate.Disable()
        DateExpiry.Enable = False
        DateExpiry.Disable()
        DatePublish.Enable = False
        DatePublish.Disable()
        DateUpdate.Enable = False
        DateUpdate.Disable()
                
        lSelector.Enabled = False
        chkActive.Enabled = False
        chkDisplay.Enabled = False
        chkIndexable.Enabled = False
        
        oSite.Enable = False
        oSite.Disable()
        
        Aree.Enable = False
        Aree.Disable()
        
        ctlCE.Enable = False
        ctlCE.Disable()
        
        'ctlContentGeo.Enable = False
        ctlContentGeo.Disable()
        
        ctlOE.Disable()
        ctlImportants.Disable()
    End Sub
            
 
</script>

<div class="topLastViewed">
     <table class="form">
        <tr>
           <td class="menuBar" valign="middle"><a href="#" onclick="window.open('<%=Domain%>/Popups/popShowControl.aspx?idService=<%=idThemes%>','','width=600,height=500,scrollbars=yes');return false"><img  src='/hp3Office/HP3Image/Ico/show_control.gif' alt="Show Controls" /></a></td>
           <%If Not isNew Then%>
               <%If ExistWorkFlow() Then%>
               <td class="menuBar" valign="middle"><asp:ImageButton id="btnshowWF" runat="server"  OnClick="ShowWorkFlow" ToolTip="Show Content Approval WorkFlow" ImageUrl="~/HP3Office/HP3Image/ico/showWF.png"/></td>
               <%End If %>
               <td class="menuBar" valign="middle"><img id="btnCheckOut" src="~/HP3Office/HP3Image/Ico/content_checkout_22.gif" visible="false" runat="server" alt=""/><asp:ImageButton ID ="btnUndoCheckOut" OnClick="UndoContentCheckOut" runat="server" ToolTip="Undo CheckOut"  Visible="false" ImageUrl="~/HP3Office/HP3Image/ico/content_undo_checkout_22.gif"/></td>
               <td class="menuBar" valign="middle"><asp:ImageButton id="btnRestore" OnClick="RestoreItem" runat="server"  Visible="false" ImageUrl="~/HP3Office/HP3Image/ico/content_rollback_22.gif"/>&nbsp;|</td>
               <td class="menuBar" valign="middle"><span class="title"><asp:Label id="menuTitle" ToolTip ="Title" runat="server"/></span>&nbsp;|</td>
               <td class="menuBar" valign="middle"><span class="title"><asp:Label id="menuId" ToolTip ="Id" runat="server"/></span>&nbsp;|</td>
               <td class="menuBar" valign="middle"><span class="title"><asp:Label ToolTip ="PrimaryKey" id="menuPK" runat="server"/></span>&nbsp;|</td>
               <td class="menuBar" valign="middle"><span class="title"><asp:Label ToolTip ="Global Key" id="menuGK" runat="server"/></span>&nbsp;|</td>
               <%If ExistWorkFlow() Then%>
               <td style="text-align:left"><%=ReadContentWFStatus()%></td>
               <%End If %>
          <%End If%>
        </tr>
    </table>
</div> 

<div id="divWF" style="margin-top:5px;color:#336699" visible="false" runat="server">
    <%If ExistWorkFlow() Then%>
       <HP3:ContentsWorkFlow id="ctlWorkFlow" runat="server" />
    <%End If %>
</div>

 <hr />  
    <table  class="form" width="99%">
        <tr><td style="width:25%" valign ="top"></td><td></td></tr>
        <tr runat="server" id="tr_lSelector">
            <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentLang&Help=cms_HelpContentLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentLang", "Content Language")%></td>
            <td><HP3:contentLanguage ID="lSelector" runat="server"  Typecontrol="StandardControl" /></td>
        </tr>
        <%If Not ExistWorkFlow() Then%>
        <tr id="apprStatus" runat="server" >
           <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentApr&Help=cms_HelpContentApr&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentApr", "Content Approval")%></td>
            <td>
               <asp:DropDownList id="dwlApprovalStatus" runat="server">
                    <asp:ListItem  Text="To Be Approved" Value="2"></asp:ListItem>
                    <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
                    <asp:ListItem  Text="Not Approved" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%End If %>
        <tr runat="server" id="tr_KeyGlobal">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyGlobal&Help=cms_HelpKeyGlobal&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyGlobal", "Global Key")%></td>
            <td><HP3:Text runat="server" ID="KeyGlobal" TypeControl="TextBox"  MaxLength="50" Style="width:100px" /></td>
        </tr>
        <tr runat="server" id="tr_DateInsert">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateInsert&Help=cms_HelpDateInsert&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateInsert", "Date Insert")%></td>
            <td><HP3:Date runat="server" ID="DateInsert" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateUpdate">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateUpdate&Help=cms_HelpDateUpdate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateUpdate", "Date Update")%></td>
            <td><HP3:Date runat="server" ID="DateUpdate" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateCreate">
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateCreate&Help=cms_HelpDateCreate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateCreate", "Date Create")%></td>
            <td><HP3:Date runat="server" ID="DateCreate" TypeControl="JsCalendar" ShowTime="true" /></td>
        </tr>
        <tr runat="server" id="tr_DatePublish">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDatePublish&Help=cms_HelpDatePublish&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDatePublish", "Date Publish")%></td>
            <td><HP3:Date runat="server" ID="DatePublish" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateExpiry">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateExpiry&Help=cms_HelpDateExpiry&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateExpiry", "Date Expiry")%></td>
            <td><HP3:Date runat="server" ID="DateExpiry" TypeControl="JsCalendar" ShowTime="true" /></td>
        </tr>
        <tr runat="server" id="tr_Title">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
        <td><HP3:Text runat="server" ID="Title" TypeControl="TextBox" Style="width:600px"/></td>
        </tr>

        <tr runat="server" id="tr_MetaKeywords">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=csm_FormPMK&Help=csm_HelpPMK&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("csm_FormPMK", "Page Meta Keywords")%></td>
            <td><HP3:Text runat="server" ID="MetaKeywords" TypeControl="Textbox" Style = "width:600px"/></td>
        </tr>

        <tr runat="server" id="tr_MetaDescription">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPMD&Help=cms_HelpPMD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPMD", "Page Meta Description")%></td>
            <td><HP3:Text runat="server" ID="MetaDescription" TypeControl="TextArea" Style = "width:600px"/></td>
        </tr>

        <tr runat="server" id="tr_PageTitle">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPageTitle&Help=cms_HelpPageTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPageTitle", "Page Title")%></td>
            <td><HP3:Text runat="server" ID="PageTitle" TypeControl="Textbox" Style = "width:600px"/></td>
        </tr>

        <tr runat="server" id="tr_Launch">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLaunch&Help=cms_HelpLaunch&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLaunch", "Launch")%></td>
            <td>
                <table>
                    <tr>
                         <td style="position:relative"><div id="divTxtEditor" class="disable"  style="width:600px;height:300px"  visible="false" runat="server"></div>
                            <%--<FCKeditorV2:FCKeditor  id="Launch"  Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />--%>
                             <telerik:RadEditor onClientLoad="onClientLoad" ID="Launch" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />

                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                            </telerik:RadEditor>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="tr_oSite">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td><HP3:ctlSite runat="server" ID="oSite" ShowRelation="False" TypeControl="ContentsConnection" SiteType="Site" /></td>
        </tr>
        <tr runat="server" id="tr_Aree">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="Aree"  ShowRelation="True" TypeControl="ContentsConnection" SiteType="area"/></td>
        </tr>
        <tr runat="server" id="tr_ctlCE">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentEditorial&Help=cms_HelpContentEditorial&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentEditorial", "Content Editorial")%></td>
            <td><HP3:ctlContentEditorial id="ctlCE" typecontrol="GenericRelation" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_ctlOE">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOptions&Help=cms_HelpOptions&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOptions", "Options")%></td>
            <td><HP3:OptionExtra ID="ctlOE" runat="server" /></td>
        </tr>
         <tr runat="server" >
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGeo&Help=cms_HelpGeo&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGeo", "Geo")%></td>
            <td><HP3:ContentGeo ID="ctlContentGeo" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_chkActive">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="chkActive" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_chkDisplay">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="chkDisplay" runat="server"/></td>
        </tr>
         <tr runat="server" id="index">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIndexable&Help=cms_HelpIndexable&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormIndexable", "Indexable")%></td>
            <td><asp:checkbox ID="chkIndexable" runat="server"/></td>
        </tr>
        <tr runat="server" id="tr_txtCode">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCode&Help=cms_HelpCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCode", "Code")%></td>
            <td><HP3:Text runat="server" ID="txtCode" TypeControl="TextBox" Style = "width:600px"/></td>
        </tr>
        <tr runat="server" id="tr_txtCopyright">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCopyright&Help=cms_HelpCopyright&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCopyright", "Copyright")%></td>
            <td><HP3:Text runat="server" ID="txtCopyright" TypeControl="TextBox" Style = "width:600px"  MaxLength="250"/></td>
        </tr>
        <tr  runat="server" >
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCopyright&Help=cms_HelpSubCategory&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSubCategory", "SubCategory")%></td>
            <td><HP3:Text runat="server" ID="txtSubCategory" TypeControl="TextBox" Style = "width:50px" MaxLength="8"/></td>
        </tr>
        <tr runat="server" id="tr_ctlImportants" visible="false">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormImportants&Help=cms_HelpImportants&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormImportants", "Importants")%></td>
            <td><HP3:Importants runat="server" ID="ctlImportants" /></td>
        </tr>
        <tr runat="server" id="tr_txtLinkLabel">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinklabel&Help=cms_HelpLinklabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinklabel", "Link label")%></td>
            <td><HP3:Text runat="server" ID="txtLinkLabel" TypeControl="Textbox" Style = "width:600px"/></td>
        </tr>
        <tr runat="server" id="tr_txtLinkUrl">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
            <td><HP3:Text runat="server" ID="txtLinkUrl" TypeControl="Textbox" Style = "width:600px"/></td>
        </tr>
        <tr runat="server" id="tr_txtQuantit�">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormQuantity&Help=cms_HelpQuantity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormQuantity", "Quantity")%></td>
            <td><HP3:Text runat="server" ID="txtQuantit�" TypeControl="numericText" Style = "width:100px"/></td>
        </tr>
        <tr runat="server" id="bitmask">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFlags&Help=cms_HelpFlags&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormFlags", "Flags")%></td>
            <td><HP3:Text runat="server" ID="txtFlags" TypeControl="numericText" MaxLength="8" Style = "width:600px"/></td>
        </tr>
         <tr runat="server" id="owner">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFlags&Help=cms_HelpOwner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormOwner", "Owner")%></td>
            <td><asp:TextBox id="txtOwnerId" style="display:none" runat="server" />
                <HP3:Text runat="server" ID="txtOwner" TypeControl="TextBox" style="width:300px" isReadOnly="true" />
            	<input type="button" class="button" id="btnInsert" value ='Select Owner' onclick="window.open('<%=strDomain%>/Popups/popUsers.aspx?sites=' + document.getElementById('<%=txtOwnerId.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtOwnerId.clientId%>&ListId=<%=txtOwner.clientId%>' ,'','width=600,height=500,scrollbars=yes')"/>
                <input  id="btnCalendarClear" value="Clear" type="button"  onclick="<%=txtOwnerId.clientid%>.value='';<%=txtOwner.clientid%>.value=''" class="button"  />
            </td>
        </tr>
    </table>
    
    <!--gestione extra fileds -->
    <asp:Repeater id="rp_extf" OnItemDataBound="LoadExtraFields"  runat="server" Visible="false">
	       <HeaderTemplate><table class="form" style="width:99%" ></HeaderTemplate>
	       <FooterTemplate></table></FooterTemplate>
           <ItemTemplate>                
               <tr>
                  <td style="width: 25%"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraField&Help=cms_HelpExtraField&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%#DataBinder.Eval(Container.DataItem, "Name") %></td>
                  <td>
                    <asp:Literal id="extFieldId" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' runat="server" visible="false" />
                    <HP3:ctlExtraField id="extrFielV" MaxLength='<%#DataBinder.Eval(Container.DataItem, "Length") %>' TypeControl='<%#DataBinder.Eval(Container.DataItem, "Type") %>' Indexable='<%#DataBinder.Eval(Container.DataItem, "Indexable") %>' runat="server"/>
                  </td>
                </tr>
          </ItemTemplate>
    </asp:Repeater>
    <!--fine gestione -->
<hr />

<script type="text/javascript">
    function onClientLoad (editor)
    {
        var filter  = editor.get_filtersManager();
        filter.getFilterByName("EncodeScriptsFilter").Enabled = true;
    }
</script>

    