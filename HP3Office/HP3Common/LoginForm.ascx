<%@ Control Language="VB" Debug="true" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script language="VB" runat="server">
    
    Public accessService As New AccessService()
    
    Sub page_load()
        If accessService.IsAuthenticated() Then
            UserLogged()
        Else
            UserUnLogged()
        End If
        
    End Sub

    Sub UserUnLogged()
        dLoginMessage.Visible = False
        dAccessForm.Visible = True
        ltLoggedMessage.Text = ""
        ui.Text = ""
        ui.Focus()
        pw.Text = ""
        lblMessage.Text = "<br />"
    End Sub
    
    Sub UserLogged()
        dLoginMessage.Visible = True
        dAccessForm.Visible = False

        'ltLoggedMessage.Text = "User " & accessService.GetTicket().User.Surname & " " & accessService.GetTicket().User.Name
        'lblMessage.Text = ""
        
    End Sub
    
    Sub DoLogin(ByVal s As Object, ByVal e As ImageClickEventArgs)
        If ui.Text = String.Empty Or pw.Text = String.Empty Then
            lblMessage.Text = "Log-in error!"
            Exit Sub
        End If
        Dim userTicket As TicketValue = accessService.Login(ui.Text, pw.Text)
        If userTicket.ErrorOccurred.Number < 0 Then
            lblMessage.Text = userTicket.ErrorOccurred.Message
        Else
            If Not Request.ServerVariables("HTTP_REFERER") Is Nothing Then
                Response.Redirect(Request.ServerVariables("HTTP_REFERER"))
            Else
                Response.Redirect("/")
            End If
            'UserLogged()
        End If
    End Sub
    
    Sub DoLogout(ByVal s As Object, ByVal e As EventArgs)
        If accessService.Logout() Then
            Response.Redirect("/")
            UserUnLogged()
        End If
        
    End Sub
	
</script>

<div class="dAccessBox">
    	
	
	
	<div id="dAccessForm" class="dloginBox" runat="server">
		
		<table class="form">
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		    <tr>
		        <td style="color:#577599;padding-left: 20px;">Username:</td>
		        <td><asp:textbox id="ui" cssclass ="tbUsernameLogin" runat="server" Width="120" /></td>
		    </tr>
		    <tr>
		        <td style="color:#577599;padding-left: 20px;">Password:</td>
		        <td><asp:textbox id="pw" textmode="password" cssclass ="tbPasswordLogin" runat="server"  Width="120" /></td>
				<td>&nbsp;</td>
				<td><asp:ImageButton src="/HP3Office/_slice/background/enter.gif" id="btnLogin" runat="server" cssClass="button" onClick="doLogin" />
				</td>
		    </tr>		
		</table>
		<br/>
		
			
	</div>

	<div class="LoginError"><asp:Label ID="lblMessage" runat="server" /></div>
	<div class="LoginError"><asp:literal id="litError" runat="server" visible ="false" /></div>
	
    <div id="dLoginMessage" runat="server" visible ="false">
        <asp:Literal ID="ltLoggedMessage" runat="Server" />
        <asp:Button id="btnLogout" runat="server" cssClass="button" Text="Logout" onClick="doLogout" />
    </div>
		
</div>