<%@ Control Language="VB" ClassName="Congress" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script runat="server">
    Private objQs As New ObjQueryString
    Private isNew As Boolean
    Private idContent As Int32
    Private strJS As String
    Private _oGlossaryManager As GlossaryManager
    Private _oGlossaryValue As GlossaryValue
    Private mPageManager As New MasterPageManager
    Private idLanguage As Int16
    Private objCMSUtility As New GenericUtility
    Private _oContentManager As ContentAccManager
    Private webRequest As New WebRequestValue
    Private hList As Hashtable
    Private idThemes As Int32
    Private _strDomain As String
    Private _language As String
        
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        LoadListParam()
                
        If hList Is Nothing Then
            isNew = WebUtility.MyPage(Me).isNew
            idLanguage = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
            idThemes = WebUtility.MyPage(Me).idThemes
        Else
            isNew = hList("isNew")
            idLanguage = objCMSUtility.ReadLanguage(hList("idCurrentLang"))
            idThemes = hList("idThemes")
        End If
        
        'webRequest.customParam.LoadQueryString()
        'isNew = webRequest.customParam.item("N")
        'idLanguage = webRequest.customParam.item("L")
        'idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
    End Sub
    
    Sub ReadControls()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                
                ctl.style.add("display", strVis)
            End If
        Next
    End Sub
    
    Sub Page_Load()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = mPageManager.GetLang.Id
        
        'Recupero dei parametri
        LoadParam()
                              
        If idContent <> 0 Then
            If Not isNew Then
                'Carico glossario
                btnCreaRelazioniAuto.Visible = True
                lbCreaRelazione.Visible = True
                _oGlossaryManager = New GlossaryManager
                _oGlossaryManager.Cache = False
                        
                Dim oGlossarySearcher As New GlossarySearcher
                oGlossarySearcher.key.Id = idContent
                oGlossarySearcher.key.IdLanguage = idLanguage
            
                Dim oGlossaryCollection As GlossaryCollection = _oGlossaryManager.Read(oGlossarySearcher)
                If Not oGlossaryCollection Is Nothing AndAlso oGlossaryCollection.Count > 0 Then
                    _oGlossaryValue = oGlossaryCollection.Item(0)
                End If
            End If
            'Tramite contentService carico i controlli nella pagina master
            'Carico il content da cui eredita content
            CType(Me.Parent.Parent, Object).LoadMasterPage(_oGlossaryValue, isNew)
        
        End If
        
        ReadControls()
    End Sub
    
    ''' <summary>
    ''' Esegue il salvataggio dell'oggetto
    ''' </summary>
    ''' <param name="objStore"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
        Try
            _oGlossaryManager = New GlossaryManager
            Dim oGlossaryValue As New GlossaryValue
            'Dim oGeoCollection As Object
            
            'Esiste il content passatomi nel buffer
            If Not objStore.Content Is Nothing Then
                'Riverso il contentvalue nel congressvalue
                ClassUtility.Copy(objStore.Content, oGlossaryValue)
               
                oGlossaryValue.IdContentSubType = ContentValue.Subtypes.Glossary
                         
                'Salvo l'oggetto
                If isNew Then
                    oGlossaryValue = _oGlossaryManager.Create(oGlossaryValue)
                Else
                    oGlossaryValue = _oGlossaryManager.Update(oGlossaryValue)
                End If
                                
                If Not oGlossaryValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    objStore = _oContentManager.Create(objStore, oGlossaryValue.Key)
                Else
                    objStore = Nothing
                End If
            End If
                       
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStore
    End Function
    
    'crea delle relazioni tra una voce di glossario e altre voci di glossario in maniera automatica
    Sub CreteRelationAutomatically(ByVal Sender As Object, ByVal E As EventArgs)
        Dim outType As Boolean = False
       
        If (_oGlossaryValue.Key.PrimaryKey <> 0) Then
            outType = _oGlossaryManager.AutoCreateGlossaryRelation(_oGlossaryValue.Key.PrimaryKey)
        End If
        
        'Gestione dei valori di ritorno
        If outType Then
            CreateJsMessage("Relation created!")
        Else
            CreateJsMessage("Relation already existing or No relations to any glossary entry")
        End If
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
        
</script>

<script type ="text/javascript" >
    <%=strJS%>
</script>

<div style=" margin-top:5px; margin-bottom:5px">
    <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=&Help=cms_HelpGlossaryAutoRelation&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <asp:Button id ="btnCreaRelazioniAuto" runat="server" Text="Create Relation Automatically"  Visible="false" CssClass="button"  style="background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;"  OnClick="CreteRelationAutomatically"/><br /> 
    <asp:Label id="lbCreaRelazione"  runat ="server" Text="[* Allows to create relationship between this glossary entry and others automatic, based on  a criterion established from the system]" Visible="false" style="color: Gray" ></asp:Label>
</div>