<%@ Control Language="VB"  Debug="true"%>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script language="VB" runat="server">
    Public webRequest As WebRequestValue = New WebRequestValue()
    Public siteManager As New SiteManager()
    Public mySite As New SiteValue()
    Public accessService As New AccessService()
    Public mPageManager As New MasterPageManager()
    Public siteArea As Integer = mPageManager.getSiteArea.Id
    Public contentType As Integer = mPageManager.getContenType.Id
    Public contentId As Integer = mPageManager.getContent.Id
    
    Sub page_load()
        Dim btnClientId As String = btnLogin.ClientID
        ui.Attributes.Add("onkeydown", "safeSubmit('" & btnClientId & "')")
        pw.Attributes.Add("onkeydown", "safeSubmit('" & btnClientId & "')")
             
        If accessService.IsAuthenticated() And mPageManager.GetTicket.Roles.HasRole("HP3", "officeAcc") Then
            UserLogged()
        Else
            UserUnLogged()
        End If
        
    End Sub

    Sub UserUnLogged()
        dLoginMessage.Visible = False
        dAccessForm.Visible = True
        ltLoggedMessage.Text = ""
        ui.Text = ""
        pw.Text = ""
        lblMessage.Text = ""		
    End Sub
    
    Sub UserLogged()
        dLoginMessage.Visible = True
        dAccessForm.Visible = False

        ltLoggedMessage.Text = "Welcome " & accessService.GetTicket().User.Surname & " " & accessService.GetTicket().User.Name
        lblMessage.Text = ""
        
    End Sub
    
    Sub DoLogin(ByVal s As Object, ByVal e As EventArgs)
        Dim userTicket As TicketValue = accessService.Login(ui.Text, pw.Text)
        If userTicket.ErrorOccurred.Number < 0 Then
            lblMessage.Text = userTicket.ErrorOccurred.Message
        Else
            Response.Redirect("/")
            UserLogged()
        End If
    End Sub
    
    Sub DoLogout(ByVal s As Object, ByVal e As EventArgs)
        If accessService.Logout() Then
            Response.Redirect("/")
            UserUnLogged()
        End If
        
    End Sub
	
</script>

<div class="dLoginBox">
    <asp:Label ID="lblMessage" runat="server" />
	
	<div id="dAccessForm" class="dAccessForm" runat="server">
		<div class="error"><asp:literal id="litError" runat="server" visible ="false"/></div>
		Username:
		        <asp:textbox id="ui" cssclass ="inText" runat="server" />
		  
		        Password:
		       <asp:textbox id="pw" textmode="password" cssclass ="inText" runat="server" />
		 
		<asp:Button 
		    id="btnLogin"
			runat="server" 
			cssClass="button"
			Text='Enter'
			onClick="doLogin" />				
	</div>

	
    <div id="dLoginMessage" runat="server" visible ="false">
        <asp:Literal ID="ltLoggedMessage" runat="Server" />
        <asp:Button 
	        id="btnLogout"
	        runat="server" 
	        cssClass="button"
	        Text="Logout"		
	        onClick="doLogout" />
    </div>
		
</div>



