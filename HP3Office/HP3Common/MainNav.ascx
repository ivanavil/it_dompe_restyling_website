<%@ Control Language="VB" ClassName="TreeNav" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>



<script language="vb" runat="server">
    Public siteMAnager As New SiteManager()
    Public themeManager As New ThemeManager()
    
    
    Dim MainThemes As Integer = siteMAnager.getDomainInfo.mainTheme
    Dim themeSearcher As ThemeSearcher
    
    
    Sub getMenuItem()
        'themeSearcher = New ThemeSearcher()
        
        'themeSearcher.KeyFather.Id = 32
        'dg.DataSource = themeManager.Read(themeSearcher)
        'dg.DataBind()
        
    End Sub
    
    Sub getService()
        Dim profilingManager As New ProfilingManager
        Dim roleCollection As New RoleCollection
        Dim accessService As New AccessService
        themeSearcher = New ThemeSearcher()
        RoleCollection = ProfilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
        themeSearcher.KeySite = New SiteIdentificator(WebUtility.MyPage(Me).ThemesSite)
        TopNav.DataSource = themeManager.Read(RoleCollection, themeSearcher)
        TopNav.DataBind()
    End Sub
    
    
    Sub Page_Load()
        ' Response.Write(WebUtility.MyPage(Me).ThemesSite)
        If Not Page.IsPostBack Then
            ' getMenuItem()
            'getService()
        End If
       
        
        
        
    End Sub
    
</script>
<asp:Repeater runat="server" ID="TopNav">
<ItemTemplate><a href=""><%#DataBinder.Eval(Container.DataItem, "Description")%>...</a></ItemTemplate>
<SeparatorTemplate> | </SeparatorTemplate>
</asp:Repeater>