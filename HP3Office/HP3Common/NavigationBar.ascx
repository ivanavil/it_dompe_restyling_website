<%@ Control Language="VB" Debug="true" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<script runat="server">
    Public webRequest As New WebRequestValue
    Public themeSearcher As New ThemeSearcher()
    Public roleCollection As New RoleCollection()
    
    'Public themeKey As New ThemeIdentificator(siteManager.getDomainInfo.mainTheme)
    
    'Private visComboSite As Boolean
    'Private idSiteThemes As String
    Private objQs As New ObjQueryString
    'Private ListLink As New Hashtable
    
    Function getLink2(ByVal theme As ThemeValue) As String
        Dim strRetValue As String = ""
        strRetValue = Me.BusinessMasterPageManager.GetLink(theme, True)
        Return strRetValue
    End Function
    
    Function getLink(ByVal theme As ThemeValue) As String
        webRequest = New WebRequestValue
        webRequest.customParam.Clear()
        
        webRequest.KeysiteArea = theme.KeySiteArea
        
        If TypeOf theme Is ThemesConsolleValue Then
            webRequest.KeycontentType = CType(theme, ThemesConsolleValue).KeyConsolleContentType
        Else
            webRequest.KeycontentType = CType(theme, ThemeValue).KeyContentType
        End If
        
        webRequest.KeyEvent = theme.KeyTraceEvent
              
        If (theme.KeyMasterPage.Id > 0) Then
            webRequest.MasterPage = theme.KeyMasterPage.Id
        End If
        
        objQs.Clear()
        objQs.SiteArea_Id = 0
        objQs.ContentType_Id = CType(theme, ThemeValue).KeyContentType.Id
        objQs.Theme_id = theme.Key.Id
        objQs.IsNew = 0
        objQs.ContentId = 0
        'Response.Write(objQs.ContentType_Id) : Response.End()
        webRequest.customParam.append(objQs)
        Dim strRetValue As String = Me.BusinessMasterPageManager.FormatRequest(webRequest)
        Return strRetValue
    End Function
       
    Function getMyChild(ByVal theme As ThemeValue) As ThemeCollection
        Dim tcol As ThemeCollection
        themeSearcher.KeyFather = theme.Key
        tcol = Me.BusinessThemeManager.Read(themeSearcher).FilterByRoles(roleCollection)
        Return tcol
    End Function
    	
    Sub page_load()
        If Not Me.BusinessAccessService.IsAuthenticated Then Exit Sub
        If WebUtility.MyPage(Me).Tab <> "" Then
            
            'se st� utilizzazndo il menu del CRM 
            If (WebUtility.MyPage(Me).Tab.Equals("CRM")) Then
                'rendo invisibile il controllo SiteSelect 
                'ovvero la dwl (di sinistra) che permette di selezionare il sito
                Dim dwlControl = Me.Parent.Parent.FindControl("SiteSelect")
                dwlControl.Visible = False
                'non visualizzo l'albero dei temi
                Dim themesControl = Me.Parent.Parent.FindControl("ThemesMenu")
                themesControl.visible = False
                'cambio la visualizzazione del layout della pagina
                'Dim classControl = Me.Parent.Parent.FindControl("dPage")
                'classControl.Attributes.Remove("class")
                Dim classControl = CType(Me.Parent.Parent.FindControl("dPage"), HtmlControl)
                If Not classControl Is Nothing Then
                    classControl.Attributes.Remove("class")
                End If
            End If
           
            roleCollection = Me.BusinessAccessService.GetTicket.Roles
            'Response.Write(WebUtility.MyPage(Me).Tab)
            'Response.End()
            themeSearcher.KeyFather.Id = Me.BusinessThemeManager.GetThemeIdentificator(New SiteIdentificator(Me.BusinessMasterPageManager.GetSite.Id), WebUtility.MyPage(Me).Tab).Id
            repMenuLev1.DataSource = Me.BusinessThemeManager.Read(themeSearcher).FilterByRoles(roleCollection)
            repMenuLev1.DataBind()
        End If
    End Sub
   
</script>

<div class="dNavigationBar">   
	<!--<a class="fRight"></a>-->
	<asp:repeater id="repMenuLev1" runat="server" EnableViewState="false" >
		<HeaderTemplate>
			<ul class="ulNavigationBar">
		</HeaderTemplate>
		<ItemTemplate>		   
			<li class="liThemeLev1" onmouseover="showMenu(<%#Container.ItemIndex%>)" onmouseout="hideMenu(<%#Container.ItemIndex%>)">
				<a id="aThemeLev1Ind<%#Container.ItemIndex%>" href="#" class="aThemeLev1Unsel">				
				    <%#DataBinder.Eval(Container.DataItem, "Description")%>
				</a>
				<div id="ulMenuOf<%#Container.ItemIndex%>"  class="ulMenu" style="display:none">
				    <asp:Repeater ID="repMenuLev2" runat="server" DataSource='<%#getMyChild(Container.DataItem)%>' EnableViewState="false" >				 
				   
				    <ItemTemplate>
				        <div class="liThemeLev2" >				        				       	                                  
				             <a class="" href='<%#getLink(Container.DataItem)%>' ><%#DataBinder.Eval(Container.DataItem, "Description")%></a>
				           <ul class="ulSubMenu">
				                
				                <asp:Repeater ID="repMenuLev3" runat="server" DataSource='<%#getMyChild(Container.DataItem)%>' EnableViewState="false" >				               				              				                 
				    			<ItemTemplate>
				                <li class="liThemeLev3" >	              
				                 <a class="" href='<%#getLink(Container.DataItem)%>'><%#DataBinder.Eval(Container.DataItem, "Description")%></a>
				                </li>
				                </ItemTemplate>
				                </asp:Repeater>				            				           				              
				            </ul>
				        </div>				      
				    </ItemTemplate>
				    </asp:Repeater>
				</div>
			</li>
		</ItemTemplate>
		<FooterTemplate>
			</ul>
		</FooterTemplate>	
	</asp:repeater>
</div>


