<%@ Control Language="VB" ClassName="CVTProduct" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@Import Namespace="Healthware.HP3.Core.Product" %>
<%@Import Namespace="Healthware.HP3.Core.Product.ObjectValues" %>
<%@Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>


<script language="VB" runat="server">
    Private _strDomain As String
    Private _language As String
    Private strJS As String
    
       
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
      
    Public Property DeafaultBrandId() As String
        Get
            Return txtBrandId.Text
        End Get
        Set(ByVal value As String)
            txtBrandId.Text = value
        End Set
    End Property
    
    Public Property DeafaultFranchiseId() As String
        Get
            Return txtFranchiseId.Text
        End Get
        Set(ByVal value As String)
            txtFranchiseId.Text = value
        End Set
    End Property
    
    Public Property DeafaultBrand() As String
        Get
            Return txtBrand.Text
        End Get
        Set(ByVal value As String)
            txtBrand.Text = value
        End Set
    End Property
    
    Public Property DeafaultFranchise() As String
        Get
            Return txtFranc.Text
        End Get
        Set(ByVal value As String)
            txtFranc.Text = value
        End Set
    End Property
           
    Public Property Latext() As Boolean
        Get
            Return chkLatext.Checked
        End Get
        Set(ByVal value As Boolean)
            chkLatext.Checked = value
        End Set
    End Property
    
    Public Property Package() As Boolean
        Get
            Return chkPackage.Checked
        End Get
        Set(ByVal value As Boolean)
            chkPackage.Checked = value
        End Set
    End Property
    
    Public Property Sample() As Boolean
        Get
            Return chkSample.Checked
        End Get
        Set(ByVal value As Boolean)
            chkSample.Checked = value
        End Set
    End Property
    
    Public Property Preferred() As Boolean
        Get
            Return chkPreferred.Checked
        End Get
        Set(ByVal value As Boolean)
            chkPreferred.Checked = value
        End Set
    End Property
    
    Sub page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
    End Sub
        
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript">
    <%=strJS%>
</script>

<div>
   <table  class="form" width="99%">
         <%--<tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdFrinchise&Help=cms_HelpProdFrinchise&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdFrinchise", "Default Franchise")%></td>
             <td><HP3:Text runat="server" id="txtFranc" TypeControl="TextBox" Style = "width:600px"/></td>
         </tr> 
         <tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdBrand&Help=cms_HelpProdBrand&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdBrand", "Default Brand")%></td>
             <td><HP3:Text runat="server" id="txtBrand" TypeControl="TextBox" Style = "width:600px"/></td>
         </tr>  --%>
         
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdFrinchise&Help=cms_HelpProdFrinchise&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdFrinchise", "Default Franchise")%></td>
            <td>
               <table width="100%">
                 <tr>
                   <td  style="width:50px"><asp:TextBox id="txtFranchiseId" style="display:none" runat="server" /><HP3:Text runat="server" ID="txtFranc" TypeControl="TextBox" style="width:300px" isReadOnly="true" /></td>
                   <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popFranchises.aspx?sites=' + document.getElementById('<%=txtFranchiseId.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtFranchiseId.clientId%>&ListId=<%=txtFranc.clientId%>','','width=600,height=500,scrollbars=yes')" /></td> 
                 </tr>
               </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdBrand&Help=cms_HelpProdBrand&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdBrand", "Default Brand")%></td>
            <td>
               <table width="100%">
                 <tr>
                   <td  style="width:50px"><asp:TextBox id="txtBrandId" style="display:none" runat="server" /><HP3:Text runat="server" ID="txtBrand" TypeControl="TextBox" style="width:300px" isReadOnly="true" /></td>
                   <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popBrands.aspx?sites=' + document.getElementById('<%=txtBrandId.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtBrandId.clientId%>&ListId=<%=txtBrand.clientId%>','','width=600,height=500,scrollbars=yes')" /></td> 
                 </tr>
               </table>        
            </td>
         </tr>
         <tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdLatext&Help=cms_HelpProdLatext&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdLatext", "Latext")%></td>
             <td><asp:checkbox ID="chkLatext" runat="server"/></td>
         </tr>
         <tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdPackage&Help=cms_HelpProdPackage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdPackage", "Package")%></td>
             <td><asp:checkbox ID="chkPackage" runat="server"/></td>
         </tr>
         <tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdSample&Help=cms_HelpProdSample&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdSample", "Sample")%></td>
             <td><asp:checkbox ID="chkSample" runat="server"/></td>
         </tr>
         <tr>
             <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdPreferred&Help=cms_HelpProdPreferred&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdPreferred", "Preferred")%></td>
             <td><asp:checkbox ID="chkPreferred" runat="server"/></td>
         </tr>
   </table>
</div>