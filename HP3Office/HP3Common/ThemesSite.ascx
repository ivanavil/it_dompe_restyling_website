<%@ Control Language="VB" ClassName="ThemesSite" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">
    Public SiteId As Int16
    Sub Page_Load()
        If Not Me.BusinessAccessService.IsAuthenticated Then Exit Sub
        If Not Page.IsPostBack Then
            LoadCmbSite()
            If SiteId <> -1 Then
                cmbSite.SelectedIndex = cmbSite.Items.IndexOf(cmbSite.Items.FindByValue(SiteId))
            End If
        End If
    End Sub
    
    Sub LoadCmbSite()
        Dim oSiteCollection As SiteCollection
        Dim oSiteSearcher As New SiteSearcher
        Dim oSiteValue As SiteValue
        Dim curIdSite As Int32 = Me.ObjectSiteDomain.KeySiteArea.Id
        oSiteCollection = Me.BusinessSiteManager.Read(Me.ObjectTicket.Roles)		
               
        'roleCollection = profilingManager.ReadRoles(New UserIdentificator(accService.GetTicket.User.Key.Id))
        'roleCollection = accService.GetTicket.Roles
        Dim siteColl As New SiteCollection
        
        For Each oSiteValue In oSiteCollection
            If oSiteValue.Status() Then siteColl.Add(oSiteValue)
        Next
        siteColl = siteColl.DistinctBy("KeySiteArea.Id")
       
        For Each oSiteValue In siteColl
            If curIdSite <> oSiteValue.KeySiteArea.Id AndAlso oSiteValue.Status() Then
                cmbSite.Items.Add(New ListItem(oSiteValue.Label, oSiteValue.KeySiteArea.Id))
            End If
        Next
        cmbSite.Items.Insert(0, New ListItem("No site selected", -1))
    End Sub
    
    Sub setLink(ByVal s As Object, ByVal e As EventArgs)
        Try
            Dim objQs As New ObjQueryString
            Dim webRequest As New WebRequestValue
            If cmbSite.SelectedItem.Value > 0 Then
                objQs.Clear()
                objQs.Site_Id = cmbSite.SelectedItem.Value
                webRequest.customParam.append(objQs)
                Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
            Else
                Response.Redirect("\")
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub
</script>
<img src="/hp3Office/HP3Image/Ico/selSiteIco.gif" style="vertical-align:bottom; margin-right:4px; float:left; width:15px;" alt="Site List" title="Site List" />
<asp:dropdownlist  ID="cmbSite"   runat="server"  OnSelectedIndexChanged ="setLink" AutoPostBack="true" />