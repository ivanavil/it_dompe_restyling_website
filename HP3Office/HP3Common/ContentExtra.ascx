<%@ Control Language="VB" ClassName="ContentExtra" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName="Date" Src="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName="Text" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private objQs As New ObjQueryString
    Private isNew As Boolean
    Private idContent As Int32
    Private _oContentExtraManager As ContentExtraManager
    Private _oContentExtraValue As ContentExtraValue
    Private idLanguage As Int16
    Private objCMSUtility As New GenericUtility
    Private _oContentManager As ContentAccManager
    Private hList As Hashtable
    Private idThemes As Int32
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private webRequest As New WebRequestValue
    Private statusManager As New StatusManager
    Private _contentManager As ContentManager
    Private _language As String
    Private _strDomain As String
    Private _contentVersion As Object
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property ContentVersion() As Object
        Get
            Return _contentVersion
        End Get
        Set(ByVal value As Object)
            _contentVersion = value
        End Set
    End Property
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = masterPageManager.GetLang.Id
        
        'LoadListParam()
        'If hList Is Nothing Then
        '    isNew = WebUtility.MyPage(Me).isNew
        '    idLanguage = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        '    idThemes = WebUtility.MyPage(Me).idThemes
        'Else
        '    isNew = hList("isNew")
        '    idLanguage = objCMSUtility.ReadLanguage(hList("idCurrentLang"))
        '    idThemes = hList("idThemes")
        'End If
        
        webRequest.customParam.LoadQueryString()
        isNew = webRequest.customParam.item("N")
        idLanguage = webRequest.customParam.item("L")
        idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
    End Sub
    
    Sub Page_Load()
        'controllo se il content da visualizzare � una versione
        'o il content attuale
        Dim versionId As String = ""
        webRequest.customParam.LoadQueryString()
        
        versionId = webRequest.customParam.item("V")
        'se � una versione
        If (versionId <> String.Empty) Then
            
            Dim obj As Object = ReadVersion(versionId)
            If Not (obj Is Nothing) Then
                'Se ha trovato il content lo carica  
                _oContentExtraValue = obj
                LoadValue()
            End If
            
            DisableAllFormElements()
        Else 'se � il content attuale
            'Recupero dei parametri
            LoadParam()
        
            If idContent <> 0 Then
                If Not isNew Then
                    'gestisce il check-in/chech-out
                    CheckOutManagement()
                    
                    'Carico il contentExtra
                    _oContentExtraValue = ReadContentExtra(idContent, idLanguage)
                End If
                
                If Not _oContentExtraValue Is Nothing Then
                    'Carico i controlli che derivano da content
                    ContentVersion = _oContentExtraValue
                    LoadValue()
                End If
            
                'Tramite contentService carico i controlli nella pagina master
                'Carico il content da cui eredita content
                CType(Me.Parent.Parent, Object).LoadMasterPage(_oContentExtraValue, isNew)
           
            End If
        
            ReadControls()
        End If
    End Sub
    
    'gestisce il check-out/check-in di un content
    Sub CheckOutManagement()
        _contentManager = New ContentManager
       
        Dim idPrimarykey As Integer = _contentManager.ReadPrimaryKey(idContent, idLanguage)
       
        If statusManager.IsEditCheckOut(idPrimarykey) And Not isNew Then
            If statusManager.IsEditCheckOut(idPrimarykey, masterPageManager.GetTicket.User.Key.Id) Then
                DisableAllFormElements()
            End If
        End If
    End Sub
    
    'legge un contentExtra dato idContent e  idLanguage
    Function ReadContentExtra(ByVal idCont As Integer, ByVal idLang As Integer) As ContentExtraValue
        Dim contextExtra As New ContentExtraValue
        
        'Carico ContentExtra
        _oContentExtraManager = New ContentExtraManager
        _oContentExtraManager.Cache = False
        Dim oContentExtraSearcher As New ContentExtraSearcher
        oContentExtraSearcher.Display = SelectOperation.All
        oContentExtraSearcher.Active = SelectOperation.All
        oContentExtraSearcher.Delete = SelectOperation.All
        oContentExtraSearcher.key.Id = idCont
        oContentExtraSearcher.key.IdLanguage = idLang
        oContentExtraSearcher.LoadOwnerDetails = True 'M1168
        Dim oContentCollection As ContentExtraCollection = _oContentExtraManager.Read(oContentExtraSearcher) 'objCMSUtility.GetContentCollection(oContentExtraSearcher)
        If Not oContentCollection Is Nothing AndAlso oContentCollection.Count > 0 Then
            contextExtra = oContentCollection.Item(0)
            Return contextExtra
        End If
        
        Return Nothing
    End Function
    
    'legge una versione in base al suo Identificatore
    'restituisce l'oggetto memorizzato nella versione
    Function ReadVersion(ByVal versionId As Integer) As Object
        Dim versioningManager As New VersioningManager
        Dim versionIdent As New VersionIdentificator
        
        versionIdent.Id = versionId
        Dim versionValue As VersionValue = versioningManager.Read(versionIdent)
        Return versionValue.StoredObject
    End Function
        
    Sub ReadControls()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                
                ctl.style.add("display", strVis)
                 
            End If
        Next
    End Sub
    
    Sub LoadValue()
        With _oContentExtraValue
            CETitle.Text = .TitleContentExtra
            SubTitle.Text = .SubTitle
            TitleOriginal.Text = .TitleOriginal
            'Abstract.Value = .Abstract
            Abstract.Content = .Abstract
            'FullText.Value = .FullText
            FullText.Content = .FullText
            'FullTextComment.Value = .FullTextComment
            FullTextComment.Content = .FullTextComment
            'FullTextOriginal.Value = .FullTextOriginal
            FullTextOriginal.Content = .FullTextOriginal
            
            LinkContentExtra.Text = .LinkContentExtra
            BookReferer.Text = .BookReferer
        End With
    End Sub
    
    ''' <summary>
    ''' Esegue il salvataggio dell'oggetto
    ''' </summary>
    ''' <param name="objStore"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
        webRequest.customParam.LoadQueryString()
        Dim versionId As String = webRequest.customParam.item("V")
        Dim idCont = webRequest.customParam.item("C")
        Dim idLang = webRequest.customParam.item("L")
        
 
        Try
            _oContentExtraManager = New ContentExtraManager
            Dim oContentExtraValue As New ContentExtraValue
            Dim optionExtraColl As New ContentOptionExtraCollection
            
            'Esiste il content passatomi nel buffer
            If Not objStore.Content Is Nothing Then
                'Riverso il contentvalue nel congressvalue
                ClassUtility.Copy(objStore.Content, oContentExtraValue)
                
   
                'Popolo l' oggetto con i dati nella form
                With oContentExtraValue
                    .TitleContentExtra = CETitle.Text
                    .SubTitle = SubTitle.Text
                    .TitleOriginal = TitleOriginal.Text
                    '.Abstract = Abstract.Value
                    .Abstract = Abstract.Content
                    '.FullText = FullText.Value
                    .FullText = FullText.Content
                    '.FullTextComment = FullTextComment.Value
                    .FullTextComment = FullTextComment.Content
                    '.FullTextOriginal = FullTextOriginal.Value
                    .FullTextOriginal = FullTextOriginal.Content
                    
                    .LinkContentExtra = LinkContentExtra.Text
                    .BookReferer = BookReferer.Text
                End With
                
                'nel caso di ripristino
                If (versionId <> String.Empty) Then
                    ContentVersion() = ReadContentExtra(idCont, idLang)
                End If
                
                'Salvo l'oggetto
                If isNew Then
                    oContentExtraValue = _oContentExtraManager.Create(oContentExtraValue)
                    'If oContentExtraValue.Key.IdGlobal <= 0 Then
                    '    Dim cm As New ContentManager
                    '    cm.Update(oContentExtraValue.Key, "Key.IdGlobal", oContentExtraValue.Key.Id)
                    'End If
                Else
                    
                    oContentExtraValue = _oContentExtraManager.Update(oContentExtraValue)
                End If
                                
                If Not oContentExtraValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
               
 
                    
                    
                    objStore = _oContentManager.Create(objStore, oContentExtraValue.Key)
                    
                    'Recupero dalla property ContentVersion il contentValue prima dell'update
                    objStore.ContentVersion = ContentVersion
                Else
                    objStore = Nothing
                End If
                                     
            End If
                       
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStore
    End Function
    
    'controlla se esistono OptionExtra per un certo contenuto
    'e se tra queste e stata specificata un OptionEtra di default
    Function defaultOptionExtra(ByRef optionExtraColl As ContentOptionExtraCollection) As Boolean
        'Dim optionExtraSearcher As New ContentOptionExtraSearcher
        Dim optionExtraValue As New ContentOptionExtraValue
        'Dim _cManager = New ContentManager
        
        'optionExtraSearcher.KeyContent.Id = idContent
        'optionExtraColl = _cManager.ReadContentOptionExtra(optionExtraSearcher)
        
        If Not (optionExtraColl Is Nothing) And (optionExtraColl.Count > 0) Then
            For Each optionExtraValue In optionExtraColl
                If (optionExtraValue.IsDefault) Then
                    Return True
                End If
            Next
        End If
        
        Return False
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    'disattiva tutti gli elementi del form
    Sub DisableAllFormElements()
        CETitle.Disable()
        SubTitle.Disable()
        TitleOriginal.Disable()
        LinkContentExtra.Disable()
        BookReferer.Disable()
        
        divTxtEditor_1.Visible = True
        divTxtEditor_2.Visible = True
        divTxtEditor_3.Visible = True
        divTxtEditor_4.Visible = True
    End Sub
    
    'attiva tutti gli elementi del form
    Public Sub EnableAllFormElements()
        CETitle.Enable()
        SubTitle.Enable()
        TitleOriginal.Enable()
        LinkContentExtra.Enable()
        BookReferer.Enable()
      
        divTxtEditor_1.Visible = False
        divTxtEditor_2.Visible = False
        divTxtEditor_3.Visible = False
        divTxtEditor_4.Visible = False
    End Sub
</script>
<table style="width:99%" class ="form">
    <tr><td style="width:25%" valign ="top"></td><td></td></tr>
    <tr runat="server" id="tr_CETitle">
        <td style="width:25%" valign ="top">
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitleContentExtra&Help=cms_HelpTitleContentExtra&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitleContentExtra", "Title ContentExtra")%>
        </td>
        <td>    
            <HP3:Text ID="CETitle" runat="server" TypeControl ="TextBox" style="width:600px"/>
        </td>
    </tr>
    <tr runat="server" id="tr_SubTitle">
        <td style="width:25%" valign ="top">    
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSubTitle&Help=cms_HelpSubTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSubTitle", "SubTitle")%>
        </td>
        <td>    
            <HP3:Text ID="SubTitle" runat="server" TypeControl ="TextBox" style="width:600px"/>
        </td>
    </tr>
    <tr runat="server" id="tr_TitleOriginal">
        <td style="width:25%" valign ="top">
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitleOriginal&Help=cms_HelpTitleOriginal&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitleOriginal", "Title Original")%>
        </td>
        <td>    
            <HP3:Text ID="TitleOriginal" runat="server" TypeControl ="TextBox" style="width:600px"/>
        </td>
    </tr>
    <tr runat="server" id="tr_Abstract">
        <td style="width:25%" valign ="top">    
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAbstract&Help=cms_HelpAbstract&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAbstract", "Abstract")%>
        </td>
        <td> 
            <table>
                <tr>
                    <td style="position:relative"><div id="divTxtEditor_1" class="disable"  style="width:600px;height:300px" visible="false" runat="server"></div>
<%--                        <FCKeditorV2:FCKeditor id="Abstract" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                        <telerik:RadEditor ContentFilters="ConvertToXhtml" ID="Abstract" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>


                                 <ImageManager SearchPatterns="*.jpg,*.jpeg,*.gif,*.png" ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<TemplateManager  ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<DocumentManager SearchPatterns="*.doc,*.txt,*.docx,*.xls,*.xlsx,*.pdf,*.pps,*.ppsx,*.ppt,*.pptx,*.exe" ViewPaths="~/HP3Download,~/HP3Download" UploadPaths="~/HP3Download,~/HP3Download" DeletePaths="~/HP3Download,~/HP3Download" MaxUploadFileSize="10485760" />
                                 
									<Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                  
                   </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr runat="server" id="tr_FullText">
        <td style="width:25%" valign ="top">    
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullText&Help=cms_HelpFullText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullText", "FullText")%>
        </td>
        <td>    
           <table>
              <tr>
                 <td style="position:relative"><div id="divTxtEditor_2" class="disable"  style="width:600px;height:300px" visible="false" runat="server"></div>
                    <%--<FCKeditorV2:FCKeditor id="FullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />--%>
                    
                     <telerik:RadEditor ContentFilters="ConvertToXhtml" ID="FullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager SearchPatterns="*.jpg,*.jpeg,*.gif,*.png" ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<TemplateManager  ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<DocumentManager SearchPatterns="*.doc,*.txt,*.docx,*.xls,*.xlsx,*.pdf,*.pps,*.ppsx,*.ppt,*.pptx,*.exe" ViewPaths="~/HP3Download,~/HP3Download" UploadPaths="~/HP3Download,~/HP3Download" DeletePaths="~/HP3Download,~/HP3Download" MaxUploadFileSize="10485760" />
                                 
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                </td>
               </tr>
           </table>
        </td>
    </tr>
    <tr runat="server" id="tr_FullTextOriginal">
        <td style="width:25%" valign ="top">  
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullTextOriginal&Help=cms_HelpFullTextOriginal&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullTextOriginal", "FullText Original")%>
        </td>
        <td>    
              <table class="form">
              <tr>
                  <td style="position:relative"><div id="divTxtEditor_3" class="disable"  style="width:600px;height:300px"  visible="false" runat="server"></div>
<%--                    <FCKeditorV2:FCKeditor id="FullTextOriginal" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                  <telerik:RadEditor ContentFilters="ConvertToXhtml" ID="FullTextOriginal" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager SearchPatterns="*.jpg,*.jpeg,*.gif,*.png" ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<TemplateManager  ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<DocumentManager SearchPatterns="*.doc,*.txt,*.docx,*.xls,*.xlsx,*.pdf,*.pps,*.ppsx,*.ppt,*.pptx,*.exe" ViewPaths="~/HP3Download,~/HP3Download" UploadPaths="~/HP3Download,~/HP3Download" DeletePaths="~/HP3Download,~/HP3Download" MaxUploadFileSize="10485760" />
                                 
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                  </td>
              </tr>
              </table>
        </td>
    </tr>
    <tr runat="server" id="tr_FullTextComment">
        <td style="width:25%" valign ="top">    
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullTextComment&Help=cms_HelpFullTextComment&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullTextComment", "FullText Comment")%>
        </td>
        <td>  
            <table>
                <tr>
                    <td style="position:relative"><div id="divTxtEditor_4" class="disable"  style="width:600px;height:300px"  visible="false" runat="server"></div>
<%--                        <FCKeditorV2:FCKeditor id="FullTextComment" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                    <telerik:RadEditor ContentFilters="ConvertToXhtml" ID="FullTextComment" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager SearchPatterns="*.jpg,*.jpeg,*.gif,*.png" ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<TemplateManager  ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
									<DocumentManager SearchPatterns="*.doc,*.txt,*.docx,*.xls,*.xlsx,*.pdf,*.pps,*.ppsx,*.ppt,*.pptx,*.exe" ViewPaths="~/HP3Download,~/HP3Download" UploadPaths="~/HP3Download,~/HP3Download" DeletePaths="~/HP3Download,~/HP3Download" MaxUploadFileSize="10485760" />
                                 
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                    
                    </td>
                </tr>
            </table>  
        </td>
    </tr>
    <tr runat="server" id="tr_LinkContentExtra">
        <td style="width:25%" valign ="top">    
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinkContentExtra&Help=cms_HelpLinkContentExtra&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkContentExtra", "Link Content Extra")%>
        </td>
        <td>    
            <HP3:Text ID="LinkContentExtra" runat="server" TypeControl ="TextBox" style="width:600px"/>
        </td>
    </tr>
    <tr runat="server" id="tr_BookReferer">
        <td style="width:25%" valign ="top">    
           <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBookReferer&Help=cms_HelpBookReferer&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBookReferer", "Book Reference")%>
        </td>
        <td>    
            <HP3:Text ID="BookReferer" runat="server" TypeControl ="TextBox" style="width:600px"/>
        </td>
    </tr>
</table>

