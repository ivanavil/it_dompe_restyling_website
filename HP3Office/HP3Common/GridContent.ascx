<%@ Control Language="VB" ClassName="GridContent" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<script runat="server">

    Public Event SelectedEvent As EventHandler
    Public Event UpdateEvent As EventHandler
    Public Event SortingEvent As GridViewSortEventHandler
    
    Private oGenericUtlity As New GenericUtility
    Private _key As String = Nothing
    Private _contentPrimaryKey As Integer
    Private _gridDataSource As ContentCollection = Nothing
    
    Private _topItems As Integer
    Private objQs As New ObjQueryString
    
    Private _allowEdit As Boolean = True
    Private _allowSelect As Boolean = True
    Private _useInPopUp As Boolean = False
    Private _existWF As Boolean = False
    Private gridViewTotal As Long
    Private strDomain As String
    Dim themeId As Integer
    
    Private WebUtility = New WebUtility()
    Private oLManager As New LanguageManager()

    Private dateUtility As New StringUtility
    Private masterPageManager As New MasterPageManager
    Private userManager As New UserManager
    Private statusManager As New StatusManager
    Private contentEditStatusValue As New ContentEditStatusValue
        
       
    Public Property GridDataSource() As ContentCollection
        Get
            Return ViewState("GridSource")
        End Get
        Set(ByVal value As ContentCollection)
            ViewState("GridSource") = value
        End Set
    End Property
    
    Public Property Key() As String
        Get
            Return _key
        End Get
        Set(ByVal Value As String)
            _key = Value
            
            ViewState("GridContentKey") = _key
            
            BindGrid()
        End Set
    End Property
    
    Public Property FilterByTopItems() As Boolean
        Get
            Return (_topItems > 0)
        End Get
        Set(ByVal value As Boolean)
            _topItems = IIf(value, ConfigurationsManager.RecentContentList, 0)
        End Set
    End Property
   
    Public Property ExistWorkFlow() As Boolean
        Get
            Return ViewState("existWF")
        End Get
        Set(ByVal value As Boolean)
            ViewState("existWF") = value
        End Set
    End Property
    
    Public Sub BindGrid(Optional ByVal Numpage As Integer = 1)
        If Not (page.IsPostBack) Then
            CheckWorkFlow()
            strDomain = WebUtility.MyPage(Me).DomainInfo
        End If
        
        
        If Not GridDataSource Is Nothing Then
            'GridDataSource.Sort(SortType, SortOrder)
            'GridDataSource = GridDataSource.DistinctBy("Key.PrimaryKey")
            'gridViewTotal = GridDataSource.Count
            'If FilterByTopItems Then
            '    Dim recentContents As ContentCollection
            '    If GridDataSource.Count > _topItems Then
            '        recentContents = New ContentCollection()
            '        Dim i As Integer = 0
            '        For i = 0 To _topItems - 1
            '            recentContents.Add(GridDataSource.Item(i))
            '        Next
            '        GridDataSource = recentContents
            '    End If
            'Else
            'End If
                                    
            GridContents.DataSource = GridDataSource
            GridContents.DataBind()
        End If
    End Sub
    
    'Protected Sub GridContents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    'GridContents.PageIndex = e.NewPageIndex
    'BindGrid()
    'End Sub
    
    'Sub linkPager_PageIndex(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'evento scatenato dal paginatore degli utenti filtrati
    '    'CType(Request("page"), Integer)
    '    Dim webRequest As New WebRequestValue
    '    Dim mPage As New MasterPageManager()
    '    Dim objQs As New ObjQueryString

    '    webRequest.customParam.append("page", sender.CommandArgument)
    '    webRequest.customParam.append(objQs)
    '    Response.Redirect(mPage.FormatRequest(webRequest))

    '    BindGrid(sender.CommandArgument)
    '    'LoadUser(CInt(sender.CommandArgument), ctxFilter.Value, txtSortOrder.Value, txtOrderType.Value)
    'End Sub
    
    Protected Sub GridContents_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        RaiseEvent SortingEvent(sender, e)
             
    End Sub
    
    '******Per gli img button
    Sub EditRow(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not AllowEdit Then Return
        RaiseEvent UpdateEvent(s, e)
    End Sub
    
   
    
    Sub ReadVersions(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not AllowEdit Then Return
        RaiseEvent UpdateEvent(s, e)
    End Sub
    
    
    '*****Per i link button
    Sub EditRow(ByVal s As Object, ByVal e As EventArgs)
        If Not AllowEdit Then Return
        RaiseEvent UpdateEvent(s, e)
    End Sub
           
    
    Sub fireSelectedEvent(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not AllowSelect Then Return
        RaiseEvent SelectedEvent(s, e)
    End Sub
    
    Function GetKey(ByVal content_id As Int32, ByVal language_id As Int16) As String
        Return content_id & "_" & language_id
    End Function
    
    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property AllowSelect() As Boolean
        Get
            Return _allowSelect
        End Get
        Set(ByVal value As Boolean)
            _allowSelect = value
        End Set
    End Property
    
    Public Property UseInPopUp() As Boolean
        Get
            Return _useInPopUp
        End Get
        Set(ByVal value As Boolean)
            _useInPopUp = value
        End Set
    End Property
    
    Public Property ContentPrimaryKey() As Integer
        Get
            Return _contentPrimaryKey
        End Get
        Set(ByVal value As Integer)
            _contentPrimaryKey = value
        End Set
    End Property
    
    Protected Sub GridContents_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
      
    End Sub
    
    Sub rbtn_change(ByVal sender As Object, ByVal e As System.EventArgs)
        _contentPrimaryKey = 1
    End Sub
        
    Sub SetStatus(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim oContentManager As New ContentManager
        Dim oContentSearcher As New ContentSearcher
        Dim oContentValue As New ContentValue
        Dim oActive As String = "1"
        oContentSearcher.key.PrimaryKey = Integer.Parse(s.commandargument)
        oContentSearcher.Delete = SelectOperation.All
        oContentSearcher.Active = SelectOperation.All
        oContentSearcher.Display = SelectOperation.All
        oContentManager.Cache = False
        oContentValue = oContentManager.Read(oContentSearcher)(0)
        If Not oContentValue Is Nothing Then
            If oContentValue.Active Then oActive = "0"
            oContentManager.Update(New ContentIdentificator(Integer.Parse(s.commandargument)), "Active", oActive)
        End If
        RaiseEvent UpdateEvent(s, e)
    End Sub
    
    Protected Sub GridContents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim lnkActive As ImageButton = e.Row.FindControl("btnActive")
        Dim lnkDelete As ImageButton = e.Row.FindControl("btn_del")
        Dim lnkDel As Image = e.Row.FindControl("imgTrash")
        Dim ctl As LinkButton = e.Row.FindControl("dFooterBar")
        Dim lnkCheckOut As Image = e.Row.FindControl("imgCheckOut")
        Dim lnkUndoCheckOut As Image = e.Row.FindControl("imgUndoCheckOut")
        Dim lnkWF As Image = e.Row.FindControl("imgWF")
        
        If Not lnkDel Is Nothing Then
            If e.Row.DataItem.delete Then
                lnkDel.Visible = True
            Else
                lnkDel.Visible = False
            End If
        End If
        
        If Not lnkDelete Is Nothing Then
            If e.Row.DataItem.Delete Then
                lnkDelete.ImageUrl = "/" + WebUtility.MyPage(Me).SiteFolder + "/HP3Image/Ico/content_grestore.gif"
                lnkDelete.ToolTip = "Restore item"
                lnkDelete.CommandName = "LogicalRestore"
            Else
                lnkDelete.ImageUrl = "/" + WebUtility.MyPage(Me).SiteFolder + "/HP3Image/Ico/content_delete.gif"
                lnkDelete.ToolTip = "Delete item"
                lnkDelete.CommandName = "LogicalDelete"
            End If
        End If
        
        If Not lnkActive Is Nothing Then
                        
            If e.Row.DataItem.Active Then
                lnkActive.ImageUrl = "/" + WebUtility.MyPage(Me).SiteFolder + "/HP3Image/Ico/content_active.gif"
                lnkActive.ToolTip += "Active"
            Else
                lnkActive.ImageUrl = "/" + WebUtility.MyPage(Me).SiteFolder + "/HP3Image/Ico/content_noactive.gif"
                lnkActive.ToolTip += "No Active"
            End If
                        
        End If
               
        If Not ctl Is Nothing Then
            ctl.Attributes.Add("onclick", "return confirm('This operation will remove the content. Continue?')")
        End If
        
        'gestione dell'icona check-out
        If Not lnkCheckOut Is Nothing Then
            Dim contentPK As Integer = e.Row.DataItem.Key.PrimaryKey
            Dim userValue As UserValue
            
            If (statusManager.IsEditCheckOut(contentPK)) Then
                userValue = New UserValue
                userValue = CheckedOutTo(contentPK)
                lnkCheckOut.Visible = True
                lnkCheckOut.ToolTip = "Checked out to " & userValue.Surname & " " & userValue.Name & " exclusively"
                
                If Not statusManager.IsEditCheckOut(contentPK, masterPageManager.GetTicket.User.Key.Id) Or (masterPageManager.GetTicket.Roles.HasRole("CMS", "cmsadmin")) Then
                    lnkUndoCheckOut.Visible = True
                    lnkCheckOut.Visible = False
                Else
                    lnkUndoCheckOut.Visible = False
                End If
            Else
                lnkCheckOut.Visible = False
                lnkUndoCheckOut.Visible = False
            End If
        End If
        
        If Not lnkWF Is Nothing Then
            Select Case e.Row.DataItem.ApprovalStatus
                Case ContentValue.ApprovalWFStatus.Approved
                    lnkWF.ToolTip = "Approved"
                Case ContentValue.ApprovalWFStatus.NotApproved
                    lnkWF.ToolTip = "Not Approved"
                Case ContentValue.ApprovalWFStatus.ToBeApproved
                    lnkWF.ToolTip = "To Be Approved"
            End Select
        End If
    End Sub

    Protected Sub GridContents_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        If GridContents.FooterRow Is Nothing Then Return
        For cellNum As Integer = GridContents.Columns.Count - 1 To 1 Step -1
            GridContents.FooterRow.Cells.RemoveAt(cellNum)
        Next
        GridContents.FooterRow.Cells(0).ColumnSpan = GridContents.Columns.Count
        GridContents.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        Dim startIndex As Integer = GridContents.PageSize * GridContents.PageIndex
        Dim s As String = IIf(GridDataSource.Count > 1, "s", "")
        If FilterByTopItems Then
            'msg.Text = "Showing latest " & GridDataSource.Count & " content" & s & " of " & gridViewTotal & " total."
        Else
            'msg.Text = String.Format("Showing content{0} {1} to {2} of {3} content{4} found.", s, startIndex + 1, startIndex + GridContents.Rows.Count, gridViewTotal, s)
        End If
    End Sub
    
    Private Sub GridContents_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If AllowSelect And UseInPopUp Then
            If e.Row.RowType = DataControlRowType.Header Then
                Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                If Not chkAll Is Nothing Then
                    chkAll.Attributes.Add("onClick", "SelectAllRows(event)")
                End If
            End If
        
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If Not chk Is Nothing Then
                    chk.Attributes.Add("onClick", "javascript:SelectRow(event)")
                End If
                e.Row.Attributes.Add("onClick", "javascript:SelectRow(event)")
                e.Row.Attributes.Add("title", "Click to toggle the selection of this row")
            End If
        End If
        If AllowSelect And UseInPopUp Then
            e.Row.Cells(0).Visible = True
        Else
            If Not e.Row.RowType = DataControlRowType.Pager Then
                e.Row.Cells(0).Visible = False
            End If
        End If
    End Sub
    
    Public Function GridContentRowsSelected() As ContentCollection
        Dim _oCollection As New ContentCollection
        Dim gr As GridViewRow
        For Each gr In GridContents.Rows
            Dim chk As CheckBox = CType(gr.FindControl("chkSelect"), CheckBox)
            If Not chk Is Nothing Then
                If chk.Checked Then
                    Dim pk As Literal = CType(gr.FindControl("lit_pk"), Literal)
                    Dim _CM As New ContentManager
                    _oCollection.Add(_CM.Read(New ContentIdentificator(Integer.Parse(pk.Text))))
                End If
            End If
        Next
        Return _oCollection
    End Function
   
    Sub ItemCommand(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim param = s.commandArgument.split(",")
        Dim commandType As String = param(0)
        Dim contentID As String = param(1)
        Dim contentLangId As String = param(2)

        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
        
        wr.KeycontentType = New ContentTypeIdentificator("CMS", commandType)
        objQs.Language_id = contentLangId
        objQs.ContentId = Int32.Parse(contentID)
        
        wr.customParam.append(objQs)
        Response.Redirect(mPage.FormatRequest(wr))
    End Sub
    
    Sub ItemApproval(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim param = s.commandArgument.split(",")
        Dim idContent As String = param(0)
        Dim idLang As String = param(1)
        Dim request As New WebRequestValue
        Dim objQs As New ObjQueryString
        
        request.KeycontentType = New ContentTypeIdentificator("CMS", "wfmcontent")
        objQs.Language_id = idLang
        objQs.ContentId = idContent
        request.customParam.append(objQs)
        Response.Redirect(masterPageManager.FormatRequest(request))
    End Sub
    
    Function RowSelectionActive(ByVal param1 As Boolean, ByVal param2 As Boolean) As String
        If param1 And param2 Then Return "True"
        Return "False"
    End Function
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, masterPageManager.GetLang.Code)
    End Function
    
    'restituisce i dati relativi all'utente che ha fatto check-out sul content
    Function CheckedOutTo(ByVal contentPK As Integer) As UserValue
        contentEditStatusValue = statusManager.ReadEditStatus(contentPK)
        Return userManager.Read(contentEditStatusValue.KeyUser)
    End Function
    
    'effettua undo check-out del content
    Sub UndoContentCheckOut(ByVal sender As Object, ByVal envent As ImageClickEventArgs)
        'l'operazione di undo check-out su un content pu� essere fatta solo
        'dall'utente che ha fatto il check-out sul content o da un utente che ha un ruolo di amministratore
        If sender.CommandName = "UndoCheckOut" Then
            Dim idPrimakey As Integer = sender.CommandArgument()
            
            If Not statusManager.IsEditCheckOut(idPrimakey, masterPageManager.GetTicket.User.Key.Id) Or (masterPageManager.GetTicket.Roles.HasRole("CMS", "cmsadmin")) Then
                Dim contentEditStatus As ContentEditStatusValue = statusManager.ReadEditStatus(idPrimakey)
                If Not (contentEditStatus Is Nothing) Then
                    'elimino il check-out senza fare il check-in
                    statusManager.Remove(contentEditStatus.Key)
                    BindGrid()
                      
                End If
            End If
        End If
    End Sub
    
    Function getWFStatus(ByVal contentvalue As ContentValue) As String
        Dim status As String = ""
        
        Select Case contentvalue.ApprovalStatus
            Case Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.Approved
                status = "contentwf_approval"
            Case Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.NotApproved
                status = "contentwf_notapproval"
            Case Healthware.HP3.Core.Content.ObjectValues.ContentValue.ApprovalWFStatus.ToBeApproved
                status = "contentwf_tobeapproval"
        End Select
        
        Return status
    End Function
    
    'controlla se esiste un content approval workflow 
    'associato ad un tema
    Sub CheckWorkFlow()
        Dim themeManager As New ThemeManager
        themeId = Request("T")
        Dim wfId As Integer = 0
        ExistWorkFlow() = False
        
        Dim themeValue As ThemeValue = themeManager.Read(New ThemeIdentificator(themeId))
        If Not (themeValue Is Nothing) Then
            wfId = themeValue.KeyWorkFlow.Id
        End If
        
        If (wfId <> 0) Then
            ExistWorkFlow() = True
        End If
    End Sub
    
    Function GetWidh() As Integer
        Return 130
    End Function
       
</script>

<script language="javascript">
    	function SelectRow(e)
    	{
    		//var obj = window.event;
			var obj;
			if (window.event){
				obj=window.event.srcElement
			}
			else
			{
				obj=e.target
			}
			
			//alert((obj.tagName);
			
			if(obj.tagName=="INPUT")    //checkbox
    		{
    		    checkRowOfObject(obj);
    		}
    		else if (obj.tagName=="TD") //cella
    		{
    		    //se il mouse punta alla riga 
    		    var row = obj.parentNode;
    		    var chk = row.cells[0].getElementsByTagName('input');
    		    chk[0].checked = !chk[0].checked;
				

    		    if (chk[0].checked)
    		    {
					
    		        row.style.backgroundColor = '#FDFFD2'
    		    }
    		    else
    		    {
                    row.style.backgroundColor = '#ffffff'
    		    }
    		}
    	}
		
		function checkRowOfObject(obj)
    	{
    	    if (obj.parentNode.parentNode.style.backgroundColor != '')
			{
				if (obj.checked)
		        {
    				obj.parentNode.parentNode.style.backgroundColor='#FDFFD2'
		            
		        }
		        else
		        {
                    obj.parentNode.parentNode.style.backgroundColor='#ffffff'
		        }
			}
    	}
		
    	function SelectAllRows(e)
    	{
			var chkAll;
			if (window.event){
				chkAll=window.event.srcElement
			}
			else
			{
				chkAll=e.target
			}
			
    	   var tbl = chkAll.parentNode.parentNode.parentNode.parentNode;
    	   if (chkAll)
    	   {
    	        for(var i=1;i<tbl.rows.length-1;i++)
    	        {
    	            var chk = tbl.rows[i].cells[0].getElementsByTagName('input');
    	            chk[0].checked=chkAll.checked;
    	            checkRowOfObject(chk[0]);
    	        }
    	   }
    	}
    	
    	function DecToHex(n){
        //converte da decimale (0..255) a esadecimale (stringa a due caratteri)
        hex=n.toString(16);
        if(hex.length==1) hex="0"+hex; /*aggiunge lo zero davanti se � un numero con una cifra sola*/
        return hex.toUpperCase();
        }

        function HexToDec(s){
        //converte da stringa esadecimale a numero decimale
        return parseInt(s,16);
        } 
</script>
<asp:Label runat="server" ID="msg"/>
<%--<asp:HiddenField  ID="Actualpage"  runat="server" Value="1"/>

<div style ="float:right; text-align:right;">
     <asp:Repeater ID="rpPager" runat="server" Visible="true" >
        <ItemTemplate>
        <%#IIf(Actualpage.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>
                        <asp:LinkButton ID="linkPager" runat="server" OnClick="linkPager_PageIndex" 
                        Visible='<%#iif (Actualpage.value=DataBinder.Eval(Container.DataItem, "value"),"false","true") %>' 
                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "value")%>' ><%#DataBinder.Eval(Container.DataItem, "text")%></asp:LinkButton>
        </ItemTemplate>
     </asp:Repeater>
</div>--%>

<div align ="center">
    <asp:GridView ID="GridContents" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        OnRowCreated="GridContents_RowCreated"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        AllowSorting="true" 
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None" 
        OnSorting ="GridContents_Sorting"
        OnDataBinding="GridContents_DataBinding" 
        OnRowDataBound="GridContents_RowDataBound" 
        OnDataBound="GridContents_DataBound" >
        <Columns>
            <asp:TemplateField HeaderStyle-Width="2%" ItemStyle-Width="2%">
                <HeaderTemplate >
                    <asp:CheckBox id="chkSelectAll" ToolTip="Click here to select/deselect all rows" runat="server"  />
                </HeaderTemplate>
                <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server"   />
                        <asp:Literal ID="lit_pk" Text="<%#Container.DataItem.Key.PrimaryKey%>" visible="false" runat="server" />
                 </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="6%">
                <ItemTemplate>
                    <%If ExistWorkFlow() Then%>
                    <asp:ImageButton ID="imgWF" runat="server" ImageUrl='<%#"/HP3Office/HP3Image/Ico/" & getWFStatus(Container.DataItem) & ".gif"%>'/>
                    <%End If %>
                    <asp:Image id="imgCheckOut" runat="server" imageurl="~/HP3Office/HP3Image/ico/content_checkout.gif"/>
                    <asp:ImageButton ID="imgUndoCheckOut" ToolTip ="Undo Checkout" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_undo_checkout.gif" onClick="UndoContentCheckOut" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>' CommandName ="UndoCheckOut" />
                    <asp:Image id="imgTrash" runat="server" imageurl="~/HP3Office/HP3Image/ico/disabled.gif" tooltip="Content Deleted"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" SortExpression="Key.Id" HeaderStyle-Width="5%" ItemStyle-Width="15%">
                <ItemTemplate>
                    <a title="Id"><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></a>
                    <a title="Global Key"><%#DataBinder.Eval(Container.DataItem, "Key.IdGlobal")%></a>
                    <a title="Primary Key"><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Pub." SortExpression="DatePublish" HeaderStyle-Width="8%" ItemStyle-Width="8%">
                <ItemTemplate><%#getCorrectedFormatDate(DataBinder.Eval(Container.DataItem, "DatePublish"))%></ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="DatePublish" HeaderText="Date Pub." SortExpression="DatePublish" HeaderStyle-Width="8%" ItemStyle-Width="8%" HtmlEncode="False" DataFormatString="{0:d}"/>--%>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Language: <%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" HtmlEncode="false"/>
            <asp:TemplateField HeaderStyle-Width="240" ItemStyle-Width="220">
                <ItemTemplate> 
                    <%If AllowSelect And Not UseInPopUp Then%>
                        <asp:ImageButton ID="btn_select" ToolTip ="Select item" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="fireSelectedEvent" CommandArgument ='<%#GetKey(Container.Dataitem.Key.Id,Container.Dataitem.Key.IdLanguage)%>' CommandName ="Select" />
                    <%End If %>
                    <%If AllowEdit Then%>
                        <div align="center">
                            <asp:ImageButton ID="btn_newlang" ToolTip ="New language" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_newlanguage.gif" onClick="EditRow" CommandArgument ='<%#GetKey(Container.Dataitem.Key.Id,0)%>' CommandName ="NewRow" />
                            <asp:ImageButton ID="btn_update" ToolTip ="Edit selected item" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_edit.gif" onClick="EditRow" CommandArgument ='<%#GetKey(Container.Dataitem.Key.Id,Container.Dataitem.Key.IdLanguage)%>' CommandName ="UpdateRow" />
                            <asp:ImageButton ID="btn_del" runat="server" OnClientClick="return confirm('Confirm?')" onClick="EditRow" CommandArgument ='<%#GetKey(Container.Dataitem.Key.Id,Container.Dataitem.Key.IdLanguage)%>'  />
                            <asp:ImageButton ID="btnActive" runat="server" ToolTip="Content Status: " onclick="SetStatus" commandargument='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>' CommandName="SearchRefresh"/>
                            <asp:ImageButton ID="btn_context" ToolTip ="Context Relation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_context.gif" onClick="ItemCommand" CommandArgument ='<%#"ContentCtx," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />
                            <asp:ImageButton ID="btn_content_content" ToolTip ="Content Relation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_content.gif" onClick="ItemCommand" CommandArgument ='<%#"ContentCnt," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>'  />
                            
							<!--
							<asp:ImageButton ID="btn_content_roles"  ToolTip ="Content Roles" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_roles.gif"  onClick="ItemCommand" CommandArgument ='<%#"ContentRol," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />
                            <asp:ImageButton ID="btn_content_user" ToolTip ="Content User" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_user.gif" onClick="ItemCommand" CommandArgument ='<%#"ContentUsr," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />
                            <asp:ImageButton ID="btn_content_author" ToolTip ="Content Author" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_author.gif" onClick="ItemCommand" CommandArgument ='<%#"ContentAut," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />
                            <asp:ImageButton ID="btn_content_comments" ToolTip ="Comments Relation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_comment.png" onClick="ItemCommand" CommandArgument ='<%#"CntComment," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>'  />
							-->
                            <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageCoverUpload.aspx?codeLang=<%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>&idLang=<%#Container.DataItem.Key.IdLanguage%>&idContent=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','CoverImage','width=750,height=550')" style="cursor:pointer" title="Image Cover" alt="" />
							<!--
							<asp:ImageButton ID="btn_versioning" ToolTip ="Content history" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_history.gif" onClick="ItemCommand" CommandArgument ='<%#"Versioning," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />
							-->
                            <%If ExistWorkFlow() Then%>
                            <a href="#"  onclick="window.open('<%=strDomain%>/Popups/popWorkFlow.aspx?HiddenId=<%#Container.Dataitem.Key.Id%>&LangId=<%#Container.Dataitem.Key.IdLanguage%>&ThemeId=<%=themeId %>','ContentApprovalWorkFlow','width=600,height=500,left=400,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/content_workflow.gif' alt="Approval WorkFlow" /></a>
                            <%End If %>
                            <%--<asp:ImageButton ID="btn_approval" ToolTip ="Content Approval" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_history.gif" onClick="ItemApproval" CommandArgument ='<%#Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage%>' />--%>
                        </div>
                    <%End If %>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
    </asp:GridView>
</div>
