<%@ Control Language="VB" ClassName="CommonThemesTreeMenu" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"  %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Import  Namespace="Telerik.Web.UI"%>

<script runat="server">

    Public themeSearcher As New ThemeSearcher()
    Public themeKey As New ThemeIdentificator()
    Public themeID As New ThemeIdentificator()
    Public webRequest As New WebRequestValue()
    
    Private curSite As Integer
    Private curTheme As Integer
  
    Dim roleCollection As New RoleCollection
    Dim ListLink As Hashtable
    Dim StringMenu As String
    Dim StringMenuXML As String
    '************************************************************************
    
    Public Property idTheme() As Integer
        Get
            Return curTheme
        End Get
        Set(ByVal value As Integer)
            curTheme = value
        End Set
    End Property
    
    Public Property SiteId() As Integer
        Get
            Return curSite
        End Get
        Set(ByVal value As Integer)
            curSite = value
        End Set
    End Property



    Sub Page_Load()
        If curSite > 0 Then
            ListLink = GenericDataContainer.Item("SiteLinks")
            If ListLink Is Nothing Then
                ListLink = New Hashtable
            End If

            If Not Page.IsPostBack Then

               
                'inizio modifiche treeview1
                '****************modifiche per la cache
                Dim keyXML As String = "themesmenuXML|" & curSite & "|" & Me.ObjectTicket.User.Key.Id
                'Response.Write("themesmenuXML|" & curSite & "|" & Me.ObjectTicket.User.Key.Id & "<br>")
                StringMenuXML = CacheManager.Read(keyXML)
                If StringMenuXML = String.Empty Then
                    ' Response.Write("vuoto")
                    commonMenuTree.Nodes.Clear()
                    commonMenuTree.DataBind()
                    roleCollection = Me.BusinessMasterPageManager.GetTicket.Roles 'profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
                    LoadRootNodes(curTheme, curSite, 1)
                    CacheManager.Insert(keyXML, commonMenuTree.GetXml, 1, 5)
                Else
                    'Response.Write("ok")
                    commonMenuTree.Nodes.Clear()
                    commonMenuTree.DataBind()
                    commonMenuTree.LoadXmlString(StringMenuXML)
                End If
                '**************************************
                commonMenuTree.CollapseAllNodes()
                'seleziona nodo corrente
                Dim myNode As RadTreeNode = commonMenuTree.FindNodeByValue(Request("T"))

                If myNode Is Nothing Then
                    Dim cookie As HttpCookie = Request.Cookies("TreeTheme")
                    If Not cookie Is Nothing Then
                        'Response.Write(Request.Cookies("TreeTheme").Value.ToString)
                        myNode = commonMenuTree.FindNodeByValue(Request.Cookies("TreeTheme").Value.ToString())
                        'Response.End()
                    Else
                        'Response.Write("cookie vuoto")
                    End If
                End If
                
                If Not myNode Is Nothing Then
                    myNode.Selected = True
                    'effettuo l'expand del padre
                    While Not myNode.ParentNode Is Nothing
                        myNode.ParentNode.Expanded = True
                        myNode = myNode.ParentNode
                    End While
                End If
                'fine modifiche treeview

                Dim key As String = "themesmenu|" & curSite & "|" & Me.ObjectTicket.User.Key.Id
                StringMenu = CacheManager.Read(key)

                If StringMenu = String.Empty Then
                    roleCollection = Me.BusinessMasterPageManager.GetTicket.Roles 'profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
                    'fillMenu(curTheme, curSite, 1)
                    CacheManager.Insert(key, StringMenu, 1, 5)
                End If
                
                'LABEL.Text = StringMenu

            End If
        
            If Not GenericDataContainer.Exists("SiteLinks") Then GenericDataContainer.Add("SiteLinks", ListLink, True)
        End If
    End Sub
    
    Function getLink(ByVal theme As ThemeValue) As String
        webRequest = New WebRequestValue
        Dim objQs As New ObjQueryString()
        objQs.Clear()
        objQs.ContentId = 0
        objQs.SiteArea_Id = theme.KeySiteArea.Id
        objQs.ContentType_Id = theme.KeyContentType.Id
        objQs.Theme_id = theme.Key.Id
    
        webRequest.KeysiteArea.Id = theme.KeySiteArea.Id
       
        webRequest.customParam.append("page", 1)
        webRequest.customParam.append(objQs)
        
        webRequest.KeycontentType.Id = theme.KeyContentType.Id
        
        If Not ListLink.ContainsKey(theme.Key.Id.ToString) Then
            ListLink.Add(theme.Key.Id.ToString, webRequest)
        End If

        Return Me.BusinessMasterPageManager.FormatRequest(webRequest)
    End Function
    
    Sub fillMenu(ByVal id As Integer, ByVal site As Integer, ByVal round As Integer)
        themeSearcher.KeyFather.Id = id
        themeSearcher.KeySite.Id = site
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        
        Dim branch As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher) 'themeManager.Read(themeSearcher)
        Dim i As Integer
        StringMenu += "<ul class='lXMenu'>"
        For i = 0 To branch.Count - 1
            If Not roleCollection Is Nothing Then
                If roleCollection.HasRole(branch(i).ConsolleRole) Then
                    'men� relativo al sito selezionato    
                    If branch(i).Active And branch(i).Display Then
                        StringMenu += "<li><a href='" & getLink(branch(i)) & "' class='lXMenu'>" + branch(i).Description + "</a></li>"
                    Else
                        If Not branch(i).Active Then
                            'StringMenu += "<li><a href='" & getLink(branch(i)) & "' class='lXMenu' style=""color:#FF0000;text-decoration:line-through;background-color:#FFF;"">" + branch(i).Description + "</a></li>"
                            StringMenu += "<li><a href='" & getLink(branch(i)) & "' class='lXMenuNoActive'>" + branch(i).Description + "</a></li>"
                        Else
                            If Not branch(i).Display Then
                                'StringMenu += "<li><a href='" & getLink(branch(i)) & "' class='lXMenu' style=""color:#FF0000;background-color:#FFF;"">" + branch(i).Description + "</a></li>"
                                StringMenu += "<li><a href='" & getLink(branch(i)) & "' class='lXMenuDisable'>" + branch(i).Description + "</a></li>"
                            End If                                                       
                        End If
                    End If
                Else
                    If HasChild(branch(i).Key.Id(), site) Then
                        StringMenu += "<li>" + branch(i).Description + "</font></li>"
                    End If
                End If
            End If
            If HasChild(branch(i).Key.Id(), site) Then fillMenu(branch(i).Key.Id(), site, 1 + round)
        Next
        StringMenu += "</ul>"
    End Sub
        
    Function HasChild(ByVal idFather As Integer, ByVal idSite As Integer) As Boolean
        themeSearcher.KeyFather.Id = idFather
        themeSearcher.KeySite.Id = idSite
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        If Me.BusinessThemeManager.Read(themeSearcher).Count > 0 Then Return True
        Return False
    End Function
    
    Private Sub LoadRootNodes(ByVal id As Integer, ByVal site As Integer, ByVal round As Integer)        
        themeSearcher.KeyFather.Id = id
        themeSearcher.KeySite.Id = site
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        Dim branch As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher) 'themeManager.Read(themeSearcher)

        
        
        Dim themeValue As New ThemeValue
        For Each themeValue In branch
            
            Dim node As New RadTreeNode()
            If Not roleCollection Is Nothing Then
                If roleCollection.HasRole(themeValue.ConsolleRole) Then
                    node.Text = themeValue.Description
                    node.Value = themeValue.Key.Id
                    If themeValue.Active And themeValue.Display Then
                        node.CssClass = "lXMenu"
                        node.NavigateUrl = getLink(themeValue)
                        If HasChild(themeValue.Key.Id, site) Then
                            node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                            node.Expanded = True
                        End If
                        commonMenuTree.Nodes.Add(node)
                    Else
                        If Not themeValue.Active And Not themeValue.Display Then
                            node.CssClass = "lXMenuNoActive"
                            'node.Style.Add("color", "#FF0000")
                            'node.Style.Add("text-decoration", "line-through")
                            'node.Style.Add("background-color", "#FFF")
                            'node.NavigateUrl = getLink(themeValue)
                            If HasChild(themeValue.Key.Id, site) Then
                                node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                                node.Expanded = True
                            End If
                            commonMenuTree.Nodes.Add(node)
                        Else
                            If Not themeValue.Display And themeValue.Active Then
                                node.CssClass = "lXMenuDisable"
                                'node.Style.Add("color", "#FFF")
                                'node.Style.Add("background-color", "#FFF")
                                'node.NavigateUrl = getLink(themeValue)
                                node.NavigateUrl = getLink(themeValue)
                                If HasChild(themeValue.Key.Id, site) Then
                                    node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                                    
                                    node.Expanded = True
                                End If
                                commonMenuTree.Nodes.Add(node)
                            End If
                        End If
                    End If
                Else
                    If HasChild(themeValue.Key.Id, site) Then
                        'node.CssClass = "lXMenu"
                        'node.NavigateUrl = getLink(themeValue)
                        node.Text = themeValue.Description
                        node.Value = themeValue.Key.Id
                        node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                        node.Expanded = True
                        commonMenuTree.Nodes.Add(node)
                    End If
                End If
            Else
                If HasChild(themeValue.Key.Id, site) Then
                    'node.CssClass = "lXMenu"
                    'node.NavigateUrl = getLink(themeValue)
                    node.Text = themeValue.Description
                    node.Value = themeValue.Key.Id
                    node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                    node.Expanded = True
                    commonMenuTree.Nodes.Add(node)
                End If
            End If
        Next
        Dim keyXML As String = "themesmenuXML|" & curSite & "|" & Me.ObjectTicket.User.Key.Id
        CacheManager.RemoveByKey(keyXML)
        CacheManager.Insert(keyXML, commonMenuTree.GetXml, 1, 5)
    End Sub
      
    Protected Sub RadTreeView1_NodeExpand(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs) Handles commonMenuTree.NodeExpand
          
        e.Node.Nodes.Clear()
        themeSearcher.KeyFather.Id = e.Node.Value
        themeSearcher.KeySite.Id = curSite
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All

        Dim branch As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher) 'themeManager.Read(themeSearcher)
        roleCollection = Me.BusinessMasterPageManager.GetTicket.Roles 'profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
        
        If branch.Count > 0 Then
            Dim themeValue As New ThemeValue
            For Each themeValue In branch
                If Not roleCollection Is Nothing Then
                    If roleCollection.HasRole(themeValue.ConsolleRole) Then
                        Dim node As New RadTreeNode()
                        node.Text = themeValue.Description
                        node.Value = themeValue.Key.Id
                        

                        If themeValue.Active And themeValue.Display Then
                            node.CssClass = "lXMenu"
                            node.NavigateUrl = getLink(themeValue)
                        Else
                            If Not themeValue.Active And Not themeValue.Display Then
                                node.CssClass = "lXMenuNoActive"
                                'node.Style.Add("color", "#FF0000")
                                'node.Style.Add("text-decoration", "line-through")
                                'node.Style.Add("background-color", "#FFF")
                            Else
                                If Not themeValue.Display And themeValue.Active Then
                                    node.CssClass = "lXMenuDisable"
                                    node.NavigateUrl = getLink(themeValue)
                                    'node.Style.Add("color", "#FFF")
                                    'node.Style.Add("background-color", "#FFF")
                                End If
                            End If
                        End If
                        If HasChild(themeValue.Key.Id, curSite) Then
                            node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                        End If
                        e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                        e.Node.Expanded = True
                        e.Node.Nodes.Add(node)
                    Else
                        If HasChild(themeValue.Key.Id, curSite) Then
                            Dim node As New RadTreeNode()
                            node.Text = themeValue.Description
                            node.Value = themeValue.Key.Id
                            node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                            e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                            e.Node.Expanded = True
                            e.Node.Nodes.Add(node)
                        End If
                    End If
                Else
                    If HasChild(themeValue.Key.Id, curSite) Then
                        Dim node As New RadTreeNode()
                        node.Text = themeValue.Description
                        node.Value = themeValue.Key.Id
                        node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                        e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                        e.Node.Expanded = True
                        e.Node.Nodes.Add(node)
                    End If
                End If
            Next
        End If
        Dim keyXML As String = "themesmenuXML|" & curSite & "|" & Me.ObjectTicket.User.Key.Id
        CacheManager.RemoveByKey(keyXML)
        CacheManager.Insert(keyXML, commonMenuTree.GetXml, 1, 5)
    End Sub
    
    Protected Sub RadTreeView1_NodeCollapse(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs) Handles commonMenuTree.NodeCollapse
        e.Node.Expanded = False
        Dim keyXML As String = "themesmenuXML|" & curSite & "|" & Me.ObjectTicket.User.Key.Id
        CacheManager.RemoveByKey(keyXML)
        CacheManager.Insert(keyXML, commonMenuTree.GetXml, 1, 5)
    End Sub
   
</script> 

<asp:Label runat="server" ID="LABEL"/>
<script type="text/javascript" language="javascript">
function ClientNodeClicked(sender, eventArgs)
{
    var node = eventArgs.get_node();    
    document.cookie= 'TreeTheme' + '=' + escape(node.get_value())+';path=/';
}
</script>

<telerik:RadTreeView id="commonMenuTree" width="100%" height="400px"    
onnodeexpand="RadTreeView1_NodeExpand" 
onnodecollapse="RadTreeView1_NodeCollapse"
runat="server"   
enableviewstate="true" OnClientNodeClicked="ClientNodeClicked">
   
</telerik:RadTreeView>
