<%@ Control Language="VB" ClassName="ContentsMedia" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Media"%>
<%@ Import Namespace="Healthware.HP3.Core.Media.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing" %>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues" %>

<%@ Register TagPrefix ="HP3" TagName ="MediaList" Src ="~/hp3Office/HP3Parts/ctlMediaList.ascx" %>

<script runat="server">
    Private objQs As New ObjQueryString
    Private idContent As Int32
    Private idLanguage As Int16
    Private _idContentType As Int32
   
    Public isNew As Boolean
    Private strJS As String
    'Private dictionaryManager As New DictionaryManager
    Private objCMSUtility As New GenericUtility
    Private _oContentManager As ContentAccManager
    'Private masterPageManager As New MasterPageManager
    Private webRequest As New WebRequestValue
    Private hList As Hashtable
    Private idThemes As Int32
    Private _strDomain As String
    Private _language As String
    Private _contentVersion As Object
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property ContentVersion() As Object
        Get
            Return _contentVersion
        End Get
        Set(ByVal value As Object)
            _contentVersion = value
        End Set
    End Property
    
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = Me.PageObjectGetLang.Id
        
        
        webRequest.customParam.LoadQueryString()
        isNew = webRequest.customParam.item("N")
        idLanguage = webRequest.customParam.item("L")
        idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
    End Sub
    
    Sub Page_Load()
        'controllo se il content da visualizzare � una versione
        'o il content attuale
        Dim versionId As String = ""
        webRequest.customParam.LoadQueryString()
        versionId = webRequest.customParam.item("V")
        
        'se � una versione
        If (versionId <> String.Empty) Then
            
            Dim obj As Object = ReadVersion(versionId)
            If Not (obj Is Nothing) Then
                'Se ha trovato il content lo carica  
                oDownloadList.idRelated = obj.Key.Id
                oDownloadList.idLanguage = obj.Key.IdLanguage
                oDownloadList.LoadControl()
            End If
        Else 'se � il content attuale       
            'Recupero dei parametri
            LoadParam()
        
            If idContent <> 0 Then
                LoadControl()
            
                'Tramite contentService carico i controlli nella pagina master
                'Carico il content da cui eredita content
                CType(Me.Parent.Parent, Object).LoadMasterPage(oDownloadList.ReadContentValue, isNew)
           
            End If
            ReadControls()
        End If
    End Sub
    
    Sub ReadControls()
        'Dim accessService As New AccessService()
        Dim idUser As Integer = Me.BusinessMasterPageManager.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                ctl.style.add("display", strVis)
            End If
        Next
    End Sub
    
    Sub LoadControl()
        oDownloadList.idRelated = idContent
        oDownloadList.idLanguage = idLanguage
        oDownloadList.LoadControl()
    End Sub
    
    'legge una versione in base al suo Identificatore
    'restituisce l'oggetto memorizzato nella versione
    Function ReadVersion(ByVal versionId As Integer) As Object
        Dim versioningManager As New VersioningManager
        Dim versionIdent As New VersionIdentificator
        
        versionIdent.Id = versionId
        Dim versionValue As VersionValue = versioningManager.Read(versionIdent)
        Return versionValue.StoredObject
    End Function
    
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
        Dim FileExists As Boolean
        Dim oMediaManager As New MediaManager
        Dim oMediaValue As New MediaValue
        
        Try
            'Esiste il content passatomi nel buffer
            If Not objStore.Content Is Nothing Then
                objStore.Content.IdContentSubType = ContentValue.Subtypes.Media
                ClassUtility.Copy(objStore.Content, oMediaValue)
                
                'nel caso di ripristino
                'If (versionId <> String.Empty) Then
                '    ContentVersion() = ReadCongress(idCont, idLang)
                'End If
                                
                If isNew Then
                    'Salvo l'oggetto (salva banalmente il content)                   
                    oMediaValue = oMediaManager.Create(oMediaValue)
                    If oMediaValue.Key.IdGlobal <= 0 Then
                        Dim cm As New ContentManager
                        cm.Update(oMediaValue.Key, "Key.IdGlobal", oMediaValue.Key.Id)
                    End If
                Else
                    oMediaValue = oMediaManager.Update(oMediaValue)
                End If
                
                'Rimuovo tutti i file aventi la stessa estensione di quelli selezionati
                Dim oDownTypeValue As DownloadTypeValue
                Dim arrExistingFile As ArrayList = oDownloadList.ExisistingFile
                For Each oDI As Object In arrExistingFile
                    oMediaManager.Remove(oDI.oDownIdentificator)
                Next
                
                'Recupero la lista dei download
                Dim DownLoadList As ArrayList = oDownloadList.DownloadInList
                For Each item As Object In DownLoadList
                    'Recupero le informazioni sul singolo download
                    oDownTypeValue = item.oDownloadTypeValue
                    oDownTypeValue.KeyContent = oMediaValue.Key
                    
                    'Salvo il file rimuovendolo dalla cartella temporanea
                    'e controllo se nella cartella di destinazione il file gi� esiste
                    If MoveTmpFileToDownloadPath(oDownTypeValue, item.FileNametoCopy) Then
                        'e lo ricreo
                        oMediaManager.Create(oDownTypeValue)
                    Else
                        Try
                            IO.File.Delete(item.FileNametoCopy)
                        Catch
                        End Try
                    End If
                Next
                                                
                If Not oMediaValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    objStore = _oContentManager.Create(objStore, oMediaValue.Key)
                    
                    'Recupero dalla property ContentVersion il contentValue prima dell'update
                    objStore.ContentVersion = ContentVersion
                Else
                    objStore = Nothing
                End If
            End If
                       
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStore
    End Function
    
    Function MoveTmpFileToDownloadPath(ByVal item As DownloadTypeValue, ByVal FileName As String) As Boolean
        Try
           
            Dim destFileName As String = SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.MediaPath & "\" & item.FileName
            If IO.File.Exists(FileName) Then
                If IO.File.Exists(destFileName) Then           
                    IO.File.Delete(destFileName)
                End If
                
                IO.File.Move(FileName, destFileName)
            End If
            
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.PageObjectGetLang.Id, description)
    End Function
</script>
<table style="width:100%" class="form">
<tr runat="server" id="tr_oDownloadList">
    <td valign="top" >
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterDownloadList&Help=cms_HelpNewsletterDownloadList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" > <img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMediaList", "Media Files")%>
    </td>
    <td valign="top">
       <HP3:MediaList runat="server" id="oDownloadList" />
    </td>
</tr>
</table>