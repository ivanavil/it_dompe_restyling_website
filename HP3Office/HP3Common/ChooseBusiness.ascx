<%@ Control Language="VB" ClassName="ChooseBusiness" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@Import Namespace="Healthware.HP3.Core.Product" %>
<%@Import Namespace="Healthware.HP3.Core.Product.ObjectValues" %>
<%@Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>
<%@Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>
<%@ Register TagName ="ThemesSite" tagprefix="HP3" Src ="~/HP3Office/HP3Common/ThemesSite.ascx"%>

<script language="VB" runat="server">
    Private businessManager As New BusinessManager
    Private businessSearcher As BusinessSearcher
    Private webrequest As WebRequestValue
    
    Private oSiteCollection As SiteCollection
    Private oSiteSearcher As New SiteSearcher
    
    Private _strDomain As String
    Private _language As String
    Private _strJS As String
    Private _contType As Integer
    
    Private _chooseSite As Boolean = False
    
        
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
       
    Public Property ChooseSite() As Boolean
        Get
            Return _chooseSite
        End Get
        Set(ByVal value As Boolean)
            _chooseSite = value
        End Set
    End Property
    
    Private Property ContentType() As Integer
        Get
            Return _contType
        End Get
        Set(ByVal value As Integer)
            _contType = value
            
        End Set
    End Property
    
    Sub page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack()) Then
            LoadBusiness()
        End If
        
        If (ChooseSite) And (Not Page.IsPostBack()) Then
            LoadSites()
            trSite.Visible = True
        End If
    End Sub

    Sub LoadBusiness()
        businessSearcher = New BusinessSearcher
        Dim businessColl As BusinessCollection = businessManager.Read(businessSearcher)
    
        dwlBusiness.Items.Clear()
        If Not (businessColl Is Nothing) Then
            For Each businessValue As BusinessValue In businessColl
                dwlBusiness.Items.Add(New ListItem(businessValue.Description, businessValue.Key.Id))
            Next
        End If
        
        dwlBusiness.Items.Insert(0, New ListItem("Select Business", -1))
    End Sub
    
    Sub LoadSites()
        Dim curIdSite As Int32 = Me.ObjectSiteDomain.KeySiteArea.Id
               
        oSiteCollection = Me.BusinessSiteManager.Read(Me.ObjectTicket.Roles)
        
        Dim siteColl As New SiteCollection
        siteColl = oSiteCollection.DistinctBy("KeySiteArea.Id")
        
        dwlSite.Items.Clear()
        For Each oSiteValue As SiteValue In siteColl
            If curIdSite <> oSiteValue.KeySiteArea.Id Then
                dwlSite.Items.Add(New ListItem(oSiteValue.Label, oSiteValue.KeySiteArea.Id))
            End If
        Next
        
        dwlSite.Items.Insert(0, New ListItem("Select Site", -1))
    End Sub
    
    Sub LoadForm(ByVal sender As Object, ByVal e As EventArgs)
        If (CheckChoose()) Then
            
            webrequest = New WebRequestValue
            webrequest.KeyTheme.Id = Me.BusinessMasterPageManager.GetTheme.Id
            webrequest.KeysiteArea.Id = Me.BusinessMasterPageManager.GetSiteArea.Id
            webrequest.KeycontentType.Id = Me.BusinessMasterPageManager.GetContenType.Id
        
            webrequest.customParam.append("B", dwlBusiness.SelectedItem.Value)
            webrequest.customParam.append("L", lSelector.Language)
            If (ChooseSite) Then
                webrequest.customParam.append("S", dwlSite.SelectedItem.Value)
                webrequest.customParam.append("Cx", 0)
                webrequest.customParam.append("Cb", 0)
                webrequest.customParam.append("Cg", 0)
            End If
                     
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webrequest))
        Else
                      
            CreateJsMessage("Please, make your choice.")
        End If
    End Sub
    
    Function CheckChoose() As Boolean
        If (dwlBusiness.SelectedIndex <> 0) And (dwlSite.SelectedIndex <> 0) Then
            Return True
        End If
        
        Return False
    End Function
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        _strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript">
    <%=_strJS%>
</script>

<div class="dContent">
    <div class="title">Choose the correct business</div>
    <br />				
		
	<div id="divChoicemarket">
		<table class="form">
		    <tr>
		        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBusiness&Help=cms_HelpBusiness&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBusiness", "Business")%> </td>
		        <td><asp:DropDownList id="dwlBusiness"  runat="server" /></td>
		    </tr>
		    <tr>
                <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguage&Help=cms_HelpBusinessLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguage", "Language")%> </td>
                <td><HP3:contentLanguage id ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
            </tr>
            <tr id="trSite" runat="server" visible="false">
                <td valign ="top"><a href="#" id="siteLab" visible ="false" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpChooseSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%> </td>
                <td><asp:DropDownList id="dwlSite" runat="server" /></td>
             </tr>
         </table>
     	<br />
		
		<div id="divButt" style="text-align:right; width:300px">
		    <asp:Button id="btnNext" runat="server" cssClass="button" Text='Next' onClick="LoadForm" />	
		</div>
		
	</div>
</div>