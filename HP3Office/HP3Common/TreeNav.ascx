﻿<%@ Control Language="VB" ClassName="MenuTree" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>



<script language="vb" runat="server"> 
    Public themeManager As New ThemeManager()
    Public themeSearcher As New ThemeSearcher()
    Public themeKey As New ThemeIdentificator()
    Public themeID As New ThemeIdentificator()
    Public cTypeManager As New ContentTypeManager()
    Public webRequest As New WebRequestValue()
    Public mPageManager As New MasterPageManager()
    Public SiteMAnager As New SiteManager()
    
    'Public nodePath As New ArrayList
    
    '************************************************************************
    Private curSite As Integer
    Private curTheme As Integer
    Private style As Integer
  
    
    '************************************************************************
    
    Public Property idTheme() As Integer
        Get
            Return curTheme
        End Get
        Set(ByVal value As Integer)
            curTheme = value
        End Set
    End Property
    
    Public Property idSite() As Integer
        Get
            Return curSite
        End Get
        Set(ByVal value As Integer)
            curSite = value
        End Set
    End Property
    
    Public Property setStyle() As Integer
        Get
            Return style
        End Get
        Set(ByVal value As Integer)
            style = value
        End Set
    End Property
      
    '**************************************************************************
    Function getLink(ByVal theme As ThemeValue) As String
       
        Dim objQs As New ObjQueryString
        objQs.SiteArea_Id = theme.KeySiteArea.Id
        objQs.ContentType_Id = theme.KeyContentType.Id
        webRequest.KeysiteArea.Id = theme.KeySiteArea.Id
        'webRequest.KeycontentType.Id = theme.KeyContentType.Id
        'webRequest.KeysiteArea = theme.KeySiteArea
        ' If TypeOf theme Is ThemesConsolleValue Then
        ' webRequest.KeycontentType = CType(theme, ThemesConsolleValue).KeyConsolleContentType
        ' Else
        ' webRequest.KeycontentType = CType(theme, ThemeValue).KeyContentType
        'End If
        
     
        webRequest.customParam.append(objQs)
       
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    Sub fillTree(ByVal id As Integer, ByVal site As Integer)
        themeSearcher.KeyFather.Id = id
        themeSearcher.KeySite.Id = site
        Dim branch As ThemeCollection = themeManager.Read(themeSearcher)
        Dim i As Integer
      
      
        
        For i = 0 To branch.Count - 1
            Dim _ctype As ContentTypeIdentificator = New ContentTypeIdentificator(branch(i).KeyContentType.Id)
            Dim myNode As TreeNode = New TreeNode(branch(i).Description + " <i>(" + cTypeManager.Read(_ctype).Key.Name + ")</i>", branch(i).Key.Id)
            
           
            myTree.Nodes.Add(myNode)
            
            themeSearcher.KeyFather.Id = branch(i).Key.Id()
            themeSearcher.KeySite.Id = site
            If (themeManager.Read(themeSearcher).Count) > 0 Then
                addChild(myTree.Nodes.Item(i), branch(i).Key.Id(), site)
                myTree.Nodes.Item(i).NavigateUrl = getLink(branch(i))
                ' checkMyState(myTree.Nodes.Item(i).DataItem.ToString)
            End If
            
        Next
     
        
       
    End Sub
    
    Sub addChild(ByVal item As TreeNode, ByVal idtheme As Integer, ByVal idsite As Integer)
        Dim i As Integer
        themeSearcher.KeyFather.Id = idtheme
        themeSearcher.KeySite.Id = idsite
        Dim tcol As ThemeCollection = themeManager.Read(themeSearcher)
        For i = 0 To tcol.Count - 1
            Dim _ctype As ContentTypeIdentificator = New ContentTypeIdentificator(tcol(i).KeyContentType.Id)
            Dim myNode As TreeNode = New TreeNode(tcol(i).Description + " <i>(" + cTypeManager.Read(_ctype).Key.Name + ")</i>", tcol(i).Key.Id)
            item.ChildNodes.Add(myNode)
            
            item.ChildNodes.Item(item.ChildNodes.Count - 1).NavigateUrl = getLink(tcol(i))
            'checkMyState(item.ChildNodes.Item(item.ChildNodes.Count - 1).DataItem.ToString)
            themeSearcher.KeyFather.Id = tcol(i).Key.Id()
            themeSearcher.KeySite.Id = idsite
            If (themeManager.Read(themeSearcher).Count) > 0 Then
                addChild(item.ChildNodes.Item(i), tcol(i).Key.Id, idsite)
            End If
        Next
    End Sub
    
    
    Sub checkMyState(ByVal item As String)
        Response.Write(item)
        'If Not ViewState("nodePath") Is Nothing Then
        'If ViewState("nodePath").IndexOf(item.DataItem.key.id) <> -1 Then
        'item.Expand()
        'End If
        'End If
    End Sub
    
    
    Sub expandMe(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs)
        ViewState("nodePath").Add(e.Node.Value)
    End Sub
   
    Sub collapseMe(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs)
        ViewState("nodePath").Remove(e.Node.Value)
    End Sub
        
    Sub Page_Load()
        If ViewState("nodePath") Is Nothing Then
            ViewState("nodePath") = New ArrayList()
        End If
       
        If Not Page.IsPostBack Then
            fillTree(curTheme, curSite)
            myTree.ImageSet = style
        End If
    End Sub
 </script>
 <asp:TreeView runat="server" ID="myTree" EnableViewState="true" ExpandDepth="0" OnTreeNodeExpanded="expandMe" OnTreeNodeCollapsed="collapseMe" />


