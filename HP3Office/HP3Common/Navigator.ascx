<%@ Control Language="VB" ClassName="Navigator" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script runat="server">
    Private idSiteThemes As String
    Private idThemes As String
    Private hList As New Hashtable
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
      
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        LoadListParam()
                               
        If hList Is Nothing Then
            idThemes = WebUtility.MyPage(Me).idThemes
            idSiteThemes = WebUtility.MyPage(Me).ThemesSite           
        Else
            idThemes = hList("idThemes")
            idSiteThemes = hList("ThemesSite")
        End If
               
    End Sub
    
    Sub Page_Load()
        'LoadParam()
        
        'LoadInfoService()
        
        'LoadInfoSite()
    End Sub
    
    Sub LoadInfoService()
        Dim oThemesManager As New ThemeManager
        oThemesManager.Cache = False
        
        Dim oThemeValue As ThemeValue
                     
        If idThemes > 0 Then
                     
            oThemeValue = oThemesManager.Read(New ThemeIdentificator(idThemes))
        
            If Not oThemeValue Is Nothing Then
                main.Text = oThemeValue.Description
            End If
        End If
    End Sub
    
    Sub LoadInfoSite()
        Dim oSiteAreaManager As New SiteAreaManager
        oSiteAreaManager.Cache = False
        Dim oSiteSearcher As New SiteSearcher
        Dim oSiteAreaValue As SiteAreaValue
                
        If idSiteThemes <> 0 Then                        
            oSiteAreaValue = oSiteAreaManager.Read(New SiteAreaIdentificator(idSiteThemes))
           
            If Not oSiteAreaValue Is Nothing Then               
                site.Text = oSiteAreaValue.Name
                If main.Text.Length > 0 Then site.Text = site.Text & " / "
                '" (Id = " & oSiteAreaValue.Key.Id & ")"
            End If
            
        End If
    End Sub
</script>
<asp:Label runat ="server" id="site" class="title" /><asp:label runat ="server" id="main"  class="title"/>
    
