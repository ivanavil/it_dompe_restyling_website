<%@ Control Language="VB" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Register TagName ="ThemesSite" TagPrefix="HP3" Src ="~/hp3Office/HP3Common/ThemesSite.ascx" %>

<script runat="server">
#Region "Public Member"
    Public mPageManager As New MasterPageManager
    Public webRequest As New WebRequestValue
    Public siteManager As New SiteManager()
    Public themeManager As New ThemeManager()
    Public themeSearcher As New ThemeSearcher()
    Public themeKey As New ThemeIdentificator(siteManager.getDomainInfo.mainTheme)
#End Region
    
#Region "Private"
    Private visComboSite As Boolean
    Private idSiteThemes As String
    Private objQs As New ObjQueryString
    Private hList As Hashtable
#End Region
    
    Function getLink(ByVal theme As Object) As String
        webRequest.KeysiteArea = theme.KeySiteArea
        
        If TypeOf theme Is ThemesConsolleValue Then
            webRequest.KeycontentType = CType(theme, ThemesConsolleValue).KeyConsolleContentType
        Else
            webRequest.KeycontentType = CType(theme, ThemeValue).KeyContentType
        End If                      
        
        objQs.ContentType_Id = CType(theme, ThemeValue).KeyContentType.Id
        
        objQs.Theme_id = theme.key.id
        objQs.IsNew = 0
        objQs.ContentId = 0
        webRequest.customParam.append(objQs)
       
        Return mPageManager.FormatRequest(webRequest)
    End Function
       
    Function getMyChild(ByVal theme As ThemeValue) As ThemeCollection
        Dim accessService As New AccessService
        Dim profilingManager As New ProfilingManager
        Dim roleCollection As New RoleCollection
        roleCollection = profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
        
        themeSearcher.KeyFather = theme.Key
        
        Return themeManager.Read(themeSearcher).FilterByRoles(roleCollection)
       
    End Function
    	
    Function getMyChildAdded(ByVal theme As ThemeValue) As ThemeConsolleCollection
        Dim accessService As New AccessService
        Dim profilingManager As New ProfilingManager
        Dim roleCollection As New RoleCollection
        roleCollection = profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
       
        Dim themeSearcher As New ThemeSearcher()
        themeSearcher.KeySite = New SiteIdentificator(idSiteThemes)
        
        Return themeManager.Read(roleCollection, themeSearcher)
    End Function
        
    
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        LoadListParam()
                               
        If hList Is Nothing Then
            idSiteThemes = WebUtility.MyPage(Me).ThemesSite           
        Else
            idSiteThemes = hList("ThemesSite")            
        End If
                
    End Sub
    
    Sub page_load()
        LoadParam()
        
        If Not Page.IsPostBack Then
            Dim accessService As New AccessService
                   
            Dim profilingManager As New ProfilingManager
            Dim roleCollection As New RoleCollection
            roleCollection = profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
             
            themeSearcher.KeyFather = themeKey
           
            repMenuLev1.DataSource = themeManager.Read(themeSearcher).FilterByRoles(roleCollection)
            repMenuLev1.DataBind()
        End If
    End Sub
   
    Function GetDataSource(ByVal theme As ThemeValue) As Object
        If theme.Key.Id = 33 Then           
            Return getMyChildAdded(theme)
        Else            
            Return getMyChild(theme)
        End If
    End Function
    
    Function CastBooleantoString(ByVal Condition As Boolean) As String
        If Condition Then
            Return "True"
        Else
            Return "False"
        End If
    End Function
</script>

<div class="dNavigationBar">   
	<a class="fRight"><img alt="artLab" src="/hp3/_slice/logo/artlab.gif" /></a>
	<asp:repeater id="repMenuLev1" runat="server" >
		<HeaderTemplate>
			<ul class="ulNavigationBar">
		</HeaderTemplate>
		<ItemTemplate>		   
			<li class="liThemeLev1" >
				<a id="aThemeLev1Ind<%#Container.ItemIndex%>" href="#" class="">				
				    <%#DataBinder.Eval(Container.DataItem, "Description")%>
				</a>
				<div id="ulMenuOf<%#Container.ItemIndex%>"  class="ulMenu" style="">
				    <asp:Repeater ID="repMenuLev2" runat="server" DataSource='<%#getMyChild(Container.DataItem)%>' >				 
				   
				    <ItemTemplate>
				        <div class="liThemeLev2" >					        				       	                                  
				            <a  id="aThemeLev1Ind<%#Container.ItemIndex%>" class="aThemeLev1Unsel" href='#'><%#DataBinder.Eval(Container.DataItem, "Description")%></a>    				        				           				      
                            <HP3:ThemesSite ID="SiteList" 
                                    runat="server" 
                                    visible="<%#CastBooleantoString(Container.DataItem.Key.Id = 33)%>"                                    
                                    ComboStyle="width:200px"
                                    SiteId=<%#idSiteThemes%>
                            />  
                                         
				            <ul class="ulSubMenu">
				                <asp:Repeater ID="repMenuLev3" runat="server" DataSource='<%#GetDataSource(Container.DataItem)%>' >				               				              				                 
				    			<ItemTemplate>
				                <li class="liThemeLev3" >				              
				                <a class="" href='<%#getLink(Container.DataItem)%>'><%#DataBinder.Eval(Container.DataItem, "Description")%></a>
				                </li>
				                </ItemTemplate>
				                </asp:Repeater>				            				           				              
				            </ul>
				        </div>				      
				    </ItemTemplate>
				    </asp:Repeater>
				</div>
			</li>
		</ItemTemplate>
		<FooterTemplate>
			</ul>
		</FooterTemplate>	
	</asp:repeater>
</div>


