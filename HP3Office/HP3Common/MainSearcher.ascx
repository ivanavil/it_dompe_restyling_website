<%@ Control Language="VB" ClassName="MainSearcher" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private _style As String
    Private idLingua As Int32
    
    Public Property Style() As String
        Get
            Return _style
        End Get
        Set(ByVal value As String)
            Dim arr() As String = value.Split(";")
            Dim arr1() As String
            
            For Each str As String In arr
                If str <> "" Then
                    arr1 = str.Split(":")
                    btnSearch.Style.Add(arr1(0), arr1(1))
                End If
            Next
            
            mainTxt.Style = value
        End Set
    End Property
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        Dim _cManager As New ContentManager
        Dim _cValue As ContentValue
                
        _cManager.Cache = False
        _cValue = _cManager.Read(New ContentIdentificator(mainTxt.Text, idLingua))
    End Sub
    
    Sub Page_Load()
        idLingua = WebUtility.MyPage(Me).idCurrentLang
    End Sub
    
    Sub BuildLink(ByVal cValue As ContentValue)
        'Dim oWebRequest As New WebRequestValue
        
        'oWebRequest.Keycontent = cValue.Key
        'oWebRequest.KeycontentType = cValue.ContentType.Key
        
        'oWebRequest.customParam.append("idContentType", CType(theme, ThemeValue).KeyContentType.Id)
        'owebRequest.customParam.append("idTheme", theme.key.id)
        'owebRequest.customParam.append("isNew", 0)
    End Sub
</script>
<hp3:Text runat="server" id="mainTxt" TypeControl="NumericText"  /><asp:button ID="btnSearch" runat="server" Text="Search" onClick="Search"/>