<%@ Control Language="VB" ClassName="SiteAreaInfo" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<script runat="server">
    Public siteareaId As Integer
    Public contentType As Integer
    Public ThemesSite As Integer
    Private hList As New Hashtable
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        LoadListParam()
        
        siteareaId = WebUtility.MyPage(Me).mPageManager.GetSiteArea.id
        contentType = WebUtility.MyPage(Me).mPageManager.GetContenType.id
        
        If hList Is Nothing Then
            ThemesSite = WebUtility.MyPage(Me).ThemesSite        
        Else
            ThemesSite = hList("ThemesSite")         
        End If
               
    End Sub
    
    Sub page_load()
        'Dim saManager = New SiteAreaManager
        'Questi parametri dovrebbero gi� essere a disposizione.....
        'Cercare di non usare l'oggetto mPageManager
        LoadParam()
       
        
        If siteareaId <> 0 And contentType <> 0 And ThemesSite <> -1 Then
            sitareainfo.Visible = True
            Dim siteAreaManager = WebUtility.MyPage(Me).siteAreaManager
            Dim saI As New SiteAreaIdentificator(siteareaId)
            Dim saV As SiteAreaValue = siteAreaManager.read(saI)
            
            Dim roleColl As RoleCollection = siteAreaManager.ReadRoles(saI)
            If roleColl.Count > 0 Then
                RolesStatus.ImageUrl = "/hp3Office/HP3Image/ico/lock.gif"
                roles.DataSource = roleColl
               
                roles.DataTextField = "Description"
                roles.DataBind()
            Else
                RolesStatus.ImageUrl = "/hp3Office/HP3Image/ico/unlock.gif"
            End If
            roles.DataSource = roleColl
            roles.DataBind()
           
            ' Name.Text = "<h3>" & saV.Name & "</h3>"
            status.Checked = saV.Status
            
        Else
            sitareainfo.Visible = False
        End If
        
    End Sub
</script>
<asp:PlaceHolder runat="server" ID="sitareainfo">
<div><asp:Label runat="server" ID="Name" Visible="false"/>
<asp:CheckBox runat="server" Text="Enabled" ID="status" />
<br />
<asp:Image runat="server" ID="RolesStatus"  BorderWidth="0"/>
<asp:ListBox runat="server" ID="roles" Visible="false"/>

</div>
</asp:PlaceHolder>
