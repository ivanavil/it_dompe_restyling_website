<%@ Control Language="VB" ClassName="LastViewedService" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ import Namespace ="Healthware.HP3.Core.web" %>
<%@ import Namespace ="Healthware.HP3.Core.web.objectvalues" %>
<%@ import Namespace ="System.Collections.Generic" %>
<%@ import Namespace ="Healthware.HP3.Core.Site" %>
<%@ import Namespace ="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ import Namespace ="Healthware.HP3.Core.Utility.CMS.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="server">
    ''' <summary>
    ''' Link memorizzato nel GenericDataContainer
    ''' </summary>
    ''' <remarks></remarks>
    Class LastLink
        Implements IComparable
        
        Public Address As String
        Public Name As String
        Public Order As Int32
        Public Key As String
        
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Return Order.CompareTo(obj.order) * -1
        End Function
    End Class
    
    Public accService As New AccessService()

    'Lista con i riferimenti ai link del sito              
    Private hsLinkList As Hashtable
    'Lista con i riferimenti ad oggetti di tipo LastLink
    Private sListService As ArrayList
    'Numero massimo di elementi consentiti nella lista visibile
    Private nMaxItem As Int16
    'Numero di elementi contenuti nello sharedobject
    Private ItemsInList As Int16
    'Lista contenente i link non visibili
    Private arrOther As New ArrayList
    
    Private oGenericUtility As New GenericUtility
    
    Function ReadThemeName(ByVal idTheme As Int32) As String
        Dim oThemValue As ThemeValue = oGenericUtility.GetThemeValue(idTheme)
        If Not oThemValue Is Nothing Then Return oThemValue.Description
        Return ""
    End Function
    
    ''' <summary>
    ''' Caricamento nello sharedobject del nuovo link
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadService()
        Dim mPageManager As New MasterPageManager
        'URL corrente
        Dim sUrl As String = Request.Url.ToString
        Dim oWebOut As WebRequestValue
        Dim curLink As String
        Dim item As LastLink       
        Dim index As Int16
        
        'id dei Servizi
        Dim idThemes, urlThemes As Int32
        'Recupero la lista con i riferimenti dei servizi della consolle
        hsLinkList = GenericDataContainer.Item("SiteLinks")
        'Recupero la lista con i riferimenti degli ultimi servizi visitati
        sListService = ListSerivice
        
        If Not hsLinkList Is Nothing Then
            'Ciclo tra i servizi del sito
            For Each strKey As String In hsLinkList.Keys
                
                If IsNumeric(strKey) Then
                    idThemes = strKey
                    
                    'Recupero l'oggetto
                    oWebOut = hsLinkList(strKey)
                    oWebOut.customParam.LoadQueryString()
                    'Dall' oggetto il link
                    curLink = mPageManager.FormatRequest(oWebOut)
                    
                    'Recupero l'id del tema relativo alla pagina corrente(sUrl) 
                    urlThemes = ReadTheme(sUrl)
                   
                    'Se i link coincidono
                    If idThemes = urlThemes Then 'idThemes = urlThemes Then                        
                        'E se non � stata gi� inserita nella lista                                                                           
                        If Not Exists(strKey, sListService, index) Then
                            'La creo
                            item = New LastLink
                        
                            item.Address = curLink
                            item.Name = ReadThemeName(Int32.Parse(idThemes))
                            item.Order = MaxOrder(sListService)
                            item.Key = strKey
                        
                            'e la aggiungo alla lista recuperata dallo sharedobject
                            sListService.Add(item)
                        Else
                            'Se esiste (Caso in cui rivisito un servizio gi� visitato)
                            'Ne cambio la posizione nella coda dandogli maggiore priorit�
                            sListService(index).Order = MaxOrder(sListService)
                        End If
                    
                        'Esco dal ciclo
                        Exit For
                    End If
                End If
            Next
            
        End If
        
        'Ordino la lista in base al campo order e la salvo nello sharedobject
        sListService.Sort()
        GenericDataContainer.Add("LastViewService", sListService, True)
        
    End Sub
    
    ''' <summary>
    ''' Dato un link ne restituisce l'id del tema
    ''' </summary>
    ''' <param name="Url"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadTheme(ByVal Url As String) As Int32
        Dim objQs As New ObjQueryString(Url)
        Return objQs.Theme_id
    End Function
    
    ''' <summary>
    ''' Controlla l'esistenza di un oggetto di tipo LastLink nella ArrayList passata in input
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <param name="arr"></param>
    ''' <param name="Index"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Exists(ByVal Key As String, ByVal arr As ArrayList, ByRef Index As Int32) As Boolean
        Dim item As Object
        
        For i As Int16 = 0 To arr.Count - 1
            item = arr(i)
            If item.Key = Key Then
                Index = i
                Return True
            End If
            
        Next
        Return False
    End Function
    
    ''' <summary>
    ''' Recupera dall'arraylist in input il massimo valore per il campo Order
    ''' </summary>
    ''' <param name="arr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function MaxOrder(ByVal arr As ArrayList) As Int16
        Dim item As Object
        Dim max As Int16 = 0
        For i As Int16 = 0 To arr.Count - 1
            item = arr(i)
            If item.Order > max Then max = item.Order
        Next
    
        Return max + 1
    End Function
        
    ''' <summary>
    ''' Confronta due url
    ''' </summary>
    ''' <param name="Url1"></param>
    ''' <param name="Url2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function MatchURL(ByVal Url1 As String, ByVal Url2 As String) As Boolean
        Dim bOut As Boolean = False
        Dim arrUrl1() As String = Url1.Split("?")
        Dim arrUrl2() As String = Url2.Split("?")
        
        Dim objQs1 As New ObjQueryString(Url1)
        Dim objQs2 As New ObjQueryString(Url2)
        
        If arrUrl1(0).ToLower = arrUrl2(0).ToLower Then
            bOut = ObjQueryString.Match(objQs1, objQs2)
        End If
        
        Return bOut
    End Function
     
    ''' <summary>
    ''' Restituisce la lista recuperata dallo sharedobject.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property ListSerivice() As ArrayList
        Get
            Dim listLastService As ArrayList = GenericDataContainer.Item("LastViewService")
            If listLastService Is Nothing Then
                listLastService = New ArrayList
            End If
            
            Return listLastService
        End Get
    End Property
    
    ''' <summary>
    ''' Esegue il Bind Del repeater
    ''' </summary>
    ''' <remarks></remarks>
    Sub DataBound()
        'Recupero la lista degli ultimi link
        Dim ListAll As ArrayList = ListSerivice
        Dim i As Int32 = 0
        'Leggo l'id del tema relativo alla pagina corrente
        Dim Key As String = ReadTheme(Request.Url.ToString)
        'Lista dei servizi visibili                  
        Dim arrVisible As New ArrayList
       
        If Not ListAll Is Nothing Then
            'Salvo il numero di elementi della lista recuperata dallo sharedobject
            ItemsInList = ListAll.Count
            
            'Se la lista contiene + elementi di quelli consentiti
            If ItemsInList > nMaxItem Then
                i = 0
                'Ciclo fino a quando la lista contiene nMaxItem elementi
                Do While arrVisible.Count < nMaxItem
                    'Se il servizio i-esimo � diverso da quello corrente(request.url)
                    'lo aggiunge.
                    'Se uguale non f� nulla in quanto si tratta proprio del servizio corrente
                    'che non deve essere visualizzato nella lista visibile
                    If ListAll(i).key <> Key Then
                        arrVisible.Add(ListAll(i))
                    End If
                    i += 1
                Loop
               
                'Recupero gli altri servizi da visualizzare nel Layer esclusi quello corrente
                'e quelli gi� presenti nella lista arrVisible
                For i = nMaxItem To ItemsInList - 1
                    If ListAll(i).key <> Key AndAlso arrVisible.IndexOf(ListAll(i)) = -1 Then
                        arrOther.Add(ListAll(i))
                    End If
                Next
            Else
                'ItemsInList <= nMaxItem � soddisfatta quindi visualizzo tutti i servizi eccetto quello corrente
               
                For i = 0 To ItemsInList - 1
                    If ListAll(i).key <> Key Then
                        arrVisible.Add(ListAll(i))
                    End If
                Next
            End If
        End If
        
        'Visualizzo la lista
        rpService.DataSource = arrVisible
        rpService.DataBind()
        If rpService.Items.Count <= 0 AndAlso Key = 0 Then rpService.Visible = False
    End Sub
    
    Sub Page_Load()
        If Not accService.IsAuthenticated Then Exit Sub
        'Recupero il numero massimo di elementi consentiti nella lista visibile
        nMaxItem = 5 'ConfigurationsManager.NumberLastViewedService
        
        'Eseguo il caricamento degli elementi attualmente presenti nella lista
        DataBound()
        
        'Carico la lista di elementi da visualizzare nella funzione DataBound
        LoadService()
    End Sub

    ''' <summary>
    ''' TEST.
    ''' Stampa gli elementi contenuti nello shared object    
    ''' </summary>
    ''' <remarks></remarks>
    Sub Print()
        Dim arr As ArrayList = ListSerivice
        For Each item As Object In arr
            Response.Write(item.Name & "<br/>")
        Next
    End Sub
    
    
    Sub DataBound(ByVal s As Object, ByVal e As RepeaterItemEventArgs)
        'Nel caso del footer
        If e.Item.ItemType = ListItemType.Footer Then
            'Recupero il Link "..."
            Dim obj As HyperLink = e.Item.FindControl("hlOther")
            'Recupero il repeater
            Dim rp As Repeater = e.Item.FindControl("rpOther")
            'Recupero la lista dallo sharedobjec
            Dim ListAll As ArrayList = ListSerivice
       
            If Not obj Is Nothing Then
                'Imposto il javascript per la visualizzazione del Layer
                obj.Attributes.Add("onclick", "Show()")
                'Se la lista contiene elementi
                If arrOther.Count > 0 Then
                    'La visualizzo
                    rp.DataSource = arrOther
                    rp.DataBind()
                    'E rendo visibile il Layer
                    obj.Visible = True
                Else
                    obj.Visible = False
                End If
            End If
        End If
    End Sub
    
    Function FormatName(ByVal name As String) As String
        Dim lenghtMax As Int16 = 15
        Dim strName As String = name
        If name.Length > lenghtMax Then
            strName = name.Substring(0, lenghtMax) & "..."
        End If
        Return strName
    End Function
    
    Function GetDescriptionTheme() As String
        Dim Key As String = ReadTheme(Request.Url.ToString)
        Dim oThemeManager As New ThemeManager
        
        Dim os As New ThemeSearcher
        os.Active = SelectOperation.All
        os.Display = SelectOperation.All
        os.Key.Id = Key
        Return oThemeManager.Read(os)(0).Description()
    End Function
</script>

<asp:repeater ID="rpService" runat="server" OnItemDataBound ="DataBound" EnableViewState="false">
<HeaderTemplate>
<img src="/hp3Office/HP3Image/ico/last_ico.gif" align="left"/>
<table class="form">
<tr>
<td style="padding:1 1 1 1 false;height:24px;" valign="middle"><span class="title">Last Viewed: <%#GetDescriptionTheme%></span>&nbsp;|</td>
</HeaderTemplate>
<ItemTemplate >
<td style="padding:1 1 1 1 false;height:24px;" valign="middle">
    <a href="<%#Container.DataItem.Address%>" Title="<%#Container.DataItem.Name%>" class="lXMenu">&nbsp;<%#FormatName(Container.DataItem.Name)%>&nbsp;|</a>            
</td>
</ItemTemplate>
<FooterTemplate>
<td>
    <asp:HyperLink ID="hlOther" runat="server" text="..." style="cursor:pointer" CssClass="lXMenu"/>
    <asp:repeater ID="rpOther" runat="server" EnableViewState="false" >
        <HeaderTemplate >
            <div id ="divOther" style="margin:12px 0 0 -15px;position:absolute;width:150px;display:none;border:1px solid #000;padding:2px;background-color:#EEE">
        </HeaderTemplate>
        <ItemTemplate>
            <div>
            <a  href="<%#Container.DataItem.Address%>" class="lXMenu">
              <%#Container.DataItem.Name%>
            </a>
            </div>                
        </ItemTemplate> 
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:repeater>
</td>
</tr>
</table>
</FooterTemplate>
</asp:repeater>

    
<script type ="text/javascript" >
    function Show()
        {
            document.getElementById('divOther').style.display=''           
            setTimeout('Hide()',2000)
        }
        
     function Hide()
        {
            document.getElementById('divOther').style.display='none'            
        }
</script>