<%@ Control Language="VB" ClassName="CategoriesTree" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@Import Namespace="Healthware.HP3.Core.Product.ObjectValues" %>
<%@Import Namespace="Healthware.HP3.Core.Product" %>
<%@Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>

<script language="VB" runat="server">
    Private ContextBusinessManager As New ContextBusinessManager
    Private ctxBussColl As ContextBusinessCollection
    
    'Assunzione:
    'le costanti sono settate con il valore dlla 'Label' presente nel DB 
    'dei seguenti ContextBusiness : Brand, ProductType, SurgeryType
    Const byBrand As String = "By Brand"
    Const byProductType As String = "By Product Type"
    Const bySurgeryType As String = "By Surgery Type"
    Const byWoundType As String = "By Wound Type"
    Const byWoundManagement As String = "By Wound management"
        
    Private idFranchise As Integer
    Private idContextGroup As Integer
    
    'Const Products As String = "Products"
    'Const Categories As String = "Categories"
    
    'Private _type As String = ""
    
    'Public Property Type() As String
    '    Get
    '        Return _type
    '    End Get
    '    Set(ByVal value As String)
    '        _type = value
    '    End Set
    'End Property
        
    Sub page_load()
            
        'Select Case Type
        'Case Categories
        ctxBussColl = ReadCategories()
        If Not (ctxBussColl Is Nothing) And (ctxBussColl.Count = 0) Then lbMesg.Text = "Sorry, but there is not a categories's tree for this market and language."
       
        'Case Products
        '    ctxBussColl = ReadFranchises()
        '    'If Not (ctxBussColl Is Nothing) And (ctxBussColl.Count = 0) Then ltMsg.Text = "Sorry, but there is not a categories's tree for this market and language."
        'End Select
    End Sub
    
    'legge i franchise filtrando per lingua, mercato 
    'Function ReadFranchises() As ContextBusinessCollection
    '    Dim businessFranchise As New ContextBusinessCollection
      
    '    Dim franciseColl As ContextCollection = ReadFranciseByGroup()
               
    '    If Not (franciseColl Is Nothing) AndAlso (franciseColl.Count > 0) Then
    '        businessFranchise = getBusinessFranchiseKilds(franciseColl)
                                
    '        rp_frinchiseNode.DataSource = businessFranchise
    '        rp_frinchiseNode.DataBind()
                        
    '    End If
    '    Return businessFranchise
    'End Function
    
    'legge i franchise filtrando per lingua, mercato 
    Function ReadCategories() As ContextBusinessCollection
        Dim businessFranchise As New ContextBusinessCollection
      
        Dim franciseColl As ContextCollection = ReadFranciseByGroup()
               
        If Not (franciseColl Is Nothing) AndAlso (franciseColl.Count > 0) Then
            businessFranchise = getBusinessFranchiseKilds(franciseColl)
                                
            rp_menufranchise.DataSource = businessFranchise
            rp_menufranchise.DataBind()
                        
        End If
        Return businessFranchise
    End Function
    
    'usato nel binding del repeater di livello 1
    'permette di recuperare tutti i figli di un frinchise attraverso la chiave del padre (frinchise)
    'e l'id del contextGroup che in questo caso ha Domain = CVT e Name = searchertype
    Function GetFrinchiseKilds(ByVal contextBusinessValue As ContextBusinessValue) As ContextBusinessCollection
        Dim idCB As Integer = contextBusinessValue.KeyContextBusiness.Id
       
        Dim contextSearcher As New ContextSearcher
        contextSearcher.KeyFather.Id = idCB
                    
        contextSearcher.KeyContextGroup = ReadGroupIdentificator("searchtype", "CVT")
        
        BusinessContextManager.Cache = False
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        Dim businessFranchiseKilds As New ContextBusinessCollection
        
        If Not (contextColl Is Nothing) AndAlso (contextColl.Count > 0) Then
            businessFranchiseKilds = getBusinessFranchiseKilds(contextColl)
        End If
        
        businessFranchiseKilds.Sort(ContextBusinessGenericComparer.SortType.ByOrder, ContextBusinessGenericComparer.SortOrder.ASC)
        Return businessFranchiseKilds
    End Function
    
    'usato nel binding del repeater di livello 2
    'permette di recuperare tutti i figli delle seguenti categorie : Brand, ProductType, SurgeryType
    Function GetCategoryByGroup(ByVal contextBusinessValue As ContextBusinessValue, ByVal franchise As Integer) As ContextBusinessCollection
        Dim ctxGroup As New ContextGroupIdentificator()
        Dim bussFrKilds As New ContextBusinessCollection
       
        Select Case contextBusinessValue.Description
            Case byBrand
                
                ctxGroup = ReadGroupIdentificator("brand", "CVT")
                bussFrKilds = GetCategory(franchise, ctxGroup)
            Case byProductType
                
                ctxGroup = ReadGroupIdentificator("prodtype", "CVT")
                bussFrKilds = GetCategory(franchise, ctxGroup)
            Case bySurgeryType
                
                ctxGroup = ReadGroupIdentificator("surgtype", "CVT")
                bussFrKilds = GetCategory(franchise, ctxGroup)
                
            Case byWoundType
                
                ctxGroup = ReadGroupIdentificator("wounType", "CVT")
                bussFrKilds = GetCategory(franchise, ctxGroup)
                
            Case byWoundManagement
                
                ctxGroup = ReadGroupIdentificator("woundManag", "CVT")
                bussFrKilds = GetCategory(franchise, ctxGroup)
        End Select
        
        Return bussFrKilds
    End Function
    
    'usato nel binding del repeater di livello 3/livello 4
    'quando gi� addentrati nel secondo livello, permette di recuperare tutti i figli 
    'della categoria con contextId = contextBusinessValue.KeyContextBusiness.Id e contextGroupId
    Function GetCategoryByFather(ByVal contextBusinessValue As ContextBusinessValue, ByVal group As Integer) As ContextBusinessCollection
        Dim keyContextBuss As Integer = contextBusinessValue.KeyContextBusiness.Id
        
        Return GetCategory(keyContextBuss, New ContextGroupIdentificator(group))
    End Function
    
    'funzione di supporto
    'restituisce una BusinessContextCollection di figli di una categoria, dati contextGroup e la categoria padre
    Function GetCategory(ByVal franchise As Integer, ByVal ctxGroup As ContextGroupIdentificator) As ContextBusinessCollection
        Dim contextSearcher As New ContextSearcher
        
        contextSearcher.KeyFather.Id = franchise
        contextSearcher.KeyContextGroup = ctxGroup
        
        BusinessContextManager.Cache = False
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        Dim businessFranchiseKilds As New ContextBusinessCollection
        
        If Not (contextColl Is Nothing) AndAlso (contextColl.Count > 0) Then
            businessFranchiseKilds = getBusinessFranchiseKilds(contextColl)
        End If
        
        businessFranchiseKilds.Sort(ContextBusinessGenericComparer.SortType.ByOrder, ContextBusinessGenericComparer.SortOrder.ASC)
        
        Return businessFranchiseKilds
    End Function
        
    
    'recupera le descrizione dei context filtrando per mercato e lingua
    Function getBusinessFranchiseKilds(ByRef franchiseKilds As ContextCollection) As ContextBusinessCollection
        Dim businessFranchiseKilds As New ContextBusinessCollection
                
        For Each franchiseChildren As ContextValue In franchiseKilds
            'recupero le descrizioni del context in base all lingua, mercato e idContext                        
            Dim contextBusinessSearcher As New ContextBusinessSearcher
            contextBusinessSearcher.KeyContextBusiness.Id = franchiseChildren.Key.Id
            contextBusinessSearcher.KeyContextBusiness.IdLanguage = Request("L")
            contextBusinessSearcher.KeyContextBusiness.IdBusiness = Request("B")
               
            ContextBusinessManager.Cache = False
            Dim contextBusinessColl As ContextBusinessCollection = ContextBusinessManager.Read(contextBusinessSearcher)
                
            If Not (contextBusinessColl Is Nothing) AndAlso (contextBusinessColl.Count > 0) Then
                businessFranchiseKilds.Add(contextBusinessColl(0))
            End If
        Next
            
        Return businessFranchiseKilds
    End Function
    
    'recupera tutti i context (franchise) 
    Function ReadFranciseByGroup() As ContextCollection
        Dim contextColl As ContextCollection = Nothing
        Dim contextIdent As New ContextGroupIdentificator
        contextIdent.Name = "franchise"
        contextIdent.Domain = "CVT"
        
        contextIdent.Id = ReadGroupIdentificator("franchise", "CVT").Id
        
        BusinessContextManager.Cache = False
        contextColl = Me.BusinessContextManager.ReadGroupContextRelation(contextIdent)
            
        Return contextColl
    End Function
    
    'recupera un ContextGroupIdentificator dato Domain e Name
    Function ReadGroupIdentificator(ByVal name As String, ByVal domain As String) As ContextGroupIdentificator
        Dim contextIdent As New ContextGroupIdentificator
        contextIdent.Name = name
        contextIdent.Domain = domain
        
        BusinessContextManager.Cache = False
        Dim contextGroupValue As ContextGroupValue = Me.BusinessContextManager.ReadGroup(contextIdent)
        If Not (contextGroupValue Is Nothing) Then
            Return contextGroupValue.Key
        End If
        
        Return Nothing
    End Function
    
    'setta l'id del frinchise
    Function setFranchise(ByVal id As Integer) As String
        idFranchise = id
        Return Nothing
    End Function
    
    'setta l'id del contextGroup
    Function SetContextGroup(ByVal id As Integer) As String
        Dim ctxGroupColl As ContextGroupCollection = Me.BusinessContextManager.ReadGroupContextRelation(New ContextIdentificator(id))
        
        If (Not (ctxGroupColl Is Nothing)) And (ctxGroupColl.Count > 0) Then
            idContextGroup = ctxGroupColl(0).Key.Id
        End If
                
        Return Nothing
    End Function
     
    
    Function getLink(ByVal contextBusiness As ContextBusinessValue, ByVal contextGroupId As Integer) As String
        Dim webRequest As New WebRequestValue()
        Dim site As Integer = Request("S")
        
        If (site = Nothing) Then site = 0
        webRequest.customParam.append("L", contextBusiness.KeyContextBusiness.IdLanguage)
        webRequest.customParam.append("B", contextBusiness.KeyContextBusiness.IdBusiness)
        webRequest.customParam.append("Cg", contextGroupId)
        webRequest.customParam.append("Cx", contextBusiness.KeyContextBusiness.Id)
        webRequest.customParam.append("Cb", contextBusiness.Key.Id)
        webRequest.customParam.append("S", site)
        
        Return Me.BusinessMasterPageManager().FormatRequest(webRequest)
    End Function
    
    Function getStyle(ByVal contextBusiness As ContextBusinessValue) As String
        If (Request("Cb") <> String.Empty) And (contextBusiness.Key.Id = Request("Cb")) Then
            
            Return "color:Blue"
        End If
        
        Return ""
    End Function
</script>


		
<div id="divCategTree">
      <div style="color:red">
        <asp:Literal id="lbMesg"  runat="server" />
      </div>
      <%--Repeater gerarchici: vengono utilizzati 5 repeater: --%>
      <%--Il 1�livello serve per visualizzare i Frinchises, gli altri per costruire l'albero delle categorie--%>
      <%--NOTA: necessario inserire un nuovo repeater per ciascun livello aggiuntivo dell'albero--%>
      <asp:Repeater id="rp_menufranchise" runat="server" >
	        <HeaderTemplate></HeaderTemplate>
	           <ItemTemplate>
	               <%#setFranchise(Container.DataItem.KeyContextBusiness.Id)%>
	               <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	               <a href='<%#getLink(Container.DataItem,idContextGroup) %>' style="font-weight:bold;text-decoration:none;color:#336699"> <%#DataBinder.Eval(Container.DataItem, "Label")%></a>
	                    <asp:Repeater id="rp_franchise_level_1" runat="server"   DataSource='<%#GetFrinchiseKilds(Container.DataItem)%>' >
	                        <HeaderTemplate><ul></HeaderTemplate>
	                            <ItemTemplate>
	                                     <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                    <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree" style='<%#getStyle(Container.DataItem) %>'><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
	                                     <asp:Repeater id="rp_franchise_level_2" runat="server"   DataSource='<%#GetCategoryByGroup(Container.DataItem,idFranchise)%>' >
	                                        <HeaderTemplate><ul></HeaderTemplate>
	                                            <ItemTemplate>
	                                                <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree" style='<%#getStyle(Container.DataItem) %>'><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
	                                                    <asp:Repeater id="rp_franchise_level_3" runat="server"   DataSource='<%#GetCategoryByFather(Container.DataItem,idContextGroup)%>' >
	                                                        <HeaderTemplate><ul></HeaderTemplate>
	                                                            <ItemTemplate>
	                                                                 <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                                  <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree" style='<%#getStyle(Container.DataItem) %>'><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
	                                                                        <asp:Repeater id="rp_franchise_level_4" runat="server"   DataSource='<%#GetCategoryByFather(Container.DataItem,idContextGroup)%>' >
	                                                                            <HeaderTemplate><ul></HeaderTemplate>
	                                                                                <ItemTemplate>
	                                                                                  <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                                                  <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree" style='<%#getStyle(Container.DataItem) %>'><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
    	                                                                                    <asp:Repeater id="rp_franchise_level_5" runat="server"   DataSource='<%#GetCategoryByFather(Container.DataItem,idContextGroup)%>' >
	                                                                                            <HeaderTemplate><ul></HeaderTemplate>
	                                                                                                <ItemTemplate>
	                                                                                                  <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                                                                  <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree" style='<%#getStyle(Container.DataItem) %>'><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
                    	                            
	                                                                                                   <%-- --%>
	                                                                                                  </ItemTemplate>
	                                                                                            <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                                                                            </asp:Repeater> 
	                                                                                   <%-- --%>
	                                                                                  </ItemTemplate>
	                                                                            <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                                                            </asp:Repeater>  
	                                                                    <%-- --%>
	                                                            </ItemTemplate>
	                                                        <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                                        </asp:Repeater>   
	                                               <%-- --%>
	                                            </ItemTemplate>
	                                          <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                        </asp:Repeater>   
	                            <%-- --%>
	                            </ItemTemplate>
	                        <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                        </asp:Repeater>
	              </ItemTemplate>
	         <FooterTemplate><div class="clearer"></div></FooterTemplate>
        </asp:Repeater>		
</div>

<%--<div id="divTree">
   <asp:Repeater id="rp_frinchiseNode" runat="server" >
	        <HeaderTemplate></HeaderTemplate>
	           <ItemTemplate>
	               <%#setFranchise(Container.DataItem.KeyContextBusiness.Id)%>
	               <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	               <div><a href='<%#getLink(Container.DataItem,idContextGroup) %>' style="font-weight:bold;text-decoration:none;color:#336699"> <%#DataBinder.Eval(Container.DataItem, "Label")%></a></div>
	                    <asp:Repeater id="rp_menufranchisekils" runat="server"   DataSource='<%#GetFrinchiseKilds(Container.DataItem)%>' >
	                        <HeaderTemplate><ul></HeaderTemplate>
	                            <ItemTemplate>
	                                    <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                    <div><li class="liCatTree"><a href='<%#getLink(Container.DataItem, idContextGroup) %>' class="aCatTree"><%#DataBinder.Eval(Container.DataItem,"Label")%></a></li></div>
	                            </ItemTemplate>
	                        <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                        </asp:Repeater>
	              </ItemTemplate>
	         <FooterTemplate><div class="clearer"></div></FooterTemplate>
        </asp:Repeater>
</div>--%>