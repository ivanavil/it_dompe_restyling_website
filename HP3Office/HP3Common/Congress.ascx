<%@ Control Language="VB" ClassName="Congress" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Localization" Src ="~/hp3Office/HP3Parts/ctlGeo.ascx" %>


<script runat="server">
    Private objQs As New ObjQueryString
    Private isNew As Boolean
    Private idContent As Int32
    Private _oCongressManager As CongressManager
    Private _contentManger As ContentManager
    Private _oCongressValue As CongressValue
    Private idLanguage As Int16
    Private objCMSUtility As New GenericUtility
    Private _oContentManager As ContentAccManager
    Private hList As Hashtable
    Private idThemes As Int32
    Private strName As String
    Private _strDomain As String
    Private _language As String
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private webRequest As New WebRequestValue
    
    Private statusManager As New StatusManager
    Private _contentVersion As Object
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property ContentVersion() As Object
        Get
            Return _contentVersion
        End Get
        Set(ByVal value As Object)
            _contentVersion = value
        End Set
    End Property
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = masterPageManager.GetLang.Id
        
        'LoadListParam()
                            
        'If hList Is Nothing Then
        '    isNew = WebUtility.MyPage(Me).isNew
        '    idLanguage = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        '    idThemes = WebUtility.MyPage(Me).idThemes
        'Else
        '    isNew = hList("isNew")
        '    idLanguage = objCMSUtility.ReadLanguage(hList("idCurrentLang"))
        '    idThemes = hList("idThemes")
                       
        'End If
        
        webRequest.customParam.LoadQueryString()
        isNew = webRequest.customParam.item("N")
        idLanguage = webRequest.customParam.item("L")
        idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
               
    End Sub
    
    Sub ReadControls()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                ctl.style.add("display", strVis)
            End If
        Next
    End Sub
    
    Sub Page_Load()
        'controllo se il content da visualizzare � una versione
        'o il content attuale
        Dim versionId As String = ""
        webRequest.customParam.LoadQueryString()
        versionId = webRequest.customParam.item("V")
        
        'se � una versione
        If (versionId <> String.Empty) Then
            
            Dim obj As Object = ReadVersion(versionId)
            If Not (obj Is Nothing) Then
                'Se ha trovato il content lo carica  
                _oCongressValue = obj
                LoadControl()
            End If
            
            DisableAllFormElements()
        Else 'se � il content attuale       
            'Recupero dei parametri
            LoadParam()
            'oLocalization.LoadControl()
            If idContent <> 0 Then
                
                If Not isNew Then
                    'gestisce il chech-in/check-out solo nel caso dell'edit sul content
                    CheckOutManagement()
                   
                    'Carico il congresso
                    _oCongressValue = ReadCongress(idContent, idLanguage)
                End If
                            
                If Not _oCongressValue Is Nothing Then
                    'Carico i controlli che derivano da content
                    'assegno a ContentVersion l'oggetto CongressValue prima che venga modificato (quello editato)
                    ContentVersion = _oCongressValue
                    LoadControl()
                End If
            
                'Tramite contentService carico i controlli nella pagina master
                'Carico il content da cui eredita content
                CType(Me.Parent.Parent, Object).LoadMasterPage(_oCongressValue, isNew)
            Else
            
                oLocalization.LoadControl()
            End If
            ReadControls()
        End If
    End Sub
    
    'gestisce il check-out/check-in di un content
    Sub CheckOutManagement()
        _contentManger = New ContentManager
                
        Dim idPrimarykey As Integer = _contentManger.ReadPrimaryKey(idContent, idLanguage)
       
        If statusManager.IsEditCheckOut(idPrimarykey) And Not isNew Then
            If statusManager.IsEditCheckOut(idPrimarykey, masterPageManager.GetTicket.User.Key.Id) Then
                DisableAllFormElements()
            End If
        End If
    End Sub
        
    Function ReadCongress(ByVal idCont As Integer, ByVal idLang As Integer) As CongressValue
        'Carico il congresso
        Dim congressValue As New CongressValue
        
        _oCongressManager = New CongressManager
        _oCongressManager.Cache = False
                        
        Dim oCongressSearcher As New CongressSearcher
        oCongressSearcher.Display = SelectOperation.All
        oCongressSearcher.Active = SelectOperation.All
        oCongressSearcher.Delete = SelectOperation.All
        oCongressSearcher.key.Id = idCont
        oCongressSearcher.key.IdLanguage = idLang
            
        Dim oCongressCollection As CongressCollection = _oCongressManager.Read(oCongressSearcher)
        If Not oCongressCollection Is Nothing AndAlso oCongressCollection.Count > 0 Then
            congressValue = oCongressCollection.Item(0)
            Return congressValue
        End If
        
        Return Nothing
    End Function
    
    'legge una versione in base al suo Identificatore
    'restituisce l'oggetto memorizzato nella versione
    Function ReadVersion(ByVal versionId As Integer) As Object
        Dim versioningManager As New VersioningManager
        Dim versionIdent As New VersionIdentificator
        
        versionIdent.Id = versionId
        Dim versionValue As VersionValue = versioningManager.Read(versionIdent)
        Return versionValue.StoredObject
    End Function
       
    
    Sub LoadControl()
        With _oCongressValue
            txtNote.Text = .Notes
                  
            If .DateStart.HasValue Then
                DateStart.Value = .DateStart.Value
            Else
                DateStart.Clear()
            End If
            
            If .DateEnd.HasValue Then
                DateEnd.Value = .DateEnd.Value
            Else
                DateEnd.Clear()
            End If
            
            txtEmail.Text = .Email
            TextLocalization.Text = .TextLocation
            'CongressFullText.Value = .FullText
            CongressFullText.Content = .FullText
            
            oLocalization.idRelated = idContent
            oLocalization.idLanguage = idLanguage
            oLocalization.LoadControl()
        End With
    End Sub
    
    ''' <summary>
    ''' Esegue il salvataggio dell'oggetto
    ''' </summary>
    ''' <param name="objStore"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
        webRequest.customParam.LoadQueryString()
        Dim versionId As String = webRequest.customParam.item("V")
        Dim idCont = webRequest.customParam.item("C")
        Dim idLang = webRequest.customParam.item("L")
        
        Try
            _oCongressManager = New CongressManager
            Dim oCongressValue As New CongressValue
            Dim oGeoCollection As Object
            
            'Esiste il content passatomi nel buffer
            If Not objStore.Content Is Nothing Then
                'Riverso il contentvalue nel congressvalue
                ClassUtility.Copy(objStore.Content, oCongressValue)
                               
                'Popolo l' oggetto con i dati nella form
                With oCongressValue
                    .Notes = txtNote.Text
                    .DateStart = DateStart.Value
                    .DateEnd = DateEnd.Value
                    .Email = txtEmail.Text
                    .TextLocation = TextLocalization.Text
                    '.FullText = CongressFullText.Value
                    .FullText = CongressFullText.Content
                    
                    objStore.GeoList = oLocalization.GeoList()
                End With
               
                'nel caso di ripristino
                If (versionId <> String.Empty) Then
                    ContentVersion() = ReadCongress(idCont, idLang)
                End If
                
                'Salvo l'oggetto
                If isNew Then
                    oCongressValue = _oCongressManager.Create(oCongressValue)
                    'If oCongressValue.Key.IdGlobal <= 0 Then
                    '    Dim cm As New ContentManager
                    '    cm.Update(oCongressValue.Key, "Key.IdGlobal", oCongressValue.Key.Id)
                    'End If
                Else
                    oCongressValue = _oCongressManager.Update(oCongressValue)
                End If
                                                
                If Not oCongressValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    objStore = _oContentManager.Create(objStore, oCongressValue.Key)
                    'Recupero dalla property ContentVersion il contentValue prima dell'update
                    objStore.ContentVersion = ContentVersion
                Else
                    objStore = Nothing
                End If
                                 
            End If
                       
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStore
    End Function
   
    'disattiva tutti gli elementi del form
    Sub DisableAllFormElements()
        TextLocalization.Disable()
        txtNote.Disable()
        txtEmail.Disable()
        
        divTxtEditor.Visible = True
        
        DateStart.Enable = False
        DateStart.Disable()
        DateEnd.Enable = False
        DateEnd.Disable()
                
        oLocalization.Disable()
    End Sub
    
    'attiva tutti gli elementi del form
    Public Sub EnableAllFormElements()
        TextLocalization.Enable()
        txtNote.Enable()
        txtEmail.Enable()
        
        divTxtEditor.Visible = False
        
        DateStart.Enable = True
        DateStart.Enabled()
        DateEnd.Enable = True
        DateEnd.Enabled()
        
        oLocalization.Enable()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>

<table style="width:99%" class="form">
    <tr><td style="width:25%" valign="top"></td><td></td></tr>
    <tr runat="server" id ="tr_oLocalization">
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLocalization&Help=cms_HelpLocalization&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLocalization", "Localization")%></td>
        <td><HP3:Localization id="oLocalization" runat="server" TypeControl="AccContentGeo" /></td>
    </tr>

    <tr runat="server" id="tr_TextLocalization">
        <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTextLocalization&Help=cms_HelpTextLocalization&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTextLocalization", "Text Localization")%></td>
        <td><HP3:Text ID="TextLocalization" runat="server" TypeControl ="TextBox" style="width:600px"/></td>
    </tr>
    <tr runat="server" id="tr_CongressFullText">
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullText&Help=cms_HelpFullText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullText", "Full Text")%></td>
        <td>    
           <table>
                <tr>
                     <td style="position:relative"><div id="divTxtEditor" class="disable"  style="width:600px;height:300px" visible="false" runat="server"></div>
<%--                     <FCKeditorV2:FCKeditor id="CongressFullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                     
                      <telerik:RadEditor ID="CongressFullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 
                                 
                                 <Links>
                                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                                   </telerik:EditorLink>
                                 </Links>
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                     </td>
                </tr>
           </table>
        </td> 
     </tr>

    <tr runat="server" id ="tr_txtNote">
        <td valign ="top">
           <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_HelpNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNote", "Note")%> 
        </td>
        <td>
            <HP3:Text runat="server" ID="txtNote" TypeControl ="TextArea" Style="width:600px"/>
        </td>
    </tr>
    <tr runat="server" id ="tr_DateStart">
        <td  valign ="top">
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateStart&Help=cms_HelpDateStart&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateStart", "Date Start")%> 
        </td>
        <td>
           <HP3:Date runat="server" id="DateStart"  Title="Date Start"  TypeControl="JsCalendar"  />
        </td>
    </tr>
    <tr runat="server" id ="tr_DateEnd">
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateEnd&Help=cms_HelpDateEnd&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateEnd", "Date End")%> </td>
        <td><HP3:Date runat="server" ID="DateEnd" Title="Date End" TypeControl="JsCalendar"  /></td>
    </tr>
    <tr runat="server" id ="tr_txtEmail">
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEmail&Help=cms_HelpEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEmail", "Email")%> </td>
        <td><HP3:Text runat="server" ID="txtEmail" TypeControl="TextEmail"  Style ="width:600px"  /></td>
    </tr>
</table>