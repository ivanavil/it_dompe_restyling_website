<%@ Control Language="VB" ClassName="ContentsDownload" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing" %>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName ="DownLoadList" Src ="~/hp3Office/HP3Parts/ctlDownLoadList.ascx" %>

<script runat="server">
    Private objQs As New ObjQueryString
    Private idContent As Int32
    Private idLanguage As Int16
    Private _idContentType As Int32
   
    Public isNew As Boolean = False
    Private dictionaryManager As New DictionaryManager
    Private objCMSUtility As New GenericUtility
    Private _oContentManager As ContentAccManager
    Private masterPageManager As New MasterPageManager
    Private webRequest As New WebRequestValue
    Private hList As Hashtable
    Private idThemes As Int32
    Private _strDomain As String
    Private _language As String
    Private _contentVersion As Object
    Private strJS As String = String.Empty
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property ContentVersion() As Object
        Get
            Return _contentVersion
        End Get
        Set(ByVal value As Object)
            _contentVersion = value
        End Set
    End Property
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = masterPageManager.GetLang.Id
        
        'LoadListParam()
                               
        'If hList Is Nothing Then
        '    isNew = WebUtility.MyPage(Me).isNew
        '    idLanguage = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        '    idThemes = WebUtility.MyPage(Me).idThemes
        'Else
        '    isNew = hList("isNew")
        '    idLanguage = objCMSUtility.ReadLanguage(hList("idCurrentLang"))
        '    idThemes = hList("idThemes")
        'End If
        
        webRequest.customParam.LoadQueryString()
        isNew = webRequest.customParam.item("N")
        idLanguage = webRequest.customParam.item("L")
        idThemes = webRequest.customParam.item("T")
        
        idContent = objQs.ContentId
    End Sub
    
    Sub Page_Load()
        'controllo se il content da visualizzare � una versione
        'o il content attuale
        Dim versionId As String = ""
        webRequest.customParam.LoadQueryString()
        versionId = webRequest.customParam.item("V")
        
        'se � una versione
        If (versionId <> String.Empty) Then
            
            Dim obj As Object = ReadVersion(versionId)
            If Not (obj Is Nothing) Then
                'Se ha trovato il content lo carica  
                oDownloadList.idRelated = obj.Key.Id
                oDownloadList.idLanguage = obj.Key.IdLanguage
                oDownloadList.LoadControl()
            End If
            
            DisableAllFormElements()
        Else 'se � il content attuale       
            'Recupero dei parametri
            LoadParam()
        
            If idContent <> 0 Then
                Dim _oDValue As New DownloadValue
            
                'Tramite contentService carico i controlli nella pagina master
                'Carico il content da cui eredita content
                If Not isNew Then
                    'gestisce il chech-in/check-out solo nel caso dell'edit sul content
                    'CheckOutManagement()
                   
                    'Carico il download
                    _oDValue = ReadDownload(idContent, idLanguage)
                End If
                            
                If Not _oDValue Is Nothing Then
                    'Carico i controlli che derivano da content
                    'assegno a ContentVersion l'oggetto CongressValue prima che venga modificato (quello editato)
                    ContentVersion = _oDValue
                    
                    LoadControl()
                End If
              
                CType(Me.Parent.Parent, Object).LoadMasterPage(oDownloadList.ReadContentValue, isNew)
            End If
            
            ReadControls()
            End If
    End Sub
    
    Sub ReadControls()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        Dim strVis As String
        For Each ctl As Object In Me.Controls
            If ctl.ID <> "" AndAlso ctl.ID.Substring(0, 3).ToLower = "tr_" Then
                If objCMSUtility.GetControlVisibility(New UserIdentificator(idUser), New ThemeIdentificator(idThemes), ctl.ID.Substring(3)) Then
                    strVis = ""
                Else
                    strVis = "none"
                End If
                
                ctl.style.add("display", strVis)
                 
            End If
        Next
    End Sub
    
    Sub LoadControl()
        oDownloadList.idRelated = idContent
        oDownloadList.idLanguage = idLanguage
        oDownloadList.LoadControl()
    End Sub
    
    'legge una versione in base al suo Identificatore
    'restituisce l'oggetto memorizzato nella versione
    Function ReadVersion(ByVal versionId As Integer) As Object
        Dim versioningManager As New VersioningManager
        Dim versionIdent As New VersionIdentificator
        
        versionIdent.Id = versionId
        Dim versionValue As VersionValue = versioningManager.Read(versionIdent)
        Return versionValue.StoredObject
    End Function
    
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
                   
        If (Not oDownloadList.DownloadInList Is Nothing) AndAlso (oDownloadList.DownloadInList.count > 0) Or (oDownloadList.DownloadCount >= 1) Then
            Try
                Dim FileExists As Boolean
                Dim oDownloadManager As New DownloadManager
                Dim oDownloadValue As New DownloadValue
        
                Dim versionId As String = webRequest.customParam.item("V")
                Dim idCont = webRequest.customParam.item("C")
                Dim idLang = webRequest.customParam.item("L")
                isNew = webRequest.customParam.item("N")
          
                'Esiste il content passatomi nel buffer
                If Not objStore.Content Is Nothing Then
                    'Riverso il contentvalue nel DownloadValue
                    ClassUtility.Copy(objStore.Content, oDownloadValue)
                
                    'nel caso di ripristino
                    If (versionId <> String.Empty) Then
                        ContentVersion() = ReadDownload(idCont, idLang)
                    End If
                                       
                    If isNew Then
                        'Salvo l'oggetto (salva banalmente il content)   
                        oDownloadValue = oDownloadManager.Create(oDownloadValue)
                        
                        'If oDownloadValue.Key.IdGlobal <= 0 Then
                        '    Dim cm As New ContentManager
                        '    cm.Update(oDownloadValue.Key, "Key.IdGlobal", oDownloadValue.Key.Id)
                        'End If
                    Else
                        oDownloadValue = oDownloadManager.Update(oDownloadValue)
                                       
                    End If
                
                   
                    
                    
                    'Rimuovo tutti i file aventi la stessa estensione di quelli selezionati
                   
                Dim oDownTypeValue As DownloadTypeValue
                Dim arrExistingFile As ArrayList = oDownloadList.ExisistingFile
                For Each oDI As Object In arrExistingFile
                    ' #519
                    'oDownloadManager.Remove(oDI.oDownIdentificator)
                   
                        Dim oDownValue As DownloadValue
                        oDownloadManager.Cache = False
                        oDownValue = oDownloadManager.Read(oDownloadValue.Key)
                        If Not oDownValue Is Nothing Then
                            For Each dt As DownloadTypeValue In oDownValue.DownloadTypeCollection
                                If dt.Key.Id = oDI.oDownIdentificator.Id Then
                     
                                    oDownloadManager.Remove(oDownValue.Key, New DownloadTypeValueIdentificator(dt.DownloadType.Key.Id), True)
                    
                                End If
                            Next
                        End If
                    Next
                    
                    
                Dim DownLoadList As ArrayList = oDownloadList.DownloadInList
                For Each item As Object In DownLoadList
                    'Recupero le informazioni sul singolo download
                    oDownTypeValue = item.oDownloadTypeValue
                    oDownTypeValue.KeyContent = oDownloadValue.Key
                      
                    'Salvo il file rimuovendolo dalla cartella temporanea
                    'e controllo se nella cartella di destinazione il file gi� esiste
                    If MoveTmpFileToDownloadPath(oDownTypeValue, item.FileNametoCopy) Then
                           
                        'e lo ricreo
                        oDownloadManager.Create(oDownTypeValue)
                    Else
                        Try
                            IO.File.Delete(item.FileNametoCopy)
                        Catch
                        End Try
                    End If
                Next
                                                
                If Not oDownloadValue Is Nothing Then
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    
                    objStore = _oContentManager.Create(objStore, oDownloadValue.Key)
                    objStore.ContentVersion = ContentVersion
                    'Recupero dalla property ContentVersion il contentValue prima dell'update
                                    
                Else
                    objStore = Nothing
                End If
                End If
                       
            Catch ex As Exception
            Throw ex
        End Try
            
            Return objStore
        Else
            CreateJsMessage("Please, select a file!")
        End If
              
    End Function
    
    Function MoveTmpFileToDownloadPath(ByVal item As DownloadTypeValue, ByVal FileName As String) As Boolean
        Try
           
            Dim destFileName As String = SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.DownloadPath & "\" & item.FileName
            If IO.File.Exists(FileName) Then
                If IO.File.Exists(destFileName) Then           
                    IO.File.Delete(destFileName)
                End If
                
                IO.File.Move(FileName, destFileName)
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    'legge un Download dato idContent e  idLanguage
    Function ReadDownload(ByVal idCont As Integer, ByVal idLang As Integer) As DownloadValue
        Dim DownloadValue As New DownloadValue
        
        'Carico ContentExtra
        Dim oDownloadManager As New DownloadManager
        oDownloadManager.Cache = False
        
        Dim oContentDSearcher As New DownloadSearcher
        
        oContentDSearcher.Display = SelectOperation.All
        oContentDSearcher.Active = SelectOperation.All
        
        oContentDSearcher.key.Id = idCont
        oContentDSearcher.key.IdLanguage = idLang
            
        Dim oDCollection As DownloadCollection = oDownloadManager.Read(oContentDSearcher) 'objCMSUtility.GetContentCollection(oContentExtraSearcher)
        If Not oDCollection Is Nothing AndAlso oDCollection.Count > 0 Then
            DownloadValue = oDCollection.Item(0)

           
            Return DownloadValue
        End If
        
        Return Nothing
    End Function
    
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    Sub DisableAllFormElements()
        'oDownloadList.DisableAllElements()
    End Sub
    
    Sub EnableAllFormElements()
        'oDownloadList.EnableAllElements()
    End Sub
</script>
<script type ="text/javascript" >
    <%=strJS%>
</script>
<table style="width:100%" class="form">
<tr runat="server" id="tr_oDownloadList">
    <td valign="top" >
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterDownloadList&Help=cms_HelpNewsletterDownloadList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" > <img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterDownloadList", "Download List")%>
    </td>
    <td valign="top">
       <HP3:DownLoadList runat="server" id="oDownloadList" />
    </td>
</tr>
</table>
