<%@ Control Language="VB" ClassName="SearchEngine" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Media"%>
<%@ Import Namespace="Healthware.HP3.Core.Media.ObjectValues"%>

<%@ Register TagName="ctlDate" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName ="Language" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="GridContent" TagPrefix="HP3" Src="~/hp3Office/HP3Common/GridContent.ascx"  %>

<script runat="server">
    Public Event SelectedEvent As EventHandler
    Public Event UpdateEvent As EventHandler
    Public Event SortingEvent As GridViewSortEventHandler
    
    Private _oContentManager As ContentManager
    Private _oContentValue As ContentValue
    Private _oContentCollection As ContentCollection
    Private _oContentSearcher As ContentSearcher
    
    Private oCollection As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    Private contentTypeSearcher As New ContentTypeSearcher
    Private contentTypeManager As New ContentTypeManager
    
    Private webRequest As New WebRequestValue
    Private utility As New WebUtility
        
    Private _idcontentType As String
    Private _allowSelect As Boolean = False
    Private _useInPopUp As Boolean = False
    Private _useInRelation As Boolean = False
    Private _allowEdit As Boolean = True
    Private idLanguage As Int32
    Private _idSite As String
    Private _onlyAssociatedContents As Boolean = False
    Private newctId As Integer
           
    Const PageSize As Integer = 20
       
    Public _sortType As ContentGenericComparer.SortType
    Public _sortOrder As ContentGenericComparer.SortOrder
    
    Private Property SortType() As ContentGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContentGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContentGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ContentGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ContentGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ContentGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Sub LoadPublicObjects()
        Dim bInitValue As Boolean = False
        If GenericDataContainer.Exists("PublicObjects") Then
            Dim hLists As Hashtable = GenericDataContainer.Item("PublicObjects")
           
            If Not hLists Is Nothing Then
                idLanguage = hLists("idCurrentLang")
                _idcontentType = hLists("idConsolleContentType")
                hiddenSiteArea.Value = hLists("idConsolleSiteArea")
                _idSite = hLists("ThemesSite")
                bInitValue = True
            End If
        End If
        
        If Not bInitValue Then
            idLanguage = WebUtility.MyPage(Me).idCurrentLang
            _idcontentType = WebUtility.MyPage(Me).idConsolleContentType
            hiddenSiteArea.Value = WebUtility.MyPage(Me).idConsolleSiteArea
            _idSite = WebUtility.MyPage(Me).ThemesSite
        End If
    End Sub
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        'if for search you need of field/fields remove the remark to the follow row
        'btnSearch.Attributes.Add("onClick", "if (!checkSearchParameters()) return false;")
        chkDeleted.Attributes.Add("onClick", "setChk()")
        chkDisplay.Attributes.Add("onClick", "setChk()")
        dwlSiteArea.Attributes.Add("onClick", "setDwl()")
        dwlContentType.Attributes.Add("onClick", "setDwl()")
        
        btnUpload.Attributes.Add("onClick", "if (!checkImportParameters()) return false;")
        
        If (Request("T") <> Nothing) Then
            Dim tv As ThemeValue
            tv = Me.BusinessThemeManager.Read(New ThemeIdentificator(Request("T")))
            If tv.KeyContent.Id > 0 Then newctId = tv.KeyContent.Id
        End If
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollection = siteAreaManager.Read(siteAreaSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.Visible = False Then Exit Sub
        ResetDate()
        LoadPublicObjects()
        
        If Not Page.IsPostBack Then
            CheckWorkFlow()
            BindDomains()
            BindSiteArea()
            BindContentType()          
            If (UseInPopUp) Then
                ShowContentType = True
                '    doSearch(True)
            End If
            If Not (UseInPopUp) Then doSearch()
        End If
    End Sub

    Sub ResetDate()
        txtDatePublishFrom.ShowDate = False
        txtDatePublishTo.ShowDate = False
    End Sub
    
    'popola la dwn per le siteArea
    Sub BindSiteArea()
        siteAreaSearcher.Key.Domain = dwlDomain.SelectedItem.Value
        
        Dim siteAreaColl As SiteAreaCollection = siteAreaManager.Read(siteAreaSearcher)
 
        utility.LoadListControl(dwlSiteArea, siteAreaColl, "Label", "Key.Id")
        dwlSiteArea.Items.Insert(0, New ListItem("Select SiteArea", -1))
    End Sub
    
    'popola la dwl per i contentType
    Sub BindContentType()
        If (OnlyAssociatedContents = True) Then contentTypeSearcher.OnlyAssociatedContents = True
        Dim contentTypeColl As ContentTypeCollection = contentTypeManager.Read(contentTypeSearcher)
                       
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwlContentType, contentTypeColl, "Description", "Key.Id")
        dwlContentType.Items.Insert(0, New ListItem("Select ContentType", -1))
    End Sub
    
    'Protected Sub btnShowAllContents_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    txtIdContent.Text = ""
    '    txtDatePublishFrom.Clear()
    '    txtDatePublishTo.Clear()
    '    txtTitle.Text = ""
    '    doSearch()
    'End Sub
    
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs)
        doSearch(False, True)
    End Sub
    
    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As EventArgs)
        If pnlImport.Visible = True Then
            pnlImport.Visible = False
            Exit Sub
        End If
        
        importSite.Text = Request("S")
        importSiteArea.Text = Request("Sa")
        importContentType.Text = Request("Ct")
        pnlImport.Visible = True
    End Sub
    
    Protected Sub btnShowAllContentsSiteArea_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtIdContent.Text = ""
        txtDatePublishFrom.Clear()
        txtDatePublishTo.Clear()
        txtTitle.Text = ""
        hiddenSiteArea.Value = 0
        doSearch()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        
        If (UseInPopUp) Then
            doSearch(True)
        Else
            doSearch()
        End If
    End Sub
        
    Public Sub doSearch()
        doSearch(False)
    End Sub
    
    Public Sub doSearch(ByVal onlyTopItems As Boolean, Optional ByVal isExport As Boolean = False, Optional ByVal page As Integer = 1)
        ViewState("OnlyTopItems") = onlyTopItems
        
        _oContentManager = New ContentManager
        _oContentSearcher = New ContentSearcher
        '_oContentSearcher.SetMaxRow = 200
        
        'se il searcher � usato nelle area CMS-ContentRelation
        _oContentSearcher.SortType = SortType()
        _oContentSearcher.SortOrder = SortOrder()
        _oContentSearcher.PageNumber = page
        _oContentSearcher.PageSize = PageSize
        
        If (UseInRelation) Then
                      
            Dim siteSelected As Integer = Request("s")
            _oContentSearcher.KeySite.Id = siteSelected
            _oContentSearcher.Active = SelectOperation.All
            _oContentSearcher.KeySite.Id = Int32.Parse(_idSite)
        Else 'in questo caso recupera i content considerando l'area e il contentType (caso News,Glossary,Article, ecc.)
                   
            _oContentSearcher.KeySite.Id = Int32.Parse(_idSite)
            _oContentSearcher.KeySiteArea.Id = Int32.Parse(hiddenSiteArea.Value)
            _oContentSearcher.KeyContentType.Id = Int32.Parse(_idcontentType)
            _oContentSearcher.Active = SelectOperation.All
        End If
          
        'setto il searcher in base alle scelte fatte dall'utente
        If chkDeleted.Checked Then _oContentSearcher.Delete = SelectOperation.All
        
        'commentato perch� in consolle devono uscire tutti gli stati
        'If Not (ExistWorkFlow()) Then _oContentSearcher.ApprovalStatus = ContentSearcher.ApprovalWFStatus.Approved
        
        If txtIdContent.Text <> "" Then
            _oContentSearcher.key.Id = txtIdContent.Text
        End If
        
        If txtGlobalKey.Text <> "" Then
            _oContentSearcher.key.IdGlobal = txtGlobalKey.Text
        End If
        If txtPrimaryKey.Text <> "" Then
            _oContentSearcher.key.PrimaryKey = txtPrimaryKey.Text
        End If
        
        If txtTitle.Text <> "" Then
            _oContentSearcher.SearchString = txtTitle.Text
        End If
        
        If lSelector.Language > 0 Then
            _oContentSearcher.key.IdLanguage = lSelector.Language
        End If
     
        If (chkDisplay.Checked) Then
            _oContentSearcher.Display = SelectOperation.Enabled
        Else
            _oContentSearcher.Display = SelectOperation.All
        End If
        
        'se il searcher � usato in una finestra di popUp
        If (UseInPopUp) Or (UseInRelation) Then
            ShowContentType = True
            _oContentSearcher.KeyContentType.Id = 0
            _oContentSearcher.KeySiteArea.Id = 0
            If (UseInPopUp) Then _oContentSearcher.KeySite.Id = 0
            
            If dwlContentType.SelectedValue <> -1 Then
                _oContentSearcher.KeyContentType.Id = dwlContentType.SelectedItem.Value
            End If
            
            If dwlSiteArea.SelectedValue <> -1 Then
                _oContentSearcher.KeySiteArea.Id = dwlSiteArea.SelectedItem.Value
            End If
        End If
        
        Try
            If txtDatePublishFrom.Value.HasValue And txtDatePublishTo.Value.HasValue Then
                If txtDatePublishFrom.Value.Value = txtDatePublishTo.Value.Value Then
                    _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.Equal
                Else
                    _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.Between
                End If
                _oContentSearcher.DatePublish.BoundDate = txtDatePublishFrom.Value
                _oContentSearcher.DatePublish.BoundDate2 = txtDatePublishTo.Value
                
            ElseIf txtDatePublishFrom.Value.HasValue And Not txtDatePublishTo.Value.HasValue Then
                _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.GreaterOrEqual
                _oContentSearcher.DatePublish.BoundDate = txtDatePublishFrom.Value
                _oContentSearcher.DatePublish.BoundDate2 = Nothing
            ElseIf Not txtDatePublishFrom.Value.HasValue And txtDatePublishTo.Value.HasValue Then
                _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.LessOrEqual
                _oContentSearcher.DatePublish.BoundDate = txtDatePublishTo.Value
                _oContentSearcher.DatePublish.BoundDate2 = Nothing
            Else
                _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.Equal
                _oContentSearcher.DatePublish.BoundDate = Nothing
                _oContentSearcher.DatePublish.BoundDate2 = Nothing
            End If
        Catch ex As Exception
            _oContentSearcher.DatePublish.Comparation = Healthware.HP3.Core.Base.ObjectValues.DateComparationType.ComparationType.Equal
            _oContentSearcher.DatePublish.BoundDate = Nothing
            _oContentSearcher.DatePublish.BoundDate2 = Nothing
        End Try

        _oContentManager.Cache = False
        
        _oContentCollection = _oContentManager.Read(_oContentSearcher)
        If _oContentCollection Is Nothing Then _oContentCollection = New ContentCollection
        
        'Grid.FilterByTopItems = onlyTopItems
        'Grid.Key = key
        LoadPager(_oContentCollection, page)
        Grid.GridDataSource = _oContentCollection
        Grid.BindGrid()
        Grid.Visible = True
        
        If isExport Then
            _oContentSearcher.PageNumber = 0
            _oContentSearcher.PageSize = 0
            
            Dim soExtra As New ContentExtraSearcher
            Dim soEvent As New CongressSearcher
            Dim ctm As New ContentTemplateManager
            Dim ct As ContentTemplateValue
            Dim ctv As ContentTypeValue
            Dim tv As ThemeValue
            ctv = Me.BusinessContentTypeManager.Read(_oContentSearcher.KeyContentType.Id)
            ct = ctm.Read(ctv.KeyContentsTemplate)
            tv = Me.BusinessThemeManager.Read(New ThemeIdentificator(Request("T")))
            Dim themeDescription As String = "NoTheme-"
            If Not tv Is Nothing Then
                themeDescription = CleanFileName(tv.Description)
                If themeDescription.Length > 25 Then
                    themeDescription = themeDescription.Substring(0, 25)
                End If
            End If
            Dim objColl As New Object
            Select Case ct.Key.Id
                Case 1
                    objColl = _oContentCollection
                    objColl = Me.BusinessContentManager.Read(_oContentSearcher)
                Case 2
                    ClassUtility.Copy(_oContentSearcher, soExtra)
                    objColl = Me.BusinessContentExtraManager.Read(soExtra)
                Case 3
                    ClassUtility.Copy(_oContentSearcher, soEvent)
                    objColl = Me.BusinessCongressManager.Read(soEvent)
            End Select
            Dim filename As String = "HP3ExportContents-" + themeDescription + "-" + DateTime.Now.ToUniversalTime.ToString("yyyyMMddhhmmss") + ".xml"
            Dim str As String = ClassUtility.SerializeObjectToXmlString(objColl, Encoding.UTF8)
           
            Response.Clear()
            Response.AddHeader("content-disposition", "fileattachment;filename=" + filename)
            Response.ContentType = "text/xml"
            Response.Write(str)
            Response.End()
        End If
    End Sub
    
    Function CleanFileName(ByVal value As String) As String
        Dim findString As String = " ,/,?,#,:,@,&reg;,&trade;,!"
        Dim findArray() As String = findString.Split(",")
        For Each find As String In findArray
            value = value.Replace(find, "")
        Next
        value = StringUtility.CleanAllHtmlEntity(StringUtility.CleanHtmlEntity(StringUtility.CleanHtml(value)))
        Return value
    End Function
    
    Sub ImportContent(ByVal sender As Object, ByVal e As EventArgs)
        If importSite.Text.Length > 0 And importSiteArea.Text.Length > 0 And importLang.Language > 0 And importMarket.Text.Length > 0 And importContentType.Text.Length > 0 Then
        Else
            Exit Sub
        End If
        Dim ImageContentPath As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.ImagePathDefault & "\Cover"
        Dim Path As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.DownloadPath & "\tmpDownloadPath"
        If Not IO.File.Exists(Path) Then
            IO.Directory.CreateDirectory(Path)
        End If
        If UploadXml.PostedFile.ContentLength > 0 Then
            Path = Path & "\" & UploadXml.FileName
            UploadXml.PostedFile.SaveAs(Path)
        End If
        Dim obj As Object
        Dim objFile As IO.StreamReader = IO.File.OpenText(Path)
        Dim strFile As String = objFile.ReadToEnd()
        objFile.Close()

        IO.File.Delete(Path)
        
        Dim ctm As New ContentTemplateManager
        Dim ct As ContentTemplateValue
        Dim ctv As ContentTypeValue
        ctv = Me.BusinessContentTypeManager.Read(Request("Ct"))
        ct = ctm.Read(ctv.KeyContentsTemplate)
        Dim col As Object
        Dim vo As Object
        Select Case ct.Key.Id
            Case 1
                col = New ContentCollection
                vo = New ContentValue
            Case 2
                col = New ContentExtraCollection
                vo = New ContentExtraValue
            Case 3
                col = New CongressCollection
                vo = New CongressValue
            Case 10
                col = New MediaCollection
                vo = New MediaValue
            Case Else
                Exit Sub
        End Select
        
        obj = ClassUtility.DeserializeXmlStringToObject(strFile, col.GetType)
        
        Dim oldKeyId As Integer
        
        For Each vo In obj
            oldKeyId = vo.Key.Id
            If isUpdate.Checked Then
                vo.DateInsert = DateTime.Now
                vo.DatePublish = DateTime.Now
                If vo.Key.IdGlobal > 0 Then
                    Dim updVo As ContentValue
                    Dim updSo As New ContentSearcher
                    updSo.key.IdGlobal = vo.Key.IdGlobal
                    updSo.key.IdLanguage = Integer.Parse(importLang.Language)

                    updVo = Me.BusinessContentManager.Read(updSo)(0)
                    If updVo.Key.PrimaryKey > 0 Then vo.Key = updVo.Key
                End If
            Else
                If Not IsNewLang.Checked Then vo.Key.Id = 0
                vo.Key.IdBusiness = Integer.Parse(importMarket.Text)
                vo.Key.IdLanguage = Integer.Parse(importLang.Language)
                vo.Key.PrimaryKey = 0
                vo.DateCreate = DateTime.Now
                vo.DateInsert = DateTime.Now
                vo.DatePublish = DateTime.Now
                vo.ContentType.Key.Id = Integer.Parse(importContentType.Text)
            End If
            If IsNewLang.Checked Then
                If vo.Key.Id <= 0 Then
                    vo.Key.Id = oldKeyId
                End If
                If Not isContentDefault.Checked And newctId > 0 Then
                    vo.Key.Id = newctId
                End If
                If refLang.Language > 0 And newctId = 0 Then
                    Dim newId As Integer = 0
                    Dim newSo As New ContentSearcher
                    newSo.key.IdGlobal = vo.Key.IdGlobal
                    newSo.key.IdLanguage = Integer.Parse(refLang.Language)
                    Try
                        newId = Me.BusinessContentManager.Read(newSo)(0).Key.Id
                    Catch ex As Exception
                    End Try
                    If newId > 0 Then vo.Key.Id = newId
                End If
            End If
            If isPageTitle.Checked Then
                Dim tvpt As ThemeValue
                tvpt = Me.BusinessThemeManager.Read(New ThemeIdentificator(Request("T")))
                If Not tvpt Is Nothing Then vo.PageTitle = tvpt.Description
            End If
            If IsUpdate.Checked Then
                Select Case ct.Key.Id
                    Case 1
                        vo = Me.BusinessContentManager.Update(vo)
                    Case 2
                        vo = Me.BusinessContentExtraManager.Update(vo)
                    Case 3
                        vo = Me.BusinessCongressManager.Update(vo)
                    Case 10
                        vo = Me.BusinessMediaManager.Update(vo)
                End Select
            Else
                Select Case ct.Key.Id
                    Case 1
                        vo = Me.BusinessContentManager.Create(vo)
                    Case 2
                        vo = Me.BusinessContentExtraManager.Create(vo)
                    Case 3
                        vo = Me.BusinessCongressManager.Create(vo)
                    Case 10
                        vo = Me.BusinessMediaManager.Create(vo)
                End Select
                If vo.Key.Id > 0 And Not IsNewLang.Checked Then
                    Me.BusinessContentManager.CreateContentSiteArea(vo.Key.Id, Integer.Parse(importSite.Text))
                    Me.BusinessContentManager.CreateContentSiteArea(vo.Key.Id, Integer.Parse(importSiteArea.Text))
                    'copy cover
                    Try
                        If IO.Directory.Exists(ImageContentPath) Then
                            If IO.File.Exists(ImageContentPath & "\" & oldKeyId & "_gen.jpg") Then
                                IO.File.Copy(ImageContentPath & "\" & oldKeyId & "_gen.jpg", ImageContentPath & "\" & vo.Key.Id & "_gen.jpg")
                            End If
                            If IO.File.Exists(ImageContentPath & "\" & oldKeyId & "_gen.gif") Then
                                IO.File.Copy(ImageContentPath & "\" & oldKeyId & "_gen.gif", ImageContentPath & "\" & vo.Key.Id & "_gen.gif")
                            End If
                            If IO.File.Exists(ImageContentPath & "\" & oldKeyId & "_gen.png") Then
                                IO.File.Copy(ImageContentPath & "\" & oldKeyId & "_gen.png", ImageContentPath & "\" & vo.Key.Id & "_gen.png")
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If
        Next
        'theme content default
        If isContentDefault.Checked Then
            Dim tv As ThemeValue
            tv = Me.BusinessThemeManager.Read(New ThemeIdentificator(Request("T")))
            If tv.KeyContent.Id = 0 Then
                tv.KeyContent.Id = vo.key.Id
                Me.BusinessThemeManager.Update(tv)
            End If
        End If
        pnlImport.Visible = False
        If isReport.Checked Then
            Response.AddHeader("content-disposition", "fileattachment;filename=HP3ImportContents-" + DateTime.Now.ToUniversalTime.ToString("yyyyMMddhhmmss") + ".xls")
            Response.ContentType = "application/ms-excel"
            
            Dim sw As String = String.Empty
            sw += "<table><tr><td>ID</td><td>Global ID</td><td>Business ID</td><td>Language ID</td><td>Title</td></tr></table>"
            sw += "<p></p>"
            sw += "<table>"
            
            For Each vo In obj
                sw += "<tr><td>" + vo.Key.Id.ToString + "</td><td>" + vo.Key.IdGlobal.ToString + "</td><td>" + vo.Key.IdBusiness.ToString + "</td><td>" + vo.Key.IdLanguage.ToString + "</td><td>" + vo.Title + "</td></tr>"
            Next
            sw += "</table>"
            Response.Write(sw)
            Response.End()
        End If
        doSearch()
    End Sub
    
    Public Property ShowContentType() As Boolean
        Get
            Return pnlPopUp.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlPopUp.Visible = value
        End Set
    End Property
    
    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
            Grid.AllowEdit = value
        End Set
    End Property
    
    Public Property AllowSelect() As Boolean
        Get
            Return _allowSelect
        End Get
        Set(ByVal value As Boolean)
            _allowSelect = value
            Grid.AllowSelect = value
        End Set
    End Property
    
    Public Property UseInPopUp() As Boolean
        Get
            Return _useInPopUp
        End Get
        Set(ByVal value As Boolean)
            _useInPopUp = value
            Grid.UseInPopUp = value
        End Set
    End Property
    
    Public Property UseInRelation() As Boolean
        Get
            Return _useInRelation
        End Get
        Set(ByVal value As Boolean)
            _useInRelation = value
            'Grid._useInRelation = value
        End Set
    End Property
    
    Public Property OnlyAssociatedContents() As Boolean
        Get
            Return _onlyAssociatedContents
        End Get
        Set(ByVal value As Boolean)
            _onlyAssociatedContents = value
        End Set
    End Property
    
    Public ReadOnly Property DropDownContentType() As DropDownList
        Get
            Return dwlContentType
        End Get
    End Property
    
    Public Property ExistWorkFlow() As Boolean
        Get
            Return ViewState("existWF")
        End Get
        Set(ByVal value As Boolean)
            ViewState("existWF") = value
        End Set
    End Property
    
    Protected Sub Grid_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent SelectedEvent(sender, e)
    End Sub
    
    Protected Sub Grid_UpdateEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent UpdateEvent(sender, e)
    End Sub
    
    Protected Sub dwlDomain_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSiteArea()
    End Sub
    
    Protected Sub ddContentTypeList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim onlyTopItems As Boolean = False
        Try
            onlyTopItems = ViewState("OnlyTopItems")
        Catch ex As Exception
        End Try
        doSearch(onlyTopItems)
    End Sub
    
    Public Function GridContentRowsSelected() As ContentCollection
        Return Grid.GridContentRowsSelected()
    End Function
    
    'controlla se esiste un content approval workflow 
    'associato ad un tema
    Sub CheckWorkFlow()
        Dim themeManager As New ThemeManager
        Dim themeId As Integer = 0
        Dim wfId As Integer = 0
        ExistWorkFlow() = False
        
        If (Request("T") <> Nothing) Then
            themeId = Request("T")
            
            Dim themeValue As ThemeValue = themeManager.Read(New ThemeIdentificator(themeId))
            If Not (themeValue Is Nothing) Then
                wfId = themeValue.KeyWorkFlow.Id
            End If
        End If
                
        If (wfId <> 0) Then
            ExistWorkFlow() = True
        End If
        
    End Sub
    
    Sub linkPager_PageIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        doSearch(False, False, sender.CommandArgument)
    End Sub
        
    Public Sub LoadPager(ByRef contentColl As ContentCollection, Optional ByVal Page As Integer = 1)
        'txtActPage2.Value = Page
        'Response.Write(Not userColl Is Nothing)
        'rpPager.DataSource = Nothing
        Const NPageVis As Integer = 6
        Const PageSize As Integer = 10

        If Not contentColl Is Nothing AndAlso contentColl.Count > 0 Then
            Dim pageList As New ArrayList
            Dim i As Integer = 0

            Dim ite As New ListItem
            myActualpage.Value = Page

            ite.Text = "&laquo;"
            ite.Value = "1"
            pageList.Add(ite)
            
            'Response.Write("page" & Page & "</br>")
            'Response.Write("count:" & pageList.Count & "</br>")
            For i = 0 To contentColl.PagerTotalNumber - 1
                If (i + 1 > (Page - NPageVis)) AndAlso (i + 1 < (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = i + 1
                    ite.Value = i + 1
                    pageList.Add(ite)

                    'Response.Write("add" & ite.Value)
                ElseIf (i + 1 = (Page - NPageVis)) Or (i + 1 = (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = "..."
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("addelse" & ite.Value)
                End If
            Next
            
            ite = New ListItem
            ite.Text = "&raquo;"
            ite.Value = contentColl.PagerTotalNumber
            pageList.Add(ite)

            'Response.Write("pagetotalnum" & contentColl.PagerTotalNumber & "</br>")
            If i > 1 Then
                rpPager.DataSource = pageList
                rpPager.DataBind()
                'Response.Write("popolo" & pageList.Count)
            Else
                rpPager.DataSource = Nothing
                rpPager.DataBind()
                'Response.Write("popolo Nothing")
            End If

            Dim nDa As Integer = ((Page * PageSize) - (PageSize)) + 1
            Dim nA As Integer = (nDa + contentColl.Count) - 1
            Dim nTot As Integer = contentColl.PagerTotalCount

            msg.Text = nDa.ToString & " to " & _
            (nA).ToString & " on " & _
            (nTot).ToString
        Else
            
            rpPager.DataSource = Nothing
            rpPager.DataBind()
        End If
      
    End Sub
    
    Protected Sub GridContents_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'Response.Write("2")
        'RaiseEvent SortingEvent(sender, e)
       
        ' GridContents.PageIndex = 0
        Select Case e.SortExpression
            Case "Key.Id"
                If SortType <> ContentGenericComparer.SortType.ById Then
                    SortType = ContentGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "DatePublish"
                If SortType <> ContentGenericComparer.SortType.ByData Then
                    SortType = ContentGenericComparer.SortType.ByData
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Title"
                If SortType <> ContentGenericComparer.SortType.ByTitle Then
                    SortType = ContentGenericComparer.SortType.ByTitle
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
                
        doSearch(False, False, CType(myActualpage.Value, Integer))
        'Dim webRequest As New WebRequestValue
        'Dim mPage As New MasterPageManager()
        'Dim objQs As New ObjQueryString

        'webRequest.customParam.append("page", Request("page"))
        'webRequest.customParam.append(objQs)
        'Response.Redirect(mPage.FormatRequest(webRequest))

        'BindGrid(CInt(Request("page")))
    End Sub
</script>

<script type="text/javascript">
    chk_status=false;
    dwl_status=false;
    
    function setChk(){
        chk_status=true
    }
    
    
    function setDwl(){
        dwl_status=true
    }
    
    function checkSearchParameters() {
        if (chk_status) return true;
        if (dwl_status) return true;
         
        var txtIdContent = document.getElementById('<%=txtIdContent.clientId() %>');
        var txtGlobalKey = document.getElementById('<%=txtGlobalKey.clientId() %>');
        var txtPrimaryKey = document.getElementById('<%=txtPrimaryKey.clientId() %>');
        var txtDatePublishFrom = document.getElementById('<%=txtDatePublishFrom.clientId() %>' + '_txtCalendar');
        var txtDatePublishTo = document.getElementById('<%=txtDatePublishTo.clientId() %>' + '_txtCalendar');
        var txtTitle = document.getElementById('<%=txtTitle.clientId() %>');
        var txtLSelector = document.getElementById('<%=lSelector.ClientID() %>' + '_dwlLanguage');    
        if ((txtIdContent.value == null || txtIdContent.value == '') &&
           (txtGlobalKey.value == null || txtGlobalKey.value == '') &&
           (txtPrimaryKey.value == null || txtPrimaryKey.value == '') &&
           (txtDatePublishFrom.value == null || txtDatePublishFrom.value == '') &&
           (txtDatePublishTo.value == null || txtDatePublishTo.value == '') &&
           (txtLSelector.value == '-1') &&
           (txtTitle.value == null || txtTitle.value == '')) { 
            alert('No search parameters!');
            return false;
        } else {
            return true;
        }
    }
    
    function checkImportParameters() {
        var txtImportSite = document.getElementById('<%=importSite.clientId() %>');
        var txtImportSiteArea = document.getElementById('<%=importSiteArea.clientId() %>');
        var txtImportLang = document.getElementById('<%=importLang.ClientID() %>' + '_dwlLanguage');
        var txtImportMarket = document.getElementById('<%=importMarket.clientId() %>');
        var txtImportContentType = document.getElementById('<%=importContentType.clientId() %>');
        var txtUploadXml = document.getElementById('<%=UploadXml.clientId() %>');
        if ((txtImportSite.value == null || txtImportSite.value == '') ||
           (txtImportSiteArea.value == null || txtImportSiteArea.value == '') ||
           (txtImportLang.value == '-1') ||
           (txtImportContentType.value == null || txtImportContentType.value == '') ||
           (txtUploadXml.value == null || txtUploadXml.value == '') ||
           (txtImportMarket.value == null || txtImportMarket.value == '')) { 
            alert('All fields are compulsory!');
            return false;
        } else {
            return true;
        }
    }
</script>

<asp:Panel ID="PanelSearchParameters"  cssclass="boxSearcher" runat="server">
<asp:HiddenField ID="hiddenSiteArea" runat="server" />
    <table class="topContentSearcher">
        <tr>
            <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
            <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
        </tr>
    </table>
    <fieldset class="_Search">
        <table class="form" width="100%">
            <tr>
                <td style="width:40px">Id</td>
                <td><HP3:ctlText ID="txtIdContent" runat="server" TypeControl="NumericText" Style="width:50px" /></td>
                <td style="width:40px">GlobalKey</td>
                <td><HP3:ctlText ID="txtGlobalKey" runat="server" TypeControl="NumericText" Style="width:50px" /></td>
                <td style="width:40px">PrimaryKey</td>
                <td><HP3:ctlText ID="txtPrimaryKey" runat="server" TypeControl="NumericText" Style="width:50px" /></td>
                <td style="width:400px"></td>
            </tr>
          </table>
          <table class="form" width="100%">
            <tr>
                <td style="width:40px">Title</td>
                <td><HP3:ctlText ID="txtTitle" runat="server" typecontrol="TextBox" Style="width:400px" /></td>
                <td style="width:50px">Display</td>
                <td style="width:40px"><asp:CheckBox ID="chkDisplay" runat="server"/></td>
                <td style="width:50px">Deleted</td>
                <td style="width:10px"><asp:CheckBox ID="chkDeleted" runat="server"/></td>
                <td style="width:200px"></td>
            </tr>
         </table>
         
         <asp:Panel id="pnlPopUp" visible="false" runat="server">
             <table class="form" width="100%">
                <tr>
                    <td style="width:40px">Domain</td>
                    <td style="width:40px"><asp:DropDownList id="dwlDomain" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dwlDomain_IndexChanged"/></td>
                    <td style="width:40px">SiteArea</td>
                    <td style="width:40px"><asp:DropDownList ID="dwlSiteArea" runat="server" /></td>
                    <td style="width:40px">ContentType</td>
                    <td><asp:DropDownList ID="dwlContentType" runat="server" /></td>
                </tr>
             </table>
         </asp:Panel>
         
         <table class="form" width="100%">
            <tr>
                <td style="width:95px">Date Publish</td>
                <td style="width:20px"> From</td>
                <td style="width:185px"><HP3:ctlDate ID="txtDatePublishFrom" runat="server" EnableViewState="false" ShowDate="false" TypeControl="JsCalendar" /></td>
                <td style="width:20px"> To</td>
                <td style="width:185px"><HP3:ctlDate ID="txtDatePublishTo" runat="server" EnableViewState="false" ShowDate="false" TypeControl="JsCalendar"/></td>     
                <td style="width:50px"> Language </td>
                <td><HP3:Language ID="lSelector" runat="server"  Typecontrol="WithSelectControl" /></td>
            </tr>
        </table>
        <table class="form" width="100%">
            <tr>
                <td>
                    <asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click"  CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
                    <%--<asp:Button ID="btnShowAll" Text="Show all Contents" runat="server" OnClick="btnShowAllContents_Click"  CssClass="button" style="margin-top:10px;padding-left:16px;width:150px;background-image:url('/HP3Office/HP3Image/Button/search_allcontent.gif');background-repeat:no-repeat;background-position:1px 1px;" />--%>
                    <asp:Button ID="btnShowAllSiteArea" Text=" Remove SiteArea Filter" runat="server" OnClick="btnShowAllContentsSiteArea_Click"  CssClass="button" style="margin-top:10px;padding-left:16px;width:180px;background-image:url('/HP3Office/HP3Image/Button/remove_siteareafilter.gif');background-repeat:no-repeat;background-position:1px 1px;" />
                    <asp:Button ID="btnExport" Text="Export" runat="server" OnClick="btnExport_Click"  CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/import_export.png');background-repeat:no-repeat;background-position:1px 1px;" />
                    <asp:Button ID="btnImport" Text="Import" runat="server" OnClick="btnImport_Click"  CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/import_export.png');background-repeat:no-repeat;background-position:1px 1px;" />
                </td>
            </tr>
         </table>
    </fieldset>
    <hr />
</asp:Panel>

<asp:Panel ID="pnlImport" visible="false" runat="server">
<hr />
    <table class="topContentSearcher">
        <tr>
            <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_import_export.png');background-repeat:no-repeat;"></td>
            <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Import</span></td>
        </tr>
    </table>
    <div style="background-color:#F3F3F3;width:100%;">
        <table class="form" border="0" style="background-color:#F3F3F3;">
            <tr><td>Site:</td><td><HP3:ctlText ID="importSite" runat="server" TypeControl="NumericText" Style="width:25px" /></td><td>&nbsp;SiteArea:</td><td><HP3:ctlText ID="importSiteArea" runat="server" TypeControl="NumericText" Style="width:25px" /></td><td>&nbsp;ContentType:</td><td><HP3:ctlText ID="importContentType" runat="server" TypeControl="NumericText" Style="width:25px" /></td><td>&nbsp;Language:</td><td><HP3:Language ID="importLang" runat="server" Typecontrol="WithSelectControl" /></td><td>&nbsp;Market:</td><td><HP3:ctlText ID="importMarket" runat="server" TypeControl="NumericText" Style="width:25px" /></td></tr>
        </table>
        <table class="form" border="0" style="background-color:#F3F3F3;">
            <tr><td>Theme Content Default (<%=newctId %>):</td><td><asp:CheckBox ID="isContentDefault" runat="server" Checked="false" /></td><td>&nbsp;Save Report:</td><td><asp:CheckBox ID="isReport" runat="server" Checked="false" /></td><td>&nbsp;Is Update:</td><td><asp:CheckBox ID="IsUpdate" runat="server" Checked="false" /></td><td>&nbsp;Is New Lang:</td><td><asp:CheckBox ID="IsNewLang" runat="server" Checked="false" /><td>&nbsp;Ref.Lang.:</td><td><HP3:Language ID="refLang" runat="server" Typecontrol="WithSelectControl" /></td><td>PageTitle:</td><td><asp:CheckBox ID="isPageTitle" runat="server" Checked="false" /></td></tr>
        </table>
        <table class="form" border="0" style="background-color:#F3F3F3;">
            <tr><td><asp:FileUpload ID="UploadXml" style="width:425px;" runat="server" />&nbsp;<asp:Button cssclass="button" ID="btnUpload" runat="server" onClick="ImportContent" Text ="Start Import" /></td></tr>
        </table>
    </div>
<hr />
</asp:Panel>

<asp:HiddenField  ID="myActualpage"  runat="server" Value="1"/>
<asp:Label runat="server" ID="msg"/>
    <div style="float:right; text-align:right;">
         <asp:Repeater ID="rpPager" runat="server" Visible="true">
            <ItemTemplate>
            
            <%#IIf(myActualpage.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>
                            <asp:LinkButton ID="linkPager" runat="server"  onclick="linkPager_PageIndex"
                            Visible='<%#iif (myActualpage.Value =DataBinder.Eval(Container.DataItem, "value"),"false","true") %>' 
                            CommandArgument='<%#DataBinder.Eval(Container.DataItem, "value")%>'> <%#DataBinder.Eval(Container.DataItem, "text")%></asp:LinkButton>
            </ItemTemplate>
         </asp:Repeater>
    </div>

<HP3:GridContent ID="Grid" AllowEdit="true" OnSortingEvent="GridContents_Sorting" AllowSelect="false" visible="false" runat="server" OnSelectedEvent="Grid_SelectedEvent" OnUpdateEvent="Grid_UpdateEvent" />
<%--
    </ContentTemplate>
</asp:UpdatePanel>--%>
<asp:Panel ID="pnlNoResultsFound" Visible="false" runat="server">
    <h5>No results found</h5><br />
</asp:Panel>