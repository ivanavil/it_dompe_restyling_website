<%@ Control Language="VB" ClassName="ListLanguages" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<script runat="server">
     
    Public mPageManager As New MasterPageManager()
    Public accService As New AccessService()
    Public webRequest As WebRequestValue = New WebRequestValue()       
   
    Sub Page_load()
        If Not Page.IsPostBack Then
            Dim langManager As New LanguageManager
            Dim langSearcher As New LanguageSearcher
            CType(Me.Page, Object).CurrentLang = accService.GetTicket.Travel.KeyLang
            
            langSearcher.Active = True
            rpLang.DataSource = langManager.Read(langSearcher)
            rpLang.DataBind()
        End If
    End Sub
    
    Sub check_lang(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType.ToString = "Item" Or e.Item.ItemType.ToString = "AlternatingItem" Then
            If e.Item.DataItem.Key.Id = CType(Me.Page, Object).CurrentLang.Id Then
                Dim btnObj As LinkButton = e.Item.FindControl("btnLang")
                btnObj.Text = "[" + btnObj.Text + "]"
                btnObj.Enabled = False
            End If
        End If        
    End Sub
    
    'evento onClick per impostare la lingua scelta
    Sub btnLangTicket_onClick(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim travelValue As New TravelTicketValue
        travelValue.KeyLang.Id = e.CommandArgument.ToString.Split(";")(1)
        travelValue.KeyLang.Code = e.CommandArgument.ToString.Split(";")(0)
        
        accService.StoreTravelTicket(travelValue)
        webRequest = New WebRequestValue()
        webRequest.Keycontent.Id = mPageManager.getContent.Id
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
</script>

<asp:Repeater ID="rpLang" runat="server" OnItemDataBound="check_lang">
    <ItemTemplate>
        <asp:LinkButton ID="btnLang" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Key.Code") & ";" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'
            OnCommand="btnLangTicket_onClick" Text='<%#DataBinder.Eval(Container.DataItem, "Description")%>'>
        </asp:LinkButton>
    </ItemTemplate>
    <SeparatorTemplate>
        -
    </SeparatorTemplate>
</asp:Repeater>
