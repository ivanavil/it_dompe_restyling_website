<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private mPageManager As New MasterPageManager
    Private contentManager As New ContentManager
    Private siteAreaManager As New SiteAreaManager
    
    Private contentTypeSearcher As New ContentTypeSearcher
    
    Private webRequest As New WebRequestValue
    Private utility As New WebUtility
    
    Private _sortOrder As SiteAreaGenericComparer.SortOrder = SiteAreaGenericComparer.SortOrder.ASC
    Private _sortType As SiteAreaGenericComparer.SortType = SiteAreaGenericComparer.SortType.ByName
    Private strJS As String
        
    'Dim _strContentIdToIndex As String = ""
    Dim _strContentIdToIndex As Boolean = False
    
    Private _strDomain As String
    Private _language As String
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not Page.IsPostBack Then
            'BindContentType(False)
            BindSite()
            'ctlArea.LoadControl()
        End If
    End Sub
   
    'Sub GoSpider(ByVal sender As Object, ByVal ev As EventArgs)
    'Dim index As IndexerModule = IndexerModule.GetInstance
    'Dim oSiteAreaValue As New SiteAreaValue
    'Dim contentSearcher As New ContentSearcher
    'Dim contentCollection As New ContentCollection
              
    ''oSiteAreaValue = GetSiteSelection(ctlSite)
    'If dwlSite.SelectedItem.Value <> -1 Then
    '    contentSearcher.KeySite.Id = dwlSite.SelectedItem.Value
            
    '    oSiteAreaValue = GetSiteSelection(dwlArea)
            
    '    If Not oSiteAreaValue Is Nothing Then
    '        contentSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
    '    End If
            
    '    If (dwlContentType.SelectedItem.Value <> -1) Then
    '        contentSearcher.KeyContentType.Id = dwlContentType.SelectedItem.Value
    '    End If
                       
    'index.ActSpider(contentSearcher)
            
    'modifiche del 6/04/09 utilizziamo lo Spider per indicizzare invece che 
    'addContent
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'contentCollection = Me.BusinessContentManager.Read(contentSearcher)
    'If Not contentCollection Is Nothing AndAlso contentCollection.Count > 0 Then
    '    _strContentIdToIndex = ClassUtility.ContentCollectionPrimaryKeyToString(contentCollection)
                
    '    'For i As Integer = 0 To contentCollection.Count - 1
    '    '    Response.Write("<br>" & contentCollection(i).Key.Id & " " & i & "/" & (contentCollection.Count - 1))
    '    '    index.AddContent(contentCollection(i).Key)
    '    'Next
    'End If
    'CreateJsMessage("Successful operation!")
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Else
    'CreateJsMessage("Select a Site, please.")
    'End If
    'End Sub
    
    'gestisce il refresh di index
    Sub RefreshIndex(ByVal sender As Object, ByVal ev As EventArgs)
        'Dim index As IndexerModule = IndexerModule.GetInstance
        Dim index As New IndexerModule()
        index.LoadIndex()
        
        CreateJsMessage("Index updated successfully!")
    End Sub
    
    'Function GetTotalWords() As String
    '    Dim index As IndexerModule = IndexerModule.GetInstance
    '    Try
    '        Return index.GetIndex.Count.ToString
    '    Catch ex As Exception
    '        Return "0"
    '    End Try
    'End Function
    
    'annulla tutta la indicizzazione
    Sub ResetIndex(ByVal sender As Object, ByVal ev As EventArgs)
        'Dim index As IndexerModule = IndexerModule.GetInstance
        Dim index As New IndexerModule()
        Dim ret As Boolean = index.ResetIndex()
        
        If (ret) Then
            CreateJsMessage("Index reset successfully!")
        Else
            CreateJsMessage("Operation failed!")
        End If
    End Sub
    
    'restituisce il valore selezionato nel controllo
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    'popola la combo dei siti
    Sub BindSite()
        Dim oSiteManager As New SiteAreaManager
        oSiteManager.Cache = False
        Dim oSiteSearcher As New SiteAreaSearcher
               
        oSiteSearcher.Status = 1
        oSiteSearcher.IdType = SiteAreaCollection.SiteAreaType.Site
        Dim _sCollection As SiteAreaCollection = Me.BusinessSiteAreaManager.Read(oSiteSearcher)
    
        If Not (_sCollection Is Nothing) Then _sCollection.Sort(_sortType, _sortOrder)
        utility.LoadListControl(dwlSite, _sCollection, "Name", "Key.Id")
        dwlSite.Items.Insert(0, New ListItem("---Select Site---", -1))
    End Sub
    
    'popola la dwl per i contentType
    Sub BindContentType(ByVal filterBySite As Boolean)
        contentTypeSearcher.OnlyAssociatedContents = True
        Dim contentTypeColl As ContentTypeCollection = Me.BusinessContentTypeManager.Read(contentTypeSearcher)
        
        If (filterBySite = True) Then contentTypeSearcher.KeySite.Id = dwlSite.SelectedItem.Value
               
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwlContentType, contentTypeColl, "Description", "Key.Id")

        dwlContentType.Items.Insert(0, New ListItem("---Select ContentType---", -1))
    End Sub
    
    Sub BindLanguageSite()
        Dim oLanguageColl As LanguageCollection = siteAreaManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(dwlSite.SelectedValue))
       
        dwlLanguage.Items.Clear()
        If Not (oLanguageColl Is Nothing) AndAlso (oLanguageColl.Count > 0) Then
            For Each LangValue As LanguageValue In oLanguageColl
                dwlLanguage.Items.Add(New ListItem(LangValue.Description, LangValue.Key.Id))
            Next
        End If
        
        dwlLanguage.Items.Insert(0, New ListItem("---Select Language---", -1))
    End Sub
    
    'controlla l'evento che si scatena quando si seleziona un Country
    Sub SelectSiteChanged(ByVal sender As Object, ByVal e As EventArgs)
        If (dwlSite.SelectedValue <> -1) Then
            _strContentIdToIndex = True
            BindLanguageSite()
            BindContentType(True)
            LoadLastIndex()
            btnResetSite.visible = True
            
            Dim site As SiteAreaValue = siteAreaManager.Read(New SiteAreaIdentificator(dwlSite.SelectedValue))
            If Not (site Is Nothing) Then
                dwlArea.SubDomainArea = site.Key.Domain
                dwlArea.LoadControl()
            End If
        Else
            dwlLanguage.Items.Clear()
            dwlContentType.Items.Clear()
            dwlArea.Clear()
        End If
    End Sub
    
    Sub LoadLastIndex()
        Dim searchIn As New SearchEngine
        Dim logColl As New Healthware.HP3.Core.Search.ObjectValues.SpiderLogCollection
        Dim vo As New Healthware.HP3.Core.Search.ObjectValues.SpiderLogValue
        
        If (dwlSite.SelectedValue <> -1) Then
            vo.KeySite.Id = dwlSite.SelectedValue
            searchIn.Cache = False
            Dim log As Healthware.HP3.Core.Search.ObjectValues.SpiderLogValue = searchIn.ReadLastLog(vo)
            
            If Not (log Is Nothing) Then
                logColl.Add(log)
                grw_LastIndex.DataSource = logColl
                grw_LastIndex.DataBind()
                pnlLstIndex.Visible = True
            Else
                Dim logN As New Healthware.HP3.Core.Search.ObjectValues.SpiderLogValue
                logN.Note = "No Log"
                logColl.Add(log)
                grw_LastIndex.DataSource = logColl
                grw_LastIndex.DataBind()
                pnlLstIndex.Visible = True
            End If
        End If
    End Sub
    
    Sub ResetIndexBySite(ByVal sender As Object, ByVal e As EventArgs)
        Dim pos As New Healthware.HP3.Core.Search.ObjectValues.PositionValue
        Dim res As Boolean = False
        
        If (dwlSite.SelectedValue <> -1) Then
            pos.Site.Id = dwlSite.SelectedValue
            If (dwlLanguage.SelectedValue <> -1) Then pos.Language.Id = dwlLanguage.SelectedValue
            'Dim index As IndexerModule = IndexerModule.GetInstance(pos)
            Dim index As New IndexerModule()
            res = index.ResetIndex(pos)
        End If
         
        If (res) Then
            CreateJsMessage("Index reset successfully!")
        Else
            CreateJsMessage("Operation failed!")
        End If
    End Sub
    
    Sub LoadLastIndex(ByVal sender As Object, ByVal e As EventArgs)
        LoadLastIndex()
    End Sub
        
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """);"
    End Sub
</script>

<script type="text/javascript" language="javascript" src="/Js/jquery.pack.js"></script>

<hr/>
<asp:Panel id="pnl_index" runat="server">
  <asp:Button id ="btnRefresh" runat="server" Text="Refresh Index"  OnClick="RefreshIndex" CssClass="button"  style="margin-bottom:10px;"/>
  <asp:Button id ="btnReset" runat="server" Text="Reset Index"  OnClick="ResetIndex" CssClass="button"  style="margin-bottom:10px;"/>
  
    <%--Spider--%>
    <asp:Panel id="pnlSpider" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;width:50px;" valign="middle"><span class="title">Spider<%--(<%=GetTotalWords()%>)--%></span></td>
                <td style="height:24px;border:0px solid #ffffff;"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIndex&Help=cms_HelpIndex&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>Site</strong></td>  
                    <td><strong>Language</strong></td>  
                    <td><strong>Area</strong></td>
                    <td><strong>ContentType</strong></td>
                </tr>
                <tr>
                    <td><asp:DropDownList id="dwlSite" runat="server"  autopostback="true" OnSelectedIndexChanged="SelectSiteChanged"/></td>
                    <td><asp:DropDownList id="dwlLanguage" runat="server"/></td>
                    <td><HP3:ctlSite runat="server" ID="dwlArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
                    <td><asp:DropDownList ID="dwlContentType" runat="server" /></td>  
                </tr>
                <tr>
                    <td>
                        <input id="btnGo" type="button" value="Go" onclick="javascript:success();" class="button"  />
                        <asp:Button id="btnResetSite"   Visible="false" runat="server" text="Reset Site Index" onclick="ResetIndexBySite"  CssClass="button"  />
                   </td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
 </asp:Panel>
 
 <div id="divload"  style="display:none;background:#F5F5F5;height:100px;width:600px;position:absolute;top:220px;left:440px;z-index:2;" >
    <img id="imgLoad" src="/hp3Office/HP3Image/Ico/ajax-loader.gif" style="display:none; margin-left:250px; margin-top:50px" alt="" title="Loading"/><br />
    <label id="msgLoad" style="margin-left:250px; margin-top:50px"></label>
</div> 
<label  id="msgfinal"></label>
             
<asp:Panel id="pnlLstIndex" runat="server" Visible="false">
<hr />
<asp:Label runat="server"  Text="Last Index" CssClass="title"/><br />

<asp:UpdatePanel ID="upPanel" runat="server">
    <ContentTemplate>
    <asp:button runat="server"  CssClass="button" ID="btnNotV"  OnClick="LoadLastIndex" style="margin-top:10px;display:none"/>
    <asp:GridView id="grw_LastIndex"
          runat="server"
          AutoGenerateColumns="false" 
          GridLines="None" 
          CssClass="tbl1"
          HeaderStyle-CssClass="header"
          AlternatingRowStyle-BackColor="#E9EEF4"
          AllowPaging="true"
          PagerSettings-Position="TopAndBottom"  
          PagerStyle-HorizontalAlign="Right"
          PagerStyle-CssClass="Pager"             
          AllowSorting="true"
          emptydatatext="No item available">
             <Columns>
                <%--<asp:BoundField  ItemStyle-Width="30%"  HeaderText="StartDate"  datafield="StartDate"/> 
                <asp:BoundField  ItemStyle-Width="30%"  HeaderText="EndDate"  datafield="EndDate"/> 
                <asp:BoundField   ItemStyle-Width="60%" HeaderText="Note"  datafield="Note"/> --%>
                 <asp:TemplateField HeaderStyle-Width="30%" HeaderText="StartDate">
                       <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "StartDate")%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderStyle-Width="30%" HeaderText="EndDate">
                       <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "EndDate")%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderStyle-Width="60%" HeaderText="Note">
                       <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Note")%></ItemTemplate>
                </asp:TemplateField>  
               <%-- <asp:TemplateField >        
                    <ItemTemplate>            
                    </ItemTemplate>
                </asp:TemplateField>--%>
              </Columns>
      </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>             


<div class="counterLabel"></div>
<%--<div class="errorLabel">Lista Errori: </div>--%>

<script type ="text/javascript" >
    <%=strJS%>
    var counter = 0;
    var strTest = '<%=_strContentIdToIndex%>';
    
    var site = document.getElementById('<%=dwlSite.ClientId%>');
    var lang = document.getElementById('<%=dwlLanguage.ClientId%>');
    var area = $("#<%=dwlArea.ClientId%>") //document.getElementById('<%=dwlArea.ClientId%>');
    var ctype = document.getElementById('<%=dwlContentType.ClientId%>');
    //var area = $("#<%=dwlArea.ClientId%>")  
    //alert (area);
    //alert (area.selectedindex);
    //alert(site.value)
    //alert(lang.value)
    //alert(ctype.value)
    //alert($("#<%=dwlArea.ClientId%>").val())
    //alert($('#<%=dwlArea.clientId%>').val();)
   
   $(document).ready(function(){ 
     
    });  
    
   function success() {
        var site = document.getElementById('<%=dwlSite.ClientId%>');
        var lang = document.getElementById('<%=dwlLanguage.ClientId%>');
        var area = $('#<%=dwlArea.clientId%>_drpSite');
        var ctype = document.getElementById('<%=dwlContentType.ClientId%>');
        
       if(site.value == -1){
            alert('Select a Site!')
            return;
        }
        
        $('div#divload').show(); 
        $('img#imgLoad').show();
        $('label#msgLoad').html("");
        $('label#msgfinal').html("");
        
        document.getElementById('btnGo').disabled=true;
//        document.getElementById('<%=btnNotV.ClientId%>').disabled=true;
        document.getElementById('<%=btnRefresh.ClientId%>').disabled=true;
        document.getElementById('<%=btnReset.ClientId%>').disabled=true;
        document.getElementById('<%=btnResetSite.ClientId%>').disabled=true;
        
        $('label#msgLoad').append("In progress....")
      
        $.ajax({ 
           type: "POST", 
            url: "/HP3Office/HP3Service/async.aspx", 
            data: "asyncType=enginespider&siteId=" + site.value + "&LanguageId=" + lang.value  + "&conTypeId=" + ctype.value + "&siteAreaId=" + area.val(),
            success: function(msg) {
              if (msg == "ko") {
                    $('img#imgLoad').hide();
                    $('div#divload').hide();
                    document.getElementById('btnGo').disabled=false;
//                    document.getElementById('<%=btnNotV.ClientId%>').disabled=false;
                    document.getElementById('<%=btnRefresh.ClientId%>').disabled=false;
                    document.getElementById('<%=btnReset.ClientId%>').disabled=false;
                    document.getElementById('<%=btnResetSite.ClientId%>').disabled=false;
                    
                     document.getElementById('<%=btnNotV.clientID%>').click();
                    $('label#msgfinal').html("Error during the indexation!")
                   
              };
                
              if (msg == "ok") {
                    $('img#imgLoad').hide();
                    $('div#divload').hide();
                     document.getElementById('btnGo').disabled=false;
//                     document.getElementById('<%=btnNotV.ClientId%>').disabled=false;
                     document.getElementById('<%=btnRefresh.ClientId%>').disabled=false;
                     document.getElementById('<%=btnReset.ClientId%>').disabled=false;
                     document.getElementById('<%=btnResetSite.ClientId%>').disabled=false;
                     
                     document.getElementById('<%=btnNotV.clientID%>').click();
                    $('label#msgfinal').html("Indexation successfully!")
                    
                };
                
            },
            error: function(msg) {
               $('img#imgLoad').hide();
               $('div#divload').hide();
                document.getElementById('btnGo').disabled=false;
//                document.getElementById('<%=btnNotV.ClientId%>').disabled=false;
                document.getElementById('<%=btnRefresh.ClientId%>').disabled=false;
                document.getElementById('<%=btnReset.ClientId%>').disabled=false;
                document.getElementById('<%=btnResetSite.ClientId%>').disabled=false;
                
                document.getElementById('<%=btnNotV.clientID%>').click();
               $('label#msgfinal').html("Error during the indexation!")
               
           }
        });  
    }; 
</script>
 