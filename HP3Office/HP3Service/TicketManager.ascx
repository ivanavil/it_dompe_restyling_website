<%@ Control Language="VB" ClassName="ctlDictionaryManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.IssueTracker"%>
<%@ Import Namespace="Healthware.HP3.CRM.IssueTracker.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Upload" Src ="~/hp3Office/HP3Parts/ctlDownLoadList.ascx" %>
<script runat="server">
    Private oLManager As New LanguageManager
    
    Private oCManager As DictionaryManager
    Private oSearcher As DictionarySearcher
    Private oCollection As DictionaryCollection
    
    Private oIssueManager As New IssueTrackerManager
    Private oIssueSearcher As New IssueTrackerSearcher
    Private oTicketColl As New IssueTrackerTicketCollection
    Private oTicketDetailsColl As New IssueTrackerTicketDetailsCollection
    
    
    Private mPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    Private objCMSUtility As New GenericUtility
    Private utility As New WebUtility
    
    Private _sortType As DictionaryGenericComparer.SortType
    Private _sortOrder As DictionaryGenericComparer.SortOrder
    Private strDomain As String
    
    Private strJS As String
    
    Private _typecontrol As ControlType = ControlType.View
    
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Copy
        Selection
        View
        ViewDetails
        EditDetails
    End Enum
    Public Property SelectedTicket() As String
        Get
            Return ViewState("selectedTicket")
        End Get
        Set(ByVal value As String)
            ViewState("selectedTicket") = value
        End Set
    End Property
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Function LoadValue() As IssueTrackerTicketValue
        If SelectedId <> 0 Then
            oIssueManager = New IssueTrackerManager
            oIssueManager.Cache = False
            Dim oIssueIdentificator As New IssueTrackerIdentificator
            oIssueIdentificator.KeyTicket = SelectedId
            
            Return oIssueManager.Read(oIssueIdentificator)
        End If
        Return Nothing
    End Function
    
    Function LoadValueDetails() As IssueTrackerTicketDetailsValue
        If SelectedId <> 0 Then
            oIssueManager = New IssueTrackerManager
            oIssueManager.Cache = False
            Dim oIssueIdentificator As New IssueTrackerIdentificator
            oIssueIdentificator.KeyTicketDetail = SelectedId
            
            Return oIssueManager.ReadDetails(oIssueIdentificator)
        End If
        Return Nothing
    End Function

    Function ItemRemove() As Boolean
        'rimuove il ticket con i suoi dettagli
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            Try
                oIssueManager = New IssueTrackerManager
                Dim key As New IssueTrackerIdentificator
                key.KeyTicket = SelectedId
                ret = oIssueManager.Delete(key)
                Dim _collDetails As New IssueTrackerTicketDetailsCollection
                Dim _searchIss As New IssueTrackerSearcher
                _searchIss.KeyTicket.KeyTicket = SelectedId
                _collDetails = oIssueManager.ReadDetails(_searchIss)
                For Each elem As IssueTrackerTicketDetailsValue In _collDetails
                    oIssueManager.DeleteDetails(elem.Key)
                Next
            Catch ex As Exception
                ret = False
            End Try
        End If
        Return ret
    End Function

    Function ItemDetailsRemove() As Boolean
        'rimuove un subitem
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            Try
                oIssueManager = New IssueTrackerManager
                Dim key As New IssueTrackerIdentificator
                key.KeyTicketDetail = SelectedId
                ret = oIssueManager.DeleteDetails(key)
            Catch ex As Exception
                ret = False
            End Try
        End If
        Return ret
    End Function
    
    Sub LoadFormDettDetails(ByVal oValue As IssueTrackerTicketDetailsValue)
        'carica il form del dettaglio ticket
        If Not oValue Is Nothing Then
            With oValue
                SelectedTicket = .Key.NTicket & "_" & .Key.KeyTicketDetail
                lblIdDetails.InnerText = "N�Ticket: " & .Key.NTicket & " - Id Details: " & .Key.KeyTicketDetail
                txtSubjectDet.Text = .Subject
                txtDetailsDet.Text = .Detail
                txtLinkDet.Text = .Link
                txtNoteDet.Text = .Note
                chkTestedDet.Checked = .Tested
                txtEnvironmentDet.Text = .Environment
                lblUser.Text = SetStrValue(.KeyUser.Id, "Owner.Id")
                
                '************* recuper i file allegati xxxx
                For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
                    If InStr(foundFile, SelectedTicket) > 0 Then
                        foundFile = foundFile
                        fileUpExDet.Visible = False
                        btnDeleteFileDet.Visible = True
                        ImgBtnFileDet.Visible = True
                        lblFileUploadDet.Text = foundFile
                        Exit For
                    Else
                        fileUpExDet.Visible = True
                        btnDeleteFileDet.Visible = False
                        ImgBtnFileDet.Visible = False
                    End If
                Next
                '************* fine recupero file
            End With
        Else
            lblDescrizioneDetails.InnerText = "New Ticket Details"
            lblIdDetails.InnerText = ""
            txtSubjectDet.Text = Nothing
            txtDetailsDet.Text = Nothing
            txtLinkDet.Text = Nothing
            txtNoteDet.Text = Nothing
            txtEnvironmentDet.Text = Nothing
            chkTestedDet.Checked = False
            lblUser.Text = Me.ObjectTicket.User.Surname & " " & Me.ObjectTicket.User.Name
        End If
    End Sub
    
    Sub LoadFormDett(ByVal oValue As IssueTrackerTicketValue)
        'carica il form del ticket
        LoadStatus(dwlActualStatus)
        LoadSubType(dwlTicketType)
        LoadProject(dwlProject)
        LoadPriority()
        
        If Not oValue Is Nothing Then
            With oValue
                If oValue.DateExpiry.HasValue Then
                    DeadLine.Value = DateTime.Parse(oValue.DateExpiry.ToString).ToShortDateString
                End If
                lblId.InnerText = "N�Ticket: " & .KeyTicket.NTicket
                dwlActualStatus.SelectedValue = .ActualStatus

                dwlProject.SelectedValue = .Project.Id
                'carico il progetto e i suoi subitems
                If .Project.Id > 0 Then
                    LoadUserRelation(.Project.Id)
                    LoadWebSite()
                    dwlWebSite.SelectedValue = .WebSite.Id
                End If
                If .WebSite.Id > 0 Then
                    LoadUserRelation(.WebSite.Id)
                    LoadSiteArea()
                    dwlSiteArea.SelectedValue = .Location.Id
                    If .Location.Id > 0 Then
                        LoadUserRelation(.Location.Id)
                    End If
                Else
                    dwlSiteArea.Items.Insert(0, New ListItem("--Select--", -1))
                End If
                
                lblFrom.Text = SetStrValue(.Owner.Id, "Owner.Id")
                dwlTicketType.SelectedValue = .IdContentSubType
                chkMRI.Checked = .RequestMri
                chkReqAgreeMRL.Checked = .AgreeMri
                chkMarket.Checked = .RequestMarket
                chkReqAgreeMarket.Checked = .AgreeMarket
                dwlPriority.SelectedValue = .IdPriority
                txtBuild.Text = .Build
                Dim detailsValue As New IssueTrackerTicketDetailsValue
                Dim detailsManager As New IssueTrackerManager
                .TicketDetails.Key = .KeyTicket
                detailsValue = detailsManager.ReadDetails(.TicketDetails.Key)

                If Not detailsValue Is Nothing Then 'se sono presenti i dettagli
                    txtSubject.Text = detailsValue.Subject
                    txtDetails.Text = detailsValue.Detail
                    txtlink.Text = detailsValue.Link
                    txtNote.Text = detailsValue.Note
                    SelectedTicket = .KeyTicket.NTicket & "_" & detailsValue.Key.KeyTicketDetail
                End If

                '************* recuper i file allegati
                For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
                    If InStr(foundFile, SelectedTicket) > 0 Then
                        foundFile = foundFile
                        fileUpEx.Visible = False
                        lblFileUpload.Visible = True
                        btnDeleteFile.Visible = True
                        lblFileUpload.Text = "<a title='View Document' target='blank' href='" & Right(foundFile, foundFile.LastIndexOf("\")) & "'> <img src='/HP3Office/HP3Image/ico/type_document_small.gif' alt='View Document'/></a>"
                        Exit For
                    End If
                Next
                '************* fine recupero file

            End With
        Else
            lblDescrizione.InnerText = "New Ticket"
            lblId.InnerText = ""
            dwlWebSite.Items.Insert(0, New ListItem("--Select--", -1))
            txtBuild.Text = Nothing
            dwlSiteArea.Items.Insert(0, New ListItem("--Select--", -1))
            txtSubject.Text = Nothing
            txtDetails.Text = Nothing
            txtlink.Text = Nothing
            txtNote.Text = Nothing
            chkMRI.Checked = False
            chkMarket.Checked = False
            chkReqAgreeMarket.Checked = False
            chkReqAgreeMRL.Checked = False
            lblFrom.Text = Me.ObjectTicket.User.Surname & " " & Me.ObjectTicket.User.Name
        End If
    End Sub

    Private Function CreateGUID()
        'genera guid di 4 caratteri
        Dim sGUID As String
        sGUID = System.Guid.NewGuid.ToString()
        sGUID = UCase(Left(sGUID, 4))
        Return sGUID
    End Function
    
    Private Function UploadMyFile(ByVal nomeFile As String, ByVal obj As FileUpload) As Boolean
        Dim ret As Boolean = False
        '**************** UOPLOAD
        If obj.HasFile Then
            Try
                Dim filepath As String = obj.PostedFile.FileName
                Dim pat As String = "\\(?:.+)\\(.+)\.(.+)"
                Dim r As Regex = New Regex(pat)
                'run
                Dim m As Match = r.Match(filepath)
                Dim file_ext As String = m.Groups(2).Captures(0).ToString()
                Dim filename As String = m.Groups(1).Captures(0).ToString()
                Dim file As String = nomeFile & "_" & filename & "." & file_ext
                'save the file to the server
                obj.PostedFile.SaveAs(Server.MapPath("~/HP3Download/") & file)
                
                ret = True
            Catch ex As Exception
                Response.Write(ex.ToString & "<br/>")
                ret = False
            End Try
        End If
        '**************** Fine Upload
        Return ret
    End Function
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        'salva i valori di ticket e subitem
        Dim oTickValue As New IssueTrackerTicketValue
        
        If SelectedId > 0 Then 'for update
            oTickValue.KeyTicket.KeyTicket = SelectedId
            oIssueManager.Cache = False
            oTickValue = oIssueManager.Read(oTickValue.KeyTicket)
        End If
        With (oTickValue)
            .Project.Id = dwlProject.SelectedValue
            .WebSite.Id = dwlWebSite.SelectedValue
            .Location.Id = dwlSiteArea.SelectedValue
            .Build = txtBuild.Text
            .RequestMri = chkMRI.Checked
            .RequestMarket = chkMarket.Checked
            .AgreeMri = chkReqAgreeMRL.Checked
            .AgreeMarket = chkReqAgreeMarket.Checked
            .ActualStatus = dwlActualStatus.SelectedValue 'status selected
            .KeyStatus.Id = dwlActualStatus.SelectedValue 'status selected
            .ContentType.Key.Id = 147 'impostare content type
            .IdContentSubType = dwlTicketType.SelectedValue 'impostare subtype bug, content...
            .Owner.Id = Me.ObjectTicket.User.Key.Id  'autenticated user

            'recupero il destinatario dalla label riempita in fase di compilazione in modo automatico
            If lblToId.Text <> "" AndAlso CInt(lblToId.Text) > 0 Then
                .Recipient.Id = CInt(lblToId.Text)
            Else
                .Recipient.Id = -1
            End If
            If dwlPriority.SelectedValue <> "" Then
                .IdPriority = dwlPriority.SelectedValue
            Else
                .IdPriority = 0
            End If
            oTickValue.DateExpiry = DeadLine.Value
        End With
       
        oIssueManager = New IssueTrackerManager
        If SelectedId <= 0 Then
            'it's a new ticket
            Dim oTicketDetailsValue As New IssueTrackerTicketDetailsValue
            Dim oIssueTrackerIdentif As New IssueTrackerIdentificator
            oTickValue.KeyTicket.NTicket = CreateGUID()
            oTickValue.DateCreate = DateTime.Now.ToShortDateString
            oTickValue.DateInsert = DateTime.Now.ToShortDateString
                     
            oTicketDetailsValue.Key = oIssueManager.Create(oTickValue)
            If oTicketDetailsValue.Key.KeyTicket > 0 Then
                oTicketDetailsValue.Link = txtlink.Text
                oTicketDetailsValue.Subject = txtSubject.Text
                oTicketDetailsValue.Note = txtNote.Text
                oTicketDetailsValue.Detail = txtDetails.Text
                'oTicketDetailsValue.Environment = ""
                oTicketDetailsValue.KeyUser.Id = Me.ObjectTicket.User.Key.Id  'autenticated user
                oTicketDetailsValue.Key.NTicket = oTickValue.KeyTicket.NTicket

                Dim strTicket As String = oTicketDetailsValue.Key.NTicket & oTicketDetailsValue.Key.KeyTicket
               
                'solo dopo aver creato il ticket � possibile definire il vero valore del numero ticket
                'perch� composto da una guid generica e il valore del record
                If oIssueManager.Update(oTicketDetailsValue.Key, "KeyTicket.NTicket", strTicket) Then
                    oTicketDetailsValue.Key.NTicket = strTicket
                End If
                oIssueTrackerIdentif = oIssueManager.CreateDetails(oTicketDetailsValue)
                'solo dopo aver creato il dettaglio � possibile allegare un eventuale file
                'perch� il nome del file � scomposta da numero ticket_numero record dettaglio
                UploadMyFile(SelectedTicket, fileUpEx)
            End If
        Else
            oTickValue.DateUpdate = DateTime.Now.ToShortDateString
            oIssueManager.Update(oTickValue)
            UploadMyFile(SelectedTicket, fileUpEx)
        End If
        TypeControl = ControlType.Selection
        ShowRightPanel()
        BindWithSearch(gridList)
    End Sub
    
    Sub SaveValueDetails(ByVal sender As Object, ByVal e As EventArgs)
        'salva i valori di subitem
        Dim oTickDetailsValue As New IssueTrackerTicketDetailsValue
        If SelectedId > 0 Then 'for update
            oTickDetailsValue.Key.KeyTicketDetail = SelectedId
            oIssueManager.Cache = False
            oTickDetailsValue = oIssueManager.ReadDetails(oTickDetailsValue.Key)
        End If
        With (oTickDetailsValue)
            .Link = txtLinkDet.Text
            .Subject = txtSubjectDet.Text
            .Note = txtNoteDet.Text
            .Detail = txtDetailsDet.Text
            .Tested = chkTestedDet.Checked
            .Environment = txtEnvironmentDet.Text
        End With
       
        oIssueManager = New IssueTrackerManager
        If SelectedId <= 0 Then
            'it's a new ticketDetals
            Dim oIssueTrackerIdentif As New IssueTrackerIdentificator
            oTickDetailsValue.KeyUser.Id = Me.ObjectTicket.User.Key.Id  'autenticated user
            oTickDetailsValue.DataTicket = DateTime.Now.ToShortDateString
            Dim s As Array
            s = lblHiddenDetails.Text.Trim.Split("-")

            If s.Length > 0 Then
                oTickDetailsValue.Key.NTicket = s(0).ToString.Trim
                oTickDetailsValue.Key.KeyTicket = CInt(s(1).ToString)
                oIssueTrackerIdentif = oIssueManager.CreateDetails(oTickDetailsValue)
                UploadMyFile(oTickDetailsValue.Key.NTicket & "_" & oIssueTrackerIdentif.KeyTicketDetail.ToString, fileUpExDet)
                
            End If
        Else
            UploadMyFile(SelectedTicket, fileUpExDet)
            oIssueManager.UpdateDetails(oTickDetailsValue)
        End If
        TypeControl = ControlType.ViewDetails
        SelectedId = oTickDetailsValue.Key.KeyTicket
        ShowRightPanel()
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.EditDetails
                pnl = FindControl("pnlDettDetails")
                LoadFormDettDetails(LoadValueDetails)
            Case ControlType.Copy
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.ViewDetails
                pnl = FindControl("pnlGridDetails")
                BindWithSearchDetails(gridListDetails)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack) Then
            LoadStatus(dwlStatus)
            LoadSubType(dwlType)
            LoadProject(dwlProj)
            ShowRightPanel()
            BindWithSearch(gridList)
        End If
    End Sub
    
    Function LoadUserRelation(ByVal i As Integer) As UserValue
        'dato un idcontext restituisce l'utente legato
        Dim userVal As UserValue = Nothing
        Dim UserManager As New UserManager
        Dim userSearcher As New UserSearcher
        Dim userColl As New UserCollection
        Dim usersVal As UserValue

        If i > 0 Then
            userSearcher.ContextId = i
        Else
            lblTo.Text = ""
            lblToId.Text = ""
            Return userVal
        End If
        Try
            userColl = UserManager.ReadUserContext(userSearcher)
            If Not userColl Is Nothing AndAlso userColl.Count > 0 Then
                usersVal = userColl(0)
                lblTo.Text = usersVal.Surname & " " & usersVal.Name
                lblToId.Text = usersVal.Key.Id
                userVal = usersVal
            Else
                lblTo.Text = ""
                lblToId.Text = ""
            End If
            
        Catch ex As Exception
            Response.Write(ex.ToString)
            userVal = Nothing
        End Try
        Return userVal
    End Function
    
    Protected Sub dwlProject_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If dwlProject.SelectedValue > 0 Then
            LoadWebSite()
        Else
            dwlWebSite.Items.Clear()
            dwlWebSite.Items.Insert(0, New ListItem("--Select--", -1))
        End If
        
        LoadUserRelation(dwlProject.SelectedValue)
        dwlSiteArea.Items.Clear()
        dwlSiteArea.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Protected Sub dwlWebSite_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If dwlWebSite.SelectedValue > 0 Then
            LoadUserRelation(dwlWebSite.SelectedValue)
        Else
            LoadUserRelation(dwlProject.SelectedValue)
        End If
        If dwlWebSite.SelectedValue > 0 Then
            LoadSiteArea()
        Else
            dwlSiteArea.Items.Clear()
            dwlSiteArea.Items.Insert(0, New ListItem("--Select--", -1))
        End If
    End Sub
    
    Protected Sub dwlSiteArea_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If dwlSiteArea.SelectedValue > 0 Then
            LoadUserRelation(dwlSiteArea.SelectedValue)
        Else
            LoadUserRelation(dwlWebSite.SelectedValue)
        End If
    End Sub
    
    Sub LoadSiteArea()
        'load the list of project
        Dim oContextManager As New ContextManager
        Dim oContextSearcher As New ContextSearcher
        Dim oContextColl As New ContextCollection
        dwlSiteArea.Items.Clear()
        
        oContextSearcher.KeyContentType.Id = 147
        oContextSearcher.KeyFather.Id = dwlWebSite.SelectedValue
        oContextColl = oContextManager.Read(oContextSearcher)
        
        utility.LoadListControl(dwlSiteArea, oContextColl, "Description", "Key.Id")
        dwlSiteArea.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub LoadWebSite()
        'load the list of project
        Dim oContextManager As New ContextManager
        Dim oContextSearcher As New ContextSearcher
        Dim oContextColl As New ContextCollection
        
        dwlWebSite.Items.Clear()
        oContextSearcher.KeyContentType.Id = 147
        oContextSearcher.KeyFather.Id = dwlProject.SelectedValue
        oContextColl = oContextManager.Read(oContextSearcher)
        
        utility.LoadListControl(dwlWebSite, oContextColl, "Key.Name", "Key.Id")
        dwlWebSite.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub LoadProject(ByVal dwl As DropDownList)
        'load the list of project
        Dim oContextManager As New ContextManager
        Dim oContextSearcher As New ContextSearcher
        Dim oContextColl As New ContextCollection
        
        oContextSearcher.KeyContentType.Id = 147
        oContextSearcher.KeyFather.Id = 96
        oContextColl = oContextManager.Read(oContextSearcher)
        utility.LoadListControl(dwl, oContextColl, "Key.Name", "Key.Id")
        dwl.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub LoadPriority()
        'load the list of project
        Dim ty As New IssueTrackerTicketValue.Priority
        Dim j As Integer = 0
        For Each ty In [Enum].GetValues(GetType(IssueTrackerTicketValue.Priority))
            Dim strMsgType As String = ty.ToString()
            Dim i As Integer() = [Enum].GetValues(GetType(IssueTrackerTicketValue.Priority))
            dwlPriority.Items.Insert(j, New ListItem(strMsgType, i(j)))
            j += 1
        Next
        dwlPriority.Items.Insert(0, New ListItem("--Select--", ""))
    End Sub
    
    Sub LoadStatus(ByVal dwl As WebControls.DropDownList)
        'load the list of status
        Dim oStatusColl As New StatusCollection
        Dim oStatusManager As New StatusManager
        Dim oStatusSearcher As New StatusSearcher
        
        oStatusColl = oStatusManager.Read(oStatusSearcher)
        utility.LoadListControl(dwl, oStatusColl, "Description", "Key.Id")
        dwl.Items.Insert(0, New ListItem("--Select--", ""))
    End Sub
    
    Sub LoadSubType(ByVal dwl As WebControls.DropDownList)
        'load the list of ticket type
        Dim ty As New IssueTrackerTicketValue.TicketType
        dwl.Items.Clear()
        Dim j As Integer = 0
        For Each ty In [Enum].GetValues(GetType(IssueTrackerTicketValue.TicketType))
            Dim strMsgType As String = ty.ToString()
            Dim i As Integer() = [Enum].GetValues(GetType(IssueTrackerTicketValue.TicketType))
            dwl.Items.Insert(j, New ListItem(strMsgType, i(j)))
            j += 1
        Next
        dwl.Items.Insert(0, New ListItem("--Select--", ""))
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        'imposta parametri ricerca ticket
        oIssueSearcher = New IssueTrackerSearcher
        If txtNTicket.Text <> "" Then oIssueSearcher.KeyTicket.NTicket = txtNTicket.Text
        If txtDateFrom.Value.HasValue AndAlso txtDateTo.Value.HasValue Then
            oIssueSearcher.InsertDataFrom = txtDateFrom.Value
            oIssueSearcher.InsertDataTo = txtDateTo.Value
        End If
        If dwlStatus.SelectedValue <> "" Then
            oIssueSearcher.KeyStatus.Id = dwlStatus.SelectedValue
        End If
        If dwlType.SelectedValue <> "" Then
            oIssueSearcher.IdContentSubType = dwlType.SelectedValue
        End If
        If dwlProj.SelectedValue > 0 AndAlso dwlProj.SelectedValue <> "" Then
            oIssueSearcher.Project.Id = dwlProj.SelectedValue
        End If
        BindGrid(objGrid, oIssueSearcher)
    End Sub
    
    Sub DeleteAttachedFile(ByVal s As Object, ByVal e As EventArgs)
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            If InStr(foundFile, SelectedTicket & "_") > 0 Then
                foundFile = foundFile & vbCrLf
                fileUpEx.Visible = True
                lblFileUpload.Visible = False
                btnDeleteFile.Visible = False
                My.Computer.FileSystem.DeleteFile(foundFile)
                Exit For
            End If
        Next
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As IssueTrackerSearcher = Nothing)
        oIssueManager = New IssueTrackerManager
        oTicketColl = New IssueTrackerTicketCollection

        If Searcher Is Nothing Then
            Searcher = New IssueTrackerSearcher
        End If
        oIssueManager.Cache = False
        oTicketColl = oIssueManager.Read(Searcher)
        If Not oTicketColl Is Nothing Then
            'oTicketColl.Sort(SortType, SortOrder)
            oTicketColl.Reverse()
        End If
        objGrid.DataSource = oTicketColl
        objGrid.DataBind()
    End Sub
    
    Sub BindGridDetails(ByVal objGrid As GridView, Optional ByVal Searcher As IssueTrackerSearcher = Nothing)
        oIssueManager = New IssueTrackerManager
        oTicketColl = New IssueTrackerTicketCollection
        
        If Searcher Is Nothing Then
            Searcher = New IssueTrackerSearcher
        End If
        oIssueManager.Cache = False
        oTicketColl = oIssueManager.ReadAll(Searcher)
        If Not oTicketColl Is Nothing Then
            'oTicketColl.Sort(SortType, SortOrder)
            oTicketColl.Reverse()
        End If
        objGrid.DataSource = oTicketColl
        objGrid.DataBind()
        oTicketColl.Reverse()
    End Sub
    Sub lnkViewDetail_View(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        ViewSelectedDetails(id)
    End Sub
    Sub ViewSelectedDetails(ByVal keyDetail As Integer)
        'carica il form con i valori del dettaglio riciesto
        Dim objManager As New IssueTrackerManager
        Dim objValue As New IssueTrackerTicketDetailsValue
        Dim objIdent As New IssueTrackerIdentificator
        objIdent.KeyTicketDetail = keyDetail
        
        objValue = objManager.ReadDetails(objIdent)
        
        lblSubject.Text = objValue.Subject
        lblDetails.Text = objValue.Detail
        lblLink.Text = "<a href='" & GetLink(objValue.Link) & "' target='blank' title='Link'>" & GetImg(objValue.Link) & "</a>"
        lblNote.Text = objValue.Note
        chkTested.Checked = Boolean.Parse(objValue.Tested.ToString)
        lblEnvironment.Text = objValue.Environment
        '************* recuper i file allegati
        ImgBtnScarica.Visible = False
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            If InStr(foundFile, objValue.Key.NTicket & "_" & objValue.Key.KeyTicketDetail) > 0 Then
                foundFile = foundFile
                ImgBtnScarica.Visible = True
                lblFileAttach.Text = foundFile
                Exit For
            End If
        Next
        '************* fine recupero file
    End Sub
    
    Sub BindWithSearchDetails(ByVal objGrid As GridView)
        'search details
        oIssueSearcher = New IssueTrackerSearcher
        oIssueSearcher.KeyTicket.KeyTicket = SelectedId
        BindGridDetails(objGrid, oIssueSearcher)
        
        If Not oTicketColl(0) Is Nothing Then
            lblHiddenDetails.Text = oTicketColl(0).KeyTicket.NTicket & "-" & oTicketColl(0).KeyTicket.KeyTicket
            lblTicket.Text = oTicketColl(0).KeyTicket.NTicket
            lblType.Text = SetStrValue(oTicketColl(0).IdContentSubType, "IdContentSubType")
            lblStatus.Text = SetStrValue(oTicketColl(0).ActualStatus, "ActualStatus")
            
            Dim strProject As String = ""
            strProject = SetStrValue(oTicketColl(0).Project.Id, "Project.Id")
            If oTicketColl(0).WebSite.Id > 0 Then
                strProject = strProject & " - " & SetStrValue(oTicketColl(0).WebSite.Id, "WebSite.Id")
                If oTicketColl(0).Location.Id > 0 Then
                    strProject = strProject & " - " & SetStrValue(oTicketColl(0).Location.Id, "Location.Id")
                End If
            End If
            If oTicketColl(0).Build <> "-1" Then
                strProject = strProject & "  (" & oTicketColl(0).Build & ")"
            End If
            lblProject.Text = strProject
            lblPriority.Text = oTicketColl(0).IdPriority.ToString
            lblOwner.Text = SetStrValue(oTicketColl(0).Owner.Id, "Owner.Id")
            lblRecipient.Text = SetStrValue(oTicketColl(0).Recipient.Id, "Recipient.Id")
            chkRequestMRL.Checked = Boolean.Parse(oTicketColl(0).RequestMri.ToString)
            chkAgreeMRL.Checked = Boolean.Parse(oTicketColl(0).AgreeMri.ToString)
            chkRequestMarket.Checked = Boolean.Parse(oTicketColl(0).RequestMarket.ToString)
            chkAgreeMarket.Checked = Boolean.Parse(oTicketColl(0).AgreeMarket.ToString)
            
            If oTicketColl(0).DateExpiry.HasValue Then
                lblDeadLine.Text = DateTime.Parse(oTicketColl(0).DateExpiry.ToString).ToShortDateString
                If oTicketColl(0).DateExpiry.HasValue AndAlso oTicketColl(0).DateExpiry < DateTime.Parse(DateTime.Now.ToShortDateString) Then
                    lblDeadLine.Text = lblDeadLine.Text & " <img src='/HP3Office/HP3Image/ico/contentwf_notapproval.gif' title='Expired'/>"
                Else
                    lblDeadLine.Text = lblDeadLine.Text & " <img src='/HP3Office/HP3Image/ico/contentwf_approval.gif' title='Not Expired'/>"
                End If
            End If
            lblDataInsert.Text = DateTime.Parse(oTicketColl(0).DateCreate.ToString).ToShortDateString
            
            'lblSubject.Text = oTicketColl(0).TicketDetails.Subject
            'lblDetails.Text = oTicketColl(0).TicketDetails.Detail
            'lblLink.Text = "<a href='" & GetLink(oTicketColl(0).TicketDetails.Link) & "' target='blank' title='Link'>" & GetImg(oTicketColl(0).TicketDetails.Link) & "</a>"
            'lblNote.Text = oTicketColl(0).TicketDetails.Note
            'chkTested.Checked = Boolean.Parse(oTicketColl(0).TicketDetails.Tested.ToString)
            'lblEnvironment.Text = oTicketColl(0).TicketDetails.Environment
            ''************* recuper i file allegati
            'ImgBtnScarica.Visible = False
            'For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            '    If InStr(foundFile, oTicketColl(0).KeyTicket.NTicket) > 0 Then
            '        foundFile = foundFile
            '        ImgBtnScarica.Visible = True
            '        lblFileAttach.Text = foundFile
            '        Exit For
            '    End If
            'Next
            ''************* fine recupero file
            
            ViewSelectedDetails(oTicketColl(0).TicketDetails.Key.KeyTicketDetail)
        End If
    End Sub
    
    Sub DeleteAttachedFileDet(ByVal s As Object, ByVal e As EventArgs)
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            If InStr(foundFile, SelectedTicket & "_") > 0 Then
                foundFile = foundFile & vbCrLf
                fileUpExDet.Visible = True
                lblFileUploadDet.Visible = False
                btnDeleteFileDet.Visible = False
                ImgBtnFileDet.Visible = False
                My.Computer.FileSystem.DeleteFile(foundFile)
                Exit For
            End If
        Next
    End Sub
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    Protected Sub gridListDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearchDetails(sender)
    End Sub
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> DictionaryGenericComparer.SortType.ById Then
                    SortType = DictionaryGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Label"
                If SortType <> DictionaryGenericComparer.SortType.ByLabel Then
                    SortType = DictionaryGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> DictionaryGenericComparer.SortType.ByKey Then
                    SortType = DictionaryGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As DictionaryGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = DictionaryGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As DictionaryGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As DictionaryGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = DictionaryGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As DictionaryGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    Protected Sub gridListDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "SelectItemDetails"
                TypeControl = ControlType.EditDetails
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DetailsItem"
                TypeControl = ControlType.ViewDetails
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItemDetails"
                SelectedId = id
                ItemDetailsRemove()
                BindWithSearchDetails(gridListDetails)
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        fileUpEx.Visible = True
        lblFileUpload.Visible = False
        btnDeleteFile.Visible = False
        ShowRightPanel()
        dwlActualStatus.Enabled = False
        dwlActualStatus.SelectedIndex = 1
    End Sub
    
    Sub NewItemDetails(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.EditDetails
        SelectedId = 0
        txtHidden.Text = ""
        fileUpExDet.Visible = True
        lblFileUploadDet.Visible = False
        ImgBtnFileDet.Visible = False  
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
    Function SetStrValue(ByVal str As String, ByVal type As String) As String
        'in fase di caricamento dati restituisce il valore passato il tipo da dato
        Dim result As String = ""
        Select Case type
            Case "IdContentSubType"
                Dim ty As New IssueTrackerTicketValue.TicketType
                Dim j As Integer = 1
                For Each ty In [Enum].GetValues(GetType(IssueTrackerTicketValue.TicketType))
                    Dim strMsgType As String = ty.ToString()
                    Dim i As Integer() = [Enum].GetValues(GetType(IssueTrackerTicketValue.TicketType))
                    If j = CInt(str) Then
                        result = strMsgType
                        Exit For
                    End If
                    j += 1
                Next
            Case "ActualStatus"
                Dim oStatusManager As New StatusManager
                Dim oStatusValue As New StatusValue
                oStatusValue = oStatusManager.Read(CInt(str))
                result = oStatusValue.Description
            Case "Project.Id"
                Dim oContextManager As New ContextManager
                Dim oContextValue As New ContextValue
                Dim oContextIdent As New ContextIdentificator
                oContextIdent.Id = CInt(str)
                oContextValue = oContextManager.Read(oContextIdent)
                result = oContextValue.Description
            Case "WebSite.Id"
                Dim oContextManager As New ContextManager
                Dim oContextValue As New ContextValue
                Dim oContextIdent As New ContextIdentificator
                oContextIdent.Id = CInt(str)
                oContextValue = oContextManager.Read(oContextIdent)
                result = oContextValue.Description
            Case "Location.Id"
                Dim oContextManager As New ContextManager
                Dim oContextValue As New ContextValue
                Dim oContextIdent As New ContextIdentificator
                oContextIdent.Id = CInt(str)
                oContextValue = oContextManager.Read(oContextIdent)
                result = oContextValue.Description
            Case "Owner.Id"
                Dim oUserManager As New UserManager
                Dim oUserValue As New UserValue
                oUserValue = oUserManager.Read(CInt(str))
                result = oUserValue.Surname & " - " & oUserValue.Name
            Case "Recipient.Id"
                If CInt(str) > 0 Then
                    Dim oUserManager As New UserManager
                    Dim oUserValue As New UserValue
                    oUserValue = oUserManager.Read(CInt(str))
                    result = oUserValue.Surname & " - " & oUserValue.Name
                End If
            Case Else
        End Select
        
        Return result
    End Function
    
    Function HasExpired(ByVal idTicket As Integer) As String
        'in fase di carimento della griglia restituisce l'img per data scadenza (scaduta o non scaduta)
        Dim res As String = ""
        Dim issueManager As New IssueTrackerManager
        Dim issueIdentificator As New IssueTrackerIdentificator
        Dim objValue As New IssueTrackerTicketValue
        issueIdentificator.KeyTicket = idTicket
        objValue = issueManager.Read(issueIdentificator)
        If objValue.DateExpiry.HasValue AndAlso objValue.DateExpiry < DateTime.Parse(DateTime.Now.ToShortDateString) Then
            res = "~/HP3Office/HP3Image/ico/contentwf_notapproval.gif"
        Else
            res = "~/HP3Office/HP3Image/ico/contentwf_approval.gif"
        End If
        Return res
    End Function
    
    Function GetLink(ByVal strLink As String) As String
        'restituisce il link nel formato corretto
        Dim res As String = ""
        If strLink <> "" Then
            If InStr(strLink, "http") Then
                res = strLink
            Else
                res = "http://" & strLink
            End If
        End If
        Return res
    End Function
    Function GetImg(ByVal str As String) As String
        'restituisce immagine se � presente link
        Dim res As String = ""
        If str <> "" Then
            res = "<img src='/HP3Office/HP3Image/ico/message.gif' />"
        End If
        Return res
    End Function
    Function GetFile(ByVal str As String) As String
        'dal nome del file se presente restituisce il percorso
        Dim res As String = ""
        '************* recuper i file allegati
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            If InStr(foundFile, str) > 0 Then
                foundFile = foundFile
                res = foundFile
                Exit For
            End If
        Next
        '************* fine recupero file
        Return res
    End Function
    Function IsVisible(ByVal str As String) As Boolean
        'restituisce le img allegato deve essere visibile o meno
        Dim res As Boolean = False
        '************* recuper i file allegati
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Server.MapPath("~/HP3Download/"), FileIO.SearchOption.SearchTopLevelOnly)
            If InStr(foundFile, str) > 0 Then
                res = True
                Exit For
            End If
        Next
        '************* fine recupero file
        Return res
    End Function
    
    Function DownloadFile(ByVal str As String) As Boolean
        'passato il percorso e nome permette di scaricare il file
        Dim ris As Boolean = False
        If StreamUtility.FileExists(str) Then
            System.Web.HttpContext.Current.Response.Clear()
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" & Right(str, str.LastIndexOf("\")))
            System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream"
            System.Web.HttpContext.Current.Server.Transfer("/" & Right(str, str.LastIndexOf("\")).Replace("\", "/"))
            ris = True
        End If
        Return ris
    End Function
    Protected Sub scaricaFile(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DownloadFile(lblFileAttach.Text)
    End Sub
    Protected Sub ScaricaFileGrid(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim strFile As String = s.commandargument
        If strFile <> "" Then
            DownloadFile(strFile)
        End If
    End Sub
    Protected Sub scaricaFileDet(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DownloadFile(lblFileUploadDet.Text)
    End Sub
</script>
<asp:Label id="lblHiddenDetails" Visible="false"  runat="server"></asp:Label>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:button CssClass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Ticket Manager</span></td>
            </tr>
        </table>
        <br />
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>N�Ticket</strong></td>
                    <td><strong>Date from</strong></td>
                    <td><strong>Date to</strong></td>
                    <td><strong>Status</strong></td>
                    <td><strong>Type</strong></td>
                    <td><strong>Project</strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtNTicket" TypeControl ="TextBox" style="width:50px"/></td>
                    <td><HP3:Date runat="server" ID="txtDateFrom" ShowDate="false" TypeControl="JsCalendar"/></td>
                    <td><HP3:Date runat="server" ID="txtDateTo" ShowDate="false" TypeControl="JsCalendar"/></td>
                    <td><asp:DropDownList id="dwlStatus"  runat="server" Width="100px" /></td>  
                    <td><asp:DropDownList id="dwlType"  runat="server" Width="100px" /></td>  
                    <td><asp:DropDownList id="dwlProj"  runat="server" /></td>
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" onclick="Search" Text="Search" CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>

    <asp:gridview ID="gridList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"
                AllowSorting="true"
                OnPageIndexChanging="gridList_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridList_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
              <Columns >
              
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="">
                    <ItemTemplate>
                    <asp:Image ID="imgDataEx" runat="server" ToolTip ='<%#DataBinder.Eval(Container.DataItem, "DateExpiry")%>' ImageUrl='<%#HasExpired(DataBinder.Eval(Container.DataItem, "KeyTicket.KeyTicket"))%>'/>

                    <%#DataBinder.Eval(Container.DataItem, "KeyTicket.KeyTicket")%>    </ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Data">
                    <ItemTemplate><%#DateTime.Parse(DataBinder.Eval(Container.DataItem, "DateInsert")).ToShortDateString%></ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Type">
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "IdContentSubType"), "IdContentSubType")%></ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="NTicket" SortExpression="Id">
                    <ItemTemplate><a title="N�Ticket"><%#DataBinder.Eval(Container.DataItem, "KeyTicket.NTicket")%></a></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Project" >
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "Project.Id"), "Project.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="From" >
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "Owner.Id"), "Owner.Id")%></ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderStyle-Width="10%" HeaderText="To" >
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "Recipient.Id"), "Recipient.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="7%" HeaderText="Priority" >
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "IdPriority")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Status" >
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "ActualStatus"), "ActualStatus")%></ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderStyle-Width="11%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeyTicket.KeyTicket")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attention! Do you want really remove the ticket and all subitems?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeyTicket.KeyTicket")%>'/>
                        <asp:ImageButton ID="lnkCopy" runat="server" ToolTip ="Details item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="OnItemSelected" CommandName ="DetailsItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeyTicket.KeyTicket")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="GoList" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button runat="server" ID="btnSave" onclick="SaveValue" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <table class="form" style="width:99%">
        <tr><td style="width:25%" valign="top"></td><td></td></tr>
        <tr>
            <td>Status</td>
            <td><asp:DropDownList id="dwlActualStatus" Width="200px" runat="server"  /></td>
        </tr>
        <tr>
            <td>DeadLine</td>
            <td><HP3:Date runat="server" ID="DeadLine" ShowDate="false" TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td>Ticket Type <strong style="color:Red ;">*</strong></td>
            <td>
                <asp:DropDownList id="dwlTicketType" Width="200px" runat="server"  />
                <asp:RequiredFieldValidator  ID="reqFiledType" ControlToValidate="dwlTicketType" runat="server" ErrorMessage="Select a ticket type"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Project <strong style="color:Red ;">*</strong></td>
            <td>
                <asp:DropDownList id="dwlProject" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="dwlProject_IndexChanged" runat="server"  />
<%--                <asp:RangeValidator ID="rangeValProj" ControlToValidate="dwlProject" runat="server" ErrorMessage="Select a project"></asp:RangeValidator>
--%>            </td>
        </tr>
        <tr>
            <td>WebSite</td>
            <td><asp:DropDownList id="dwlWebSite" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="dwlWebSite_IndexChanged"  runat="server"  /></td>
        </tr>
        <tr>
            <td>SiteArea</td>
            <td><asp:DropDownList id="dwlSiteArea" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="dwlSiteArea_IndexChanged" runat="server"  /></td>
        </tr>
        <tr>
            <td>Build</td>
            <td><asp:TextBox id="txtBuild" Width="200px" runat="server"  /></td>
        </tr>
        <tr>
            <td>From</td>
            <td><asp:Label id="lblFrom" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>To</td>
            <td><asp:Label id="lblTo" runat="server"></asp:Label><asp:Label Visible="false" id="lblToId" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Subject</td>
            <td><asp:TextBox id="txtSubject" Width="600px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Details</td>
            <td><asp:TextBox id="txtDetails" TextMode="MultiLine" Width="600px" Rows="10" Columns="100"  runat="server"/></td>
        </tr>
        <tr>
            <td>Link</td>
            <td><asp:TextBox id="txtlink" Width="300px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Note</td>
            <td><asp:TextBox id="txtNote" TextMode="MultiLine" Width="600px" Rows="3" runat="server"/></td>
        </tr>
        <tr>
            <td>FileUpload</td> 
            <td>
                <asp:FileUpload ID="fileUpEx" runat="server" />
                <asp:Label ID="lblFileUpload" Visible="false" runat="server"></asp:Label>
                <asp:Button ID="btnDeleteFile" Text="Delete attached file" OnClientClick="return confirm('Attention! Do you want really remove attacched file?')"  OnClick="DeleteAttachedFile"  Visible="false"  runat="server" />
            </td>
        </tr>
        <tr>
            <td>Priority</td>
            <td><asp:DropDownList id="dwlPriority" Width="100px"  runat="server"  /></td>
        </tr>
        <tr>
            <td>Request by MRL</td>
            <td><asp:CheckBox id="chkMRI" Width="100px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Agree MRL</td>
            <td><asp:CheckBox id="chkReqAgreeMRL" Width="100px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Request by Market</td>
            <td><asp:CheckBox id="chkMarket" Width="100px" runat="server"  /></td>
        </tr> 
        <tr>
            <td>Agree Market</td>
            <td><asp:CheckBox id="chkReqAgreeMarket" Width="100px" runat="server"  /></td>
        </tr> 
    </table> 
</asp:Panel>

<asp:Panel id="pnlGridDetails" Visible="false"  runat="server">
    <asp:button CssClass="button" ID="btnNewDetails" runat="server" Text="New Details" onclick="NewItemDetails"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="Panel2" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Ticket</span></td>
            </tr>
        </table>
        <table class="tbl1">
            <tr  class="header">
                <td colspan="2">
                    <asp:Label id="lblType" runat="server"></asp:Label> - 
                    <asp:Label id="lblTicket" runat="server"></asp:Label> -
                    (<asp:Label id="lblStatus" runat="server"></asp:Label>) 
                </td>
            </tr>
            <tr>
                <td><strong>Project:</strong> <asp:Label id="lblProject" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><strong>Insert Data:</strong> <asp:Label id="lblDataInsert" runat="server"></asp:Label></td>
                <td><strong>DeadLine:</strong> <asp:Label id="lblDeadLine" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><strong>From:</strong> <asp:Label id="lblOwner" runat="server"></asp:Label></td>
                <td><strong>To:</strong> <asp:Label id="lblRecipient" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Subject:</strong>  <asp:Label id="lblSubject" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Details:</strong> <asp:Label id="lblDetails" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Note:</strong> <asp:Label id="lblNote" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Priority:</strong> <asp:Label id="lblPriority" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <strong>Request by MRL:</strong>
                    <asp:CheckBox id="chkRequestMRL" runat="server" Enabled="false"/> 
                    <strong>Agree MRL:</strong>
                    <asp:CheckBox id="chkAgreeMRL" runat="server" Enabled="false"/> 
                </td>
                <td>
                    <strong>Request by Market:</strong> 
                    <asp:CheckBox id="chkRequestMarket" runat="server" Enabled="false"/>
                    <strong>Agree Market:</strong>
                    <asp:CheckBox id="chkAgreeMarket" runat="server" Enabled="false"/>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Tested:</strong> 
                    <asp:CheckBox ID="chkTested" runat="server" Enabled="false" />
                </td>
                <td><strong>Environment:</strong> <asp:Label id="lblEnvironment" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td ><strong>FileAttached:</strong> 
                    <asp:Label id="lblFileAttach" runat="server" Visible="false" ></asp:Label>
                    <asp:ImageButton id="ImgBtnScarica" OnClick="scaricaFile" ImageUrl="~/HP3Office/HP3Image/ico/type_document_small.gif" runat="server"/>
                </td>
               <td ><strong>Lnk:</strong> <asp:Label id="lblLink" runat="server"></asp:Label></td>
            </tr>
        </table>
        <hr />
    </asp:Panel>
    <table class="topContentSearcher">
        <tr>
            <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
            <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Details</span></td>
        </tr>
    </table>
    <asp:gridview ID="gridListDetails" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"
                AllowSorting="true"
                OnPageIndexChanging="gridListDetails_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridListDetails_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
              <Columns>
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                        <ItemTemplate><a title="Id"><%#DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail")%></a></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Insert Data">
                        <ItemTemplate><%#Left(DataBinder.Eval(Container.DataItem, "TicketDetails.DataTicket"), 10)%></ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Subject" >
                    <ItemTemplate><%#Left(DataBinder.Eval(Container.DataItem, "TicketDetails.Subject"), 30) & "..."%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Detail" >
                    <ItemTemplate><%#Left(DataBinder.Eval(Container.DataItem, "TicketDetails.Detail"), 60) & "..."%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Link" >
                    <ItemTemplate>
                        <a target="blank" href='<%#GetLink(DataBinder.Eval(Container.DataItem, "TicketDetails.Link"))%>' title="link">
                        <%#GetImg(DataBinder.Eval(Container.DataItem, "TicketDetails.Link"))%>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="File" >
                    <ItemTemplate>
                    <asp:ImageButton Visible='<%#IsVisible(DataBinder.Eval(Container.DataItem, "KeyTicket.NTicket") & "_" & DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail"))%>' ID="imgFile" runat="server" ToolTip ="File" ImageUrl="~/HP3Office/HP3Image/ico/type_document_small.gif" OnClick="ScaricaFileGrid" CommandArgument ='<%#GetFile(DataBinder.Eval(Container.DataItem, "KeyTicket.NTicket") & "_" & DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail"))%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="User" >
                    <ItemTemplate><%#SetStrValue(DataBinder.Eval(Container.DataItem, "TicketDetails.KeyUser.Id"), "Owner.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="11%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItemDetails" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attention! Do you want really remove the TicketDetails?')" OnClick="OnItemSelected" CommandName ="DeleteItemDetails" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail")%>'/>
                        <asp:ImageButton ID="lnkViewDetail" runat="server" ToolTip ="View item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="lnkViewDetail_View" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "TicketDetails.Key.KeyTicketDetail")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDettDetails" runat="server" Visible="false" >
    <div>
        <asp:button runat="server" ID="btnSaveDetails" onclick="SaveValueDetails" Text="Save Details" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="lblIdDetails" class="title" runat ="server"/>
    <span id="lblDescrizioneDetails" class="title" runat ="server"/>
    <table class="form" style="width:99%">
        <tr><td style="width:25%" valign="top"></td><td></td></tr>
        <tr>
            <td>From</td>
            <td><asp:Label id="lblUser" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Subject</td>
            <td><asp:TextBox id="txtSubjectDet" Width="600px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Details</td>
            <td><asp:TextBox id="txtDetailsDet" TextMode="MultiLine" Width="600px" Rows="10" Columns="100"  runat="server"/></td>
        </tr>
        <tr>
            <td>Link</td>
            <td><asp:TextBox id="txtLinkDet" Width="300px" runat="server"  /></td>
        </tr>
        <tr>
            <td>Note</td>
            <td><asp:TextBox id="txtNoteDet" TextMode="MultiLine" Width="600px" Rows="3" runat="server"/></td>
        </tr>
        <tr>
            <td>FileUpload</td>
            <td>
                <asp:FileUpload ID="fileUpExDet" runat="server" />
                <asp:Label ID="lblFileUploadDet" Visible="false" runat="server"></asp:Label>
                <asp:ImageButton id="ImgBtnFileDet" OnClick="scaricaFileDet" ImageUrl="~/HP3Office/HP3Image/ico/type_document_small.gif" runat="server"/>

                
                <asp:Button ID="btnDeleteFileDet" Text="Delete attached file" OnClientClick="return confirm('Attention! Do you want really remove attacched file?')"  OnClick="DeleteAttachedFileDet"  Visible="false"  runat="server" />
            </td>
        </tr>
        <tr>
            <td>Tested</td>
            <td><asp:CheckBox ID="chkTestedDet" runat="server" /></td>
        </tr>
        <tr>
            <td>Environment Test</td>
            <td><asp:TextBox ID="txtEnvironmentDet" Width="300px" runat="server" /></td>
        </tr>
    </table> 
</asp:Panel>