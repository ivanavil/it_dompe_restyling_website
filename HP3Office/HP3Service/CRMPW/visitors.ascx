﻿<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="HP3Office.CRMPW.ObjectValues" %>
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>

<script language="VB" runat="server">

#Region "LOCAL VARIABLES"
    Protected _PW_SITE As Integer = 8 ' getWebCongif
    Protected _PW_DOMAIN As String = "http://piwik.healthware.it/index.php" ' getWebCongif
    ' Protected _period As String = "year"
    ' Protected _date As String = "today"
    Protected _actualEvent As String = String.Empty
#End Region
    
#Region "HP3 EVENTS"
    Protected Const EventOverview As String = "overview"
    Protected Const EventVisLog As String = "visitorlog"
    Protected Const EventLocations As String = "locations"
    Protected Const EventTimes As String = "times"
    Protected Const EventSettings As String = "settings"
    Protected Const EventReturning As String = "returning"
#End Region
    
   
#Region "PIWIK Api (Module/Action)"
    Protected WidgetApi As Dictionary(Of String, List(Of WidgetApiValue)) = Nothing 'la dimensione della lista indica il numero di widget da popolare
#End Region
    
      
    Private Property SelPeriod() As String
        Get
            Return ViewState("period")
        End Get
        Set(ByVal value As String)
            ViewState("period") = value
        End Set
    End Property
    
    Private Property SelDate() As String
        Get
            Return ViewState("date")
        End Get
        Set(ByVal value As String)
            ViewState("date") = value
        End Set
    End Property
    
    Private Property Segment() As String
        Get
            Return ViewState("segment")
        End Get
        Set(ByVal value As String)
            ViewState("segment") = value
        End Set
    End Property
    
    Sub page_init()
        _actualEvent = Me.PageObjectGetEvent.Name.ToLower
        
        If Not (String.IsNullOrEmpty(_actualEvent)) Then
            LoadDictionaries(_actualEvent)
            SetCalendar()
        End If
    End Sub
    
    Sub page_load()
              
        If Not Page.IsPostBack Then
            If Not (String.IsNullOrEmpty(_actualEvent)) Then
                LoadWidgets()
            End If
        End If
    End Sub
    
    
    Sub LoadWidgets()
        Dim widgetApiList As New List(Of WidgetApiValue)
        widgetApiList.AddRange(WidgetApi.Item(_actualEvent))
        
        Dim widgetReq As New WidgetRequestValue
        widgetReq.SelectDate = getSelectedDate()
        widgetReq.SelectPeriod = getSelectedPeriod()
        widgetReq.Segment = getSegment()
        widgetReq.SiteId = _PW_SITE
               
        If widgetApiList.Count > 1 Then
            dlWidget.RepeatColumns = 2
           
        End If
       
        dlWidget.DataSource = widgetApiList
        dlWidget.DataBind()
                  
        Dim i As Integer = 0
        For Each widget As DataListItem In dlWidget.Items
            Dim w As System.Web.UI.HtmlControls.HtmlGenericControl = widget.FindControl("iwidget")
          
            If Not (w Is Nothing) Then
                
                Dim widgetapi As WidgetApiValue = widgetApiList.Item(i)
                
                widgetReq.ModuleToWidgetize = widgetapi.ModuleApi
                widgetReq.ActionToWidgetize = widgetapi.ActionApi
            
                Dim url As String = generateWidgetUrl(widgetReq)
                'Response.Write(url)
                If Not String.IsNullOrEmpty(url) Then w.Attributes("src") = url
                If dlWidget.Items.Count > 1 Then w.Attributes("height") = 330
                If dlWidget.Items.Count = 1 Then w.Attributes("height") = 480
            End If
           
                i = i + 1
        Next
        
    End Sub
    
    Sub LoadDictionaries(ByVal actualEvent As String)
        Dim olistApi As List(Of WidgetApiValue) = Nothing
        Dim oWidgetApiValue As WidgetApiValue = Nothing
        
        If Not (String.IsNullOrEmpty(actualEvent)) Then
            'Overview
            If actualEvent.Equals(EventOverview) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
                
                Dim omoduleList As String() = {"VisitsSummary"}
                Dim oactionList As String() = {"index"}
                
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
  
                WidgetApi.Add(EventOverview, olistApi)
            End If
            
            'VisLog
            If actualEvent.Equals(EventVisLog) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
                
                Dim omoduleList As String() = {"Live"}
                Dim oactionList As String() = {"getVisitorLog"}
              
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
  
                WidgetApi.Add(EventVisLog, olistApi)
            End If
        
            'Locations
            If actualEvent.Equals(EventLocations) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
                
                Dim omoduleList As String() = {"UserCountry", "UserCountry", "Provider"}
                Dim oactionList As String() = {"getCountry", "getContinent", "getProvider"}
                
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
             
                WidgetApi.Add(EventLocations, olistApi)
            End If
        
            'Settings
            If actualEvent.Equals(EventSettings) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
           
                Dim omoduleList As String() = {"UserSettings", "UserSettings", "UserSettings", "UserSettings", "UserSettings", "UserSettings", "UserSettings"}
                Dim oactionList As String() = {"getBrowserType", "getBrowser", "getPlugin", "getConfiguration", "getOS", "getResolution", "getWideScreen"}
                
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
             
                WidgetApi.Add(EventSettings, olistApi)
                              
            End If
            
            'Times
            If actualEvent.Equals(EventTimes) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
                
                Dim omoduleList As String() = {"VisitTime", "VisitTime"}
                Dim oactionList As String() = {"getVisitInformationPerLocalTime", "getVisitInformationPerServerTime"}
                
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
             
                WidgetApi.Add(EventTimes, olistApi)
            End If
            
            'Frequency
            If actualEvent.Equals(EventReturning) Then
                WidgetApi = New Dictionary(Of String, List(Of WidgetApiValue))
                olistApi = New List(Of WidgetApiValue)
                
                Dim omoduleList As String() = {"VisitorInterest", "VisitorInterest", "VisitorInterest", "VisitorInterest"}
                Dim oactionList As String() = {"getNumberOfVisitsPerVisitDuration", "getNumberOfVisitsPerPage", "getNumberOfVisitsByVisitCount", "getNumberOfVisitsByDaysSinceLast"}
                
                For i As Integer = 0 To omoduleList.Count - 1
                    oWidgetApiValue = New WidgetApiValue
                    oWidgetApiValue.ModuleApi = omoduleList(i)
                    oWidgetApiValue.ActionApi = oactionList(i)
                    
                    olistApi.Add(oWidgetApiValue)
                Next
             
                WidgetApi.Add(EventReturning, olistApi)
            End If
        End If
    End Sub
    
    Function generateWidgetUrl(ByVal widgetReq As WidgetRequestValue) As String
        Dim url As String = _PW_DOMAIN
        Dim sburl As New StringBuilder(url)
        
        If Not widgetReq Is Nothing Then
                       
            sburl.AppendFormat("?module={0}&action={1}&moduleToWidgetize={2}&actionToWidgetize={3}&idSite={4}&period={5}&date={6}&token_auth={7}", widgetReq.MainModule, widgetReq.MainAction, widgetReq.ModuleToWidgetize, widgetReq.ActionToWidgetize, widgetReq.SiteId, widgetReq.SelectPeriod, widgetReq.SelectDate, widgetReq.TokenAuthor)
            If Not String.IsNullOrEmpty(widgetReq.Segment) Then sburl.AppendFormat("&segment={0}", widgetReq.Segment)
        End If
        
       ' Response.Write(sburl.ToString & "</br>")
        Return sburl.ToString
    End Function
    
    Function getSelectedDate() As String
        Dim defaultDate As String = "last30"
        If String.IsNullOrEmpty(SelDate()) Then Return defaultDate
            
        Return SelDate()
    End Function
    
    Function getSelectedPeriod() As String
        Dim defaultPeriod As String = "range"
        
        If String.IsNullOrEmpty(SelPeriod()) Then Return defaultPeriod
           
        Return SelPeriod()
    End Function
    
    'period=day&date=2012-03-29 (date.now)
    'period=week&date=2012-03-29 (date.now)
    'period=month&date=2012-03-29 (date.now)
    'period=year&date=2012-03-29 (date.now)
    'period=range&date=2012-02-01,2012-03-29 (range)
    Sub setRange(ByVal s As Object, ByVal e As EventArgs)
        Dim period As String = s.CommandArgument
              
        If Not (String.IsNullOrEmpty(period)) Then
            SelPeriod() = period
            
            Select Case period
                Case "day", "week", "month", "year"
                    Dim dateNow As String = Format(Date.Now, "yyyy-MM-dd")
                    SelDate() = dateNow
                    
                Case "range"
                    'recupero da range
                    Dim dateFrom As String = Format(ddateFrom.Value, "yyyy-MM-dd")
                    Dim dateTo As String = Format(ddateTo.Value, "yyyy-MM-dd")
                   
                    SelDate() = dateFrom & "," & dateTo
            End Select
        End If
      
        LoadWidgets()
    End Sub
    
    Sub SetCalendar()
        Dim month As Double = -1
        ddateFrom.Value = Date.Now.AddMonths(month)
        ddateTo.Value = Date.Now
    End Sub
    
    Sub AddSegment(ByVal s As Object, ByVal e As EventArgs)
        If dwlevent.SelectedValue = "-1" Then
            Segment() = String.Empty
           
        Else
            Segment() = "customVariablePageName1=myevent" & ";customVariablePageValue1=" & dwlevent.SelectedValue.ToLower
            btnenter.Enabled = False
        End If
        
        LoadWidgets()
    End Sub
    
    Sub RemoveSegment(ByVal s As Object, ByVal e As EventArgs)
       
        Segment() = String.Empty
        btnenter.Enabled = True
        LoadWidgets()
    End Sub
    
    Function getSegment() As String
        Dim defaultSegment As String = String.Empty
        
        If String.IsNullOrEmpty(Segment()) Then Return defaultSegment
           
        Return Segment()
    End Function
    
    
</script>

<head>
	<title></title>
    <link href="/hp3Office/_css/piwikcrmstyle.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="/Js/jquery.js"></script>



</head>
<div>
<div>

           <div class="pageSearch">
                	<div class="rangeFrom">
                    	<label>From</label>
                        <HP3:Date runat="server" ID="ddateFrom" TypeControl="JsCalendar" ShowTime="false"/>
                        
                    </div>
                    <div class="rangeTo">
                    	<label>To</label>
                        <HP3:Date runat="server" ID="ddateTo" TypeControl="JsCalendar" ShowTime="false"/>
                    </div>

                    <div class="rangeBtn">
                          <asp:Button   id="btnrange" runat="server" text="Select Range"  type="button" class="button"  OnClick="setRange"  CommandArgument="range" />
                    </div>                 
                    <div class="btnDay">
                        <div class="month">
                             <asp:Button  id="btnmonth" text="Month"   type="button" class="button" runat="server" OnClick="setRange"  CommandArgument="month"/>
                        </div>
                        <div class="week">
                             <asp:Button  id="btnweek" text="Week"   type="button" class="button" runat="server" OnClick="setRange"  CommandArgument="week"/>
                        </div>
                        <div class="day">
                            <asp:Button  id="btnday" text="Day"   type="button" class="button" runat="server" onclick="setRange" CommandArgument="day" />
                        </div>
                    </div>
            </div> <!--end pageSearch-->
                 
           <div class="pageSearch" >
           <div  class="row">
                	<div class="segment1">
                    	<label>MyEvent</label>
                        <asp:DropDownList id="dwlevent" runat="server">
                            <asp:ListItem Text="--select--" Value="-1"/>
                            <asp:ListItem Text="print" Value="print"/>
                            <asp:ListItem Text="sendfriend" Value="sendfriend"/> 
                        </asp:DropDownList>
                    </div>


                    <div class="segment2">
                    	<label>Operation</label>
                        <asp:DropDownList id="dwlOperation" runat="server">
                            <asp:ListItem Text="=" Value="="/>
                            <asp:ListItem Text="<>" Value="!="/> 
                            <asp:ListItem Text="<" Value="<"/> 
                            <asp:ListItem Text="<" Value=">"/> 
                        </asp:DropDownList>
                    </div>

                    <div class="segment3">
                    	<%--<label>User Identificator</label>--%>
                        <%--<asp:DropDownList id="dwluserIdent" runat="server">
                            <asp:ListItem Text="8|luca.roberto@publicishealthware.com" Value="8|luca.roberto@publicishealthware.com"/>
                            <asp:ListItem Text="7|lino.mari@publicishealthware.com " Value="7|lino.mari@publicishealthware.com "/> 
                     </asp:DropDownList>--%>
                    </div>

                    <div class="btnEnter">
                        <div  class="remove">
                            <asp:Button  id="btnremove" text="Remove" type="button" class="button" runat="server"   OnClick="RemoveSegment" />
                        </div>

                          <div class="enter">
                             <asp:Button  id="btnenter" text="Add" type="button" class="button" runat="server" OnClick="AddSegment"/>
                         </div>
                     
                    

                    </div>
                   </div>                 
                </div> <!--end pageSearch-->
                               
            

           
            
            <asp:Datalist id="dlWidget" runat="server" class="tableWidget" BorderStyle="None">
	          
	            <ItemTemplate>
	                  	
                    <iframe id="iwidget" name="iwidget" runat="server" src=""  width="100%" frameborder="0" marginwidth="0" marginheight="0"/>
                        
    	        </ItemTemplate>
            </asp:Datalist> 

            </div> <!--end rightCol-->
        </div> <!--end containerRight-->


