﻿<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<script language="VB" runat="server" >

    Sub page_init()
       
    End Sub

    Sub page_prerender()
    
    End Sub
    
    Sub page_load()
    
    End Sub
    
</script>
<head>
	<title></title>
    <link href="/hp3Office/_css/piwikcrmstyle.css" type="text/css" rel="stylesheet" />
</head>
<div>
<div>

                <div class="personalyze">
            		<p></p>
                </div>
                
                <div class="titlePageSearch">
                	<div class="searchSite">
                    	<label>User Status</label>
                        <asp:DropDownList id="dwluserStatus" runat="server">
                            <asp:ListItem Text="Logged" Value="Logged"/>
                            <asp:ListItem Text="Anonymous" Value="Anonymous"/> 
                        </asp:DropDownList>
                    </div>


                    <div class="range">
                    	<label>User Type</label>
                        <asp:DropDownList id="dwluserType" runat="server">
                            <asp:ListItem Text="Generic" Value="Generic"/>
                            <asp:ListItem Text="baseadmin" Value="baseadmin"/> 
                            <asp:ListItem Text="CoAdmin" Value="CoAdmin"/> 
                            <asp:ListItem Text="Admin" Value="Admin"/> 
                        </asp:DropDownList>
                    </div>

                    <div class="range">
                    	<label>User Identificator</label>
                        <asp:DropDownList id="dwluserIdent" runat="server">
                            <asp:ListItem Text="8|luca.roberto@publicishealthware.com" Value="8|luca.roberto@publicishealthware.com"/>
                            <asp:ListItem Text="7|lino.mari@publicishealthware.com " Value="7|lino.mari@publicishealthware.com "/> 
                     </asp:DropDownList>
                    </div>

                    <div class="rangeBtn">
                    	<input type="image" src="_slice/range.jpg" />
                    </div>
                    
                    <div class="btnDay">
                        <div class="month">
                    		<input type="image" src="_slice/month.jpg" />
                        </div>
                        <div class="week">
                    		<input type="image" src="_slice/week.jpg" />
                        </div>
                        <div class="day">
                            <input type="image" src="_slice/day.jpg" />
                        </div>
                    </div>
                </div> <!--end titlePageSearch-->
                
                
                <div class="titlePage1">
                	<div class="titlePage2">
                    	<h2 class="visit"><b>Visitors - Visitor Log</b></h2>
                    </div> 
                	<div class="titlePage3">
                    		<div class="all">
                                <iframe width="100%" scrolling="no" height="350" frameborder="0" marginwidth="0" marginheight="0"
                                        src="http://piwik.healthware.it/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getOutlinks&idSite=8&period=year&date=today&token_auth=c90e43390a63bc9961584f146ddd34de"></iframe>
                            </div>
                    </div>
                </div>
                
                
                <div class="titlePage1">
                	<div class="titlePage2">
                    	<h2 class="visit"><b>Web site usage</b></h2>
                    </div> 
                	<div class="titlePage3">
                    		<div class="right">
                            	<div class="right1">
                             		
                                    <img class="imgStyle" src="_slice/img.jpg" />
                                     <a href="#">Lorem ipsum dixit</a>
                                     <p class="noMargin">Lorem ipsum dolor sit amet</p>
                                     <p class="italic">999 Visits</p>
                                 
                                </div>
                                
                                <div class="right1">
                                	<img class="imgStyle" src="_slice/img.jpg" />
                                     <a href="#">Lorem ipsum dixit</a>
                                   	 <p class="noMargin">Lorem ipsum dolor sit amet</p>
                                     <p class="italic">999 Visits</p>
                                </div>
                            </div>
                            <div class="left">
                            
                            	<div class="right1">
                             		
                                    <img class="imgStyle" src="_slice/img.jpg" />
                                     <a href="#">Lorem ipsum dixit</a>
                                     <p class="noMargin">Lorem ipsum dolor sit amet</p>
                                     <p class="italic">999 Visits</p>
                                 
                                </div>
                                
                                <div class="right1">
                                	<img class="imgStyle" src="_slice/img.jpg" />
                                     <a href="#">Lorem ipsum dixit</a>
                                   	 <p class="noMargin">Lorem ipsum dolor sit amet</p>
                                     <p class="italic">999 Visits</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div> <!--end rightCol-->
        </div> <!--end containerRight-->