<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>   
<%@ Import Namespace="Healthware.HP3.Core.Community"%>
<%@ Import Namespace="Healthware.HP3.Core.Community.ObjectValues"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Linq"%>
<%@ Import Namespace="Healthware.Dmp.Helpers" %>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %> 
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlExtraField" Src ="~/hp3Office/HP3Parts/ctlExtraField.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlProfiles" Src ="~/hp3Office/HP3Parts/ctlProfilesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Localization" Src ="~/hp3Office/HP3Parts/ctlGeo.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlUserGroups" Src ="~/hp3Office/HP3Parts/ctlUserGroups.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlContext" Src ="~/hp3Office/HP3Parts/ctlContextList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlCommunity" Src ="~/hp3Office/HP3Parts/ctlCommunityList.ascx"%>
<script runat="server">
    Private systemUtility As New SystemUtility()
    Private mPageManager As New MasterPageManager()
    Private userManager As New UserManager()
    Private userHCPManager As New UserHCPManager
    Private oGenericUtlity As New GenericUtility
    Private profilingManager As New ProfilingManager
    Private dictionaryManager As New DictionaryManager
    Private userGroupManager As New UserGroupManager
    Private ContextManager As New ContextManager
    
    Private omailTempalteManager As New MailTemplateManager
    Private omailTemplateValue As New MailTemplateValue
    Private omailTemplateColl As New MailTemplateCollection
    Private omailTemplateSearcher As New MailTemplateSearcher
    
    Private contentSearcher As New ContentSearcher
    Private _sortType As UserGenericComparer.SortType
    Private _sortOrder As UserGenericComparer.SortOrder
    Private extFManager As New ExtraFieldsManager
    
    Private utility As New WebUtility
       
    Private mailValue As New Healthware.HP3.Core.Utility.ObjectValues.MailValue
    Private _mySortType As GeoGenericComparer.SortType = GeoGenericComparer.SortType.ByDescription
    Dim _objCachedDataSet As DataSet = Nothing
	Dim strQuery As String = "readPartners"
    Private strJS As String
    Private strDomain As String
    Private strName As String
    Private siteLabel As String
    Private _strDomain As String
    Private _language As String
    
    Private PrivateData As Boolean = True
    Private _userValue As Integer
    Private serviceColl As New ServiceCollection
    Private communitySelected As Integer
    
    Const _Single As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._single
    Const Married As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._married
    Const Divorced As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._divorced
    Const Widowed As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._widowed
    Const Separated As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._separated
    Const Living_with_partner As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._living_with_partner
    
    Private _oGeoCollection As GeoCollection
    Private _HCPGeoCollection As GeoCollection
    Private _oGeoManager As New GeoManager
    
    Private Const PageSize As Integer = 20
    Private Const NPageVis As Integer = 6
   
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property User() As Integer
        Get
            Return _userValue
        End Get
        Set(ByVal value As Integer)
            _userValue = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property SortType() As UserGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = UserGenericComparer.SortType.ByKey ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As UserGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As UserGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = UserGenericComparer.SortOrder.ASC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As UserGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
       
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = Me.PageObjectGetLang.Id
        Domain() = strDomain
             
        If Not (Page.IsPostBack) Then
            ActivePanelGrid()
            'LoadArchive() 'Carica la lista utenti
            'BindCommunity()
           
        Else
            'BindSpeciality()
            'Carica se stesso con un type diverso
            
            'oLocalization.LoadControl()
            'hcpLocalization.LoadControl()
        End If
        
        BindAllUserGeo(-1, "user")
        'BindAllUserGeo(-1, "hcp")
    End Sub
    
    Function ReadSons(ByVal item As Int32) As GeoCollection
        _oGeoManager.Cache = False
        Dim _oGeoSearcher = New GeoSearcher
        _oGeoSearcher.SonsOf = item
        
        Dim _oGeoCollection = _oGeoManager.Read(_oGeoSearcher)
        Return _oGeoCollection
    End Function

    Sub BindAllUserGeo(ByVal geoParent As Int16, ByVal type As String)
        Dim _oGeoCollection As GeoCollection = ReadSons(geoParent)
      
        If Not _oGeoCollection Is Nothing Then
            _oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
            'Select Case type.ToLower
                'Case "user"
                    For Each geoItem As GeoValue In _oGeoCollection
                        pnlGeoTree.Controls.Add(New LiteralControl(AddGetoToTree(geoItem, "USER").ToString))
                    Next
                'Case "hcp"
                '    For Each geoItem As GeoValue In _oGeoCollection
                '        pnlGeoHCPTree.Controls.Add(New LiteralControl(AddGetoToTree(geoItem, "HCP").ToString))
                '    Next
            'End Select
        End If
    End Sub
    
    Function AddGetoToTree(ByVal item As GeoValue, ByVal type As String) As StringBuilder
        Dim outDiv As New StringBuilder
        
        Dim oGeoCollection As GeoCollection = ReadSons(item.Key.Id)
           
        Dim nCount As Int16
        Dim oGeoItem As GeoValue
        Dim bHasSons As Boolean
            
        If oGeoCollection Is Nothing Then
            nCount = 0
            bHasSons = False
        Else
            oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
            nCount = oGeoCollection.Count
            bHasSons = True
        End If
           
        'divMain
        outDiv.Append("<Div style='margin:10px' id='divMain'>" & vbCrLf)
            
        'divLink
        outDiv.Append("<Div id='divLink' onclick='DisplayDiv(""divSons_" & type & item.Key.Id & """,document.getElementById(""linkPlus_" & type & item.Key.Id & """));SelItem(this,""" & item.Key.Id & """,""" & item.Description.Replace("'", Chr(60)) & """, """ & type & """)' onmouseout='RestoreColor(this)' onmouseover='ChangeColor(this)' >" & vbCrLf)
        If nCount > 0 Then
            outDiv.Append("<a id='linkPlus_" & type & item.Key.Id & "'>+&nbsp;</a>")
        Else
            outDiv.Append("<a id='linkPlus_" & type & item.Key.Id & "'>&nbsp;&nbsp;&nbsp;</a>")
        End If
        outDiv.Append("<a id='linkDescr'>" & item.Description & "</a>" & vbCrLf)
            
        'divLink
        outDiv.Append("</Div>" & vbCrLf)
        '************************divLink********************************
            
        'divSons
        If bHasSons Then
            outDiv.Append("<Div id='divSons_" & type & item.Key.Id & "' style='display:none'>" & vbCrLf)
            For Each oGeoItem In oGeoCollection
                outDiv.Append(AddGetoToTree(oGeoItem, type))
            Next
            outDiv.Append("</Div>" & vbCrLf)
        End If
            
        outDiv.Append("</Div>" & vbCrLf)
               
        Return outDiv
    End Function
    
    Sub Page_prerender()
       
        If (ctlCommunity.ListIndex > 0) Then
            communitySelected = ctlCommunity.ListIndex
            Dim contentIdentificator As New ContentIdentificator()
            contentIdentificator.PrimaryKey = communitySelected
            
            Dim contentValue As ContentValue = Me.BusinessContentManager.Read(contentIdentificator)
            communityId.Text = contentValue.Key.Id
            communityLanguageId.Text = contentValue.Key.IdLanguage
           
            If (LoadService(communityId.Text, communityLanguageId.Text)) Then
                divCommunity.Visible = True
                LoadServiceUserPermission()
            End If
        End If
    End Sub
      
    'recupera la lista dei siti
    Sub LoadArchive()
        'Dim userSearcher As New UserSearcher
        'Dim userColl As New UserCollection
                      
        'userManager.Cache = False
        'userColl = userManager.Read(userSearcher)
       
        ' sectionTit.Visible = True
        ' gdw_usersList.DataSource = userColl
        ' gdw_usersList.DataBind()
        
       BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
       
        
    Sub AddItemHCPFatherText(ByVal s As Object, ByVal e As EventArgs)
        'Dim geoId As String
        'geoId = txtHCPFatherHidden.Text
        'If geoId <> "" Then
        '    HCPFather.Text = ReadGeo(Int32.Parse(geoId))
        'Else
        '    strJS = "alert('Select an element from the list hcp')"
        'End If
    End Sub
    
    Sub AddItemGeoText(ByVal s As Object, ByVal e As EventArgs)
        Dim geoId As String
               
        geoId = _txtFatherHidden.Text
        If geoId <> "" Then
            '_Father.Text = ReadGeo(Int32.Parse(geoId))
        Else
            strJS = "alert('Select an element from the list hcp')"
        End If
    End Sub
    
    Function ReadGeo(ByVal item As Integer) As String
        _oGeoManager = New GeoManager
        _oGeoManager.Cache = False 'necessario
            
        Dim oGeoValue As GeoValue = _oGeoManager.Read(New GeoIdentificator(item))
        If Not (oGeoValue) Is Nothing Then
            Return oGeoValue.Description
        End If
        
        Return Nothing
    End Function
    
    ' Edit di un utente di tipo User generico
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userValue As New UserHcpValue
        Dim userSearcher As New UserHcpSearcher
        Dim UserId As Integer
        divCommunity.Visible = False

        If sender.CommandName = "SelectItem" Then
            UserId = sender.CommandArgument
            SelectedId = UserId
            userManager.Cache = False
			userHcpManager.Cache=false
            userValue = userHcpManager.Read(New UserIdentificator(UserId))
            If Not userValue Is Nothing Then
                If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.HCP) Then
                    EditHCP()
                End If
				
				btnRegistration_1.Visible = False
                btnRegistration_2.Visible = False
                
                User = userValue.Key.Id
                LoadUserDett(userValue)
                ActivePanelDett()
            End If
        End If
        
        If sender.CommandName = "ActiveUser" Then
            UserId = sender.CommandArgument
            SelectedId = UserId
           
            userHCPManager.Cache = False
            userValue = userHCPManager.Read(New UserIdentificator(UserId))
            If Not userValue Is Nothing Then
                userValue.Status = 3
                userHCPManager.Update(userValue)
            End If
            BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
            'LoadArchive()
        End If
		
		
        If sender.CommandName = "NoActiveUser" Then
            UserId = sender.CommandArgument
            SelectedId = UserId
           
            userHCPManager.Cache = False
            userValue = userHCPManager.Read(New UserIdentificator(UserId))
            If Not userValue Is Nothing Then
                userValue.Status = 1
                userHCPManager.Update(userValue)
            End If
            BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
            'LoadArchive()
        End If		
    End Sub
    
    'Edit di un utente di tipo HCP
    Sub EditHCP()
        Dim userHCPValue As New UserHCPValue
        Dim userHCPSearcher As New UserHCPSearcher
                     
        userManager.Cache = False
        userHCPSearcher.Key.Id = SelectedId
        userHCPManager.Cache = False
        userHCPValue = userHCPManager.Read(userHCPSearcher)(0)
       
        'If Not userHCPValue Is Nothing Then
        '    LoadHCPDett(userHCPValue)
        'End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
        'LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim userHCPValue As UserHCPValue = Nothing
        Dim userHcp As UserHCPValue = Nothing
		
		'Create a new Account		
        If SelectedId = 0 Then
			userHCPValue = SetHCPDett()
			If Not (userHCPValue Is Nothing) Then
				userHcp = userHCPManager.Create(userHCPValue)
			
				Dim _intRoleId As Integer = usertype.SelectedItem.Value
				Dim _intProfileId As Integer = 0
				
				Select Case _intRoleId
					Case 64 'Hcp
						_intProfileId = 10
					
					Case 63 'Media
						_intProfileId = 11
					
					Case 65 'Partner
						_intProfileId = 12
					
					Case 78 'Interni
						_intProfileId = 13
					
				End Select
				'Utente Partner
				If _intProfileId = 12 Then
					For each li as listitem in chkFocusOn.Items
						If li.Selected Then 
							Dim o As New ContentUserValue
							With o
								o.KeyContent.Id = li.Value
								o.KeyContent.IdLanguage = 1
								o.KeyRelationType.Id = Dompe.RelationType.PartnerContent
								o.KeyUser.Id = userHCPValue.Key.Id
							End With
							Me.BusinessContentManager.CreateContentUser(o)
						End If
					Next
				End If
				
				'Utente di sede
				If _intProfileId = 13 Then
					'Press
					Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(63))
					Me.BusinessProfilingManager.CreateUserProfile(userHcp.Key, New ProfileIdentificator(11))
					
					'HCP
					Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(64))
					Me.BusinessProfilingManager.CreateUserProfile(userHcp.Key, New ProfileIdentificator(10))

					'Partner
					Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(65))
					Me.BusinessProfilingManager.CreateUserProfile(userHcp.Key, New ProfileIdentificator(12))	

					'Do i permessi sui contenuti dei focus on partner
					Dim so As New ContentSearcher()
					so.KeySiteArea.Id = 51
					so.KeyContentType.Id = 174
					so.key.IdLanguage = 1
					so.SortType = ContentGenericComparer.SortType.ByData
					so.SortOrder = ContentGenericComparer.SortOrder.DESC
					so.Properties.Add("Title")
					so.Properties.Add("Key.Id")
				
					Dim coll As New ContentCollection
					coll = Me.BusinessContentManager.Read(so)
					If Not coll Is Nothing AndAlso coll.Count > 0 Then
						For each item as contentvalue in coll
							Dim o As New ContentUserValue
							With o
								o.KeyContent.Id = item.Key.Id
								o.KeyContent.IdLanguage = 1
								o.KeyRelationType.Id = Dompe.RelationType.PartnerContent
								o.KeyUser.Id = userHCPValue.Key.Id
							End With
							Me.BusinessContentManager.CreateContentUser(o)
						Next
					End If
					
					If showDraft.checked Then
						Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(79))
					End If
				End If
				
				'gestione dei ruoli e dei profili---------------------------------------------------------------------------
				Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(_intRoleId))
				Me.BusinessProfilingManager.CreateUserProfile(userHcp.Key, New ProfileIdentificator(_intProfileId))
				'------------------------------------------------------------------------------------------------------------

				'Ricarico la griglia iniziale---------------
				BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
				ActivePanelGrid()
				'-------------------------------------------
			End If
			
        Else 'update existing account
			userHCPManager.cache = false
            userHcp = userHCPManager.Read(New UserIdentificator(SelectedId))
            'user = userValue
			userHCPValue = SetHCPDett(userHcp)

			If Not (userHCPValue Is Nothing) Then
				'userHCPValue.Key.Id = SelectedId
				userHCPManager.Update(userHCPValue)
				BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
				ActivePanelGrid()
				
				If usertype.SelectedItem.Value = 78 Then
					If showDraft.checked Then
						Me.BusinessProfilingManager.CreateUserRole(userHcp.Key, New RoleIdentificator(79))
					Else
						Me.BusinessProfilingManager.RemoveUserRole(userHcp.Key, New RoleIdentificator(79))
					End If
				End If
				
				'Utente Partner
				If usertype.SelectedItem.Value = 65 Then
					For each li as listitem in chkFocusOn.Items
						Dim cuSo As New ContentUserSearcher
						With cuSo
							.KeyContent.Id = li.Value
							.KeyContent.IdLanguage = 1
							.KeyRelationType.Id = Dompe.RelationType.PartnerContent
							.KeyUser.Id = userHCPValue.Key.Id
						End With
						Me.BusinessContentManager.RemoveContentUser(cuSo)
						
						If li.Selected Then 
							Dim o As New ContentUserValue
							With o
								o.KeyContent.Id = li.Value
								o.KeyContent.IdLanguage = 1
								o.KeyRelationType.Id = Dompe.RelationType.PartnerContent
								o.KeyUser.Id = userHCPValue.Key.Id
							End With
							Me.BusinessContentManager.CreateContentUser(o)
						End If
					Next
				End If
			End If
        End If
    End Sub

    
    'carica il controllo che gestisce l'associazioni Community/Utente
    Sub LoadCommunityControl(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim Userident As Integer
        Dim UserValue As UserValue
        
        divCommunity.Visible = False
        If sender.CommandName = "Community" Then
            Userident = sender.CommandArgument
            ActivePanelCommunity()
            
            UserValue = userManager.Read(New UserIdentificator(Userident))
            If Not UserValue Is Nothing Then
                'gestione community
                userId.Text = UserValue.Key.Id
                
                ctlCommunity.GenericCollection = GetUserCommunityValue(UserValue.Key.Id)
                'Response.Write(CType(ctlCommunity.GenericCollection, CommunityCollection).Count)
                ctlCommunity.LoadControl()
            End If
           
        End If
    End Sub
    
    'gestisce il salvataggio dell'associazione community/utente
    Sub SaveCommunityRelation(ByVal sender As Object, ByVal e As EventArgs)
        'gestione dell'associazione user/community
        Dim userIdent As New UserIdentificator()
        userIdent.Id = userId.Text
        
        CommunityManager(userIdent)
        strJS = "alert('Select a Community to Continue!')"
        
        'btSaveRelation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    '
    Sub SaveSegmentation(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
    
    'gestisce il salvataggio dell'associazione community/utente/permessi
    Sub SaveUserPermission(ByVal sender As Object, ByVal e As EventArgs)
        Dim userIdent As New UserIdentificator()
        userIdent.Id = userId.Text
        
        SaveServiceUserPermission(userIdent)
        
        'LoadArchive()
        BindWithSearch(gdw_usersList)
        ActivePanelGrid()
    End Sub
    
    'gestione dei ruoli
    Sub RolesManager(ByVal key As UserIdentificator)
        profilingManager.RemoveAllUserRole(key)
        If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
            Dim Rc As New Object
            Dim role As New RoleValue
            Rc = ctlRoles.GetSelection
            For Each role In Rc
                profilingManager.CreateUserRole(key, New RoleIdentificator(role.Key.Id))
            Next
        End If
    End Sub
    
    'gestione dei profili
    Sub ProfilesManager(ByVal key As UserIdentificator)
        
        profilingManager.RemoveAllUserProfile(key)
        If Not ctlProfiles.GetSelection Is Nothing AndAlso ctlProfiles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim profile As New ProfileValue
            Pc = ctlProfiles.GetSelection
            For Each profile In Pc
                 profilingManager.CreateUserProfile(key, New ProfileIdentificator(profile.Key.Id))
            Next
        End If
    End Sub
    
    'gestione dei gruppi
    Sub GroupsManager(ByVal key As UserIdentificator)
        userGroupManager.RemoveUserRelation(key)
        
        If Not ctluserGroups.GetSelection Is Nothing AndAlso ctluserGroups.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim group As New UserGroupValue
            Pc = ctluserGroups.GetSelection
            For Each group In Pc
                userGroupManager.CreateUserGroupRelation(key, New UserGroupIdentificator(group.Key.Id))
            Next
        End If
    End Sub
    

    
    'gestione community
    Sub CommunityManager(ByVal key As UserIdentificator)
        Me.BusinessCommunityManager.RemoveAllUser(key)
        
        If Not ctlCommunity.GetSelection Is Nothing AndAlso ctlCommunity.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim community As New CommunityValue
            Pc = ctlCommunity.GetSelection
            For Each community In Pc
                Me.BusinessCommunityManager.AddUser(New CommunityIdentificator(community.Key.Id, community.Key.IdLanguage), key)
            Next
        End If
    End Sub
       
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
		rowApprovalCheck.visible= false
		showDraft.checked = false
		
        btnSave.Text = "Save"
        SelectedId = 0
        ActivePanelDett()
        LoadUserDett(Nothing)
		
		userType.Items.Clear()
		userType.Items.Add(new ListItem("Select Please", 0))
		userType.Items.Add(new ListItem("Partner", 65))
		userType.Items.Add(new ListItem("Interno", 78))
		userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("0"))
		
        'LoadHCPDett(Nothing)
    End Sub
	
	Sub loadFocusOn(ByVal intUserId as Integer)
		Dim strAssignedFocusOn as String = String.Empty
		If intUserId > 0 Then
			Me.BusinessContentManager.Cache = False
			
			Dim so1 As New ContentSearcher()
			so1.KeySiteArea.Id = 51
			so1.KeyContentType.Id = 174
			so1.key.IdLanguage = 1
		
			so1.RelatedTo.ContentUser.KeyUser.Id = intUserId
			so1.RelatedTo.ContentUser.KeyRelationType.Id = Dompe.RelationType.PartnerContent
			so1.Properties.Add("Title")
			so1.Properties.Add("Key.Id")
			
			Dim coll1 As New ContentCollection
			coll1 = Me.BusinessContentManager.Read(so1)
			
			If Not coll1 Is Nothing AndAlso coll1.Count > 0 Then
				For each ob as contentvalue in coll1
					strAssignedFocusOn += ob.Key.Id & ","
				Next
				strAssignedFocusOn = "," & strAssignedFocusOn
			End if
		End If

	
		Dim so As New ContentSearcher()
		so.KeySiteArea.Id = 51
		so.KeyContentType.Id = 174
		so.key.IdLanguage = 1
		'dm comuni a tutte le chiamate , nel caso generalizzare
		so.SortType = ContentGenericComparer.SortType.ByData
		so.SortOrder = ContentGenericComparer.SortOrder.DESC
		so.Properties.Add("Title")
		so.Properties.Add("Key.Id")
	
		Dim coll As New ContentCollection
		coll = Me.BusinessContentManager.Read(so)

		If Not coll Is Nothing AndAlso coll.Count > 0 Then
			chkFocusOn.Items.Clear()
			Dim li as listItem
			For each ob as contentvalue in coll
				li = New listitem
				li.Value = ob.Key.Id
				li.Text = ob.Title
				If Not String.IsNullOrEmpty(strAssignedFocusOn) Then
					li.Selected = (strAssignedFocusOn.IndexOf("," & ob.Key.id & ",") > -1)
				End if
				chkFocusOn.Items.Add(li)
			Next
		End If


	End Sub
	
	Sub LoadFurtherDetails(ByVal sender As Object, ByVal e As EventArgs)
		If userType.SelectedItem.Value = "65" Then 'Utente partner
			trEmailPartner.Visible=True
			partnerUserName.Text = String.Empty
			rowApprovalCheck.visible= false
			rowFocusOnPartner.visible = true
			loadFocusOn(0)
		
		ElseIf userType.SelectedItem.Value = "78" Then 'Utente di sede
			partnerUserName.Text = String.Empty
			trEmailPartner.Visible=False
			rowApprovalCheck.visible= true
			rowFocusOnPartner.visible = false
		End If
	End Sub 
    
    Function UserExtFields() As ExtraFieldsCollection
        Dim extFieldManager As New ExtraFieldsManager
        Dim extFieldSearcher As New ExtraFieldsSearcher
        extFieldSearcher.Template = ExtraFieldsValue.ExtraFieldsTemplate.UserTemplate
        
        extFieldManager.Cache = False
        Dim coll As ExtraFieldsCollection = extFieldManager.Read(extFieldSearcher)
        
        Return coll
    End Function
    
      
    Sub LoadExtraFields(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim extraFId As Literal = CType(e.Item.FindControl("extFId"), Literal)
        Dim textField As ctlExtraField = CType(e.Item.FindControl("extrF"), ctlExtraField)
        
        If Not extraFId Is Nothing AndAlso Not textField Is Nothing Then
            Select Case textField.TypeControl
                Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                    textField.Text() = ReadExtraFieldValue(extraFId.Text)
                    
                Case ctlExtraField.ControlType.BooleanType
                    Dim extFcheck As String = ReadExtraFieldValue(extraFId.Text)
                   
                    If (extFcheck = "True") Then
                        textField.Checked() = True
                    ElseIf (extFcheck = "False") Then
                        textField.Checked() = False
                    End If
            End Select

        End If
        
        'If e.Item.ItemType = ListItemType.Item Then
    End Sub
         
    Function ReadExtraFieldValue(ByVal extFId As String) As String
        If (extFId <> String.Empty) And (SelectedId <> 0) Then
            Dim extFStoreSearcher As New ExtraFieldsStoreSearcher
        
            extFStoreSearcher.KeyExtraField.Id = CType(extFId, Integer)
            extFStoreSearcher.KeyUser.Id = SelectedId
            
            extFManager.Cache = False
            Dim extrFColl As ExtraFieldsStoreCollection = extFManager.Read(extFStoreSearcher)
            If Not (extrFColl Is Nothing) AndAlso (extrFColl.Count > 0) Then
                Dim extFieldValue As ExtraFieldsStoreValue = extFManager.Read(extFStoreSearcher)(0)
                Return extFieldValue.Value
            End If
        End If
        
        Return Nothing
    End Function
    
    'DA FARE L'UPDATE
    Sub SaveExtraFields(ByVal userId As Integer)
        If (rp_extrafield.Items.Count > 0) Then
                      
            For Each elem As RepeaterItem In rp_extrafield.Items
                Dim extraFId As Literal = CType(elem.FindControl("extFId"), Literal)
                Dim textField As ctlExtraField = CType(elem.FindControl("extrF"), ctlExtraField)
                
                Dim extFValue As New ExtraFieldsStoreValue
                extFValue.KeyExtraField.Id = CType(extraFId.Text, Integer)
                extFValue.KeyUser.Id = userId
                
                Select Case textField.TypeControl
                    Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                        extFValue.Value = textField.Text
                    Case ctlExtraField.ControlType.BooleanType
                        If (textField.Checked = True) Then
                            extFValue.Value = "True"
                        ElseIf (textField.Checked = False) Then
                            extFValue.Value = "False"
                        End If
                End Select
               
                If SelectedId = 0 Then ' create user
                    'If (textField.Text <> "") Then
                    extFManager.Create(extFValue)
                    'End If
                    
                Else 'update user
                    Dim extrFSearcher As New ExtraFieldsStoreSearcher
                    extrFSearcher.KeyExtraField.Id = CType(extraFId.Text, Integer)
                    extrFSearcher.KeyUser.Id = userId
                        
                    extFManager.Cache = False
                    Dim extFielColl As ExtraFieldsStoreCollection = extFManager.Read(extrFSearcher)
                    If Not (extFielColl Is Nothing) AndAlso (extFielColl.Count > 0) Then
                        extFValue.Key = extFielColl(0).Key
                        extFManager.Update(extFValue)
                    Else
                        extFManager.Create(extFValue)
                    End If
                End If
            Next
       
        End If
    End Sub
    
    'cancella una riga 
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim removeItemId As Integer
        Dim risRemove As Boolean
        Dim userValue As New UserValue
        
        'siteAreaManager = New SiteAreaManager
        If sender.CommandName = "DeleteItem" Then
            removeItemId = sender.CommandArgument
            SelectedId = removeItemId
            userValue = userManager.Read(New UserIdentificator(removeItemId))
            If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.User) Then
                risRemove = userManager.Delete(New UserIdentificator(removeItemId))
            End If
            
            If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.HCP) Then
                risRemove = userHCPManager.Delete(New UserIdentificator(removeItemId))
            End If
            
            'LoadArchive()
            BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
            ActivePanelGrid()
            
            'Gestione dei valori di ritorno
            If Not risRemove Then
                CreateJsMessage("Impossible to delete this User")
            End If
        End If
    End Sub
	
	Sub getPartnership(BYVal intUserId as INteger)
		Dim oDt as DataTable = Nothing
		If intUserId > 0 Then
			oDt = LoadPartner()
			If Not oDt is Nothing Then
				Dim query = _
				From partner In oDt.AsEnumerable() _
				where partner("partner_userid") = intUserId
				Select partner

				Dim productsArray = query.ToArray()
				If Not productsArray Is NOthing Then
					rpPartnerData.DataSource = productsArray
					rpPartnerData.DataBind()
				End if
			End If
		End If
	End Sub
	
	Function LoadPartner() As DataTable
		Dim _cacheKey as String = "partnerDataSet"
		Dim oDt as DataTable = Nothing
		'If request("cache") = "false" Then cacheManager.RemoveByKey(_cacheKey)
		cacheManager.RemoveByKey(_cacheKey)
		
		_objCachedDataSet = cacheManager.Read(_cacheKey, 1)
		If _objCachedDataSet Is Nothing Then
			If Not String.IsNullOrEmpty(strQuery) Then
				Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQuery, False)
			
				If Not String.IsNullOrEmpty(query) Then
					Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, Nothing) 'Al posto di Nothing vanno  passati gli SqlParams
					If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
						_objCachedDataSet = ds
						'Cache Content and send to the oputput-------
						CacheManager.Insert(_cacheKey, _objCachedDataSet, 1, 120)
						'--------------------------------------------
					Else
						Return Nothing
					End If
				End If
			End If
		End If	

		Return  _objCachedDataSet.Tables(0)
	End Function	
    
    'popola i campi di testo con le informazioni contenute nello UserValue
    Sub LoadUserDett(ByVal userValue As UserHcpValue)
        If Not userValue Is Nothing Then
			Dim _strUserType As String = userValue.HCPMoreInfo.ToString
			Dim _isDocCheck as Boolean = False
			
			userType.Items.Clear()
			userType.Items.Add(new ListItem("Select Please", 0))
			userType.Items.Add(new ListItem("HCP", 64))
			userType.Items.Add(new ListItem("Media", 63))
			userType.Items.Add(new ListItem("Partner", 65))
			userType.Items.Add(new ListItem("Interno", 78))
			
			rowApprovalCheck.visible = false
			showDraft.checked = false
			
			rowFocusOnPartner.visible = false
			chkFocusOn.Items.Clear()
			
			trEmailPartner.Visible=False
			
			Select case _strUserType.ToLower()
				Case "hcp"
					userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("64"))
					_isDocCheck = True
					
				Case "media"
					userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("63"))
					_isDocCheck = True
					
				Case "partner"
					userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("65"))
					rowFocusOnPartner.visible = true
					loadFocusOn(userValue.Key.Id)
					
					rowFocusOnPartner2.visible = true
					getPartnership(userValue.Key.Id)
					
					trEmailPartner.Visible=True
					partnerUserName.text = userValue.Email.ToString
					
				Case "interno"
					userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("78"))
					rowApprovalCheck.visible= true		
					Me.BusinessProfilingManager.cache=false
					Dim orolecoll As RoleCollection = Me.BusinessProfilingManager.ReadRoles(New UserIdentificator(userValue.Key.Id))
					If Not orolecoll Is Nothing AndAlso orolecoll.Count > 0 Then
						If orolecoll.HasRole(79) Then
							showDraft.checked = True
						Else
							showDraft.checked = false
						End If
					End If
					
				Case else
					userType.SelectedIndex = userType.Items.IndexOf(userType.Items.FindByValue("0"))
			End Select

            'userId.Text = userValue.Key.Id
            userName.Text = userValue.Name
            userSurname.Text = userValue.Surname
            userTitle.Text = userValue.Title
            userAddress.Text = userValue.Address.GetString
            userCap.Text = userValue.Cap
            userCell.Text = userValue.Cellular.GetString
            userCity.Text = userValue.City
            userFax.Text = userValue.Fax.GetString

            If userValue.Geo.Key.Id <> 0 Then
                _txtFatherHidden.Text = userValue.Geo.Key.Id
                '_Father.Text = oGenericUtlity.GetGeoValue(_txtFatherHidden.Text).Description
				_Father.SelectedIndex = _Father.Items.IndexOf(_Father.Items.FindByValue(userValue.Geo.Key.Id))
			End If

            userProvince.Text = userValue.Province
            userPassword.Text = userValue.Password
            userTel.Text = userValue.Telephone.GetString
            userUserName.Text = userValue.UserName.ToString
             
            getUserStatus(userValue)
                
            userGenter.SelectedIndex = userGenter.Items.IndexOf(userGenter.Items.FindByText(userValue.Gender))
            
            If (userValue.CanReceiveNewsletter) Then
                flagNewsletter.Checked = True
            Else
                flagNewsletter.Checked = False
            End If
                              
            If userValue.RegistrationDate.HasValue Then userRegistrationDate.Value = userValue.RegistrationDate.Value
			
			If _isDocCheck Then 'spegnere tutti i campi non modificabili
				userType.Enabled = false
				userName.Disable
				userSurname.Disable
				userTitle.Disable
				userUserName.Disable
				userPassword.Disable
				userGenter.Enabled = false
				userAddress.Disable
				userCap.Disable
				userCity.Disable
				_Father.Enabled = false
				userRegistrationDate.Disable
			Else
				userType.Enabled = false
				userName.Enable
				userSurname.Enable
				userTitle.Enable
				userUserName.Enable
				userPassword.Enable
				userGenter.Enabled = true
				userAddress.Enable
				userCap.Enable
				userCity.Enable
				_Father.Enabled = true
				userRegistrationDate.Disable
			End If
            
        Else
            userName.Text = Nothing
            userSurname.Text = Nothing
            userTitle.Text = Nothing
            userAddress.Text = Nothing
            userCap.Text = Nothing
            userCell.Text = Nothing
            userCity.Text = Nothing
            userFax.Text = Nothing
            userGenter.Enabled = true
            _txtFatherHidden.Text = Nothing
            _Father.SelectedIndex = _Father.Items.IndexOf(_Father.Items.FindByValue(1))
			_Father.Enabled = true
            userType.Enabled = true
			
			userRegistrationDate.Enabled
			
            userPassword.Text = Nothing
            userProvince.Text = Nothing
            userTel.Text = Nothing
            userUserName.Text = Nothing
               
            flagNewsletter.Checked = false
            
            ddluserStatus.SelectedIndex = ddluserStatus.Items.IndexOf(ddluserStatus.Items.FindByValue(1))
            userGenter.SelectedIndex = userGenter.Items.IndexOf(userGenter.Items.FindByValue(1))
            
            btnRegistration_1.Visible = False
            btnRegistration_2.Visible = False
            btnSendMail.Visible = False
            
            lbServicePermission.Items.Clear()
        End If
    End Sub
    
    'popola i campi di testo con le informazioni contenute nello UserHCPValue
    'Sub LoadHCPDett(ByVal userHCPValue As UserHCPValue)
    'End Sub
    
    'Setta tutte le informazioni di un utente User in un UserValue
    Function SetUserDett() As UserValue
        Dim UserValue As New UserValue
        Try          
       
        If (IsUserDettNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            If (IsEmails(username.Text)) Then
                With (UserValue)
                    .Name = userName.Text
                    .Surname = userSurname.Text
                    .Title = userTitle.Text
					
					.Address.Clear()
                    .Address.GetString = userAddress.Text
                    .Cap = userCap.Text
					
					.Cellular.Clear()
                    .Cellular.GetString = userCell.Text
                    .CF = String.Empty
                    .City = userCity.Text
                    .Email.GetString = username.Text
					
					.Fax.Clear()
                    .Fax.GetString = userFax.Text
                    .Gender = userGenter.SelectedItem.Text
                    '.Geo.Key.Id = GetGeoSelection(oLocalization)
                    
                    If _txtFatherHidden.Text <> "" Then
                        .Geo.Key.Id = _txtFatherHidden.Text
                    End If
                        .Province = userProvince.Text
                        .SiteAcquaintedThrougt = String.Empty
                        .SiteAcquaintedThrougtId = String.Empty
                        '.SiteAcquaintedThrougtStatus = String.Empty
                        .StatusActivationDate = Nothing
                    
                        .Password = userPassword.Text
						
						.Telephone.Clear()
                        .Telephone.GetString = userTel.Text
                                         
                        If (flagNewsletter.Checked) Then
                            .CanReceiveNewsletter = True
                        Else
                            .CanReceiveNewsletter = False
                        End If
                      
                        .Status = ddluserStatus.SelectedItem.Value
                        .Birthday = String.Empty
                        .DegreeDate = Nothing
                        .RegistrationDate = userRegistrationDate.Value
                        .StatusDataRichiesta = Nothing
                        .StatusDataAzione = Nothing
						.RegistrationSite.Id = 46
                        .MaritalStatus = 0                                   
                    End With
                Return UserValue
            Else
                CreateJsMessage("Inserire l'email nel giusto formato!")
            End If
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    'Setta tutte le informazioni per un Utente HCP in un UserHCPValue
    Function SetHCPDett(Optional ByVal userHcp as UserHcpValue = Nothing) As UserHCPValue
        
		Dim userHCPValue As UserHCPValue = Nothing
		If userHcp Is Nothing Then 
			userHCPValue = New UserHCPValue
		Else
			userHCPValue = userHcp
		End If 
		
        If (IsUserDettNotNull()) Then 'Controllo sull'obbligatoriet� dei campi del form (da completare)
            If (IsEmails(userUserName.Text) ) Then
                With (userHCPValue)
					If SelectedId = 0 Then 'Create Account
						.Password = userPassword.Text
						.SiteAcquaintedThrougt = String.Empty
						.SiteAcquaintedThrougtId = 0
						'.SiteAcquaintedThrougtStatus = String.Empty
						.StatusActivationDate = Datetime.Now()						
						.Birthday = Nothing
						.DegreeDate = Nothing
						.RegistrationDate = userRegistrationDate.Value
						.StatusDataRichiesta = Datetime.Now()
						.StatusDataAzione = Datetime.Now()
						.RegistrationSite.Id = 46
						.HCPOtherInfo.GetString = String.Empty
						
						Dim _strMoreInfo As String = String.empty
						Select Case userType.selecteditem.value
							Case 64
								_strMoreInfo = "HCP"
								
							Case 63
								_strMoreInfo = "Media"
							
							Case 65
								_strMoreInfo = "Partner"
							
							Case 78
								_strMoreInfo = "Interno"
						End Select
						.HCPMoreInfo.GetString = _strMoreInfo
					End If

					'Propriet� caricate in entrambi i casi---------------
					.Name = userName.Text
					.Surname = userSurname.Text
					.Title = userTitle.Text
					.Address.Clear()
					.Address.GetString = userAddress.Text
					.Cap = userCap.Text
					.Cellular.Clear()
					.Cellular.GetString = userCell.Text
					.CF = String.empty
					.City = userCity.Text
					.Email.Clear()

					'Quando Partner l'email viene gestita da un campo testo diverso
					If userType.selecteditem.value = 65 Then
						.Email.GetString = partnerUserName.Text
					Else
						.Email.GetString = userUsername.Text
					End If

					.Username = userUsername.Text
					.Fax.Clear()
					.Fax.GetString = userFax.Text
					.Gender = userGenter.SelectedItem.Text
					.Geo.Key.Id = _Father.SelectedItem.Value

					.Province = userProvince.Text
					.Telephone.Clear()
                    .Telephone.GetString = userTel.Text
                    If (flagNewsletter.Checked) Then
                        .CanReceiveNewsletter = True
                    Else
                        .CanReceiveNewsletter = False
                    End If
                    .Status = ddluserStatus.SelectedItem.Value
					'----------------------------------------------------
                End With
                Return userHCPValue
            Else
                CreateJsMessage("Inserire l'email nel giusto formato!")
            End If
        Else
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Function
    
    'Gestisce il RecoveryPasword  
    Sub RecoveryPassword(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userValue As New UserValue
        If sender.CommandName = "RecoveryPassword" Then
            If (IsSiteSelected()) Then
				userManager.Cache = false
                userValue = userManager.Read(New UserIdentificator(sender.CommandArgument))
                'omailTemplateValue = getMailTemplate("RecoveryPasswordByDashboard")
                SendMail(userValue)
            Else
                CreateJsMessage("Select a Site!")
            End If
        End If
    End Sub
    
    'Gestisce il Recovery Password 
    Sub RecoveryPassword(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
		userManager.Cache = false
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RecoveryPasswordByEmail")
        SendMail(userValue)
        
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
        'LoadArchive()
    End Sub
    
    Sub ConfirmRegistration(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
              
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RegistrationConfirmed")
        SendMail(userValue)
        
        'ActivePanelGrid()
        'LoadArchive()
    End Sub
    
    Sub RejectRegistration(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
        PrivateData = False
        
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RegistrationRejected")
        SendMail(userValue)
        
        'ActivePanelGrid()
        'LoadArchive()
    End Sub
    
    'Si occupa del recoveri password 
    'Carica il gluisto MailTemplate e spedicse una mail all'utente con la sua Password e UserId
    Sub SendMail(ByVal userValue As UserValue)
        If Not (userValue Is Nothing) Then
            Try
                'omailTemplateValue = getMailTemplate("RecoveryPasswordByDashboard")
                'If Not (omailTemplateValue Is Nothing) Then
                '    mailValue.MailTo.Add(New MailAddress(getFirstElem(userValue.Email)))
                '    mailValue.MailFrom = New MailAddress(omailTemplateValue.SenderAddress, RepleceSender(omailTemplateValue.SenderName))
                '    mailValue.MailBody = RepleceBody(omailTemplateValue.Body, userValue)
                '    mailValue.MailSubject = RepleceSubject(omailTemplateValue.Subject)
                '    mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
                    
                '    If (omailTemplateValue.BccRecipients <> String.Empty) Then
                '        mailValue.MailBcc.Add(New MailAddress(omailTemplateValue.BccRecipients))
                '    End If
                    
                '    If (omailTemplateValue.FormatType = 1) Then
                '        mailValue.MailIsBodyHtml = True
                '    Else
                '        mailValue.MailIsBodyHtml = False
                '    End If
             
                    'invio della mail
                '    systemUtility.SendMail(mailValue)
                '    CreateJsMessage("L'email � stata inviata correttamente!")
                'Else
                '    CreateJsMessage("MailTemplate non attivo! L'email non � stata inviata correttamente!")
                'End If

				Dim replacing As New Dictionary(Of String, String)
				replacing.Add("[USER]", userValue.CompleteName.ToUpper)
				replacing.Add("[PASSWORD]", userValue.Password)
				replacing.Add("[USERID]", userValue.UserName)
				
				Dim htmlTemplate As String = Me.BusinessDictionaryManager.Read("DMP_htmlTemplate", 1, "DMP_htmlTemplate")
				MailHelper.SendMessage(New MailTemplateIdentificator("RecoveryPasswordByDashboard"), 1, htmlTemplate, replacing, False, userValue.Email.ToString)
				CreateJsMessage("L'email � stata inviata correttamente!")
                 
            Catch Err As System.Net.Mail.SmtpFailedRecipientException
                CreateJsMessage("L'email non � stata inviata correttamente!")
            End Try
            
        Else 'se non esiste 
            CreateJsMessage("I dati relativi a questo utente potrebbero non essere esatti!")
        End If
    End Sub
    
    'restituisce tutti i servizi associati ad una community
    Function getService(ByVal community As CommunityIdentificator) As ServiceCollection
        Return Me.BusinessCommunityManager.ReadServices(community)
    End Function
    
    'gestisce Community/Service/Permission
    Sub AddPermission(ByVal sender As Object, ByVal e As EventArgs)
        Dim serviceValue As Integer
        Dim permissionValue As Integer
        Dim serviceText As String
        Dim permissionText As String
        
        If (dwlService.SelectedIndex <> 0) Then
            serviceValue = CType(dwlService.SelectedItem.Value(), Integer)
            serviceText = dwlService.SelectedItem.Text
            
            permissionValue = dwlPermission.SelectedItem.Value()
            permissionText = dwlPermission.SelectedItem.Text
                                         
            lbServicePermission.Items.Add(New ListItem(serviceText & " --> " & permissionText, serviceValue & "|" & permissionValue))
        Else
            strJS = "alert('Select a Service')"
        End If
    End Sub
    
    'gestisce Community/Service/Permission
    Sub DeleteServicePermission(ByVal sender As Object, ByVal e As EventArgs)
        If Not (lbServicePermission.SelectedItem Is Nothing) Then
            lbServicePermission.Items.RemoveAt(lbServicePermission.SelectedIndex)
        Else
            strJS = "alert('Select an Service to remove ')"
        End If
    End Sub
    
    'carica i valori community/service/permission
    Sub LoadServiceUserPermission()
        Dim userIdentificator As New UserIdentificator()
        userIdentificator.Id = userId.Text
                
        If Not (serviceColl Is Nothing) Then
            lbServicePermission.Items.Clear()
            For Each serviceValue As ServiceValue In serviceColl
                'Response.Write("loadService:" & "community" & communitySelected & "user" & userIdentificator.Id & "service" & serviceValue.Key)
                Dim permission As BaseServiceValue.Permission = Me.BusinessCommunityManager.CheckServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), userIdentificator, serviceValue.Key)
              
                Select Case permission
                    Case BaseServiceValue.Permission.Read_Write
                        lbServicePermission.Items.Add(New ListItem(serviceValue.Description & " --> " & "Read_Write", serviceValue.Key & "|" & BaseServiceValue.Permission.Read_Write))
                            
                    Case BaseServiceValue.Permission.Read
                        lbServicePermission.Items.Add(New ListItem(serviceValue.Description & " --> " & "Read", serviceValue.Key & "|" & BaseServiceValue.Permission.Read))
                           
                End Select
               
            Next
        Else
            lbServicePermission.Items.Clear()
        End If
    End Sub
    
    
    'crea le associazioni community/service/permission
    Function SaveServiceUserPermission(ByVal key As UserIdentificator) As Boolean
        Try
            Dim servicePermissionCollection As Object = lbServicePermission.Items
                     
            Dim res As Boolean = Me.BusinessCommunityManager.RemoveAllServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), key)
            If Not (servicePermissionCollection Is Nothing) Then
                If (servicePermissionCollection.Count > 0) Then
                    For Each servicePermissionItem As ListItem In servicePermissionCollection
                        Dim resultStrings() As String = servicePermissionItem.Value.Split("|")
                                          
                        Select Case resultStrings(1)
                            Case 1
                                'Response.Write("Save" & "communitySelected" &  & "resultStrings(0)" & resultStrings(0) & "key" & key.Id)
                                Dim result As Boolean = Me.BusinessCommunityManager.AddServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), resultStrings(0), key, BaseServiceValue.Permission.Read)
                            Case 2
                                'Response.Write("Save" & "communitySelected" & CType(communityId.Text, Integer) & "resultStrings(0)" & resultStrings(0) & "key" & key.Id)
                                Dim result As Boolean = Me.BusinessCommunityManager.AddServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), resultStrings(0), key, BaseServiceValue.Permission.Read_Write)
                        End Select
                                          
                    Next
                End If
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
   
    'popola la dwl delle community nel pannello della ricerca
    Sub BindCommunity()
        Dim communitySearcher As New CommunitySearcher
        Dim communityColl As New CommunityCollection
                      
        communityColl = Me.BusinessCommunityManager.Read(communitySearcher)
       
        utility.LoadListControl(dwlComm, communityColl, "Title", "Key.PrimaryKey")
        dwlComm.Items.Insert(0, New ListItem("--Select--", 0))
     
    End Sub
       
    
    'gestisce la ricerca degli Utente 
    Sub SearchUser(ByVal sender As Object, ByVal e As EventArgs)
		btnClear.visible = true
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
	
    Sub ClearSearchUser(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(Request.URL.ToString, False)
		Response.End
    End Sub	
	
    ''' <summary>
    ''' Caricamento della griglia degli utenti
    ''' Verifica la presenza di criteri di ricerca
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView, Optional ByVal page As Integer = 1)
		userManager.Cache = false
        Dim userSearcher As New UserHCPSearcher
        userSearcher.PageSize = PageSize
        userSearcher.PageNumber = page
        
        If txtId.Text <> "" Then userSearcher.Key = New UserIdentificator(txtId.Text)
        If txtSurname.Text <> "" Then userSearcher.Surname = txtSurname.Text
        If txtName.Text <> "" Then userSearcher.Name = txtName.Text
		
		'Sono sempre HCP dal punto di vista di struttura dati
		userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.HCP
		
		'Filtro sulla tipologia di utenti
        If (drpUserType.SelectedValue > 0) Then
			userSearcher.KeysRoles = drpUserType.SelectedValue.ToString()		
		Else
			userSearcher.KeysRoles = "63,64,65,78"
		End If
		
	   'Filtro sullo stato dell'account
	   If (dwlStatus.SelectedValue <> 0) Then
            userSearcher.Status = dwlStatus.SelectedItem.Value
       End If
        
        If (txtId.Text = "") And (txtSurname.Text = "") And (txtName.Text = "")  And (userSearcher.UserType = 0) And (userSearcher.Status = 0) Then
            BindGrid(objGrid, Nothing)
        Else
            BindGrid(objGrid, userSearcher, page)
        End If
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserHCPSearcher = Nothing, Optional ByVal page As Integer = 1)
        Dim userCollection As UserHcpCollection
  
        'If Searcher Is Nothing Then
        'Searcher = New UserSearcher
        'End If
        
        If Not (Searcher) Is Nothing Then
            Searcher.SortType = SortType
            Searcher.SortOrder = SortOrder
            
            Me.BusinessUserHCPManager.Cache = False
			Me.BusinessUserManager.Cache = false
            userCollection = Me.BusinessUserHCPManager.Read(Searcher)
            
            'If Not userCollection Is Nothing Then
            '    userCollection.Sort(SortType, SortOrder)
            'End If
            
            sectionTit.Visible = True
            
            'carica il controllo per la paginazione
            LoadPager(userCollection, page)
            
            objGrid.DataSource = userCollection
            objGrid.DataBind()
        Else
            'ActivePanelGrid()
        End If
    End Sub
         
    'gestiste la paginazione
    Sub LoadPager(ByRef userColl As UserHCPCollection, Optional ByVal Page As Integer = 1)
        'txtActPage2.Value = Page
        'Response.Write(Not userColl Is Nothing)
        'rpPager.DataSource = Nothing
        If Not userColl Is Nothing AndAlso userColl.Count > 0 Then
            Dim pageList As New ArrayList
            Dim i As Integer = 0
            
            Dim ite As New ListItem
            Actualpage.value = Page
            
            ite.Text = "&laquo;"
            ite.Value = "1"
            pageList.Add(ite)
            
            For i = 0 To userColl.PagerTotalNumber - 1
                If (i + 1 > (Page - NPageVis)) AndAlso (i + 1 < (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = i + 1
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("add" & ite.Value)
                ElseIf (i + 1 = (Page - NPageVis)) Or (i + 1 = (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = "..."
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("addelse" & ite.Value)
                End If
            Next
            
            ite = New ListItem
            ite.Text = "&raquo;"
            ite.Value = userColl.PagerTotalNumber
            pageList.Add(ite)
            
            'Response.Write("tot" & pageList.Count)
            If i > 1 Then
                rpPager.DataSource = pageList
                rpPager.DataBind()
                'Response.Write("popolo" & pageList.Count)
                'rptContactPagerBottom.DataSource = pageList
            Else
                rpPager.DataSource = Nothing
                rpPager.DataBind()
                ' Response.Write("popolo Nothing")
                'rptContactPagerBottom.DataSource = Nothing
            End If
        
            'Dim nDa As Integer = ((userColl.PagerTotalNumber - 1) * userColl.PagerTotalCount) + 1
            'Dim nA As Integer = (nDa + userColl.Count) - 1
            'Dim nTot As Integer = userColl.PagerTotalCount
            'lblContactpager.Text = nDa.ToString & " - " & _
            '(nA).ToString & " on " & _
            '(nTot).ToString
        Else
            rpPager.DataSource = Nothing
            rpPager.DataBind()
        End If
             
    End Sub
    
    'restituisce il Tipo Utente
    Sub getUserType(ByVal userValue As UserValue)
        'ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(ddlUserType.Items.FindByValue(userValue.UserType))
    End Sub
    
    'restituisce lo Status dell'utente
    Sub getUserStatus(ByVal userValue As UserValue)
        ddluserStatus.SelectedIndex = ddluserStatus.Items.IndexOf(ddluserStatus.Items.FindByValue(userValue.Status))
    End Sub
              
    'recupera tutti i ruoli associati ad un Utente
    Function GetUserRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        profilingManager.Cache = False
        Return profilingManager.ReadRoles(key)
    End Function
       
    'recupera tutti i profili associati ad un Utente
    Function GetUserProfilesValue(ByVal id As Int32) As ProfileCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        profilingManager.Cache = False
        Return profilingManager.ReadProfiles(key)
    End Function
    
    'recupera tutti i gruppi associati ad un Utente
    Function GetUserGroupsValue(ByVal id As Int32) As UserGroupCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        
        userGroupManager.Cache = False
        Return userGroupManager.ReadUserRelated(key)
    End Function
   
    'recupera tutti i context associati ad un Utente
    Function GetUserContextValue(ByVal id As Integer) As ContextCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        
        ContextManager.Cache = False
        Return ContextManager.ReadContextUser(key, New GroupIdentificator())
    End Function
    
    'recupera tutti i profili associati ad un Utente
    Function GetUserCommunityValue(ByVal id As Int32) As CommunityCollection
        Dim communitySearcher As New CommunitySearcher
        
        If id = 0 Then Return Nothing
        'Dim key As New UserIdentificator
        'key.Id = id
        communitySearcher.KeyUser.Id = id
        Me.BusinessCommunityManager.Cache = False
        Return Me.BusinessCommunityManager.Read(communitySearcher)
    End Function
       
    'recupera il Mail Template Corretto
    'dati la Lingua, il Nome e il Sito
    Function getMailTemplate(ByVal templateName As String) As MailTemplateValue
        omailTemplateSearcher.Key.KeyLanguage = New LanguageIdentificator(1)
        omailTemplateSearcher.KeySite.Id = 46
        omailTemplateSearcher.Key.MailTemplateName = templateName
        omailTempalteManager.Cache = False
        Dim coll As MailTemplateCollection = omailTempalteManager.Read(omailTemplateSearcher)
		
        If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
            omailTemplateValue = coll(0)
            Return omailTemplateValue
        End If
                      
        Return Nothing
    End Function
    
    'fa il replace di alcuni parametri nel Body del MailTemplate
    Function RepleceBody(ByVal body As String, ByVal userValue As UserValue) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        Dim completeName As String = userValue.Name & " " & userValue.Surname
        If (PrivateData) Then
            body = body.Replace("[PASSWORD]", userValue.Password).Replace("[USERID]", userValue.Username).Replace("[USER]", userValue.CompleteName)
        End If
        
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
        Else
            siteLabel = ""
        End If
       
        Return body.Replace("[UTENTE]", completeName).Replace("[SITE]", siteLabel)
    End Function
    
    'fa il replace di alcuni parametri nel Subject del MailTemplate
    Function RepleceSubject(ByVal subject As String) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
        Else
            siteLabel = ""
        End If
        
        Return subject.Replace("[SITE]", siteLabel)
    End Function
    
    'fa il replace di alcuni parametri nel Sender del MailTemplate
    Function RepleceSender(ByVal subject As String) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
           
        Else
            siteLabel = ""
        End If
       
        Return subject.Replace("[SITE]", siteLabel)
    End Function
    
     
    'controlla l'evento che si scatena quando si seleziona un Tipo Utente
    Sub SelectChanged(ByVal Sender As Object, ByVal e As EventArgs)
        'If ddlUserType.SelectedItem.Text = "HCP" Then
            'ActivateHCPElements()
            'LoadHCPDett(Nothing)
        'ElseIf ddlUserType.SelectedItem.Text = "User" Then
        '    EnableHCPElements()
        'End If
    End Sub
    
    'restituisce la label del Site dato il suo id
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
   
    'restituisce il primo elemento 
    Function getFirstElem(ByRef mutiField As Healthware.HP3.Core.Base.ObjectValues.MultiFieldType) As String
        Return mutiField.Item(0)
    End Function
    
    'restituisce il sito seletionato
    'Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteValue
    '    Dim siteAreaValue As New SiteAreaValue
    '    Dim siteValue As New SiteValue
    '    Dim siteManager As New SiteManager
    '    Dim siteSearcher As New SiteSearcher
    '    Dim oSiteAreaCollection As Object = olistItem.GetSelection
        
    '    If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
    '        siteAreaValue = oSiteAreaCollection.item(0)
    '        siteSearcher.KeySiteArea.Id = siteAreaValue.Key.Id
    '        siteValue = siteManager.Read(siteSearcher)(0)
            
    '        If Not (siteValue Is Nothing) Then
    '            Return siteValue
    '        End If
    '    End If
               
    '    Return Nothing
    'End Function
    
    'carica i servizi della community nella dwl
    Function LoadService(ByVal communityId As Integer, ByVal communityLanguageId As Integer) As Boolean
        
        Dim _return As Boolean = False
        
        Dim communityValue As CommunityValue = Me.BusinessCommunityManager.Read(New CommunityIdentificator(communityId, communityLanguageId))
        
        If Not (communityValue Is Nothing) Then
            serviceColl = Me.BusinessCommunityManager.ReadServices(communityValue.KeyCommunity)
            
            If Not (serviceColl Is Nothing) Then
                If (serviceColl.Count > 0) Then
                    _return = True
                End If
            End If
            
            utility.LoadListControl(dwlService, serviceColl, "Description", "Key")
            dwlService.Items.Insert(0, New ListItem("--Select--", 0))
        End If
        
        Return _return
    End Function
    
    'restiuisce il Geo selezionato
    Function GetGeoSelection(ByVal oLocalization As ctlLocalization) As Integer
        Dim geoColl As New GeoCollection
        Dim geoValue As New GeoValue
        
        geoColl = oLocalization.GeoList()
        If Not (geoColl Is Nothing) AndAlso (geoColl.Count > 0) Then
            geoValue = geoColl(0)
            If Not (geoValue Is Nothing) Then
                Return geoValue.Key.Id
            End If
        End If
        
        Return 0
    End Function
    
    'controlla che i campi non siano nulli
    Function IsUserDettNotNull() As Boolean
        Dim _return As Boolean = False
        
        'If Not (userRequestDate.Value.HasValue) Then strName = "Request Date"
        'If Not (userActionDate.Value.HasValue) Then strName = "Action Date"
        'If Not (userDegreeDate.Value.HasValue) Then strName = "DegreeDate"
        'If Not (userRegistrationDate.Value.HasValue) Then strName = "Registration Date"
       
        'txtSite
        'If (txtHideSite.Text = String.Empty) Then strName = "Site"
        'If Not (ctlProfiles.GetSelection.Count > 0) Then strName = "Profile"
        'If (userAddress.Text = String.Empty) Then strName = "Address"
        'If (userCap.Text = String.Empty) Then strName = "Cap"
        'If (userCity.Text = String.Empty) Then strName = "City"
        'If (userProvince.Text = String.Empty) Then strName = "Province"
        'If (userTel.Text = String.Empty) Then strName = "Tel"
        
        'If (userUserName.Text = String.Empty) Then strName = "UserName"
        'If (userFax.Text = String.Empty) Then strName = "Fax"
        'If (userCell.Text = String.Empty) Then strName = "Cell"
        'If Not (userBirthday.Value.HasValue) Then strName = "Birthday"
        
        'If ((userName.Text <> String.Empty) And (userSurname.Text <> String.Empty) And (userCap.Text <> String.Empty) And (userCity.Text <> String.Empty) And (userTel.Text <> String.Empty) And (userEmail.Text <> String.Empty) And (userProvince.Text <> String.Empty) And (ctlProfiles.GetSelection.Count > 0) And (txtHideSite.Text <> String.Empty)) Then
        '    _return = True
        'End If
		
		If userType.SelectedItem.Value = "0" Then strName = "User Type\n"
		If (userName.Text = String.Empty) Then strName += "Name\n"
        If (userSurname.Text = String.Empty) Then strName += "Surname\n"
        If (userUserName.Text = String.Empty) Then strName += "Email / UserName\n"
        
		If String.IsNullOrEmpty(strName) Then Return True
        
        Return _return
    End Function
    
    'controlla che i campi non siano nulli
    Function IsHCPDettNotNull() As Boolean
        Dim _return As Boolean = False
        
        'If (hcpRegistration.Text = String.Empty) Then strName = "HCP Registration"
        'If (hcpWard.Text = String.Empty) Then strName = "HCP Ward"
        'If (hcpStructure.Text = String.Empty) Then strName = "HCP Structure"
        'If (hcpEmail.Text = String.Empty) Then strName = "HCP Email"
        'If (hcpFax.Text = String.Empty) Then strName = "HCP Fax"
        'If (hcpProvince.Text = String.Empty) Then strName = "HCP Province"
        'If (hcpCell.Text = String.Empty) Then strName = "HCP Cell"
        'If (hcpTel.Text = String.Empty) Then strName = "HCP Tel"
        'If (hcpCity.Text = String.Empty) Then strName = "HCP City"
        'If (hcpCap.Text = String.Empty) Then strName = "HCP Cap"
        'If (hcpAddress.Text = String.Empty) Then strName = "HCP Address"
        
          
        'If ((hcpAddress.Text <> String.Empty) And (hcpCap.Text <> String.Empty) And (hcpCity.Text <> String.Empty) And (hcpTel.Text <> String.Empty) And (hcpProvince.Text <> String.Empty)) And (hcpEmail.Text <> String.Empty) And (hcpStructure.Text <> String.Empty) And (hcpWard.Text <> String.Empty) And (hcpRegistration.Text <> String.Empty) Then
        '    _return = True
        'End If
        
        Return _return
    End Function
    
    ''controlla che tutte le mail inserite siano in un formato corretto
    Function IsEmails(ByVal emails As String) As Boolean
	
		If userType.SelectedItem.Value = 65 Then Return True
	
	
        Dim EmailsArray() As String = emails.Split("|")
        Dim elem As String
        Dim _result As Boolean
        
        For Each elem In EmailsArray
            _result = IsEMail(elem)
            If Not (_result) Then
                CreateJsMessage("Email is not in a correct format ")
                Return False
            End If
        Next
        
        Return True
    End Function
    
    'controlla che la email sia nel corretto formato
    Function IsEMail(ByVal email As String) As Boolean
        Dim emailFormat As String = "([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})"
        Return (Regex.IsMatch(email, emailFormat))
    End Function
    
    'True, se � stato selezionato un Sito
    'False, Altrimenti
    Function IsSiteSelected() As Boolean
        Dim objQs As New ObjQueryString
        
        If (objQs.Site_Id <> 0) Then 'se � stato selezionato un sito 
            Return True
        Else
            Return False
        End If
        
    End Function
    
    'restituisce la giusta icona 
    'per il recovery password
    Function getIcon() As String
        Dim str_out As String
        
        If (IsSiteSelected()) Then 'se � stato selezionato un sito 
            str_out = "iMail_on"
        Else
            str_out = "iMail_off"
        End If
        
        Return str_out
    End Function
       
    'restituisce lo status dell'utente
    Function getStatus(ByVal userValue As UserValue) As String
        Dim out_str As String = ""
        
        Select Case userValue.Status
            Case 1
                out_str = "Active"
            Case 2
                out_str = "Pending"
            Case 3
                out_str = "No Active"
            Case 4
                out_str = "Disabled"
            Case 5
                out_str = "Deleted"
        End Select
        
        Return out_str
    End Function
     
    'popola la dwl delle speciality 
    Sub BindSpeciality()
        'Dim contextSearcher As New ContextSearcher
       
        'contextSearcher.KeyContextGroup.Id = Integer.Parse(ConfigurationManager.AppSettings("GroupContextUT"))
        'contextSearcher.KeyFather.Id = 0
        
        'Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        'dwlHcpSpecialty.Items.Clear()
        'utility.LoadListControl(dwlHcpSpecialty, contextColl, "Description", "Key.Id")
        'dwlHcpSpecialty.Items.Insert(0, New ListItem("Select Specialty", -1))
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        panelCommunity.Visible = False
        'panelSegmentation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        divExtraFields.Visible = False
        pnlDett.Visible = True
        pnlGrid.Visible = False
        panelCommunity.Visible = False
        'panelSegmentation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    Sub ActivePanelCommunity()
        panelCommunity.Visible = True
        'panelSegmentation.Visible = False
        pnlGrid.Visible = False
    End Sub
    
    Sub ActivePanelSegmentation()
        'panelSegmentation.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    
    'Disattiva gli elementi per un utente HCP
    Sub EnableHCPElements()
        divHCP.Visible = False
    End Sub
           
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
     
    ''' <summary>
    ''' Ordinemento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListUsers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        'gdw_usersList.PageIndex = 0
        'impostare il PageNumber 
        'Dim Page As Integer = 1
        
        Select Case e.SortExpression
            Case "Key.Id"
                If SortType <> UserGenericComparer.SortType.ByKey Then
                    SortType = UserGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Surname"
                If SortType <> UserGenericComparer.SortType.BySurName Then
                    SortType = UserGenericComparer.SortType.BySurName
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
                
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
        
    'Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    sender.PageIndex = e.NewPageIndex
    '    BindWithSearch(sender, e.NewPageIndex)
    'End Sub
       
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Sub linkPager_PageIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        'evento scatenato dal paginatore degli utenti filtrati
        BindWithSearch(gdw_usersList, sender.CommandArgument)
        ' LoadUser(CInt(sender.CommandArgument), ctxFilter.Value, txtSortOrder.Value, txtOrderType.Value)
    End Sub
    
    
    Sub ItemCommand(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim param = s.commandArgument.split(",")
        Dim commandType As String = param(0)
        Dim contentID As String = param(1)
        ' Dim contentLangId As String = param(2)

        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
      
        wr.KeycontentType = New ContentTypeIdentificator("HP3", commandType)
        '        objQs.Language_id = contentLangId
        'objQs = Int32.Parse(contentID)
        wr.customParam.append(objQs)
        wr.customParam.append("us", contentID.ToString())
        Response.Redirect(mPage.FormatRequest(wr))
    End Sub
    
    
    Function tooltipinformation(ByVal userid As Integer) As String
        
        Dim so As New UserSearcher
        Dim userStatusSearch As New StatusSiteSearcher
        userStatusSearch.User.Id = userid
        userStatusSearch.StatusId = 1
        Me.BusinessUserManager.Cache = False
        Dim coll As StatusSiteCollection = Me.BusinessUserManager.ReadStatus(userStatusSearch)
        Dim toolMess As String
        toolMess = "Il seguente utente ha zero associazioni attive"
        If (Not coll Is Nothing AndAlso coll.Any AndAlso coll(0).KeySiteArea.Id > 0) Then
            
            toolMess = "Il seguente utente ha" & " " & coll.Count & " " & "associazioni attive"
            Return toolMess
        Else
            
            Return toolMess
        End If
    End Function
	
	
	Sub onGwDataBound (ByVal sender As Object, ByVal e As GridViewRowEventArgs)
		If e.Row.RowType = DataControlRowType.DataRow Then
			Dim imgBtn as ImageButton = Directcast(e.Row.FindControl("lnkSendMail"), ImageButton)
			Dim imgBtnAct as ImageButton = Directcast(e.Row.FindControl("lnkActive"), ImageButton)
			Dim imgBtnNoAct as ImageButton = Directcast(e.Row.FindControl("lnkNoActive"), ImageButton)
			If e.Row.Cells(3).Text.ToLower() = "media" OR e.Row.Cells(3).Text.ToLower() = "hcp" Then imgBtn.Visible = false
			If DataBinder.Eval(e.Row.DataItem, "Status") = "1" Then 
				imgBtnAct.Visible = true
				imgBtnNoAct.Visible = false
			Else
				imgBtnAct.Visible = false
				imgBtnNoAct.Visible = true	
			End If
		End If
	End Sub
	
	
	Sub delItem(ByVal sender As Object, ByVal e As EventArgs)
		Dim partner_id = sender.CommandArgument
		Dim query As String = Dompe.Helpers.Sql.ReadQuery("deletePartnerData", False)
		Dim mIdNuovo as Integer = 0
		If Not String.IsNullOrEmpty(query) Then
			Dim args = New List(Of SqlParameter)
			args.Add(New SqlParameter("@partner_id", partner_id))	

			Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, args.ToArray()) 'Al posto di Nothing vanno  passati gli SqlParams
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dt = ds.Tables(0)
				If Not dt.Rows Is Nothing AndAlso 0 <> dt.Rows.Count Then
					Dim row = dt.Rows(0)
					mIdNuovo = row(0)
					If mIdNuovo > 0 Then 
						response.redirect(Request.Url.ToString(), False)
					End If
				End If
			End If
		End If		
	End Sub
</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
 <asp:HiddenField  ID="Actualpage"  runat="server" Value="1"/>
<asp:Panel id="pnlGrid" runat="server" visible="false">
 <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style=" margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <%--FILTRI SUGLI UTENTI--%>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Surname</strong></td>
                    <td><strong>Name</strong></td>
                    <td><strong>User Type</strong></td>
                    <td><strong>Status</strong></td> 
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat ="server" id="txtSurname" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                    <td><HP3:Text runat ="server" id="txtName" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                    <td>    
                        <asp:dropdownlist ID="drpUserType" runat="server">
                          <asp:ListItem Selected= "True" Text="Select Type" Value="0"></asp:ListItem>
						  <asp:ListItem Text="Utenti HCP" Value="64"></asp:ListItem>
						  <asp:ListItem Text="Utenti Media" Value="63"></asp:ListItem>
						  <asp:ListItem Text="Utenti Partner" Value="65"></asp:ListItem>
						  <asp:ListItem Text="Utenti Interni" Value="78"></asp:ListItem>
                        </asp:dropdownlist>
						<%-- <asp:ListItem visible="false" Text="User" Value="1"></asp:ListItem>
                          <asp:ListItem visible="false" Text="HCP" Value="2"></asp:ListItem>
                          <asp:ListItem visible="false" Text="SA" Value="3"></asp:ListItem> --%>
                    </td>
                     <td>
                         <asp:dropdownlist id="dwlStatus" runat="server">
							<asp:ListItem  Text="--Select Status--" Value="0"></asp:ListItem>
                            <asp:ListItem  Text="Active" Value="1"></asp:ListItem>
                            <asp:ListItem  Text="Not active" Value="3"></asp:ListItem>
                         </asp:dropdownlist>
						<%--
						<asp:ListItem  Text="Pending" Value="2"></asp:ListItem>
						<asp:ListItem  Text="Disabled" Value="4"></asp:ListItem>
						<asp:ListItem  Text="Deleted" Value="5"></asp:ListItem>
						--%>
                    </td> 
                </tr>
                <!--<tr>
                    <td><strong>Community</strong></td> 
                    <td><asp:dropdownlist id="dwlComm" runat="server"/></td>  
                </tr>-->
                <tr>
                    <td colspan="5">
						<asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchUser" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
						<asp:button visible="false" runat="server" CssClass="button" ID="btnClear" onclick="ClearSearchUser" Text="Clear" style="margin-top:10px;padding-left:16px;width:80px;background-repeat:no-repeat;background-position:1px 1px;"/>
					</td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
<%--FINE--%>    


 <%--<div style="margin-bottom:10px">
    <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>--%>
 
 <asp:Label CssClass="title" runat="server"  visible="false" id="sectionTit">Users List</asp:Label>
 <div style ="float:right; text-align:right;">
     <asp:Repeater ID="rpPager" runat="server" Visible="true" >
        <ItemTemplate>
          
        <%#IIf(Actualpage.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>
                        <asp:LinkButton ID="linkPager" runat="server" OnClick="linkPager_PageIndex" 
                        Visible='<%#iif (Actualpage.value=DataBinder.Eval(Container.DataItem, "value"),"false","true") %>' 
                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "value")%>' ><%#DataBinder.Eval(Container.DataItem, "text")%></asp:LinkButton>
        </ItemTemplate>
     </asp:Repeater>
</div>
 
 <asp:label  id="lblContactpager" runat="server"></asp:label>
 
 <asp:gridview id="gdw_usersList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                OnSorting="gridListUsers_Sorting"
                emptydatatext="No item available"
				onrowdatabound="onGwDataBound">   
              <Columns>
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Key.Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%> </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField HeaderStyle-Width="30%" DataField ="Name" HeaderText ="Name" />
                <asp:BoundField HeaderStyle-Width="30%" DataField ="Surname" HeaderText ="Surname" SortExpression="Surname" />
				<asp:BoundField HeaderStyle-Width="15%" DataField ="HCPMoreInfo" HeaderText ="User type" SortExpression="HCPMoreInfo" />
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Status">
					<ItemTemplate><%#getStatus(Container.DataItem)%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit Account" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Remove Account" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
 						<asp:ImageButton ID="lnkActive" runat="server" ToolTip ="Active Account, click here to deactivate it" ImageUrl="~/hp3Office/HP3Image/ico/content_active.gif" OnClick="EditRow" CommandName ="ActiveUser" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkNoActive" runat="server" ToolTip ="Deactivated Account, click here to activate it" ImageUrl="~/hp3Office/HP3Image/ico/content_noactive.gif" OnClick="EditRow" CommandName ="NoActiveUser" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <!--<asp:ImageButton ID="lnkSendMail" runat="server" ToolTip ="Recovery Password" ImageUrl='<%#"~/hp3Office/HP3Image/ico/" & getIcon() & ".gif"%>'  OnClick="RecoveryPassword" CommandName ="RecoveryPassword" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>-->
                        <asp:ImageButton visible="false" ID="lnkCommunity" runat="server" ToolTip ="Community" ImageUrl="~/hp3Office/HP3Image/ico/Forum.gif"   OnClick = "LoadCommunityControl" CommandName ="Community" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton visible="false" ID="Siteassociation" runat="server" ToolTip = <%#tooltipinformation(Container.Dataitem.Key.Id) %> ImageUrl="~/hp3Office/HP3Image/ico/content_content.gif"    onClick="ItemCommand"  CommandArgument ='<%#"UserSiteSt," & Container.Dataitem.Key.Id.toString() %>'/>

						<%--<asp:ImageButton ID="lnkSegmentation" runat="server" ToolTip ="Segmentation" ImageUrl="~/hp3Office/HP3Image/Ico/content_context.gif"  OnClick="Segmentation" CommandName ="Segmentation" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                        <%--<img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageUserUpload.aspx?idUser=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','CoverImage','width=550,height=350')" style="cursor:pointer" title="Image Cover" alt="" />--%>
					</ItemTemplate>
				</asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button runat="server" ID="btnSave" onclick="UpdateRow" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <div>
        <asp:Button id ="btnSendMail" runat="server" Text="Recovery Password" OnClick="RecoveryPassword"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
        <asp:Button id ="btnRegistration_1" runat="server" Text="Confirm Registration" OnClick="ConfirmRegistration"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
        <asp:Button id ="btnRegistration_2" runat="server" Text="Reject Registration" OnClick="RejectRegistration"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%" >
        <tr>
            <td></td>
            <td><HP3:Text runat ="server" id="userId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
            <td><HP3:Text runat ="server" id="communityId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
            <td><HP3:Text runat ="server" id="communityLanguageId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
        </tr>
        <tr>
            <td width="220px"><a href="#"><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a>User Type</td>
            <td>
                <asp:DropDownList autopostback="true" OnSelectedIndexChanged="LoadFurtherDetails" id="userType" runat="server"></asp:DropDownList>			
					<%--
			        <asp:ListItem selected="selected" Text="Select Please" Value="0"></asp:ListItem>
                    <asp:ListItem Text="HCP" Value="64"></asp:ListItem>
					<asp:ListItem Text="Media" Value="63"></asp:ListItem>
					<asp:ListItem Text="Partner" Value="65"></asp:ListItem>
					<asp:ListItem Text="Interno" Value="78"></asp:ListItem>
					--%>
			</td>
        </tr>
        <tr id="rowApprovalCheck" runat="server" visible="false">
            <td width="220px">Visualizza Draft</td>
            <td><asp:checkbox id="showDraft" runat="server" /></td>
        </tr>
        <tr id="rowFocusOnPartner" runat="server" visible="false">
            <td width="220px">Focus On</td>
            <td><asp:CheckBoxList ID="chkFocusOn" runat="server" /></td>
        </tr>	
        <tr id="rowFocusOnPartner2" runat="server" visible="false">
            <td width="220px">&nbsp;</td>
            <td>
				<asp:repeater id="rpPartnerData" runat="server">
					<HeaderTemplate>
					<div style="margin:10px 0;"><strong>ASSOCIAZIONI PARTNER:</strong></div>

					
					<style>
					#tableA td{border:solid 1px #000;padding:3px 5px;font-size:12px;}
					</style>					
					<table class="form" id="tableA">
					<tr style="background-color:#AAA;color:#FFF">
					<td>Country</td>
					<td>Direct Partnet</td>
					<td>Indirect Partner</td>
					<td>Area Terapeutica</td>
					<td>Brand</td>
					<td>Principio Attivo</td>
					<td>&nbsp;</td>
					</tr>
					</HeaderTemplate>
					<itemtemplate>
					<tr>
						<td><%#Container.DataItem("partner_country")%></td>
						<td><%#Container.DataItem("partner_directPartner")%></td>
						<td><%#Container.DataItem("partner_indirectPartner")%></td>
						<td><%#Container.DataItem("partner_areaterapeutica")%></td>
						<td><%#Container.DataItem("partner_brand")%></td>
						<td><%#Container.DataItem("partner_principioattivo")%></td>
						<td>
						[<a href="#" onclick="javascript:window.open('/hp3Office/Popups/popPartnership.aspx?userId=<%#Container.DataItem("partner_userid")%>&row=<%#Container.DataItem("partner_id")%>','','width=600,height=500,scrollbars=yes');return false;">Edit</a>]
						&nbsp;
						[<asp:linkbutton runat="server" onclick="delItem" id="hlkDel" commandArgument='<%#Container.DataItem("partner_id")%>' runat="server">Delete</asp:linkbutton>]
						</td>
					</tr>
					</itemtemplate>
					<FooterTemplate>
					</table>
					<div>&nbsp;</div>
					<div style="padding:10px 0;margin:10px 0;text-align:center;">
					<a style="background-color:#AAAAAA;color:#FFF;padding:8px 10px;border:1px solid #000;text-decoration:none;" href="javascript:window.open('/hp3Office/Popups/popPartnership.aspx?row=0','','width=600,height=500,scrollbars=yes');">NUOVA ASSOCIAZIONE PARTNER</a>
					</div>					
					</FooterTemplate>					
				</asp:repeater>
			</td>
        </tr>
		
        <tr>
            <td width="220px"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpUserTName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text runat ="server" id="userName" TypeControl ="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserSurname&Help=cms_HelpUserSurname&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserSurname", "Surname")%></td>
            <td><HP3:Text runat ="server" id="userSurname" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr>
		
		<tr id="trEmailPartner" runat="server" visible="false">
            <td>Partner Email</td>
            <td><HP3:Text runat ="server" id="partnerUserName"  TypeControl ="TextBox" style="width:150px" MaxLength="50"/></td>
        </tr>
		
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserName&Help=cms_HelpUserUName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserNamea", "Email / Username")%></td>
            <td><HP3:Text runat ="server" id="userUserName"  TypeControl ="TextBox" style="width:150px" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPassword&Help=cms_HelpUserPassword&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPassword", "Password")%></td>
            <td><HP3:Text runat ="server" id="userPassword" TypeControl ="TextBox" style="width:150px" MaxLength="50"/></td>
        </tr>		
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserGender&Help=cms_HelpUserGender&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserGender", "Gender")%></td>
            <td>
                <asp:DropDownList id="userGenter" runat="server"  Width="50px">
                    <asp:ListItem  Selected= "True" Text="M" Value="1"></asp:ListItem>
                    <asp:ListItem Text="F" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpUserTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
            <td><HP3:Text runat ="server" id="userTitle" TypeControl ="TextBox" style="width:100px" MaxLength="15"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserAddress&Help=cms_HelpUserAddress&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserAddress", "Address")%>*</td>
            <td><HP3:Text runat ="server" id="userAddress" TypeControl ="TextBox" style="width:300px" MaxLength="400"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCap&Help=cms_HelpUserCap&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCap", "Cap")%></td>
            <td><HP3:Text runat ="server" id="userCap" TypeControl ="TextBox" style="width:60px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCity&Help=cms_HelpUserCity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCity", "City")%></td>
            <td><HP3:Text runat ="server" id="userCity" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProvince&Help=cms_FormUserProvince&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProvince", "Province")%></td>
            <td><HP3:Text runat ="server" id="userProvince" TypeControl ="TextBox" style="width:30px" MaxLength="2"/></td>
        </tr> 
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLocalization&Help=cms_HelpLocalization&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> Country</td>
            <td><%--<HP3:Localization id="oLocalization" runat="server" TypeControl="AccContentGeo" />--%>
            <table width="100%">    
                    <tr>
                        <td style="width:300px">
                            <asp:textbox ID="_txtFatherHidden" runat="Server" style="display:none"/>
							<asp:dropdownlist ID="_Father" runat="server">
								<asp:ListItem Text="Afghanistan" value="16"></asp:listitem>
								<asp:ListItem Text="Albania" value="17"></asp:listitem>
								<asp:ListItem Text="Algeria" value="18"></asp:listitem>
								<asp:ListItem Text="American Samoa" value="19"></asp:listitem>
								<asp:ListItem Text="Andorra" value="20"></asp:listitem>
								<asp:ListItem Text="Angola" value="21"></asp:listitem>
								<asp:ListItem Text="Anguilla" value="22"></asp:listitem>
								<asp:ListItem Text="Antarctica" value="23"></asp:listitem>
								<asp:ListItem Text="Antigua and Barbuda" value="24"></asp:listitem>
								<asp:ListItem Text="Argentina" value="25"></asp:listitem>
								<asp:ListItem Text="Armenia" value="26"></asp:listitem>
								<asp:ListItem Text="Aruba" value="27"></asp:listitem>
								<asp:ListItem Text="Ascension Island" value="28"></asp:listitem>
								<asp:ListItem Text="Australia" value="13"></asp:listitem>
								<asp:ListItem Text="Austria" value="29"></asp:listitem>
								<asp:ListItem Text="Azerbaijan" value="30"></asp:listitem>
								<asp:ListItem Text="Bahamas" value="31"></asp:listitem>
								<asp:ListItem Text="Bahrain" value="32"></asp:listitem>
								<asp:ListItem Text="Bangladesh" value="33"></asp:listitem>
								<asp:ListItem Text="Barbados" value="34"></asp:listitem>
								<asp:ListItem Text="Belarus" value="35"></asp:listitem>
								<asp:ListItem Text="Belgium" value="36"></asp:listitem>
								<asp:ListItem Text="Belize" value="37"></asp:listitem>
								<asp:ListItem Text="Benin" value="38"></asp:listitem>
								<asp:ListItem Text="Bermuda" value="39"></asp:listitem>
								<asp:ListItem Text="Bhutan" value="40"></asp:listitem>
								<asp:ListItem Text="Bolivia" value="41"></asp:listitem>
								<asp:ListItem Text="Bosnia and Herzegovina" value="42"></asp:listitem>
								<asp:ListItem Text="Botswana" value="43"></asp:listitem>
								<asp:ListItem Text="Bouvet Island" value="44"></asp:listitem>
								<asp:ListItem Text="Brazil" value="45"></asp:listitem>
								<asp:ListItem Text="British Indian Ocean Territory" value="46"></asp:listitem>
								<asp:ListItem Text="Brunei" value="47"></asp:listitem>
								<asp:ListItem Text="Bulgaria" value="48"></asp:listitem>
								<asp:ListItem Text="Burkina Faso" value="49"></asp:listitem>
								<asp:ListItem Text="Burundi" value="50"></asp:listitem>
								<asp:ListItem Text="Cambodia" value="51"></asp:listitem>
								<asp:ListItem Text="Cameroon" value="52"></asp:listitem>
								<asp:ListItem Text="Canada" value="53"></asp:listitem>
								<asp:ListItem Text="Cape Verde" value="54"></asp:listitem>
								<asp:ListItem Text="Cayman Islands" value="55"></asp:listitem>
								<asp:ListItem Text="Central African Republic" value="56"></asp:listitem>
								<asp:ListItem Text="Chad" value="57"></asp:listitem>
								<asp:ListItem Text="Chile" value="58"></asp:listitem>
								<asp:ListItem Text="China" value="59"></asp:listitem>
								<asp:ListItem Text="Christmas Island" value="60"></asp:listitem>
								<asp:ListItem Text="Cocos (Keeling) Islands" value="61"></asp:listitem>
								<asp:ListItem Text="Colombia" value="62"></asp:listitem>
								<asp:ListItem Text="Comoros" value="63"></asp:listitem>
								<asp:ListItem Text="Congo" value="65"></asp:listitem>
								<asp:ListItem Text="Congo (DRC)" value="64"></asp:listitem>
								<asp:ListItem Text="Cook Islands" value="66"></asp:listitem>
								<asp:ListItem Text="Costa Rica" value="67"></asp:listitem>
								<asp:ListItem Text="C�te d'Ivoire" value="68"></asp:listitem>
								<asp:ListItem Text="Croatia" value="69"></asp:listitem>
								<asp:ListItem Text="Cuba" value="70"></asp:listitem>
								<asp:ListItem Text="Cyprus" value="71"></asp:listitem>
								<asp:ListItem Text="Czech Republic" value="72"></asp:listitem>
								<asp:ListItem Text="Denmark" value="73"></asp:listitem>
								<asp:ListItem Text="Djibouti" value="74"></asp:listitem>
								<asp:ListItem Text="Dominica" value="75"></asp:listitem>
								<asp:ListItem Text="Dominican Republic" value="76"></asp:listitem>
								<asp:ListItem Text="Ecuador" value="77"></asp:listitem>
								<asp:ListItem Text="Egypt" value="78"></asp:listitem>
								<asp:ListItem Text="El Salvador" value="79"></asp:listitem>
								<asp:ListItem Text="Equatorial Guinea" value="80"></asp:listitem>
								<asp:ListItem Text="Eritrea" value="81"></asp:listitem>
								<asp:ListItem Text="Estonia" value="82"></asp:listitem>
								<asp:ListItem Text="Ethiopia" value="83"></asp:listitem>
								<asp:ListItem Text="Falkland Islands (Islas Malvinas)" value="84"></asp:listitem>
								<asp:ListItem Text="Faroe Islands" value="85"></asp:listitem>
								<asp:ListItem Text="Fiji Islands" value="86"></asp:listitem>
								<asp:ListItem Text="Finland" value="87"></asp:listitem>
								<asp:ListItem Text="France" value="88"></asp:listitem>
								<asp:ListItem Text="French Guiana" value="89"></asp:listitem>
								<asp:ListItem Text="French Polynesia" value="90"></asp:listitem>
								<asp:ListItem Text="French Southern and Antarctic Lands" value="91"></asp:listitem>
								<asp:ListItem Text="Gabon" value="92"></asp:listitem>
								<asp:ListItem Text="Gambia,The" value="93"></asp:listitem>
								<asp:ListItem Text="Georgia" value="94"></asp:listitem>
								<asp:ListItem Text="Germany" value="95"></asp:listitem>
								<asp:ListItem Text="Ghana" value="96"></asp:listitem>
								<asp:ListItem Text="Gibraltar" value="97"></asp:listitem>
								<asp:ListItem Text="Greece" value="98"></asp:listitem>
								<asp:ListItem Text="Greenland" value="99"></asp:listitem>
								<asp:ListItem Text="Grenada" value="100"></asp:listitem>
								<asp:ListItem Text="Guadeloupe" value="101"></asp:listitem>
								<asp:ListItem Text="Guam" value="102"></asp:listitem>
								<asp:ListItem Text="Guatemala" value="103"></asp:listitem>
								<asp:ListItem Text="Guernsey" value="104"></asp:listitem>
								<asp:ListItem Text="Guinea" value="105"></asp:listitem>
								<asp:ListItem Text="Guinea-Bissau" value="106"></asp:listitem>
								<asp:ListItem Text="Guyana" value="107"></asp:listitem>
								<asp:ListItem Text="Haiti" value="108"></asp:listitem>
								<asp:ListItem Text="Heard Island and McDonald Islands" value="109"></asp:listitem>
								<asp:ListItem Text="Honduras" value="110"></asp:listitem>
								<asp:ListItem Text="Hong Kong SAR" value="111"></asp:listitem>
								<asp:ListItem Text="Hungary" value="112"></asp:listitem>
								<asp:ListItem Text="Iceland" value="113"></asp:listitem>
								<asp:ListItem Text="India" value="114"></asp:listitem>
								<asp:ListItem Text="Indonesia" value="115"></asp:listitem>
								<asp:ListItem Text="Iran" value="116"></asp:listitem>
								<asp:ListItem Text="Iraq" value="117"></asp:listitem>
								<asp:ListItem Text="Ireland" value="118"></asp:listitem>
								<asp:ListItem Text="Isle of Man" value="119"></asp:listitem>
								<asp:ListItem Text="Israel" value="120"></asp:listitem>
								<asp:ListItem Text="Italy" value="1"></asp:listitem>
								<asp:ListItem Text="Jamaica" value="122"></asp:listitem>
								<asp:ListItem Text="Japan" value="123"></asp:listitem>
								<asp:ListItem Text="Jersey" value="125"></asp:listitem>
								<asp:ListItem Text="Jordan" value="124"></asp:listitem>
								<asp:ListItem Text="Kazakhstan" value="126"></asp:listitem>
								<asp:ListItem Text="Kenya" value="127"></asp:listitem>
								<asp:ListItem Text="Kiribati" value="128"></asp:listitem>
								<asp:ListItem Text="Korea" value="129"></asp:listitem>
								<asp:ListItem Text="Kuwait" value="130"></asp:listitem>
								<asp:ListItem Text="Kyrgyzstan" value="131"></asp:listitem>
								<asp:ListItem Text="Laos" value="132"></asp:listitem>
								<asp:ListItem Text="Latvia" value="133"></asp:listitem>
								<asp:ListItem Text="Lebanon" value="134"></asp:listitem>
								<asp:ListItem Text="Lesotho" value="135"></asp:listitem>
								<asp:ListItem Text="Liberia" value="136"></asp:listitem>
								<asp:ListItem Text="Libya" value="137"></asp:listitem>
								<asp:ListItem Text="Liechtenstein" value="138"></asp:listitem>
								<asp:ListItem Text="Lithuania" value="139"></asp:listitem>
								<asp:ListItem Text="Luxembourg" value="140"></asp:listitem>
								<asp:ListItem Text="Macao SAR" value="141"></asp:listitem>
								<asp:ListItem Text="Macedonia, Former Yugoslav Republic of" value="142"></asp:listitem>
								<asp:ListItem Text="Madagascar" value="143"></asp:listitem>
								<asp:ListItem Text="Malawi" value="144"></asp:listitem>
								<asp:ListItem Text="Malaysia" value="145"></asp:listitem>
								<asp:ListItem Text="Maldives" value="146"></asp:listitem>
								<asp:ListItem Text="Mali" value="147"></asp:listitem>
								<asp:ListItem Text="Malta" value="148"></asp:listitem>
								<asp:ListItem Text="Marshall Islands" value="149"></asp:listitem>
								<asp:ListItem Text="Martinique" value="150"></asp:listitem>
								<asp:ListItem Text="Mauritania" value="151"></asp:listitem>
								<asp:ListItem Text="Mauritius" value="152"></asp:listitem>
								<asp:ListItem Text="Mayotte" value="153"></asp:listitem>
								<asp:ListItem Text="Mexico" value="154"></asp:listitem>
								<asp:ListItem Text="Micronesia" value="155"></asp:listitem>
								<asp:ListItem Text="Moldova" value="156"></asp:listitem>
								<asp:ListItem Text="Monaco" value="157"></asp:listitem>
								<asp:ListItem Text="Mongolia" value="158"></asp:listitem>
								<asp:ListItem Text="Montserrat" value="159"></asp:listitem>
								<asp:ListItem Text="Morocco" value="160"></asp:listitem>
								<asp:ListItem Text="Mozambique" value="161"></asp:listitem>
								<asp:ListItem Text="Myanmar" value="162"></asp:listitem>
								<asp:ListItem Text="Namibia" value="163"></asp:listitem>
								<asp:ListItem Text="Nauru" value="164"></asp:listitem>
								<asp:ListItem Text="Nepal" value="165"></asp:listitem>
								<asp:ListItem Text="Netherlands Antilles" value="166"></asp:listitem>
								<asp:ListItem Text="Netherlands, The" value="167"></asp:listitem>
								<asp:ListItem Text="New Caledonia" value="168"></asp:listitem>
								<asp:ListItem Text="New Zeland" value="14"></asp:listitem>
								<asp:ListItem Text="Nicaragua" value="169"></asp:listitem>
								<asp:ListItem Text="Niger" value="170"></asp:listitem>
								<asp:ListItem Text="Nigeria" value="171"></asp:listitem>
								<asp:ListItem Text="Niue" value="172"></asp:listitem>
								<asp:ListItem Text="Norfolk Island" value="173"></asp:listitem>
								<asp:ListItem Text="North Korea" value="174"></asp:listitem>
								<asp:ListItem Text="Northern Mariana Islands" value="175"></asp:listitem>
								<asp:ListItem Text="Norway" value="176"></asp:listitem>
								<asp:ListItem Text="Oman" value="177"></asp:listitem>
								<asp:ListItem Text="Pakistan" value="178"></asp:listitem>
								<asp:ListItem Text="Palau" value="179"></asp:listitem>
								<asp:ListItem Text="Palestinian Authority" value="180"></asp:listitem>
								<asp:ListItem Text="Panama" value="181"></asp:listitem>
								<asp:ListItem Text="Papua New Guinea" value="182"></asp:listitem>
								<asp:ListItem Text="Paraguay" value="183"></asp:listitem>
								<asp:ListItem Text="Peru" value="184"></asp:listitem>
								<asp:ListItem Text="Philippines" value="185"></asp:listitem>
								<asp:ListItem Text="Pitcairn Islands" value="186"></asp:listitem>
								<asp:ListItem Text="Poland" value="187"></asp:listitem>
								<asp:ListItem Text="Portugal" value="188"></asp:listitem>
								<asp:ListItem Text="Puerto Rico" value="189"></asp:listitem>
								<asp:ListItem Text="Qatar" value="190"></asp:listitem>
								<asp:ListItem Text="Reunion" value="191"></asp:listitem>
								<asp:ListItem Text="Romania" value="192"></asp:listitem>
								<asp:ListItem Text="Russia" value="193"></asp:listitem>
								<asp:ListItem Text="Rwanda" value="194"></asp:listitem>
								<asp:ListItem Text="Samoa" value="195"></asp:listitem>
								<asp:ListItem Text="San Marino" value="196"></asp:listitem>
								<asp:ListItem Text="S�o Tom� and Pr�ncipe" value="197"></asp:listitem>
								<asp:ListItem Text="Saudi Arabia" value="198"></asp:listitem>
								<asp:ListItem Text="Senegal" value="199"></asp:listitem>
								<asp:ListItem Text="Serbia and Montenegro" value="200"></asp:listitem>
								<asp:ListItem Text="Seychelles" value="201"></asp:listitem>
								<asp:ListItem Text="Sierra Leone" value="202"></asp:listitem>
								<asp:ListItem Text="Singapore" value="203"></asp:listitem>
								<asp:ListItem Text="Slovakia" value="204"></asp:listitem>
								<asp:ListItem Text="Slovenia" value="205"></asp:listitem>
								<asp:ListItem Text="Solomon Islands" value="206"></asp:listitem>
								<asp:ListItem Text="Somalia" value="207"></asp:listitem>
								<asp:ListItem Text="South Africa" value="208"></asp:listitem>
								<asp:ListItem Text="South Georgia and the South Sandwich Islands" value="209"></asp:listitem>
								<asp:ListItem Text="Spain" value="210"></asp:listitem>
								<asp:ListItem Text="Sri Lanka" value="211"></asp:listitem>
								<asp:ListItem Text="St. Helena" value="212"></asp:listitem>
								<asp:ListItem Text="St. Kitts and Nevis" value="213"></asp:listitem>
								<asp:ListItem Text="St. Lucia" value="214"></asp:listitem>
								<asp:ListItem Text="St. Pierre and Miquelon" value="215"></asp:listitem>
								<asp:ListItem Text="St. Vincent and the Grenadines" value="216"></asp:listitem>
								<asp:ListItem Text="Sudan" value="217"></asp:listitem>
								<asp:ListItem Text="Suriname" value="218"></asp:listitem>
								<asp:ListItem Text="Svalbard and Jan Mayen" value="219"></asp:listitem>
								<asp:ListItem Text="Swaziland" value="220"></asp:listitem>
								<asp:ListItem Text="Sweden" value="221"></asp:listitem>
								<asp:ListItem Text="Switzerland" value="222"></asp:listitem>
								<asp:ListItem Text="Syria" value="223"></asp:listitem>
								<asp:ListItem Text="Taiwan" value="224"></asp:listitem>
								<asp:ListItem Text="Tajikistan" value="225"></asp:listitem>
								<asp:ListItem Text="Tanzania" value="226"></asp:listitem>
								<asp:ListItem Text="Thailand" value="227"></asp:listitem>
								<asp:ListItem Text="Timor-Leste" value="228"></asp:listitem>
								<asp:ListItem Text="Togo" value="229"></asp:listitem>
								<asp:ListItem Text="Tokelau" value="230"></asp:listitem>
								<asp:ListItem Text="Tonga" value="231"></asp:listitem>
								<asp:ListItem Text="Trinidad and Tobago" value="232"></asp:listitem>
								<asp:ListItem Text="Tristan da Cunha" value="233"></asp:listitem>
								<asp:ListItem Text="Tunisia" value="234"></asp:listitem>
								<asp:ListItem Text="Turkey" value="235"></asp:listitem>
								<asp:ListItem Text="Turkmenistan" value="236"></asp:listitem>
								<asp:ListItem Text="Turks and Caicos Islands" value="237"></asp:listitem>
								<asp:ListItem Text="Tuvalu" value="238"></asp:listitem>
								<asp:ListItem Text="Uganda" value="239"></asp:listitem>
								<asp:ListItem Text="Ukraine" value="240"></asp:listitem>
								<asp:ListItem Text="United Arab Emirates" value="241"></asp:listitem>
								<asp:ListItem Text="United Kingdom" value="242"></asp:listitem>
								<asp:ListItem Text="United States" value="15"></asp:listitem>
								<asp:ListItem Text="United States Minor Outlying Islands" value="243"></asp:listitem>
								<asp:ListItem Text="Uruguay" value="244"></asp:listitem>
								<asp:ListItem Text="Uzbekistan" value="245"></asp:listitem>
								<asp:ListItem Text="Vanuatu" value="246"></asp:listitem>
								<asp:ListItem Text="Vatican City" value="247"></asp:listitem>
								<asp:ListItem Text="Venezuela" value="248"></asp:listitem>
								<asp:ListItem Text="Vietnam" value="249"></asp:listitem>
								<asp:ListItem Text="Virgin Islands" value="250"></asp:listitem>
								<asp:ListItem Text="Virgin Islands, British" value="251"></asp:listitem>
								<asp:ListItem Text="Wallis and Futuna" value="252"></asp:listitem>
								<asp:ListItem Text="Yemen" value="253"></asp:listitem>
								<asp:ListItem Text="Zambia" value="254"></asp:listitem>
								<asp:ListItem Text="Zimbabwe" value="255"></asp:listitem>
							</asp:dropdownlist>
							
                            <HP3:Text visible="false" runat="server" ID="_Father2" TypeControl="TextBox" style="width:300px" />
                        </td>
                    </tr>
                </table>
                
                <div id="divGEO" style="display:none">
                    <table>
                        <tr>
                            <td><asp:panel ID="pnlGeoTree" runat="server" style="overflow:auto;height:300px;border:1px solid #000;width:300px"/></td>
                            <td><asp:button ID="_btnAddSelect" runat="server" CssClass="button" text="ADD" OnClick="AddItemGeoText" style="display:none"/></td>
                        </tr>   
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserTel&Help=cms_HelpUserTel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserTel", "Tel")%>*</td>
            <td><HP3:Text runat ="server" id="userTel" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCell&Help=cms_HelpUserCell&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCell", "Cell")%>*</td>
            <td><HP3:Text runat ="server" id="userCell" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserFax&Help=cms_HelpUserFax&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserFax", "Fax")%>*</td>
            <td><HP3:Text runat ="server" id="userFax" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRegistrationDate&Help=cms_HelpRegistrationDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRegistrationDate", "Registration Date")%></td>
            <td><HP3:Date runat="server" id="userRegistrationDate"  TypeControl="JsCalendar"/></td>
        </tr>

		<!--
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListProfiles&Help=cms_HelpListProfiles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListProfiles", "List Profiles")%></td>
            <td><HP3:ctlProfiles runat ="server" id="ctlProfiles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" id="ctlRoles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
           <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListGroups&Help=cms_HelpListGroups&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListGroups", "List Groups")%></td>
          <td><HP3:ctlUserGroups runat ="server" id="ctluserGroups" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListContext&Help=cms_HelpListContext&Language=<%=Language()%>','Help','width=800,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListContext", "List Context")%></td>
            <td><HP3:ctlContext runat ="server" id="ctlContext" typecontrol="GenericRelation" /></td>
        </tr>
		-->
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormReceiveNewsletter&Help=cms_HelpReceiveNewsletter&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormReceiveNewsletter", "Receive Newsletter")%></td>
            <td><asp:checkbox id="flagNewsletter" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserStatus&Help=cms_HelpUserStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormUserStatus", "Actual Status")%></td>
            <td>
                <asp:DropDownList id="ddluserStatus" runat="server">
				  <asp:ListItem  Selected ="True" Text="Active" Value="1"></asp:ListItem>
				  <asp:ListItem  Text="Not active" Value="3"></asp:ListItem>
                </asp:DropDownList>
				<%--
				  <asp:ListItem  Text="Pending" Value="2"></asp:ListItem>
				  <asp:ListItem  Text="Disabled" Value="4"></asp:ListItem>
				  <asp:ListItem  Text="Deleted" Value="5"></asp:ListItem>
				--%>
            </td>
        </tr>
        <%--<tr>
            <td>Community</td>
            <td><HP3:ctlCommunity runat ="server" id="ctlCommunity" typecontrol="GenericRelation" /></td>
        </tr> --%>
     </table>
 
    <div id="divExtraFields" runat="server" visible="false" >
        <asp:Repeater id="rp_extrafield" OnItemDataBound="LoadExtraFields"  runat="server">
	           <HeaderTemplate><table class="form" style="width:75%" ></HeaderTemplate>
	           <FooterTemplate></table></FooterTemplate>
               <ItemTemplate>                
                   <tr>
                        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraField&Help=cms_HelpExtraField&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%#DataBinder.Eval(Container.DataItem, "Name") %></td>
                        <td>
                            <asp:Literal id="extFId" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' runat="server" visible="false" />
                            <HP3:ctlExtraField id="extrF"  MaxLength='<%#DataBinder.Eval(Container.DataItem, "Length") %>' TypeControl='<%#DataBinder.Eval(Container.DataItem, "Type") %>' runat="server"/>
                        </td>
                  </tr>
               </ItemTemplate>
          </asp:Repeater>
    </div>
    
    <div id="divHCP" runat="server" visible="false">
    </div>   
    </span></span>   
</asp:Panel>

<asp:Panel id="panelCommunity" runat="server" Visible="false">
    <asp:button ID="btLoad" runat="server" Text="Archive" onclick="LoadArchive" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px; font-size:12px;" />
    <asp:button runat="server" ID="btSave" onclick="SaveUserPermission" Text="Save" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px; font-size:12px;"/><br /> <br />
    <hr />
    <table class="form">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunity&Help=cms_HelpCommunity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunity", "Community")%></td>
            <td><HP3:ctlCommunity runat ="server" id="ctlCommunity" typecontrol="GenericRelation" /></td>
        </tr> 
        <tr>
            <td><asp:button ID="btSaveRelation" runat="server" Text="Save Relations" onclick="SaveCommunityRelation" CssClass="button" /></td> 
        </tr>
    </table>
</asp:Panel> 
    
 <asp:Panel id="divCommunity" runat="server" visible="false">
    <table class="form" >
        <tr>
            <td style="width:145px"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserPermissions&Help=cms_HelpUserPermissions&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserPermissions", "User Permissions on Service")%></td>
            <td><asp:ListBox id="lbServicePermission"  width="200px" runat="server"></asp:ListBox> </td>
            <td><asp:button ID="btDelete" CssClass="button" runat="server" Text="Delete" Style ="width:100px"  OnClick="DeleteServicePermission" /></td>
        </tr>
        <tr>
             <td>&nbsp;</td>
             <td><asp:dropdownlist id="dwlService" runat="server"></asp:dropdownlist></td>
             <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPermission&Help=cms_HelpPermission&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPermission", "Permission")%></td>
             <td>       
                <asp:dropdownlist id="dwlPermission" runat="server">
                   <asp:ListItem Selected="True" Text="Read/Write" Value="2" />
                   <asp:ListItem Text="Read" Value="1" />
                </asp:dropdownlist></td>
              <td> <asp:button ID="btPermission" CssClass="button" runat="server" Text="Add" onclick="AddPermission" /></td> 
        </tr>
    </table>
</asp:Panel>

