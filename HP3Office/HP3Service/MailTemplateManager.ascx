<%@ Control Language="VB" ClassName="myMailTemplate" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>

<script runat="server">
    Private mPageManager As New MasterPageManager()
    Private mailTemplateManager As New MailTemplateManager
    Private mailTemplateValue As MailTemplateManager
    Private objCMSUtility As New GenericUtility
      
    Private optManager As ContentOptionService
    Private optSearcher As ContentOptionSearcher
    Private optCollection As ContentOptionCollection
    
    Dim oSearcher As New MailTemplateSearcher
    Dim oCManager = New MailTemplateManager
    Dim oCollection = New MailTemplateCollection
    
    Private dictionaryManager As New DictionaryManager
    Private _strDomain As String
    Private _language As String
    
    Private strJS As String
    Private strName As String
    Private strDomain As String
    Private utility As New WebUtility
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    'popola la dwl delle lingue nel searcher
    Sub ReadLanguage()
        Dim olangualeCollection As LanguageCollection
        Dim oLanguageSearcher As New LanguageSearcher
             
        olangualeCollection = Me.BusinessLanguageManager.Read(oLanguageSearcher)
        
        utility.LoadListControl(dwlLanguage, olangualeCollection, "Key.Code", "Key.Id")
        dwlLanguage.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = mPageManager.GetLang.Id
        Domain() = strDomain
        
        If Not (Page.IsPostBack) Then
            ActivePanelGrid()
            ReadLanguage()
            LoadArchive()
        End If
    End Sub
    
    Protected Sub gdw_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        LoadArchive()
    End Sub
   
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gdw_mailTemplateList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New MailTemplateSearcher
        
        If dwlLanguage.SelectedItem.Value <> 0 Then oSearcher.Key.KeyLanguage.Id = dwlLanguage.SelectedItem.Value
        oSearcher.Active = SelectOperation.All
        BindGrid(objGrid, oSearcher)
    End Sub
    
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As MailTemplateSearcher = Nothing)
        oCManager = New MailTemplateManager
        oCollection = New MailTemplateCollection
        
        If Searcher Is Nothing Then
            Searcher = New MailTemplateSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)

        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub

       'recupera la lista dei mail Template 
    Sub LoadArchive()
        Dim mailTemaplateSearcher As New MailTemplateSearcher
        Dim mailTemplateCollection As New MailTemplateCollection
        mailTemplateManager.Cache = False
        mailTemaplateSearcher.Active = SelectOperation.All
        'mailTemaplateSearcher.Delete = SelectOperation.Disabled
        mailTemplateCollection = mailTemplateManager.Read(mailTemaplateSearcher)
        If Not (mailTemplateCollection Is Nothing) Then
            gdw_mailTemplateList.DataSource = mailTemplateCollection
            gdw_mailTemplateList.DataBind()
        End If
    End Sub
    
    Sub onItemDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim oImgBtn As ImageButton
            oImgBtn = e.Row.FindControl("lnkDisable")
            If Not e.Row.DataItem.active Then
                oImgBtn.ImageUrl = "~/hp3Office/HP3Image/ico/content_noactive.gif"
                oImgBtn.CommandName = "EnableItem"
            Else
                oImgBtn.ImageUrl = "~/hp3Office/HP3Image/ico/content_active.gif"
                oImgBtn.CommandName = "DisableItem"
            End If
        End If
    End Sub
    
    ' modifica un singolo record
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim mailTemplateValue As New MailTemplateValue
        Dim domainId As Integer
        'siteAreaManager = New SiteAreaManager
        If sender.CommandName = "SelectItem" Then
            domainId = sender.CommandArgument
            SelectedId = domainId
            mailTemplateManager.Cache = False
            
            Dim oMailSearcher As New MailTemplateSearcher
            oMailSearcher.Key.Id = domainId
            oMailSearcher.Active = SelectOperation.All
            
            'mailTemplateValue = mailTemplateManager.Read(New MailTemplateIdentificator(domainId))
            mailTemplateValue = mailTemplateManager.Read(oMailSearcher)(0)
            If Not mailTemplateValue Is Nothing Then
                LoadDett(mailTemplateValue)
                ActivePanelDett()
            End If
        End If
    End Sub
    
    ' disabilita un singolo record
    Sub DisableRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim mailTemplateValue As New MailTemplateValue
        Dim disableItemId As Integer

        disableItemId = sender.CommandArgument
        SelectedId = disableItemId
        mailTemplateManager.Cache = False
        
        Dim oMailSearcher As New MailTemplateSearcher
        oMailSearcher.Key.Id = disableItemId
        oMailSearcher.Active = SelectOperation.All

        Dim oMTValue As MailTemplateValue
        oMTValue = mailTemplateManager.Read(oMailSearcher)(0)
        
        If Not oMTValue Is Nothing AndAlso oMTValue.Key.Id > 0 Then
            If sender.CommandName = "DisableItem" Then
                oMTValue.Active = False
            ElseIf sender.CommandName = "EnableItem" Then
                oMTValue.Active = True
            End If
            oMTValue = mailTemplateManager.Update(oMTValue)
        End If
        LoadArchive()
        ActivePanelGrid()
    End Sub
    
    ' cancella un singolo record
    Sub RemoveRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim mailTemplateValue As New MailTemplateValue
        Dim removeItemId As Integer
        'Dim risRemove As Boolean
        
        'siteAreaManager = New SiteAreaManager
        
        If sender.CommandName = "DeleteItem" Then
            removeItemId = sender.CommandArgument
            SelectedId = removeItemId
             
            mailTemplateManager.Cache = False
            
            Dim oMailSearcher As New MailTemplateSearcher
            oMailSearcher.Key.Id = removeItemId
            oMailSearcher.Active = SelectOperation.All

            Dim oMTValue As MailTemplateValue
            oMTValue = mailTemplateManager.Read(oMailSearcher)(0)
            If Not oMTValue Is Nothing AndAlso oMTValue.Key.Id > 0 Then
                'risRemove = mailTemplateManager.Remove(New MailTemplateIdentificator(removeItemId))    
                oMTValue.Delete = True
            End If
            oMTValue = mailTemplateManager.Update(oMTValue)
            
                                  
            LoadArchive()
            ActivePanelGrid()
            
            'Gestione dei valori di ritorno
            'If Not risRemove Then
            'CreateJsMessage("Impossible to delete this Mail Template")
            'End If
                       
        End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim mailTemplateValue As New MailTemplateValue
        Dim oSiteAreaValue As New SiteAreaValue
        'siteAreaManager = New SiteAreaManager
       
        If (IsNotNull()) Then
            
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni
            'If (IsEMail(senderEmail.Text)) Then
            'If (IsEmails()) Then
            With (mailTemplateValue)
                .Key.MailTemplateName = mailTemlateName.Text
                .Key.KeyLanguage.Id = lSelector.Language
                .SenderName = senderName.Text
                .SenderAddress = senderEmail.Text
                .Recipients = senderRec.Text
                .CcRecipients = senderCc.Text
                .BccRecipients = senderBcc.Text
                .Subject = senderSubject.Text
                '.Body = senderBody.Value
                '.Body = senderBody.Content'telerik
                .Body = senderBody.Text
                .FormatType = senderHtml.SelectedItem.Value()
                .Note = taNote.Text
                .Active = hdActive.Value

                If txtHideSite.Text <> "" Then
                    .KeySite.Id = txtHideSite.Text
                End If
            End With
            
            If SelectedId = 0 Then
                mailTemplateManager.Create(mailTemplateValue)
                LoadArchive()
                ActivePanelGrid()
            Else
                mailTemplateValue.Key.Id = SelectedId
                mailTemplateManager.Update(mailTemplateValue)
                LoadArchive()
                ActivePanelGrid()
            End If
            'End If
            'Else
            'CreateJsMessage("Email is not in a correct format ")
            'End If
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Sub
    
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
    
    'popola i campi di testo con le informazioni contenute nel mailTemplateValue
    Sub LoadDett(ByVal mailTemplateValue As MailTemplateValue)
        If Not mailTemplateValue Is Nothing Then
            hdActive.value = mailTemplateValue.Active()
            mailTemlateName.Text = mailTemplateValue.Key.MailTemplateName
            senderName.Text = mailTemplateValue.SenderName
            senderEmail.Text = mailTemplateValue.SenderAddress
            senderRec.Text = mailTemplateValue.Recipients
            senderCc.Text = mailTemplateValue.CcRecipients
            senderBcc.Text = mailTemplateValue.BccRecipients
            senderSubject.Text = mailTemplateValue.Subject
            'senderBody.Value = mailTemplateValue.Body
            'senderBody.Content = mailTemplateValue.Body'telerik
            senderBody.Text = mailTemplateValue.Body
            taNote.Text = mailTemplateValue.Note
            
            senderHtml.SelectedIndex = senderHtml.Items.IndexOf(senderHtml.Items.FindByValue(mailTemplateValue.FormatType))
            'lSelector.DefaultValue = mailTemplateValue.Key.KeyLanguage.Id '
            lSelector.LoadLanguageValue(mailTemplateValue.Key.KeyLanguage.Id)
                       
            txtHideSite.Text = mailTemplateValue.KeySite.Id
            txtSite.Text = ReadSiteValue(mailTemplateValue.KeySite.Id)
        Else
            mailTemlateName.Text = Nothing
            senderName.Text = Nothing
            senderEmail.Text = Nothing
            senderRec.Text = Nothing
            senderCc.Text = Nothing
            senderBcc.Text = Nothing
            senderSubject.Text = Nothing
            'senderBody.Value = Nothing
            'senderBody.Content = Nothing'telerik
            senderBody.Text = ""
            taNote.Text = Nothing
            hdActive.Value = True
            
            'lSelector.DefaultValue =objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
            'lSelector.IsNew = True
            'lSelector.LoadDefaultValue()
        End If
    End Sub
    
    'restituisce il sito selezionato 
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = objCMSUtility.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
    
    'controlla che la email sia nel corretto formato
    Function IsEMail(ByVal email As String) As Boolean
        Dim _result As Boolean
        Dim emailFormat As String = "([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})"
           
        If Not (Regex.IsMatch(email, emailFormat)) Then
            _result = False
        Else
 
            _result = True
        End If
        
        Return _result
    End Function
    
    'controlla che tutte le mail inserite siano in un formato corretto
    Function IsEmails() As Boolean
        Dim EmailsArray() As String = senderRec.Text.Split(";")
        Dim elem As String
        Dim _result As Boolean
        
        For Each elem In EmailsArray
            _result = IsEMail(elem)
            If Not (_result) Then
                CreateJsMessage("Email is not in a correct format ")
                Return False
            End If
        Next
        
        Return True
    End Function
    
    'controlla che i campi di testo non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (mailTemlateName.Text = String.Empty) Then strName = "Mail Temaplate Name"
        'If (senderName.Text = String.Empty) Then strName = "Sender Name"
        'If (senderEmail.Text = String.Empty) Then strName = "Sender Email"
        'If (senderRec.Text = String.Empty) Then strName = "Sender Recipients"
        If (senderSubject.Text = String.Empty) Then strName = "Sender Subject"
        'If (senderBody.Value = String.Empty) Then strName = "Sender Body"
        'If (senderBody.Content = String.Empty) Then strName = "Sender Body"
        'If ((mailTemlateName.Text <> String.Empty) And (senderSubject.Text <> String.Empty) And (senderBody.Value <> String.Empty)) Then
        If ((mailTemlateName.Text <> String.Empty) And (senderSubject.Text <> String.Empty)) Then 'And (senderBody.Content <> String.Empty)
            _return = True
        End If
        
        Return _return
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlSearch.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
        pnlSearch.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
</script>
<script type ="text/javascript" ><%=strJS%></script>
<hr />
<!--Mail template search panel-->
<asp:Panel ID="pnlSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
    <table class="topContentSearcher">
    <tr>
        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Relation Searcher</span></td>
    </tr>
    </table>
    <asp:Table ID="tableSearch" CssClass="form" runat="server">
        <asp:TableRow>
            <asp:TableCell>Language</asp:TableCell>      
        </asp:TableRow>
        <asp:TableRow >
            <asp:TableCell><asp:DropDownList id="dwlLanguage"  runat="server" /></asp:TableCell> 
        </asp:TableRow>
     </asp:Table>
     <asp:Table ID="tableSearch2"  CssClass="form" runat="server">
        <asp:TableRow>
            <asp:TableCell><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<hr/>

<asp:Panel id="pnlGrid" runat="server" visible="true"> 
    <div style="margin-bottom:10px"><asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></div>
    <asp:Label CssClass="title" runat="server" id="sectionTit">Mail Template List</asp:Label>
    <asp:gridview id="gdw_mailTemplateList" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        OnPageIndexChanging="gdw_PageIndexChanging"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"             
        AllowSorting="true"
        PageSize ="20"
        emptydatatext="No item available"
        OnRowDataBound="onItemDatabound">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="5%" HeaderText="ID">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField HeaderStyle-Width="70%" HeaderText="Template Name">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.MailTemplateName")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField HeaderStyle-Width="7%">
                <ItemTemplate>
                    <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    <asp:ImageButton ID="lnkDisable" runat="server" ToolTip ="Disable item" ImageUrl="~/hp3Office/HP3Image/ico/content_active.gif" OnClick="DisableRow" CommandName ="DisableItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClick="RemoveRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
    <div>
        <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <table  style=" margin-top:10px" class="form" width="98%">
    <tr><td style="width:50%" valign ="top"></td><td></td></tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemplateName&Help=cms_HelpMailTemplateName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemplateName", "Mail Template Name")%></td>
        <td><HP3:Text runat ="server" id="mailTemlateName" TypeControl ="TextBox" style="width:600px" MaxLength="50"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTempalteLanguage&Help=cms_HelpMailTempalteLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTempalteLanguage", "Mail Tempalte Language")%></td>
        <td><HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
    </tr>
    <tr>
       <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
        <td>
            <table width="100%">
            <tr>
                <td width="50px">
                    <asp:TextBox id="txtHideSite" runat="server" style="display:none"/>
                    <HP3:Text runat ="server" id="txtSite"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSite.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSite.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Site%>&ListId=<%=txtSite.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderName&Help=cms_HelpMailTemSenderName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderName", "Sender Name")%></td>
        <td><HP3:Text runat ="server" id="senderName" TypeControl ="TextBox" style="width:600px" MaxLength="250"/></td>
    </tr>
    <tr>
        <td><asp:Label id="lbMsg"  runat ="server" Text="[* More than one e-mail address could be insert separated with ';' ]" style="color: Gray" ></asp:Label></td> 
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderEmail&Help=cms_HelpMailTemSenderEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderEmail", "Sender Email")%></td>
        <td><HP3:Text runat ="server" id="senderEmail" TypeControl ="TextBox" style="width:600px" MaxLength="100"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderRecipients&Help=cms_HelpMailTemSenderRecipients&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderRecipients", "Sender Recipients")%></td>
        <td><HP3:Text runat ="server" id="senderRec" TypeControl ="TextBox" style="width:600px" MaxLength="1024"/></td>
        
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderCC&Help=cms_HelpMailTemSenderCC&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderCC", "Sender CC")%></td>
        <td><HP3:Text runat ="server" id="senderCc" TypeControl ="TextBox" style="width:600px" MaxLength="1024"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderBcc&Help=cms_HelpMailTemSenderBcc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderBcc", "Sender Bcc")%></td>
        <td><HP3:Text runat ="server" id="senderBcc" TypeControl ="TextBox" style="width:600px" MaxLength="1024"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderSubject&Help=cms_HelpMailTemSenderSubject&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderSubject", "Sender Subject")%></td>
        <td><HP3:Text runat ="server" id="senderSubject" TypeControl ="TextBox" style="width:600px" MaxLength="1024"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderBody&Help=cms_HelpMailTemSenderBody&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderBody", "Sender Body")%></td>
        <td>
            <%-- <FCKeditorV2:FCKeditor id="senderBody" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />--%>            
            
            
            
            <asp:TextBox TextMode="MultiLine" Width="600px" Rows="10" Columns="100" ID="senderBody" runat="server"></asp:TextBox>
            <%--<telerik:RadEditor ID="senderBody" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                <Content></Content>
                <Snippets>
                    <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                </Snippets>
                <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                </Languages>
                
                <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                        <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                    </telerik:EditorLink>
                </Links>
                <Links>
                    <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                </Links>        
            </telerik:RadEditor>--%>
            
            
        </td>
    </tr>
    <tr>
        <td>
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMailTemSenderHtml&Help=cms_HelpMailTemSenderHtml&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMailTemSenderHtml", "Sender Html")%>
        </td>
        <td>
            <asp:dropdownlist id="senderHtml" runat="server">
                <asp:ListItem Selected="true" Text="HTML" Value="1" />
                <asp:ListItem  Selected ="False" Text="Text Plain" Value="0" />
            </asp:dropdownlist>
        </td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_HelpNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNote", "Note")%></td>
        <td><HP3:Text runat ="server" id="taNote"  TypeControl="TextArea"  style="width:300px"  MaxLength="255"/>    </td>
    </tr>
    </table>
    <asp:HiddenField ID="hdActive" runat="server" />
</asp:Panel>