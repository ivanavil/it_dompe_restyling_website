<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<script runat="server">

    Public themeSearcher As New ThemeSearcher()
    Public themeKey As New ThemeIdentificator()
    Public themeID As New ThemeIdentificator()
    Public webRequest As New WebRequestValue()

    Private curSite As Integer
    Private curTheme As Integer
  
    Dim roleCollection As New RoleCollection
    Dim ListLink As Hashtable
    Dim StringMenuPage As String = ""
    Public aaa As Integer = 0
    
    Sub Page_load()
        
        'themeSearcher.KeyFather.Id = 154
        'themeSearcher.KeySite.Id = 42
        'themeSearcher.Active = SelectOperation.All
        'themeSearcher.Display = SelectOperation.All
        'Dim ocoll As ThemeCollection
        'ocoll = Me.BusinessThemeManager.Read(themeSearcher)
        'For Each o As ThemeValue In ocoll
        '    Response.Write(o.Description & "<br>")
        'Next
        
        
        'Response.End()
        
        
        If Not Page.IsPostBack Then LoadCmbSite()
        If Not Request("fileName") Is Nothing And Request("fileName") <> "" Then
            Dim strFileName As String = ""
            strFileName = Request("fileName")
            If strFileName.IndexOf(".zip") > -1 Then
                Dim strPath As String = "/HP3Image/exp/" & strFileName
                If StreamUtility.FileExists(Server.MapPath(strPath)) Then
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileName)
                    Response.ContentType = "application/octet-stream"
                    Server.Transfer(strPath)
                End If
            End If
        End If
    End Sub
    
    Sub LoadCmbSite()
        Dim oSiteCollection As SiteCollection
        Dim oSiteSearcher As New SiteSearcher
        Dim oSiteValue As SiteValue
        Dim curIdSite As Int32 = Me.ObjectSiteDomain.KeySiteArea.Id
        oSiteCollection = Me.BusinessSiteManager.Read(Me.ObjectTicket.Roles)
        
        'roleCollection = profilingManager.ReadRoles(New UserIdentificator(accService.GetTicket.User.Key.Id))
        'roleCollection = accService.GetTicket.Roles
        
        Dim siteColl As New SiteCollection
        siteColl = oSiteCollection.DistinctBy("KeySiteArea.Id")
        
        For Each oSiteValue In siteColl
            If curIdSite <> oSiteValue.KeySiteArea.Id AndAlso oSiteValue.Status() Then
                ddSites.Items.Add(New ListItem(oSiteValue.Label, oSiteValue.KeySiteArea.Id))
            End If
        Next
        
        ddSites.Items.Insert(0, New ListItem("Select a web site", -1))
        ddlLangs.Items.Insert(0, New ListItem("Select a language", -1))
        ddlLangs.SelectedIndex = 0
    End Sub
    
    Sub LoadLang(ByVal s As Object, ByVal e As EventArgs)
        If ddSites.SelectedItem.Value > 0 And ddlLangs.Items.Count = 1 Then
            Dim oLangSiterelColl As New LanguageSiteRelationCollection
            Dim so As New LanguageSiteRelationSearcher
            so.SiteAreaId.Id = ddSites.SelectedItem.Value
            oLangSiterelColl = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(so)
        
            If Not oLangSiterelColl Is Nothing AndAlso oLangSiterelColl.Count > 0 Then
                Dim oLangColl As New LanguageCollection
                For Each x As LanguageSiteRelationValue In oLangSiterelColl
                    oLangColl.Add(Me.BusinessLanguageManager.Read(x.LanguageId))
                Next
            
                If Not oLangColl Is Nothing AndAlso oLangColl.Count > 0 Then
                    For Each y As LanguageValue In oLangColl
                        ddlLangs.Items.Add(New ListItem(y.Description, y.Key.Id))
                    Next
                End If
            End If
        Else
            ddlLangs.Items.Clear()
            ddlLangs.Items.Insert(0, New ListItem("Select a language", -1))
            ddlLangs.SelectedIndex = 0
        End If
    End Sub
    
    Sub setLink(ByVal s As Object, ByVal e As EventArgs)
        Try
            If ddSites.SelectedItem.Value > 0 Then
                roleCollection = Me.BusinessMasterPageManager.GetTicket.Roles 'profilingManager.ReadRoles(New UserIdentificator(accessService.GetTicket.User.Key.Id))
                StringMenuPage = ""
                aaa = Me.BusinessSiteManager.Read(Integer.Parse(ddSites.SelectedItem.Value)).mainTheme
                fillMenu(aaa, ddSites.SelectedItem.Value)
                
                ltlThemeTree.Text = StringMenuPage
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub

    Sub fillMenu(ByVal id As Integer, ByVal siteId As Integer, Optional ByVal round As Integer = 1) 'ByVal id As Integer, ByVal site As Integer, ByVal round As Integer)
        'Dim site As Integer = ddSites.SelectedItem.Value
        themeSearcher.KeyFather.Id = id
        themeSearcher.KeySite.Id = siteId
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        
        Dim branch As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher) 'themeManager.Read(themeSearcher)
        Dim i As Integer
        StringMenuPage += "<ul class='lXMenu'>"
        For i = 0 To branch.Count - 1
            If Not roleCollection Is Nothing Then
                If roleCollection.HasRole(branch(i).ConsolleRole) Then
                    'men� relativo al sito selezionato    
                    If branch(i).Active And branch(i).Display Then
                        StringMenuPage += "<li>" + branch(i).Description + " - " + "<input name=""myC"" class=""treeLeaf"" value=""" & branch(i).Key.Id & """ type=""checkbox"" /></li>"
                    Else
                        'If Not branch(i).Active Then
                        'StringMenuPage += "<li style=""color:#FF0000;text-decoration:line-through;background-color:#FFF;"">" + branch(i).Description + " - " + "<input name=""myC"" class=""treeLeaf"" value=""" & branch(i).Key.Id & """ type=""checkbox"" /></li>"
                        'Else
                        If branch(i).Active And Not branch(i).Display Then StringMenuPage += "<li style=""color:#FF0000;background-color:#FFF;"">" + branch(i).Description + " - " + "<input name=""myC"" class=""treeLeaf"" value=""" & branch(i).Key.Id & """ type=""checkbox"" /></li>"
                        'End If
                    End If
                Else
                    If HasChild(branch(i).Key.Id(), siteId) Then
                        StringMenuPage += "<li>" + branch(i).Description + " - " + "<input name=""myC"" class=""treeLeaf"" value=""" & branch(i).Key.Id & """ type=""checkbox"" /></li>"
                    End If
                End If
            End If
            If HasChild(branch(i).Key.Id(), siteId) Then fillMenu(branch(i).Key.Id(), siteId, 1 + round)
        Next
        StringMenuPage += "</ul>"
    End Sub
    
    Function HasChild(ByVal idFather As Integer, ByVal idSite As Integer) As Boolean
        themeSearcher.KeyFather.Id = idFather
        themeSearcher.KeySite.Id = idSite
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        If Me.BusinessThemeManager.Read(themeSearcher).Count > 0 Then Return True
        Return False
    End Function
</script>
<script type="text/javascript" language="javascript" src="/Js/jquery-1.2.6.pack.js"></script>
<%  '-1.2.6.pack %>
<script type ="text/javascript">
    var strCheckBox = '';
    $(document).ready(function(){
        $("#btnExport").click(function(){ exportData(); });
        $("#btnSelect").click(function(){ selectAll(); }); 
        $("#btnRemove").click(function(){ unSelectAll(); });

        //Sites        
        if ( $('.ddlSite').val() > 0 ){
            $(".ddlLang").removeAttr("disabled");
        }
        
        else {
            $(".ddlLang").attr("disabled", "disabled");
            
            $("#dTree").html("");
            $("#btnSelect").show().attr("disabled","disabled");
            $("#btnRemove").attr("disabled","disabled").hide();
        }; 
        
        //Languages
        if ( $('.ddlLang').val() > 0 ){
            $("#btnSelect").removeAttr("disabled");
        }
        
        else {
            $("#dTree").html("");
            $("#btnSelect").show().attr("disabled","disabled");
            $("#btnRemove").attr("disabled","disabled").hide();
        };

        $(".treeLeaf").click (function(){
            strCheckBox = $("input[@type=checkbox]").serialize().replace(/&/gi, ",").replace(/myC=/gi, "");
            if ( strCheckBox.length == 0 ){
                $("#btnExport").attr("disabled", "disabled");
            }
            else {
                $("#btnExport").removeAttr("disabled");
            };
        });
    }); 
    
    function exportData() {
        strCheckBox = $("input[@type=checkbox]").serialize().replace(/&/gi, ",").replace(/myC=/gi, "");
        $("#dTree").hide();
        $("#dMessage").show();
        $("#btnSelect").show().attr("disabled","disabled");
        $("#btnRemove").attr("disabled","disabled").hide();
        $("#btnExport").attr("disabled","disabled")
        $("#btnReset").show();
        alert("asyncType=ExportWholeContent&siteId=" + <%=ddSites.SelectedItem.Value%> + "&mainTheme=" + <%=aaa %> + "&langId=" + <%=ddlLangs.SelectedItem.Value%> + "&selectedThemes=" + strCheckBox);
        $.ajax({ 
            type: "POST", 
            url: "/HP3Office/HP3Service/async.aspx", 
            data: "asyncType=ExportWholeContent&siteId=" + <%=ddSites.SelectedItem.Value%> + "&mainTheme=" + <%=aaa %> + "&langId=" + <%=ddlLangs.SelectedItem.Value%> + "&selectedThemes=" + strCheckBox,
            success: function(msg) {
                if (msg == "ko") {
                     $("#dMessage").html("si � verificato un errore. Riprovare prego");
                };
                $("#dMessage").html(msg);
                $("#btnZip").show();
                $("#btnZip").click(function(){ startDown(); });
                //$("#dMessage").html(msg)
            },
            error: function(msg) {
                $("#dMessage").html("si � verificato un errore. Riprovare prego");
            }
        });
    };

    function selectAll() {
        $('.treeLeaf').attr("checked", "checked");
        $("#btnSelect").hide();
        $("#btnRemove").show();
        $("#btnExport").removeAttr("disabled");
    };
    
    function unSelectAll() {
        $('.treeLeaf').removeAttr("checked");
        $("#btnSelect").show();
        $("#btnRemove").hide();
        $("#btnExport").attr("disabled", "disabled");
    };
    
    function resetAll() {
        $("#dMessage").html(" ");
        $("#btnRemove").hide();
        $("#btnSelect").hide();
        $("#btnExport").hide();
    
        $("#btnReset").click(function(){
            $("#btnReset").show();
            location.href='?Ct=<%=request("Ct") %>&N=<%=request("N") %>&T=<%=request("T") %>&C=<%=request("C") %>&Sa=<%=request("Sa") %>';
        });
    };    
    
    function startDown() {
        $.ajax({ 
            type: "POST", 
            url: "/HP3Office/HP3Service/async.aspx", 
            data: "asyncType=ZipExportContent&siteId=" + <%=ddSites.SelectedItem.Value%> + "&mainTheme=" + <%=aaa %> + "&langId=" + <%=ddlLangs.SelectedItem.Value%>,
            success: function(msg) {
                if (msg == "ko") {
                     $("#dMessage").html("si � verificato un errore. Inziare la procedura di nuovo");
                };
                $("#btnZip").hide();
                $("#btnDown").show();
                $("#btnDown").click(function(){
                     location.href='?Ct=<%=request("Ct") %>&N=<%=request("N") %>&T=<%=request("T") %>&C=<%=request("C") %>&Sa=<%=request("Sa") %>&fileName=' + msg;
                });
                $("#btnReset").click(function(){
                    $("#btnReset").show();
                    location.href='?Ct=<%=request("Ct") %>&N=<%=request("N") %>&T=<%=request("T") %>&C=<%=request("C") %>&Sa=<%=request("Sa") %>';
                });
                $("#dMessage").html(" ");               
            },
            error: function(msg) {
                $("#dMessage").html("si � verificato un errore. Riprovare prego");
            }
        });     
    };
</script>
<hr />
<asp:dropdownlist ID="ddSites" CssClass="ddlSite" runat="server" OnSelectedIndexChanged ="LoadLang" AutoPostBack="true" />
<asp:dropdownlist ID="ddlLangs" CssClass="ddlLang" runat="server" OnSelectedIndexChanged ="setLink" AutoPostBack="true" />
<hr />
<input type="button" disabled="disabled" id="btnExport" value="Export Now" />&nbsp;
<input type="button" disabled="disabled" id="btnSelect" value="Select All" /><input style="display:none;" type="button" id="btnRemove" value="Unselect All" />
<hr />
<div id="dTree"><asp:literal runat="server" ID="ltlThemeTree"/></div>
<input id="btnZip" type="button" value="Click here to create the zip archive" style="display:none;" />
<input id="btnDown" type="button" value="Click here to download the zip archive" style="display:none;" />
<input id="btnReset" type="button" value="Click here to restart the exportation process" style="display:none;" />
<div id="dMessage" style="display:none;">Please wait xml generation is running...</div>