<%@ Control Language="VB" ClassName="LMSUserRelation" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.VirtualClass"%>
<%@ Import Namespace="Healthware.HP3.LMS.VirtualClass.ObjectValues"%>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagName="ctlLanguage" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"%>

<script runat="server">
    Private oLManager As New LanguageManager
    Private VCManager As New VirtualClassManager
    'Private ContentManager As New ContentManager
    Private MasterPageManager As New MasterPageManager
    
    Sub Page_load()
        If Not Page.IsPostBack Then
            ReadVirtualClass()
            ReadRelations()
        End If
    End Sub
    
    'recupera la classe selezionata
    Sub ReadVirtualClass()
        Dim VCSearcher As New VirtualClassSearcher
        VCSearcher.Key.Id = Request("V")
        
        Dim VCColl As VirtualClassCollection = VCManager.Read(VCSearcher)
        
        If Not (VCColl Is Nothing) AndAlso (VCColl.Count > 0) Then
            Dim CVValue As VirtualClassValue = VCColl(0)
            
            lblVCDescription.Text = CVValue.Description
            lblVCId.Text = CVValue.Key.Id
        End If
    End Sub
    
    'recupera le relazioni classe /utente
    Sub ReadRelations()
        VCManager.Cache = False
        Dim vIdent As New VirtualClassIdentificator
        vIdent.Id = Request("V")
        Dim UserColl As UserCollection = VCManager.ReadUserClassRelation(vIdent)
                       
        gridRelations.DataSource() = UserColl
        gridRelations.DataBind()
    End Sub
    
    'aggiunge una nuova relazione
    Sub AddRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Add") Then
            Dim userId As Integer = sender.commandArgument
                       
            VCManager.CreateUserClassRelation(New UserIdentificator(userId), New VirtualClassIdentificator(Request("V")))
            ReadRelations()
        End If
    End Sub
    
    'elimina una relazione classe / utente
    Sub RemoveRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Remove") Then
            Dim userId As Integer = sender.commandArgument
                       
            VCManager.RemoveUserClassRelation(New UserIdentificator(userId))
            ReadRelations()
        End If
    End Sub
    
    'ricerca i content in base ai valori selezionati nel form
    Sub SearchUser(ByVal sender As Object, ByVal e As EventArgs)
        Dim UserManager As New UserManager
        Dim userSearcher As New UserSearcher
       
        If (txtIdFilter.Text <> String.Empty) Then userSearcher.Key.Id = txtIdFilter.Text
        If (txtNameFilter.Text <> String.Empty) Then userSearcher.Name = txtNameFilter.Text
        If (txtSurnameFilter.Text <> String.Empty) Then userSearcher.Surname = txtSurnameFilter.Text
       
            
        Dim UserColl As UserCollection = UserManager.Read(userSearcher)
        
        gridUsers.DataSource() = UserColl
        gridUsers.DataBind()
    End Sub
    
    'fa un reset del form di ricerca
    Sub ResetResearch(ByVal sender As Object, ByVal e As EventArgs)
        txtIdFilter.Text = Nothing
        txtNameFilter.Text = Nothing
        txtSurnameFilter.Text = Nothing
    End Sub
    
    'ritorna alla lista delle classi virtuali
    Sub BackToManager(ByVal sender As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        Dim objQs As New ObjQueryString
        
        webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "VClass")
        webRequest.customParam.append(objQs)
        Response.Redirect(MasterPageManager.FormatRequest(webRequest))
    End Sub
</script>

<script type="text/javascript">
</script>

<hr />
<asp:Panel ID="pnlHeader" runat="server">
    <asp:Button ID="btBackToManager" Text="Archive" CssClass="button" runat="server" OnClick="BackToManager" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
</asp:Panel>
<hr />

<asp:Panel ID="pnlVirtualClass" runat="server">
    <asp:Panel ID="QuestSummary" Width="100%" runat="server">
        <div class="title">Virtual Class Selected</div>
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:30%">Title</th>
            </tr>
            <tr>
                 <td><asp:Label ID="lblVCId" runat="server" /></td>
                <td><asp:Label ID="lblVCDescription" runat="server" /></td>
           </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="true" runat="server">
            <div class="title">Active Relations</div>
            <asp:GridView ID="gridRelations"
                AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
                ShowHeader="true" HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" width="100%"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Name" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        <ItemTemplate>
                            <asp:Literal ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Surname" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        <ItemTemplate>
                            <asp:Literal ID="lblSurname" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Surname")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="RemoveRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
            </tr>
        </table>
        <asp:Table ID="tableSearchParameters" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>Id</asp:TableCell>
                <asp:TableCell>Name</asp:TableCell>
                <asp:TableCell>Surname</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="txtIdFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtNameFilter" runat="server" typecontrol="TextBox" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtSurnameFilter" runat="server" typecontrol="TextBox" /></asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchParameters2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchRelated" Enabled="true" OnClick="SearchUser" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
                <asp:TableCell><asp:Button ID="btnClearSearch" Enabled="true"  OnClick="ResetResearch" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <hr />
   
    <asp:Panel ID="pnlUsers" Width="100%" Visible="true" runat="server">
    <asp:Label runat="server" ID="msg"/>
        <asp:GridView ID="gridUsers" 
            AutoGenerateColumns="false"
            AllowPaging="true" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager" PageSize="20"
            AllowSorting="false"
            ShowHeader="true" HeaderStyle-CssClass="header"
            ShowFooter="true" FooterStyle-CssClass="gridFooter"
            GridLines="None" AlternatingRowStyle-BackColor="#eeefef" width="100%"
            runat="server">
            <Columns>
                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Literal ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>' />
                </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="Surname">
                    <ItemTemplate>
                        <asp:Literal ID="lblSurname" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Surname")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <asp:ImageButton ID="btn_add" ToolTip="Add content relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/insert.gif" onClick="AddRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
 </asp:Panel>   