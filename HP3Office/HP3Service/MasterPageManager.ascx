<%@ Control Language="VB" ClassName="ctlMasterPageManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private _typecontrol As ControlType = ControlType.View
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
    End Sub

    'effettua la read dell'item selezionato
    Function LoadValue() As MasterPageValue
        If SelectedId <> 0 Then
            Me.BusinessMasterPageManager.Cache = False
            Return Me.BusinessMasterPageManager.Read(New MasterPageIdentificator(SelectedId))
        End If
        Return Nothing
    End Function
    
    'Cancella fisicamente l'item selezionato
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        Try
            If SelectedId <> 0 Then
                ret = Me.BusinessMasterPageManager.Remove(New MasterPageIdentificator(SelectedId))
            End If
        Catch ex As Exception
        End Try
        Return ret
    End Function
    
    Sub LoadFormDett(ByVal oValue As MasterPageValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                Filename.Text = .FileName
                txtHideSite.Text = .KeySite.Id
                txtSite.Text = GetSiteDescription(.KeySite.Id)
                Description.Text = .Description
            End With
        Else
            lblDescrizione.InnerText = "New Event"
            lblId.InnerText = ""
            Filename.Text = Nothing
            Description.Text = Nothing
            txtHideSite.Text = Nothing
            txtSite.Text = Nothing
        End If
    End Sub
    
    Function GetSiteDescription(ByVal idSite As Integer) As String
        Try
            Return Me.BusinessSiteAreaManager.Read(idSite).Name
        Catch ex As Exception
        End Try
        Return String.Empty
    End Function
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New MasterPageValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Description = Description.Text
            .FileName = Filename.Text
            If txtHideSite.Text <> "" Then
                .KeySite.Id = txtHideSite.Text
            End If
        End With

        If SelectedId <= 0 Then
            oValue = Me.BusinessMasterPageManager.Create(oValue)
        Else
            oValue = Me.BusinessMasterPageManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        'strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
        
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As MasterPageSearcher = Nothing)
        Dim oCollection As New MasterPageCollection
        
        If Searcher Is Nothing Then
            Searcher = New MasterPageSearcher
        End If
        
        Me.BusinessMasterPageManager.Cache = False
        oCollection = Me.BusinessMasterPageManager.Read(Searcher)

        'If Not oCollection Is Nothing Then
        '    oCollection.Sort(SortType, SortOrder)
        'End If
        
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSearcher As New MasterPageSearcher
        If txtId.Text <> "" Then oSearcher.KeySite.Id = Integer.Parse(txtId.Text)
        If txtFileName.Text <> "" Then oSearcher.FileName = txtFileName.Text
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        'gridList.PageIndex = 0
        
        'Select Case e.SortExpression
        '    Case "Id"
        '        If SortType <> TraceEventGenericComparer.SortType.ById Then
        '            SortType = TraceEventGenericComparer.SortType.ById
        '        Else
        '            SortOrder = SortOrder * -1
        '        End If
        '    Case "Label"
        '        If SortType <> TraceEventGenericComparer.SortType.ByLabel Then
        '            SortType = TraceEventGenericComparer.SortType.ByLabel
        '        Else
        '            SortOrder = SortOrder * -1
        '        End If
        '    Case "Key"
        '        If SortType <> TraceEventGenericComparer.SortType.ByKey Then
        '            SortType = TraceEventGenericComparer.SortType.ByKey
        '        Else
        '            SortOrder = SortOrder * -1
        '        End If
        'End Select
        BindWithSearch(sender)
    End Sub
    
    'Private Property SortType() As TraceEventGenericComparer.SortType
    '    Get
    '        If Not ViewState("SortType") Is Nothing Then
    '            _sortType = ViewState("SortType")
    '        Else
    '            _sortType = TraceEventGenericComparer.SortType.ById ' SortType di Default                
    '        End If
    '        Return _sortType
    '    End Get
    '    Set(ByVal value As TraceEventGenericComparer.SortType)
    '        _sortType = value
    '        ViewState("SortType") = _sortType
    '    End Set
    'End Property
    
    'Private Property SortOrder() As TraceEventGenericComparer.SortOrder
    '    Get
    '        If Not ViewState("SortOrder") Is Nothing Then
    '            _sortOrder = ViewState("SortOrder")
    '        Else
    '            _sortOrder = TraceEventGenericComparer.SortOrder.DESC ' SortOrder di Default                
    '        End If
    '        Return _sortOrder
    '    End Get
    '    Set(ByVal value As TraceEventGenericComparer.SortOrder)
    '        _sortOrder = value
    '        ViewState("SortOrder") = _sortOrder
    '    End Set
    'End Property
    
    Public ReadOnly Property GetSelection() As MasterPageCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curValue As MasterPageValue
                    Dim outCollection As New MasterPageCollection
            
                    For Each str As String In CheckedItems
                        Me.BusinessMasterPageManager.Cache = False
                        curValue = Me.BusinessMasterPageManager.Read(New MasterPageIdentificator(Int32.Parse(str)))
                        If Not curValue Is Nothing Then
                            outCollection.Add(curValue)
                        End If
                    Next
                    Return outCollection
            End Select
        End Get
    End Property
    
    'Costruisce la struttura necessaria per il mantenimento
    'dello stato dei radio button    
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridList.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litsel")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    'Imposta l'option button selezionato per default
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    'Recupero lo stato dei radio button dal viewstate
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False

                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
</script>

<script type="text/javascript" >       
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">MasterPage Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>File Name</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><HP3:Text runat="server" ID="txtFileName" TypeControl="TextBox" style="width:200px"/></td>  
                    <td></td>  
                    <td></td>  
                    <td></td>  
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    AllowSorting="true"
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridList_Sorting"
                    emptydatatext="No item available"                
                    OnRowDataBound="gridList_RowDataBound"
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                    >
                  <Columns >
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:radiobutton ID="chkSel" runat="server" />
                            <asp:literal ID="litSel" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Site">
                            <ItemTemplate><%#GetSiteDescription(DataBinder.Eval(Container.DataItem, "KeySite.Id"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="20%" DataField ="FileName" HeaderText ="FileName" SortExpression="Label" />
                    <asp:BoundField HeaderStyle-Width="50%" DataField ="Description" HeaderText ="Description" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/><br /><br />
    </div>  
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
        <table class="form" style="width:100%">
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFileName&Help=cms_HelpMPFileName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFileName", "FileName")%></td>
                <td><HP3:Text runat ="server" ID="Filename" TypeControl ="TextBox" style="width:300px"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpMPDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextArea" style="width:300px"/></td>
            </tr>
            <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td style="width:50px"><asp:TextBox ID="txtHideSite" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtSite"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSite.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSite.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Site%>&ListId=<%=txtSite.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
       </table>
</asp:Panel>