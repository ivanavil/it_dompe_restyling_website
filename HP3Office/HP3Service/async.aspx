<%@ Page Language="VB" ClassName="myAsync" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="ICSharpCode.SharpZipLib.Checksums" %>
<%@ Import Namespace="ICSharpCode.SharpZipLib.Zip" %>

<script runat="server">
    'Da eliminare
    Public themeSearcher As New ThemeSearcher()
    Public themeKey As New ThemeIdentificator()
    Public themeID As New ThemeIdentificator()
    Public roleCollection As New RoleCollection

    Public siteId As Integer = 0
    Public _exportStrOut As String = ""
    
    Public strSessionId As String = ""
    Public dirPath As String = ""
    Public dirPathChild As String = ""
    Public dirName As String = ""
    Public selectedThemes As String = ""
    Public folderDirInfo As System.IO.DirectoryInfo()
    Public strDirWhereTozip As String
    Public strGeneratedXml As String = "XML Files<br /><br />"

    Sub Page_Load()
        Try
            If Not Request("asyncType") Is Nothing AndAlso Request("asyncType") <> "" Then
                Dim strRequest As String = Request("asyncType")
                Select Case strRequest.ToLower()
                    Case "enginespider"
                        IndexEngine()
                        
                    Case "exportwholecontent"
                        selectedThemes = "," & Request("selectedThemes") & ","
                        siteId = Request("siteId")
                        Response.Write("Load OK<br/>")
                        ExportWholeContent()
                    
                    Case "zipexportcontent"
                        siteId = Request("siteId")
                        strDirWhereTozip = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.DownloadPath.Replace("Download", "Image") & "\"
                        zipExportContent()
                                           
                End Select
            Else
                Response.End()
            End If
        Catch ex As Exception
            Response.Write("Errore nel page load -> " & ex.ToString)
        End Try
    End Sub
   
    
   
    
    'Launches Search Engine Spider 
    Sub IndexEngine()
        'versione del 07/04/2009 con Spider
        Dim _blOut As Boolean = True
        Dim so As New ContentSearcher
    
        Dim index As New IndexerModule()
        Dim info As Integer = -1
        
        Dim siteId As String = Request("siteId")
        Dim contentTypeId As String = Request("conTypeId")
        Dim siteAreaId As String = Request("siteAreaId")
        Dim LanguageId As String = Request("LanguageId")
        Dim keysContentType As String = Request("KeysCtype")
        
        Dim requestTimeOut As Integer = ConfigurationManager.AppSettings("RequestTimeOut")
       
        Try
            If (LanguageId <> "-1") Then so.key.IdLanguage = LanguageId
            
            If (contentTypeId <> "-1") Then
                so.KeyContentType.Id = contentTypeId
                
            ElseIf (keysContentType <> "-1") Then
                so.KeysContentType = keysContentType.ToString
            End If
           
            If (siteAreaId <> "0") Then so.KeySiteArea.Id = siteAreaId
            
            If (siteId <> "-1") Then
                so.KeySite.Id = siteId

                If requestTimeOut <> 0 Then Server.ScriptTimeout = requestTimeOut
                index.ActSpider(so)

            Else
                Response.Write("ko")
                Return
            End If
                        
        Catch ex As Exception
            _blOut = False
        End Try
        
        If _blOut Then
            Response.Write("ok" & info.ToString)
        Else
            Response.Write("ko" & info.ToString)
        End If
    End Sub
    
   
    'Launches zipping exported contents
    Sub zipExportContent()
        Dim strDirZipPath As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.DownloadPath.Replace("Download", "Image") & "\" & Me.ObjectTicket.User.Key.Id & "-" & siteId
        Dim strZipNameToCreate As String = "export-" & Me.ObjectTicket.User.Key.Id & "-" & siteId & ".zip"
        Dim isCreated As Boolean = CreateZipFile(strDirWhereTozip, strZipNameToCreate, New DirectoryInfo(strDirZipPath))
        If isCreated Then
            Response.Write(strZipNameToCreate)
        Else
            Response.Write("ko")
        End If
    End Sub
    
    Function CreateZipFile(ByVal wheretoZip As String, ByVal zipName As String, ByVal dirInfo As DirectoryInfo) As Boolean
        Return CreateZipFile(wheretoZip, zipName, CType(dirInfo, FileSystemInfo))
    End Function
    
    Function CreateZipFile(ByVal wheretoZip As String, ByVal zipName As String, ByVal fsInfo As FileSystemInfo) As Boolean
        Dim fsInfoAr As FileSystemInfo() = {fsInfo}
        Return CreateZipFile(wheretoZip, zipName, fsInfoAr)
    End Function
    
    Function CreateZipFile(ByVal wheretoZip As String, ByVal zipName As String, ByVal fsInfo As FileSystemInfo()) As Boolean
        Dim isCreated As Boolean = False
        Try
            Dim z As ZipFile = ZipFile.Create(wheretoZip & zipName)
            z.BeginUpdate()
            GetFilesToZip(fsInfo, z)
            z.CommitUpdate()
            z.Close()
            isCreated = True
        Catch ex As Exception
            isCreated = False
        End Try
        
        Return isCreated
    End Function
    
    Sub GetFilesToZip(ByVal fsInfo As FileSystemInfo(), ByVal z As ZipFile)
        If Not fsInfo Is Nothing And Not z Is Nothing Then
            For Each fi As FileSystemInfo In fsInfo
                If fi.Attributes = FileAttributes.Directory Then
                    Dim dInfo As DirectoryInfo = CType(fi, DirectoryInfo)
                    z.AddDirectory(dInfo.FullName)
                    GetFilesToZip(dInfo.GetFileSystemInfos(), z)
                Else
                    z.Add(fi.FullName)
                End If
            Next
                
        End If
    End Sub

    ' Launches content whole content exportation
    Sub ExportWholeContent()
        Response.Write("Export While begin OK<br/>")
        dirPath = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.DownloadPath.Replace("Download", "Image") & "\" & Me.ObjectTicket.User.Key.Id & "-" & siteId
        Response.Write("directory di destinazione " & dirPath & "<br/>")
        Try
            roleCollection = Me.BusinessMasterPageManager.GetTicket.Roles()
            Dim dInfo As New DirectoryInfo(dirPath)
            Response.Write(dInfo.Root.ToString & "<br/>")
            If dInfo.Exists Then
                dInfo.Delete(True)
                Dim fileInfo As New FileInfo(dirPath & ".zip")
                If fileInfo.Exists Then fileInfo.Delete()
            End If
        
            dInfo.Create()
            fillMenu(Request("mainTheme"), siteId)
            Response.Write(strGeneratedXml)
        Catch ex As Exception
            Response.Write("Errore nell'ExportWholeContent -> " & ex.ToString)
        End Try
    End Sub
    ----------------------------    

    'Create Single Xml Files given themeId
    Sub CreateXmlfile(ByVal oThemeValue As ThemeValue, ByVal themefolder As String)
        Response.Write("xmlfile OK" & themefolder & "<br/>")
        Try
            If selectedThemes.IndexOf("," & oThemeValue.Key.Id & ",") = -1 Then Exit Sub

            Dim oContSearcher As New ContentSearcher
            oContSearcher.SetMaxRow = 200
            oContSearcher.KeyContentType = oThemeValue.KeyContentType
            oContSearcher.KeySiteArea = oThemeValue.KeySiteArea
            oContSearcher.KeySite = oThemeValue.KeySite
            oContSearcher.Active = SelectOperation.All
            oContSearcher.key.IdLanguage = Request("langId")
            Me.BusinessContentManager.Cache = False

            Dim _oContentCollection As ContentCollection
            _oContentCollection = Me.BusinessContentManager.Read(oContSearcher)
            If Not _oContentCollection Is Nothing AndAlso _oContentCollection.Count > 0 Then
                Dim soExtra As New ContentExtraSearcher
                Dim soEvent As New CongressSearcher
                Dim ctm As New ContentTemplateManager
                Dim ct As ContentTemplateValue
                Dim ctv As ContentTypeValue
                Dim tv As ThemeValue
                ctv = Me.BusinessContentTypeManager.Read(oContSearcher.KeyContentType.Id)
                ct = ctm.Read(ctv.KeyContentsTemplate)
                tv = oThemeValue 'Me.BusinessThemeManager.Read(New ThemeIdentificator(Request("T")))
            
                Dim themeDescription As String = "NoTheme-"
                If Not tv Is Nothing Then
                    themeDescription = cutString(tv.Description) & "_" & tv.Key.Id 'CleanFileName()
                End If
            
                Dim objColl As New Object
                Select Case ct.Key.Id
                    Case 1
                        objColl = _oContentCollection
                        objColl = Me.BusinessContentManager.Read(oContSearcher)
                    Case 2
                        ClassUtility.Copy(oContSearcher, soExtra)
                        objColl = Me.BusinessContentExtraManager.Read(soExtra)
                    Case 3
                        ClassUtility.Copy(oContSearcher, soEvent)
                        objColl = Me.BusinessCongressManager.Read(soEvent)
                End Select
            
                Dim filename As String = themefolder & "\" + themeDescription + ".xml"
                strGeneratedXml += "::" & themeDescription + ".xml<br />"
                Dim str As String = ClassUtility.SerializeObjectToXmlString(objColl, Encoding.UTF8)
                Dim myXmlFile As New StreamWriter(filename, True)
                myXmlFile.Write(str)
                myXmlFile.Close()
                filename = ""
                str = ""
            End If
            _oContentCollection = Nothing
        Catch ex As Exception
            Response.Write("Errore nell'CreateXmlfile -> " & ex.ToString)
        End Try
    End Sub
    
    Function cutString(ByVal strToCut As String, Optional ByVal cutLength As Integer = 10) As String
        Dim _strOut As String = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(Server.HtmlDecode(strToCut)).Replace(" ", "").Replace("?", "").Replace("�", "").Replace("�", "")
        
        If cutLength > 0 Then
            If _strOut.Length() > cutLength Then _strOut = Left(_strOut, cutLength)
        End If
        
        Return _strOut
    End Function
    '----------------------------    
    
    Sub fillMenu(ByVal id As Integer, ByVal siteId As Integer, Optional ByVal round As Integer = 1)
        'Dim app As String = ""
        Try
            themeSearcher.KeyFather.Id = id
            themeSearcher.KeySite.Id = siteId
            themeSearcher.Active = SelectOperation.All
            themeSearcher.Display = SelectOperation.All
        
            Dim branch As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher)
            Dim i As Integer
            For i = 0 To branch.Count - 1
                'app = ""

                'men� relativo al sito selezionato    
                If branch(i).Active Then
                    'app = dirPath & "\" & cutString(branch(i).Description)
                    Dim folderDirInfo As New DirectoryInfo(dirPath & "\" & cutString(branch(i).Description) & "_" & branch(i).Key.Id)
                    If Not folderDirInfo.Exists Then folderDirInfo.Create()
                    CreateXmlfile(branch(i), dirPath & "\" & cutString(branch(i).Description) & "_" & branch(i).Key.Id)
                End If
                
                If HasChild(branch(i).Key.Id(), siteId) Then
                    Dim folderDirInfo As New DirectoryInfo(dirPath & "\" & cutString(branch(i).Description) & "_" & branch(i).Key.Id)
                    If Not folderDirInfo.Exists Then folderDirInfo.Create()
                    'CreateXmlfile(branch(i), dirPath & "\" & cutString(branch(i).Description) & "_" & branch(i).Key.Id)

                    dirPath += "\" & cutString(branch(i).Description) & "_" & branch(i).Key.Id
                    fillMenu(branch(i).Key.Id(), siteId, 1 + round)
                    dirPath = Left(dirPath, dirPath.LastIndexOf("\"))
                End If
            Next
        Catch ex As Exception
            Response.Write("Errore nel fillMenu -> " & ex.ToString)
            '_exportStrOut = "ko"
        End Try
    End Sub
    
    Function HasChild(ByVal idFather As Integer, ByVal idSite As Integer) As Boolean
        themeSearcher.KeyFather.Id = idFather
        themeSearcher.KeySite.Id = idSite
        themeSearcher.Active = SelectOperation.All
        themeSearcher.Display = SelectOperation.All
        If Me.BusinessThemeManager.Read(themeSearcher).Count > 0 Then Return True
        Return False
    End Function
    '----------------------------
     
</script>

