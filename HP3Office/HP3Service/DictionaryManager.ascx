<%@ Control Language="VB" ClassName="ctlDictionaryManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<script runat="server">
    Private oLManager As New LanguageManager
    Private oCManager As DictionaryManager
    Private oSearcher As DictionarySearcher
    Private oCollection As DictionaryCollection
    Private mPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    Private objCMSUtility As New GenericUtility
    Private utility As New WebUtility
    
    Private _sortType As DictionaryGenericComparer.SortType
    Private _sortOrder As DictionaryGenericComparer.SortOrder
    Private strDomain As String
    
    Private _typecontrol As ControlType = ControlType.View
    
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Copy
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As DictionaryValue
        If SelectedId <> 0 Then
            oCManager = New DictionaryManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
  
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New DictionaryManager
            Dim key As New DictionaryIdentificator
            key.Id = SelectedId
            ret = oCManager.remove(key)
        End If
        Return ret
    End Function
    
    Sub LoadFormDett(ByVal oValue As DictionaryValue)
       
        If TypeControl = ControlType.Copy Then SelectedId = 0
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Label & " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                'lSelector.DefaultValue = .KeyLanguage.Id
                lSelector.LoadLanguageValue(.KeyLanguage.Id)
                keyDomain.Text = .Key.Domain
                keyName.Text = .Key.Name
                Label.Text = .Label
                'Description.Value = .Description
                'Description.Content = .Description'telerik
                Description.Text = .Description
                Active.Checked = .Active
            End With
        Else
            lblDescrizione.InnerText = "New Label in Dictionary"
            lblId.InnerText = ""
            'lSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
            keyDomain.Text = Nothing
            keyName.Text = Nothing
            Label.Text = Nothing
            'Description.Value = Nothing
            'Description.Content = Nothing' Telerik
            Description.Text = Nothing
            Active.Checked = True
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New DictionaryValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .KeyLanguage.Id = lSelector.Language
            .Key.Domain = keyDomain.Text
            .Key.Name = keyName.Text
            .Label = Label.Text
            '.Description = Description.Value
            '.Description = Description.Content'Telerik
            .Description = Description.Text
            .Active = Active.Checked
        End With

        oCManager = New DictionaryManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Copy
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack) Then
            ReadLanguage()
        End If
        
        ShowRightPanel()
    End Sub
    
    'popola la dwl delle lingue nel searcher
    Sub ReadLanguage()
        Dim olangualeCollection As LanguageCollection
        Dim oLanguageSearcher As New LanguageSearcher
             
        olangualeCollection = Me.BusinessLanguageManager.Read(oLanguageSearcher)
        
        utility.LoadListControl(dwlLanguage, olangualeCollection, "Key.Code", "Key.Id")
        dwlLanguage.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As DictionarySearcher = Nothing)
        oCManager = New DictionaryManager
        oCollection = New DictionaryCollection
        
        If Searcher Is Nothing Then
            Searcher = New DictionarySearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New DictionarySearcher
        If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
        If txtLabel.Text <> "" Then oSearcher.SearchString = txtLabel.Text
        If txtDescription.Text <> "" Then oSearcher.SearchString = txtDescription.Text
        If dwlLanguage.SelectedItem.Value <> -1 Then oSearcher.KeyLanguage.Id = dwlLanguage.SelectedItem.Value
        
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> DictionaryGenericComparer.SortType.ById Then
                    SortType = DictionaryGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Label"
                If SortType <> DictionaryGenericComparer.SortType.ByLabel Then
                    SortType = DictionaryGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> DictionaryGenericComparer.SortType.ByKey Then
                    SortType = DictionaryGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As DictionaryGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = DictionaryGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As DictionaryGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As DictionaryGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = DictionaryGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As DictionaryGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "CopyItem"
                
                TypeControl = ControlType.Copy
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:button CssClass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Dictionary Searcher</span></td>
            </tr>
        </table>
        <br />
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>
                    <td><strong>Language</strong></td>
                    <td><strong>Label</strong></td>
                    <td><strong>Description</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><asp:DropDownList id="dwlLanguage"  runat="server" /></td>  
                    <td><HP3:Text runat="server" ID="txtLabel" TypeControl="TextBox" style="width:150px"/></td>  
                    <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:250px"/></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" onclick="Search" Text="Search" CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
   
    
    <asp:gridview ID="gridList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"
                AllowSorting="true"
                OnPageIndexChanging="gridList_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridList_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                >
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Identificator" SortExpression="Key">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Language" >
                    <ItemTemplate><img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "KeyLanguage.Id"))%>_small.gif" /></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="25%" DataField="Label" HeaderText ="Label" SortExpression="Label" />
                <asp:BoundField HeaderStyle-Width="30%"  DataField="Description" HeaderText="Description" />
                <asp:BoundField HeaderStyle-Width="5%" DataField="Active" HeaderText="Active" />
                <asp:TemplateField HeaderStyle-Width="11%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attenzione! Sicuro di voler cancellare?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkCopy" runat="server" ToolTip ="Copy item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="OnItemSelected" CommandName ="CopyItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="GoList" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button runat="server" ID="btnSave" onclick="SaveValue" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <table class="form" style="width:99%">
        <tr><td style="width:25%" valign="top"></td><td></td></tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat ="server" ID="keyDomain" TypeControl ="TextBox" style="width:80px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat ="server" ID="keyName" TypeControl ="TextBox" style="width:80px" MaxLength="10"/></td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguage&Help=cms_HelpDictionaryLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguage", "Language")%></td>
            <td><HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpDictionaryLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
            <td><HP3:Text runat ="server" ID="Label" TypeControl ="TextBox" style="width:600px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpDictionaryDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td> 
<%--            <FCKeditorV2:FCKeditor id="Description" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>             

            <asp:TextBox TextMode="MultiLine" Width="600px" Rows="10" Columns="100" ID="Description" runat="server"></asp:TextBox>

                <%--<telerik:RadEditor ID="Description" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                     <Content></Content>
                     <Snippets>
                         <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                     </Snippets>
                     <Languages>
                        <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                        <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                        <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                        <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                     </Languages>

                     <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     
                     
                     <Links>
                        <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                        <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                       </telerik:EditorLink>
                     </Links>
                     <Links>
                       <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                     </Links>        
                                         
                </telerik:RadEditor>--%>
                
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="Active" runat="server" /></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Panel>