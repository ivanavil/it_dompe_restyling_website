<%@ Control Language="VB" ClassName="ctlDictionaryManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private oCTmpManager As ContentTemplateManager
    Private oSearcher As ContentTemplateSearcher
    Private oCollection As ContentTemplateCollection
    Private oGenericUtlity As New GenericUtility
    Private dictionaryManager As New DictionaryManager
    Private mPageManager As New MasterPageManager()
    'Private _sortType As DictionaryGenericComparer.SortType
    'Private _sortOrder As DictionaryGenericComparer.SortOrder
    Private strDomain As String
    Private _typecontrol As ControlType = ControlType.View
    'Private objCMSUtility As New GenericUtility
    
    Private _strDomain As String
    Private _language As String
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As ContentTemplateValue
        If SelectedId <> 0 Then
            oCTmpManager = New ContentTemplateManager
            oCTmpManager.Cache = False
            Return oCTmpManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
  
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCTmpManager = New ContentTemplateManager
            Dim key As New ContentTemplateIdentificator
            key.Id = SelectedId
            ret = oCTmpManager.Remove(key)
        End If
        Return ret
    End Function
    
    ''' <summary>
    ''' Lettura delle descrizioni dei content type dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentTypeValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetContentType(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
    
    Sub LoadFormDett(ByVal oValue As ContentTemplateValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                Description.Text = .Description
                Manager.Text = .Manager
                ObjectValue.Text = .ObjectValue
                DLLName.Text = .DLLName
                
                txtHiddenContentTypeContainer.Text = .KeyCMSContainer.Id
                txtContentTypeContainer.Text = ReadContentTypeValue(.KeyCMSContainer.Id)
                
                txtHiddenContentTypeMaster.Text = .KeyCMSMaster.Id
                txtContentTypeMaster.Text = ReadContentTypeValue(.KeyCMSMaster.Id)
                
                txtHiddenContentTypeSlave.Text = .KeyCMSSlave.Id
                txtContentTypeSlave.Text = ReadContentTypeValue(.KeyCMSSlave.Id)
            End With
        Else
            lblDescrizione.InnerText = "New Content Template"
            lblId.InnerText = ""
            Description.Text = Nothing
            Manager.Text = Nothing
            ObjectValue.Text = Nothing
            DLLName.Text = Nothing
            txtHiddenContentTypeContainer.Text = Nothing
            txtContentTypeContainer.Text = Nothing
            txtHiddenContentTypeMaster.Text = Nothing
            txtContentTypeMaster.Text = Nothing
            txtHiddenContentTypeSlave.Text = Nothing
            txtContentTypeSlave.Text = Nothing
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New ContentTemplateValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Description = Description.Text
            .Manager = Manager.Text
            .ObjectValue = Manager.Text
            .DLLName = DLLName.Text
            
            If txtHiddenContentTypeContainer.Text <> "" Then
                .KeyCMSContainer.Id = txtHiddenContentTypeContainer.Text
            End If

            If txtHiddenContentTypeMaster.Text <> "" Then
                .KeyCMSMaster.Id = txtHiddenContentTypeMaster.Text
            End If
            
            If txtHiddenContentTypeSlave.Text <> "" Then
                .KeyCMSSlave.Id = txtHiddenContentTypeSlave.Text
            End If
        End With

        oCTmpManager = New ContentTemplateManager
        If SelectedId <= 0 Then
            oValue = oCTmpManager.Create(oValue)
        Else
            oValue = oCTmpManager.Update(oValue)
        End If
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContentTemplateSearcher = Nothing)
        oCTmpManager = New ContentTemplateManager
        oCollection = New ContentTemplateCollection
        
        If Searcher Is Nothing Then
            Searcher = New ContentTemplateSearcher
        End If
        oCTmpManager.Cache = False
        oCollection = oCTmpManager.Read(Searcher)
        'If Not oCollection Is Nothing Then
        '    oCollection.Sort(SortType, SortOrder)
        'End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        'oSearcher = New DictionarySearcher
        'If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
        'If txtLabel.Text <> "" Then oSearcher.SearchString = txtLabel.Text
        'If txtDescription.Text <> "" Then oSearcher.SearchString = txtDescription.Text
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:button CssClass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
    <asp:gridview ID="gridList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"
                AllowSorting="true"
                OnPageIndexChanging="gridList_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridList_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
                <Columns >
                    <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="38%"  DataField="Description" HeaderText="Description" />
                    <asp:BoundField HeaderStyle-Width="25%" DataField="Manager" HeaderText="Manager" />
                    <asp:BoundField HeaderStyle-Width="25%" DataField="DLLName" HeaderText="DLLName" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attenzione! Sicuro di voler cancellare?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="GoList" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button runat="server" ID="btnSave" onclick="SaveValue" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div> 
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContentTemplDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemplManager&Help=cms_HelpContentTemplManager&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemplManager", "Manager")%></td>
            <td><HP3:Text runat ="server" ID="Manager" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemplObjectV&Help=cms_HelpContentTemplObjectV&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemplObjectV", "Object Value")%></td>
            <td><HP3:Text runat ="server" ID="ObjectValue" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemDLLName&Help=cms_HelpContentTemDLLName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemDLLName", "DLL Name")%></td>
            <td><HP3:Text runat ="server" ID="DLLName" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemKeyCmsC&Help=cms_HelpContentTemKeyCmsC&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemKeyCmsC", "KeyCMSContainer")%></td>
            <td>
                <table width="100%">
                    <tr>
                    <td width="50px"><asp:TextBox ID="txtHiddenContentTypeContainer" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentTypeContainer"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                    <td><input type="button" value="..." class="button" onclick="window.open('<%=strDomain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentTypeContainer.clientid%>').value + '&ctlHidden=<%=txtHiddenContentTypeContainer.clientid%>&ctlVisible=<%=txtContentTypeContainer.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemKeyCmsM&Help=cms_HelpContentTemKeyCmsM&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemKeyCmsM", "KeyCMSMaster")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td width="50px"><asp:TextBox ID="txtHiddenContentTypeMaster" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentTypeMaster" TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td><td><input type="button" value="..." class="button" onclick="window.open('<%=strDomain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentTypeMaster.clientid%>').value + '&ctlHidden=<%=txtHiddenContentTypeMaster.clientid%>&ctlVisible=<%=txtContentTypeMaster.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemKeyCMSS&Help=cms_HelpContentTemKeyCMSS&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemKeyCMSS", "KeyCMSSlave")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td width="50px"><asp:TextBox ID="txtHiddenContentTypeSlave" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentTypeSlave" TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td><td><input type="button" value="..." class="button" onclick="window.open('<%=strDomain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentTypeSlave.clientid%>').value + '&ctlHidden=<%=txtHiddenContentTypeSlave.clientid%>&ctlVisible=<%=txtContentTypeSlave.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
