<%@ Control Language="VB" ClassName="ContentContent" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Implements Interface="System.Web.UI.ICallbackEventHandler" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName ="Language" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private webRequest As New WebRequestValue
    Private _webUtility As New WebUtility
    Private dateUtility As New StringUtility
    Private objQs As New ObjQueryString
    Private oLManager As New LanguageManager()
   
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    ' il datasource corrente
    Private _dataSource As ContentCollection = Nothing
    
    Private _allowEdit As Boolean = True
    Private _allowSelect As Boolean = False
    Private _useInPopup As Boolean = False

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue
    
    Private _strDomain As String
    Private _language As String
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property AllowSelect() As Boolean
        Get
            Return _allowSelect
        End Get
        Set(ByVal value As Boolean)
            _allowSelect = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
  
    
    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                '1149
                _keyContent = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
        End Set
    End Property
    
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As New ContentCollection
     
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        contentCollection = _contentManager.Read(so)
        
        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function

    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public ReadOnly Property GlobalKeyTextFilter() As String
        Get
            Return txtGlobalKeyFilter.Text
        End Get
    End Property
    
    Public ReadOnly Property PrimaryKeyTextFilter() As String
        Get
            Return txtPrimaryKeyFilter.Text
        End Get
    End Property
    
    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)
        'Legge il datasource completo 
        getDataSource(clearCache)
       
        'Effettua il bind del datasource sulla gridview
        gridContents.DataSource = _dataSource
        gridContents.DataBind()
    End Sub
   
    Protected Sub gridContents_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridContents.DataBound
        If Not _dataSource Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
            Dim startIndex As Integer = gridContents.PageSize * gridContents.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            msg.Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", s, startIndex + 1, startIndex + gridContents.Rows.Count, gridViewTotalCount, s)
        Else
            msg.Text = "No items found"
        End If
    End Sub

    Protected Sub gridContents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridContents.PageIndexChanging
        gridContents.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        txtGlobalKeyFilter.Text = ""
        txtPrimaryKeyFilter.Text = ""
        filterDataSource()
    End Sub
   

    Private Sub LoadContentSummary()
        If Not KeyContent Is Nothing AndAlso KeyContent.Id > 0 Then
            lblContentId.Text = ContentValue.Key.Id
            lblContentPk.Text = ContentValue.Key.PrimaryKey
            lblContentType.Text = ContentValue.ContentType.Description
            If ContentValue.DatePublish.HasValue Then
                lblContentDatePublish.Text = getCorrectedFormatDate(ContentValue.DatePublish)
            Else
                lblContentDatePublish.Text = ""
            End If
            imgContentLanguage.ImageUrl = "/HP3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
            imgContentLanguage.Visible = True
            lblContentTitle.Text = ContentValue.Title
        Else
            imgContentLanguage.Visible = False
        End If
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not AllowEdit Then
            'pnlHeader.Visible = False
            ContentSummary.Visible = False
        End If
        
        If Not Page.IsCallback Then
            Dim callbackRef As String
            callbackRef = Page.ClientScript.GetCallbackEventReference(Me, "getCbParams()", "callbackReorderItems", Nothing, "callbackErrorReorderItems", True)
        End If
        
        If Not Page.IsPostBack Then
            ContentType.LoadControl()
            LoadContentSummary()
            ReadRelations()
            Site.LoadControl()
            
            If (Request("s") <> 0) Then
                Site.SetSelectedValue(Request("s"))
                Site.Disable()
            End If
           
        End If
        
        If objQs.ContentId = 0 Then 'And Not UseInPopup Then
            'SearchEngine.DropDownContentType.Items.Clear()
            ' _webUtility.LoadListControl(SearchEngine.DropDownContentType, ContentTypes, "Description", "Key.Id")
            SearchEngine.Visible = True
            pnlRelationEdit.Visible = False
        Else
            SearchEngine.Visible = False
            If Not Page.IsPostBack Then
                'LoadControl()
            End If
        End If
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
        
    Sub GoRelationType(ByVal s As Object, ByVal e As EventArgs)
        ReadRelationType()
        DisablePanelRelation()
        
        btBackToList.Visible = False
        btGoRelationType.Visible = False
    End Sub
    
    'legge tutti i tipi di relazioni
    Sub ReadRelationType()
        Dim contentRelationTypeSearcher As New ContentRelationTypeSearcher
        Dim contentRelationTypeColl As New ContentRelationTypeCollection
        
        Me.BusinessContentManager.Cache = False
        contentRelationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(contentRelationTypeSearcher)
        Me.BusinessContentManager.Cache = True
        gdw_relatioTypeList.DataSource = contentRelationTypeColl
        gdw_relatioTypeList.DataBind()
        
        pnlRelationTypeManager.Visible = True
        pnlGrid.Visible = True
        pnlDett.Visible = False
            
    End Sub
    
    Private ReadOnly Property ContentTypes() As ContentTypeCollection
        Get
            If _contentTypeCollection Is Nothing Then
                Dim _contentTypeSearcher As New ContentTypeSearcher
                _contentTypeSearcher.OnlyAssociatedContents = True
                _contentTypeCollection = Me.BusinessContentTypeManager.Read(_contentTypeSearcher)
            End If
            Return _contentTypeCollection
        End Get
    End Property

    Public Function GridContentRowsSelected() As ContentCollection
        Return SearchEngine.GridContentRowsSelected()
    End Function
    
    Private Function getDataSource() As ContentCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As ContentCollection
        If _dataSource Is Nothing Or clearCache Then
            Dim _ContentSearcher As New ContentSearcher()
            '1149
            _ContentSearcher.Active = SelectOperation.All
            _ContentSearcher.Display = SelectOperation.All
            
            Dim ctc As ContentTypeCollection = ContentType.GetSelection()
                     
            If (Request("s") <> 0) Then
               
                _ContentSearcher.KeySite.Id = Request("s")
            End If
            
            'se viene scelto il contentType dalla dwnList
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                _ContentSearcher.KeyContentType.Id = ctc.Item(0).Key.Id
            Else 'se non � stato scelto il contentType dalla dwl
                webRequest.customParam.LoadQueryString()
                Dim contentType = webRequest.customParam.item("Ct")
                'se la siteArea � diversa da CMS-Servizi
                'e cio� ci troviamo nel caso News-Events ecc..
                If Not (Me.BusinessMasterPageManager.GetSiteArea.Name = "Servizi") And Not (Me.BusinessMasterPageManager.GetSiteArea.Domain = "CMS") Then
                    'filtro la ricerca dei content in base ala contentType recuperato dal customParam del webRequest
                    _ContentSearcher.KeyContentType.Id = contentType
                End If
                
            End If
            If Not DataValueFilter Is Nothing AndAlso DataValueFilter <> "" Then
                ' _ContentSearcher.key = New ContentIdentificator(Integer.Parse(DataValueFilter))
                _ContentSearcher.key.Id = DataValueFilter
            End If
            If Not DataTextFilter Is Nothing AndAlso DataTextFilter <> "" Then
                _ContentSearcher.SearchString = DataTextFilter
            End If
            
            If Not GlobalKeyTextFilter Is Nothing AndAlso GlobalKeyTextFilter <> "" Then
                _ContentSearcher.key.IdGlobal = GlobalKeyTextFilter
            End If
            
            If Not PrimaryKeyTextFilter Is Nothing AndAlso PrimaryKeyTextFilter <> "" Then
                _ContentSearcher.key.PrimaryKey = PrimaryKeyTextFilter
            End If
            
            If lSelector.Language > 0 Then
                _ContentSearcher.key.IdLanguage = lSelector.Language
            End If
            
            Dim oSiteAreaValue As SiteAreaValue
            oSiteAreaValue = GetSiteSelection(Site)
            If Not oSiteAreaValue Is Nothing Then
                _ContentSearcher.KeySite.Id = oSiteAreaValue.Key.Id
            End If
            
            Dim _oContentManager As New ContentManager()
            _oContentManager.Cache = Not clearCache
            _dataSource = _oContentManager.Read(_ContentSearcher)
        End If
        Return _dataSource
    End Function
       
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Private Overloads Sub LoadControl()
        'Dim _oContentCollection As ContentCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
      
        If objQs.ContentId > 0 Then
            '1149
            Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        End If
        BindGrid()
    End Sub
    
    Protected Sub SearchEngine_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arr() As String = sender.CommandArgument.split("_")
        Dim id As Int32 = arr(0)
        Dim idLanguage As Int32 = arr(1)
        
        objQs.ContentId = id
        objQs.Language_id = idLanguage
        webRequest.customParam.append(objQs)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub
    
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
        Response.Clear()
        'SimpleLogger.Log("Callback params: " & eventArgument)
        
        Dim params() As String = eventArgument.Split("&")
        If params(0).Split("=")(1) = "reorderItems" Then
            Dim items As String = params(1).Split("=")(1)
            reorderItems(items)
        End If
        Response.End()
    End Sub
    
    Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
        Return "OK"
    End Function
    
    Private Sub reorderItems(ByVal items As String)
        Try
            If Not items Is Nothing Then
                Dim arrayId() = items.Split(",")
                If Not arrayId Is Nothing AndAlso arrayId.Length > 0 Then
                    SimpleLogger.Log("reordering items: " & items & " - count = " & arrayId.Length)
                    Dim _oContentManager As New ContentManager
                    Dim pkey As String
                    Dim _oRelationValue As New ContentRelationValue
                    Dim _oCRelationSearcher As New ContentRelationSearcher
                    Dim _oCRelationCollection As New ContentRelationCollection
                    Dim _oOrder As Integer = 0
                    'SimpleLogger.Log(cc.Count & KeyContent.PrimaryKey)
                    For Each pkey In arrayId
                        'SimpleLogger.Log("+ reordering item: " & pkey)
                        _oCRelationSearcher.KeyContent.PrimaryKey = KeyContent.PrimaryKey
                        _oCRelationSearcher.KeyContentRelated.PrimaryKey = Integer.Parse(pkey)
                        
                        '1149
                        _oCRelationSearcher.ActiveContent = SelectOperation.All
                        _oCRelationSearcher.DisplayContent = SelectOperation.All
                        _oCRelationSearcher.ActiveContentRelated = SelectOperation.All
                        _oCRelationSearcher.DisplayContentRelated = SelectOperation.All
            
                        
                        _oCRelationCollection = _oContentManager.ReadContentRelation(_oCRelationSearcher)
                        If Not _oCRelationCollection Is Nothing AndAlso _oCRelationCollection.Count > 0 Then
                            _oRelationValue = _oCRelationCollection(0)
                            _oRelationValue.Order = _oOrder
                        End If
                        _oContentManager.UpdateContentRelation(_oRelationValue)
                        _oOrder += 1
                    Next
                End If
            End If
        Catch ex As Exception
            Throw New Exceptions.BusinessException(ex.ToString())
        End Try
    End Sub

    Protected Sub editRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim RelationId As Integer = sender.commandArgument.ToString
        Dim ContentId As Integer = objQs.ContentId
                
        BindRelationType()
        DisablePanelRelation()
        pnl_Edit.Visible = True
        
        SelectedId = RelationId
        
        Dim contentRelationValue As New ContentRelationValue
        Dim contentRelationIdent As New ContentRelationIdentificator
        contentRelationIdent.Id = RelationId
       
        Me.BusinessContentManager.Cache = False
        Dim so As New ContentRelationSearcher
        so.Key = contentRelationIdent
        so.CustomQuery = "AllRelation"
       
        '1149
        so.ActiveContent = SelectOperation.All
        so.DisplayContent = SelectOperation.All
        so.ActiveContentRelated = SelectOperation.All
        so.DisplayContentRelated = SelectOperation.All
            
        contentRelationValue = Me.BusinessContentManager.ReadContentRelation(so)(0)
        Me.BusinessContentManager.Cache = True
                
        If (contentRelationValue.KeyRelationType.Id <> 0) Then
            dwlRelationType.SelectedIndex = dwlRelationType.Items.IndexOf(dwlRelationType.Items.FindByValue(contentRelationValue.KeyRelationType.Id))
        End If
    End Sub
    
    Protected Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oContentManager As New ContentManager()
        Dim _oRelationValue As New ContentRelationValue
        
        _oRelationValue.Key.Id = key
        Dim bol As Boolean = _oContentManager.RemoveContentRelation(_oRelationValue)
        
        ReadRelations()
    End Sub
    
    Protected Sub addRelation(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oContentManager As New ContentManager()
        Dim _oRelationValue As New ContentRelationValue
        With _oRelationValue
            .KeyContent = KeyContent
            '1149
            .KeyContentRelation = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(key)
            .KeyRelationType.Id = 1 'relazione di tipo standard
        End With
        
        Dim bol As Boolean = _oContentManager.CreateContentRelation(_oRelationValue)
        
        ReadRelations()
    End Sub
    
    'permette di impostare il tipo ad una relazione Content/Content  
    Sub saveRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim contentRelationValue As New ContentRelationValue
               
        contentRelationValue.Key.Id = SelectedId
        Dim relationIdent As New ContentRelationIdentificator
        relationIdent.Id = SelectedId
        Dim so As New ContentRelationSearcher
        so.Key = relationIdent
        so.CustomQuery = "AllRelation"
        
        '1149
        so.ActiveContent = SelectOperation.All
        so.DisplayContent = SelectOperation.All
        so.ActiveContentRelated = SelectOperation.All
        so.DisplayContentRelated = SelectOperation.All
        
        contentRelationValue = Me.BusinessContentManager.ReadContentRelation(so)(0)
        contentRelationValue.KeyRelationType.Id = dwlRelationType.SelectedItem.Value
        
        Me.BusinessContentManager.UpdateContentRelation(contentRelationValue)
        GoContentRelation()
    End Sub
    
    Sub ReadRelations()
        If Not KeyContent Is Nothing AndAlso KeyContent.PrimaryKey > 0 Then
            Dim _oCollection As ContentRelationCollection
            Dim _oSearcher As New ContentRelationSearcher()
            
            '1149
            _oSearcher.ActiveContent = SelectOperation.All
            _oSearcher.DisplayContent = SelectOperation.All
            _oSearcher.ActiveContentRelated = SelectOperation.All
            _oSearcher.DisplayContentRelated = SelectOperation.All
            
            _oSearcher.KeyContent = KeyContent
            _oSearcher.CustomQuery = "AllRelation"
            
            Me.BusinessContentManager.Cache = False
            _oCollection = Me.BusinessContentManager.ReadContentRelation(_oSearcher)
            Me.BusinessContentManager.Cache = True
            
            gridRelation.DataSource = _oCollection
            gridRelation.DataBind()
        End If
    End Sub
    
    Protected Sub gridRelation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowIndex >= 0 Then
            'e.Row.ID = "td_content_" & DataBinder.Eval(e.Row.DataItem, "Key.Id") & "_" & DataBinder.Eval(e.Row.DataItem, "Key.IdLanguage") & "_" & DataBinder.Eval(e.Row.DataItem, "Key.PrimaryKey")
            Dim img As Image = e.Row.Cells(0).FindControl("dragHandler")
            If Not img Is Nothing Then
                img.Attributes.Add("onmousedown", "makeDraggable(document.getElementById('" & e.Row.ClientID & "'));")
            End If
        End If
    End Sub
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazione del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
      
    'Bind della dwl relativa ai relation type
    Sub BindRelationType()
        Dim relationTypeSearcher As New ContentRelationTypeSearcher
        Dim relationTypeColl As ContentRelationTypeCollection
        
        relationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(relationTypeSearcher)
        
        For index As Integer = 0 To relationTypeColl.Count - 1
            Dim text As String = relationTypeColl(index).Description
            If (relationTypeColl(index).Domain <> String.Empty And relationTypeColl(index).Name <> String.Empty) Then text = text & " " & "(" & relationTypeColl(index).Domain & "-" & relationTypeColl(index).Name & ")"
            dwlRelationType.Items.Insert(index, New ListItem(text, relationTypeColl(index).Key.Id))
        Next
        ' _webUtility.LoadListControl(dwlRelationType, relationTypeColl, "Description", "Key.Id")
        dwlRelationType.Items.Insert(0, "--Select Type--")
    End Sub
    
    'gestisce la creazione di un nuovo relation type
    Sub newRelationType(ByVal sender As Object, ByVal e As EventArgs)
        pnlDett.Visible = True
        pnlGrid.Visible = False
        
        SelectedId = 0
        relationTypeDescription.Text = Nothing
        relationTypeDomain.Text = Nothing
        relationTypeName.Text = Nothing
    End Sub
    
    'riporta alla gestione delle relazioni content/content
    Sub GoContentRelation(ByVal sender As Object, ByVal e As EventArgs)
        GoContentRelation()
    End Sub
    
    'riporta alla gestione delle relazioni content/content
    Sub GoContentRelation()
        webRequest.customParam.LoadQueryString()
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
    'carica l'archivio delle relation type
    Sub LoadRelationTypeArchive(ByVal sender As Object, ByVal e As EventArgs)
        ReadRelationType()
    End Sub
    
    'gestisce l'update di una relation type
    Sub UpdateRelationType(ByVal sender As Object, ByVal e As EventArgs)
        Dim contentRelationTypeValue As New ContentRelationTypeValue
        
        contentRelationTypeValue.Description = relationTypeDescription.Text
        contentRelationTypeValue.Domain = relationTypeDomain.Text
        contentRelationTypeValue.Name = relationTypeName.Text
        
        If (SelectedId = 0) Then
            Me.BusinessContentManager.CreateContentRelationType(contentRelationTypeValue)
        Else
            contentRelationTypeValue.Key.Id = SelectedId
            Me.BusinessContentManager.UpdateContentRelationType(contentRelationTypeValue)
        End If
        
        ReadRelationType()
    End Sub
    
    'gestisce l'edit di una relation type
    Sub EditRelationType(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim relationTypeId As Integer = sender.CommandArgument().ToString
        
        Dim contentRelationTypeSearcher As New ContentRelationTypeSearcher
        Dim contentRelationTypeColl As New ContentRelationTypeCollection
        
        contentRelationTypeSearcher.Key.Id = relationTypeId
        SelectedId = relationTypeId
        
        BusinessContentManager.Cache = False
        contentRelationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(contentRelationTypeSearcher)
        If Not (contentRelationTypeColl Is Nothing) Then
            relationTypeDescription.Text = contentRelationTypeColl(0).Description
            relationTypeName.Text = contentRelationTypeColl(0).Name
            relationTypeDomain.Text = contentRelationTypeColl(0).Domain
            pnlDett.Visible = True
            pnlGrid.Visible = False
        End If
    End Sub
    
    'gestisce la cancellazione di una relation type
    Sub RemoveRelationType(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim relationTypeId As Integer = sender.CommandArgument().ToString
        
        Dim contentRelationTypeIdent As New ContentRelationTypeIdentificator
        contentRelationTypeIdent.Id = relationTypeId
        
        Me.BusinessContentManager.RemoveContentRelationType(contentRelationTypeIdent)
        
        ReadRelationType()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Function getTypeRelation(ByVal relation As ContentRelationValue) As String
        Dim contentRelationTypeSearcher As New ContentRelationTypeSearcher
        contentRelationTypeSearcher.Key.Id = relation.KeyRelationType.Id
        
        If (relation.KeyRelationType.Id <> 0) Then
            Dim typeColl As ContentRelationTypeCollection = Me.BusinessContentManager.ReadContentRelationtType(contentRelationTypeSearcher)
           
            If Not (typeColl Is Nothing) Then
                Return typeColl(0).Description
            End If
        End If
          
             
        Return "--"
    End Function
    
    Sub DisablePanelRelation()
        ContentSummary.Visible = False
        pnlActiveRelations.Visible = False
        gridContents.Visible = False
        pnlRelatedSearch.Visible = False
        msg.Visible = False
        pnl_Edit.Visible = False
    End Sub
    
    Function getContentTypeDescription(ByVal id As Integer) As String
        Try
            Return Me.BusinessContentTypeManager.Read(id).Description
        Catch ex As Exception
        End Try
        Return String.Empty
    End Function
        
</script>

<script type="text/javascript" language="javascript" src="~/HP3Office/Js/CallBackObject.js"></script>
<script type="text/javascript">

    // *** BEGIN DRAG & DROP CODE ***
    // var opacity = parseFloat(0.8);
    
    document.onmousemove = mouseMove;
    document.onmouseup   = mouseUp;
    
    var dragObject  = null;
    var mouseOffset = null;
    var targetRow   = null;
    var rowsTop     = null;
    var rowsId      = null;
    var inCallback = false;

    function getMouseOffset(target, e) {
	    e = e || window.event;
	    var docPos = getPosition(target);
	    var mousePos = mouseCoords(e);
	    return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
    }

    function getPosition(e) {
	    var left = 0;
	    var top  = 0;

	    while (e.offsetParent) {
		    left += e.offsetLeft;
		    top  += e.offsetTop;
		    e     = e.offsetParent;
	    }

	    left += e.offsetLeft;
	    top  += e.offsetTop;

	    return {x:left, y:top};
    }
    
    function mouseMove(e) {
	    e            = e || window.event;
	    var mousePos = mouseCoords(e);

	    if (dragObject) {
	        if (!rowsTop) init();
	        if (!targetRow) makeTargetRow(dragObject);
            targetRow.style.display = 'block';

            // controlla se spostare la riga target
            var table = getTable();
            var targetIndex = table.rows.length-1; //dragObject.rowIndex;
            var mouseY = 0;
            
            for (var i=1; i<=table.rows.length; i++) {
            
                mouseY = parseInt(parseInt(getPosition(dragObject).y)); 
                if (i == 1 && (mouseY <= rowsTop[i])) {
                    targetIndex = i;
                    break;
                } else if (i == table.rows.length && (mouseY >= rowsTop[i])) {
                    targetIndex = i;
                    break;
                } else if (rowsTop[i-1] <= mouseY && mouseY <= rowsTop[i]) {
                    targetIndex = i;
                    break;
                }
            }
            table.moveRow(targetRow.rowIndex, targetIndex);
	        
	        var tablePos = getPosition(getTable());
	    
		    dragObject.style.position = 'absolute';
		    dragObject.style.top      = mousePos.y - mouseOffset.y;
		    dragObject.style.left     = mousePos.x - mouseOffset.x;
		    
		    return false;
	    }
    }
    
    function mouseUp(e) {
        if (dragObject != null) {
            if (targetRow != null) {
                var table = getTable();
                table.removeChild(dragObject);
                table.appendChild(dragObject);

                var targetRowIndex = targetRow.rowIndex;
                table.moveRow(targetRow.rowIndex, table.rows.length - 1);
                table.moveRow(dragObject.rowIndex, targetRowIndex);
                targetRow.style.display = 'none';
		        dragObject.style.position = '';

                alternateRowStyle();
                makeIdArray();
                reorderItems();
            }
		    //dragObject.cells[4].style.display = 'block';
            dragObject.onmousedown = null;
	        dragObject = null;
	    }
    }

    function mouseCoords(e) {
	    if (e.pageX || e.pageY) {
		    return { x:e.pageX, y:e.pageY };
	    }
	    return {
		    x:e.clientX + document.body.scrollLeft - document.body.clientLeft,
		    y:e.clientY + document.body.scrollTop  - document.body.clientTop
	    };
    }
    
    function makeDraggable(item) {
	    if (!item || inCallback) return;
	    item.onmousedown = function(e) {
		    dragObject  = this;
		    dragObject.style.border = 'solid 1px #eeeeee';
		    //dragObject.cells[4].style.display = 'none';
		    mouseOffset = getMouseOffset(this, e);
		    return false;
	    }
    }
    
    function getTable() {
        var table = document.getElementById('<%=gridRelation.clientId %>');
        var tbody = table.getElementsByTagName('tbody').item(0);
        return tbody;
    }
    
    function init() {
        var table = getTable();
        rowsTop = new Array(table.rows.length);
        
        for (var i=1; i<table.rows.length; i++) {
            rowsTop[i] = getPosition(table.rows[i]).y;
        }
        makeIdArray();
    }
    
    function makeIdArray() {
        var table = getTable();
        rowsId = new Array(table.rows.length - 2);
        var j = 0;
        for (var i=1; i<table.rows.length; i++) {
            if (table.rows[i].pk != 'target_row') {
                rowsId[j++] = Trim(table.rows[i].cells[3].innerHTML);
            }
        }
    }
    
    function makeTargetRow() {
        var table = getTable();
        targetRow = document.createElement('tr');
        targetRow.style.border = 'solid 1px #eeeeee';
        targetRow.pk = 'target_row';
        var td = document.createElement('td');
        td.colSpan = getTable().parentElement.rows[0].cells.length;
        var div = document.createElement('div');
        div.style.width = '100%';
        div.style.height = '17px';
        div.style.margin = '0px';
        div.style.border = 'dotted 1px #ff0000';
        
        td.appendChild(div);
        targetRow.appendChild(td);
        table.appendChild(targetRow);
    }
    
    function alternateRowStyle() {
        var table = getTable();
        var color = '#ffffff';
        for (var i = 0; i < table.rows.length; i++) {
            if (i % 2 == 0) {
                var color = '#eeefef';
            } else {
                var color = '#ffffff';
            }
            table.rows[i].style.backgroundColor = color;
        }
        targetRow.style.backgroundColor = '#ffffff';
    }
    
    function getCbParams() {
        var params = "action=reorderItems"
        params += "&items=";
        for (var i=0; i<rowsId.length; i++) {
            params += rowsId[i] + ((i<rowsId.length-1)?',':'');
        }
        return params;
    }
    
    function reorderItems() {
        inCallback = true;
        WebForm_DoCallback('<%=Me.clientId() %>',getCbParams(),callbackReorderItems,null,callbackErrorReorderItems,true)
    }
    
    function callbackReorderItems(result, context) {
        inCallback = false;
    }
    
    function callbackErrorReorderItems(result, context) {
        inCallback = false;
    }
        
    // *** END DRAG & DROP CODE ***
 
</script>
<hr />
<HP3:SearchEngine ID="SearchEngine" visible="false" OnlyAssociatedContents="True" UseInRelation ="true"  AllowEdit="false" AllowSelect="true" ShowContentType="true" runat="server" OnSelectedEvent="SearchEngine_SelectedEvent" />

<asp:Panel ID="pnlRelationEdit" runat="server">
<asp:Button ID="btBackToList" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
<asp:Button ID="btGoRelationType" Text="Go Relation Type" CssClass="button" runat="server" OnClick="GoRelationType" />

  <br />
    <br />
    <%--Pannello per l'edit di una relazione  --%>  
    <asp:Panel ID="pnl_Edit" Width="100%" runat="server" Visible="false">
    <asp:Button id ="btn_SaveRelation" runat="server" Text="Save" OnClick="saveRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
        <div class="title" style="margin-bottom:20px">Relation between Title-Title</div>
        <br />
        <table class="form" width="99%">
              <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Relation Type")%></td>
                <td><asp:DropDownList id="dwlRelationType" runat="server"/></td>
             </tr>
        </table>
    </asp:Panel>
    
    <%--Pannello del Content Selezionato --%>    
    <asp:Panel ID="ContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div>
                <br />
        <table class="tbl1">
            <tr>
                <th style="width:5%">Id</th>
                <th style="width:5%">Pk</th>
                <th style="width:10%">Date Publish</th>
                <th style="width:2%"></th>
                <th style="width:58%">Title</th>
                <th style="width:20%">Content Type</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentPk" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
                <td><asp:Label ID="lblContentType" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
   
    
    <%--Pannello delle Relazioni attive--%>
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="True" runat="server">
    <hr />   
        <div class="title">Active Relations</div>
          <br />
           <asp:GridView ID="gridRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
                OnRowDataBound="gridRelation_RowDataBound"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="1%" ItemStyle-Width="1%">
                        <ItemTemplate>
                            <asp:Image ID="dragHandler" ImageUrl="~/hp3Office/HP3Image/Ico/dragHandler.gif" ToolTip="Drag this item to change order" style="cursor:move;" runat="server" /> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContentRelation.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Pk" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblPk" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContentRelation.PrimaryKey")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "KeyContentRelation.IdLanguage"))%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Title">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Relation Type">
                        <ItemTemplate>
                            <%#getTypeRelation(Container.DataItem) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Content Type" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <%#getContentTypeDescription(Container.DataItem.KeyContentType.Id)%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-Width="7%">
                        <ItemTemplate>
                         <asp:ImageButton ID="btn_edit" runat="server" ToolTip ="Edit relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="editRelation" CommandName ="SelectItem" CommandArgument ='<%#Container.Dataitem.Key.Id%>'  />
                         <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif"  OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
            
    </asp:Panel>
    
    
    <%-- Pannello del Content Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" Width="100%" cssclass="boxSearcher" Visible="true" runat="server">
    <hr /> 
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td style="width:70px">Id</td>
                <td style="width:70px">GlobalKey</td>
                <td style="width:70px">PrimaryKey</td>
                <td>Title</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" Style="width:60px"/></td>
                <td><HP3:ctlText ID="txtGlobalKeyFilter" runat="server" TypeControl="NumericText" Style="width:60px"/></td>
                <td><HP3:ctlText ID="txtPrimaryKeyFilter" runat="server" TypeControl="NumericText" Style="width:60px"/></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox"  Style="width:300px"/></td>

            </tr>
            </table>
            <table class="form">
            <tr>
                
                <td>Language</td>
                <td>Site</td> 
                <td>Content Type</td>                              
            </tr>
            <tr>
                <%--<td><HP3:ctlContentType ID="ContentType" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></td>--%>
                <td><HP3:Language ID="lSelector" runat="server"  Typecontrol="WithSelectControl" /></td>
                <td><HP3:ctlSite runat="server" ID="Site" TypeControl ="combo" SiteType="Site" ItemZeroMessage="---Select Site---"/></td>
                <td><HP3:ctlContentType ID="ContentType" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    <hr />
    </asp:Panel>
    
 
    <%--Griglia dei Content--%>
    <asp:Label runat="server" ID="msg" />
    <asp:GridView ID="gridContents" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="false" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server">
        <Columns>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="15%">
                <ItemTemplate>
  <a title="Id: <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>"><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></a>
  <a title="Global Key: <%#DataBinder.Eval(Container.DataItem, "Key.IdGlobal")%>"><%#DataBinder.Eval(Container.DataItem, "Key.IdGlobal")%></a>
  <a title="Primary Key: <%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>"><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></a>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  HeaderText="Title">
                <ItemTemplate>
                    <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>

<%--Pannello per la gestione delle relation type (edit,new,delete)--%>
<asp:Panel id="pnlRelationTypeManager"  Width="100%" runat="server" Visible="false">
     
     <%--Griglia RelationType--%>
    <asp:Panel id="pnlGrid" runat="server" visible="false"> 
        <div style="margin-bottom:10px">
            <asp:Button id ="btnNewRelationType" runat="server" Text="New" OnClick="newRelationType"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
            <asp:Button id ="btnRelationManager" runat="server" Text="Go Content Relations" OnClick="GoContentRelation"  CssClass="button" />
        </div>
        
        <asp:Label CssClass="title" runat="server" id="sectionTit">Relation Type List</asp:Label>
        <asp:gridview id="gdw_relatioTypeList" 
                        runat="server"
                        AutoGenerateColumns="false" 
                        GridLines="None" 
                        CssClass="tbl1"
                        HeaderStyle-CssClass="header"
                        AlternatingRowStyle-BackColor="#E9EEF4"
                        AllowPaging="true"
                        PagerSettings-Position="TopAndBottom"  
                        PagerStyle-HorizontalAlign="Right"
                        PagerStyle-CssClass="Pager"             
                        AllowSorting="true"
                        PageSize ="20"
                        emptydatatext="No item available">
                      <Columns >
                        <asp:TemplateField HeaderStyle-Width="2%" HeaderText="ID">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Description">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderStyle-Width="5%">
                            <ItemTemplate>
                               <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRelationType" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                               <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClick="RemoveRelationType" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                      </Columns>
         </asp:gridview>
    </asp:Panel>

    <%---Dettaglio RelationType---%>
    <asp:Panel id="pnlDett" runat="server" visible="false">
         <div>
            <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadRelationTypeArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
            <asp:Button id ="btnUpdate" runat="server" Text="Save" OnClick="UpdateRelationType" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
         </div>
         
         <table  style="margin-top:10px" class="form" width="99%">
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpRelationTypeDescr&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                <td><HP3:Text runat ="server" id="relationTypeDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
                <td><HP3:Text runat ="server" id="relationTypeDomain" TypeControl ="TextBox" style="width:200px" MaxLength="5"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
                <td><HP3:Text runat ="server" id="relationTypeName" TypeControl ="TextBox" style="width:200px" MaxLength="10"/></td>
            </tr>
        </table>
    </asp:Panel>
    
</asp:Panel>