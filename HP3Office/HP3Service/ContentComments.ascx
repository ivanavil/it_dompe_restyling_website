﻿<%@ Control Language="VB" ClassName="ContentComment" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.Utility"%> 

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private dictionaryManager As New Healthware.HP3.Core.Localization.DictionaryManager
    Private strJs As String
    Private _domain As String = String.Empty
    Private _language As Integer = 0
    Private _idcomm As Integer = 0
    
    Private commentManager As New Healthware.HP3.Core.Content.CommentManager
    Private oLManager As New languagemanager
    
    Private Const Pending As Integer = Healthware.HP3.Core.Content.ObjectValues.CommentValue.EnumStatus.Pending
    Private Const Approved As Integer = Healthware.HP3.Core.Content.ObjectValues.CommentValue.EnumStatus.Approved
    Private Const Deleted As Integer = Healthware.HP3.Core.Content.ObjectValues.CommentValue.EnumStatus.Deleted
        
    Private Const IsPublic As Integer = Healthware.HP3.Core.Content.ObjectValues.CommentValue.EnumVisibile.IsPublic
    Private Const IsOnlyAuthor As Integer = Healthware.HP3.Core.Content.ObjectValues.CommentValue.EnumVisibile.IsOnlyAuthor
    
    
    Property Domain() As String
        Get
            Return _domain
        End Get
        Set(ByVal value As String)
            _domain = value
        End Set
    End Property
    
    Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
      
    Public Property SelectedId() As Integer
        Get
            Return ViewState("selId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selId") = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.PageObjectGetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not Page.IsPostBack Then
            LoadContentInfo()
            LoadRelatedComments()
            ActiveGridPanel()
            
        End If
    End Sub

    
    Sub LoadRelatedComments()
        Dim so As New CommentSearcher
      
        Dim contId As Integer = Request.QueryString("C")
        Dim contLang As Integer = Request.QueryString("L")
        Dim siteId As Integer = Request.QueryString("S")
        
        If (contId <> 0 And contLang <> 0 And siteId <> 0) Then
            so.KeyContent.Id = contId
            so.KeyContent.IdLanguage = contLang
            so.KeySite.Id = siteId
        End If
      
        so.Status = dwlStatus.SelectedValue
        commentManager.Cache = False
        Dim commentColl As CommentCollection = commentManager.Read(so)
        
        gdw_commentsList.DataSource = commentColl
        gdw_commentsList.DataBind()
       
        
    End Sub
    
    Sub LoadContentInfo()
        Dim contId As Integer = Request.QueryString("C")
        Dim contLang As Integer = Request.QueryString("L")
        
        Dim so As New ContentSearcher
        so.key = New ContentIdentificator(contId, contLang)
        so.Active = SelectOperation.All
        
        Dim coll As ContentCollection = Me.BusinessContentManager.Read(so)
        
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            Dim content As ContentValue = coll(0)
                        
            contentId.Text = Content.Key.Id
            contentKey.Text = Content.Key.PrimaryKey
            contType.Text = Content.ContentType.Description
            contTitle.Text = Content.Title
        End If
       
    End Sub
    
    Sub DoSearch(ByVal sender As Object, ByVal e As EventArgs)
         LoadRelatedComments()
    End Sub
    
    
    Sub ViewItem(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim commentId As Integer = sender.CommandArgument
        SelectedId = commentId
                   
        If commentId <> 0 Then
            commentManager.Cache = False
                             
            
            Dim so As New CommentSearcher()
            so.key.Id = commentId
            so.Status = dwlStatus.SelectedValue
            
            Dim commcoll As CommentCollection = commentManager.Read(so)
            
          
            If Not commcoll Is Nothing AndAlso commcoll.Count > 0 Then
                Dim commvalue As CommentValue = commcoll(0)
                
                If commvalue.DateCreate.HasValue Then commDateCreation.Value = commvalue.DateCreate
               
                commText.Text = commvalue.Text
                commTitle.Text = commvalue.Title
                commAuthor.Text = commvalue.Author.Name & " " & commvalue.Author.Surname
                commAuthorEmail.Text = commvalue.Author.Email
                commUserIP.Text = commvalue.UserIP
                commVisibility.Text = commvalue.Visible.ToString
                commchkNotify.Checked = commvalue.Notify
                commdwlStatus.SelectedValue = commvalue.Status
            Else
                ClearForm()
            End If
            
        Else
            ClearForm()
           
        End If
        
        
        ActiveDettPanel()
        DisableAllFormElements()
    End Sub
    
    Sub ClearForm()
        commText.Text = String.Empty
        commTitle.Text = String.Empty
        commAuthor.Text = String.Empty
        commAuthorEmail.Text = String.Empty
        commUserIP.Text = String.Empty
        commVisibility.Text = String.Empty
        commchkNotify.Checked = False
        commdwlStatus.SelectedValue = Pending
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        LoadRelatedComments()
        ActiveGridPanel()
    End Sub
    
    Sub ChangeStatus(ByVal sender As Object, ByVal e As EventArgs)
        Dim idComm As Integer = SelectedId
        Dim bool As Boolean = False
        
        Dim status As String = commdwlStatus.SelectedValue
        If idComm <> 0 Then
            bool = commentManager.Update(New CommentIdentificator(idComm), "Status", commdwlStatus.SelectedValue)
        End If
        
        If bool Then strJs = "alert('The Status has been updated successful!')"
        
        LoadRelatedComments()
        ActiveGridPanel()
    End Sub
   
     
    Sub DisableAllFormElements()
        commText.Disable()
        commTitle.Disable()
        commAuthor.Disable()
        commAuthorEmail.Disable()
        commUserIP.Disable()
        commVisibility.Disable()
               
        commDateCreation.Enable = False
        commDateCreation.Disable()
       
        commchkNotify.Enabled = False
    End Sub
    
    Sub ActiveGridPanel()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        pnlSearcher.Visible = True
    End Sub
    
    Sub ActiveDettPanel()
        pnlGrid.Visible = False
        pnlDett.Visible = True
        pnlSearcher.Visible = False
    End Sub
    
    Function getStatus(ByVal vo As CommentValue) As String
        Dim _status As String = String.Empty
              
        Select Case vo.Status
            Case Pending
                _status = "content_noactive"
            Case Approved
                _status = "content_active"
            Case Deleted
                _status = "content_noactive"
        End Select
     
        Return _status
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
     
    Protected Sub gdw_commList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindGrid(sender)
    End Sub
      
    Sub BindGrid(ByVal objGrid As GridView)
        Dim so As New CommentSearcher
      
        Dim contId As Integer = Request.QueryString("C")
        Dim contLang As Integer = Request.QueryString("L")
        Dim siteId As Integer = Request.QueryString("S")
        
        If (contId <> 0 And contLang <> 0 And siteId <> 0) Then
            so.KeyContent.Id = contId
            so.KeyContent.IdLanguage = contLang
            so.KeySite.Id = siteId
        End If
      
        so.Status = dwlStatus.SelectedValue
        commentManager.Cache = False
        Dim commentColl As CommentCollection = commentManager.Read(so)
        
        objGrid.DataSource = commentColl
        objGrid.DataBind()
       
              
       
    End Sub
</script>
<script type="text/javascript">
       <%=strJs%>                
</script>


<hr/>

<asp:HiddenField  ID="Actualpage"  runat="server" Value="1"/>
<asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Comment Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form" width="100%">
                <tr>
                    <td><strong>Status</strong></td>    
                    <td></td>
                </tr>
                <tr>                
                    <td style="width:40px">
                         <asp:DropDownList  id="dwlStatus" cssClass="select" runat="server">
                            <asp:ListItem  Selected="True" Text="Pending" Value="0"></asp:ListItem>
                            <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
                            <asp:ListItem  Text="Delete" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                   </td>
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" onclick="DoSearch" Text="Search" cssclass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
 </asp:Panel>

<asp:Panel id="pnlGrid" runat="server" visible="false">
<br /> 
 <hr />

    <div id="contentinfo" class="form">

    <fieldset>
        <legend style= 'color:#336699; font-weight:bold'>Content Info:</legend>
        (<asp:Label id="contentId" runat="server"    Text="" Font-Bold="True"  ForeColor="#336699" ToolTip="Content Id"/>)
        (<asp:Label  id="contentKey" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Content Primary Key"/>)
        <asp:Label  id="contType" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Content Type"/>|
        <asp:Label  id="contTitle" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Title"/>
     </fieldset>

    </div>
<hr />
<br /> 

 <asp:Label CssClass="title" runat="server" id="sectionTit">Comments List</asp:Label>

 <asp:gridview id="gdw_commentsList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                OnPageIndexChanging="gdw_commList_PageIndexChanging"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager" 
                AllowSorting="true"
                PageSize ="20"
                Emptydatatext="No item available">
              <Columns >
                <asp:TemplateField HeaderText="Key.Id" HeaderStyle-Width="3%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                  
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <img id="imgLang" src='<%#"/HP3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(Container.DataItem.KeyContent.IdLanguage) & "_small.gif"%>'  title="Lang" alt=""/>
                     </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Title">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="DateCreate">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "DateCreate")%></ItemTemplate>
                </asp:TemplateField>
                 
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="View item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="ViewItem" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <img id="imgStatus" src='<%#"/HP3Office/HP3Image/Ico/" & getStatus(Container.DataItem) & ".gif"%>'  title="Status" alt=""/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>



 <asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Change Status" OnClick="ChangeStatus" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>

 <table  style=" margin-top:10px" class="form" width="99%">
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStatus&Help=cms_HelpStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormStatus", "Status")%> </td>
        <td>
            <asp:DropDownList  id="commdwlStatus" cssClass="select" runat="server">
               <asp:ListItem  Text="Pending" Value="0"></asp:ListItem>
               <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
               <asp:ListItem  Text="Delete" Value="2"></asp:ListItem>
             </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateCreation&Help=cms_HelpDateCreation&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormDateCreation", "Date Creation")%> </td>
        <td><HP3:Date runat="server" ID="commDateCreation" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormTitle", "Title")%> </td>
        <td><HP3:Text runat="server" ID="commTitle" TypeControl="TextBox" Style="width:600px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormText&Help=cms_HelpText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormText", "Text")%> </td>
        <td><HP3:Text runat="server" ID="commText" TypeControl="TextArea" Style="width:600px;height:400px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAuthorName&Help=cms_HelpAuthorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormAuthorName", "Author Complete Name")%></td>
        <td><HP3:Text runat ="server" id="commAuthor" TypeControl ="TextBox" style="width:200px" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAuthorEmail&Help=cms_HelpAuthorEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormAuthorEmail", "Author Email")%></td>
        <td><HP3:Text runat ="server" id="commAuthorEmail" TypeControl ="TextBox" style="width:300px" /></td>
    </tr>
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserIP&Help=cms_HelpUserIP&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormUserIP", "User IP")%></td>
        <td><HP3:Text runat ="server" id="commUserIP" TypeControl ="TextBox" style="width:300px" /></td>
    </tr>
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNotify&Help=cms_HelpNotify&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormNotify", "Notify")%></td>
        <td><asp:checkbox ID="commchkNotify" runat="server" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormVisibility&Help=cms_HelpVisibility&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormVisibility", "Visibility")%></td>
        <td><HP3:Text runat ="server" id="commVisibility" TypeControl ="TextBox" style="width:300px" /></td>
    </tr>
 </table>
</asp:Panel>


 