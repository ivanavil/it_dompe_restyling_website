﻿<%@ Control Language="VB" ClassName="ContentContexts" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
 

<script runat="server">
    
    ' l'id del context corrente
    Private _keyContext As ContextIdentificator
    ' l'istanza del Context corrente
    Private _contextValue As ContextValue
    
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    Private Domain As String = String.Empty
    Private Language As String = String.Empty
    
    Private _webUtility As WebUtility
    Private dateUtility As New StringUtility
    
    Private oLManager As New LanguageManager()
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    Private comboContextGroupBinded As Boolean = False
    
    
    Private oCollection As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    ' il datasource corrente
    Private _dataSource As ContextCollection = Nothing
    ' la lista degli id che hanno una relazione con il content
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue
    
    Private utility As New WebUtility

    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    ' ----- M1222
    Public Property KeyContext() As ContextIdentificator
        Get
            If _keyContext Is Nothing Then
                _keyContext = New ContextIdentificator()
                webRequest.customParam.LoadQueryString()
                If webRequest.customParam.item("KeyContext") <> String.Empty Then _keyContext.Id = Integer.Parse(webRequest.customParam.item("KeyContext"))
                If webRequest.customParam.item("ContextLanguage") <> String.Empty Then _keyContext.Language = New LanguageIdentificator(Integer.Parse(webRequest.customParam.item("ContextLanguage")))
            End If
            Return _keyContext
        End Get
        Set(ByVal value As ContextIdentificator)
            _keyContext = value
        End Set
    End Property
    ' ----- M1222
    Private Sub readContextValue()
        Dim _contextManager As New ContextManager
        _contextManager.Cache = False
        _contextValue = _contextManager.Read(KeyContext)
    End Sub
    ' ----- M1222
    Public Property ContextValue() As ContextValue
        Get
            If (_contextValue Is Nothing OrElse _contextValue.Key.Id = 0) AndAlso Not KeyContext Is Nothing Then
                readContextValue()
            End If
            Return _contextValue
        End Get
        Set(ByVal value As ContextValue)
            _contextValue = value
        End Set
    End Property
    
    
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)

        ' Legge il datasource completo 
        getDataSource(clearCache)

       
        ' Effettua il bind del datasource sulla gridview
        gridContext.DataSource = _dataSource
        gridContext.DataBind()
    End Sub

 
 

    Protected Sub gridRelations_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridContext.DataBound

        If gridContext.FooterRow Is Nothing Then Return
     
        For cellNum As Integer = gridContext.Columns.Count - 1 To 1 Step -1
            Try
                gridContext.FooterRow.Cells.RemoveAt(cellNum)
            Catch ex As Exception
            End Try
        Next
        gridContext.FooterRow.Cells(0).ColumnSpan = gridContext.Columns.Count
        gridContext.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        If Not _dataSource Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
            Dim startIndex As Integer = gridContext.PageSize * gridContext.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            gridContext.FooterRow.Cells(0).Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", _
                s, startIndex + 1, startIndex + gridContext.Rows.Count, gridViewTotalCount, s)
        Else
            gridContext.FooterRow.Cells(0).Text = "No items found."
        End If
    End Sub


    
    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridContext
        End Get
    End Property

    '1222 Carica il dettaglio del context Principale
    Private Sub LoadContextSummary()
        
        If KeyContext.Id > 0 Then
            lblContextId.Text = ContextValue.Key.Id
            If Not ContextValue.Key.Language Is Nothing Then
                imgContextLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContextValue.Key.Language.Id) & "_small.gif"
                imgContextLanguage.Visible = True
            Else
                imgContextLanguage.Visible = False
            End If
            lblContextDescription.Text = ContextValue.Description
        Else
            imgContextLanguage.Visible = False
        End If

    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollection = siteAreaManager.Read(siteAreaSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        
        Domain = WebUtility.MyPage(Me).DomainInfo
        Language = Me.BusinessMasterPageManager.GetLang.Id
     
        If Not AllowEdit Then
            'pnlHeader.Visible = False
            ContextSummary.Visible = False
        End If
        
        If Not Page.IsPostBack Then
            LoadContextSummary()
            BindComboGroup()
            ReadRelations()
            BindDomains()
         
            
        End If
        
        If KeyContext.Id = 0 And Not UseInPopup Then
            Dim _webUtility As New WebUtility
            pnlRelationEdit.Visible = False
        Else
            If Not Page.IsPostBack Then
                LoadControl()
            End If
        End If
         
    End Sub
    
    Function GetSiteDomain() As String
        siteAreaManager = New SiteAreaManager
        Dim siteareavalue As SiteAreaValue = siteAreaManager.Read(New SiteAreaIdentificator(Request("s")))
        If Not (siteareavalue Is Nothing) Then Return siteareavalue.Key.Domain
        Return Nothing
    End Function
    
    Private ReadOnly Property ContentTypes() As ContentTypeCollection
        Get
            If _contentTypeCollection Is Nothing Then
                Dim _contentTypeManager As New ContentTypeManager
                Dim _contentTypeSearcher As New ContentTypeSearcher
                _contentTypeSearcher.OnlyAssociatedContents = True
                _contentTypeCollection = _contentTypeManager.Read(_contentTypeSearcher)
            End If
            Return _contentTypeCollection
        End Get
    End Property

    Sub BindComboGroup()
        If comboContextGroupBinded Then Exit Sub
        
        Dim _oContextManager As New ContextManager()
        Dim _oContextGroupCollection As ContextGroupCollection
        Dim _oContextGroupSearcher As New ContextGroupSearcher
        _oContextGroupSearcher.Key.Language = New LanguageIdentificator()
        _oContextGroupCollection = _oContextManager.ReadGroup(_oContextGroupSearcher)

        ContextGroup.Items.Clear()
        ContextGroup.Items.Add(New ListItem("--Select Context--", 0))
        ContextGroup.Items.Add(New ListItem("Context without group", -1))
        ContextGroup.Items.Add(New ListItem("All contexts", 0))
        If Not _oContextGroupCollection Is Nothing Then
            Dim _oContextGroup As ContextGroupValue
            ContextGroup.Items.Add(New ListItem("---------------------------------------------------------------", -2))
            
            For Each _oContextGroup In _oContextGroupCollection
                ContextGroup.Items.Add(New ListItem(_oContextGroup.Description, _oContextGroup.Key.Id))
            Next
            
            ContextGroup.SelectedIndex = 0
        End If
        
        comboContextGroupBinded = True

    End Sub

    Private Function getDataSource() As ContextCollection
        Return getDataSource(False)
    End Function
    
 
    Private Function getDataSource(ByVal clearCache As Boolean) As ContextCollection
        If _dataSource Is Nothing Then
            Dim _ContextSearcher As New ContextSearcher()
                      
            Dim selGroupID As Integer = 0
            Dim sqlContextID As Integer = 0
            
            If Not ContextGroup Is Nothing AndAlso Not ContextGroup.SelectedValue Is Nothing And Integer.TryParse(ContextGroup.SelectedValue, selGroupID) Then
                _ContextSearcher.KeyContextGroup.Id = selGroupID
            End If
            
            If Not String.IsNullOrEmpty(DataValueFilter) AndAlso Integer.TryParse(DataValueFilter, selGroupID) Then
                _ContextSearcher.Key = New ContextIdentificator(selGroupID)
            End If
            
            If Not String.IsNullOrEmpty(DataTextFilter) Then
                _ContextSearcher.Description = "%" & DataTextFilter & "%"
            End If
            
            If Not dwlDomain Is Nothing AndAlso dwlDomain.SelectedItem.Value <> "-1" Then _ContextSearcher.Key.Domain = dwlDomain.SelectedItem.Value
            
            Dim _oContextManager As New ContextManager()
          
            _oContextManager.Cache = False
            _dataSource = _oContextManager.Read(_ContextSearcher)
             
        End If
        Return _dataSource
    End Function
 
    
    Private Function readRelated(ByVal keyContext As ContextIdentificator) As ContextsRelationCollection
        Dim _oContextRelatedCollection As ContextsRelationCollection = Nothing
        Dim _oContextManager As New ContextManager
        Dim oSearcher = New ContextsRelationSearcher
        If _related Is Nothing AndAlso Not keyContext Is Nothing Then
         
            oSearcher.KeyContext.Id = keyContext.Id
            oSearcher.LoadRelatedContextTitle = True
            oSearcher.LoadRelationTitle = True
            _oContextManager.Cache = False
            _oContextRelatedCollection = _oContextManager.ReadContextsRelation(oSearcher)
          
        End If
        Return _oContextRelatedCollection
    End Function
 
    Private Overloads Sub LoadControl()
     
        Dim _oContextGroupCollection As ContextCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
        Dim _oContextManager As New ContextManager
     
        readRelated(KeyContext)
        
        BindGrid()
        pnlRelationEdit.Visible = True
  
    End Sub
    
    Protected Sub SearchEngine_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arr() As String = sender.CommandArgument.split("_")
        Dim id As Int32 = arr(0)
        Dim idLanguage As Int32 = arr(1)
        
        objQs.ContentId = id
        objQs.Language_id = idLanguage
        webRequest.customParam.append(objQs)
        Dim mPageManager As New MasterPageManager
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub

    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
 
    
    'aggiunge una nuova relazione context/Context
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
      
       
        Dim key As Integer = sender.commandArgument.ToString
 
        Dim oRel As New ContextsRelationValue
        oRel.KeyContext.Id = KeyContext.Id
        oRel.KeyRelatedContext.Id = key
        Me.BusinessContextManager.CreateContextsRelation(oRel)
 
         
        ReadRelations()
    End Sub
     
    'rimuove una relazione context/content
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        'Key della relazione
        Dim key As Integer = sender.commandArgument.ToString
        Dim so As New ContextsRelationSearcher
        so.Key.Id = key
        Me.BusinessContextManager.RemoveContextsRelation(so)
        ReadRelations()
    End Sub
    
    'legge le relazioni context/context
    Sub ReadRelations()
        
  
        Dim oSearcher As New ContextsRelationSearcher
        oSearcher.KeyContext.Id = Me.KeyContext.Id
       
        oSearcher.LoadContextTitle = True
        oSearcher.LoadRelationTitle = True
    
        Me.BusinessContextManager.Cache = False
        Dim _oContextCollection As ContextsRelationCollection = Me.BusinessContextManager.ReadContextsRelation(oSearcher)
       
      
        
        gridActiveRelation.DataSource = _oContextCollection
        gridActiveRelation.DataBind()
       
    End Sub
    
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridContext.PageIndexChanging
        'SaveRelatedStatus()
        gridContext.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    Private Sub ActiveEdit(ByVal active As Boolean)
        pnlEdit.Visible = active
        pnlRelatedSearch.Visible = Not active
        pnlActiveRelations.Visible = Not active
        gridContext.Visible = Not active
    End Sub
    
    'Bind della dwl relativa ai relation type
    Sub BindRelationType()
        Dim relationTypeSearcher As New ContentRelationTypeSearcher
        Dim relationTypeColl As ContentRelationTypeCollection
        
        
        If dwlRelationType.Items.Count = 0 Then
        
            relationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(relationTypeSearcher)
        
            If Not relationTypeColl Is Nothing Then
                For index As Integer = 0 To relationTypeColl.Count - 1
                    Dim text As String = relationTypeColl(index).Description
                    If (relationTypeColl(index).Domain <> String.Empty And relationTypeColl(index).Name <> String.Empty) Then text = text & " " & "(" & relationTypeColl(index).Domain & "-" & relationTypeColl(index).Name & ")"
                    dwlRelationType.Items.Insert(index, New ListItem(text, relationTypeColl(index).Key.Id))
                Next
                ' _webUtility.LoadListControl(dwlRelationType, relationTypeColl, "Description", "Key.Id")
                dwlRelationType.Items.Insert(0, New ListItem("--No Type--", "0"))
            End If
        End If
        
    End Sub
    
    Private Function GetRelationValue(ByVal id As Integer, ByVal loadContextTitle As Boolean) As ContextsRelationValue
        Dim coll As ContextsRelationCollection
        Dim so As New ContextsRelationSearcher
        Dim vo As ContextsRelationValue = Nothing
        so.LoadRelatedContextTitle = loadContextTitle
        so.Key = New Identificator(id)
        Me.BusinessContextManager.Cache = False
        coll = Me.BusinessContextManager.ReadContextsRelation(so)
        
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            vo = coll(0)
        End If
        Return vo
    End Function
    
    
    Protected Sub editRelation(ByVal sender As Object, ByVal e As System.EventArgs)
        ActiveEdit(True)
        '
        Dim key As Integer = sender.commandArgument.ToString

        
        Dim vo As ContextsRelationValue = GetRelationValue(key, True)
        
        If Not vo Is Nothing Then
            txtOrder.Text = vo.Order
            txtKeyRelation.Text = vo.Key.Id
            lblContextName.Text = vo.RelatedContextTitle
            BindRelationType()
                
            If (vo.KeyRelationType.Id > 0) Then
                dwlRelationType.SelectedIndex = dwlRelationType.Items.IndexOf(dwlRelationType.Items.FindByValue(vo.KeyRelationType.Id))
            End If
            
            
        End If
             
    End Sub
    Protected Sub saveRelation(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vo As ContextsRelationValue = GetRelationValue(txtKeyRelation.Text, False)
 
        If txtOrder.Text <> "" Then vo.Order = Int32.Parse(txtOrder.Text)
        vo.KeyRelationType.Id = dwlRelationType.SelectedItem.Value
 
        Me.BusinessContextManager.UpdateContextsRelation(vo)
        
        ActiveEdit(False)
        ReadRelations()
    End Sub
    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        'SaveRelatedStatus()
        filterDataSource()
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        
        'If Not AllowEdit Then Return
        'Response.Redirect(GetBackUrl)
    End Sub

    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue

        objQs.ContentId = 0

        webRequest.customParam.append(objQs)
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
</script>

<%--<script type="text/javascript">
    function enableApplyButton(state) {
        if (state == null) state = true;
        try {
            document.getElementById('<%=btApply.ClientId %>').disabled = !state;
        } catch (e) {
        }
    }
</script>--%>
<hr />
<HP3:SearchEngine ID="SearchEngine" visible="false" UseInRelation ="true"  AllowEdit="false" AllowSelect="true" ShowContentType="true" runat="server" OnSelectedEvent="SearchEngine_SelectedEvent" />
<hr />

<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="Button1" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
     <%--Pannello del Content Selezionato--%>  
     <asp:Panel ID="ContextSummary" Width="100%" runat="server">
        <div class="title">Context Selected</div> <br />
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:5%"></th>
                <th style="width:90%">Description</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContextId" runat="server" /></td>
                <td><asp:Image ID="imgContextLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContextDescription" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />


     <%--Pannello delle Relazioni attive--%>
      <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div><br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Context Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "KeyRelatedContext.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                              <%#DataBinder.Eval(Container.DataItem, "RelatedContextTitle")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="RelationType" HeaderStyle-Width="5%" ItemStyle-Width="20%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "RelationTitle")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Order" HeaderStyle-Width="5%" ItemStyle-Width="20%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Order")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                            <asp:ImageButton ID="btn_edit" runat="server" ToolTip ="Edit relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="editRelation" CommandName ="SelectItem" CommandArgument ='<%#Container.Dataitem.Key.Id%>'/>
                    
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
                <hr />
     
     <br />
    </asp:Panel>

    <%-- Pannello del Context Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Context Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Description</td>
                <td>Context Group</td>
                <td>Domain</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" Style="width:100px;" runat="server" TypeControl="NumericText" /></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></td>
                <td><asp:DropDownList ID="ContextGroup" runat="server" /></td>
                <td style="width:40px"><asp:DropDownList id="dwlDomain" runat="server" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    <hr />
      <br />
    </asp:Panel>

    <%--Griglia dei Context--%>
    <asp:GridView ID="gridContext" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server"
        >
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                       <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Description")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>


       <%--Pannello per l'edit di una relazione  --%>  
    <asp:Panel ID="pnlEdit" Width="100%" runat="server" Visible="false">
    <asp:Button id ="btn_SaveRelation" runat="server" Text="Save" OnClick="saveRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
        <div class="title" style="margin-bottom:20px">Relation between Context - Context </div>
        <br />
                <asp:TextBox ID="txtKeyRelation" runat="server" Visible="false"></asp:TextBox>
        <br />
        <table class="form" width="99%">
              <tr>
              <td>Context </td>
              <td><asp:Label runat = "server" ID="lblContextName"></asp:Label></td>
              </tr>
              <tr>
                <td><a href="#"  onclick="window.open('<%=Domain%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Relation Type")%></td>
                <td><asp:DropDownList id="dwlRelationType" runat="server"/></td>
             </tr>
             <tr>
                <td><a href="#"  onclick="window.open('<%=Domain%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpContextOrder&Language=<%=Language%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                <td><HP3:ctlText runat="server" ID="txtOrder" TypeControl="TextBox" style="width:300px" /></td>
             </tr>
        </table>
    </asp:Panel>

</asp:Panel>