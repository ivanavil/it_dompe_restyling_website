<%@ Control Language="VB" ClassName="extrF" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>

<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register TagPrefix="HP3" TagName="ExtraField"  Src="~/hp3Office/HP3Parts/ctlExtraField.ascx" %>

<script runat="server">
    'Private oLManager As New LanguageManager
    'Private oCManager As DictionaryManager
    'Private oSearcher As DictionarySearcher
    'Private oCollection As DictionaryCollection
    Private mPageManager As New MasterPageManager
    
    'Private dictionaryManager As New DictionaryManager
    'Private objCMSUtility As New GenericUtility
    'Private utility As New WebUtility
    
    'Private _sortType As DictionaryGenericComparer.SortType
    'Private _sortOrder As DictionaryGenericComparer.SortOrder
    
    Private strDomain As String
    
    'Private _typecontrol As ControlType = ControlType.View
    
    Private _strDomain As String
    Private _language As String
    
    'Public Enum ControlType
    '    Edit = 0
    '    Copy
    '    Selection
    '    View
    'End Enum
    
    'Public Property SelectedId() As Int32
    '    Get
    '        Return ViewState("selectedId")
    '    End Get
    '    Set(ByVal value As Int32)
    '        ViewState("selectedId") = value
    '    End Set
    'End Property
    
    'Public Property TypeControl() As ControlType
    '    Get
    '        Return _typecontrol
    '    End Get
        
    '    Set(ByVal value As ControlType)
    '        _typecontrol = value
    '    End Set
    'End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    'Sub ShowPanel(Optional ByVal pnlName As String = "")
    '    For Each pnl As Control In Me.Controls
    '        If TypeOf (pnl) Is Panel Then
    '            If pnl.ID = pnlName Then
    '                pnl.Visible = True
    '            Else
    '                pnl.Visible = False
    '            End If
    '        End If
    '    Next
    'End Sub

    'Sub BindFilerObject()
    '    'GetTypeBoxValue("Select BoxType", 1)
    'End Sub

    'Function LoadValue() As DictionaryValue
    '    If SelectedId <> 0 Then
    '        oCManager = New DictionaryManager
    '        oCManager.Cache = False
    '        Return oCManager.Read(SelectedId)
    '    End If
    '    Return Nothing
    'End Function
  
    '''' <summary>
    '''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    '''' </summary>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Function ItemRemove() As Boolean
    '    Dim ret As Boolean = False
    '    If SelectedId <> 0 Then
    '        oCManager = New DictionaryManager
    '        Dim key As New DictionaryIdentificator
    '        key.Id = SelectedId
    '        ret = oCManager.remove(key)
    '    End If
    '    Return ret
    'End Function
    
    'Sub LoadFormDett(ByVal oValue As DictionaryValue)
       
    '    'If TypeControl = ControlType.Copy Then SelectedId = 0
    '    'If Not oValue Is Nothing Then
    '    '    With oValue
    '    '        lblDescrizione.InnerText = " - " & .Label & " - " & .Description
    '    '        lblId.InnerText = "ID: " & .Key.Id
    '    '        'lSelector.DefaultValue = .KeyLanguage.Id
    '    '        lSelector.LoadLanguageValue(.KeyLanguage.Id)
    '    '        keyDomain.Text = .Key.Domain
    '    '        keyName.Text = .Key.Name
    '    '        Label.Text = .Label
    '    '        'Description.Value = .Description
    '    '        'Description.Content = .Description'telerik
    '    '        Description.Text = .Description
    '    '        Active.Checked = .Active
    '    '    End With
    '    'Else
    '    '    lblDescrizione.InnerText = "New Label in Dictionary"
    '    '    lblId.InnerText = ""
    '    '    'lSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
    '    '    keyDomain.Text = Nothing
    '    '    keyName.Text = Nothing
    '    '    Label.Text = Nothing
    '    '    'Description.Value = Nothing
    '    '    'Description.Content = Nothing' Telerik
    '    '    Description.Text = Nothing
    '    '    Active.Checked = True
    '    'End If
    'End Sub
    
    'Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
    '    'Dim oValue As New DictionaryValue
    '    'With (oValue)
    '    '    If SelectedId > 0 Then .Key.Id = SelectedId
    '    '    .KeyLanguage.Id = lSelector.Language
    '    '    .Key.Domain = keyDomain.Text
    '    '    .Key.Name = keyName.Text
    '    '    .Label = Label.Text
    '    '    '.Description = Description.Value
    '    '    '.Description = Description.Content'Telerik
    '    '    .Description = Description.Text
    '    '    .Active = Active.Checked
    '    'End With

    '    'oCManager = New DictionaryManager
    '    'If SelectedId <= 0 Then
    '    '    oValue = oCManager.Create(oValue)
    '    'Else
    '    '    oValue = oCManager.Update(oValue)
    '    'End If

    '    'BindWithSearch(gridList)
    'End Sub
    
    'Sub ShowRightPanel()
    '    Dim pnl As Panel = Nothing
    '    strDomain = WebUtility.MyPage(Me).DomainInfo
    '    Select Case TypeControl
    '        Case ControlType.Edit
    '            pnl = FindControl("pnlDett")
    '            LoadFormDett(LoadValue)
    '        Case ControlType.Copy
    '            pnl = FindControl("pnlDett")
    '            LoadFormDett(LoadValue)
    '        Case ControlType.Selection, ControlType.View
    '            pnl = FindControl("pnlGrid")
    '            If Not Page.IsPostBack Then
    '                'BindWithSearch(gridList)
    '                BindFilerObject()
    '            End If
    '    End Select
        
    '    If Not pnl Is Nothing Then
    '        ShowPanel(pnl.ID)
    '    End If
    'End Sub
    
    'Sub Page_Init()
    '    ShowPanel()
    'End Sub
    
    'Sub Page_load()
    '    Language() = mPageManager.GetLang.Id
    '    Domain() = WebUtility.MyPage(Me).DomainInfo
        
    '    If Not (Page.IsPostBack) Then
    '        ReadLanguage()
    '    End If
        
    '    ShowRightPanel()
    'End Sub
    
    'Sub BindLanguages()
    '    ReadLanguage()
    'End Sub
    
    ''popola la dwl delle lingue nel searcher
    'Sub ReadLanguage()
    '    Dim olangualeCollection As LanguageCollection
    '    Dim oLanguageSearcher As New LanguageSearcher
             
    '    olangualeCollection = Me.BusinessLanguageManager.Read(oLanguageSearcher)
        
    '    utility.LoadListControl(dwlLanguage, olangualeCollection, "Key.Code", "Key.Id")
    '    dwlLanguage.Items.Insert(0, New ListItem("--Select--", -1))
    'End Sub
    
    'Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As DictionarySearcher = Nothing)
    '    oCManager = New DictionaryManager
    '    oCollection = New DictionaryCollection
        
    '    If Searcher Is Nothing Then
    '        Searcher = New DictionarySearcher
    '    End If
    '    oCManager.Cache = False
    '    oCollection = oCManager.Read(Searcher)
    '    If Not oCollection Is Nothing Then
    '        oCollection.Sort(SortType, SortOrder)
    '    End If
    '    objGrid.DataSource = oCollection
    '    objGrid.DataBind()
    'End Sub
    
    'Sub BindWithSearch(ByVal objGrid As GridView)
    '    oSearcher = New DictionarySearcher
    '    If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
    '    If txtLabel.Text <> "" Then oSearcher.SearchString = txtLabel.Text
    '    If txtDescription.Text <> "" Then oSearcher.SearchString = txtDescription.Text
    '    If dwlLanguage.SelectedItem.Value <> -1 Then oSearcher.KeyLanguage.Id = dwlLanguage.SelectedItem.Value
        
    '    BindGrid(objGrid, oSearcher)
    'End Sub
    
    'Sub Search(ByVal s As Object, ByVal e As EventArgs)
    '    BindWithSearch(gridList)
    'End Sub
    
    'Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    sender.PageIndex = e.NewPageIndex
    '    BindWithSearch(sender)
    'End Sub
    
    'Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
    '    gridList.PageIndex = 0
        
    '    Select Case e.SortExpression
    '        Case "Id"
    '            If SortType <> DictionaryGenericComparer.SortType.ById Then
    '                SortType = DictionaryGenericComparer.SortType.ById
    '            Else
    '                SortOrder = SortOrder * -1
    '            End If
    '        Case "Label"
    '            If SortType <> DictionaryGenericComparer.SortType.ByLabel Then
    '                SortType = DictionaryGenericComparer.SortType.ByLabel
    '            Else
    '                SortOrder = SortOrder * -1
    '            End If
    '        Case "Key"
    '            If SortType <> DictionaryGenericComparer.SortType.ByKey Then
    '                SortType = DictionaryGenericComparer.SortType.ByKey
    '            Else
    '                SortOrder = SortOrder * -1
    '            End If
    '    End Select
    '    BindWithSearch(sender)
    'End Sub
    
    'Private Property SortType() As DictionaryGenericComparer.SortType
    '    Get
    '        If Not ViewState("SortType") Is Nothing Then
    '            _sortType = ViewState("SortType")
    '        Else
    '            _sortType = DictionaryGenericComparer.SortType.ById ' SortType di Default                
    '        End If
    '        Return _sortType
    '    End Get
    '    Set(ByVal value As DictionaryGenericComparer.SortType)
    '        _sortType = value
    '        ViewState("SortType") = _sortType
    '    End Set
    'End Property
    
    'Private Property SortOrder() As DictionaryGenericComparer.SortOrder
    '    Get
    '        If Not ViewState("SortOrder") Is Nothing Then
    '            _sortOrder = ViewState("SortOrder")
    '        Else
    '            _sortOrder = DictionaryGenericComparer.SortOrder.DESC ' SortOrder di Default                
    '        End If
    '        Return _sortOrder
    '    End Get
    '    Set(ByVal value As DictionaryGenericComparer.SortOrder)
    '        _sortOrder = value
    '        ViewState("SortOrder") = _sortOrder
    '    End Set
    'End Property
    
    'Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    '    Dim ctl As RadioButton
    '    Dim lnkSelect As ImageButton
        
    '    ctl = e.Row.FindControl("chkSel")
    '    lnkSelect = e.Row.FindControl("lnkSelect")
        
    '    If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
    '        Select Case TypeControl
    '            Case ControlType.Selection
    '                ctl.Visible = True
    '                ctl.Attributes.Add("onclick", "GestClick(this)")
    '                lnkSelect.Visible = False
    '            Case ControlType.View
    '                lnkSelect.Visible = True
    '                ctl.Visible = False
    '            Case ControlType.Edit, ControlType.View
    '                ctl.Visible = False
    '                lnkSelect.Visible = False
    '        End Select
    '    End If
    'End Sub
    
    'Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
    '    Dim id As Int32 = s.commandargument
    '    Select Case s.commandname
    '        Case "SelectItem"
    '            TypeControl = ControlType.Edit
                
    '            SelectedId = id
    '            ViewState("CheckedItem") = Nothing
    '            txtHidden.Text = ""
    '            ShowRightPanel()
    '        Case "CopyItem"
                
    '            TypeControl = ControlType.Copy
    '            SelectedId = id
    '            ViewState("CheckedItem") = Nothing
    '            txtHidden.Text = ""
    '            ShowRightPanel()
    '        Case "DeleteItem"
    '            SelectedId = id
    '            ItemRemove()
    '            BindWithSearch(gridList)
    '    End Select
    'End Sub
    
    'Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
    '    TypeControl = ControlType.Edit
    '    SelectedId = 0
    '    txtHidden.Text = ""
    '    ShowRightPanel()
    'End Sub
    
    'Sub GoList(ByVal s As Object, ByVal e As EventArgs)
    '    TypeControl = ControlType.View
    '    SelectedId = 0
    '    txtHidden.Text = ""
    '    ShowRightPanel()
    'End Sub
    
    'Function getLabel(ByVal labelName As String, ByVal description As String) As String
    '    Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    'End Function
    
    Sub Page_Load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        
        ReadExtraField()
    End Sub
    
    Sub ReadExtraField()
        Dim extrFManager As New ExtraFieldsManager
        Dim extrFSearcher As ExtraFieldsSearcher
        Dim extFColl As New ExtraFieldsCollection
        
        extrFSearcher = New ExtraFieldsSearcher
        extrFSearcher.KeyTheme.Id = 180
        extrFSearcher.KeyContentType.Id = Me.BusinessMasterPageManager.GetContenType.Id
        extrFSearcher.KeyContentTemplate.Id = 2
        
        extrFManager.Cache = False
        extFColl = extrFManager.Read(extrFSearcher)
        Response.Write("zero coll: " & extFColl.Count)
        If (Not extFColl Is Nothing) AndAlso (extFColl.Count > 0) Then
            Response.Write("zero")
            repExtraF.DataSource = extFColl
            repExtraF.DataBind()
            Exit Sub
        End If
        
        extrFSearcher = New ExtraFieldsSearcher
        extrFSearcher.KeyTheme.Id = 180
        extrFManager.Cache = False
        extFColl = extrFManager.Read(extrFSearcher)
        
        Response.Write("primo coll: " & extFColl.Count)
        If (Not extFColl Is Nothing) AndAlso (extFColl.Count > 0) Then
            Response.Write("primo")
            repExtraF.DataSource = extFColl
            repExtraF.DataBind()
            Exit Sub
        End If
        
        extrFSearcher = New ExtraFieldsSearcher
        extrFSearcher.KeyContentType.Id = Me.BusinessMasterPageManager.GetContenType.Id
        extrFManager.Cache = False
        extFColl = extrFManager.Read(extrFSearcher)
        
        Response.Write("secondo coll: " & extFColl.Count)
        If (Not extFColl Is Nothing) AndAlso (extFColl.Count > 0) Then
            Response.Write("secondo")
            repExtraF.DataSource = extFColl
            repExtraF.DataBind()
            Exit Sub
        End If
        
        extrFSearcher = New ExtraFieldsSearcher
        extrFSearcher.KeyContentTemplate.Id = 3
        extrFManager.Cache = False
        extFColl = extrFManager.Read(extrFSearcher)
        
        Response.Write("terzo coll: " & extFColl.Count)
        If (Not extFColl Is Nothing) AndAlso (extFColl.Count > 0) Then
            Response.Write("terzo")
            repExtraF.DataSource = extFColl
            repExtraF.DataBind()
            Exit Sub
        End If
    End Sub
     
    '
    Function IsOtherType(ByVal type As Integer) As Boolean
        Select Case type
            Case ExtraFieldsValue.ExtraFieldsTypeEnum.TypeString, ExtraFieldsValue.ExtraFieldsTypeEnum.TypeInteger, ExtraFieldsValue.ExtraFieldsTypeEnum.TypeDecimal, ExtraFieldsValue.ExtraFieldsTypeEnum.TypeMultiField
                Return True
        End Select
     
        Return False
    End Function
    
    Function IsBooleanType(ByVal type As Integer) As Boolean
        If (type = ExtraFieldsValue.ExtraFieldsTypeEnum.TypeBoolean) Then
            Return True
        End If
       
        Return False
    End Function
</script>



<%--<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

    <asp:Panel id="pnlGrid" runat="server">
        <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        </asp:Panel>
    </asp:Panel>

    <asp:Panel id="pnlDett" runat="server">
    </asp:Panel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                    AllowSorting="True" 
                                                    AutoGenerateColumns="False" 
                                                    DataKeyNames="Key.Id" 
                                                    DataSourceID="ObjectDataSource1" 
                                                    CssClass="gridview" 
                                                    AlternatingRowStyle-CssClass="even" 
                                                    GridLines="None">
            
                  <Columns>
                    <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="Language" >
                            <ItemTemplate><img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.Id"))%>_small.gif" /></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="9%" HeaderText="Code" SortExpression="Code">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Code")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="10%" DataField="Culture" HeaderText="Culture" />
                    <asp:BoundField HeaderStyle-Width="50%" DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderStyle-Width="10%" DataField="Active" HeaderText="Active" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                    </asp:TemplateField>
                  </Columns>
              </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>  


    <asp:ObjectDataSource ID="ObjectDataSource1" 
                            runat="server"
                            SelectMethod="Read"
                            TypeName="Localization.DictionaryManager"
                            DataObjectTypeName="Localization.ObjectValues.DictionaryValue">
     </asp:ObjectDataSource>--%>

<asp:Repeater ID="repExtraF" runat="server">
    <HeaderTemplate><table class="form" style="width:100%"></HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraField&Help=cms_HelpExtraField&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%#Container.DataItem.Name%></td>
                <td><HP3:ExtraField id="myControl" runat="server"  TypeControl="StringType"  Visible='<%#IsOtherType(Container.DataItem.Type)%>'/> <HP3:ExtraField id="ExtraField1" runat="server" TypeControl="BooleanType" Visible='<%#IsBooleanType(Container.DataItem.Type)%>'/></td>
            </tr>
        </ItemTemplate>
    <FooterTemplate></table></FooterTemplate>
</asp:Repeater>
