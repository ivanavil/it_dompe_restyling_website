<%@ Control Language="VB" ClassName="ctlCultureManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private oCManager As New CultureManager
    Private oSearcher As New CultureSearcher
    Private oCollection As New CultureCollection
    Private mPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    
    Private _sortType As CultureGenericComparer.SortType
    Private _sortOrder As CultureGenericComparer.SortOrder
    'Private strDomain As String
    
    Private _typecontrol As ControlType = ControlType.View
    
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As CultureValue
        If SelectedId <> 0 Then
            oCManager = New CultureManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
      
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        'Dim ret As Boolean = False
        'If SelectedId <> 0 Then
        '    oCManager = New SiteAreaManager
        '    Dim key As New SiteAreaIdentificator
        '    key.Id = SelectedId
        '    ret = oCManager.remove(key)
        '    If ret Then
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Site)
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Area)
        '        oAdvManager.RemoveAllBannerContentType(keyBanner)
        '        oAdvManager.RemoveAllBannerBox(keyBanner)
        '    End If
        'End If
        'Return ret
    End Function
    
   
    Sub LoadFormDett(ByVal oValue As CultureValue)
        'GetTypeBoxValue("", 2)
        
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Code
                lblId.InnerText = "ID: " & .Key.Id
                Code.Text = .Code
                Identifier.Text = .Identifier
                Country.Text = .Country
            End With
        Else
            lblDescrizione.InnerText = "New Culture"
            lblId.InnerText = ""
            Code.Text = Nothing
            Identifier.Text = Nothing
            Country.Text = Nothing
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New CultureValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Code = Code.Text
            .Identifier = Identifier.Text
            .Country = Country.Text
        End With

        'oCManager = New BoxManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        'strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As CultureSearcher = Nothing)
        'oCManager = New BoxManager
        'oCollection = New BoxCollection
        
        'If Searcher Is Nothing Then
        'Searcher = New BoxSearcher
        'End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New CultureSearcher
        If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
        If txtCode.Text <> "" Then oSearcher.Code = txtCode.Text
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        Select Case e.SortExpression
            Case "Id"
                SortType = CultureGenericComparer.SortType.ById
                SortOrder = SortOrder * -1
            Case "Code"
                SortType = CultureGenericComparer.SortType.ByCode
                SortOrder = SortOrder * -1
            Case "Country"
                SortType = CultureGenericComparer.SortType.ByCountry
                SortOrder = SortOrder * -1
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As CultureGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = CultureGenericComparer.SortType.ByCode ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As CultureGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As CultureGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = CultureGenericComparer.SortOrder.ASC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As CultureGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'Dim ctl As RadioButton
        'Dim lnkSelect As ImageButton
        
        'ctl = e.Row.FindControl("chkSel")
        'lnkSelect = e.Row.FindControl("lnkSelect")
        
        'If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
        '    Select Case TypeControl
        '        Case ControlType.Selection
        '            ctl.Visible = True
        '            ctl.Attributes.Add("onclick", "GestClick(this)")
        '            lnkSelect.Visible = False
        '        Case ControlType.View
        '            lnkSelect.Visible = True
        '            ctl.Visible = False
        '        Case ControlType.Edit, ControlType.View
        '            ctl.Visible = False
        '            lnkSelect.Visible = False
        '    End Select
        'End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function

</script>

<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
    
<asp:Panel id="pnlGrid" runat="server">
    <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Culture Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Code</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl="NumericText" style="width:50px"/></td>
                    <td><HP3:Text runat="server" ID="txtCode" TypeControl="TextBox" style="width:100px"/></td>  
                    <td></td>  
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"             
        AllowSorting="true"
        OnPageIndexChanging="gridList_PageIndexChanging"
        PageSize ="20"
        OnSorting="gridList_Sorting"
        emptydatatext="No item available"                
        OnRowDataBound="gridList_RowDataBound">
            <Columns >
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id" SortExpression="Id">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="20%" DataField="Code" HeaderText ="Code" SortExpression="Code" />
                <asp:BoundField HeaderStyle-Width="10%" DataField="Identifier" HeaderText="Identifier" />
                <asp:BoundField HeaderStyle-Width="40%" DataField="Country" HeaderText ="Country" SortExpression="Country" />
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive"  onclick="GoList" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/><br /><br />
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCode&Help=cms_HelpCultureCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCode", "Code")%></td>
            <td><HP3:Text runat ="server" ID="Code" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCultureIdentifier&Help=cms_HelpCultureIdentifier&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCultureIdentifier", "Identifier")%></td>
            <td><HP3:Text runat ="server" ID="Identifier" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCountry&Help=cms_HelpCountry&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCountry", "Country")%></td>
            <td><HP3:Text runat ="server" ID="Country" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
    </table>
</asp:Panel>