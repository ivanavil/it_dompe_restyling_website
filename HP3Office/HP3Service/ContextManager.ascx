<%@ Control Language="VB" ClassName="ctlContextManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagPrefix="HP3" TagName="ctlLanguage" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private _oContextManager As ContextManager
    Private _oContextSearcher As ContextSearcher
    Private _oContextCollection As ContextCollection
    Private _oGenericUtility As New GenericUtility
    Private oLManager As New LanguageManager()
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
   
    Private Const PageSize As Integer = 20
    Private Const NPageVis As Integer = 6
    
    
    Private strDomain As String
    Private strJs As String
    'Private nodes As Dictionary(Of String, ContextNode) = Nothing
    Private comboContextGroupBinded As Boolean = False
    'Private gridContextBinded As Boolean = False
    Private utility As New WebUtility
    Private _strDomain As String
    Private _language As String
    
    Private _sortType As ContextGenericComparer.SortType = ContextGenericComparer.SortType.ById
    Private _sortOrder As ContextGenericComparer.SortOrder = ContextGenericComparer.SortOrder.DESC
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdContext() As Int32
        Get
            Return ViewState("SelectedIdContext")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdContext") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il ContextValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadContextValue() As ContextValue
        If SelectedIdContext <> 0 Then
            Return _oGenericUtility.GetContext(SelectedIdContext)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oContextValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oContextValue As ContextValue)
        If Not oContextValue Is Nothing Then
            With oContextValue
                txtKeyName.Text = .Key.KeyName
                txtDomain.Text = .Key.Domain
                txtName.Text = .Key.Name
                'languageSelector.DefaultValue = .Key.Language.Id
                'txtLanguage.Text = .Key.Language.Id
                languageSelector.LoadLanguageValue(.Key.Language.Id)
                txtDescription.Text = .Description
                txtText.Text = .Text
                txtHiddenFather.Text = .KeyFather.Id
                txtFather.Text = ReadContextValue(.KeyFather.Id)
                txtOrder.Text = .Order
                relContentType.SetSelectedValue(.KeyContentType.Id)
                ctlSiteArea.SetSelectedValue(.KeySiteArea.Id)
                ContextDescrizione.InnerText = " - " & .Description
                ContextId.InnerText = "Context ID: " & .Key.Id
            End With
        End If
    End Sub
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                If SelectedIdContext > 0 Then
                    languageSelector.Enabled = False
                Else
                    languageSelector.Enabled = True
                End If
                LoadFormDett(LoadContextValue)
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindComboGroup()
                    BindWithSearch(gridListContext)
                End If
            Case ControlType.Selection
                    rowToolbar.Visible = False
                    pnl = FindControl("pnlGrid")
                    If Not Page.IsPostBack Then
                        BindComboGroup()
                        BindWithSearch(gridListContext)
                    End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        BindFatherContext()
        
        If Not Page.IsPostBack Then '
            ctlSiteArea.LoadControl()
            relContentType.LoadControl()
        End If

        ShowRightPanel()
        
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContextSearcher = Nothing, Optional ByVal page As Integer = 1)
        _oContextManager = New ContextManager
        

        If Not (Searcher) Is Nothing Then
            'Response.Write("so " & Searcher.Key.Id)
            Searcher.SortType = SortType
            Searcher.SortOrder = SortOrder
            Searcher.PageSize = PageSize
            Searcher.PageNumber = page
            
            sectionTit.Visible = True
            _oContextManager.Cache = False
            _oContextCollection = _oContextManager.Read(Searcher)
        
            LoadPager(_oContextCollection, page)
            
            objGrid.DataSource = _oContextCollection
            objGrid.DataBind()
      
        
        End If
        
        
       
        
       
      
       
    End Sub
    
    ''' <summary>
    ''' Lettura delle descrizioni dei content type dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContextValue(ByVal id As Int32) As String
        Dim ov As Object = _oGenericUtility.GetContext(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function

    Sub BindFatherContext()
        Dim _oContextSearcher As New ContextSearcher
        BusinessContextManager.Cache = False 'M1201
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(_oContextSearcher)
        
        utility.LoadListControl(dwlFatherContext, contextColl, "Key.Name", "Key.Id")
        dwlFatherContext.Items.Insert(0, New ListItem("--Select Father Context--", -1))
    End Sub
              
    ''' <summary>
    ''' Caricamento della griglia dei Contexts
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView, Optional ByVal page As Integer = 1)
        
        '_oContextManager = New ContextManager
        _oContextSearcher = New ContextSearcher()
        If txtFilterId.Text <> "" Then _oContextSearcher.Key.Id = CInt(txtFilterId.Text)
        ' Response.Write(txtFilterId.Text)
        If txtFilterDescription.Text <> "" Then _oContextSearcher.Description = "%" & txtFilterDescription.Text & "%"
        If (textDomain.Text <> "") Then _oContextSearcher.Key.Domain = textDomain.Text
        If (dwlFatherContext.SelectedItem.Value <> -1) Then _oContextSearcher.KeyFather.Id = dwlFatherContext.SelectedItem.Value
        _oContextSearcher.KeyContextGroup.Id = ddContextGroup.SelectedValue
        
        ReadSelectedItems()
        BindGrid(objGrid, _oContextSearcher, page)
    End Sub
    
    Sub SearchContexts(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListContext, CInt(Actualpage.Value))
    End Sub
    
    Private Property SortType() As ContextGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContextGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContextGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ContextGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ContextGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ContextGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListContext.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litContextId")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ContextCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curContextValue As ContextValue
                    Dim outContextCollection As New ContextCollection
            
                    For Each str As String In CheckedItems
                        curContextValue = _oGenericUtility.GetContext(Int32.Parse(str))
                        If Not curContextValue Is Nothing Then
                            outContextCollection.Add(curContextValue)
                        End If
                    Next
            
                    Return outContextCollection
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedIdContext = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindComboGroup()
        BindWithSearch(gridListContext, CInt(Actualpage.Value))
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ClearForm()
        Dim oContextId As New ContextIdentificator()
        oContextId.Id = s.commandArgument.ToString.Split("_")(0)
        oContextId.Language = New LanguageIdentificator(s.commandArgument.ToString.Split("_")(1))

        SelectedIdContext = oContextId.Id
        
        Select Case s.commandname
            Case "SelectItem"
                btnSave.Text = "Update"
                
                TypeControl = ControlType.Edit
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                
                NewItem = False
                
                ShowRightPanel()
                
            Case "ContextContextGroup"
                Dim wr As New WebRequestValue
                Dim ctm As New ContentTypeManager()
                Dim mPage As New MasterPageManager()
                Dim objQs As New ObjQueryString
                wr.KeycontentType = New ContentTypeIdentificator("CMS", "CtxCtxGrp")
                wr.customParam.append("KeyContext", oContextId.Id)
                wr.customParam.append("ContextLanguage", oContextId.Language.Id)
                wr.customParam.append(objQs)
                Response.Redirect(mPage.FormatRequest(wr))
            
            Case "ContextContext"
                Dim wr As New WebRequestValue
                Dim ctm As New ContentTypeManager()
                Dim mPage As New MasterPageManager()
                Dim objQs As New ObjQueryString
                wr.KeycontentType = New ContentTypeIdentificator("CMS", "CtxCtx")
                wr.customParam.append("KeyContext", oContextId.Id)
                wr.customParam.append("ContextLanguage", oContextId.Language.Id)
                wr.customParam.append(objQs)
                Response.Redirect(mPage.FormatRequest(wr))
        End Select
        
    End Sub

    ''' <summary>
    ''' Gestione del click sul bottone Delete nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnDeleteItem(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim oContextId As New ContextIdentificator()
        oContextId.Id = s.commandArgument.ToString.Split("_")(0)
        oContextId.Language = New LanguageIdentificator(s.commandArgument.ToString.Split("_")(1))
        Dim oContextManager As New ContextManager
        oContextManager.Delete(oContextId)
        goBack(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Save/Update del Context
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oContextManager As New ContextManager
        Dim oContextValue As ContextValue = ReadFormValues()
        
        If NewItem Then
            oContextValue = oContextManager.Create(oContextValue)
        Else
            oContextValue = oContextManager.Update(oContextValue)
        End If
        If oContextValue Is Nothing Then
            strJs = "alert('Error during saving')"
        Else
            strJs = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As ContextValue
        Dim oContextValue As New ContextValue
        
        With oContextValue
            .Key.Domain = txtDomain.Text
            .Key.Name = txtName.Text
            .Key.KeyName = txtKeyName.Text
            .Description = txtDescription.Text
            .Text = txtText.Text
            If txtHiddenFather.Text <> "" Then .KeyFather.Id = Int32.Parse(txtHiddenFather.Text)
            If txtOrder.Text <> "" Then .Order = Int32.Parse(txtOrder.Text)

            Dim ctc As ContentTypeCollection = relContentType.GetSelection()
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                .KeyContentType.Id = ctc.Item(0).Key.Id
            End If

            Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(ctlSiteArea)
            If Not oSiteAreaValue Is Nothing Then
                .KeySiteArea = oSiteAreaValue.Key
            End If

            .Key.Id = SelectedIdContext
            .Key.Language.Id = languageSelector.Language
        End With
        
        Return oContextValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo Context
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewContext(ByVal s As Object, ByVal e As EventArgs)
        Dim objCMSUtility As New GenericUtility

        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedIdContext = 0
        'languageSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is CheckBox Then
                CType(ctl, CheckBox).Checked = False
            End If
        Next
        
        relContentType.SetSelectedIndex(0)
        ctlSiteArea.SetSelectedIndex(0)

        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momento ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        ContextId.InnerText = ""
        ContextDescrizione.InnerText = "New Context"
    End Sub

    Sub BindComboGroup()
        If comboContextGroupBinded Then Exit Sub
        
        Dim _oContextManager As New ContextManager()
        Dim _oContextGroupCollection As ContextGroupCollection
        Dim _oContextGroupSearcher As New ContextGroupSearcher
        _oContextGroupSearcher.Key.Language = New LanguageIdentificator()
        _oContextGroupCollection = _oContextManager.ReadGroup(_oContextGroupSearcher)
        
        _oContextGroupCollection = _oContextManager.ReadGroup(_oContextGroupSearcher)

        ddContextGroup.Items.Clear()
        ddContextGroup.Items.Add(New ListItem("--Select Context--", 0))
        ddContextGroup.Items.Add(New ListItem("Context without group", -1))
        ddContextGroup.Items.Add(New ListItem("All contexts", 0))
        If Not _oContextGroupCollection Is Nothing Then
            Dim _oContextGroup As ContextGroupValue
            ddContextGroup.Items.Add(New ListItem("---------------------------------------------------------------", -2))
            
            For Each _oContextGroup In _oContextGroupCollection
                ddContextGroup.Items.Add(New ListItem(_oContextGroup.Description, _oContextGroup.Key.Id))
            Next
            
            ddContextGroup.SelectedIndex = 0
        End If
        
        comboContextGroupBinded = True
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litContextId")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        lnkCopy = e.Row.FindControl("lnkCopy")

        If Not lnkDelete Is Nothing Then
            lnkDelete.Attributes.Add("onClick", "if (!ConfirmDelete()) return false;")
        End If
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    sender.PageIndex = e.NewPageIndex
    '    BindWithSearch(sender)
    'End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'gridListContext.PageIndex = 0
        Select Case e.SortExpression
            Case "Id"
                If SortType <> ContextGenericComparer.SortType.ById Then
                    SortType = ContextGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> ContextGenericComparer.SortType.ByDescription Then
                    SortType = ContextGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> ContextGenericComparer.SortType.ByKey Then
                    SortType = ContextGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "KeyName"
                If SortType <> ContextGenericComparer.SortType.ByKeyName Then
                    SortType = ContextGenericComparer.SortType.ByKeyName
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender, CInt(Actualpage.Value))
    End Sub
    
    Sub GoContextGroup(ByVal s As Object, ByVal e As EventArgs)
                
        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
        wr.KeycontentType = New ContentTypeIdentificator("CMS", "CtxGroup")
        wr.customParam.append(objQs)
        Response.Redirect(mPage.FormatRequest(wr))

    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
    
    Sub linkPager_PageIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        'evento scatenato dal paginatore degli utenti filtrati
        BindWithSearch(gridListContext, sender.CommandArgument)
        ' LoadUser(CInt(sender.CommandArgument), ctxFilter.Value, txtSortOrder.Value, txtOrderType.Value)
    End Sub
    
    Sub LoadPager(ByRef contextColl As ContextCollection, Optional ByVal Page As Integer = 1)
       
        If Not contextColl Is Nothing AndAlso contextColl.Count > 0 Then
            Dim pageList As New ArrayList
            Dim i As Integer = 0
            
            Dim ite As New ListItem
            Actualpage.Value = Page
            
            ite.Text = "&laquo;"
            ite.Value = "1"
            pageList.Add(ite)
            
            For i = 0 To contextColl.PagerTotalNumber - 1
                If (i + 1 > (Page - NPageVis)) AndAlso (i + 1 < (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = i + 1
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("add" & ite.Value & "i: " & i)
                ElseIf (i + 1 = (Page - NPageVis)) Or (i + 1 = (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = "..."
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("addelse" & ite.Value & "i: " & i)
                End If
            Next
            
            ite = New ListItem
            ite.Text = "&raquo;"
            ite.Value = contextColl.PagerTotalNumber
            pageList.Add(ite)
            
            'Response.Write("tot" & pageList.Count & "--" & Page)
            If i > 1 Then
                rpPager.DataSource = pageList
                rpPager.DataBind()
                'Response.Write("popolo" & pageList.Count)
                'rptContactPagerBottom.DataSource = pageList
            Else
                rpPager.DataSource = Nothing
                rpPager.DataBind()
                'Response.Write("popolo Nothing")
                'rptContactPagerBottom.DataSource = Nothing
            End If
        
            'Dim nDa As Integer = ((userColl.PagerTotalNumber - 1) * userColl.PagerTotalCount) + 1
            'Dim nA As Integer = (nDa + userColl.Count) - 1
            'Dim nTot As Integer = userColl.PagerTotalCount
            'lblContactpager.Text = nDa.ToString & " - " & _
            '(nA).ToString & " on " & _
            '(nTot).ToString
        Else
            ' Response.Write("else" )
            rpPager.DataSource = Nothing
            rpPager.DataBind()
        End If
             
    End Sub
    
    
    
</script>
<script type="text/javascript" >  
    function GestClick(obj) {         
        var selRadio
        txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
        if (txtVal.value != '') {
            selRadio=document.getElementById (txtVal.value);
            if (selRadio!= null) selRadio.checked = false;
        }
                
        txtVal.value= obj.id                
    }
    
    function getTextVal(strText) {            
        var oTxt = document.getElementById(strText);
        return oTxt.value;
    }    

    function ConfirmDelete() {
        return confirm('Are you sure to delete this item?');
    }

//    function hideShowPanel(img, panelId) {
//        
//        var panel = document.getElementById(panelId);
//        if (panel) {
//            if (panel.style.display == "none") {
//                img.src = "/HP3Office/HP3Image/Tree/minus.gif";
//                panel.style.display = "block";
//            } else {
//                img.src = "/HP3Office/HP3Image/Tree/plus.gif";
//                panel.style.display = "none";
//            }
//        }
//    }
        
        <%=strJs%>
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr /> 
<%--FILTRI SUI Context--%>
<asp:Panel id="pnlGrid" runat="server">
    <table class="form">
        <tr id="rowToolbar" runat="server">
                <td  colspan="4">
                    <asp:button ID="btnNew" runat="server" CssClass="button" Text="New" onclick="NewContext" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
                    <asp:button ID="btnContextGroup" runat="server" CssClass="button" Text="Go Context Group" Style ="width:150px" onclick="GoContextGroup" /><br /><br />
               </td>
        </tr>
    </table>  
          
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Context Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
        <table class="form">
            <tr>
                <td><strong>ID</strong></td>    
                <td ><strong>Description</strong></td>
                <td><strong>Context Group</strong></td>
            </tr>
            <tr>
                <td><HP3:Text runat="server" ID="txtFilterId" TypeControl="NumericText" style="width:50px"/></td>    
                <td><HP3:Text runat="server" ID="txtFilterDescription" TypeControl="TextBox" style="width:300px"/></td>  
                <td><asp:DropDownList runat="server" ID="ddContextGroup" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                 <td><strong>Domain</strong></td>
                 <td><strong>Father Context</strong></td>
            </tr>
            <tr>
                <td><HP3:Text runat="server" ID="textDomain" TypeControl="TextBox" style="width:150px"/></td>    
                <td><asp:DropDownList id="dwlFatherContext"  runat="server"/></td>
            </tr>
            <tr>
                <td><asp:button runat="server" ID="btnSearch" CssClass="button" onclick="SearchContexts" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
            </tr>
            
        </table>
    </fieldset>
    <hr />
 </asp:Panel>
<%--FINE FILTRI SUI Context--%>

<%--LISTA Context--%>
<%--<asp:Table ID="gridListContext" Width="100%" GridLines="Both" runat="server">
</asp:Table>
--%>

<asp:HiddenField  ID="Actualpage"  runat="server" Value="1"/>
<asp:Label CssClass="title" runat="server"  visible="false" id="sectionTit">Context List</asp:Label>
<div style ="float:right; text-align:right;">
     <asp:Repeater ID="rpPager" runat="server" Visible="true" >
        <ItemTemplate>
          
        <%#IIf(Actualpage.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>
                        <asp:LinkButton ID="linkPager" runat="server" OnClick="linkPager_PageIndex" 
                        Visible='<%#iif (Actualpage.value=DataBinder.Eval(Container.DataItem, "value"),"false","true") %>' 
                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "value")%>' ><%#DataBinder.Eval(Container.DataItem, "text")%></asp:LinkButton>
        </ItemTemplate>
     </asp:Repeater>
</div> 
    <asp:gridview ID="gridListContext" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
      
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"
        AllowSorting="true"
          
        emptydatatext="No contexts available"                                                          
        OnRowDataBound="gridRowDataBound" 
       
        OnSorting="gridSorting">
        
        <Columns >
            <asp:TemplateField HeaderStyle-Width="2%" ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:radiobutton ID="chkSel" runat="server" />
                    <asp:literal ID="litContextId" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%" ItemStyle-Width="5%" HeaderText="Id" SortExpression="Id">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.Key.Language.Id)%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>         
            <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-Width="13%" HeaderText="Domain" SortExpression="Key">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="15%" HeaderText="Key Name" SortExpression="KeyName">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.KeyName")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:BoundField HeaderStyle-Width="40%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="7%">
                <ItemTemplate>
                    <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>'/>
                    <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attenzione! Sicuro di voler cancellare?')" OnClick="OnDeleteItem" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>'/>
                    <asp:ImageButton ID="lnkContextContextGroup" runat="server" ToolTip ="Context / Context Group Relation" ImageUrl="~/HP3Office/HP3Image/ico/content_content.gif" OnClick="OnItemSelected" CommandName="ContextContextGroup" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>'/>
                   <asp:ImageButton ID="btn_context" ToolTip ="Context Relation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_context.gif" onClick="OnItemSelected"  CommandName="ContextContext" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>' />
             
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:gridview>
</asp:Panel> 
<%--FINE LISTA Context--%>


<%--EDIT Context--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnBack" runat="server" CssClass="button" Text="Archive" onclick="goBack" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" runat="server" CssClass="button" Text="Save" onclick="Update" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    </div>
    <span id="ContextId" class="title" runat ="server"/>
    <span id="ContextDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
         <tr>
            <td width ="20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextKeyName&Help=cms_HelpContextKeyName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextKeyName", "Key Name")%></td>
            <td><HP3:Text runat="server" ID="txtKeyName" TypeControl="TextBox" MaxLength="30" style="width:300px" /></td>
        </tr>
        <%--<tr>--%>
        <tr>
            <td width ="20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat="server" ID="txtDomain" TypeControl="TextBox" MaxLength="5" style="width:50px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat="server" ID="txtName" TypeControl="TextBox" MaxLength="10" style="width:100px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextLanguage&Help=cms_HelpContextLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextLanguage", "Context Language")%></td>
            <td><HP3:ctlLanguage ID="languageSelector" TypeControl="AlternativeControl" runat="server" />
                <%--<HP3:Text runat="server" ID="txtLanguage" TypeControl="TextBox" style="width:300px" />--%>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContextDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormText&Help=cms_HelpText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormText", "Text")%></td>
            <td><HP3:Text runat="server" ID="txtText" TypeControl="TextArea" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextFather&Help=cms_HelpContextFather&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextFather", "Father")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td width="50px"><asp:TextBox ID="txtHiddenFather" style="display:none" runat="server" /><HP3:Text runat="server" ID="txtFather" TypeControl="TextBox" style="width:300px" isReadOnly="true" /></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popContentContext.aspx?sites=' + document.getElementById('<%=txtHiddenFather.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenFather.clientId%>&ListId=<%=txtFather.clientId%>','','width=600,height=500,scrollbars=yes')" /></td> 
                        
                        <%--<td><input type="button" value="..." class="button" onclick="window.open('<%=strDomain%>/Popups/popContentContext.aspx?SelItem=' + document.getElementById('<%=txtHiddenFather.clientId%>').value + '&ctlHidden=<%=txtHiddenFather.clientId%>&ctlVisible=<%=txtFather.clientId%>','','width=780,height=480,scrollbars=yes')" /></td>        --%>
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpContextOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
            <td><HP3:Text runat="server" ID="txtOrder" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td><HP3:ctlContentType ID="relContentType" TypeControl="combo" ItemZeroMessage="Select ContentType" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td><HP3:ctlSite runat="server" ID="ctlSiteArea" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></td>
        </tr>
    </table>
</asp:Panel>
<%--FINE EDIT Context--%>
 