<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
        Search.SearchAreaUser = True
        Search.SearchAreaContent = True
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim crmManager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()
        Dim webRequest As New WebRequestValue()
        
        If Not Page.IsPostBack Then
            webRequest.customParam.LoadQueryString()
        End If

        ' verifica il data range selezionato
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate.AddDays(1)
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        Else
            If webRequest.customParam.item("DateStart") <> String.Empty And webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            ElseIf webRequest.customParam.item("DateStart") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("Date")).AddDays(1)
            ElseIf webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1)
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            End If
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.IdSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                trackSearcher.IdSite = Integer.Parse(webRequest.customParam.item("IdSite"))
            End If
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.IdArea = Search.DdlArea.Value
        Else
            If webRequest.customParam.item("IdArea") <> String.Empty Then
                trackSearcher.IdArea = Integer.Parse(webRequest.customParam.item("IdArea"))
            End If
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.IdFF = Search.DdlFF.Value
        Else
            If webRequest.customParam.item("IdFF") <> String.Empty Then
                trackSearcher.IdFF = Integer.Parse(webRequest.customParam.item("IdFF"))
            End If
        End If
       
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.IdUT = Search.DdlUserType.Value
        Else
            If webRequest.customParam.item("IdUT") <> String.Empty Then
                trackSearcher.IdUT = Integer.Parse(webRequest.customParam.item("IdUT"))
            End If
        End If
        
        If (Not Search.DdlEvent Is Nothing) Then
            trackSearcher.IdEvent = Search.DdlEvent.Value
        Else
            If webRequest.customParam.item("IdEvent") <> String.Empty Then
                trackSearcher.IdEvent = Integer.Parse(webRequest.customParam.item("IdEvent"))
            End If
        End If
    
        If (Not Search.IdUser Is Nothing AndAlso Search.IdUser <> String.Empty) Then
            trackSearcher.IdUser = Integer.Parse(Search.IdUser)
        Else
            If webRequest.customParam.item("IdUser") <> String.Empty Then
                trackSearcher.IdUser = Integer.Parse(webRequest.customParam.item("IdUser"))
            End If
        End If

        If (Not Search.IdContent Is Nothing AndAlso Search.IdContent <> String.Empty) Then
            trackSearcher.IdContent = Integer.Parse(Search.IdContent)
        Else
            If webRequest.customParam.item("IdContent") <> String.Empty Then
                trackSearcher.IdContent = Integer.Parse(webRequest.customParam.item("IdContent"))
            End If
        End If

        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        trackSearcher.TypeQuery = "ReadTrackDetail"
        trackSearcher.MaxRows = 200

        Dim webTrackCollection As New WebTrackCollection
        
        webTrackCollection = crmManager.ReadTrack(trackSearcher)

        GWEvent.DataSource = webTrackCollection
        GWEvent.DataBind()

        msg.Text = String.Empty
        If Not webTrackCollection Is Nothing AndAlso webTrackCollection.Count > 0 Then
            msg.Text = "Results (Total <strong>" & webTrackCollection.Count & "</strong>)"
            If webTrackCollection.Count = trackSearcher.MaxRows Then msg.Text += " research has produced more than 500 results"
        End If
        
        If (Not Search.DdlEvent Is Nothing AndAlso Search.DdlEvent.Value > 0) Then
            Dim oCollection As New WebTrackCollection
            Dim crmMan As New WebTrackManager()
            Dim str As String = ","
            lblTotali.Text = ""
            For Each elem As WebTrackValue In webTrackCollection
                If InStr(str, "," & elem.KeyUser.Id & ",") = 0 Then
                    str = str & elem.KeyUser.Id & ","
                    trackSearcher.IdUser = elem.KeyUser.Id
                    oCollection = crmMan.ReadTrack(trackSearcher)
                    If Not oCollection Is Nothing AndAlso oCollection.Count > 0 Then
                        lblTotali.Text = lblTotali.Text & "<tr><td>" & elem.KeyUser.Id & "</td><td>" & oCollection.Count & "</td></tr>"
                    End If
                End If
            Next
        End If
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("hh:mm:ss")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function
    
    Protected Sub GridEvent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWEvent.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
    
    Function GetUserDetail(ByVal value As Integer) As String
        If value = 0 Then Return ""
        Return "<img  src=""/HP3Office/HP3Image/Ico/user_detail_inspector.gif"" title=""Detail""/>"
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
    <asp:GridView ID="GWEvent" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridEvent_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#GetDateFormat(DataBinder.Eval(Container.DataItem, "DateInsert"))%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Time" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#GetTimeFormat(DataBinder.Eval(Container.DataItem, "DateInsert"))%></div></ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Lang" HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><img alt="Language: <%#Me.BusinessLanguageManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "KeyLanguage.Id"))%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#Me.BusinessLanguageManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "KeyLanguage.Id"))%>_small.gif" /></div>
                </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Id User" HeaderStyle-Width="5%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "KeyUser.Id")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Site" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "SiteDescription")%></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Area" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "SiteAreaDescription")%></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Event" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "EventDescription")%></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Service" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ContentTypeDescription")%></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %>,'','width=800,height=600,scrollbars=yes')"><%#GetUserDetail(Container.DataItem.KeyUser.Id) %></a></div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    
    <br />
    <br />
    
    <%If (Not Search.DdlEvent Is Nothing AndAlso Search.DdlEvent.Value > 0) Then %>
    <table class="tbl1">
    <tr>
        <td>User</td><td>Download</td>
    </tr>
    
     <asp:Label ID="lblTotali" runat="server" Text="no content"></asp:Label>
    
    </table>
    
    <%End If%>
    
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>