<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>


<script language="VB" runat="server" >
    
    Dim TYPE_CONTENTS_INSP_AREA_DETAIL As String = "contentsinspector_area"
    Dim TYPE_CONTENTS_INSP_EVENT_DETAIL As String = "contentsinspector_event"
    Dim TYPE_VISITS As String = "visits"
    Dim TYPE_PAGES As String = "pages"
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim TYPE_EVENT_BY_AREA As String = "eventsbysitearea"

    Dim TXT_TOT_VIEW As String = "TotView"

    Dim s_ColumnsInfo As New ArrayList
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = False
        Search.SearchAreaUT = False
    End Sub

    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = False
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        Dim dt As DataTable
        
        ' CALL AREAS
        If trackSearcher.WhereSiteCondition <> "" Then
            trackSearcher.StoredName = "SP_HP3_CRM_MDX_Contents"
            trackSearcher.TypeQuery = TYPE_CONTENTS_INSP_AREA_DETAIL
            trackSearcher.WhereAreaCondition = ""
            'trackSearcher.WhereData = True
                
            cls = crmmanager.ReadCellSet(trackSearcher)
            ' call display cellset 
            dt = getDataTable(cls, TYPE_CONTENTS_INSP_AREA_DETAIL, trackSearcher.UseRange)
        
            GWAreas.DataSource = dt
            GWAreas.DataBind()
            msg.Visible = False
        Else
            msg.Text = "Select Site, please"
        End If
        
        ' CALL EVENTS
        If Search.objAll.Area <> "" Then
            trackSearcher.StoredName = "SP_HP3_CRM_MDX_Contents"
            trackSearcher.TypeQuery = TYPE_CONTENTS_INSP_EVENT_DETAIL
            trackSearcher.WhereAreaCondition = Search.objAll.Area
            'trackSearcher.WhereData = True
        
            cls = crmmanager.ReadCellSet(trackSearcher)
            ' call display cellset 
            dt = getDataTable(cls, TYPE_CONTENTS_INSP_EVENT_DETAIL, trackSearcher.UseRange)
        
            GWEvents.DataSource = dt
            GWEvents.DataBind()
            GWEvents.Visible = True
        Else
            GWEvents.Visible = False
        End If
    End Sub
    

    ''' <summary>
    ''' COstruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        s_ColumnsInfo.Clear()
        
        Dim hRes As Integer
        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member

        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If

        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                'name = GetRowHeaderLink(y, gridType)
                If (name.Equals("")) Then
                    name = py.Members.Item(0).Caption
                End If
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    Dim val As String = cs(x, y).FormattedValue
                    dr(x + 1) = cs(x, y).FormattedValue.Replace(",", "") 'other cells in the row
                Next

                '' idSP (swap colonna idPS e Description (prima e ultima colonna)

                
                'dr(x + 1) = dr(0)
                Dim idSP As String = dr(0).ToString()
                dr(0) = dr(4) 'cs(x, y).FormattedValue
                dr(4) = idSP
                
                ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                If (dr("Total Views").ToString().Equals("")) Then
                    Exit For
                End If
                If gridType = TYPE_CONTENTS_INSP_AREA_DETAIL And Search.objAll.IdArea <> "" Then
                    If idSP = Search.objAll.IdArea Then dt.Rows.Add(dr) 'add the row
                Else
                    dt.Rows.Add(dr) 'add the row
                End If
            End If
            y = y + 1
        Next
        Return dt
    End Function

    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If (gridType.Equals(TYPE_CONTENTS_INSP_AREA_DETAIL) Or gridType.Equals(TYPE_CONTENTS_INSP_EVENT_DETAIL)) Then
            
            ColumnInfoArray.Add("Descrizione")
            ColumnInfoArray.Add("by Registered Users")
            ColumnInfoArray.Add("by Anonymous Users")
            ColumnInfoArray.Add("Total views")
            ColumnInfoArray.Add("idSP")
            ColumnInfoArray.Add("% by Registered Users")
            ColumnInfoArray.Add("% by Anonymous Users")
            ColumnInfoArray.Add("% Total views")
        End If
    End Function
    

    ''' <summary>
    ''' Costruisce l'url da linkare ai contenuti della tabella 
    ''' </summary>
    ''' <param name="Campo"></param>
    ''' <param name="RowIndex">Indice della riga</param>
    ''' <param name="ColumnType">Tipo Colonna</param>
    ''' <param name="TypeGrid"></param>
    ''' <returns>string acontenente URL (se ammesso)</returns>
    ''' <remarks></remarks>
    Function GetUrlUsers(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return ""
        If Campo = "" Then Return ""
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then
                
                Dim webRequest As New WebRequestValue
                
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                If RowIndex <> "" And TypeGrid = TYPE_AREA_BY_SITE Then
                    webRequest.customParam.append("IdArea", RowIndex) 'Id Area
                Else
                    webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                End If
                If RowIndex <> "" And TypeGrid = TYPE_EVENT_BY_AREA Then
                    webRequest.customParam.append("IdEvent", RowIndex) 'Id Event
                End If
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                
                webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                If TypeGrid <> "showevents" Then
                    webRequest.KeycontentType.Domain = "CRM"
                    webRequest.KeycontentType.Name = "userdetail"
                Else
                    webRequest.customParam.append("IdArea", RowIndex) 'Id Area
                End If
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                strUrl = "<a href='" + strHref + "' title='User Detail'>"
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
            End If
    End Function
    

  
    Function GetPercent(ByVal value As String) As String
        Return value
        If value Is Nothing Then Return String.Empty
        If value.IndexOf(".") > 0 Then
            value = value.Substring(0, value.IndexOf(".") + 2)
        End If
        If value.Length > 1 Then value += " %"
        Return value
    End Function

</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <div style="font-size:x-large;"><asp:Literal ID="msg" runat="server" /></div>
    <asp:Repeater id="GWAreas" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <colgroup class="cgFirst"/>
			    <thead>				
				    <tr>
					    <th Width="34%" style="text-align:left;">+ Areas</th>
					    <th Width="12%"><%#s_ColumnsInfo(3)%></th>
   					    <th Width="12%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="12%"><%#s_ColumnsInfo(2)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(7)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(5)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(6)%></th>
   					    <th Width="6%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td class="tLeft"><%#Container.DataItem(s_ColumnsInfo(0))%></td>
			    <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3))%></td>
		        <td style="text-align:center;"><%#Container.dataitem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#Container.dataitem(s_ColumnsInfo(2))%></td>
		        <td style="text-align:center;"><%#GetPercent(Container.DataItem(s_ColumnsInfo(7)))%></td>
		        <td style="text-align:center;"><%#GetPercent(Container.DataItem(s_ColumnsInfo(5)))%></td>
		        <td style="text-align:center;"><%#GetPercent(Container.DataItem(s_ColumnsInfo(6)))%></td>
		        <td style="text-align:center;"><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, TYPE_AREA_BY_SITE)%><img  src="/HP3Office/HP3Image/Ico/user_detail.gif" title="User Detail" alt="User Detail"/></a><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, "showevents")%><img  src="/HP3Office/HP3Image/Ico/show_events.gif" title="Event Detail" alt="Event Detail"/></a></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>

   <asp:Repeater id="GWEvents" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <colgroup class="cgFirst"/>
			    <thead>				
				    <tr>
					    <th Width="34%" style="text-align:left;">+ Events</th>
					    <th Width="12%"><%#s_ColumnsInfo(3)%></th>
   					    <th Width="12%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="12%"><%#s_ColumnsInfo(2)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(7)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(5)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(6)%></th>
   					    <th Width="6%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
			    <td style="text-align:center;"><%#Container.dataitem(s_ColumnsInfo(3))%></td>
		        <td style="text-align:center;"><%#Container.dataitem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#Container.dataitem(s_ColumnsInfo(2))%></td>
                <td style="text-align:center;"><%#GetPercent(Container.DataItem(s_ColumnsInfo(7)))%></td>
		        <td style="text-align:center;"><%#GetPercent(Container.DataItem(s_ColumnsInfo(5)))%></td>
		        <td style="text-align:center;"><%#GetPercent(Container.dataitem(s_ColumnsInfo(6)))%></td>
		        <td style="text-align:center;"><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, TYPE_EVENT_BY_AREA)%><img  src="/HP3Office/HP3Image/Ico/user_detail.gif" title="User Detail" alt="User Detail"/></a></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
          	
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>
	