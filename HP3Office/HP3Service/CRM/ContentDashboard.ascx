<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import NameSpace="wsDotNetCharting"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
   
    Dim TYPE_CONTENTS_NAV_CONTENTS As String = "contens_navcontents"
    Dim TYPE_CONTENTS_NAV_SERVICE As String = "contens_navservices"
    Dim TYPE_CONTENTS_NAV_EVENTS As String = "contens_navevents"
    Dim TYPE_CONTENTS_NAV_AREAS As String = "contens_navareas"

    Dim TXT_REG_USER As String = "regUser"
    Dim TXT_TOT_USER As String = "TotUser"
    
    Dim s_ColumnsInfo As New ArrayList
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        'Search.SearchAreaFF = True
        'Search.SearchAreaUT = True
    End Sub
    
    Sub page_prerender()
        Esegui()
    End Sub
        
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            'trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime("2006", 1, 1)
            trackSearcher.DateRangeEnd = Search.DateUpdate
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        'If (Not Search.DdlFF Is Nothing) Then
        '    trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
        '    Search.objAll.FF = Search.DdlFF.Text
        '    Search.objAll.IdFF = Search.DdlFF.Value
        'End If
        
        'If (Not Search.DdlUserType Is Nothing) Then
        '    trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
        '    Search.objAll.UT = Search.DdlUserType.Text
        '    Search.objAll.IdUT = Search.DdlUserType.Value
        'End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Contents"

        Dim dt As DataTable
        ' CONTENTS NAV CONTENTS
        trackSearcher.TypeQuery = TYPE_CONTENTS_NAV_CONTENTS
        cls = crmmanager.ReadCellSet(trackSearcher)
        dt = getDataTable(cls, TYPE_CONTENTS_NAV_CONTENTS, False)
        
        GWContents.DataSource = dt
        GWContents.DataBind()
        
        '' CONTENTS NAV SERVICES
        trackSearcher.TypeQuery = TYPE_CONTENTS_NAV_SERVICE
        cls = crmmanager.ReadCellSet(trackSearcher)
        dt = getDataTable(cls, TYPE_CONTENTS_NAV_SERVICE, False)
        
        GWServices.DataSource = dt
        GWServices.DataBind()

        '' CONTENTS NAV EVENTS
        trackSearcher.TypeQuery = TYPE_CONTENTS_NAV_EVENTS
        cls = crmmanager.ReadCellSet(trackSearcher)
        dt = getDataTable(cls, TYPE_CONTENTS_NAV_EVENTS, False)
        
        GWEvents.DataSource = dt
        GWEvents.DataBind()

        ' CONTENTS NAV AREAS
        trackSearcher.TypeQuery = TYPE_CONTENTS_NAV_AREAS
        ' WORKARIUND, EVENTS sono parte integrante degli assi della query MDX , 
        ' non posso inserirla nella condizione WHERE
        trackSearcher.WhereAreaCondition = ""
        cls = crmmanager.ReadCellSet(trackSearcher)
        dt = getDataTable(cls, TYPE_CONTENTS_NAV_AREAS, False)
 
        GWAreas.DataSource = dt
        GWAreas.DataBind()
        
    End Sub
    

    ''' <summary>
    ''' COstruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) _
                                  As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        s_ColumnsInfo.Clear()
        
        Dim hRes As Integer
        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member
        Dim addRow As Boolean = True

        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If

        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = GetRowHeaderLink(y, gridType)
                If (name.Equals("")) Then
                    name = py.Members.Item(0).Caption
                End If
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    Dim val As String = cs(x, y).FormattedValue
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next

                ' idSP (swap colonna idPS e Description (prima e ultima colonna)
                dr(x + 1) = dr(0)
                dr(0) = cs(x, y).FormattedValue
                
                If dr(4) = "0" Then addRow = False
                If dr(0) = String.Empty Then addRow = False
                
                If addRow Then dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function
    
    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, Optional ByVal TypeGrid As String = "", Optional ByVal TypeLink As Boolean = False) As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then
            
                Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
                
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                webRequest.customParam.append("IdContent", Search.objAll.idContent) 'Id Content
                webRequest.customParam.append("IdUser", Search.objAll.idUser) 'Id User
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                Dim now As DateTime = DateTime.Now()
                Dim tempDate As DateTime
                
                webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                If Search.objAll.DataStart.HasValue() Then
                    QSDateStart = DateTime.Parse(Search.objAll.DataStart).ToString("dd-MM-yyyy")
                Else
                    QSDateStart = ""
                End If
			    
                If Search.objAll.DateEnd.HasValue() Then
                    QSDateEnd = DateTime.Parse(Search.objAll.DateEnd).ToString("dd-MM-yyyy")
                Else
                    QSDateEnd = ""
                End If
                webRequest.customParam.append("IdContent", RowIndex) 'Id Site
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                webRequest.KeycontentType.Domain = "CRM"
                webRequest.KeycontentType.Name = "userdetail"
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                If TypeLink Then
                    strUrl = strHref
                Else
                    strUrl = "<a href='" + strHref + "' title='User Detail'>" & strCampo & "</a>"
                End If
                
            Else
                If TypeLink Then
                    strUrl = "#"
                Else
                    strUrl = "<strong>ND</strong>"
                End If
                
            End If
            Return strUrl
        End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        ColumnInfoArray.Add("Description")              ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
        ColumnInfoArray.Add("Views by Registered user")  ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
        ColumnInfoArray.Add("Views by Anonymous user") ' idem
        ColumnInfoArray.Add("Total Views")              ' idem
        ColumnInfoArray.Add("idSP")                     ' non visualizzato 
        'End If
    End Function

    ''' <summary>
    ''' Costruisce il nome della prima riga 
    ''' </summary>
    ''' <param name="rowindex">indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRowHeaderLink(ByVal rowindex As Integer, ByVal TypeGrid As String) As String
        Dim RetVal As String = ""
        Return RetVal
    End Function

    ''' <summary>
    ''' Rstituisce True se la riga deve contenere un elemento linkato 
    ''' </summary>
    ''' <param name="rowindex">Indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns>true se la riga deve essere linkata</returns>
    ''' <remarks></remarks>
    Function GetLinkedInfo(ByVal rowindex As String, ByVal TypeGrid As String) As Boolean
        Dim RetVal As Boolean = True
        Return RetVal
    End Function
    
    Function GetIcon(ByVal value As String, ByVal imagePath As String, ByVal imageTitle As String) As String
        If value <> String.Empty Then
            Return "<img src=""" + imagePath + """ title=""" + imageTitle + """/>"
        End If
        Return Nothing
    End Function
    
    Protected Sub GridContents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWContents.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
  
    Protected Sub GridServices_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWServices.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
    
    Protected Sub GridEvents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWEvents.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
    
    Protected Sub GridAreas_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWAreas.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
	<div>&nbsp;</div>
	
	<asp:GridView ID="GWContents" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="7"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridContents_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Content Views" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#Container.dataitem(s_ColumnsInfo(0))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Registered User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(1))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Anonymous User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(2))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(3))%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <img src="/HP3Office/HP3Image/ico/type_content_small.gif" title="Content Detail" />
                        <a href="<%#GetUrl(Container.dataitem(s_ColumnsInfo(2)),Container.dataitem("idSP"),TXT_REG_USER, TYPE_CONTENTS_NAV_CONTENTS, true)%>" title="User Detail">
                            <%#GetIcon(Container.DataItem(s_ColumnsInfo(2)), "/HP3Office/HP3Image/ico/user_detail.gif", "Users List")%>
                        </a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>

    <div>&nbsp;</div>

<asp:GridView ID="GWServices"
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="7"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridServices_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Services Views" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#Container.dataitem(s_ColumnsInfo(0))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Registered User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(1))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Anonymous User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(2))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(3))%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <img src="/HP3Office/HP3Image/ico/user_detail_inspector.gif" title="User Detail" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
     
     <div>&nbsp;</div>
     <asp:GridView ID="GWEvents" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="7"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridEvents_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Events Views" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#Container.dataitem(s_ColumnsInfo(0))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Registered User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(1))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Anonymous User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(2))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(3))%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <img src="/HP3Office/HP3Image/ico/user_detail_inspector.gif" title="User Detail" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <div>&nbsp;</div>
    <asp:GridView ID="GWAreas" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="7"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridAreas_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Areas Views" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#Container.dataitem(s_ColumnsInfo(0))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Registered User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(1))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Anonymous User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(2))%></div></ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#Container.DataItem(s_ColumnsInfo(3))%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <img src="/HP3Office/HP3Image/ico/user_detail_inspector.gif" title="User Detail" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <div>&nbsp;</div>
    </div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer" />
</div>
	