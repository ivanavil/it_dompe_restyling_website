<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
    Dim TYPE_OVERVIEW_BY_SITE As String = "overviewbysite"
    Dim TYPE_VISITS As String = "visits"
    Dim TYPE_PAGES As String = "pages"
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim TYPE_GETSITE As String = "sites"
    Dim TYPE_OVERVIEW_BY_SITE_PAGES = "overviewbysitePages"
    Dim TYPE_OVERVIEW_BY_SITE_Elapsed = "overviewbysiteElapsed"
    
    Dim s_ColumnsInfo As New ArrayList
    
    Sub Page_Init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub

    Sub Page_Prerender(ByVal sender As Object, ByVal e As EventArgs)
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub

    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet

        ' CALL PAGES ANALYSIS
        ' verifica il data range selezionato
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = False
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            'trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Read"
        trackSearcher.TypeQuery = TYPE_OVERVIEW_BY_SITE
        trackSearcher.WhereData = False
        
        '' CALL REGISTERED
        Dim dt As DataTable
        Try
            trackSearcher.WhereUserType = "Registered"
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            dt = getDataTable(cls, TYPE_OVERVIEW_BY_SITE, trackSearcher.UseRange)
        
            DWRegistered.DataSource = dt
            DWRegistered.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
        End Try
        

        '' CALL ANONYMOUS
        Try
            trackSearcher.WhereUserType = "Anonymous"
            cls = crmmanager.ReadCellSet(trackSearcher)
            ' call display cellset 
            dt = getDataTable(cls, TYPE_OVERVIEW_BY_SITE, trackSearcher.UseRange)
            DWAnonymous.DataSource = dt
            DWAnonymous.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
        End Try
        
        '' CALL REGISTERED Pages 
        
        Try
            trackSearcher.TypeQuery = TYPE_OVERVIEW_BY_SITE_PAGES
            trackSearcher.WhereUserType = ""
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            dt = getDataTable2(cls, TYPE_OVERVIEW_BY_SITE_PAGES, trackSearcher.UseRange, "Registered")
        
            DWRegisteredPages.DataSource = dt
            DWRegisteredPages.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
            'Response.Write(ex.ToString)
        End Try
        
        '' CALL ANONYMOUS Pages 
        
        Try
            trackSearcher.TypeQuery = TYPE_OVERVIEW_BY_SITE_PAGES
            trackSearcher.WhereUserType = ""
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            dt = getDataTable2(cls, TYPE_OVERVIEW_BY_SITE_PAGES, trackSearcher.UseRange, "Anonymous")
        
            DWAnonymousPages.DataSource = dt
            DWAnonymousPages.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
            'Response.Write(ex.ToString)
        End Try

        '' CALL Registered Elapsed 
        Try
            trackSearcher.TypeQuery = TYPE_OVERVIEW_BY_SITE_Elapsed
            trackSearcher.WhereUserType = ""
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            dt = getDataTable2(cls, TYPE_OVERVIEW_BY_SITE_Elapsed, trackSearcher.UseRange, "Registered")
        
            DWRegisteredElapsed.DataSource = dt
            DWRegisteredElapsed.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
            'Response.Write(ex.ToString)
        End Try
        
        '' CALL ANONYMOUS Pages 
        
        ' '' CALL ANONYMOUS Elapsed
        Try
            trackSearcher.TypeQuery = TYPE_OVERVIEW_BY_SITE_Elapsed
            trackSearcher.WhereUserType = ""
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            dt = getDataTable2(cls, TYPE_OVERVIEW_BY_SITE_Elapsed, trackSearcher.UseRange, "Anonymous")
        
            DWAnonymousElapsed.DataSource = dt
            DWAnonymousElapsed.DataBind()
        Catch ex As Exception
            ' Add gestione dell'errore
            'Response.Write(ex.ToString)
        End Try
        
        
    End Sub
    

    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable2(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean, ByVal typeUs As String) As DataTable
        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' modificare il testo e il formato delle colonne in base al tipo passato
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member

        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
       
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim addRow As Boolean = True
        Dim py As Position
        y = 0
       
        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            
            
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(7) = name 'first cell in the row
                
                'If dr(7) > 0 Then 'per non includere eventuali righe senza sito di riferimento (undef.)
                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 3
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next
                
                'Response.Write(py.Members.Item(1).Caption & " " & count)
                
            
                ''NEW -- inserisce l'ultima colonna con 'informazione dell'indice di riga -- gestione del RANGE
                If bRange Then
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Else
                    dr(x + 1) = "ND"
                End If
                
                x = x + 1
                '' aggiunge la colonna idSP
                Try
                    dr(0) = cs(x, y).FormattedValue
                    If gridType = TYPE_OVERVIEW_BY_SITE_Elapsed Then
                        dr(0) = py.Members.Item(2).Caption
                        Response.Write(cs(x, y).FormattedValue)
                    End If
                Catch ex As Exception

                End Try
   
                If Search.objAll.Site <> String.Empty Then
                    If dr(0) <> Search.objAll.Site Then addRow = False
                End If
                    
                ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                If (dr("Total").ToString().Equals("")) Then addRow = False
                If addRow AndAlso name = "Session" AndAlso py.Members.Item(1).Caption = typeUs Then dt.Rows.Add(dr) 'add the row
                If addRow AndAlso name = "ElapsedMedia" AndAlso py.Members.Item(1).Caption = typeUs Then dt.Rows.Add(dr) 'add the row

                
                'End If
            End If
            
            y = y + 1
        Next
        Return dt
    End Function
    
    
    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' modificare il testo e il formato delle colonne in base al tipo passato
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member

        
        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
       
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim addRow As Boolean = True
        Dim py As Position
        y = 0
       
        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            
            
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(7) = name 'first cell in the row
               
                If dr(7) > 0 Then 'per non includere eventuali righe senza sito di riferimento (undef.)
                    ' inserisce i dati Data cells
                    Dim x As Integer
                    For x = 0 To cs.Axes(0).Positions.Count - 3
                        dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                    Next

                    ''NEW -- inserisce l'ultima colonna con 'informazione dell'indice di riga -- gestione del RANGE
                    If bRange Then
                        dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                    Else
                        dr(x + 1) = "ND"
                    End If
                
                    x = x + 1
                    '' aggiunge la colonna idSP
                    dr(0) = cs(x, y).FormattedValue
                    
                    If Search.objAll.Site <> String.Empty Then
                        If dr(0) <> Search.objAll.Site Then addRow = False
                    End If
                    
                    ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                    If (dr("Total").ToString().Equals("")) Then addRow = False
                    If addRow Then dt.Rows.Add(dr) 'add the row
                End If
            End If
            
            y = y + 1
        Next
        Return dt
    End Function
    
    
    ''' <summary>
    ''' Costruisce l'url da linkare ai contenuti della tabella 
    ''' </summary>
    ''' <param name="Campo"></param>
    ''' <param name="RowIndex">Indice della riga</param>
    ''' <param name="ColumnType">Tipo Colonna</param>
    ''' <param name="TypeGrid"></param>
    ''' <returns>string acontenente URL (se ammesso)</returns>
    ''' <remarks></remarks>
    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString
            If strCampo.ToString <> "" And strCampo.ToString <> "ND" Then
                ' Verifica se il campo � in una riga che deve essere linkata (URL) 
                'If Not TypeGrid.Equals("") Then Return Campo
                If TypeGrid.Equals("overviewbysitea") Then Return Campo

                Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
               
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                Dim now As DateTime = DateTime.Now()
                Dim tempDate As DateTime
               webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                If ColumnType = "Range" Then
                    If Search.objAll.DataStart.HasValue() Then
                        QSDateStart = DateTime.Parse(Search.objAll.DataStart).ToString("dd-MM-yyyy")
                    Else
                        QSDateStart = ""
                    End If
			    
                    If Search.objAll.DateEnd.HasValue() Then
                        QSDateEnd = DateTime.Parse(Search.objAll.DateEnd).ToString("dd-MM-yyyy")
                    Else
                        QSDateEnd = ""
                    End If
                End If
                If ColumnType = "All" Then
                    QSDateStart = ""
                    QSDateEnd = ""
                End If
                If ColumnType = "Anno" Then
                    QSDateStart = New DateTime(Search.DateUpdate.Year, 1, 1).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "mesePrec" Then
                    tempDate = New DateTime(Search.DateUpdate.Year, Search.DateUpdate.Month, 1).Subtract(New TimeSpan(1, 0, 0, 0, 0))
                    QSDateEnd = tempDate.ToString("dd-MM-yyyy")
                    QSDateStart = New DateTime(tempDate.Year, tempDate.Month, 1).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "mese" Then
                    QSDateStart = New DateTime(Search.DateUpdate.Year, Search.DateUpdate.Month, 1).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "oggi" Then
                    QSDateStart = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                webRequest.KeycontentType.Domain = "CRM"
                webRequest.KeycontentType.Name = "userdetail"
                
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                strUrl = "<a href='" + strHref + "'>" & strCampo & "</a>"
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
            End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_OVERVIEW_BY_SITE) Then
            
            ColumnInfoArray.Add("Description")
            ColumnInfoArray.Add("Last Day")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("This Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Corrente")
            ColumnInfoArray.Add("Last Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Precedente")
            ColumnInfoArray.Add("This Year")    ' sar� sostituito da multilingue.getDictionary("CRM_Anno_Corrente")
            ColumnInfoArray.Add("Total")    ' sar� sostituito da multilingue.getDictionary("CRM_Da_On_Line")
            ColumnInfoArray.Add("Time Range")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
            ColumnInfoArray.Add("idSP")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
        End If
        If gridType.Equals(TYPE_OVERVIEW_BY_SITE_PAGES) Then
            ColumnInfoArray.Add("Description")
            ColumnInfoArray.Add("Last Day")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("This Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Corrente")
            ColumnInfoArray.Add("Last Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Precedente")
            ColumnInfoArray.Add("This Year")    ' sar� sostituito da multilingue.getDictionary("CRM_Anno_Corrente")
            ColumnInfoArray.Add("Total")    ' sar� sostituito da multilingue.getDictionary("CRM_Da_On_Line")
            ColumnInfoArray.Add("Time Range")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
            ColumnInfoArray.Add("idSP")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
 
        End If
        If gridType.Equals(TYPE_OVERVIEW_BY_SITE_Elapsed) Then
            ColumnInfoArray.Add("Description")
            ColumnInfoArray.Add("Last Day")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("This Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Corrente")
            ColumnInfoArray.Add("Last Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Precedente")
            ColumnInfoArray.Add("This Year")    ' sar� sostituito da multilingue.getDictionary("CRM_Anno_Corrente")
            ColumnInfoArray.Add("Total")    ' sar� sostituito da multilingue.getDictionary("CRM_Da_On_Line")
            ColumnInfoArray.Add("Time Range")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
            ColumnInfoArray.Add("idSP")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
 
        End If
        
    End Function
    
    Function GetTime(ByVal value As String) As String
        If value = String.Empty Or value.IndexOf("ND") >= 0 Then Return value
        If Not IsNumeric(value) Then Return "<strong>ND</strong>"
        Return New TimeSpan(0, 0, Decimal.Parse(value)).ToString()
    End Function
    Function SetND(ByVal value As String)
        If value.ToString <> "" Then
            Return value
        Else
            Return "<strong>ND</strong>"
        End If
    End Function

</script>

<div class="dMain">
<div>&nbsp;</div>
 	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWRegistered" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Visits by registered users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(1)), Container.DataItem(s_ColumnsInfo(7)), "oggi", "overviewbysite")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(2)), Container.DataItem(s_ColumnsInfo(7)), "mese", "overviewbysite")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem(s_ColumnsInfo(7)), "mesePrec", "overviewbysite")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(4)), Container.DataItem(s_ColumnsInfo(7)), "Anno", "overviewbysite")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(5)), Container.DataItem(s_ColumnsInfo(7)), "All", "overviewbysite")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(6)), Container.dataitem(s_ColumnsInfo(7)), "Range", "overviewbysite")%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWAnonymous" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Visits by anonymous users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(1)), 0, "oggi", "overviewbysitea")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(2)), 0, "mese", "overviewbysitea")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(3)), 0, "mesePrec", "overviewbysitea")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(4)), 0, "Anno", "overviewbysitea")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(5)), 0, "All", "overviewbysitea")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(6)), 0, "Range", "overviewbysitea")%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWRegisteredPages" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Pages by registered users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(1)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(2)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(3)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(4)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(5)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(6)))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWAnonymousPages" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Pages by Anonymous users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(1)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(2)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(3)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(4)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(5)))%></td>
		        <td style="text-align:center;"><%#SetND(Container.DataItem(s_ColumnsInfo(6)))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWRegisteredElapsed" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Average Duration by Registered users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(1))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(2))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(3))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(4))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(5))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(6))))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="DWAnonymousElapsed" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Average Duration by Anonymous users</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(1))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(2))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(3))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(4))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(5))))%></td>
		        <td style="text-align:center;"><%#SetND(GetTime(Container.DataItem(s_ColumnsInfo(6))))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
</div>
</div>

<div class="dFooterBoxesContainer">	
	<div class="clearer">
    </div>
</div>
	