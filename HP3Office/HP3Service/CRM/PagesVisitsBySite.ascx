<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Content"%> 
<%@ Import Namespace="Healthware.HP3.Core.User"%> 
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 

<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="System.Data" %> 

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>


<%@ Register tagprefix="HP3" TagName="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagPrefix="HP3" TagName="ctlLanguage" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>


<script language="VB" runat="server" >
    
    Private sortType As ContentGenericComparer.SortType = ContentGenericComparer.SortType.ById
    Private sortOrder As ContentGenericComparer.SortOrder = ContentGenericComparer.SortOrder.DESC
    
    Dim TYPE_VISITS As String = "visits"
    Dim TYPE_PAGES As String = "pages"
    Dim TYPE_ELAPSED As String = "Elapsed"
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim TYPE_GETSITE As String = "sites"
    Dim CMB_SELECT_SITE As String = "Select Site"
    Dim CMB_SELECT_AREA As String = "Select Area"

    Dim s_ColumnsInfo As New ArrayList
    Dim objAll As objFilter
    Private oCollection As SiteAreaCollection

    
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If (Not IsPostBack) Then
            LoadCombos()
        End If
    End Sub

    Sub LoadCombos()
        ' GIOCOS - ADD carica i Site da DB.....
        'cmbSite.Items.Clear()
        Dim dt As DataTable = LoadSiteArea("", TYPE_GETSITE, CMB_SELECT_SITE)
        cmbSite.DataSource = dt
        cmbSite.DataTextField = "Descrizione"
        cmbSite.DataBind()
        
        cmbArea.Items.Add(CMB_SELECT_AREA)
        cmbArea.Text = CMB_SELECT_AREA
        LoadComboPeriod()

    End Sub

    Private Sub LoadComboPeriod()
        cmbPeriod.Items.Add("Select")
        cmbPeriod.Items.Add("last week")
        cmbPeriod.Items.Add("this month")
        cmbPeriod.Items.Add("last month")
        cmbPeriod.Items.Add("last 7 days")
        cmbPeriod.Items.Add("last 30 days")
        cmbPeriod.Items.Add("last 60 days")
        cmbPeriod.Items.Add("last 90 days")
        cmbPeriod.Items.Add("this year")
        cmbPeriod.Items.Add("last year")
        
        
    End Sub
    ''' <summary>
    ''' carica dinamicamente le aree in relazione al site selezionato
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cmbSite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim siteDescr As String = cmbSite.Text
        ' se � selezionata il site di default non viene richiesta 
        ' alcuna ricerca
        If siteDescr.Equals(CMB_SELECT_SITE) Then
            cmbArea.Items.Clear()
            cmbArea.Items.Add(CMB_SELECT_AREA)
            cmbArea.Text = CMB_SELECT_AREA
            Return
        End If

        Dim dt As DataTable = LoadSiteArea(siteDescr, TYPE_AREA_BY_SITE, CMB_SELECT_AREA)
        cmbArea.DataSource = dt
        cmbArea.DataTextField = "Descrizione"
        cmbArea.DataBind()
    End Sub
    
    
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        objAll = New objFilter
        ' CALL PAGES ANALYSIS
        '----------------------------------------
        If (DateRangeStart.Value.HasValue) Then
            trackSearcher.UseRange = True
            trackSearcher.DateRangeStart = DateRangeStart.Value
            objAll.m_bUserRange = True
            objAll.Datadal = trackSearcher.DateRangeStart
        End If
        
        If (DateRangeEnd.Value.HasValue) Then
            trackSearcher.UseRange = True
            trackSearcher.DateRangeEnd = DateRangeEnd.Value
            objAll.m_bUserRange = True
            objAll.Dataal = trackSearcher.DateRangeEnd
        Else
            trackSearcher.UseRange = False
            objAll.m_bUserRange = False
            trackSearcher.DateRangeStart = DateTime.Now()
            trackSearcher.DateRangeEnd = DateTime.Now()
        End If
        
        'Dim oSiteAreaValue As SiteValue = GetSiteSelection(Site)
        If (Not cmbSite.Text.Equals(CMB_SELECT_SITE)) Then
            trackSearcher.WhereSiteCondition = cmbSite.Text
            objAll.Sito = cmbSite.Text
        End If

        If (Not cmbArea.Text.Equals(CMB_SELECT_AREA)) Then
            trackSearcher.WhereAreaCondition = cmbArea.Text
            objAll.Area = cmbArea.Text
        End If
        
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Read"
        '        trackSearcher.TypeQuery = TYPE_VISITS
        trackSearcher.DateStart = DateTime.Now()
        objAll.Data = DateTime.Now()
        
        ' call display cellset 
        Dim dt As DataTable
        
        ' CALL VISIT ANALYSIS
        ''----------------------------------------
        'Try
        trackSearcher.TypeQuery = TYPE_ELAPSED
        cls = crmmanager.ReadCellSet(trackSearcher)
        ' call display cellset 
        dt = getDataTable(cls, TYPE_ELAPSED, trackSearcher.UseRange)
        GWPages.DataSource = dt
        GWPages.DataBind()
        'Catch ex As Exception
        '' ADD gestione dell'errore 
        'End Try
        
    End Sub
    

    ''' <summary>
    ''' COstruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) _
                                  As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' ADD ... modificare il testo e il formato delle colonne in base al tipo 
        ' passato( in realt� devo aggiufngere anche il linguaggio da utilizzare 
        ' e in seguito portare il tutto in un db)
        'Dim ColumnInfoArray As New ArrayList
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member


        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
        ' aggiunge una nuova colonna per contenere l'indice della riga
        'dt.Columns.Add("idSP")

        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = GetRowHeaderLink(y, gridType)
                If (name.Equals("")) Then
                    name = py.Members.Item(0).Caption
                End If
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    Dim val As String = cs(x, y).FormattedValue
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next

                'NEW -- inserisce l'ultima colonna con 'informazione dell'indice di riga -- gestione del RANGE
                If bRange Then
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Else
                    dr(x + 1) = ""
                End If
                
                ' aggiunge la colonna idSP
                dr(x + 2) = y
                

                dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function
    
    Protected Sub btnStartSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Esegui()
    End Sub

    ''' <summary>
    ''' Costruisce l'url da linkare ai contenuti della tabella 
    ''' </summary>
    ''' <param name="Campo"></param>
    ''' <param name="RowIndex">Indice della riga</param>
    ''' <param name="ColumnType">Tipo Colonna</param>
    ''' <param name="TypeGrid"></param>
    ''' <returns>string acontenente URL (se ammesso)</returns>
    ''' <remarks></remarks>
    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, _
                    Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then

                ' Verifica se il campo � in una riga che deve essere linkata (URL) 
                If Not TypeGrid.Equals("") Then
                    If Not GetLinkedInfo(RowIndex, TypeGrid) Then Return Campo
                Else
                    Return Campo
                End If
            
                Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("idArea", objAll.Area)
                webRequest.customParam.append("idSito", objAll.Sito)
                webRequest.customParam.append("Row", objAll.Area)
                
                Dim QSDate As String
                'QSDate += "&Date=" & DateTime.Parse(objAll.Data).ToShortDateString
                webRequest.customParam.append("Date", DateTime.Parse(objAll.Data).ToShortDateString)
            
                If objAll.Datadal.HasValue() Then
                    'QSDate += "&DateRangeStart=" & DateTime.Parse(objAll.Datadal).ToShortDateString
                    QSDate = DateTime.Parse(objAll.Datadal).ToShortDateString
                Else
                    'QSDate = "&DateRangeStart="
                    QSDate = ""
                End If
                webRequest.customParam.append("DateRangeStart", QSDate)
			    
			
                If objAll.Dataal.HasValue() Then
                    'QSDate += "&DateRangeEnd=" & DateTime.Parse(objAll.Dataal).ToShortDateString
                    QSDate = DateTime.Parse(objAll.Dataal).ToShortDateString
                Else
                    'QSDate += "&DateRangeEnd="
                    QSDate = ""
                End If
                webRequest.customParam.append("DateRangeEnd", QSDate)
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
           
                'strUrl = "<a href='/Health1/Overview_pages_visits.aspx?Type=" & ColumnType & "&Row=" & RowIndex & "&idArea=" _
                '           & objAll.Area & "&idSito=" & objAll.Sito & QSDate & "'>" & strCampo & "</a>"
                strUrl = "<a href='" + strHref + "' >" & strCampo & "</a>"
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
        End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_ELAPSED) Then
            ColumnInfoArray.Add("Description")
            ColumnInfoArray.Add("Today")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            
            ColumnInfoArray.Add("This Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Corrente")
            ColumnInfoArray.Add("Last Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Precedente")
            ColumnInfoArray.Add("This Year")    ' sar� sostituito da multilingue.getDictionary("CRM_Anno_Corrente")
            ColumnInfoArray.Add("Total")    ' sar� sostituito da multilingue.getDictionary("CRM_Da_On_Line")
            ColumnInfoArray.Add("Time Range")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
            ColumnInfoArray.Add("idSP")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
        End If
    End Function
    

    ''' <summary>
    ''' Costruisce il nome della prima riga 
    ''' </summary>
    ''' <param name="rowindex">indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRowHeaderLink(ByVal rowindex As Integer, ByVal TypeGrid As String) As String
        Dim RetVal As String = ""
        If TypeGrid.Equals(TYPE_ELAPSED) Then
            Select Case rowindex
                'Case 0
                '    RetVal = "Page views"  ' sar� sostituito da multilingue.getDictionary("Page views")
                'Case 1
                '    RetVal = "Page views by Registered user"
                'Case 2
                '    RetVal = "Page views by Anonymous user"
                'Case 3
                '    RetVal = "Average page views per day"
                'Case 4
                '    RetVal = "Average page views by Registered user"
                'Case 5
                '    RetVal = "Average page views by Anonymous user"
                Case 0
                    RetVal = "Average Duration per day"
                Case 1
                    RetVal = "Average Duration by Registered user"
                Case 2
                    RetVal = "Average Duration Anonymous user"
            End Select
        End If
        Return RetVal
    End Function

    ''' <summary>
    ''' Rstituisce True se la riga deve contenere un elemento linkato 
    ''' </summary>
    ''' <param name="rowindex">Indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns>true se la riga deve essere linkata</returns>
    ''' <remarks></remarks>
    Function GetLinkedInfo(ByVal rowindex As String, ByVal TypeGrid As String) As Boolean
        Dim RetVal As Boolean = True
        If TypeGrid.Equals(TYPE_VISITS) Then
            Select Case rowindex
                Case "0"
                    RetVal = True     ' solo la prima riga � linkata
                Case Else
                    RetVal = False
            End Select
        End If

        If TypeGrid.Equals(TYPE_PAGES) Then RetVal = False ' nessuna riga � linkata
        Return RetVal
    End Function

    'Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
    '    Dim oSiteAreaCollection As Object = olistItem.GetSelection
    '    If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
    '        Return oSiteAreaCollection.item(0)
    '    End If
    '    Return Nothing
    'End Function
    
    ''' <summary>
    ''' Classe utilizzata per mantenere le selezioni di filtro 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class objFilter
        Public Data As Nullable(Of DateTime)
        Public m_bUserRange As Boolean = False
        Public Datadal As Nullable(Of DateTime)
        Public Dataal As Nullable(Of DateTime)
        Public Sito As String = ""
        Public Area As String = ""
    End Class

    
    ' GIOCOS NEW - 15-05-2007 
    '********************************
    Function LoadSiteArea(ByVal siteDescr As String, ByVal MDXType As String, ByVal DefaultText As String) As DataTable
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        trackSearcher.StoredName = "SP_HP3_CRM_MDX_GETSA_Read"
        trackSearcher.TypeQuery = MDXType
        trackSearcher.WhereSiteCondition = siteDescr  ' non usato per il site
        
        Dim dt As New DataTable
        'Dim dc As DataColumn
        Dim dr As DataRow

        dt.Columns.Add(New DataColumn("Descrizione"))
        dt.Rows.Add(DefaultText) 'add first row
        

        Dim cls As CellSet = crmmanager.ReadCellSet(trackSearcher)
        If cls Is Nothing Then Return Nothing
        
        ' Inserisce i valori nella grid
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim py As Position
        For count As Integer = 0 To cls.Axes(1).Positions.Count - 1
            py = cls.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            Dim name As String = ""
            If Not py.Members(0).Caption.Equals("") Then
                'name = GetRowHeader1(y, gridType)
                'If (name.Equals("")) Then
                name = py.Members.Item(0).Caption
                'End If
                dr(0) = name 'first cell in the row
                dt.Rows.Add(dr) 'add the row
            End If
        Next
        Return dt
    End Function

    ' TEST ONLY ---------------------------------------------------------------------------------
    'Dim olapStringConn As String = "Provider=MSOLAP;Data Source=HISERVER;Initial Catalog=HP3_V2"
    'Dim olapConn As New AdomdConnection()
    'olapConn.ConnectionString = olapStringConn
    'olapConn.Open()
    '--------------------------------------------------------------------------------------------
   
    Protected Sub cmbPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (cmbPeriod.Text.Equals("Select")) Then
            ' add clear data range selection
            DateRangeStart.Clear()
            DateRangeEnd.Clear()
            Return
        End If
        
        Dim sel As String = cmbPeriod.Text
        
        Dim dtToday As Date = DateTime.Now
        
        Dim ts As TimeSpan = Nothing
        Dim dtLast As Date = Nothing
        
        Select Case sel
            Case "last week"
                ' da rivedere 
                Dim DayofWeek As Integer = dtToday.DayOfWeek()
                dtToday = dtToday.Subtract(New TimeSpan(DayofWeek, 0, 0, 0, 0))
                dtLast = dtToday.Subtract(New TimeSpan(6, 0, 0, 0, 0))
                
            Case "this month"
                dtLast = New DateTime(dtToday.Year, dtToday.Month, 1)
                
            Case "last month"
                dtToday = New DateTime(dtToday.Year, dtToday.Month, 1)
                dtToday = dtToday.Subtract(New TimeSpan(1, 0, 0, 0, 0))
                dtLast = New DateTime(dtToday.Year, dtToday.Month, 1)
                
            Case "last 7 days"
                dtLast = dtToday.Subtract(New TimeSpan(6, 0, 0, 0, 0))
            Case "last 30 days"
                dtLast = dtToday.Subtract(New TimeSpan(29, 0, 0, 0, 0))
            Case "last 60 days"
                dtLast = dtToday.Subtract(New TimeSpan(59, 0, 0, 0, 0))
            Case "last 90 days"
                dtLast = dtToday.Subtract(New TimeSpan(89, 0, 0, 0, 0))
            Case "this year"
                dtLast = New DateTime(dtToday.Year, 1, 1)
               
            Case "last year"
                dtToday = New DateTime(dtToday.Year, 1, 1)
                dtToday = dtToday.Subtract(New TimeSpan(1, 0, 0, 0, 0))
                dtLast = New DateTime(dtToday.Year, 1, 1)
               
                'Case Else
        End Select
        DateRangeStart.Value = dtLast
        DateRangeEnd.Value = dtToday
    End Sub
    
    ' ADD ... inserire in una fuunzione comune.
    'Public Function GetCubeInfo(ByVal cubeName As String) As Date
    '    'Get a reference to an AdomdConnection object.
    '    Dim objConnection As AdomdConnection = New AdomdConnection()
    '    objConnection.ConnectionString = _strAdoMdConnectionString
    '    objConnection.Open()
    '    Dim cubedf As CubeDef = objConnection.Cubes.Item(cubeName)
    '    Return cubedf.LastUpdated
    'End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
    <asp:Panel ID="PanelMenu" runat="server" Height="125px" Width="100%">
        <table class="form">
        <tr>
            <td style="width: 171px"><strong>Sites</strong></td>
            <td style="width: 165px"><strong>Areas</strong></td>
            <td style="width: 241px"></td>
            <td style="width: 197px">
                <asp:Label ID="LastUpdate" runat="server" Width="257px" ></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 171px"><asp:DropDownList ID="cmbSite" runat="server" Width="136px" autopostback="true" OnSelectedIndexChanged="cmbSite_SelectedIndexChanged" ></asp:DropDownList></td>  
            <td style="width: 165px"><asp:DropDownList ID="cmbArea" runat="server" Width="136px"></asp:DropDownList></td>  
            <td style="width: 241px"></td>
            <td style="width: 197px"></td>
        </tr>
        </table> 

        <table class="form">
        <tr>
            <td><strong>Period</strong></td> 
            <td style="width: 171px"><strong>Start</strong></td> 
            <td><strong>End</strong></td> 
        </tr> 
        <tr>
            <td style="width: 171px"><asp:DropDownList ID="cmbPeriod" runat="server" Width="136px" autopostback="true" OnSelectedIndexChanged="cmbPeriod_SelectedIndexChanged" ></asp:DropDownList></td>  
            <td style="width: 171px"><HP3:Date runat="server" ID="DateRangeStart" TypeControl="JsCalendar"/></td> 
            <td><HP3:Date runat="server" ID="DateRangeEnd" TypeControl="JsCalendar"/></td> 
        </tr> 
            <tr>
                <td>
                </td>
                <td style="width: 171px">
                </td>
            </tr>
        <tr>
            <td></td>
            <td style="width: 171px"><asp:Button ID="btnStartSearch" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" OnClick="btnStartSearch_Click" /></td>
        </tr>
        </table> 

        </asp:Panel>

		<div class="" style="float:left;">			
            <asp:Label ID="lblContent" CssClass="title" runat="server">Last Content Loaded</asp:Label>
            <div>&nbsp;</div>
			<asp:GridView         
            ID="GridContents" AutoGenerateColumns="false" CssClass="tbl1"
            AlternatingRowStyle-BackColor="#EEEFEF"
            AllowPaging="false"
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            EnableViewState="false"
            emptydatatext="No content available"
            Width="100%"
            GridLines="None" >
            <Columns>
             <asp:TemplateField HeaderText=" " ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <img alt="Type: <%#Container.DataItem.IdContentSubType%>" 

src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DatePublish" HeaderText="Date Pub." SortExpression="DatePublish" HeaderStyle-Width="10%" 

ItemStyle-Width="10%" HtmlEncode="False" DataFormatString="{0:d}"/>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="" 

src="/hp3Office/HP3Image/Language/ico_flag_<%#Me.BusinessLanguageManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gi

f" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" HeaderStyle-Width="60%" 

ItemStyle-Width="60%"/>

            <asp:TemplateField HeaderText="Service" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ContentType.Description")%></ItemTemplate>
            </asp:TemplateField>
            </Columns>
            </asp:GridView>
		</div>
		<div align=left></div>
        <br />
    <br />

    <br />
    <strong style="text-align: left">PAGES Elapsed </strong><br />
    <br />

    <asp:Repeater id="GWPages" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <colgroup class="cgFirst"/>
			    <thead>				
				    <tr>
					    <th Width="30%">&nbsp;</th>
   					    <!--<th><%#s_ColumnsInfo(0)%></th>-->
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td class="tLeft">
    				<asp:label id="btnPeriodo"  tooltip="descrizione" Text='<%#Container.dataitem(s_ColumnsInfo(0))%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="btnMese" Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(1)),Container.dataitem("idSP"),"oggi", "pages")%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="Literal1" Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(2)),Container.dataitem("idSP"),"mese", "pages")%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="Literal2" Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(3)),Container.dataitem("idSP"),"mesePrec", "pages")%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="Literal3"  Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(4)),Container.dataitem("idSP"),"Anno", "pages")%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="Literal4" Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(5)),Container.dataitem("idSP"),"All", "pages")%>' runat="server" />
			    </td>
		        <td class="tCenter" align=right>
				    <asp:literal id="Literal6" Text='<%#GetUrl(Container.dataitem(s_ColumnsInfo(6)),Container.dataitem("idSP"),"Range", "pages")%>' runat="server" />
			    </td>
    	    </tr>
	    </itemtemplate>
    					
	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 

        	
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>
	