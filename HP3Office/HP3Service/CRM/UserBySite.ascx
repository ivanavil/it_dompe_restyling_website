<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import NameSpace="DotNetCharting"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
   
    Dim TYPE_USERS_BY_SITE As String = "usersbysite"
    
    Dim s_ColumnsInfo As New ArrayList
    Dim ws As Chart
   
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        'Search.SearchAreaFF = True
        'Search.SearchAreaUT = True
    End Sub

    Sub page_prerender()
        Esegui()
    End Sub
    
    Sub showGraph(ByVal title As String, ByVal titlex As String, ByVal titley As String, ByVal ds As DataSet)
        Try
            ws = New Chart
            ws.Title = title
            ws.Type = ChartType.Pie
            ws.DefaultElement.ShowValue = True
            ws.DefaultElement.ExplodePieSlice = True
            ws.PieLabelMode = PieLabelMode.Outside
            ws.DefaultElement.Transparency = 20
            ws.ChartArea.ClearColors()
            ws.YAxis.Scale = Scale.Normal
            ws.Use3D = True
            ws.Size = "540x370"
            ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", 0, ""))
            ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
            ImageChart.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
            ImageChart.Attributes.Add("usemap", ws.ImageMapName)
        Catch ex As Exception
        End Try
    End Sub
    
    Function getSeries(ByVal src As DataSet, ByVal FieldX As String, ByVal FieldY As String, ByVal argb As Int32, ByVal nameSerie As String, Optional ByVal indexTables As Integer = 0) As SeriesCollection
        If src Is Nothing Then Return Nothing
        Dim SC As New SeriesCollection()
        Dim s As Series
        Dim objRow As DataRow
	 
        For Each objRow In src.Tables(indexTables).Rows
            s = New Series
            If objRow(FieldY) <> String.Empty Then
                Dim e As New Element()
                'e.Name = 
                e.YValue = objRow(FieldY)
                s.Elements.Add(e)
                s.Name = GetLegend(objRow(FieldX))
                SC.Add(s)
            End If
        Next
        
        Dim oColor As Color = Color.FromArgb(argb)
		 
        
        Return SC
    End Function
    
    Function GetLegend(ByVal value As Object) As String
        If value Is DBNull.Value Then Return ""
        If value.ToString.ToString.Length > 15 Then Return value.ToString.Substring(0, 15)
        Return value.ToString
    End Function
    
    Function RouteDataTable(ByVal dtSource As DataTable) As DataSet
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable("route")
            ds.Tables.Add(dt)
            Dim dc1 As New DataColumn("Name")
            Dim dc2 As New DataColumn("Value")
            dt.Columns.Add(dc1)
            dt.Columns.Add(dc2)
            For i As Integer = 0 To dtSource.Rows.Count - 1
                Dim dr As DataRow = dt.NewRow
                dr("Name") = dtSource.Rows(i).Item(0)
                dr("Value") = dtSource.Rows(i).Item(1)
                dt.Rows.Add(dr)
            Next

            Return ds
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet

        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            'trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime("2006", 1, 1)
            trackSearcher.DateRangeEnd = Search.DateUpdate
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            'trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        'If (Not Search.DdlArea Is Nothing) Then
        '    trackSearcher.WhereAreaCondition = Search.DdlArea.Text
        '    Search.objAll.Area = Search.DdlArea.Text
        '    Search.objAll.IdArea = Search.DdlArea.Value
        'End If

        'verifica se il filtro FF (Field Force) � selezionato
        'If (Not Search.DdlFF Is Nothing) Then
        '    trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
        '    Search.objAll.FF = Search.DdlFF.Text
        '    Search.objAll.IdFF = Search.DdlFF.Value
        'End If
        
        'If (Not Search.DdlUserType Is Nothing) Then
        '    trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
        '    Search.objAll.UT = Search.DdlUserType.Text
        '    Search.objAll.IdUT = Search.DdlUserType.Value
        'End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Users"
        trackSearcher.TypeQuery = TYPE_USERS_BY_SITE
       
        'CALL REGISTERED
        trackSearcher.WhereUserType = "Registered"
        Try
            cls = crmmanager.ReadCellSet(trackSearcher)
            'Response.Write(crmmanager.ReadMDX(trackSearcher))
            Dim dt As DataTable
            dt = getDataTable(cls, TYPE_USERS_BY_SITE, trackSearcher.UseRange)
            
            GWUser.DataSource = dt
            GWUser.DataBind()
            
            'call display cellset 
            Dim ds As DataSet = RouteDataTable(dt)
            showGraph("User by Site", "User", "Site", ds)
            
        Catch ex As Exception
            
        End Try
    End Sub
    

    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) _
                                  As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' ADD ... modificare il testo e il formato delle colonne in base al tipo 
        ' passato( in realt� devo aggiufngere anche il linguaggio da utilizzare 
        ' e in seguito portare il tutto in un db)
        'Dim ColumnInfoArray As New ArrayList
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member
        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
        
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0
        Dim addRow As Boolean = True

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next

                ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                If (dr("User").ToString().Equals("") Or dr("User").ToString().Equals("0")) Then
                    Exit For
                End If
                
                ' '' aggiunge la colonna idSP (swap tra prima e ultima colonna)
                'Dim idsp As String = dr(0).ToString()
                dr(x + 1) = dr(0)
                dr(0) = cs(x, y).FormattedValue
                
                If Search.objAll.Site <> String.Empty Then
                    If dr(0) <> Search.objAll.Site Then addRow = False
                End If
                
                If addRow Then dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function
    


    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then
            
                Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
                
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                Dim now As DateTime = DateTime.Now()
                Dim tempDate As DateTime
                
                webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                If Search.objAll.DataStart.HasValue() Then
                    QSDateStart = DateTime.Parse(Search.objAll.DataStart).ToString("dd-MM-yyyy")
                Else
                    QSDateStart = ""
                End If
			    
                If Search.objAll.DateEnd.HasValue() Then
                    QSDateEnd = DateTime.Parse(Search.objAll.DateEnd).ToString("dd-MM-yyyy")
                Else
                    QSDateEnd = ""
                End If
                webRequest.customParam.append("IdSite", RowIndex) 'Id Site
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                webRequest.KeycontentType.Domain = "CRM"
                webRequest.KeycontentType.Name = "eventispec"
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                strUrl = "<a href='" + strHref + "' title='User Detail'>" & strCampo & "</a>"
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
        End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_USERS_BY_SITE) Then
            
            ColumnInfoArray.Add("Site")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
            ColumnInfoArray.Add("User")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
            ColumnInfoArray.Add("%")        ' sar� sostituito da multilingue.getDictionary(".....")
            ColumnInfoArray.Add("idSP")     ' non visualizzato 
        End If
    End Function
    

    
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
	<div>&nbsp;</div>
	<div style="float:right;"><asp:Image id="ImageChart" runat="server"/></div>
	<div style="float:left;">
	<asp:Repeater id="GWUser" runat="Server">
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="70%" style="text-align:left;">+ User by Site</th>
   					    <th Width="15%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="15%"><%#s_ColumnsInfo(2)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(1)), Container.DataItem("idSP"), "User", TYPE_USERS_BY_SITE)%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(2))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
         </div>
        	
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer">
    </div>
</div>
	