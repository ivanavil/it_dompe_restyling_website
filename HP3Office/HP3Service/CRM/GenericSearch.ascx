<%@ Control Language="VB" ClassName="GenericSearch" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 

<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>

<script language="VB" runat="server" >

    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim TYPE_GETSITE As String = "sites"
    Dim CMB_SELECT_SITE As String = "Select Site"
    Dim CMB_SELECT_AREA As String = "Select Area"
    Dim CMB_SELECT_FFCONTEXT As String = "Select FF Context"
    Dim CMB_SELECT_DTCONTEXT As String = "Select DT Context"

    Public objAll As New objFilter
    Private oCollection As SiteAreaCollection
    Private _dateStart As Nullable(Of DateTime) = Nothing
    Private _dateEnd As Nullable(Of DateTime) = Nothing
    
    Public SearchAreaSite As Boolean = False
    Public SearchAreaEvent As Boolean = False
    Public SearchAreaFF As Boolean = False
    Public SearchAreaUT As Boolean = False
    Public SearchAreaUser As Boolean = False
    Public SearchAreaContent As Boolean = False
    
    Public DateUpdate As Date
    
    Public ReadOnly Property DdlSite() As System.Web.UI.WebControls.ListItem
        Get
            If cmbSite.SelectedIndex > 0 Then
                Return cmbSite.SelectedItem
            Else
                Return Nothing
            End If
        End Get
    End Property
    
    Public ReadOnly Property DdlArea() As System.Web.UI.WebControls.ListItem
        Get
            If cmbArea.SelectedIndex > 0 Then
                Return cmbArea.SelectedItem
            Else
                Return Nothing
            End If
        End Get
    End Property
    
    Public ReadOnly Property DdlFF() As System.Web.UI.WebControls.ListItem
        Get
            If cmbFFContext.SelectedIndex > 0 Then
                Return cmbFFContext.SelectedItem
            Else
                Return Nothing
            End If
        End Get
    End Property
    
    Public ReadOnly Property DdlUserType() As System.Web.UI.WebControls.ListItem
        Get
            If cmbDTContext.SelectedIndex > 0 Then
                Return cmbDTContext.SelectedItem
            Else
                Return Nothing
            End If
        End Get
    End Property
    
    Public ReadOnly Property DateStart() As Nullable(Of DateTime)
        Get
            Return _dateStart
        End Get
    End Property
    
    Public ReadOnly Property DateEnd() As Nullable(Of DateTime)
        Get
            Return _dateEnd
        End Get
    End Property
    
    Public ReadOnly Property DdlEvent() As System.Web.UI.WebControls.ListItem
        Get
            If cmbEvent.SelectedIndex > 0 Then
                Return cmbEvent.SelectedItem
            Else
                Return Nothing
            End If
        End Get
    End Property
    
    Public ReadOnly Property IdUser() As String
        Get
            Return txtIdUser.Text
        End Get
    End Property
    
    Public ReadOnly Property IdContent() As String
        Get
            Return txtIdContent.Text
        End Get
    End Property
    
    Public webRequest As New WebRequestValue
    
    Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs)
        If (Not IsPostBack) Then
            webRequest.customParam.LoadQueryString()
            LoadCombos()
        End If
        Dim crmmanager As New WebTrackManager()
        DateUpdate = crmmanager.GetCubeLastUpdate("HP3_PROD1")
    End Sub

    Sub LoadCombos()
        ' GIOCOS - ADD carica i Site da DB.....
        'cmbSite.Items.Clear()
        Dim dt As DataTable = LoadSiteArea("", TYPE_GETSITE, CMB_SELECT_SITE)
        cmbSite.DataSource = dt
        cmbSite.DataTextField = "Descrizione"  ' indica quale colonna deve essere visualizata nella combo 
        cmbSite.DataValueField = "ID"
        cmbSite.DataBind()
        

        If webRequest.customParam.item("IdSite") <> String.Empty Then
            cmbSite.SelectedIndex = cmbSite.Items.IndexOf(cmbSite.Items.FindByValue(webRequest.customParam.item("IdSite")))
            
            Dim dtArea As DataTable = LoadSiteArea(cmbSite.SelectedItem.Text, TYPE_AREA_BY_SITE, CMB_SELECT_AREA)
            cmbArea.DataSource = dtArea
            cmbArea.DataTextField = "Descrizione"  ' indica quale colonna deve essere visualizata nella combo 
            cmbArea.DataValueField = "ID"
            cmbArea.DataBind()
            
            If webRequest.customParam.item("IdArea") <> String.Empty Then
                cmbArea.SelectedIndex = cmbArea.Items.IndexOf(cmbArea.Items.FindByValue(webRequest.customParam.item("IdArea")))
            End If
        Else
            cmbArea.Items.Add(CMB_SELECT_AREA)
            cmbArea.Text = CMB_SELECT_AREA
        End If
       
        LoadComboPeriod()

        Dim CSearcher As New ContextSearcher
        Dim wUtility As New WebUtility
        Dim groupFF As String = ConfigurationManager.AppSettings("GroupContextFF")
        If groupFF <> String.Empty Then

            CSearcher.KeyContextGroup.Id = Integer.Parse(groupFF)
            CSearcher.KeyFather.Id = -1
            'field force
            cmbFFContext.Items.Clear()
            wUtility.LoadListControl(cmbFFContext, Me.BusinessContextManager.Read(CSearcher), "Description", "Key.Id")
            '            cmbFFContext.Items.Insert(0, New ListItem("Select Field Force", CMB_SELECT_FFCONTEXT))
            cmbFFContext.Items.Insert(0, New ListItem("Select Field Force", -1))
            If webRequest.customParam.item("IdFF") <> String.Empty Then
                cmbFFContext.SelectedIndex = cmbFFContext.Items.IndexOf(cmbFFContext.Items.FindByValue(webRequest.customParam.item("IdFF")))
            End If
            'If IsNumeric(cmbFFContext.SelectedValue) Then

            '    LoadFFChild(cmbFFContext.SelectedValue, cmbFFContext.SelectedItem.Text)
            'End If
        End If
        
        Dim groupUT As String = ConfigurationManager.AppSettings("GroupContextUT")
        If groupUT <> String.Empty Then
            'user type
            CSearcher.KeyContextGroup.Id = Integer.Parse(groupUT)
            CSearcher.KeyFather.Id = 0
            cmbDTContext.Items.Clear()
            wUtility.LoadListControl(cmbDTContext, Me.BusinessContextManager.Read(CSearcher), "Description", "Key.Id")
            cmbDTContext.Items.Insert(0, New ListItem("Select User Type", CMB_SELECT_DTCONTEXT))
            If webRequest.customParam.item("IdUT") <> String.Empty Then
                cmbDTContext.SelectedIndex = cmbDTContext.Items.IndexOf(cmbDTContext.Items.FindByValue(webRequest.customParam.item("IdUT")))
            End If
        End If
        
        'events
        Dim wtManager As New WebTrackManager()
        Dim wtSearcher As New WebTrackSearcher()
        ' se selezionato un sito filtra solo gli eventi tracciati per il sito
        If webRequest.customParam.item("IdSite") <> String.Empty Then
            wtSearcher.IdSite = Integer.Parse(webRequest.customParam.item("IdSite"))
        End If

        cmbEvent.Items.Clear()
        wUtility.LoadListControl(cmbEvent, wtManager.ReadTraceEvents(wtSearcher), "Description", "Key.Id")
        cmbEvent.Items.Insert(0, New ListItem("Select Event", 0))
        If webRequest.customParam.item("IdEvent") <> String.Empty Then
            cmbEvent.SelectedIndex = cmbEvent.Items.IndexOf(cmbEvent.Items.FindByValue(webRequest.customParam.item("IdEvent")))
        End If
        
        'Recupera le date da querystring
        If webRequest.customParam.item("DateStart") <> String.Empty Then
            DateRangeStart.Value = DateTime.Parse(webRequest.customParam.item("DateStart"))
        End If

        If webRequest.customParam.item("DateEnd") <> String.Empty Then
            DateRangeEnd.Value = DateTime.Parse(webRequest.customParam.item("DateEnd"))
        End If
        
        If webRequest.customParam.item("IdContent") <> String.Empty Then
            txtIdContent.Text = webRequest.customParam.item("IdContent")
        End If

        If webRequest.customParam.item("IdUser") <> String.Empty Then
            txtIdUser.Text = webRequest.customParam.item("IdUser")
        End If
    End Sub
    
    Private Sub LoadComboPeriod()
        cmbPeriod.Items.Add("Select Period")
        cmbPeriod.Items.Add("last week")
        cmbPeriod.Items.Add("this month")
        cmbPeriod.Items.Add("last month")
        cmbPeriod.Items.Add("last 7 days")
        cmbPeriod.Items.Add("last 30 days")
        cmbPeriod.Items.Add("last 60 days")
        cmbPeriod.Items.Add("last 90 days")
        cmbPeriod.Items.Add("this year")
        cmbPeriod.Items.Add("last year")
    End Sub
    
    ''' <summary>
    ''' carica dinamicamente le aree in relazione al site selezionato
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cmbSite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DateRangeStart.Value.HasValue Then
            _dateStart = DateRangeStart.Value
        End If
        If DateRangeEnd.Value.HasValue Then
            _dateEnd = DateRangeEnd.Value
        End If
        Dim siteDescr As String = cmbSite.SelectedItem.Text
        ' se � selezionata il site di default non viene richiesta 
        ' alcuna ricerca
        If siteDescr.Equals(CMB_SELECT_SITE) Then
            cmbArea.Items.Clear()
            cmbArea.Items.Add(CMB_SELECT_AREA)
            cmbArea.Text = CMB_SELECT_AREA
            Return
        End If

        Dim dt As DataTable = LoadSiteArea(siteDescr, TYPE_AREA_BY_SITE, CMB_SELECT_AREA)
        cmbArea.DataSource = dt
        cmbArea.DataTextField = "Descrizione"  ' indica quale colonna deve essere visualizata nella combo 
        cmbArea.DataValueField = "ID"
        cmbArea.DataBind()
    End Sub
    Protected Sub btnStartSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        LoadFFChild(cmbFFContext.SelectedValue, cmbFFContext.SelectedItem.Text)
        If DateRangeStart.Value.HasValue Then
            _dateStart = DateRangeStart.Value
        End If
        If DateRangeEnd.Value.HasValue Then
            _dateEnd = DateRangeEnd.Value
        End If
    End Sub
   

    ''' <summary>
    ''' Classe utilizzata per mantenere le selezioni di filtro 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class objFilter
        Public DateUpd As Nullable(Of DateTime)
        Public UseRange As Boolean = False
        Public WhereData As Boolean = False
        Public DataStart As Nullable(Of DateTime)
        Public DateEnd As Nullable(Of DateTime)
        Public Site As String = ""
        Public IdSite As String = ""
        Public Area As String = ""
        Public IdArea As String = ""
        Public FF As String = ""
        Public IdFF As String = ""
        Public UT As String = ""
        Public IdUT As String = ""
        Public idEvent As String = ""
        Public idContent As String = ""
        Public idUser As String = ""
    End Class

    
    ' GIOCOS NEW - 15-05-2007 
    '********************************
    Function LoadSiteArea(ByVal siteDescr As String, ByVal MDXType As String, ByVal DefaultText As String) As DataTable
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        trackSearcher.StoredName = "SP_HP3_CRM_MDX_GETSA_Read"
        trackSearcher.TypeQuery = MDXType
        trackSearcher.WhereSiteCondition = siteDescr  ' non usato per il site

        Dim dt As New DataTable
        'Dim dc As DataColumn
        Dim dr As DataRow

        dt.Columns.Add(New DataColumn("Descrizione"))
        dt.Columns.Add(New DataColumn("ID"))
        dt.Rows.Add(DefaultText, 0) 'add first row

        Dim cls As CellSet = crmmanager.ReadCellSet(trackSearcher)
        If cls Is Nothing Then Return Nothing
        
        ' Inserisce i valori nella grid
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim py As Position
        For count As Integer = 0 To cls.Axes(1).Positions.Count - 1
            py = cls.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            Dim name As String = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(0) = name 'first cell in the row
                dr(1) = cls(1, count).Value
                dt.Rows.Add(dr) 'add the row
            End If
        Next
        Return dt
    End Function
   
    Protected Sub cmbPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (cmbPeriod.Text.Equals("Select")) Then
            ' add clear data range selection
            DateRangeStart.Clear()
            DateRangeEnd.Clear()
            Return
        End If
        
        Dim sel As String = cmbPeriod.Text
        Dim dtToday As Date = DateTime.Now
        Dim ts As TimeSpan = Nothing
        Dim dtLast As Date = Nothing
        
        Select Case sel
            Case "last week"
                ' da rivedere 
                Dim DayofWeek As Integer = dtToday.DayOfWeek()
                dtToday = dtToday.Subtract(New TimeSpan(DayofWeek, 0, 0, 0, 0))
                dtLast = dtToday.Subtract(New TimeSpan(6, 0, 0, 0, 0))
                
            Case "this month"
                dtLast = New DateTime(dtToday.Year, dtToday.Month, 1)
                
            Case "last month"
                dtToday = New DateTime(dtToday.Year, dtToday.Month, 1)
                dtToday = dtToday.Subtract(New TimeSpan(1, 0, 0, 0, 0))
                dtLast = New DateTime(dtToday.Year, dtToday.Month, 1)
                
            Case "last 7 days"
                dtLast = dtToday.Subtract(New TimeSpan(6, 0, 0, 0, 0))
            Case "last 30 days"
                dtLast = dtToday.Subtract(New TimeSpan(29, 0, 0, 0, 0))
            Case "last 60 days"
                dtLast = dtToday.Subtract(New TimeSpan(59, 0, 0, 0, 0))
            Case "last 90 days"
                dtLast = dtToday.Subtract(New TimeSpan(89, 0, 0, 0, 0))
            Case "this year"
                dtLast = New DateTime(dtToday.Year, 1, 1)
               
            Case "last year"
                dtToday = New DateTime(dtToday.Year, 1, 1)
                dtToday = dtToday.Subtract(New TimeSpan(1, 0, 0, 0, 0))
                dtLast = New DateTime(dtToday.Year, 1, 1)
        End Select
        DateRangeStart.Value = dtLast
        DateRangeEnd.Value = dtToday
    End Sub
    
    'Protected Sub cmbFFContext_Changed(ByVal s As Object, ByVal e As System.EventArgs)
    '    ' LoadFFChild(cmbFFContext.SelectedValue, cmbFFContext.SelectedItem.Text)
    'End Sub
       
    Private Sub LoadFFChild(ByVal selVal As Integer, ByVal selText As String)
        If IsNumeric(selVal) Then
            Dim selectedVal As Integer = selVal
            Dim CSearcher As New ContextSearcher
            Dim wUtility As New WebUtility
            Dim groupFF As String = ConfigurationManager.AppSettings("GroupContextFF")
            If groupFF <> String.Empty Then
                CSearcher.KeyContextGroup.Id = Integer.Parse(groupFF)
                CSearcher.KeyFather.Id = selectedVal
                'field force
                cmbFFContext.Items.Clear()
                wUtility.LoadListControl(cmbFFContext, Me.BusinessContextManager.Read(CSearcher), "Description", "Key.Id")
                cmbFFContext.Items.Insert(0, New ListItem("Select Field Force", -1))
                If selText <> "Select Field Force" AndAlso selectedVal > 0 Then
                    cmbFFContext.Items.Insert(1, New ListItem(selText, selectedVal))
                    cmbFFContext.Items.Insert(2, New ListItem("-----", -1))
                    cmbFFContext.Items(1).Selected = True
                End If
            End If
        End If
    End Sub
    
</script>
<asp:Panel ID="PanelMenu" runat="server" cssclass="boxSearcher">
        <table class="topContentSearcher">
        <tr>
            <td style="border:0px solid #ffffff;" width="1%"><img src="/hp3Office/HP3Image/ico/content_searcher.gif" align="left"/></td>
            <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Searcher - Last update: <%=DateUpdate.ToString("dd/MM/yyyy")%></span></td>
        </tr>
        </table>
        <fieldset class="_Search">
        <table class="form">
        <%if SearchAreaSite = True then %>
        <tr>
            <td style="width: 80px"><strong>Sites</strong></td>
            <td style="width: 200px"><asp:DropDownList ID="cmbSite" runat="server" Width="190px" autopostback="true" OnSelectedIndexChanged="cmbSite_SelectedIndexChanged" ></asp:DropDownList></td>
            <td style="width: 80px"><strong>Areas</strong></td>  
            <td style="width: 200px"><asp:DropDownList ID="cmbArea" runat="server" Width="190px"></asp:DropDownList></td>  
            <td style="width: 80px"><%if SearchAreaEvent = True then %><strong>Events</strong><%end if %></td>  
            <td style="width: 180px"><%if SearchAreaEvent = True then %><asp:DropDownList ID="cmbEvent" runat="server" Width="190px"></asp:DropDownList><%end if %></td>
        </tr>
        <%end if %>
        <%If SearchAreaFF = True Or SearchAreaUT = True Then%>
         <tr>
            <td><%If SearchAreaFF = True%><strong>Field Force</strong><%end if %></td>
            <td>
            <%If SearchAreaFF = True%>
                <%--<asp:UpdatePanel ID="UpdatePanelSearch" runat="server" runat="server">
                    <ContentTemplate>--%>
                        <asp:DropDownList ID="cmbFFContext" runat="server" Width="170px"  ></asp:DropDownList>
                    <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
               
            <%end if %>
            </td>
            <td><%If SearchAreaUT = True%><strong>User Type</strong><%end if %>
            </td>
            <td><%If SearchAreaUT = True%><asp:DropDownList ID="cmbDTContext" runat="server" Width="190px"></asp:DropDownList><%end if %></td>
            <td></td>
            <td></td>
         </tr>
         <%end if %>
         <%If SearchAreaUser = True And SearchAreaContent = True Then%>
         <tr>
            <td><%if SearchAreaUser = True then%><strong>ID User</strong><%end if %></td>
            <td><%if SearchAreaUser = True then%><asp:TextBox ID="txtIdUser" Width="80px" runat="server" /><%end if %></td>
            <td><%if SearchAreaContent = True then%><strong>ID Content</strong><%end if %></td>
            <td><%if SearchAreaContent = True then%><asp:TextBox ID="txtIdContent" Width="80px" runat="server" /><%end if %></td>
            <td></td>
            <td></td>
        </tr>
         <%end if %>
         <tr>
            <td><strong>Period</strong></td>
            <td><asp:DropDownList ID="cmbPeriod" runat="server" Width="190px" autopostback="true" OnSelectedIndexChanged="cmbPeriod_SelectedIndexChanged" ></asp:DropDownList></td>
            <td><strong>Date Start</strong></td>
            <td><HP3:Date runat="server" ID="DateRangeStart" ShowDate="false" TypeControl="JsCalendar"/></td>
            <td><strong>Date End</strong></td>  
            <td><HP3:Date runat="server" ID="DateRangeEnd" ShowDate="false" TypeControl="JsCalendar"/></td>
         </tr>
         </table>
        <asp:Button ID="btnStartSearch" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" OnClick="btnStartSearch_Click" />
        </fieldset>
</asp:Panel>
	