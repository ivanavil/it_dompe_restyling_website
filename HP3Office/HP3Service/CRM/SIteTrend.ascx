<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>


<script language="VB" runat="server" >
 
    Dim TYPE_OVERVIEW_SITETREND As String = "trend"
    Dim s_ColumnsInfo As New ArrayList

    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub
    
    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub

    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        
        'valori di selezione di default
        trackSearcher.UseRange = True

        trackSearcher.DateStart = Search.DateUpdate
        trackSearcher.DateRangeStart = Search.DateUpdate
        trackSearcher.DateRangeEnd = New DateTime(Search.DateUpdate.Year - 1, Search.DateUpdate.Month + 1, 1)

        Search.objAll.DataStart = trackSearcher.DateRangeStart
        Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Search.objAll.DateUpd = Search.DateUpdate
        
        ' verifica il data range selezionato
        ' ------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            'trackSearcher.WhereData = False
            'Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.TrendSiteSite = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Read"
        trackSearcher.TypeQuery = TYPE_OVERVIEW_SITETREND
                
        'CALL REGISTERED
        Dim dt As DataTable
        
        trackSearcher.WhereUserType = "Registered"
        cls = crmmanager.ReadCellSet(trackSearcher)
        dt = getDataTable(cls, TYPE_OVERVIEW_SITETREND, trackSearcher.UseRange, "Visits by Registered Users")

        DWRegistered.DataSource = dt
        DWRegistered.DataBind()


        'CALL ANONYMOUS
        trackSearcher.WhereUserType = "Anonymous"
        cls = crmmanager.ReadCellSet(trackSearcher)
        
        dt = getDataTable(cls, TYPE_OVERVIEW_SITETREND, trackSearcher.UseRange, "Visits by Anonymous Users")
        DWAnonymous.DataSource = dt
        DWAnonymous.DataBind()
    End Sub
    
    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean, ByVal UserType As String) As DataTable
        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dcTrend As DataColumn
        Dim dr As DataRow
        'Dim cu As New CommonUtilities()

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' ADD ... modificare il testo e il formato delle colonne in base al tipo 
        ' passato( in realt� devo aggiufngere anche il linguaggio da utilizzare 
        ' e in seguito portare il tutto in un db)
        s_ColumnsInfo.Clear()
        Dim hRes As Integer
        'hRes = GetColumnInfo(s_ColumnsInfo)
        'If (hRes = 0) Then
        '' ADD ERROR BEHAVIOUR
        'End If
        Dim name As String
        Dim m As Member

        ' Inserisce le colonne
        '----------------------------------------------
        'If s_ColumnsInfo.Count > 0 Then
        '    '' visualizza il testo nelle colonne        
        '    Dim columnInfo As [Object]
        '    For Each columnInfo In s_ColumnsInfo
        '        dc = New DataColumn(columnInfo.ToString())
        '        dt.Columns.Add(dc) 'first column
        '    Next columnInfo

        'Else ' default: inserisce il nome colonna standard
        Dim dtit As DataColumn = New DataColumn(UserType)
        'dtit.MaxLength = 10
        'dt.Columns.Add(New DataColumn("Descrizione"))
        dt.Columns.Add(dtit)
        ''get the other columns from axis
        Dim p As Position
        Dim i As Integer = 0
        For Each p In cs.Axes(0).Positions
            dc = New DataColumn
            dcTrend = New DataColumn     ' colonna per il segno 
            name = ""
            'name = GetMonthIndex(Trim(p.Members(0).Caption))
            name = p.Members(0).Caption 'indice del mese (numerico)
            dc.ColumnName = name & "/" & cs(i, 2).FormattedValue
            dcTrend.ColumnName = i.ToString()
            dt.Columns.Add(dc)
            'dt.Columns.Add(dcTrend)
            i = i + 1
        Next
        'End If

        ' Inserisce i valori nella grid
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        Dim addRow As Boolean = True
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)

            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                dr(0) = py.Members(0).Caption
                ' Others Data cells
                Dim x As Integer
                Dim visitCount As Integer = 0
                Dim xpos As Integer = 1

                For x = 0 To cs.Axes(0).Positions.Count - 1
                    dr(xpos) = cs(x, y).FormattedValue '& "-" & cs(x, y + 1).FormattedValue
                    If dr(xpos) <> "" Then visitCount = visitCount + Integer.Parse(dr(xpos))
                    Select Case cs(x, y + 1).FormattedValue
                        Case "0"
                            If dr(xpos) <> "" Then dr(xpos) = "<span style=""float:right;"">" & dr(xpos) & "&nbsp;=</span>"
                        Case "1"
                            If dr(xpos) <> "" Then dr(xpos) = "<span style=""float:right;color:green"">" & dr(xpos) & "&nbsp;&uarr;</span>"
                        Case "-1"
                            If dr(xpos) <> "" Then dr(xpos) = "<span style=""float:right;color:red"">" & dr(xpos) & "&nbsp;&darr;</span>"
                    End Select
                    xpos += 1
                    'dr(xpos) = cs(x, y + 1).FormattedValue
                    'xpos += 1
                    'Dim annoColonna As String = cs(x, y + 2).FormattedValue
                Next
                If visitCount > 0 Then dt.Rows.Add(dr) 'add the row
            End If
            y = y + 3 ' y+1 contiene il trend , y+2 contiene l'anno
            count += 2 ' come prima 
        Next
        Return dt
    End Function

    Function GetMonthIndex(ByVal Month As String) As String
        Dim RetVal As String = ""
        Select Case Month
            Case "gennaio"
                RetVal = "01"
            Case "febbraio"
                RetVal = "02"
            Case "marzo"
                RetVal = "03"
            Case "aprile"
                RetVal = "04"
            Case "maggio"
                RetVal = "05"
            Case "giugno"
                RetVal = "06"
            Case "luglio"
                RetVal = "07"
            Case "agosto"
                RetVal = "08"
            Case "settembre"
                RetVal = "09"
            Case "ottobre"
                RetVal = "10"
            Case "novembre"
                RetVal = "11"
            Case "dicembre"
                RetVal = "12"
            Case Else
                RetVal = ""
        End Select
        Return RetVal
    End Function
    
    Protected Sub RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            For i As Integer = 0 To e.Row.Cells.Count - 1
                e.Row.Cells(i).Text = Server.HtmlDecode(e.Row.Cells(i).Text)
            Next
        End If
    End Sub

</script>

<div class="dMain">
<div>&nbsp;</div>

	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
   
        <asp:GridView ID="DWRegistered" CssClass="tbl1"  
            AlternatingRowStyle-BackColor="#EEEFEF"
            AllowPaging="false"
            OnRowDataBound = "RowDataBound"
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            runat="server"
            emptydatatext="No content available"
            Width="100%">
        <FooterStyle CssClass="gridFooter" />
        <HeaderStyle CssClass="header" />
        <AlternatingRowStyle BackColor="#EEEFEF" />
       </asp:GridView>  
       
       <div>&nbsp;</div>      
       <div>&nbsp;</div>
       
        <asp:GridView ID="DWAnonymous" CssClass="tbl1"
            AlternatingRowStyle-BackColor="#EEEFEF"
            AllowPaging="false"
            OnRowDataBound = "RowDataBound"
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            emptydatatext="No content available"
            Width="100%">
        <FooterStyle CssClass="gridFooter" />
        <HeaderStyle CssClass="header" />
        <AlternatingRowStyle BackColor="#EEEFEF" />
       </asp:GridView>        
        	
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer">
    </div>
</div>
	