<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    Private idSite As String
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim CMB_SELECT_AREA As String = "Select Area"
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = False
        Search.SearchAreaFF = False
        Search.SearchAreaUT = False
        Search.SearchAreaUser = False
        Search.SearchAreaContent = False
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim webRequest As New WebRequestValue()
        Dim nwMan As New NewsletterManager
        Dim nwSearcher As New NewsletterSearcher
        Dim nwColl As New NewsletterCollection
       
        
        If (Not Search.DdlSite Is Nothing) Then
            nwSearcher.KeySite.Id = Search.DdlSite.Value
            idSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                nwSearcher.KeySite.Id = Integer.Parse(webRequest.customParam.item("IdSite"))
                idSite = webRequest.customParam.item("IdSite")
            End If
        End If
        nwColl = nwMan.Read(nwSearcher)
        'nwColl(0).Title
        'nwColl(0).Key.PrimaryKey
        'nwColl(0).TotSended
        'nwColl(0).TotBounced 
       
        GridNewsletter.DataSource = nwColl
        GridNewsletter.DataBind()
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("hh:mm:ss")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function

    Function GetUserDetail(ByVal value As Integer) As String
        If value = 0 Then Return ""
        Return "<img  src=""/HP3Office/HP3Image/Ico/user_detail_inspector.gif"" title=""Detail""/>"
    End Function
    
    Function GetTotal(ByVal id As Integer, ByVal eventId As Integer, Optional ByVal bUnique As Boolean = False) As Integer
        Dim tot As Integer = 0

        Dim WebTrackMan As New WebTrackManager
        Dim webTrackNwlVal As New WebTrackNewsletterValue
        Dim webTrackNwlSearch As New WebTrackNewsletterSearcher
        webTrackNwlSearch.EventId = eventId
        webTrackNwlSearch.NwId = id
        
        
        If Not bUnique Then
            webTrackNwlVal = WebTrackMan.ReadNwLTotalTraceByEvent(webTrackNwlSearch)
        Else
            webTrackNwlVal = WebTrackMan.ReadNwLUniqueTraceByEvent(webTrackNwlSearch)
        End If
        If Not webTrackNwlVal Is Nothing Then
            tot = webTrackNwlVal.Total
        End If
        Return tot
    End Function
    Function GetDate(ByVal id As Integer, ByVal eventId As Integer, Optional ByVal tip As Boolean = False) As String
        Dim res As String = ""
        Dim WebTrackMan As New WebTrackManager
        Dim webTrackNwlColl As New WebTrackNewsletterCollection
        Dim webTrackNwlSearch As New WebTrackNewsletterSearcher
        webTrackNwlSearch.EventId = eventId
        webTrackNwlSearch.NwId = id
        
        webTrackNwlColl = WebTrackMan.ReadNwLDataSend(webTrackNwlSearch)
        If Not webTrackNwlColl Is Nothing Then
            For Each elem As WebTrackNewsletterValue In webTrackNwlColl
                If tip Then
                    res = res & Left(elem.DataSend, 10) & " - "
                Else
                    res = Left(elem.DataSend, 10)
                End If
            Next
        End If
        Return res
    End Function
    Function GetNumber(ByVal numb As String) As String
        Dim res As String = ""
        Try
            If Not IsNumeric(CInt(numb)) Then
                res = "N/A"
            Else
                res = numb
            End If
        Catch ex As Exception
            res = "N/A"
        End Try
        
        Return res
    End Function
</script>
<a style="display:none;" id="ViewSearch" href="javascript:mostra();">View Advanced Filters</a>  <a style="display: none;" id="HiddenSearch" href="javascript:nascondi();">Hidden Advanced Filters</a>
<script type="text/javascript" language="JavaScript"> 
<!-- 
function mostra(){ 

document.getElementById('ctl04_divTesto').style.display=''; 
document.getElementById('ViewSearch').style.display='none'; 
document.getElementById('HiddenSearch').style.display='';} 
function nascondi(){ 
document.getElementById('ctl04_divTesto').style.display='none';
document.getElementById('HiddenSearch').style.display='none';
document.getElementById('ViewSearch').style.display=''} 
//--> 

</script> 
<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
 <asp:Label ID="lblAppUserId" runat="server" Visible="false"></asp:Label>
 <%--<h3>Visit area by User</h3>--%>
 

 <h3>Newsletter</h3>
 <asp:GridView ID="GridNewsletter" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Title - Key" HeaderStyle-Width="15%">
                <ItemTemplate><div style="text-align:left"><%#DataBinder.Eval(Container.DataItem, "Title")%> - <%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%> </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="6%">
                <ItemTemplate>
                <div style="text-align:left">
                    <asp:Label Text='<%#GetDate(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"),1)%>' ToolTip='<%#GetDate(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"),1,"True")%>' ID="lbldate" runat="server"></asp:Label>
                </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Sended" HeaderStyle-Width="9%">
                <ItemTemplate>
                <div style="text-align:center">
                    <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_NwlUser.aspx?DetailEvent=1&SelItem=<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>','','width=800,height=600,scrollbars=yes')">
                    <%#DataBinder.Eval(Container.DataItem, "TotSended")%>
                    </a>                     
                </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Bounced" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "TotBounced")%> </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="UniqueReaders" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <div style="text-align:center">
                    <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_NwlUser.aspx?DetailEvent=2&SelItem=<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>','','width=800,height=600,scrollbars=yes')">
                        <%#GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 2, True)%> 
                    </a>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Total Readership" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 2)%> </div></ItemTemplate>
            </asp:TemplateField>
            <%--(Total Readership/UniqueReaders)--%>
            <asp:TemplateField HeaderText="Average ReaderShip" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center">
                    <%#GetNumber(Math.Round((GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 2) / GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 2, True)), 2))%> 
                </div></ItemTemplate>
            </asp:TemplateField>
            <%--(uniqueReaders/(Total Sended-Bounced))%)--%>
            <asp:TemplateField HeaderText="Unique ReaderShip " HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center">
                    <%#GetNumber(Math.Round(GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 2, True) / (DataBinder.Eval(Container.DataItem, "TotSended") - DataBinder.Eval(Container.DataItem, "TotBounced")), 2) * 100)%>%
                </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Click Through" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#GetTotal(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"), 3)%> </div></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>

    
   
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>