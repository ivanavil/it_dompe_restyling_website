<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import NameSpace="DotNetCharting"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Dim TYPE_YEAR_PAGES As String = "pageviewsyear"
    Dim TYPE_YEAR_PAGESREGISTERED As String = "pageviewsRegisteredyear"
    Dim TYPE_YEAR_PAGESANONYMOUS As String = "pageviewsAnonymousyear"
    
    Dim TYPE_MONTH_PAGES As String = "pageviewsmonth"
    Dim TYPE_MONTH_PAGESREGISTERED As String = "pageviewsRegisteredmonth"
    Dim TYPE_MONTH_PAGESANONYMOUS As String = "pageviewsAnonymousmonth"
    
    Dim TYPE_WEEK_PAGES As String = "pageviewsweek"
    Dim TYPE_WEEK_PAGESREGISTERED As String = "pageviewsRegisteredweek"
    Dim TYPE_WEEK_PAGESANONYMOUS As String = "pageviewsAnonymousweek"
    
    Dim TYPE_DAY_PAGES As String = "pageviewsday"
    Dim TYPE_DAY_PAGESREGISTERED As String = "pageviewsRegisteredday"
    Dim TYPE_DAY_PAGESANONYMOUS As String = "pageviewsAnonymousday"
   
    Dim s_ColumnsInfo As New ArrayList
    
    Dim ws As Chart
    Dim PathBar, PathLine As String
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub

    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub showGraph(ByVal title As String, ByVal titlex As String, ByVal titley As String, ByVal ds As DataSet)
        ws = New Chart
        ws.Title = title
        ws.Type = ChartType.Combo
        ws.DefaultSeries.Type = SeriesType.Bar
        ws.DefaultElement.ShowValue = True
        ws.ShadingEffectMode = ShadingEffectMode.Four
        ws.ChartArea.ClearColors()
        
        ws.ChartArea.XAxis.Label.Text = titlex
        ws.ChartArea.YAxis.Label.Text = titley
        ws.Size = "920x440"
        ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", Color.MediumSlateBlue.ToArgb, "total page views"))
        ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", Color.MediumSpringGreen.ToArgb, "registered", 1))
        ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", Color.Orange.ToArgb, "anonymous", 2))
        ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
        ImageChart.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
        ImageChart.Attributes.Add("usemap", ws.ImageMapName)
        ImageChart.Visible = True
    End Sub
    
    Function getSeries(ByVal src As DataSet, ByVal FieldX As String, ByVal FieldY As String, ByVal argb As Int32, ByVal nameSerie As String, Optional ByVal indexTables As Integer = 0) As SeriesCollection
        If src Is Nothing Then Return Nothing
		 
        Dim SC As New SeriesCollection()
        Dim s As New Series()
        s.Name = nameSerie
	 
        Dim objRow As DataRow
		 
        For Each objRow In src.Tables(indexTables).Rows
            If objRow(FieldY) <> String.Empty Then
                Dim e As New Element()
                e.Name = objRow(FieldX)
                e.YValue = objRow(FieldY)
                s.Elements.Add(e)
            End If
        Next
        SC.Add(s)
        Dim oColor As Color = Color.FromArgb(argb)
		 
        SC(0).DefaultElement.Color = oColor
        SC(0).Line.Width = 4
        SC(0).LegendEntry.Visible = True
        
        Return SC
    End Function
    
    Function RouteDataTable(ByVal dtSource As DataTable, Optional ByVal nameTable As String = "route") As DataTable
        Dim dt As New DataTable(nameTable)
        Dim dc1 As New DataColumn("Name")
        Dim dc2 As New DataColumn("Value")
        dt.Columns.Add(dc1)
        dt.Columns.Add(dc2)
        For i As Integer = 1 To dtSource.Columns.Count - 1
            Dim dr As DataRow = dt.NewRow
            dr("Name") = dtSource.Columns.Item(i).ColumnName
            dr("Value") = dtSource.Rows(0).Item(i)
            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
    
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = False
            Search.objAll.UseRange = True
            trackSearcher.UseRange = True
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.DateRangeStart = trackSearcher.DateRangeEnd.AddDays(-13)
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        Dim dt As DataTable
        Dim chartTitle As String
        Dim ds As DataSet
        ds = New DataSet
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Graph"
        If trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days >= 366 Then
            ' CALL YEAR VISITS ANALYSIS
            trackSearcher.TypeQuery = TYPE_YEAR_PAGES
            cls = crmmanager.ReadCellSet(trackSearcher)

            ' call display cellset total visits
            dt = getDataTable(cls, TYPE_YEAR_PAGES, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd)
            ds = New DataSet
            ds.Tables.Add(RouteDataTable(dt))
        
            ' call display cellset registered
            trackSearcher.TypeQuery = TYPE_YEAR_PAGESREGISTERED
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_YEAR_PAGESREGISTERED, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route1"))
        
            ' call display cellset anonymous
            trackSearcher.TypeQuery = TYPE_YEAR_PAGESANONYMOUS
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_YEAR_PAGESANONYMOUS, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route2"))
        
            chartTitle = "Page Views by Year - " & trackSearcher.DateRangeStart.ToString("yyyy/MM/dd") & "-" & trackSearcher.DateRangeEnd.ToString("yyyy/MM/dd")
            showGraph(chartTitle, "Years", "Page Views", ds)
        End If
        
        If trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days < 366 And trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days >= 90 Then
            ' CALL MONTH VISITS ANALYSIS
            trackSearcher.TypeQuery = TYPE_MONTH_PAGES
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_MONTH_PAGES, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd)

            ds = New DataSet
            ds.Tables.Add(RouteDataTable(dt))
        
            trackSearcher.TypeQuery = TYPE_MONTH_PAGESREGISTERED
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_MONTH_PAGESREGISTERED, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route1"))
        
            trackSearcher.TypeQuery = TYPE_MONTH_PAGESANONYMOUS
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_MONTH_PAGESANONYMOUS, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route2"))
        
            chartTitle = "Page Views by Month - " & trackSearcher.DateRangeStart.ToString("yyyy/MM/dd") & "-" & trackSearcher.DateRangeEnd.ToString("yyyy/MM/dd")
            showGraph(chartTitle, "Months", "Page Views", ds)
        End If
        
        If trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days < 90 And trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days >= 15 Then
            ' CALL WEEK VISITS ANALYSIS
            trackSearcher.WeekRangeStart = DatePart(DateInterval.WeekOfYear, trackSearcher.DateRangeStart)
            trackSearcher.WeekRangeEnd = DatePart(DateInterval.WeekOfYear, trackSearcher.DateRangeEnd)

            ' call display cellset total visits
            trackSearcher.TypeQuery = TYPE_WEEK_PAGES
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_WEEK_PAGES, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd)

            ds = New DataSet
            ds.Tables.Add(RouteDataTable(dt))
        
            trackSearcher.TypeQuery = TYPE_WEEK_PAGESREGISTERED
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_WEEK_PAGESREGISTERED, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route1"))
        
            trackSearcher.TypeQuery = TYPE_WEEK_PAGESANONYMOUS
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_WEEK_PAGESANONYMOUS, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route2"))
        
            chartTitle = "Page Views by Week - " & trackSearcher.DateRangeStart.ToString("yyyy/MM/dd") & "-" & trackSearcher.DateRangeEnd.ToString("yyyy/MM/dd")
            showGraph(chartTitle, "Week", "Page Views", ds)
        End If
        
        If trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days < 15 And trackSearcher.DateRangeEnd.Subtract(trackSearcher.DateRangeStart).Days > 2 Then
            ' CALL DAY VISITS ANALYSIS
            trackSearcher.TypeQuery = TYPE_DAY_PAGES
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_DAY_PAGES, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd)

            ds = New DataSet
            ds.Tables.Add(RouteDataTable(dt))
        
            trackSearcher.TypeQuery = TYPE_DAY_PAGESREGISTERED
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_DAY_PAGESREGISTERED, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route1"))
        
            trackSearcher.TypeQuery = TYPE_DAY_PAGESANONYMOUS
            cls = crmmanager.ReadCellSet(trackSearcher)
            ds.Tables.Add(RouteDataTable(getDataTable(cls, TYPE_DAY_PAGESANONYMOUS, trackSearcher.UseRange, "descrizione", trackSearcher.DateRangeStart, trackSearcher.DateRangeEnd), "route2"))
        
            chartTitle = "Page Views by Day - " & trackSearcher.DateRangeStart.ToString("yyyy/MM/dd") & "-" & trackSearcher.DateRangeEnd.ToString("yyyy/MM/dd")
            showGraph(chartTitle, "Day", "Page Views", ds)
        End If
    End Sub

    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean, ByVal UserType As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As DataTable
        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dcTrend As DataColumn
        Dim dr As DataRow

        s_ColumnsInfo.Clear()
        Dim hRes As Integer

        Dim name, namenew As String
        Dim dtit As DataColumn = New DataColumn(UserType)
        dt.Columns.Add(dtit)

        Dim p As Position
        Dim i As Integer = 0
        Dim y1 As Integer = 0
        Dim yname As String = dateStart.Year.ToString
        Dim nameold As String
        For Each p In cs.Axes(0).Positions
            dc = New DataColumn
            dcTrend = New DataColumn     ' colonna per il segno 
            nameold = name
            name = ""
            'name = GetMonthIndex(Trim(p.Members(0).Caption))
            name = p.Members(0).Caption 'indice del mese (numerico)
            If dt.Columns.Contains(name) Then
                namenew = name + "-2/"
            Else
                namenew = name
            End If
            If gridType = TYPE_MONTH_PAGES Or gridType = TYPE_MONTH_PAGESREGISTERED Or gridType = TYPE_MONTH_PAGESANONYMOUS Then
                If bRange Then
                    If IsNumeric(name) And IsNumeric(nameold) Then
                        If Integer.Parse(name) < Integer.Parse(nameold) Then
                            y1 += 1
                            yname = dateStart.Year + y1
                        End If
                    End If
                End If
                If dt.Columns.Contains(name + "/" + yname) Then
                    namenew = name + "-2/" + yname
                Else
                    namenew = name + "/" + yname
                End If
            End If
            
            If gridType = TYPE_WEEK_PAGES Or gridType = TYPE_WEEK_PAGESREGISTERED Or gridType = TYPE_WEEK_PAGESANONYMOUS Then
                If bRange Then
                    If IsNumeric(name) And IsNumeric(nameold) Then
                        If Integer.Parse(name) < Integer.Parse(nameold) Then
                            y1 += 1
                            yname = dateStart.Year + y1
                        End If
                    End If
                End If
                If dt.Columns.Contains(name + "/" + yname) Then
                    namenew = name + "-2/" + yname
                Else
                    namenew = name + "/" + yname
                End If
            End If
            dc.ColumnName = namenew '& "/" & cs(i, 1).FormattedValue
            dcTrend.ColumnName = i.ToString()
            dt.Columns.Add(dc)
            'dt.Columns.Add(dcTrend)
            i = i + 1
        Next

        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        Dim addRow As Boolean = True
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            addRow = True
            py = cs.Axes(1).Positions(count)

            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                dr(0) = py.Members(0).Caption
                ' Others Data cells
                Dim x As Integer
                Dim visitCount As Integer = 0
                Dim xpos As Integer = 1

                For x = 0 To cs.Axes(0).Positions.Count - 1
                    dr(xpos) = cs(x, y).FormattedValue '& "-" & cs(x, y + 1).FormattedValue
                    xpos += 1
                    'Dim annoColonna As String = cs(x, y + 2).FormattedValue
                Next
                dt.Rows.Add(dr) 'add the row
            End If
            y = y + 3 ' y+1 contiene il trend , y+2 contiene l'anno
            count += 2 ' come prima 
        Next
        Return dt
    End Function
</script>
<div class="dMain">
    <div>&nbsp;</div>
 	<div class="dMiddleBox">
	    <HP3:GenericSearch ID="Search" runat="server" />
        <div>&nbsp;</div>
        <asp:Image id="ImageChart" Visible="false" runat="server"/>
        <div>&nbsp;</div>
    </div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>