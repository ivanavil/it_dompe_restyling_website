<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Content"%> 
<%@ Import Namespace="Healthware.HP3.Core.User"%> 
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 

<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>


<script language="VB" runat="server" >
    
    Dim TYPE_USERS_BY_REGION As String = "usersbyregion"
    Dim TYPE_USERS_BY_COUNTRY As String = "usersbycountry"
    Dim TYPE_USERS_BY_STATE As String = "usersbystate"

    Dim s_ColumnsInfo As New ArrayList

    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub
    
    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            'trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime("2006", 1, 1)
            trackSearcher.DateRangeEnd = Search.DateUpdate
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Users"
        
        Dim dt As DataTable

        'USER BY REGION
        Try
            trackSearcher.TypeQuery = TYPE_USERS_BY_REGION
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_USERS_BY_REGION, trackSearcher.UseRange)
            RPRegion.Visible = False
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                RPRegion.DataSource = dt
                RPRegion.DataBind()
                RPRegion.Visible = True
            End If
        Catch ex As Exception
        End Try
        
        'USERS BY COUNTRY
        Try
            trackSearcher.TypeQuery = TYPE_USERS_BY_COUNTRY
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_USERS_BY_COUNTRY, trackSearcher.UseRange)
            RPCountry.Visible = False
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                RPCountry.DataSource = dt
                RPCountry.DataBind()
                RPCountry.Visible = True
            End If
        Catch ex As Exception
            ' Add error message 
        End Try

        'USERS BY STATE
        Try
            trackSearcher.TypeQuery = TYPE_USERS_BY_STATE
            cls = crmmanager.ReadCellSet(trackSearcher)
            dt = getDataTable(cls, TYPE_USERS_BY_STATE, trackSearcher.UseRange)
            RPState.Visible = False
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                RPState.DataSource = dt
                RPState.DataBind()
                RPState.Visible = True
            End If
        Catch ex As Exception
            ' Add error message 
        End Try
    End Sub
    

    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' ADD ... modificare il testo e il formato delle colonne in base al tipo 
        ' passato( in realt� devo aggiufngere anche il linguaggio da utilizzare 
        ' e in seguito portare il tutto in un db)
        'Dim ColumnInfoArray As New ArrayList
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member
        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
        
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 1
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next
                ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                If (dr(1).ToString().Equals("") Or dr(1).ToString().Equals("0")) Then
                    Exit For
                End If
                
                ' '' aggiunge la colonna idSP (swap tra prima e ultima colonna)
                'Dim idsp As String = dr(0).ToString()
                'dr(x + 1) = dr(0)
                'dr(0) = cs(x, y).FormattedValue
                dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function

    ''' <summary>
    ''' Costruisce l'url da linkare ai contenuti della tabella 
    ''' </summary>
    ''' <param name="Campo"></param>
    ''' <param name="RowIndex">Indice della riga</param>
    ''' <param name="ColumnType">Tipo Colonna</param>
    ''' <param name="TypeGrid"></param>
    ''' <returns>string acontenente URL (se ammesso)</returns>
    ''' <remarks></remarks>
    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, _
                    Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return ""
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then

                Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
                
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                webRequest.customParam.append("IdContent", Search.objAll.idContent) 'Id Content
                webRequest.customParam.append("IdUser", Search.objAll.idUser) 'Id User
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                Dim now As DateTime = DateTime.Now()
                Dim tempDate As DateTime
                
                webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                If Search.objAll.DataStart.HasValue() Then
                    QSDateStart = DateTime.Parse(Search.objAll.DataStart).ToString("dd-MM-yyyy")
                Else
                    QSDateStart = ""
                End If
			    
                If Search.objAll.DateEnd.HasValue() Then
                    QSDateEnd = DateTime.Parse(Search.objAll.DateEnd).ToString("dd-MM-yyyy")
                Else
                    QSDateEnd = ""
                End If
                webRequest.customParam.append("IdContent", RowIndex) 'Id Site
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                webRequest.KeycontentType.Domain = "CRM"
                webRequest.KeycontentType.Name = "userdetail"
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                'If TypeLink Then
                '    strUrl = strHref
                'Else
                strUrl = "<a href='" + strHref + "' title='User Detail'>" & strCampo & "</a>"
                'End If
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
        End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_USERS_BY_REGION) Then
            ColumnInfoArray.Add("+ User by Region")   ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
            ColumnInfoArray.Add("Users")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
            ColumnInfoArray.Add("% Users")        ' sar� sostituito da multilingue.getDictionary(".....")
            ColumnInfoArray.Add("Visits")     ' non visualizzato 
            ColumnInfoArray.Add("% Visits")     ' non visualizzato 
            ColumnInfoArray.Add("Page Views")     ' non visualizzato 
            ColumnInfoArray.Add("% Page Views")     ' non visualizzato 
        End If
        If gridType.Equals(TYPE_USERS_BY_COUNTRY) Then
            ColumnInfoArray.Add("+ User by Country")   ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
            ColumnInfoArray.Add("Users")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
            ColumnInfoArray.Add("% Users")        ' sar� sostituito da multilingue.getDictionary(".....")
            ColumnInfoArray.Add("Visits")     ' non visualizzato 
            ColumnInfoArray.Add("% Visits")     ' non visualizzato 
            ColumnInfoArray.Add("Page Views")     ' non visualizzato 
            ColumnInfoArray.Add("% Page Views")     ' non visualizzato 
        End If
        If gridType.Equals(TYPE_USERS_BY_STATE) Then
            ColumnInfoArray.Add("+ User by State/Province")   ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
            ColumnInfoArray.Add("Users")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
            ColumnInfoArray.Add("% Users")        ' sar� sostituito da multilingue.getDictionary(".....")
            ColumnInfoArray.Add("Visits")     ' non visualizzato 
            ColumnInfoArray.Add("% Visits")     ' non visualizzato 
            ColumnInfoArray.Add("Page Views")     ' non visualizzato 
            ColumnInfoArray.Add("% Page Views")     ' non visualizzato 
        End If
        Return True
    End Function
</script>
<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
    <HP3:GenericSearch ID="Search" runat="server" />
	<div>&nbsp;</div>

    <asp:Repeater id="RPRegion" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="46%" style="text-align:left;"><%#s_ColumnsInfo(0)%></th>
					    <th Width="8%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(2)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(3)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(4)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(5)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(6)%></th>
   					    <th Width="6%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td class="tLeft"><%#Container.DataItem(s_ColumnsInfo(0))%></td>
			    <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(2))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(4))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(5))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(6))%></td>
		        <td></td>
<%--		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, TYPE_AREA_BY_SITE)%><img  src="/HP3Office/HP3Image/Ico/user_detail.gif" title="User Detail" alt="User Detail"/></a><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, "showevents")%><img  src="/HP3Office/HP3Image/Ico/show_events.gif" title="Event Detail" alt="Event Detail"/></a></td>
--%>            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="RPCountry" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="46%" style="text-align:left;"><%#s_ColumnsInfo(0)%></th>
					    <th Width="8%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(2)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(3)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(4)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(5)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(6)%></th>
   					    <th Width="6%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td class="tLeft"><%#Container.DataItem(s_ColumnsInfo(0))%></td>
			    <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(2))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(4))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(5))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(6))%></td>
		        <td></td>
<%--		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, TYPE_AREA_BY_SITE)%><img  src="/HP3Office/HP3Image/Ico/user_detail.gif" title="User Detail" alt="User Detail"/></a><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, "showevents")%><img  src="/HP3Office/HP3Image/Ico/show_events.gif" title="Event Detail" alt="Event Detail"/></a></td>
--%>            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 

    <div>&nbsp;</div>
    <div>&nbsp;</div>
        
    <asp:Repeater id="RPState" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="46%" style="text-align:left;"><%#s_ColumnsInfo(0)%></th>
					    <th Width="8%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(2)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(3)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(4)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(5)%></th>
   					    <th Width="8%"><%#s_ColumnsInfo(6)%></th>
   					    <th Width="6%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td class="tLeft"><%#Container.DataItem(s_ColumnsInfo(0))%></td>
			    <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(2))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(4))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(5))%></td>
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(6))%></td>
		        <td></td>
<%--		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, TYPE_AREA_BY_SITE)%><img  src="/HP3Office/HP3Image/Ico/user_detail.gif" title="User Detail" alt="User Detail"/></a><%#GetUrlUsers(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), TXT_TOT_VIEW, "showevents")%><img  src="/HP3Office/HP3Image/Ico/show_events.gif" title="Event Detail" alt="Event Detail"/></a></td>
--%>            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    </div>  	
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer">
    </div>
</div>