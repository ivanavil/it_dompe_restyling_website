<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    Private idSite As String
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim CMB_SELECT_AREA As String = "Select Area"
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = False
        Search.SearchAreaFF = False
        Search.SearchAreaUT = False
        Search.SearchAreaUser = False
        Search.SearchAreaContent = False
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim webRequest As New WebRequestValue()
        Dim questMan As New QuestionnaireManager
        Dim questSearch As New QuestionnaireSearcher
        Dim questColl As New QuestionnaireCollection
        Dim site As Integer
        If (Not Search.DdlSite Is Nothing) Then
            site = Search.DdlSite.Value
            idSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                site = Integer.Parse(webRequest.customParam.item("IdSite"))
                idSite = webRequest.customParam.item("IdSite")
            End If
        End If
        questSearch.Delete = False
        questColl = questMan.ReadSiteRelation(questSearch, New SiteAreaIdentificator(site))
        GridQuest.DataSource = questColl
        GridQuest.DataBind()
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("hh:mm:ss")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function

    Function GetUserDetail(ByVal value As Integer) As String
        If value = 0 Then Return ""
        Return "<img  src=""/HP3Office/HP3Image/Ico/user_detail_inspector.gif"" title=""Detail""/>"
    End Function
    
    Function GetTotal(ByVal id As Integer) As Integer
        Dim res As Integer = 0
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQstSearch As New WebTrackQSTSearcher
        Dim webTrackQstColl As New WebTrackQSTCollection
        webTrackQstSearch.QuestionnaireId = id
        webTrackQstColl = webTrackMan.ReadQSTVisit(webTrackQstSearch)
        
        If Not webTrackQstColl Is Nothing AndAlso webTrackQstColl.Count > 0 Then
            res = webTrackQstColl.Count.ToString
        End If
        Return res
    End Function
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim appId As String = s.commandargument
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQstSearch As New WebTrackQSTSearcher
        Dim webTrackQstCollEvent As New WebTrackQSTCollection
        Dim webTrackQstCollResult As New WebTrackQSTCollection

        webTrackQstSearch.QuestionnaireId = appId
        webTrackQstCollEvent = webTrackMan.ReadQSTGroupByEvent(webTrackQstSearch)
        webTrackQstCollResult = webTrackMan.ReadQSTGroupByResult(webTrackQstSearch)

        GridDetailsEvent.DataSource = webTrackQstCollEvent
        GridDetailsEvent.DataBind()
        GridDetailsResult.DataSource = webTrackQstCollResult
        GridDetailsResult.DataBind()
        lblDetails.Visible = True
    End Sub
    
    Function MappingResultId(ByVal resultId As Integer) As String
        Dim res As String = ""
        Select Case resultId
            Case 1
                res = "Passed"
            Case 2
                res = "Flunk"
        End Select
        Return res
    End Function
    
    Function MappingEventId(ByVal eventId As Integer) As String
        Dim res As String = ""
        Select Case eventId
            Case 1
                res = "Start"
            Case 2
                res = "Complete"
            Case 3
                res = "Certificate"
        End Select
        Return res
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
 <asp:Label ID="lblAppUserId" runat="server" Visible="false"></asp:Label>
<h3>Questionnaire</h3>
<asp:GridView ID="GridQuest" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Key" HeaderStyle-Width="2%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Key.Id")%> </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title" HeaderStyle-Width="50%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Description")%></div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total Users" HeaderStyle-Width="2%">
                <ItemTemplate>
                    <div style="text-align:center">
                    <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_Quest.aspx?DetailUser=y&SelItem=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','','width=800,height=600,scrollbars=yes')">
                        <%#GetTotal(DataBinder.Eval(Container.DataItem, "Key.Id"))%>
                    </a>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Details" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" Visible="false"  runat="server" ToolTip ="Details" ImageUrl="/HP3Office/HP3Image/Ico/user_detail_inspector.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_Quest.aspx?DetailUser=n&SelItem=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','','width=800,height=600,scrollbars=yes')">Event/Result</a><br />
                        <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_Quest.aspx?SelItem=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','','width=800,height=600,scrollbars=yes')">Question</a>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <br /> 
    <strong>
        <asp:Label ID="lblDetails" runat="server" Text="Event and Result Details" Visible="false"></asp:Label>
    </strong>
   <asp:GridView ID="GridDetailsEvent" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Event Description" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#MappingEventId(DataBinder.Eval(Container.DataItem, "EventId"))%>  </div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "total")%> </div></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <br />
    <asp:GridView ID="GridDetailsResult" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Result Description" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#MappingResultId(DataBinder.Eval(Container.DataItem, "ResulId"))%> </div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "total")%> </div></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>