<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    Private idSite As String
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim CMB_SELECT_AREA As String = "Select Area"
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = False
        Search.SearchAreaFF = False
        Search.SearchAreaUT = False
        Search.SearchAreaUser = False
        Search.SearchAreaContent = False
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim webRequest As New WebRequestValue()
        
        If Not Page.IsPostBack Then
            webRequest.customParam.LoadQueryString()
        End If
        Dim TNodesManager As New TraceNodesManager
        Dim TNodesSearcher As New TraceNodesSearcher
        Dim TNodesColl As New TraceNodesCollection
        Dim Course As New TraceNodesCollection
        Dim TotalColl As New TraceNodesCollection
        Dim TNodesVal As New TraceNodeValue
        
        ' TNodesSearcher.KeyUser.Id = 5
        'TNodesSearcher.KeyCourse.PrimaryKey = 1
        'TNodesSearcher.KeyEvent = 4
        'TNodesColl = TNodesManager.Read(TNodesSearcher)
        'TNodesColl.Sort(TraceNodeGenericComparer.SortType.ByDate, TraceNodeGenericComparer.SortOrder.DESC)
        'Course = TNodesColl.DistinctBy("IdSession")
        '' Example 2 - It returns the nodes tracking according to the session id


        Dim catMana As New CatalogManager
        Dim oCollCat As New CourseCollection
        Dim so As New CatalogSearcher
        
        If (Not Search.DdlSite Is Nothing) Then
            so.KeySite.Id = Search.DdlSite.Value
            idSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                so.KeySite.Id = Integer.Parse(webRequest.customParam.item("IdSite"))
                idSite = webRequest.customParam.item("IdSite")
            End If
        End If
        'so.KeySite.Id = 42
        oCollCat = catMana.ReadCourses(so)
        ' Response.Write(oCollCat.Count)
        
        Dim List As New ListItem
        Dim collList As New ListItemCollection
        For Each elem As CourseValue In oCollCat
            List = New ListItem
            TNodesSearcher.KeyCourse.PrimaryKey = elem.Key.PrimaryKey
            TNodesSearcher.KeyEvent = 4
            
            TNodesColl = TNodesManager.Read(TNodesSearcher)
            TNodesColl.Sort(TraceNodeGenericComparer.SortType.ByDate, TraceNodeGenericComparer.SortOrder.DESC)
            Course = TNodesColl.DistinctBy("IdSession")
            List.Value = Course.Count
            List.Text = elem.Title.Trim & " - " & elem.Key.PrimaryKey
            TotalColl.AddRange(Course)
            collList.Add(List)
            'Response.Write(elem.Key.PrimaryKey & " " & elem.Title & "<br/>")
        Next
        GWEvent.DataSource = TotalColl
        GWEvent.DataBind()
        GridView1.DataSource = collList
        GridView1.DataBind()
 
        LoadUser()
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("hh:mm:ss")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function
    
    Protected Sub GridEvent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWEvent.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
    'Protected Sub GridEvent2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    GridView2.PageIndex = e.NewPageIndex
    '    Esegui()
    'End Sub
    Protected Sub GridUser_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridUser.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
    Protected Sub GridUserDet_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridUserDet.PageIndex = e.NewPageIndex
        AreaTraceByUser(lblAppUserId.Text)
    End Sub
    
    Function GetUserDetail(ByVal value As Integer) As String
        If value = 0 Then Return ""
        Return "<img  src=""/HP3Office/HP3Image/Ico/user_detail_inspector.gif"" title=""Detail""/>"
    End Function
    
     Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        'in base all'id utente che viene passato legge le visite per prodotto o per area
        If s.commandname <> "UserDetail" Then 'visite per prodotto
            Dim appId As String = s.commandargument
            Dim arr As Array
            arr = appId.Split(" - ")
            appId = arr(arr.Length - 1)
            Dim usMan As New UserManager
            Dim soUse As New UserSearcher
            Dim UseColl As New UserCollection
            Dim List As New ListItem
            Dim collList As New ListItemCollection
            Dim catMana As New CatalogManager
            Dim oCollCat As New CourseCollection
            Dim so As New CatalogSearcher
            Dim TNodesManager As New TraceNodesManager
            Dim TNodesSearcher As New TraceNodesSearcher
            Dim TNodesColl As New TraceNodesCollection
            Dim Course As New TraceNodesCollection
            Dim TotalColl As New TraceNodesCollection
            Dim TNodesVal As New TraceNodeValue
            Dim webRequest As New WebRequestValue()
        
            If Not Page.IsPostBack Then
                webRequest.customParam.LoadQueryString()
            End If
            If (Not Search.DdlSite Is Nothing) Then
                so.KeySite.Id = Search.DdlSite.Value
            Else
                If webRequest.customParam.item("IdSite") <> String.Empty Then
                    so.KeySite.Id = Integer.Parse(webRequest.customParam.item("IdSite"))
                End If
            End If
            'so.KeySite.Id = 42
            oCollCat = catMana.ReadCourses(so)
            UseColl = usMan.Read(soUse)
            collList.Clear()
        
            For Each elemUser As UserValue In UseColl
                List = New ListItem
                TNodesSearcher.KeyCourse.PrimaryKey = CInt(appId)
                TNodesSearcher.KeyUser.Id = elemUser.Key.Id
                TNodesSearcher.KeyEvent = 4
                TNodesColl = TNodesManager.Read(TNodesSearcher)
                Course = TNodesColl.DistinctBy("IdSession")
                If Not Course Is Nothing AndAlso Course.Count > 0 Then
                    List.Value = Course.Count
                    List.Text = elemUser.Key.Id
                    collList.Add(List)
                End If
            Next
            GridDetUserProd.DataSource = collList
            GridDetUserProd.DataBind()
        Else 'visite pe 
            Dim idU As Integer = s.commandargument
            lblAppUserId.Text = idU
            AreaTraceByUser(idU)
        End If
    End Sub
    
    Private Sub LoadUser()
        'carica gli utenti in base al sito e area selezionata
        Dim userCollection As New WebTrackUserCollection
        Dim crmManager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()
        Dim webRequest As New WebRequestValue()
        
        If Not Page.IsPostBack Then
            webRequest.customParam.LoadQueryString()
        End If

        ' verifica il data range selezionato
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate.AddDays(1)
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        Else
            If webRequest.customParam.item("DateStart") <> String.Empty And webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            ElseIf webRequest.customParam.item("DateStart") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("Date")).AddDays(1)
            ElseIf webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1)
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            End If
        End If
       
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.IdSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                trackSearcher.IdSite = Integer.Parse(webRequest.customParam.item("IdSite"))
            End If
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.IdArea = Search.DdlArea.Value
        Else
            If webRequest.customParam.item("IdArea") <> String.Empty Then
                trackSearcher.IdArea = Integer.Parse(webRequest.customParam.item("IdArea"))
            End If
        End If
        Try
            userCollection = crmManager.ReadUser(trackSearcher)
            
        Catch ex As Exception
        End Try
        
        GridUser.DataSource = userCollection
        GridUser.DataBind()
    End Sub
    
    Private Sub AreaTraceByUser(ByVal idUser As Integer)
        Dim sqlManager As New SqlConnectionManager
        Dim Connection As SqlConnection = sqlManager.GetDBOLAPConnection
        Dim UserType As Integer = 0
        Dim strQuery As String
        Dim aList As New TraceCollection
        Dim idUserCur As Integer
        Try
            idUserCur = idUser
            lblAppUserId.Text = idUser
            
            Dim cmdGet As New SqlCommand '("", Connection)
            cmdGet.Connection = Connection
            cmdGet.CommandType = CommandType.Text
            
            strQuery = "SELECT COUNT(trace_user_id) AS visit, trace_user_id, SessionIDNum,"
            strQuery = strQuery & " (SELECT sitearea_descrizione FROM PP_SiteArea WHERE sitearea_id=trace_sitearea_id)as description,trace_sitearea_id"
            strQuery = strQuery & " FROM PP_Trace"
            strQuery = strQuery & " WHERE (trace_sitearea_id > 0) AND (trace_user_id = " & idUserCur & ")"
            strQuery = strQuery & " GROUP BY trace_user_id, SessionIDNum, trace_sitearea_id"
            strQuery = strQuery & " ORDER BY trace_sitearea_id"
            cmdGet.CommandText = strQuery

            Connection.Open()
            Dim dr As SqlDataReader = cmdGet.ExecuteReader
                
            If dr.HasRows Then
                Dim Map As New MappingManager
                While dr.Read()
                    Dim vo As New TraceValue
                    'alimenta il valueobject
                    vo = CType(Map.LoadValueObject(vo, dr), TraceValue)
                    vo.Description = dr("description").ToString
                    aList.Add(vo)
                End While
            Else
            End If
        Catch Err As Exception
            Response.Write(Err.ToString)
        Finally
            sqlManager.CloseConnection()
        End Try
        'Return aList
    
        Dim ContaArea As New ListItem
        Dim ContatoreColl As New ListItemCollection
        Dim AreaPrec As String = ""
        Dim contatore As Integer = 0
        Dim area As String = ""
        
        For Each elem As TraceValue In aList
            If AreaPrec <> elem.Description Then
                If AreaPrec <> "" Then
                    ContatoreColl.Add(New ListItem(AreaPrec, contatore))
                End If
                ContaArea = New ListItem
                contatore = 1
                AreaPrec = elem.Description
            Else
                contatore = contatore + 1
            End If
            ContaArea.Value = contatore
            ContaArea.Text = AreaPrec
        Next
        ContatoreColl.Add(New ListItem(AreaPrec, contatore))
        
        GridUserDet.DataSource = ContatoreColl
        GridUserDet.DataBind()
    End Sub
    
    Function GetUserDet(ByVal idUserCur As Integer) As String
        Dim userCollection As New WebTrackUserCollection
        Dim crmManager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()
        Dim res As String = ""
        trackSearcher.IdUser = idUserCur
        Try
            userCollection = crmManager.ReadUser(trackSearcher)
            If Not userCollection Is Nothing AndAlso userCollection.Count > 0 Then
                res = userCollection(0).UserValue.Name & " " & userCollection(0).UserValue.Name
            End If
        Catch ex As Exception
        End Try
        Return res
    End Function
    
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
 <asp:Label ID="lblAppUserId" runat="server" Visible="false"></asp:Label>
 <%--<h3>Visit area by User</h3>--%>
 <asp:GridView ID="GridUserDet" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridUserDet_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Area" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Text")%> </div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Visits" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Value")%> </div></ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "email")%></div></ItemTemplate>
            </asp:TemplateField>--%>
            <%--<asp:TemplateField HeaderText="user/product detail" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Dett" ImageUrl="/HP3Office/HP3Image/Ico/user_detail_inspector.gif" OnClick="OnItemSelected" CommandName ="UserDetail" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        
                    </div>
                </ItemTemplate>
            </asp:TemplateField>--%>
            
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>

 <h3>User</h3>
 <asp:GridView ID="GridUser" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="5"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridUser_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="User" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "UserValue.name")%> <%#DataBinder.Eval(Container.DataItem, "UserValue.surname")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="E-mail" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "UserValue.email")%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="user/product detail" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Area Details" ImageUrl="/HP3Office/HP3Image/Ico/user_detail_inspector.gif" OnClick="OnItemSelected" CommandName ="UserDetail" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "UserValue.Key.ID")%>'/>
<%--                        <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_ProdDetBaxter.aspx?S=<%=idSite%>&SelItem=<%#DataBinder.Eval(Container.DataItem, "text")%>','','width=800,height=600,scrollbars=yes')">section</a>
--%>                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
 
 
 <br />
 <br />
 
    <%--<h3>Visit User / product</h3>--%>
    <asp:GridView ID="GridDetUserProd" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Username - id" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#GetUserDet(DataBinder.Eval(Container.DataItem, "text"))%> - <%#DataBinder.Eval(Container.DataItem, "text") %></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "value")%></div></ItemTemplate>
            </asp:TemplateField>

            <%--<asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %>,'','width=800,height=600,scrollbars=yes')"><%#GetUserDetail(Container.DataItem.KeyUser.Id) %></a></div>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <h3>Visit Product</h3>
    <asp:GridView ID="GridView1" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridEvent_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Product - Key" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "text")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "value")%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="user/product detail" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Users Visit" ImageUrl="/HP3Office/HP3Image/Ico/user_detail_inspector.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "text")%>'/>
                        <a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_ProdDetBaxter.aspx?S=<%=idSite%>&SelItem=<%#DataBinder.Eval(Container.DataItem, "text")%>','','width=800,height=600,scrollbars=yes')">section</a>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %>,'','width=800,height=600,scrollbars=yes')"><%#GetUserDetail(Container.DataItem.KeyUser.Id) %></a></div>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <br />
    <h3>Visit product Details by User</h3>
    <asp:GridView ID="GWEvent" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridEvent_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#GetDateFormat(DataBinder.Eval(Container.DataItem, "DateTrace"))%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Time" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#GetTimeFormat(DataBinder.Eval(Container.DataItem, "DateTrace"))%></div></ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Username - id" HeaderStyle-Width="5%">
                <ItemTemplate><div style="text-align:center"><%#GetUserDet(DataBinder.Eval(Container.DataItem, "KeyUser.Id"))%> - <%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Prodotto" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "KeyCourse.PrimaryKey")%></ItemTemplate>
            </asp:TemplateField>
            
            <%--<asp:TemplateField HeaderText="Event" HeaderStyle-Width="17%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "IdSession")%></ItemTemplate>
            </asp:TemplateField>--%>
            
            
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %>,'','width=800,height=600,scrollbars=yes')"><%#GetUserDetail(Container.DataItem.KeyUser.Id) %></a></div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    
    <br />
    
    <%--<asp:GridView ID="GridView2" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GridEvent2_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Product - User" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "text")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "value")%></div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "KeyUser.Id") %>,'','width=800,height=600,scrollbars=yes')"><%#GetUserDetail(Container.DataItem.KeyUser.Id) %></a></div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>--%>
    
    <br />
    <br />
    
   
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>