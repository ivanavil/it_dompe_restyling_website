<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
   
    Dim TYPE As String = "GroupReferrer"
  
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        'Search.SearchAreaFF = True
        'Search.SearchAreaUT = True
    End Sub
    
    Sub page_prerender()
        Esegui()
    End Sub
        
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackCollection As New WebTrackExtendedCollection
        Dim trackSearcher As New WebTrackSearcher()

        'Browser
        trackSearcher.TypeQuery = TYPE
        trackSearcher.MaxRows = 100
        trackCollection = crmmanager.ReadTrackExtended(trackSearcher)
        If Not trackCollection Is Nothing AndAlso trackCollection.Count > 0 Then
            GWTraffic.DataSource = trackCollection
            GWTraffic.DataBind()
        End If
    End Sub
    
    Protected Sub GWTraffic_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GWTraffic.PageIndex = e.NewPageIndex
        Esegui()
    End Sub
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" Visible="false" runat="server" />
	<div>&nbsp;</div>
	<asp:GridView ID="GWTraffic" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="25"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        OnPageIndexChanging="GWTraffic_PageIndexChanging"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Browser" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Referrer")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Visits" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Total")%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    </div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer" />
</div>
	