<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
    Dim TYPE_UNIQUE_USER As String = "uniqueusers"
    Dim TYPE_VISITS As String = "visits"
    Dim TYPE_PAGES As String = "pages"
    Dim TYPE_WEEK_VISITS As String = "visitsweek"
    Dim TYPE_AREA_BY_SITE As String = "areasbysite"
    Dim TYPE_GETSITE As String = "sites"
    Dim TYPE_ELAPSED As String = "elapsed"
    
    Dim s_ColumnsInfo As New ArrayList
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub

    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = False
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            'Response.Write("id: " & Search.DdlFF.Value & " testo: " & Search.DdlFF.Text)
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        Dim dt As DataTable
        
        ' CALL WEEK VISIT ANALYSIS
        'trackSearcher.StoredName = "SP_HP3_CRM_MDX_ReadPeriodSett"
        'trackSearcher.TypeQuery = TYPE_WEEK_VISITS
        
        'cls = crmmanager.ReadCellSet(trackSearcher)
    
        ' call display cellset 
        'dt = getDataTable2(cls, TYPE_WEEK_VISITS, trackSearcher.UseRange)
        
        'Dim ds As DataSet = RouteDataTable(dt)
        'showGraph("visits by weekday", "weekday", "visits", ds)
        
        'GWWeekVisits.DataSource = dt
        'GWWeekVisits.DataBind()
        
        ' CALL VISIT ANALYSIS & UNIQUE USERS
        Dim dt2 As DataTable
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Read"
        trackSearcher.TypeQuery = TYPE_UNIQUE_USER
        trackSearcher.WhereData = False
        
        cls = crmmanager.ReadCellSet(trackSearcher)
        
        ' call display cellset 
        dt2 = getDataTable(cls, TYPE_UNIQUE_USER, trackSearcher.UseRange)
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Read"
        trackSearcher.TypeQuery = TYPE_VISITS
        trackSearcher.WhereData = False
        
        cls = crmmanager.ReadCellSet(trackSearcher)
        ' Response.Write(crmmanager.ReadMDX(trackSearcher))
        ' call display cellset 
        dt = getDataTable(cls, TYPE_VISITS, trackSearcher.UseRange)

        ' aggiunge i record visits al datatable
        For i As Integer = 0 To dt.Rows.Count - 1
            dt2.ImportRow(dt.Rows(i))
        Next
        
        GWVisits.DataSource = dt2
        GWVisits.DataBind()

        ' CALL PAGES ANALYSIS
        trackSearcher.TypeQuery = TYPE_PAGES

        cls = crmmanager.ReadCellSet(trackSearcher)
        'Response.Write(crmmanager.ReadMDX(trackSearcher))
        trackSearcher.WhereData = False
        
        ' call display cellset 
        dt = getDataTable(cls, TYPE_PAGES, trackSearcher.UseRange)
        
        GWPages.DataSource = dt
        GWPages.DataBind()
        
        ' CALL VISIT ANALYSIS
        ''----------------------------------------
        'Try
        trackSearcher.TypeQuery = TYPE_ELAPSED

        cls = crmmanager.ReadCellSet(trackSearcher)
        'Response.Write(crmmanager.ReadMDX(trackSearcher))
        ' call display cellset 
        dt = getDataTable(cls, TYPE_ELAPSED, trackSearcher.UseRange)
        
        GWPagesElapsed.DataSource = dt
        GWPagesElapsed.DataBind()
        
    End Sub

    ''' <summary>
    ''' COstruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member

        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
        
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = GetRowHeaderLink(y, gridType)
                If (name.Equals("")) Then
                    name = py.Members.Item(0).Caption
                End If
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    Dim val As String = cs(x, y).FormattedValue
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next

                'NEW -- inserisce l'ultima colonna con 'informazione dell'indice di riga -- gestione del RANGE
                If bRange Then
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Else
                    dr(x + 1) = ""
                End If
                
                ' aggiunge la colonna idSP
                dr(x + 2) = y
                
                dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function

    Function GetUrl(ByVal Campo As Object, ByVal RowIndex As Object, ByVal ColumnType As Object, Optional ByVal TypeGrid As String = "") As String
        Dim strCampo As String
        If Campo Is DBNull.Value Then Return 0
        If RowIndex Is DBNull.Value Then
            strCampo = Campo.ToString
            If strCampo.ToString <> "" Then
                Return Campo
            Else
                Return "<strong>ND</strong>"
            End If
        Else
            Dim strHref As String
            Dim strUrl As String
            If Campo = "0" Then Return Campo
            strCampo = Campo.ToString

            If strCampo.ToString <> "" Then

                ' Verifica se il campo � in una riga che deve essere linkata (URL) 
                If Not TypeGrid.Equals("") Then
                    If Not GetLinkedInfo(RowIndex, TypeGrid) Then Return Campo
                Else
                    Return Campo
                End If
            
                'Dim queryString As New QueryString
                Dim webRequest As New WebRequestValue
                
                webRequest.customParam.LoadQueryString()
                webRequest.customParam.Clear()
                webRequest.customParam.append("Type", ColumnType)
                webRequest.customParam.append("Row", RowIndex)
                webRequest.customParam.append("Area", Search.objAll.Area) 'Area
                webRequest.customParam.append("IdArea", Search.objAll.IdArea) 'Id Area
                webRequest.customParam.append("Site", Search.objAll.Site) 'Site
                webRequest.customParam.append("IdSite", Search.objAll.IdSite) 'Id Site
                webRequest.customParam.append("FF", Search.objAll.FF) 'Field Force (Context)
                webRequest.customParam.append("IdFF", Search.objAll.IdFF) 'Field Force (Context)
                webRequest.customParam.append("UT", Search.objAll.UT) 'UserType (Context)
                webRequest.customParam.append("IdUT", Search.objAll.IdUT) 'UserType (Context)
                
                Dim QSDateStart As String = ""
                Dim QSDateEnd As String = ""
                Dim now As DateTime = DateTime.Now()
                Dim tempDate As DateTime
                
                webRequest.customParam.append("Date", DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy"))
                If ColumnType = "Range" Then
                    If Search.objAll.DataStart.HasValue() Then
                        QSDateStart = DateTime.Parse(Search.objAll.DataStart).ToString("dd-MM-yyyy")
                    Else
                        QSDateStart = ""
                    End If
			    
                    If Search.objAll.DateEnd.HasValue() Then
                        QSDateEnd = DateTime.Parse(Search.objAll.DateEnd).ToString("dd-MM-yyyy")
                    Else
                        QSDateEnd = ""
                    End If
                End If
                If ColumnType = "All" Then
                    QSDateStart = ""
                    QSDateEnd = ""
                End If
                If ColumnType = "Anno" Then
                    QSDateStart = New DateTime(Search.DateUpdate.Year, 1, 1).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "mesePrec" Then
                    tempDate = New DateTime(Search.DateUpdate.Year, Search.DateUpdate.Month, 1).Subtract(New TimeSpan(1, 0, 0, 0, 0))
                    QSDateEnd = tempDate.ToString("dd-MM-yyyy")
                    QSDateStart = New DateTime(tempDate.Year, tempDate.Month, 1).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "mese" Then
                    QSDateStart = New DateTime(Search.DateUpdate.Year, Search.DateUpdate.Month, 1).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                If ColumnType = "oggi" Then
                    QSDateStart = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                    QSDateEnd = Search.DateUpdate.ToString("dd-MM-yyyy") 'DateTime.Parse(Search.objAll.DateUpd).ToString("dd-MM-yyyy")
                End If
                webRequest.customParam.append("DateStart", QSDateStart)
                webRequest.customParam.append("DateEnd", QSDateEnd)
                webRequest.KeycontentType.Domain = "CRM"
                webRequest.KeycontentType.Name = "userdetail"
                strHref = Me.BusinessMasterPageManager.FormatRequest(webRequest)
                strUrl = "<a href='" + strHref + "' title='User Detail'>" & strCampo & "</a>"
            Else
                strUrl = "<strong>ND</strong>"
            End If
            Return strUrl
        End If
    End Function
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_VISITS) Or gridType.Equals(TYPE_PAGES) Or gridType.Equals(TYPE_ELAPSED) Or gridType.Equals(TYPE_UNIQUE_USER) Then
            ColumnInfoArray.Add("Description")
            ColumnInfoArray.Add("Last day")     ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("This Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Corrente")
            ColumnInfoArray.Add("Last Month")   ' sar� sostituito da multilingue.getDictionary("CRM_Mese_Precedente")
            ColumnInfoArray.Add("This Year")    ' sar� sostituito da multilingue.getDictionary("CRM_Anno_Corrente")
            ColumnInfoArray.Add("Total")        ' sar� sostituito da multilingue.getDictionary("CRM_Da_On_Line")
            ColumnInfoArray.Add("Time Range")   ' sar� sostituito da multilingue.getDictionary("CRM_Per_Data")
            ColumnInfoArray.Add("idSP")         ' non viene visualizzato 
        End If
        
        If gridType.Equals(TYPE_WEEK_VISITS) Then
            ColumnInfoArray.Add("+ Week Visits")
            ColumnInfoArray.Add("Monday")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Tuesday")       ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Wednesday")     ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Thursday")      ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Friday")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Saturday")      ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Sunday")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
        End If
    End Function
    

    ''' <summary>
    ''' Costruisce il nome della prima riga 
    ''' </summary>
    ''' <param name="rowindex">indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRowHeaderLink(ByVal rowindex As Integer, ByVal TypeGrid As String) As String
        Dim RetVal As String = ""
        If TypeGrid.Equals(TYPE_UNIQUE_USER) Then
            Select Case rowindex
                Case 0
                    RetVal = "Unique registered users" ' sar� sostituito dal dictionary
            End Select
        End If

        If TypeGrid.Equals(TYPE_VISITS) Then
            Select Case rowindex
                Case 0
                    RetVal = "Visits by registered users" ' sar� sostituito dal dictionary
                Case 1
                    RetVal = "Visits by anonymous users"
                Case 2
                    RetVal = "Average visits by registered users"
                Case 3
                    RetVal = "Average visits by anonymous users"
            End Select
        End If
    
        If TypeGrid.Equals(TYPE_PAGES) Then
            Select Case rowindex
                Case 0
                    RetVal = "Page views"  ' sar� sostituito da multilingue.getDictionary("Page views")
                Case 1
                    RetVal = "Page views by registered users"
                Case 2
                    RetVal = "Page views by anonymous users"
                Case 3
                    RetVal = "Average page views per day"
                Case 4
                    RetVal = "Average page views by registered users"
                Case 5
                    RetVal = "Average page views by anonymous users"
            End Select
        End If
        
        If TypeGrid.Equals(TYPE_ELAPSED) Then
            Select Case rowindex
                Case 0
                    RetVal = "Average Duration per day"
                Case 1
                    RetVal = "Average Duration by Registered user"
                Case 2
                    RetVal = "Average Duration Anonymous user"
            End Select
        End If
        Return RetVal
    End Function

    
    
    
    ''' <summary>
    ''' Restituisce True se la riga deve contenere un elemento linkato 
    ''' </summary>
    ''' <param name="rowindex">Indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns>true se la riga deve essere linkata</returns>
    ''' <remarks></remarks>
    Function GetLinkedInfo(ByVal rowindex As String, ByVal TypeGrid As String) As Boolean
        Dim RetVal As Boolean = True
        If TypeGrid.Equals(TYPE_VISITS) Then
            Select Case rowindex
                Case "0"
                    RetVal = True     ' solo la prima riga � linkata
                Case Else
                    RetVal = False
            End Select
        End If

        If TypeGrid.Equals(TYPE_PAGES) Or TypeGrid.Equals(TYPE_ELAPSED) Then RetVal = False ' nessuna riga � linkata
        
        Return RetVal
    End Function
    
    Function GetTime(ByVal value As String) As String
        If value = String.Empty Or value.IndexOf("ND") >= 0 Then Return value
        If Not IsNumeric(value) Then Return "<strong>ND</strong>"
        Return New TimeSpan(0, 0, Decimal.Parse(value)).ToString()
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    
    <asp:Repeater id="GWVisits" runat="Server">
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Visits</th>
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>

	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(1)), Container.DataItem("idSP"), "oggi", "visits")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(2)), Container.DataItem("idSP"), "mese", "visits")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), "mesePrec", "visits")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(4)), Container.DataItem("idSP"), "Anno", "visits")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(5)), Container.DataItem("idSP"), "All", "visits")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.DataItem(s_ColumnsInfo(6)), Container.DataItem("idSP"), "Range", "visits")%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="GWPages" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Pages</th>
   					    <!--<th><%#s_ColumnsInfo(0)%></th>-->
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(1)),Container.dataitem("idSP"),"oggi", "pages")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(2)),Container.dataitem("idSP"),"mese", "pages")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(3)),Container.dataitem("idSP"),"mesePrec", "pages")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(4)),Container.dataitem("idSP"),"Anno", "pages")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(5)),Container.dataitem("idSP"),"All", "pages")%></td>
		        <td style="text-align:center;"><%#GetUrl(Container.dataitem(s_ColumnsInfo(6)),Container.dataitem("idSP"),"Range", "pages")%></td>
    	    </tr>
	    </itemtemplate>
    					
	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
    
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    
    <asp:Repeater id="GWPagesElapsed" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th Width="30%" style="text-align:left;">+ Average Duration</th>
   					    <!--<th><%#s_ColumnsInfo(0)%></th>-->
   					    <th><%#s_ColumnsInfo(1)%></th>
   					    <th><%#s_ColumnsInfo(2)%></th>
   					    <th><%#s_ColumnsInfo(3)%></th>
   					    <th><%#s_ColumnsInfo(4)%></th>
   					    <th><%#s_ColumnsInfo(5)%></th>
   					    <th><%#s_ColumnsInfo(6)%></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(1)), Container.DataItem("idSP"), "oggi", "elapsed"))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(2)), Container.DataItem("idSP"), "mese", "elapsed"))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(3)), Container.DataItem("idSP"), "mesePrec", "elapsed"))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(4)), Container.DataItem("idSP"), "Anno", "elapsed"))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(5)), Container.DataItem("idSP"), "All", "elapsed"))%></td>
		        <td style="text-align:center;"><%#GetTime(GetUrl(Container.DataItem(s_ColumnsInfo(6)), Container.DataItem("idSP"), "Range", "elapsed"))%></td>
    	    </tr>
	    </itemtemplate>
    					
	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater> 
    <div>&nbsp;</div>

</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>