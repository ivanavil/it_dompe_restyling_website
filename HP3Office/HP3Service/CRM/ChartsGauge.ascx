<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import NameSpace="DotNetCharting"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
    Dim ws As Chart
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        'Search.SearchAreaFF = True
        'Search.SearchAreaUT = True
    End Sub

    Sub page_prerender()
        'If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
        '    Esegui()
        'End If
        'If Not Page.IsPostBack Then Esegui()
        showGauge()
        showGauge2()
    End Sub
    
    Sub showGauge()
        ws = New Chart
        ws.Debug = True
        'ws.Title = "title"
        ws.DefaultSeries.Type = SeriesType.BarSegmented
        'ws.DefaultSeries.GaugeType = GaugeType.Horizontal
        ws.YAxis.Orientation = dotnetCHARTING.Orientation.BottomRight
        
        ws.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
        ws.LegendBox.Visible = False
        'ws.Margin = "0"
        'ws.ChartArea.Padding = 2
        ws.ShadingEffectMode = ShadingEffectMode.Three

        ws.ChartArea.ClearColors()
        
        ws.Size = "200x440"
        ' memoria utilizzata dal processo
        Dim memMB As Integer = System.Environment.WorkingSet / (1024 * 1024)
        
        ' memoria disponibile
        Dim memMBAvailable As System.Diagnostics.PerformanceCounter

        ' Put into page load
        memMBAvailable = New System.Diagnostics.PerformanceCounter("Memory", "Available MBytes")
        
        ws.SeriesCollection.Add(New SeriesCollection(New Series("", New Element("RAM HP3 Process (Mb)", memMB))))
        ws.SeriesCollection.Add(New SeriesCollection(New Series("", New Element("RAM Available (Mb)", memMBAvailable.NextValue()))))
        ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
        ImageChart2.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
    End Sub
    
    Sub showGauge2()
        ws = New Chart
        ws.Type = ChartType.Gauges
        ws.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
        ws.LegendBox.Visible = False
        ws.YAxis.SweepAngle = 90
        
        ws.ChartArea.ClearColors()
        ws.Use3D = True
        ws.Size = "300x300"
        
        ws.SeriesCollection.Add(New SeriesCollection(New Series("Active Users", New Element("Active Users", Me.BusinessTraceService.GetOnLineData(TraceService.OnLineDataEnum.ActiveSession, 0, 21)))))

        ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
        ImageChart3.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
    End Sub
</script>
<asp:GridView ID="prova" runat="server" />
<div class="dMain">
    <div>&nbsp;</div>
 	<div class="dMiddleBox">
	    <HP3:GenericSearch ID="Search" runat="server" />
        <div>&nbsp;</div>
        <asp:Image id="ImageChart2" runat="server"/><span style="margin-left:200px;">&nbsp;</span><asp:Image id="ImageChart3" runat="server"/>
        <div>&nbsp;</div>
    </div>
</div>

<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>