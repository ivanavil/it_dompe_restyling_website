<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="System.Text.RegularExpressions"%>
 
<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    Private arrayCount(5) As String
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = False
        Search.SearchAreaFF = False
        Search.SearchAreaUT = False
        Search.SearchAreaUser = False
        Search.SearchAreaContent = True
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub

    Sub Esegui()
        Dim webTrackSearcher As New WebTrackSearcher
        Dim webTrackManager As New WebTrackManager

        'webTrackSearcher.IdUser = userId
        ' webTrackSearcher.MaxRows = maxRows
        
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            webTrackSearcher.DateRangeStart = Search.DateStart.Value
            webTrackSearcher.DateRangeEnd = Search.DateEnd.Value
        End If
        If (Not Search.DdlSite Is Nothing) Then
            webTrackSearcher.IdSite = Search.DdlSite.Value
        End If
        Dim userColl As WebTrackUserCollection = webTrackManager.ReadTrackUserFidelity(webTrackSearcher)
        
       
        Dim conta1 As Integer = 0
        Dim conta2 As Integer = 0
        Dim conta3 As Integer = 0
        Dim conta4 As Integer = 0
        Dim conta5 As Integer = 0
        Dim contaV1 As Integer = 0
        Dim contaV2 As Integer = 0
        Dim contaV3 As Integer = 0
        Dim contaV4 As Integer = 0
        Dim contaV5 As Integer = 0
        'Response.Write(userColl.Count)
        '----- visit duration and visit number per Range -----
        If Not userColl Is Nothing AndAlso userColl.Count > 0 Then
            For Each elem As WebTrackUserValue In userColl
                If elem.AverageTime > 0 AndAlso elem.AverageTime <= 10 Then
                    conta1 = conta1 + 1
                ElseIf elem.AverageTime > 10 AndAlso elem.AverageTime <= 30 Then
                    conta2 = conta2 + 1
                ElseIf elem.AverageTime > 30 AndAlso elem.AverageTime <= 100 Then
                    conta3 = conta3 + 1
                ElseIf elem.AverageTime > 100 AndAlso elem.AverageTime <= 300 Then
                    conta4 = conta4 + 1
                ElseIf elem.AverageTime > 300 Then
                    conta5 = conta5 + 1
                End If
            
                If elem.TotVisits > 0 AndAlso elem.TotVisits <= 10 Then
                    contaV1 = contaV1 + 1
                ElseIf elem.TotVisits > 10 AndAlso elem.TotVisits <= 30 Then
                    contaV2 = contaV2 + 1
                ElseIf elem.TotVisits > 30 AndAlso elem.TotVisits <= 100 Then
                    contaV3 = contaV3 + 1
                ElseIf elem.TotVisits > 100 AndAlso elem.TotVisits <= 300 Then
                    contaV4 = contaV4 + 1
                ElseIf elem.TotVisits > 300 Then
                    contaV5 = contaV5 + 1
                End If
            
            Next
            lblRange1.Text = conta1
            lblRange2.Text = conta2
            lblRange3.Text = conta3
            lblRange4.Text = conta4
            lblRange5.Text = conta5
            lblRangeV1.Text = contaV1
            lblRangeV2.Text = contaV2
            lblRangeV3.Text = contaV3
            lblRangeV4.Text = contaV4
            lblRangeV5.Text = contaV5
        End If
        'GWUser.DataSource = userColl
        'GWUser.DataBind()
        '-----
        '----- page input and page output -----
        If (Not Search.DdlSite Is Nothing) AndAlso Search.DdlSite.Value > 0 AndAlso (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            userColl = New WebTrackUserCollection
            userColl = webTrackManager.ReadTrackUserFidelityVisit(webTrackSearcher)
            If Not userColl Is Nothing AndAlso userColl.Count > 0 Then
                Dim so As New WebTrackSearcher
                Dim man As New WebTrackManager
                Dim oColl As New WebTrackCollection
                Dim contentVal As New ContentValue
                Dim TotalStr As String
                Dim TotalStrOut As String
                
                'mi costruisco la stringa che mi permettera di contare quante volte la tripla idContent,Sitearea,Contenttype si ripete
                Dim i As Integer = 0
                For Each elem As WebTrackUserValue In userColl
                    so.TraceId = elem.TraceEventValue.Key.Id
                    oColl = man.ReadTrack(so) 'pagine di input
                    If Not oColl Is Nothing AndAlso oColl.Count > 0 Then
                        TotalStr = TotalStr & oColl(0).KeyContent.Id & "," & oColl(0).SiteAreaDescription & "," & oColl(0).ContentTypeDescription & "|"
                    End If
                    
                    so.TraceId = CInt(elem.TraceEventValue.Key.Name)
                    oColl = man.ReadTrack(so) 'pagine di output
                    If Not oColl Is Nothing AndAlso oColl.Count > 0 Then
                        TotalStrOut = TotalStrOut & oColl(0).KeyContent.Id & "," & oColl(0).SiteAreaDescription & "," & oColl(0).ContentTypeDescription & "|"
                    End If
                Next
              
                Dim CollMulti As String = ""
                Dim CollMultiApp As String = ""
                Dim Title As String = ""
                Dim Input As String = ""
                Dim arrayTot As Array = TotalStr.Split("|")
                'ciclo la stringa delle pagine input � conto il numero di volte che si ripetono le triple
                For Each elem As String In arrayTot
                    CollMultiApp = elem & ""
                    If CollMulti.IndexOf(CollMultiApp) < 0 Then
                        If elem.Split(",")(0) > 0 Then
                            contentVal = Me.BusinessContentManager.Read(oColl(0).KeyContent.PrimaryKey)
                            Title = contentVal.Title & " -" & elem.Split(",")(1) & " -" & elem.Split(",")(2)
                        Else
                            Title = elem.Split(",")(1) & " -" & elem.Split(",")(2)
                        End If
                        Input = Input & "<tr><td>" & Title & "</td><td>" & Regex.Matches(TotalStr.ToLower(), CollMultiApp.ToLower()).Count & "</td></tr>"
                        Title = ""
                        CollMulti = CollMulti & CollMultiApp
                        TotalStr = TotalStr.Replace(CollMultiApp, "")
                    End If
                Next
                
                CollMulti = ""
                CollMultiApp = ""
                Title = ""
                Dim Output As String = ""
                arrayTot = TotalStrOut.Split("|")
                'ciclo la stringa delle pagine output � conto il numero di volte che si ripetono le triple
                For Each elem As String In arrayTot
                    CollMultiApp = elem & ""
                    If CollMulti.IndexOf(CollMultiApp) < 0 Then
                        If elem.Split(",")(0) > 0 Then
                            contentVal = Me.BusinessContentManager.Read(oColl(0).KeyContent.PrimaryKey)
                            Title = contentVal.Title & " -" & elem.Split(",")(1) & " -" & elem.Split(",")(2)
                        Else
                            Title = elem.Split(",")(1) & " -" & elem.Split(",")(2)
                        End If
                        Output = Output & "<tr><td>" & Title & "</td><td>" & Regex.Matches(TotalStrOut.ToLower(), CollMultiApp.ToLower()).Count & "</td></tr>"
                        Title = ""
                        CollMulti = CollMulti & CollMultiApp
                        TotalStrOut = TotalStrOut.Replace(CollMultiApp, "")
                    End If
                Next
                lblOutputPages.Text = Output
                lblInputPages.Text = Input
            End If
        End If
        '-----
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
    
    To see input and output pages select a site and a period.
    <table cellpadding="0" cellspacing="0" class="tbl1">
        <thead>	
            <tr>
	            <th style="width:5%">Description</th>
	            <th style="width:25%">Input pages</th>
	        </tr>
	    </thead>
		    <asp:Label ID="lblInputPages" runat="server"></asp:Label>
    </table>
    <table cellpadding="0" cellspacing="0" class="tbl1">
        <thead>	
            <tr>
	            <th style="width:5%">Description</th>
	            <th style="width:25%">Output pages</th>
	        </tr>
	    </thead>
		    <asp:Label ID="lblOutputPages" runat="server"></asp:Label>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" class="tbl1">
        <thead>	
            <tr>
	            <th style="width:5%">Visit Duration (second)</th>
	            <th style="width:25%">Total User</th>
	        </tr>
	    </thead>
	        <tr>
			    <td style="text-align:center;"><strong>0 - 10</strong></td>
		        <td><asp:Label ID="lblRange1" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>11 - 30</strong></td>
		        <td><asp:Label ID="lblRange2" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>31 - 100</strong></td>
		        <td><asp:Label ID="lblRange3" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>101 - 300</strong></td>
		        <td><asp:Label ID="lblRange4" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"> <strong>>301</strong> </td>
		        <td><asp:Label ID="lblRange5" runat="server"></asp:Label></td>
            </tr>
      
    </table>
    
    <table cellpadding="0" cellspacing="0" class="tbl1">
        <thead>	
            <tr>
	            <th style="width:5%">Visit Number</th>
	            <th style="width:25%">Total User</th>
	        </tr>
	    </thead>
	        <tr>
			    <td style="text-align:center;"><strong>0 - 10</strong></td>
		        <td><asp:Label ID="lblRangeV1" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>11 - 30</strong></td>
		        <td><asp:Label ID="lblRangeV2" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>31 - 100</strong></td>
		        <td><asp:Label ID="lblRangeV3" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"><strong>101 - 300</strong></td>
		        <td><asp:Label ID="lblRangeV4" runat="server"></asp:Label></td>
            </tr>
            <tr>
			    <td style="text-align:center;"> <strong>>301</strong> </td>
		        <td><asp:Label ID="lblRangeV5" runat="server"></asp:Label></td>
            </tr>
      
    </table>
        <asp:Repeater id="GWUser" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th style="width:5%">ID</th>
   					    <th style="width:25%">User Name</th>
   					    <th style="width:35%">Email</th>
   					    <th style="width:15%">Last Date Visit</th>
   					    <th style="width:10%">Total Visits</th>
   					    <th style="width:10%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id")%></td>
		        <td><%#DataBinder.Eval(Container.DataItem, "UserValue.CompleteName").ToLower%></td>
		        <td><%#GetEmail(DataBinder.Eval(Container.DataItem, "UserValue.Email"))%></td>
		        <td style="text-align:center;"><%#GetDateFormat(DataBinder.Eval(Container.DataItem, "DateLastVisit"))%></td>
		        <td style="text-align:center;"><%#DataBinder.Eval(Container.DataItem, "TotVisits")%></td>
		        <td style="text-align:center;"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id") %>,'','width=800,height=600,scrollbars=yes')"><img  src='/HP3Office/HP3Image/Ico/user_detail_inspector.gif' title='Detail' alt=""/></a></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>