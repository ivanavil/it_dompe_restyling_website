<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
        Search.SearchAreaUser = True
        Search.SearchAreaContent = True
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub Esegui()
        Dim crmManager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()
        Dim webRequest As New WebRequestValue()
        
        If Not Page.IsPostBack Then
            webRequest.customParam.LoadQueryString()
        End If

        ' verifica il data range selezionato
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate.AddDays(1)
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        Else
            If webRequest.customParam.item("DateStart") <> String.Empty And webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            ElseIf webRequest.customParam.item("DateStart") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("Date")).AddDays(1)
            ElseIf webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1)
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            End If
        End If
       
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.IdSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                trackSearcher.IdSite = Integer.Parse(webRequest.customParam.item("IdSite"))
            End If
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.IdArea = Search.DdlArea.Value
        Else
            If webRequest.customParam.item("IdArea") <> String.Empty Then
                trackSearcher.IdArea = Integer.Parse(webRequest.customParam.item("IdArea"))
            End If
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.IdFF = Search.DdlFF.Value
        Else
            If webRequest.customParam.item("IdFF") <> String.Empty Then
                trackSearcher.IdFF = Integer.Parse(webRequest.customParam.item("IdFF"))
            End If
        End If
       
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.IdUT = Search.DdlUserType.Value
        Else
            If webRequest.customParam.item("IdUT") <> String.Empty Then
                trackSearcher.IdUT = Integer.Parse(webRequest.customParam.item("IdUT"))
            End If
        End If
        
        If (Not Search.DdlEvent Is Nothing) Then
            trackSearcher.IdEvent = Search.DdlEvent.Value
        Else
            If webRequest.customParam.item("IdEvent") <> String.Empty Then
                trackSearcher.IdEvent = Integer.Parse(webRequest.customParam.item("IdEvent"))
            End If
        End If
    
        If (Not Search.IdUser Is Nothing AndAlso Search.IdUser <> String.Empty) Then
            trackSearcher.IdUser = Integer.Parse(Search.IdUser)
        Else
            If webRequest.customParam.item("IdUser") <> String.Empty Then
                trackSearcher.IdUser = Integer.Parse(webRequest.customParam.item("IdUser"))
            End If
        End If

        If (Not Search.IdContent Is Nothing AndAlso Search.IdContent <> String.Empty) Then
            trackSearcher.IdContent = Integer.Parse(Search.IdContent)
        Else
            If webRequest.customParam.item("IdContent") <> String.Empty Then
                trackSearcher.IdContent = Integer.Parse(webRequest.customParam.item("IdContent"))
            End If
        End If

        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        trackSearcher.MaxRows = 500

        'trackSearcher.TypeQuery = "ReadUser"
        Dim userCollection As New WebTrackUserCollection
        Try
            userCollection = crmManager.ReadUser(trackSearcher)
        Catch ex As Exception
        End Try
        GWUser.DataSource = userCollection
        GWUser.DataBind()
        msg.Text = String.Empty
        If GWUser.Items.Count > 0 Then msg.Text = "Results (Total <strong>" & GWUser.Items.Count & "</strong>)"
        If GWUser.Items.Count = trackSearcher.MaxRows Then msg.Text += " Research has produced more than " & trackSearcher.MaxRows.ToString & " results"
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
    
        <asp:Repeater id="GWUser" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th style="width:5%">ID</th>
   					    <th style="width:25%">User Name</th>
   					    <th style="width:35%">Email</th>
   					    <th style="width:15%">Last Date Visit</th>
   					    <th style="width:10%">Total Visits</th>
   					    <th style="width:10%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id")%></td>
		        <td><%#DataBinder.Eval(Container.DataItem, "UserValue.CompleteName").ToLower%></td>
		        <td><%#GetEmail(DataBinder.Eval(Container.DataItem, "UserValue.Email"))%></td>
		        <td style="text-align:center;"><%#GetDateFormat(DataBinder.Eval(Container.DataItem, "DateLastVisit"))%></td>
		        <td style="text-align:center;"><%#DataBinder.Eval(Container.DataItem, "TotVisits")%></td>
		        <td style="text-align:center;"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id") %>,'','width=800,height=600,scrollbars=yes')"><img  src='/HP3Office/HP3Image/Ico/user_detail_inspector.gif' title='Detail' alt=""/></a></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>