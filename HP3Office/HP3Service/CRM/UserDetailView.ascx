<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    Private strDomain As String
    Dim VISIT_USERS As String = "VisitTimePageByUser"
    Dim s_ColumnsInfo As New ArrayList
    
    Sub page_init()
        Search.SearchAreaSite = True
        Search.SearchAreaEvent = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
        Search.SearchAreaUser = True
        Search.SearchAreaContent = True
    End Sub

    Sub page_prerender()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
            UserDet()
        End If
        If Not Page.IsPostBack Then
            Esegui()
            UserDet()
        End If
    End Sub
    
    Sub Esegui()
        Dim crmManager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()
        Dim webRequest As New WebRequestValue()
        
        If Not Page.IsPostBack Then
            webRequest.customParam.LoadQueryString()
        End If

        ' verifica il data range selezionato
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate.AddDays(1)
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            trackSearcher.DateRangeEnd = Search.DateEnd.Value.AddDays(1)
        Else
            If webRequest.customParam.item("DateStart") <> String.Empty And webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            ElseIf webRequest.customParam.item("DateStart") <> String.Empty Then
                trackSearcher.DateRangeStart = DateTime.Parse(webRequest.customParam.item("DateStart"))
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("Date")).AddDays(1)
            ElseIf webRequest.customParam.item("DateEnd") <> String.Empty Then
                trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1)
                trackSearcher.DateRangeEnd = DateTime.Parse(webRequest.customParam.item("DateEnd")).AddDays(1)
            End If
        End If
       
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.IdSite = Search.DdlSite.Value
        Else
            If webRequest.customParam.item("IdSite") <> String.Empty Then
                trackSearcher.IdSite = Integer.Parse(webRequest.customParam.item("IdSite"))
            End If
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.IdArea = Search.DdlArea.Value
        Else
            If webRequest.customParam.item("IdArea") <> String.Empty Then
                trackSearcher.IdArea = Integer.Parse(webRequest.customParam.item("IdArea"))
            End If
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.IdFF = Search.DdlFF.Value
        Else
            If webRequest.customParam.item("IdFF") <> String.Empty Then
                trackSearcher.IdFF = Integer.Parse(webRequest.customParam.item("IdFF"))
            End If
        End If
       
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.IdUT = Search.DdlUserType.Value
        Else
            If webRequest.customParam.item("IdUT") <> String.Empty Then
                trackSearcher.IdUT = Integer.Parse(webRequest.customParam.item("IdUT"))
            End If
        End If
        
        If (Not Search.DdlEvent Is Nothing) Then
            trackSearcher.IdEvent = Search.DdlEvent.Value
        Else
            If webRequest.customParam.item("IdEvent") <> String.Empty Then
                trackSearcher.IdEvent = Integer.Parse(webRequest.customParam.item("IdEvent"))
            End If
        End If
    
        If (Not Search.IdUser Is Nothing AndAlso Search.IdUser <> String.Empty) Then
            trackSearcher.IdUser = Integer.Parse(Search.IdUser)
        Else
            If webRequest.customParam.item("IdUser") <> String.Empty Then
                trackSearcher.IdUser = Integer.Parse(webRequest.customParam.item("IdUser"))
            End If
        End If

        If (Not Search.IdContent Is Nothing AndAlso Search.IdContent <> String.Empty) Then
            trackSearcher.IdContent = Integer.Parse(Search.IdContent)
        Else
            If webRequest.customParam.item("IdContent") <> String.Empty Then
                trackSearcher.IdContent = Integer.Parse(webRequest.customParam.item("IdContent"))
            End If
        End If

        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        trackSearcher.MaxRows = 500

        'trackSearcher.TypeQuery = "ReadUser"
        Dim userCollection As New WebTrackUserCollection
        Try
            userCollection = crmManager.ReadUser(trackSearcher)
        Catch ex As Exception
        End Try
        GWUser.DataSource = userCollection
        GWUser.DataBind()
        msg.Text = String.Empty
        If GWUser.Items.Count > 0 Then msg.Text = "Results (Total <strong>" & GWUser.Items.Count & "</strong>)"
        If GWUser.Items.Count = trackSearcher.MaxRows Then msg.Text += " Research has produced more than " & trackSearcher.MaxRows.ToString & " results"
    End Sub
    
    Function GetEmail(ByVal value As MultiFieldType) As String
        If value Is Nothing Then Return String.Empty
        Return value.Item(0).ToLower
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function getId(ByVal WebTrackUserValue As WebTrackUserValue) As String
        Return WebTrackUserValue.UserValue.Key.Id
    End Function
    
    
    Sub UserDet()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet

        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            'trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            'trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            'Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime("2006", 1, 1)
            trackSearcher.DateRangeEnd = Search.DateUpdate
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            'Response.Write("test")
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        'If (Not Search.DdlArea Is Nothing) Then
        '    trackSearcher.WhereAreaCondition = Search.DdlArea.Text
        '    Search.objAll.Area = Search.DdlArea.Text
        '    Search.objAll.IdArea = Search.DdlArea.Value
        'End If

        'verifica se il filtro FF (Field Force) � selezionato
        'If (Not Search.DdlFF Is Nothing) Then
        '    trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
        '    Search.objAll.FF = Search.DdlFF.Text
        '    Search.objAll.IdFF = Search.DdlFF.Value
        'End If
        
        'If (Not Search.DdlUserType Is Nothing) Then
        '    trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
        '    Search.objAll.UT = Search.DdlUserType.Text
        '    Search.objAll.IdUT = Search.DdlUserType.Value
        'End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_Users"
        trackSearcher.TypeQuery = VISIT_USERS
       
        'CALL REGISTERED
        ' trackSearcher.WhereUserType = "Registered"
        Try
            cls = crmmanager.ReadCellSet(trackSearcher)
            '   Response.Write(crmmanager.ReadMDX(trackSearcher))
            Dim dt As DataTable
            dt = getDataTable(cls, VISIT_USERS, trackSearcher.UseRange)
            'Response.Write(dt.Rows.Count)
            rptUserDet.DataSource = dt
            rptUserDet.DataBind()
            
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub
    ''' <summary>
    ''' Costruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) _
                                  As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        ' ADD ... modificare il testo e il formato delle colonne in base al tipo 
        ' passato( in realt� devo aggiufngere anche il linguaggio da utilizzare 
        ' e in seguito portare il tutto in un db)
        'Dim ColumnInfoArray As New ArrayList
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member
        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                
                dc.ColumnName = name ' + i.ToString()
                dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If
        
        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0
        Dim addRow As Boolean = True

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            
            addRow = True
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = py.Members.Item(0).Caption
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 2
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next

                ' WORKAROUND per non visualizzare le righe vuote (brutto ma efficace)
                'If (dr("User").ToString().Equals("") Or dr("User").ToString().Equals("0")) Then
                '    Exit For
                'End If
                If dr(0).ToString.Equals("0") Or dr(1).ToString().Equals("") Or dr(1).ToString.Equals("0") Then
                    addRow = False
                End If
                ' '' aggiunge la colonna idSP (swap tra prima e ultima colonna)
                'Dim idsp As String = dr(0).ToString()
                dr(x + 1) = dr(0)
                dr(0) = cs(x, y).FormattedValue
                
                'If Search.objAll.Site <> String.Empty Then
                'If dr(0) <> Search.objAll.Site Then addRow = False
                'End If
                
                If addRow Then dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
            
        Next
        'Response.Write(dt.Rows.Count)
        Return dt
    End Function
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(VISIT_USERS) Then
            
            ColumnInfoArray.Add("Page")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("...."))    
            ColumnInfoArray.Add("Visit")     ' sar� sostituito da multilingue.getDictionary((multilingue.getDictionary("....."))
            ColumnInfoArray.Add("Time")        ' sar� sostituito da multilingue.getDictionary(".....")
            ColumnInfoArray.Add("User")     ' non visualizzato 
            ColumnInfoArray.Add("UserName")     ' non visualizzato 
       
        End If
    End Function
    
    Function GetTime(ByVal tempo As String) As String
        Dim aTemp As String = ""
        
        aTemp = New TimeSpan(0, 0, Long.Parse(tempo.Replace(",", ""))).ToString
        Return aTemp
    End Function
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" runat="server" />
    <div>&nbsp;</div>
    <asp:Literal ID="msg" runat="server" />
    <div>&nbsp;</div>
    
        <asp:Repeater id="GWUser" runat="Server" >
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
					    <th style="width:5%">ID</th>
   					    <th style="width:25%">User Name</th>
   					    <th style="width:35%">Email</th>
   					    <th style="width:15%">Last Date Visit</th>
   					    <th style="width:10%">Total Visits</th>
   					    <th style="width:10%"></th>
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
			    <td><%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id")%></td>
		        <td><%#DataBinder.Eval(Container.DataItem, "UserValue.CompleteName").ToLower%></td>
		        <td><%#GetEmail(DataBinder.Eval(Container.DataItem, "UserValue.Email"))%></td>
		        <td style="text-align:center;"><%#GetDateFormat(DataBinder.Eval(Container.DataItem, "DateLastVisit"))%></td>
		        <td style="text-align:center;"><%#DataBinder.Eval(Container.DataItem, "TotVisits")%></td>
		        <td style="text-align:center;"><a href="#" onclick="window.open('<%=strDomain%>/Popups/popcrm_UserDetail.aspx?SelItem='+<%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id") %>,'','width=800,height=600,scrollbars=yes')"><img  src='/HP3Office/HP3Image/Ico/user_detail_inspector.gif' title='Detail' alt=""/></a></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    
    <br /><br /><br />
    
   
    <asp:Repeater id="rptUserDet" runat="Server">
	    <headertemplate>
		    <table cellpadding="0" cellspacing="0" class="tbl1">
			    <thead>				
				    <tr>
				        <th Width="5%">Id</th>
				        <th Width="70%">User Name</th>
					    <th Width="10%" style="text-align:left;"><%#s_ColumnsInfo(0)%></th>
   					    <th Width="5%"><%#s_ColumnsInfo(1)%></th>
   					    <th Width="10%"><%#s_ColumnsInfo(2)%></th>
   					    
    				</tr>
			    </thead>
	    </headertemplate>
    						
	    <itemtemplate>
		    <tr>
		        <td><%#Container.DataItem(s_ColumnsInfo(4))%></td>
		        <td><%#Container.dataitem(s_ColumnsInfo(0))%></td>
		        <td><%#Container.DataItem(s_ColumnsInfo(3))%></td>
			    
		        <td style="text-align:center;"><%#Container.DataItem(s_ColumnsInfo(1))%></td>
		        <td style="text-align:center;"><%#GetTime(Container.DataItem(s_ColumnsInfo(2)))%></td>
            </tr>
	    </itemtemplate>
  	    <footertemplate>
		    </table>
	    </footertemplate>
    </asp:Repeater>
    
    
</div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>