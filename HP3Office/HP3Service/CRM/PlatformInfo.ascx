<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import NameSpace="DotNetCharting"%>

<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
   
    Dim TYPE As String = "GroupPlatform"
   
    Dim s_ColumnsInfo As New ArrayList
    Dim ws As Chart
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        'Search.SearchAreaFF = True
        'Search.SearchAreaUT = True
    End Sub
    
    Sub page_prerender()
        Esegui()
    End Sub
        
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackCollection As New WebTrackExtendedCollection
        Dim trackSearcher As New WebTrackSearcher()

        'Browser
        trackSearcher.TypeQuery = TYPE
        'trackSearcher.MaxRows = 10
        trackCollection = crmmanager.ReadTrackExtended(trackSearcher)
        If Not trackCollection Is Nothing AndAlso trackCollection.Count > 0 Then
            GWBrowser.DataSource = trackCollection
            GWBrowser.DataBind()
            
            showGraph("Platform", "Platform", "Total", CollectionToDataSet(trackCollection))
        End If
    End Sub
    
    Function CollectionToDataSet(ByVal value As WebTrackExtendedCollection) As DataSet
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable("route")
            ds.Tables.Add(dt)
            Dim dc1 As New DataColumn("Name")
            Dim dc2 As New DataColumn("Value")
            dt.Columns.Add(dc1)
            dt.Columns.Add(dc2)
            For i As Integer = 0 To value.Count - 1
                Dim dr As DataRow = dt.NewRow
                dr("Name") = value(i).Platform
                dr("Value") = value(i).Total
                dt.Rows.Add(dr)
            Next
            Return ds
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    Function getSeries(ByVal src As DataSet, ByVal FieldX As String, ByVal FieldY As String, ByVal argb As Int32, ByVal nameSerie As String, Optional ByVal indexTables As Integer = 0) As SeriesCollection
        If src Is Nothing Then Return Nothing
        Dim SC As New SeriesCollection()
        Dim s As Series
        Dim objRow As DataRow
	 
        For Each objRow In src.Tables(indexTables).Rows
            s = New Series
            If objRow(FieldY) <> String.Empty Then
                Dim e As New Element()
                'e.Name = 
                e.YValue = objRow(FieldY)
                s.Elements.Add(e)
                s.Name = objRow(FieldX)
                SC.Add(s)
            End If
        Next
        
        Dim oColor As Color = Color.FromArgb(argb)
		 
        
        Return SC
    End Function
    
    Sub showGraph(ByVal title As String, ByVal titlex As String, ByVal titley As String, ByVal ds As DataSet)
        Try
            ws = New Chart
            ws.Title = title
            ws.Type = ChartType.Pie
            ws.DefaultElement.ShowValue = True
            ws.DefaultElement.ExplodePieSlice = True
            ws.PieLabelMode = PieLabelMode.Outside
            ws.DefaultElement.Transparency = 20
            ws.ChartArea.ClearColors()
            ws.YAxis.Scale = Scale.Normal
            ws.Use3D = True
            ws.Size = "560x480"
            ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", 0, ""))
            ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
            ImageChart.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
        Catch ex As Exception
        End Try
    End Sub
</script>

<div class="dMain">
<div>&nbsp;</div>
 
	<div class="dMiddleBox">
	<HP3:GenericSearch ID="Search" Visible="false" runat="server" />
	<div>&nbsp;</div>
    <div style="float:right;"><asp:Image id="ImageChart" runat="server"/></div>
    <div style="float:left;">
	<asp:GridView ID="GWBrowser" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="+ Platform" HeaderStyle-Width="65%" ItemStyle-Width="65%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Platform")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Visits" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "Total")%></div></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    </div>
</div>
<div class="dFooterBoxesContainer">	
	<div class="clearer" />
</div>
	