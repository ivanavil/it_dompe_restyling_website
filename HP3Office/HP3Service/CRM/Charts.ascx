<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.Persistent"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack"%> 
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<%@ Import Namespace="Microsoft.AnalysisServices.AdomdClient" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import NameSpace="DotNetCharting"%>

<%@ Register TagPrefix="HP3" TagName="GenericSearch" Src="~/hp3Office/HP3Service/CRM/GenericSearch.ascx" %>

<script language="VB" runat="server" >
    
    Dim TYPE_WEEK_VISITS As String = "visitsweek"
   
    Dim s_ColumnsInfo As New ArrayList
    
    Dim ws As Chart
    
    Sub page_init()
        'rende visibili le aree del box di ricerca
        Search.SearchAreaSite = True
        Search.SearchAreaFF = True
        Search.SearchAreaUT = True
    End Sub

    Sub page_prerender()
        If Not Request.Form("__EventTarget") Is Nothing AndAlso (Request.Form("__EventTarget").IndexOf("cmbPeriod") < 0 And Request.Form("__EventTarget").IndexOf("cmbSite")) < 0 Then
            Esegui()
        End If
        If Not Page.IsPostBack Then Esegui()
    End Sub
    
    Sub showGraph(ByVal title As String, ByVal titlex As String, ByVal titley As String, ByVal ds As DataSet)
        ws = New Chart
        ws.Title = title
        ws.Type = ChartType.Combo
        ws.DefaultSeries.Type = SeriesType.Bar
        ws.DefaultElement.ShowValue = True
        ws.ShadingEffectMode = ShadingEffectMode.Four
        ws.ChartArea.ClearColors()
        
        ws.ChartArea.XAxis.Label.Text = titlex
        ws.ChartArea.YAxis.Label.Text = titley
        ws.Size = "920x440"
        ws.SeriesCollection.Add(getSeries(ds, "Name", "Value", Color.MediumSlateBlue.ToArgb, "total visits"))
        ws.TempDirectory = "/hp3office/" & ConfigurationsManager.ImagePathDefault & "/"
        ImageChart.ImageUrl = ws.FileManager.SaveImage(ws.GetChartBitmap())
        ImageChart.Attributes.Add("usemap", ws.ImageMapName)
    End Sub
    
    Function getSeries(ByVal src As DataSet, ByVal FieldX As String, ByVal FieldY As String, ByVal argb As Int32, ByVal nameSerie As String, Optional ByVal indexTables As Integer = 0) As SeriesCollection
        If src Is Nothing Then Return Nothing
		 
        Dim SC As New SeriesCollection()
        Dim s As New Series()
        s.Name = nameSerie
	 
        Dim objRow As DataRow
		 
        For Each objRow In src.Tables(indexTables).Rows
            If objRow(FieldY) <> String.Empty Then
                Dim e As New Element()
                e.Name = objRow(FieldX)
                e.YValue = objRow(FieldY)
                s.Elements.Add(e)
            End If
        Next
        SC.Add(s)
        Dim oColor As Color = Color.FromArgb(argb)
		 
        SC(0).DefaultElement.Color = oColor
        SC(0).Line.Width = 4
        SC(0).LegendEntry.Visible = True
        
        Return SC
    End Function
    
    Function RouteDataTable(ByVal dtSource As DataTable, Optional ByVal nameTable As String = "route") As DataTable
        'Dim ds As New DataSet
        Dim dt As New DataTable(nameTable)
        'ds.Tables.Add(dt)
        Dim dc1 As New DataColumn("Name")
        Dim dc2 As New DataColumn("Value")
        dt.Columns.Add(dc1)
        dt.Columns.Add(dc2)
        For i As Integer = 1 To dtSource.Columns.Count - 1
            Dim dr As DataRow = dt.NewRow
            dr("Name") = dtSource.Columns.Item(i).ColumnName
            dr("Value") = dtSource.Rows(0).Item(i)
            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
    
    Sub Esegui()
        Dim crmmanager As New WebTrackManager()
        Dim trackSearcher As New WebTrackSearcher()

        Dim cls As CellSet
        
        ' verifica il data range selezionato
        '------------------------------------------
        If (Search.DateStart.HasValue And Search.DateEnd.HasValue) Then
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateStart.HasValue) Then
            'trackSearcher.WhereData = True  'selezionato solo il DateRangeStart
            trackSearcher.DateRangeStart = Search.DateStart.Value
            trackSearcher.DateRangeEnd = Search.DateUpdate
            trackSearcher.UseRange = True
            'DateRangeEnd.Value = trackSearcher.DateRangeEnd
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        ElseIf (Search.DateEnd.HasValue) Then  'selezionato solo il DateRangeEnd
            'trackSearcher.WhereData = True
            trackSearcher.DateRangeStart = New DateTime(DateTime.Now.Year, 1, 1) ' primo giorno dell'anno corrente
            'DateRangeStart.Value = trackSearcher.DateRangeStart
            trackSearcher.DateRangeEnd = Search.DateEnd.Value
            trackSearcher.UseRange = True
            'Search.objAll.WhereData = True
            Search.objAll.UseRange = True
            Search.objAll.DataStart = trackSearcher.DateRangeStart
            Search.objAll.DateEnd = trackSearcher.DateRangeEnd
        Else
            trackSearcher.WhereData = False
            Search.objAll.UseRange = False
        End If
        
        'verifica se site � selezionato
        If (Not Search.DdlSite Is Nothing) Then
            trackSearcher.WhereSiteCondition = Search.DdlSite.Text
            Search.objAll.Site = Search.DdlSite.Text
            Search.objAll.IdSite = Search.DdlSite.Value
        End If

        'verifica se Area � selezionato
        If (Not Search.DdlArea Is Nothing) Then
            trackSearcher.WhereAreaCondition = Search.DdlArea.Text
            Search.objAll.Area = Search.DdlArea.Text
            Search.objAll.IdArea = Search.DdlArea.Value
        End If

        'verifica se il filtro FF (Field Force) � selezionato
        If (Not Search.DdlFF Is Nothing) Then
            trackSearcher.WhereFFContext = "[" + Trim(Search.DdlFF.Text) + "]"
            Search.objAll.FF = Search.DdlFF.Text
            Search.objAll.IdFF = Search.DdlFF.Value
        End If
        
        If (Not Search.DdlUserType Is Nothing) Then
            trackSearcher.WhereDTContext = "[" + Trim(Search.DdlUserType.Text) + "]"
            Search.objAll.UT = Search.DdlUserType.Text
            Search.objAll.IdUT = Search.DdlUserType.Value
        End If
        
        'imposta la data dell'ultimo aggiornamento  
        trackSearcher.DateStart = Search.DateUpdate
        Search.objAll.DateUpd = Search.DateUpdate
        
        Dim dt As DataTable
       
        ' CALL WEEK VISITS ANALYSIS
        trackSearcher.StoredName = "SP_HP3_CRM_MDX_ReadPeriodSett"
        trackSearcher.TypeQuery = TYPE_WEEK_VISITS
        cls = crmmanager.ReadCellSet(trackSearcher)
            
        ' call display cellset 
        dt = getDataTable2(cls, TYPE_WEEK_VISITS, trackSearcher.UseRange)

        Dim ds As DataSet
        ds = New DataSet
        ds.Tables.Add(RouteDataTable(dt))
        showGraph("Visits by WeekDay", "WeekDay", "Visits", ds)
    End Sub


    ''' <summary>
    ''' COstruisce il DataTable a partire dal cellset
    ''' </summary>
    ''' <param name="cs">CellSet in ingresso</param>
    ''' <param name="gridType">Tipo Tabella</param>
    ''' <param name="bRange">Flag che indica se valorizzare la colonna Range</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Private Function getDataTable2(ByRef cs As CellSet, ByVal gridType As String, ByVal bRange As Boolean) As DataTable

        'design the datatable
        Dim dt As New DataTable
        Dim dc As DataColumn
        Dim dr As DataRow

        'INSERISCE LA TESTATA COLONNA (Columns)
        ' ------------------------------------------------------------
        'Dim ColumnInfoArray As New ArrayList
        s_ColumnsInfo.Clear()

        Dim hRes As Integer

        hRes = GetColumnInfo(s_ColumnsInfo, gridType)
        If (hRes = 0) Then
            ' ADD ERROR BEHAVIOUR
        End If
        Dim name As String
        Dim m As Member


        ' Inserisce la definizione delle colonne 
        '----------------------------------------------
        If s_ColumnsInfo.Count > 0 Then
            '' visualizza il testo nelle colonne        
            Dim columnInfo As [Object]
            For Each columnInfo In s_ColumnsInfo
                dc = New DataColumn(columnInfo.ToString())
                dt.Columns.Add(dc) 'first column
            Next columnInfo

        Else ' default: inserisce il nome colonna standard
            dt.Columns.Add(New DataColumn("Descrizione"))
            ''get the other columns from axis
            Dim p As Position
            Dim i As Integer = 0
            For Each p In cs.Axes(0).Positions
                dc = New DataColumn
                name = "" ' p.Members.Item(0).Name
                For Each m In p.Members
                    name = name + m.Caption + " "
                Next
                dc.ColumnName = "Name" ' + i.ToString()
                'dc.Caption = name
                dt.Columns.Add(dc)
                i = i + 1
            Next
        End If

        ' Inserisce le RIGHE 
        '---------------------------------------
        'add each row, row label first, then data cells
        Dim y As Integer
        Dim py As Position
        y = 0

        For count As Integer = 0 To cs.Axes(1).Positions.Count - 1
            py = cs.Axes(1).Positions(count)
            dr = dt.NewRow 'create new row
            ' RowHeader (elemento in Prima colonna) 
            '-----------------------------------------------
            name = ""
            If Not py.Members(0).Caption.Equals("") Then
                name = GetRowHeaderLink(y, gridType)
                If (name.Equals("")) Then
                    name = py.Members.Item(0).Caption
                End If
                dr(0) = name 'first cell in the row

                ' inserisce i dati Data cells
                Dim x As Integer
                For x = 0 To cs.Axes(0).Positions.Count - 1
                    Dim val As String = cs(x, y).FormattedValue
                    dr(x + 1) = cs(x, y).FormattedValue 'other cells in the row
                Next
                
                dt.Rows.Add(dr) 'add the row
            End If
            y = y + 1
        Next
        Return dt
    End Function

    
    
    ''' <summary>
    ''' Questa funzione deve essere agganciata al dictionary e al tipo di richiesta in modo 
    '''     da costruire correttamente il nome delle colonne
    ''' </summary>
    ''' <param name="ColumnInfoArray"></param>
    ''' <remarks></remarks>
    Function GetColumnInfo(ByVal ColumnInfoArray As ArrayList, ByVal gridType As String)
        If gridType.Equals(TYPE_WEEK_VISITS) Then
            ColumnInfoArray.Add("+ Week Visits")
            ColumnInfoArray.Add("Sunday")
            ColumnInfoArray.Add("Monday")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Tuesday")       ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Wednesday")     ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Thursday")      ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Friday")        ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ColumnInfoArray.Add("Saturday")      ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
            ' sar� sostituito da ColumnInfoArray.Add((multilingue.getDictionary("CRM_Oggi"))
        End If
    End Function
    

    ''' <summary>
    ''' Costruisce il nome della prima riga 
    ''' </summary>
    ''' <param name="rowindex">indice della riga</param>
    ''' <param name="TypeGrid">Tipo Tabella</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRowHeaderLink(ByVal rowindex As Integer, ByVal TypeGrid As String) As String
        Dim RetVal As String = ""
        Return RetVal
    End Function
</script>
<asp:GridView ID="prova" runat="server" />
<div class="dMain">
    <div>&nbsp;</div>
 	<div class="dMiddleBox">
	    <HP3:GenericSearch ID="Search" runat="server" />
        <div>&nbsp;</div>
        <asp:Image id="ImageChart" runat="server"/>
        <div>&nbsp;</div>
    </div>
</div>

<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>