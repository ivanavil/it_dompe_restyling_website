<%@ Control Language="VB" ClassName="Newsletter_Send" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register TagPrefix ="ctl" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagPrefix ="ctl" TagName ="Data" Src ="~/hp3Office/HP3Parts/ctlDate.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Localization" Src ="~/hp3Office/HP3Parts/ctlGeo.ascx" %>


<script runat="server">
#Region "Variabili Private"
    'Manager delle Newsletter
    Private _oNewsletterManager As NewsletterManager
    'Lista delle newsletter visualizzate allo step 1
    Private _oNewsletterCollection As NewsletterCollection
    Private _oNewsletterSearcher As NewsletterSearcher
    Private _sortType As NewsletterComparer.SortType = NewsletterComparer.SortType.BynewsletterId
    Private _sortOrder As NewsletterComparer.SortOrder = NewsletterComparer.SortOrder.DESC
    'Manager delle lingue
    Private oLManager As New LanguageManager()
    Private strDomain As String
    Private strJs As String
    Private DefaultMittente As String = ""
    'Oggetto temporaneo usato per la valorizzazione dell' objectvalue usato nell'invio
    Private oCurNewsletterSend As NewsletterSendingValue
    Private dictionaryManager As New DictionaryManager
    Private dateUtility As New StringUtility
    Private masterPageManager As New MasterPageManager
    Private _strDomain As String
    Private _language As String
    Private _newsletterlist As New ArrayList
    Private _grouplist As New ArrayList
#End Region
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property NewsletterList() As ArrayList
        Get
            Return _newsletterlist
        End Get
        Set(ByVal value As ArrayList)
            _newsletterlist = value
        End Set
    End Property
    
    Private Property GroupList() As ArrayList
        Get
            Return _grouplist
        End Get
        Set(ByVal value As ArrayList)
            _grouplist = value
        End Set
    End Property
    
#Region "Strutture Dati"
    Enum SendNewsletterStep
        'Primo step.Fase in cui l'utente seleziona le newsletter
        NewsletterSelection = 1
        'Secondo step. Fase in cui l'utente seleziona se si tratta di un invio di test
        'e il mittente
        NewsletterSendParameters = 2
        'Terzo step. Selezione dei destinatari
        NewsletterTargetSelection = 3
        'Invio della newsletter e riepilogo degli utenti selezionati
        NewsletterSend = 4
    End Enum
#End Region
    
#Region "Metodi Privati"
    ''' <summary>
    ''' Legge dall' Oggetto SelectedNewsletter una property della classe
    ''' </summary>
    ''' <param name="FieldName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadNewsletterSendField(ByVal FieldName As String) As String
        Dim oClassUtility As New ClassUtility
        Try
            Dim oNews As NewsletterSendingValue = SelectedNewsletter
           
            Return oClassUtility.GetPropertyValue(oNews, FieldName)
            
        Catch ex As Exception
            Return ""
        End Try
    End Function
    
    ''' <summary>
    ''' In base allo step stabilisce se abilitare o meno il tasto next
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CheckForNext(ByVal PanelIdentificator As SendNewsletterStep) As Boolean
        Dim bOut As Boolean
        Select Case PanelIdentificator
            Case SendNewsletterStep.NewsletterSelection
                btnPrevious.Visible = False
                'Se non viene selezionata alcuna newsletter
                'Il bottone avanti deve essere spento
                Return txtSelectedNews.Text <> ""
                
            Case SendNewsletterStep.NewsletterSendParameters
                'Il campo mittente deve contenere un indirizzo email valido
                Return txtMittente.Text <> "" 'AndAlso Regex.IsMatch(txtMittente.Text, "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")
           
            Case SendNewsletterStep.NewsletterTargetSelection
                'Recupero info sulla newsletter.
                'In modo particolare controllo se si tratta di un test
                oCurNewsletterSend = SelectedNewsletter
                If oCurNewsletterSend.IsTest Then
                    'in tal caso devo controllare anche i campi contenenti le info sulle mail dei destinatari
                    bOut = txtMaxEmail.Text <> "" AndAlso txtListaDestinatariTest.Text <> ""
                Else
                    bOut = True
                End If
                
                'Controllo il campo hidden per risalire al nodo di filtro aperto
                Select Case GetIntForOpenTargetPanel()
                    Case 1
                        'FILTRO GENERICO                       
                        'GEO
                        bOut = bOut And txtGeoId.Text <> ""
                        'ISCRITTI ALLE NEWSLETTER
                        bOut = bOut Or (lstNewsletterSubscription.Items.Count > 0)
                        'GRUPPI DI UTENTI
                        bOut = bOut Or txtGruppi.Text <> ""
                    Case 3
                        'FILTRO PER QUERY
                        bOut = bOut And txtQuery.Text <> ""                                                
                    Case 0
                        'NESSUN PANNELLO E' STATO APERTO
                        bOut = False
                End Select
                
                Return bOut
                
            Case SendNewsletterStep.NewsletterSend
                'FASE DI INVIO E RIEPILOGO DEGLI UTENTI SELEZIONATI
                
                'Ultimo step
                Return False
        End Select
    End Function
    
    ''' <summary>
    ''' Restituisce un intero in corrispondenza del pannello di filtro aperto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetIntForOpenTargetPanel() As Int16
        
        Select Case txtHidePanelSelection.Text
            Case "aSelectorGeneric"
                Return 1
            Case "aSelectorQuery"
                Return 3
            Case Else
                Return 0
        End Select
    End Function
    
    ''' <summary>
    ''' Legge un NewsletterValue dato l'id
    ''' </summary>
    ''' <param name="idNewsletter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSendingNewsletter(ByVal idNewsletter As Int16) As NewsletterSendingValue
        _oNewsletterManager = New NewsletterManager
        _oNewsletterSearcher = New NewsletterSearcher
        _oNewsletterSearcher.Key.KeyNewsletter = idNewsletter
        _oNewsletterCollection = _oNewsletterManager.Read(_oNewsletterSearcher)
        Dim oNewsletterValue As NewsletterValue = _oNewsletterCollection(0)
        
        Dim oNewsletterSendedValue As New NewsletterSendingValue(oNewsletterValue)
        Return oNewsletterSendedValue
    End Function
    
    Function GetContentType() As Integer
        Dim cttypeManager As New ContentTypeManager
        Dim cttypeIndent As New ContentTypeIdentificator
        cttypeIndent.Domain = "CMS"
        cttypeIndent.Name = "Newsletter"
        
        Dim cttypeValue As ContentTypeValue = cttypeManager.Read(cttypeIndent)
        If Not cttypeValue Is Nothing Then
            Return cttypeValue.Key.Id
        End If
        
        Return 0
    End Function
    ''' <summary>
    ''' Carica la lista delle newsletter.
    ''' Usata nel primo step    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function LoadNewsletter() As ContentCollection
        Dim _oContentCollection As New ContentCollection
        
        If (Request("s") <> 0) Then
            Dim _oContentSearcher As New ContentSearcher
            Dim _oContentManager As New ContentManager
          
            Dim ctypeId As Integer = GetContentType()
          
            If (ctypeId <> 0) Then
                _oContentSearcher.KeySite.Id = Request("s")
                _oContentSearcher.KeyContentType.Id = ctypeId
            
                _oContentManager.Cache = False
                _oContentCollection = _oContentManager.Read(_oContentSearcher)
           
                If Not (_oContentCollection Is Nothing) AndAlso (_oContentCollection.Count > 0) Then _oContentCollection.Sort(SortType, SortOrder)
                              
                Return _oContentCollection
            
            End If
            Return Nothing
        End If
        
        Return Nothing
    End Function
    
    Private Function LoadNewsletterList() As NewsletterListCollection
        Try
            _oNewsletterManager = New NewsletterManager
            _oNewsletterManager.Cache = False
        
            Dim _oNewsletterListCollection As NewsletterListCollection
            _oNewsletterListCollection = _oNewsletterManager.Read(New NewsletterListSearcher())
            Return _oNewsletterListCollection
        Catch ex As Exception
            ErrorManager(ex)
        Finally
            _oNewsletterManager = Nothing
        End Try
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Preleva l'errore e lo stampa nel pannello pnlError.
    ''' </summary>
    ''' <param name="oException"></param>
    ''' <remarks></remarks>
    Private Sub ErrorManager(ByVal oException As Exception)
        pnlError.Visible = True
        
        lblErrorInfo.Text = oException.ToString
    End Sub
    
    ''' <summary>
    ''' Gestisce la visualizzazione delle info relative ad ogni step
    ''' </summary>
    ''' <param name="Message"></param>
    ''' <remarks></remarks>
    Private Sub InfoManager(ByVal Message As String)
        pnlInfo.Visible = True
        lblInfo.Text = Message
    End Sub
    
    
    ''' <summary>
    ''' determino gli uetenti relazionati agli utenti destinatari della newsletter
    ''' </summary>
    ''' <remarks></remarks>
    Function ReadRelatedUser() As Dictionary(Of String, ContactValue)
        Dim outRelUserColl As Dictionary(Of String, ContactValue) = Nothing
        
        If chkIncludeRelUser.Checked Then
            outRelUserColl = _oNewsletterManager.ExecuteContactQuery(txtRelUserQuery.Text)
       
        End If
       
        Return outRelUserColl
    End Function
    
    ''' <summary>
    ''' Valorizzo la lista degli utenti dell' oggetto SelectedNewsletter in base al tipo di filtro effettuato
    ''' </summary>
    ''' <remarks></remarks>
    Function ReadUser() As UserCollection
        Dim outUserCollection As UserCollection
        Dim oUserSearcher As New UserSearcher
        Dim oUserManager As New UserManager
        Dim localUserCollection As UserCollection
        
        _oNewsletterManager = New NewsletterManager
        _oNewsletterManager.Cache = False
        
        Select Case GetIntForOpenTargetPanel()
            Case 1
                'FILTRO GENERICO
                
                'GEO-------------------------------------------------------
                If txtGeoId.Text <> "" Then
                    'Controllo se � stata selezionata una localit�
                    oUserSearcher.Geo.Key.Id = txtGeoId.Text
                    
                    'Prelevo gli utenti sulla base dei filtri effettuati
                    outUserCollection = oUserManager.Read(oUserSearcher)
                End If
                
                If outUserCollection Is Nothing Then outUserCollection = New UserCollection
                '----------------------------------------------------------                                
                
                'ISCRITTI ALLA NEWSLETTER----------------------------------
                Dim oNewsSearcher As New NewsletterSubscriptionSearcher
                NewsletterList = New ArrayList
                Dim lst As ListItem = lstNewsletterSubscription.Items(0)
                            
                For Each lstItem As ListItem In lstNewsletterSubscription.Items
                    If lstItem.Selected Then
                        'oNewsSearcher.KeyNewsletterList = lstItem.Value.Trim
                        NewsletterList.Add(lstItem.Value.Trim)
                        'Recupero la lista degli utenti relativa alla newsletter corrente
                        'localUserCollection = _oNewsletterManager.ReadUserSubscriptions(oNewsSearcher)
                        'Faccio il merge delle due collezioni
                        'If Not localUserCollection Is Nothing Then outUserCollection = UserCollection.MergeCollection(outUserCollection, localUserCollection)
                    End If
                Next
                
                If (chkssubscribtionMail.Checked) Then
                    If Not (NewsletterList Is Nothing) AndAlso (NewsletterList.Count > 0) Then
                        oNewsSearcher.KeyNewsletterList = NewsletterList(0)
                        outUserCollection = _oNewsletterManager.ReadUserSubscriptions(oNewsSearcher)
                    End If
                Else
                     
                    If Not (NewsletterList Is Nothing) AndAlso (NewsletterList.Count > 0) Then
                        Dim userM As New UserManager
                        Dim userSubCollection As New UserCollection
                        outUserCollection = New UserCollection
                       
                        oNewsSearcher.KeyNewsletterList = NewsletterList(0)
                        userSubCollection = _oNewsletterManager.ReadUserSubscriptions(oNewsSearcher)
                        For Each user As UserValue In userSubCollection
                            Dim userV As New UserValue
                            
                            userV = userM.Read(user.Key)
                            outUserCollection.Add(userV)
                        Next
                    End If
                End If
                '----------------------------------------------------------
                
                'FILTRI PER GRUPPI-----------------------------------------                
                'Lista degli utenti selezionati
                
                GroupList = New ArrayList
                'Dim lstgrp As ListItem = lstGruppi.Items(0)
                            
                For Each lstItem As ListItem In lstGruppi.Items
                    If lstItem.Selected Then
                        'oNewsSearcher.KeyNewsletterList = lstItem.Value.Trim
                        GroupList.Add(lstItem.Value.Trim)
                        'Recupero la lista degli utenti relativa alla newsletter corrente
                        'localUserCollection = _oNewsletterManager.ReadUserSubscriptions(oNewsSearcher)
                        'Faccio il merge delle due collezioni
                        'If Not localUserCollection Is Nothing Then outUserCollection = UserCollection.MergeCollection(outUserCollection, localUserCollection)
                    End If
                Next
                
                If Not (GroupList Is Nothing) AndAlso (GroupList.Count > 0) Then
                    Dim _oUserGroupManager As New UserGroupManager
                    
                    For Each group As Integer In GroupList
                        localUserCollection = _oUserGroupManager.ReadUserRelated(New UserGroupIdentificator(group))
                        If Not localUserCollection Is Nothing Then outUserCollection = UserCollection.MergeCollection(outUserCollection, localUserCollection)
                    Next
                    'outUserCollection = 
                End If
                                
                localUserCollection = New UserCollection
                Dim arrUserId As String()
                Dim item As String
                Dim _oUserManager As New UserManager
                _oUserManager.Cache = False
                
                Dim oUserValue As UserValue
                
                If txtHiddenGruop.Text <> "" Then
                    'Se esistono utenti selezionati
                    arrUserId = txtHiddenGruop.Text.Split(",")
                    For Each item In arrUserId
                        'Se l' item corrente � nel formato giusto
                        If IsNumeric(item) Then
                            'Carico l'utente associato
                            oUserValue = _oUserManager.Read(New UserIdentificator(item))
                            'Se lo ha trovato lo carico nella lista
                            If Not oUserValue Is Nothing Then localUserCollection.Add(oUserValue)
                        End If
                    Next
                End If
                                  
                'Faccio il merge delle due collezioni
                If Not localUserCollection Is Nothing AndAlso localUserCollection.Count > 0 Then outUserCollection = UserCollection.MergeCollection(outUserCollection, localUserCollection)
                '----------------------------------------------------------
                
            Case 3
                'FILTRO PER QUERY 
                outUserCollection = _oNewsletterManager.ExecuteQuery(txtQuery.Text)
                        
            Case 0
                'NESSUN PANNELLO E' STATO APERTO                
        End Select
                
        Return outUserCollection.DistinctBy("Key.Id")
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    ''' <summary>
    ''' Gestisce l'apertura dei pannelli associati al servizio
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub OpenPanel(ByVal PanelIdentificator As SendNewsletterStep)
        Dim msgInfo As String
        'Carico le localit�
        oLocaliztion.TypeControl = ctlLocalization.ControlType.Geo
        oLocaliztion.LoadControl()
      
        'Carico la lista delle newsletter usate per i filtri generici        
        If Not Page.IsPostBack Then
            BindListNewsletterSubscription()
            BindUserGroup()
        End If
      
                
        'Chiude tutti i pannelli presenti sulla form
        CloseAllPanel()
        
        Select Case PanelIdentificator
            Case SendNewsletterStep.NewsletterSelection                               
                'Primo step selezione della newsletter    
                
                'Carico la lista delle newsletter
                BindListNewsletter()
                
                                              
                msgInfo = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormSendNewsletterStep1&Help=cms_HelpSendNewsletterStep1&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormSendNewsletterStep1", "Step 1 - Select Newsletter") & "</strong>"
                
                pnl_Step1.Visible = True
                
            Case SendNewsletterStep.NewsletterSendParameters
                'Step per il recupero dei parametri di invio.                
                'Newsletter test e mail mittente
                
                msgInfo = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormSendNewsletterStep2&Help=cms_HelpSendNewsletterStep2&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormSendNewsletterStep2", "Step 2 - Parameters to Send") & "</strong>"
                pnl_Step2.Visible = True
                
                'leggo l'id della newsletter dal campo Hidden
                SelectedNewsletter = GetSendingNewsletter(txtSelectedNews.Text)
                
                txtMittente.Attributes.Add("onKeyUp", "CheckMittente()")
                'txtMittente.Text = DefaultMittente
            Case SendNewsletterStep.NewsletterTargetSelection
                        
                'Terzo step. Effettuo la selezione dei destinatari                
                msgInfo = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormSendNewsletterStep3&Help=cms_HelpSendNewsletterStep3&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormSendNewsletterStep3", "Step 3 - Recipients") & "</strong>"
                
                pnl_Step3.Visible = True
                
                'Salvo lo stato dell' oggetto
                oCurNewsletterSend = SelectedNewsletter
                If Not oCurNewsletterSend Is Nothing Then
                    If ctlData.Value.HasValue Then
                        oCurNewsletterSend.DataPosticipata = ctlData.Value
                    End If
                    
                    oCurNewsletterSend.IsTest = chkTest.Checked
                    oCurNewsletterSend.Mittente = txtMittente.Text
                    oCurNewsletterSend.Subject = txtOggetto.Text
                    SelectedNewsletter = oCurNewsletterSend
                End If
                
                If chkIncludeRelUser.Checked Then
                    div_SelectorRelUserQuery.Visible = True
                Else
                    div_SelectorRelUserQuery.Visible = False
                End If
                
                If chkTest.Checked Then
                    'NEL CASO DI TEST VISUALIZZA IL PANNELLO CON LE INFO SUI DESTINATARI
                    pnl_Test.Visible = True
                Else
                    pnl_Test.Visible = False
                End If
                
                txtQuery.Attributes.Add("onKeyUp", "CheckForTarget()")
                txtMaxEmail.Attributes.Add("onKeyUp", "CheckForTarget()")
                txtListaDestinatariTest.Attributes.Add("onKeyUp", "CheckForTarget()")
                lstNewsletterSubscription.Attributes.Add("onchange", "CheckForTarget()")
                txtGruppi.Attributes.Add("onKeyDown", "return false")
                txtGruppi.Attributes.Add("onPaste", "return false")
                
                'Se un pannello � gi� stato aperto
                If txtHidePanelSelection.Text <> "" Then
                    'Richiamo il metodo javascript che simula il click sul nodo
                    strJs = "OpenTargetDiv(document.getElementById('" & txtHidePanelSelection.Text & "'));"
                End If
                                                              
            Case SendNewsletterStep.NewsletterSend
                'Controllo lo step precedente.
                'Controllo la query(se sto filtrando per query)
                'e la lista dei destinatari
                Dim Errore As String
                If Not CheckForStep4(Errore) Then
                   
                    'Torno indietro
                    GoBack(Nothing, Nothing)
                    
                    'stampo l'errore
                    strJs = "alert('" & Errore & "');"
                    
                    'Se un pannello � gi� stato aperto
                    If txtHidePanelSelection.Text <> "" Then
                        'Richiamo il metodo javascript che simula il click sul nodo
                        strJs += "OpenTargetDiv(document.getElementById('" & txtHidePanelSelection.Text & "'));"
                    End If
                    
                    Return
                End If
                
                'INVIO DELLA NEWSLETTER E RIEPILOGO DEGLI UTENTI SELEZIONATI
                'msgInfo = "<strong>Step 4</strong>. Send."
                msgInfo = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormSendNewsletterStep4&Help=cms_HelpSendNewsletterStep4&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormSendNewsletterStep4", "Step 4 - Send") & "</strong>"
                
                pnl_Step4.Visible = True
                
                'Salvo lo stato dell' oggetto
                oCurNewsletterSend = SelectedNewsletter
                Dim ListUSer As UserCollection = ReadUser()
                
                Dim ListRelUser As Dictionary(Of String, ContactValue) = Nothing
                If chkIncludeRelUser.Checked Then ListRelUser = ReadRelatedUser()
                
                If Not oCurNewsletterSend Is Nothing Then
                    oCurNewsletterSend.Destinatari = txtListaDestinatariTest.Text
                    oCurNewsletterSend.MaxMailForTest = txtMaxEmail.Text
                    oCurNewsletterSend.ListUser = ListUSer
                    
                    If Not ListRelUser Is Nothing Then oCurNewsletterSend.ListRelatedUser = ListRelUser
                    SelectedNewsletter = oCurNewsletterSend
                End If
                
                'Il recupero degli utenti ha avuto esito negativo.Impossibile inviare la newsletter
                If ListUSer.Count = 0 Then
                    divSend.Visible = False
                    ErrorManager(New Exception("Users not found. To modify the filters and to try again."))
                Else
                    divSend.Visible = True
                End If
                               
                'SALVO L'OGGETTO NELLA SESSIONE
                Session("NewsletterSend") = SelectedNewsletter
                Session("NewsletterList") = NewsletterList
        End Select
                       
        'Stampo le info sullo Step
        InfoManager(msgInfo)
        
        'Gestisco la visibilit� dei bottoni cursore
        ButtonVisibility(PanelIdentificator)
    End Sub
    
    ''' <summary>
    ''' Spegne tutti pannelli sulla WebForn
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CloseAllPanel()
        For Each pnlGeneric As Control In Me.Controls
            If TypeOf pnlGeneric Is HtmlControls.HtmlContainerControl Then pnlGeneric.Visible = False
        Next
    End Sub
    
    ''' <summary>
    ''' Gestisce la visibilit� dei bottoni cursore
    ''' </summary>
    ''' <param name="PanelIdentificator"></param>
    ''' <remarks></remarks>
    Sub ButtonVisibility(ByVal PanelIdentificator As SendNewsletterStep)
        btnNext.Style.Add("display", "''")
        btnPrevious.Style.Add("display", "''")
        
        btnPrevious.Visible = True
        btnNext.Visible = True
        
        'Verifico lo step corrente e stabilisco se accendere o spengnere il pulsante next
        If CheckForNext(PanelIdentificator) Then
            btnNext.Style.Add("display", "''")
        Else
            btnNext.Style.Add("display", "none")
        End If
                
    End Sub
#End Region
    
#Region "Property"
    ''' <summary>
    ''' Contiene le informazioni sulla newsletter selezionata.
    ''' Tale valore viene inserito nel secondo step.    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property SelectedNewsletter() As NewsletterSendingValue
        Get
            Return ViewState("SelectedNewsletter")
        End Get
        Set(ByVal value As NewsletterSendingValue)
            ViewState("SelectedNewsletter") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Recupera e setta le info sullo step corrente.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ServiceStep() As SendNewsletterStep
        Get
            If ViewState("STEP") Is Nothing Then
                ViewState("STEP") = SendNewsletterStep.NewsletterSelection
            End If
            
            Return ViewState("STEP")
        End Get
        Set(ByVal value As SendNewsletterStep)
            ViewState("STEP") = value
        End Set
    End Property
        
    ''' <summary>
    ''' Legge il dominio di riferimento
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DomainInfo() As String
        Get
            Dim SiteManager As New SiteManager
            Dim SiteValue As SiteValue = SiteManager.getDomainInfo()
            Dim arr() As String = SiteValue.Folder.Split("/")
            ReDim Preserve arr(arr.GetUpperBound(0) - 1)
            Dim strDomain = "http://" & SiteValue.Domain & "/" & Join(arr, "/")
            
            Return strDomain
        End Get
    End Property
    
    Private Property SortType() As NewsletterComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = NewsletterComparer.SortType.ByNewsletterId ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As NewsletterComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As NewsletterComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = NewsletterComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As NewsletterComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
#End Region
    
#Region "Eventi della Classe"
    Sub Page_Load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo()
        
        'Salvo l'url
        Session("LastViewedUrl") = Request.Url.ToString
        strDomain = DomainInfo
        
        'Gestisco l' apertura dei pannelli in base allo step corrente
        OpenPanel(ServiceStep)
                     
    End Sub
    
    ''' <summary>
    ''' Spostamento verso lo step successivo
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub GoBack(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ServiceStep -= 1
        'Gestisco l' apertura dei pannelli in base allo step corrente
        OpenPanel(ServiceStep)
    End Sub
    
    ''' <summary>
    ''' Spostamento verso lo step precedente
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub GoNext(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ServiceStep += 1
        'Gestisco l' apertura dei pannelli in base allo step corrente
        OpenPanel(ServiceStep)
    End Sub
    
    ''' <summary>
    ''' Routine per l'associazione delle localit�.Usata nel terzo step
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub SetGeo(ByVal s As Object, ByVal e As EventArgs)
        Dim _oGeoCollection As GeoCollection = oLocaliztion.GeoList
        oLocaliztion.GeoListResetSelection()
        Dim oGeoValue As GeoValue
        If Not _oGeoCollection Is Nothing Then
            oGeoValue = _oGeoCollection(0)
            
            txtGeoId.Text = oGeoValue.Key.Id
            txtGeoDescr.Text = oGeoValue.Description & " (" & oGeoValue.GeoType.Description & ")"
            'Dopo aver recuperato le info sulla localit�
            'Chiamo il javascript per forzare l'abilitazione del pulsante next
            strJs += "CheckForTarget();" & vbCrLf
        End If
        
        'Se un pannello � gi� stato aperto
        If txtHidePanelSelection.Text <> "" Then
            'Richiamo il metodo javascript che simula il click sul nodo
            strJs += "OpenTargetDiv(document.getElementById('" & txtHidePanelSelection.Text & "'));"
            
        End If
    End Sub
    
    ''' <summary>
    ''' Ripulisce dalla selezione effettuata su GEO
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub ClearGeo(ByVal s As Object, ByVal e As EventArgs)
        txtGeoId.Text = ""
        txtGeoDescr.Text = ""
        
        'Dopo aver recuperato le info sulla localit�
        'Chiamo il javascript per forzare l'abilitazione del pulsante next
        strJs += "CheckForTarget();" & vbCrLf
        
        'Se un pannello � gi� stato aperto
        If txtHidePanelSelection.Text <> "" Then
            'Richiamo il metodo javascript che simula il click sul nodo
            strJs += "OpenTargetDiv(document.getElementById('" & txtHidePanelSelection.Text & "'));"
            
        End If
    End Sub
#End Region
    
#Region "Metodi Privati Per lo STEP3"
    'SELEZIONE DEI DESTINATARI
    
    ''' <summary>
    ''' Carica la lista delle newsletter usata per filtrare i destinatari
    ''' </summary>
    ''' <remarks></remarks>
    Sub BindListNewsletterSubscription()       
        Dim oWebUtility As New WebUtility
        oWebUtility.LoadListControl(lstNewsletterSubscription, LoadNewsletterList, "Label", "Key")
    End Sub
    
    Sub BindUserGroup()
        Dim oWebUtility As New WebUtility
        Dim _oUserGroupManager As New UserGroupManager
        Dim Searcher As New UserGroupSearcher
        
        Dim _oUserGroupColl As UserGroupCollection
        _oUserGroupColl = _oUserGroupManager.Read(Searcher)
        oWebUtility.LoadListControl(lstGruppi, _oUserGroupColl, "Description", "Key.Id")
        'in attesa gestione ruoli con usergroup
        
        ''''''BAXTER UTILITY'''''''''''''''''''''
        'If (Request("S") <> 0) Then
        '    Searcher.Type = Request("S")
        '    _oUserGroupColl = _oUserGroupManager.Read(Searcher)
        '    If Not (_oUserGroupColl Is Nothing) AndAlso (_oUserGroupColl.Count > 0) Then
        '        oWebUtility.LoadListControl(lstGruppi, _oUserGroupColl, "Description", "Key.Id")
        '    Else
        '        ''''COMPORTAMENTO STANDRD'''''''
        '        Searcher = New UserGroupSearcher
        '        _oUserGroupColl = _oUserGroupManager.Read(Searcher)
        '        oWebUtility.LoadListControl(lstGruppi, _oUserGroupColl, "Description", "Key.Id")
        '    End If
        'End If
        ''''''BAXTER UTILITY''''''''''''''''''
               
    End Sub
#End Region
    
#Region "Metodi Privati Per lo STEP1"
    ''' <summary>
    ''' Funzione usata lato client
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function IsTest() As Int16
        Dim ret As String = ReadNewsletterSendField("IsTest")
        
        If ret = "" Then
            Return 0
        Else
            Return IIf(ret.ToLower = "true", 1, 0)
        End If
    End Function
    
    ''' <summary>
    ''' Caricamento della lista delle newsletter
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindListNewsletter()
        GridNewsletter.DataSource = LoadNewsletter()
        GridNewsletter.DataBind()
    End Sub
    
    'Ordinamento della griglia
    Sub Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        GridNewsletter.PageIndex = 0
        Select Case e.SortExpression
            Case "KeyNewsletter.Keynewsletter"
               
                If SortType <> NewsletterComparer.SortType.ByNewsletterId Then
                    SortType = NewsletterComparer.SortType.ByNewsletterId
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Title"
                If SortType <> NewsletterComparer.SortType.ByTitle Then
                    SortType = NewsletterComparer.SortType.ByTitle
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "DateCreate"
                If SortType <> NewsletterComparer.SortType.ByData Then
                    SortType = NewsletterComparer.SortType.ByData
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        
        BindListNewsletter()
    End Sub
    
    Sub GridBound(ByVal s As Object, ByVal e As GridViewRowEventArgs)
        Dim objRadio As HtmlInputRadioButton
        Dim objLit As Literal
        
        objRadio = e.Row.FindControl("rdNewsletter")
        objLit = e.Row.FindControl("litVal")
        If Not objRadio Is Nothing AndAlso Not objLit Is Nothing Then
            'Gestione dell' evento
            objRadio.Attributes.Add("onclick", "onRadioClick(document.getElementById('" & objRadio.ClientID & "')," & objLit.Text & ")")
            
            If objLit.Text = txtSelectedNews.Text Then
                'Recupero del valore
                strJs = "onRadioClick(document.getElementById('" & objRadio.ClientID & "')," & objLit.Text & ")"
            End If
        End If

    End Sub
    
    Sub PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindListNewsletter()
    End Sub
    
    ''' <summary>
    ''' Leggo dato l'idTemplate l'objectvalue associato
    ''' </summary>
    ''' <param name="idTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadTemplateInfoById(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateNewsletterValue
        Dim _oNewsletterManager As New NewsletterManager
        
        Return _oNewsletterManager.Read(idTemplate)
    End Function
#End Region
    
#Region "Metodi Privati Per lo STEP4"
    ''' <summary>
    ''' Controlla la validit� dei dati inseriti nello step 3
    ''' True se � possibile procedere
    ''' False si � verificato un errore gestito tramite alert    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CheckForStep4(ByRef outErrore As String) As Boolean
        Dim bRet As Boolean = True
        Dim errore As String
        
        'Controllo la query se ho selezionato il filtro per query
        'Controllo se st� filtrando per query
        If GetIntForOpenTargetPanel() = 3 Then
            'Controllo se la query inserita � di selezione
            If CheckCampoQueryForSelect(txtQuery.Text) Then
                
                outErrore = "Only Select Query"
                Return False
                
                'Controllo se la query inserita � valida o meno
            ElseIf CheckQueryValidate(txtQuery.Text, errore) Then
                
                outErrore = errore
                Return False
                
            End If
        End If
                
        'Controllo la lista dei destinatari se si tratta di un test
        If SelectedNewsletter.IsTest Then
            Dim arrEmails As String()
            Dim strItemEmail As String
            'Controllo se esiste il separatore ,
            If txtListaDestinatariTest.Text.IndexOf(",") <> -1 Then
                'In tal caso splitto il testo nell' array
                arrEmails = txtListaDestinatariTest.Text.Split(",")
            Else
                'altrimenti inserisco nell'array l'unico elemento presente
                ReDim arrEmails(0)
                arrEmails(0) = txtListaDestinatariTest.Text
            End If
        
            'Ciclo sulla lista verificando che gli indirizzi siano corretti
            For Each strItemEmail In arrEmails
                If Not Regex.IsMatch(strItemEmail, "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$") Then
                
                    outErrore = "Email Recipient Error."
                    Return False
                End If
            Next
        End If
        
        Return bRet
    End Function
    
    ''' <summary>
    ''' Controlla se la query inserita � di selezione
    ''' </summary>
    ''' <param name="txtQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CheckCampoQueryForSelect(ByVal txtQuery As String) As Boolean
        Dim str As String = txtQuery
        Dim arr() As String
        Dim arr2() As String
        Dim strItem, strItem2 As String
		
        str = str.Trim
        arr = str.Split(" ")
		
        For Each strItem2 In arr
            arr2 = strItem2.Split(vbCrLf)
            For Each strItem In arr2
                If (strItem.Trim.ToLower = "insert") Or (strItem.Trim.ToLower = "update") Or (strItem.Trim.ToLower = "delete") Or (strItem.Trim.ToLower = "truncate") Or (strItem.Trim.ToLower = "drop") Or (strItem.Trim.ToLower = "alter") Then
                    Return True
                End If
            Next
        Next
		
        Return False
    End Function
    
    ''' <summary>
    ''' Controlla la validit� della query
    ''' </summary>
    ''' <param name="sqlQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CheckQueryValidate(ByVal sqlQuery As String, ByRef Errore As String) As Boolean
        Dim Err As String
        _oNewsletterManager = New NewsletterManager
        Dim outVal As Boolean = _oNewsletterManager.TestQuery(sqlQuery, Err)
	
        If outVal Then
            Errore = Err.Replace("'", "")
        End If
               
        Return outVal
    End Function
#End Region
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, masterPageManager.GetLang.Code)
    End Function
    
    Function GetKeyNewsletter(ByVal primaryKey As Integer) As Integer
        _oNewsletterManager = New NewsletterManager
        Dim ident As New NewsletterIdentificator()
        ident.PrimaryKey = primaryKey
        
        Dim _oNewsletter As NewsletterValue = _oNewsletterManager.Read(ident)
        
        If Not (_oNewsletter Is Nothing) Then
            Return _oNewsletter.KeyNewsletter.KeyNewsletter
        End If
        
        Return Nothing
    End Function
    
    Function GetTotSended(ByVal primaryKey As Integer) As Integer
        _oNewsletterManager = New NewsletterManager
        Dim ident As New NewsletterIdentificator()
        ident.PrimaryKey = primaryKey
        
        Dim _oNewsletter As NewsletterValue = _oNewsletterManager.Read(ident)
        
        If Not (_oNewsletter Is Nothing) Then
            Return _oNewsletter.TotSended
        End If
        
        Return Nothing
    End Function
    
    
</script>
<div>&nbsp;</div>  
<div id="pnlError" runat="server" visible="false" style="border:1px">
<%--PANNELLO CONTENENTE I MESSAGGI DI ERRORE--%>
    <asp:Label ID="lblErrorInfo" runat="server" />
</div>

<div id="pnlInfo" runat="server"  style="margin-bottom:5px; margin-top:0px" visible="false">
<%--PANNELLO CONTENENTE LE DESCRIZIONI DEGLI STEP--%>
    <asp:Label ID="lblInfo" runat="server" />
</div>

<div id ="pnl_Step1" runat="server" visible="false" style="margin:10px 0 10px 0">
<%--PANNELLO CONTENENTE LA LISTA DELLE NEWSLETTER DA INVIARE--%>
<asp:textbox ID="txtSelectedNews" runat="server" style="display:none"/>
<asp:GridView         
    ID="GridNewsletter" AutoGenerateColumns="false"  CssClass="tbl1"
    AlternatingRowStyle-BackColor="#eeefef"
    AllowPaging="true"  PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager"  
    PageSize="15" OnPageIndexChanging="PageIndexChanging"
    AllowSorting="true" 
    ShowHeader="true"  HeaderStyle-CssClass="header"
    ShowFooter="false"  FooterStyle-CssClass="gridFooter"
    runat="server"
    emptydatatext="No content available"
    Width="100%"
    GridLines="None" OnSorting="Sorting"     
    OnRowDataBound   ="GridBound">
    
    <Columns>
        <asp:TemplateField ItemStyle-Width="2%" >
            <ItemTemplate>
                <input type="radio" runat="server" id ="rdNewsletter"/><%--text='<%#container.dataitem.KeyNewsletter.KeyNewsletter%>'--%>
				<asp:literal id="litVal" runat="Server" text='<%#GetKeyNewsletter(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"))%>' visible="false" />
            </ItemTemplate>
        </asp:TemplateField>        
        <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <img alt="Language: <%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" />
            </ItemTemplate>
        </asp:TemplateField>         
        <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" SortExpression ="KeyNewsletter.Keynewsletter">
                 <ItemTemplate><%#GetKeyNewsletter(DataBinder.Eval(Container.DataItem, "Key.PrimaryKey"))%><%--<%#DataBinder.Eval(Container.DataItem, "KeyNewsletter.KeyNewsletter")%>--%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Date" SortExpression="DateCreate" HeaderStyle-Width="10%">
                <ItemTemplate><%#getCorrectedFormatDate(DataBinder.Eval(Container.DataItem, "DateCreate"))%></ItemTemplate>
            </asp:TemplateField>
        <%--<asp:BoundField SortExpression="DateCreate" DataField ="DateCreate" HeaderText="Date"/>    --%>
         <asp:TemplateField HeaderText="Content ID" HeaderStyle-Width="10%">
            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
        </asp:TemplateField>    
        <asp:BoundField SortExpression="Title" DataField ="Title" HeaderText="Title"/>                             
         <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>                     
                <asp:image ID="Image1" runat="server" title="Newsletter already sended" ImageUrl="~/HP3Office/HP3Image/ico/content_active.gif" visible ="<%#GetTotSended(container.dataitem.Key.PrimaryKey) > 0%>"/>
                <asp:image ID="Image2" runat="server" title="Newsletter not sended" ImageUrl="~/HP3Office/HP3Image/ico/content_noactive.gif" visible ="<%#GetTotSended(container.dataitem.Key.PrimaryKey) = 0%>"/><%--<%#container.dataitem.KeyNewsletter.KeyNewsletter%>--%>
                <img style="cursor:pointer" alt="Newsletter preview" src="/HP3Office/HP3Image/ico/content_preview.gif" onclick="window.open('<%=strDomain%>/Popups/anteprima_newsletter.aspx?idNewsletter=<%#GetKeyNewsletter(container.dataitem.Key.PrimaryKey)%>','','width=800,height=600,scrollbars=no,status=yes')"/>
            </ItemTemplate>
        </asp:TemplateField>     
    </Columns>    
    <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
</asp:GridView>
</div>

<div id ="pnl_Step2" runat="server" visible="false" style="margin:10px 0 10px 0;width:90%">
    <%--PANNELLO CONTENENTE I PARAMETRI DI INVIO--%>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterSendOn&Help=cms_HelpNewsletterSendOn&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterSendOn", "Send on")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;width:300px;">
        <ctl:Data runat="server" id ="ctlData" TypeControl ="PopupCalendar" ShowTime="true" />
    </div>
    <div style="clear:both;"></div>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailSubject&Help=cms_HelpNewsletterMailSubject&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMailSubject", "Mail Subject")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;">
        <asp:textbox runat="server" ID="txtOggetto" columns ="50" />                
    </div>
       <div style="clear:both;"></div>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailFrom&Help=cms_HelpNewsletterMailFrom&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMailFrom", "Mail From")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;">
        <asp:textbox runat="server" ID="txtMittente" columns ="50" />                
    </div>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailAlias&Help=cms_HelpNewsletterMailAlias&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMailAlias", "Alias")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;">
        <asp:textbox runat="server" ID="txtAlias" columns ="50" />                
    </div>
    <div style="clear:both;"></div>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterTestDelivery&Help=cms_HelpNewsletterTestDelivery&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterTestDelivery", "Test Delivery")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;width:300px;">
        <asp:checkbox checked="true" ID="chkTest" runat="server" title="Test Send"/>
    </div>
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailSubscribtion&Help=cms_HelpNewsletterMailSubscribtion&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMailSubscribtion", "Use Mail Subscribtion")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;width:300px;">
        <asp:checkbox checked="false" ID="chkssubscribtionMail" runat="server" title="Use Mail Subscribtion"/>
    </div>
     <!--Include Related User -->
     <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailSubscribtion&Help=cms_HelpNewsletterMailSubscribtion&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormNewsletterRelUser", "Include Related User")%>
    </div>
     <div style="margin-left:200px;margin-bottom:10px;width:300px;">
       <asp:checkbox checked="false" ID="chkIncludeRelUser" runat="server" title=""/>
    </div>
   
     <!--end -->
  
    <div style="font-weight:bold;width:180px;float:left;">
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterTestDelivery&Help=cms_HelpNewsletterTestDelivery&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterTraceBy", "Trace by WebService")%>
    </div>
    <div style="margin-left:200px;margin-bottom:10px;width:300px;">
        <asp:checkbox checked="false" ID="chkTraceByWebService" runat="server" title="Trace By WebService"/>
    </div>
    <div style="clear:both;"></div>
</div>

<div id ="pnl_Step3" runat="server" visible="false" style="margin:10px 0 10px 0;width:90%">
    <%--USO QUESTO CAMPO PER TENERE TRACCIA QUALE PANNELLO DI FILTRO L'UTENTE HA APERTO--%>
    <asp:textbox id="txtHidePanelSelection" runat="server" style="display:none"/>
    <%--
        PANNELLO PER LA SELEZIONE DEI DESTINATARI.
        NEL CASO DI INVIO DI TEST, UN ULTERIORE PANNELLO CONSENTE DI INSERIRE LE EMAIL DEI DESTINATARI REALI E IL
        NUMERO TOTALE DI EMAIL DA INVIARE A PRESCINDERE DA QUANTI SONO GLI UTENTI RISULTANTI DALLE DIVERSE
        METODOLOGIE DI FILTRO
    --%>
    <div id ="pnl_Test" runat="server" visible ="false" >
        <%--QUESTO PANNELLO VIENE MOSTRATO SOLO IL CASO DI INVIO DI TEST--%>
        <%--<div style="border:1px solid #000;margin-bottom:10px;padding:4px;">
		    <strong>Attenzione!.</strong> Si ricorda che, in un invio di test, i dati degli utenti vengono 
		    prelevati come specificato dai filtri o dalla query, ma le e-mail vengono inviate ai soli 
		    destinatari indicati nello specifico box. Eventuali click su tali e-mail non verranno tracciati.
	    </div>	--%>
	    <table id="tblTestDest" cellpadding="0" cellspacing="0" style="width:90%;" class="form">
	    <tr>
	        <td style="width:30%">
	            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMaxNumber&Help=cms_HelpNewsletterMaxNumber&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMaxNumber", "Max number to send")%> 
	        </td>
	        <td>
	            <ctl:Text ID="txtMaxEmail" runat ="server" TypeControl="NumericText" Style="width:50px" Text="1"/>
	        </td>
	    </tr>
	    <tr>
	        <td>
	            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterMailRecipients&Help=cms_HelpNewsletterMailRecipients&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterMailRecipients", "Mail Recipients")%> 
	        </td>
	        <td>
	            <ctl:Text ID="txtListaDestinatariTest" runat="server" TypeControl ="TextArea" Style="width:300px"/>
	        </td>
	        <td><asp:Label id="lbMsg"  runat ="server" Text="[* More than one e-mail address could be insert separated with ',' ]" style="color: Gray" ></asp:Label></td>
	    </tr>
	    
	    </table>
    </div>
    <%--
        I PANNELLI CHE SEGUONO VENGONO VISUALIZZATI UNO ALLA VOLTA.
        L'APERTURA DI UNO CHIUDE GLI ALTRI
    --%>
    
    <div style="margin-top:5px">
        <%--CONSENTE DI SELEZIONARE GLI UTENTI PER GRUPPI DI UTENTI
        PER ISCRITTI AD UNA DETERMINATA NEWSLETTER
        E TRAMITE AREA GEOGRAFICA
        --%>        
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNesletterStandardFilter&Help=cms_HelpNesletterStandardFilter&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <a id="aSelectorGeneric" style="font-weight:bold;cursor:pointer;color:Green;" onclick="OpenTargetDiv(this)"><%=getLabel("cms_FormNesletterStandardFilter", "Standard Filter")%> >></a>
        <div id ="pnl_aSelectorGeneric" style="display:none;margin:top2px;">
        <table cellpadding="0" border="0" cellspacing="0" style="width:90%" class="form">  
              
  <!--    <tr>
           <td style="width:30%"><strong>Localization</strong></td>
           <td align="center">
            <%--SEZIONE LEGATA AI FILTRI PER LOCALITA'--%>
            <asp:TextBox ID ="txtGeoId" runat="server" style="display:none"/>
            <asp:TextBox ID ="txtGeoDescr" readonly="true" runat="server" /><asp:button id="btnAssocia" runat="server" CssClass="button" text="Seleziona" onclick="SetGeo"/>
            <asp:button id="btnClear" runat="server" CssClass="button" text="Pulisci" onclick="ClearGeo"/>
            <HP3:Localization id="oLocaliztion" runat="server" />
           </td>
        </tr>-->
        <tr><td style="width:30%" valign="top"></td><td></td></tr>
        <tr>           
            <%--SEZIONE LEGATA AI FILTRI PER ISCRITTI AD UNA DETERMINATA NEWSLETTER--%>
            <td ><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewsletterSubscription&Help=cms_HelpNewsletterSubscription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterSubscription", "Newsletter Subscription")%></td>            
            <td>
                 <asp:listbox ID="lstNewsletterSubscription" runat="server"  style="width:300px" SelectionMode ="Single" />      
            </td>
        </tr>    
        <tr>
            <%--SEZIONE LEGATA AI FILTRI PER GRUPPI DI UTENTI--%>
            <td ><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGroups&Help=cms_HelpNesletterGroups&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGroups", "Groups")%></td>            
            <td>            
                <asp:textbox style="display:none" ID ="txtHiddenGruop" runat="server" Visible="false"/>
                <asp:textbox ID="txtGruppi" TextMode="MultiLine" runat="server"   Rows="4" style="width:300px" Visible="false"/> 
                
                <asp:listbox ID="lstGruppi" runat="server"  style="width:300px; height:200px;"  SelectionMode ="Multiple"/>      
            </td>
               <td> 
                <input style="display:none" type="button" value="Select Groups" class="button" onclick="window.open('<%=strDomain%>/Popups/popUserGroupSelectio.aspx?ListUser='+ReadListUser(),','width=600,height=500,scrollbars=yes,status=no')" /> 
                <input style="margin-top:5px;display:none" type="button"  value="Delete" class="button" onclick="document.getElementById ('<%=txtGruppi.clientid%>').value = '';document.getElementById ('<%=txtHiddenGruop.clientid%>').value ='';CheckForTarget();" />
               </td>
            </tr> 
       </table>
       </div> 
       <div>&nbsp;</div>
    </div>         
                
    <div style="margin-top:5px">
        <%--CONSENTE DI SELEZIONARE GLI UTENTI TRAMITE QUERY--%>        
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNesletterQueries&Help=cms_HelpNesletterQueries&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <a id="aSelectorQuery" style="font-weight:bold;cursor:pointer;color:Green" onclick="OpenTargetDiv(this)" ><%=getLabel("cms_FormNesletterQueries", "Queries")%> >></a>
        <div id ="pnl_aSelectorQuery" style="display:none;margin-top:2px;">
        <table cellpadding="0" border="0" cellspacing="0" style="width:90%" class="form">
        <tr>
            <td style="width:30%">&nbsp;</td>
            <td> <ctl:Text ID="txtQuery" runat="server" TypeControl ="TextArea" Style="width:300px" /> </td>
            <td><input type="button" value="Select Query" class="button" onclick="window.open('<%=strDomain%>/Popups/popSelectQueryNewsletter.aspx?Test=<%=iif(ReadNewsletterSendField("IsTest"),1,0)%>&idNL=<%=ReadNewsletterSendField("KeyNewsletter.KeyNewsletter")%>&Type=SHOW&TypeQ=<%=NewsletterQueriesSearcher.TypeQuery.ListToSend%>','','width=600,height=500,scrollbars=yes,status=no')" /></td>
        </tr>
       </table>
        </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
    </div>
    <div>&nbsp;</div>

    <div id="div_SelectorRelUserQuery" runat="server"  visible="false" style="margin-top:5px" >
        <%--CONSENTE DI SELEZIONARE GLI UTENTI RELAZIONATI AI DESTINATARI DELLA NEWSLETTER TRAMITE QUERY--%>        
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNesletterQueries&Help=cms_HelpNesletterQueries&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <a id="aSelectorRelUserQuery" style="font-weight:bold;cursor:pointer;color:Green" onclick="OpenTargetDiv(this)" ><%= getLabel("cms_FormNesletterSelRelUser", "Select Related User")%> >></a>
        <div id ="pnl_aSelectorRelUserQuery" style="display:none;margin-top:2px;">
        <table cellpadding="0" border="0" cellspacing="0" style="width:90%" class="form">
        <tr>
            <td style="width:30%">&nbsp;</td>
            <td> <ctl:Text ID="txtRelUserQuery" runat="server" TypeControl ="TextArea" Style="width:300px" /> </td>
            <td><input type="button" value="Select Query" class="button" onclick="window.open('<%=strDomain%>/Popups/popSelectRelUserQueryNewsletter.aspx?Test=<%=iif(ReadNewsletterSendField("IsTest"),1,0)%>&idNL=<%=ReadNewsletterSendField("KeyNewsletter.KeyNewsletter")%>&Type=SHOW&TypeQ=<%=NewsletterQueriesSearcher.TypeQuery.ListRelatedUser%>','','width=600,height=500,scrollbars=yes,status=no')" /></td>
        </tr>
       </table>
        </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
    </div>
    <div>&nbsp;</div>
</div>
<div>&nbsp;</div>

<div id ="pnl_Step4" runat="server" visible="false" style="margin:10px 0 10px 10px;width:89%">
    <%--INVIO DELLA NEWSLETTER E RIEPILOGO DEGLI UTENTI SELEZIONATI--%>    
    <div runat="server" id="divSend">
        <%--Bottone per l'invio della newsletter--%>
        <input type="button" value="View selected users" class="button" onclick="window.open('<%=strDomain%>/Popups/popSelectQueryNewsletter.aspx?Test=<%=iif(ReadNewsletterSendField("IsTest"),1,0)%>&idNL=<%=ReadNewsletterSendField("KeyNewsletter.KeyNewsletter")%>&Type=TESTSQL','','width=600,height=500,scrollbars=yes,status=no')" />
        <input type="button" value="Send newsletter" class="button" onclick="window.open('<%=strDomain%>/Popups/popSendingNewsletter.aspx?webservice=<%=chkTraceByWebService.checked%>&alias=<%=txtAlias.Text%>','','width=500,height=50,scrollbars=no,status=no,resizable=no,menubar=no,location=no')" />
    </div>
</div>
<div>&nbsp;</div>
<div>
    <%--BOTTONI PER LA NAVIGAZIONE ATTRAVERSO GLI STEP--%>
    <asp:ImageButton onclick ="GoBack" STYLE="float:left" Width="24" ImageUrl ="~/hp3Office/HP3Image/ico/back.gif" ID="btnPrevious" runat="Server" title="Go previous step" CssClass="button"/>
    <asp:ImageButton onclick ="GoNext" STYLE="float:right" Width="24" ImageUrl ="~/hp3Office/HP3Image/ico/forward.gif" ID="btnNext" runat="Server" title="Go next step" CssClass="button" Visible="true"/>
</div>
<div style="clear:both">&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<%--FUNZIONI JAVASCRIPT--%>
<script type="text/javascript" >
     //variabile usata per controllare se abilitare o meno il pulsante next nel terzo step
     var enableNextTargetSelection
     var selRadio
     <%=strJs%>
     //Gestione dei click sui radio button
     function onRadioClick(srcRadio,idNewsletter)
        {       
       
            if (selRadio != null) selRadio.checked = false;
                
            var txtHidden = document.getElementById ('<%=txtSelectedNews.clientid%>')
            if (txtHidden != null)
                {
                   txtHidden.value = idNewsletter; 
                   selRadio=srcRadio;
                }
            
            srcRadio.checked=true;
            document.getElementById('<%=btnNext.clientid%>').style.display='';
        }
        
     //Pressione di qualsiasi tasto nella casella di testo mittente
     function CheckMittente()
        {           
            var txtMittente = document.getElementById ('<%=txtMittente.clientid%>')
            var bntNext = document.getElementById ('<%=btnNext.clientid%>')
            
            if ((txtMittente!=null) && (bntNext!=null))
                {                    
                    //devo controllare se si tratta di un indirizzo email valido
                    if (ValidMail(txtMittente))
                        //Il campo � vuoto e spengo il bottone
                        bntNext.style.display = '';                          
                    else                                            
                        bntNext.style.display = 'none';
                        
                }
        }
        
     //Gestisce l'apertura dei pannelli contenenti i diversi criteri di filtro
     function OpenTargetDiv(srcAnchor)
        {      
            //prelevo il div contenitore
            var Container = document.getElementById('<%=pnl_Step3.clientid%>')
            if (Container != null)
            {
                //recupero la lista delle ancore
                var anchorList = Container.getElementsByTagName('a')               
                for (i=0;i<anchorList.length;i++)
                {
                    
                    //Se l'ancora � relativa ai pannelli
                    if (anchorList[i].id.indexOf('aSelector') >= 0)                        
                        //Se l'elemento corrente � diverso da srcAnchor
                        //procedo con l'operazione                   
                        if (srcAnchor.id != anchorList[i].id)
                            {
                                //Chiudo gli altri pannelli
                                CloseTargetDiv(document.getElementById (anchorList[i].id));                          
                            }
                }
                
            }
                       
            //Rendo non cliccabile il link corrente
            srcAnchor.style.cursor='default';
            
            //apro il pannello
            document.getElementById ('pnl_' + srcAnchor.id).style.display='';
            
            //Tengo traccia del pannello aperto
            document.getElementById ('<%=txtHidePanelSelection.clientid%>').value = srcAnchor.id;
            
            //Contrllo se abilitare o meno il pulsante next
            CheckForTarget()
        }
        
     //Gestisce la chiusura dei pannelli contenenti i diversi criteri di filtro
     function CloseTargetDiv(srcAnchor)
        {
       
            //Rendo non cliccabile il link corrente
            srcAnchor.style.cursor='pointer';
            
            //apro il pannello
            document.getElementById ('pnl_' + srcAnchor.id).style.display='none';
        }
     
     //Leggo dal campo hidden il pannello aperto
     function GetIntForOpenTargetPanel()
        {       
            var txtSelection = document.getElementById ('<%=txtHidePanelSelection.clientid%>')
            switch (txtSelection.value)
                {
                    //FILTRO GENERICO
                    case 'aSelectorGeneric':                        
                        return 1
                        break;                                           
                        
                    //FILTRO PER QUERY
                    case 'aSelectorQuery':
                        return 3
                        break;                   
                        
                    //NESSUN PANNELLO E' STATO APERTO
                    default :    
                        return 0
                        break;                
                }
        }
        
     //Controllo l'abilitazione del tasto next sulla base della valorizzazione della form 
     //del terzo step
     function CheckForTarget()
        {       
            var bTest =<%=IsTest()%>
                  
            var bRet=true;
            var iCase = GetIntForOpenTargetPanel()
           
            //Controllo l' esistenza del pannello relativo ai test
            //var oTestPanel =document.getElementById ('<%=pnl_Test.clientid%>');
            if (bTest == 0)
                {
                    //non siamo in un invio di test
                    bRet=true;
                }
            else
                {
                    //siamo in un invio di test
                    //devo controllare i campi del pannello                   
                    var txtMaxEmail = document.getElementById ('<%=txtMaxEmail.clientid%>')
                    var txtListaDestinatariTest = document.getElementById ('<%=txtListaDestinatariTest.clientid%>')
                    bRet = (bRet && (txtMaxEmail.value != '') && (txtListaDestinatariTest.value != ''));
                
                }
           
            //Controllo il pannello aperto 
            switch (parseInt(iCase))
                {
                    //FILTRO GENERICO
                    case 2:    
                        //Gestione Di GEO
                        var txt =document.getElementById('<%=txtGeoId.clientid%>')                   
                        bRet =  bRet && (txt.value != '')                        
                        /////////////////////////////////////////////////////////
                        //Gestione degli inscritti alle newsletter                        
                        var lst = document.getElementById('<%=lstNewsletterSubscription.clientid%>')                        
                        bRet =  bRet ||  (lst.value!='')
                        /////////////////////////////////////////////////////////
                        //Filtro su gruppi di utenti
                        var txtGruppi = document.getElementById('<%=txtgruppi.clientid%>')                        
                        bRet =  bRet ||  (txtGruppi.value!='')
                        /////////////////////////////////////////////////////////
                        break;                                            
                        
                    //FILTRO PER QUERY
                    case 3:
                        var txtQuery = document.getElementById ('<%=txtQuery.clientid%>');                        
                        bRet = bRet && (txtQuery.value != '')
                        break;
                            
                    //NESSUN PANNELLO E' STATO APERTO
                    case 0:    
                        bRet=false;
                        break;                
                }
                
             var bntNext = document.getElementById ('<%=btnNext.clientid%>');
                       
             if (bRet)
                bntNext.style.display = '';
             else
                bntNext.style.display = 'none';
                
        }
        
        //Funzione chiamata dal popup per la selezione delle query
        //valorizza il campo Query della form
        function setQuery(SqlQuery)
            {
                var txtQuery = document.getElementById ('<%=txtQuery.clientid%>')
                txtQuery.value=SqlQuery;
                
                //Chiamo esplicitamente l'evento KeyUp sulla textarea
                CheckForTarget();
            }

        //Funzione chiamata dal popup per la selezione delle query
        //valorizza il campo Query della form
        function setRelQuery(SqlQuery)
            {
                var txtRelUserQuery = document.getElementById ('<%=txtRelUserQuery.clientid%>')
                txtRelUserQuery.value=SqlQuery;
                
                //Chiamo esplicitamente l'evento KeyUp sulla textarea
                CheckForTarget();
            }
            
        //Funzione chiamata dal popUp per la selezione dei gruppi
        function WriteSelectedUsers(listUser,listUserId)
            {
                
                document.getElementById ('<%=txtGruppi.clientid%>').value = listUser//OutString
                document.getElementById ('<%=txtHiddenGruop.clientid%>').value = listUserId//OutId
                
                //Avvio il controllo per l'abilitazione del tasto next
                CheckForTarget();
            }
        
       //Restituisce la lista degli degli utenti selezionati tramite il popUp dei gruppi             
       function ReadListUser()
        {
             return document.getElementById ('<%=txtHiddenGruop.clientid%>').value
        }
</script>