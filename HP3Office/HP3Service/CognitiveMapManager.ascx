<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlProfiles" Src ="~/hp3Office/HP3Parts/ctlProfilesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlContentEditorial" Src ="~/hp3Office/HP3Parts/ctlContentEditorialList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<%@ Register tagPrefix="FCKeditorV2" namespace="FredCK.FCKeditorV2" assembly="FredCK.FCKeditorV2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    
    Private CatalogManager As New CatalogManager
    Private UserGroupManager As New UserGroupManager
    Private utility As New WebUtility
    Private LangManager As New LanguageManager
    
    Private Property SelectedId() As Integer
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Public Property NodePk() As Integer
        Get
            Return ViewState("NodePk")
        End Get
        Set(ByVal value As Int32)
            ViewState("NodePk") = value
        End Set
    End Property
    
    Public Property ContentId() As Integer
        Get
            Return ViewState("contentId")
        End Get
        Set(ByVal value As Integer)
            ViewState("contentId") = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        BindContentType(srcContentType)
        
        
        If Not (Page.IsPostBack) Then
            srcArea.LoadControl()
            
            BindCourses()
            LoadNodes()
            LoadRootNodes()
        End If
    End Sub
    
    Sub LoadNodes()
        'Dim catalogSearcher As New CatalogSearcher
        'catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Node
        'catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
        
        'catalogSearcher.Active = SelectOperation.All
        'catalogSearcher.Display = SelectOperation.All
        
        'CatalogManager.Cache = False
        'Dim nodeColl As NodeCollection = CatalogManager.ReadNodes(catalogSearcher)
        
        'gdw_Nodes.DataSource = nodeColl
        'gdw_Nodes.DataBind()
        If Not (Request("S") Is Nothing) Then
            BindWithSearch(gdw_Nodes)
        End If
        
    End Sub
    
    Private Sub LoadRootNodes()
       
        RadTreeCourse.Nodes.Clear()
        Dim CatSearcher As New CatalogSearcher
       
        CatalogManager.Cache = False
        'CatSearcher.KeySiteArea.Id = 
        CatSearcher.KeySite.Id = Request("S")
        CatSearcher.key.IdLanguage = srcNodeLang.Language
        CatSearcher.Active = SelectOperation.All
        CatSearcher.Display = SelectOperation.All
        
        CatalogManager.Cache = False
        Dim CoursesColl As CourseCollection = CatalogManager.ReadCourses(CatSearcher)
        
        Dim courseValue As New CourseValue
        If Not (CoursesColl Is Nothing) AndAlso (CoursesColl.Count > 0) Then
            For Each courseValue In CoursesColl
                Dim node As New RadTreeNode()
                If Not (courseValue Is Nothing) Then
                    node.Text = courseValue.Title
                    node.Value = courseValue.Key.PrimaryKey
                    node.Attributes.Add("Less", "Lesson")
                
                    If HasChilds(courseValue.Key.PrimaryKey) Then
                        node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                        node.Expanded = True
                    End If
                
                    RadTreeCourse.Nodes.Add(node)
                End If
            Next
        End If
    End Sub
    
    Function HasChilds(ByVal pk As Integer) As Boolean
        Dim catalogSearcher As New CatalogSearcher
        catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
        catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
        
        If (pk <> 0) Then
            catalogSearcher.key.PrimaryKey = pk
            catalogSearcher.KeySite.Id = Request("S")
            catalogSearcher.key.IdLanguage = srcNodeLang.Language
            catalogSearcher.Active = SelectOperation.All
            catalogSearcher.Display = SelectOperation.All
            
            CatalogManager.Cache = False
            Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
            If Not (catColl Is Nothing) AndAlso catColl.Count > 0 Then
                Return True
            End If
        End If
               
        Return False
    End Function
    
    Sub Refresh(ByVal sender As Object, ByVal e As EventArgs)
        LoadRootNodes()
    End Sub
        
    Sub ClickForEdit(ByVal sender As Object, ByVal e As EventArgs)
        If (sender.CommandName = "EditNode") Then
            btn_edit.Visible = False
            Dim relId As Integer = sender.CommandArgument
             
            EditRelation(relId)
        End If
    End Sub
    
    Sub EditRelation(ByVal relationId As Integer)
        SelectedId = relationId
        Dim NodeRelIdent As New NodeRelationIdentificator
        NodeRelIdent.Id = relationId
            
        Dim nodeRelSearcher As New NodeRelationSearcher
        nodeRelSearcher.Key = NodeRelIdent
        CatalogManager.Cache = False
        Dim NodeRelValue As NodeRelationValue = CatalogManager.Read(NodeRelIdent)
            
           
        If Not (NodeRelValue Is Nothing) Then
            h2Title.InnerText = "Edit relation " & getTitle(NodeRelValue.KeyStartNode.PrimaryKey) & " - " & getTitle(NodeRelValue.KeyEndNode.PrimaryKey)
            LoadRelationValue(NodeRelValue)
            ActivePanelDett()
        End If
    End Sub
    
    Sub ClickForAdd(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs) Handles RadTreeCourse.NodeClick
        If (e.Node.Value <> 0) Then
            
            If (Not e.Node.Attributes("Chap") Is Nothing) AndAlso (e.Node.Attributes("Chap").ToString = "Chapter") Then
                NewRelation()
                txtHiddenLesson.Text = e.Node.Value
                txtLesson.Text = getTitle(e.Node.Value)
                
                h2Title.InnerText = "Add a new child to " & txtLesson.Text
                btn_edit.Visible = False
                
                isStartRel.Checked = True
            End If
           
            If (Not e.Node.Attributes("Par") Is Nothing) AndAlso (e.Node.Attributes("Par").ToString = "Paragraph") Then
                'il 4 livello � quello dei sottoparagrafi
                'ai quali non � possibile aggiungere nodi
                If (e.Node.Level < 4) Then
                    NewRelation()
                    
                    btn_edit.CommandArgument = e.Node.Attributes("Idrel")
                    btn_edit.CommandName = "EditNode"
                    btn_edit.Visible = True
                    
                    txtHiddenStartNode.Text = e.Node.Value
                    txtStartNode.Text = getTitle(e.Node.Value)
                    txtHiddenLesson.Text = e.Node.Attributes("LesId")
                    txtLesson.Text = getTitle(e.Node.Attributes("LesId"))
                    h2Title.InnerText = "Add a new child to " & txtStartNode.Text
                    isStartRel.Checked = False
                End If
                
                If (e.Node.Level = 4) Then
                    btn_edit.Visible = False
                    EditRelation(e.Node.Attributes("Idrel"))
                    
                End If
            End If
        End If
    End Sub
    
    
    Protected Sub RadTreeView1_NodeExpand(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs) Handles RadTreeCourse.NodeExpand
        e.Node.Nodes.Clear()
        
        If (e.Node.Value <> 0) Then
            
            'Chapter Lesson
            If (Not e.Node.Attributes("Less") Is Nothing) AndAlso (e.Node.Attributes("Less").ToString = "Lesson") Then
                Dim catalogSearcher As New CatalogSearcher
                catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
                catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
                catalogSearcher.key.PrimaryKey = e.Node.Value
                'CatSearcher.KeySiteArea.Id = 
                catalogSearcher.KeySite.Id = Request("S")
                catalogSearcher.key.IdLanguage = srcNodeLang.Language
                catalogSearcher.Active = SelectOperation.All
                catalogSearcher.Display = SelectOperation.All
                
                CatalogManager.Cache = False
                Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
            
                If catColl.Count > 0 Then
                    
                    Dim catalogValue As New CatalogValue
                    For Each catalogValue In catColl
                        Dim node As New RadTreeNode()
                        node.Text = catalogValue.Title
                        node.Value = catalogValue.Key.PrimaryKey
                        node.Attributes.Add("Chap", "Chapter")
                        
                        If HasChapters(catalogValue.Key.PrimaryKey) Then
                            node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                            e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                            e.Node.Expanded = True
                        End If
                        
                        e.Node.Nodes.Add(node)
                    Next
                End If
            
            End If
       
            'Case "Chapter"
            If (Not e.Node.Attributes("Chap") Is Nothing) AndAlso (e.Node.Attributes("Chap").ToString = "Chapter") Then
                Dim MapNodesSearcher As New NodeRelationSearcher
                CatalogManager.Cache = False
        
                MapNodesSearcher.KeyLesson.PrimaryKey = e.Node.Value
                'MapNodesSearcher.IsStartRelation = True
                MapNodesSearcher.TypeRelation = NodeRelationSearcher.TypeRel.StartRelation
                
                Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
            
                If nodesRelColl.Count > 0 Then
                    Dim NoderelValue As New NodeRelationValue
                    For Each NoderelValue In nodesRelColl
                           
                        Dim node As New RadTreeNode()
                        Dim capValue As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(NoderelValue.KeyStartNode.PrimaryKey))
                      
                        If Not (capValue Is Nothing) Then
                            node.Text = capValue.Key.Id & " - " & capValue.Title
                            node.Value = NoderelValue.KeyEndNode.PrimaryKey
                            node.Attributes.Add("Par", "Paragraph")
                            node.Attributes.Add("LesId", NoderelValue.KeyLesson.PrimaryKey)
                            node.Attributes.Add("Idrel", NoderelValue.Key.Id)
                            
                            If HasParagraphs(NoderelValue.KeyEndNode.PrimaryKey, NoderelValue.KeyLesson.PrimaryKey) Then
                                node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                                e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                                e.Node.Expanded = True
                            End If
                            
                            e.Node.Nodes.Add(node)
                        End If
                    Next
                End If
            End If
                        
            'Case "Paragraph"
            If (Not e.Node.Attributes("Par") Is Nothing) AndAlso (e.Node.Attributes("Par").ToString = "Paragraph") Then
                Dim MapNodesSearcher As New NodeRelationSearcher
                CatalogManager.Cache = False
        
                MapNodesSearcher.KeyLesson.PrimaryKey = e.Node.Attributes("LesId")
                MapNodesSearcher.KeyStartNode.PrimaryKey = e.Node.Value
                'MapNodesSearcher.IsStartRelation = False
                MapNodesSearcher.TypeRelation = NodeRelationSearcher.TypeRel.NotStartRelation
                
                Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
            
                If nodesRelColl.Count > 0 Then
                    Dim NoderelValue As New NodeRelationValue
                    For Each NoderelValue In nodesRelColl
    
                        Dim node As New RadTreeNode()
                        Dim capValue As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(NoderelValue.KeyEndNode.PrimaryKey))
                        
                        If Not (capValue Is Nothing) Then
                            node.Text = capValue.Key.Id & " - " & capValue.Title
                            node.Value = NoderelValue.KeyEndNode.PrimaryKey
                            node.Attributes.Add("Par", "Paragraph")
                            node.Attributes.Add("LesId", NoderelValue.KeyLesson.PrimaryKey)
                            node.Attributes.Add("Idrel", NoderelValue.Key.Id)
                            If HasParagraphs(NoderelValue.KeyEndNode.PrimaryKey, NoderelValue.KeyLesson.PrimaryKey) Then
                                node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack
                                e.Node.ExpandMode = TreeNodeExpandMode.ClientSide
                                e.Node.Expanded = True
                            End If
                            
                            e.Node.Nodes.Add(node)
                        End If
                    Next
                End If
                'End Select
            End If
       
        End If
    End Sub
    
    Function HasParagraphs(ByVal pknode As Integer, ByVal lesson As Integer) As Boolean
        Dim MapNodesSearcher As New NodeRelationSearcher
        CatalogManager.Cache = False
        
        MapNodesSearcher.KeyLesson.PrimaryKey = lesson
        MapNodesSearcher.KeyStartNode.PrimaryKey = pknode
        'MapNodesSearcher.IsStartRelation = False
        MapNodesSearcher.TypeRelation = NodeRelationSearcher.TypeRel.NotStartRelation
        
        Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
        If Not (nodesRelColl Is Nothing) AndAlso (nodesRelColl.Count > 0) Then
            Return True
        End If
        Return False
    End Function
    
    Function HasChapters(ByVal pkless As Integer) As Boolean
        Dim MapNodesSearcher As New NodeRelationSearcher
        CatalogManager.Cache = False
        
        MapNodesSearcher.KeyLesson.PrimaryKey = pkless
        'MapNodesSearcher.IsStartRelation = True
        MapNodesSearcher.TypeRelation = NodeRelationSearcher.TypeRel.StartRelation
        
        Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
        If Not (nodesRelColl Is Nothing) AndAlso (nodesRelColl.Count > 0) Then
            Return True
        End If
        Return False
    End Function
          
    'fa il bind dei corsi
    Sub BindCourses()
        Dim catSearcher As New CatalogSearcher
        catSearcher.Active = SelectOperation.All
        catSearcher.Display = SelectOperation.All
        catSearcher.KeySite.Id = Request("S")
        catSearcher.key.IdLanguage = srcNodeLang.Language
        
        CatalogManager.Cache = False
        Dim courseColl As CourseCollection = CatalogManager.ReadCourses(catSearcher)
        
        dwlCourse.Items.Clear()
        If Not (courseColl Is Nothing) AndAlso (courseColl.Count > 0) Then
            utility.LoadListControl(dwlCourse, courseColl, "Title", "Key.PrimaryKey")
            dwlCourse.Items.Insert(0, New ListItem("--Select--", 0))
        End If
    End Sub
    
    
    Sub BindLessons(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim catSearcher As CatalogSearcher
        
        dwlLesson.Items.Clear()
        dwlStartNode.Items.Clear()
        dwlEndNode.Items.Clear()
        
        If (dwlCourse.SelectedValue <> 0) Then
            catSearcher = New CatalogSearcher
            catSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
            catSearcher.IdContentSubType = ContentValue.Subtypes.Course
            catSearcher.key.PrimaryKey = dwlCourse.SelectedValue
            catSearcher.Active = SelectOperation.All
            catSearcher.Display = SelectOperation.All
            
            CatalogManager.Cache = False
            Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catSearcher)
            If Not (catColl Is Nothing) AndAlso (catColl.Count > 0) Then
                utility.LoadListControl(dwlLesson, catColl, "Title", "Key.PrimaryKey")
                dwlLesson.Items.Insert(0, New ListItem("--Select--", 0))
            End If
        End If
    End Sub
    
    'fa il bind dei nodi
    Sub BindNodes(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim catSearcher As CatalogSearcher
        Dim MapNodesSearcher As New NodeRelationSearcher
           
        dwlStartNode.Items.Clear()
        dwlEndNode.Items.Clear()
        If (dwlLesson.SelectedValue <> 0) Then
            Dim listNodeColl As New CatalogCollection
            
            MapNodesSearcher = New NodeRelationSearcher
            MapNodesSearcher.KeyLesson.PrimaryKey = dwlLesson.SelectedValue
            CatalogManager.Cache = False
            Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
            
            If Not (nodesRelColl Is Nothing) AndAlso (nodesRelColl.Count > 0) Then
                For Each rel As NodeRelationValue In nodesRelColl
                    catSearcher = New CatalogSearcher
                    catSearcher.key.PrimaryKey = rel.KeyEndNode.PrimaryKey
                    catSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Node
                    catSearcher.IdContentSubType = ContentValue.Subtypes.Course
                    CatalogManager.Cache = False
                    Dim nodeC As NodeCollection = CatalogManager.ReadNodes(catSearcher)
                    
                    If Not (nodeC Is Nothing) AndAlso (nodeC.Count > 0) Then
                        listNodeColl.Add(nodeC(0))
                    End If
                Next
            End If
            
            
            'popola le dwl 
            If Not (listNodeColl Is Nothing) AndAlso (listNodeColl.Count > 0) Then
                utility.LoadListControl(dwlStartNode, listNodeColl, "Title", "Key.PrimaryKey")
                dwlStartNode.Items.Insert(0, New ListItem("--Select--", 0))
                
                utility.LoadListControl(dwlEndNode, listNodeColl, "Title", "Key.PrimaryKey")
                dwlEndNode.Items.Insert(0, New ListItem("--Select--", 0))
            End If
        End If
    End Sub
    
    'recupera la lista delle relazioni che compongono la mappa cognitiva
    Sub LoadNodeRelation()
        Dim MapNodesSearcher As New NodeRelationSearcher
        CatalogManager.Cache = False
        
        Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
        
        gdw_NodesRelation.DataSource = nodesRelColl
        gdw_NodesRelation.DataBind()
        
        ActivePanelGrid()
    End Sub
             
    'cancellazione di una relazione nella mappa cognitiva
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim itemId As Integer
        
        If sender.CommandName = "DeleteItem" Then
            itemId = sender.commandArgument()
            
            CatalogManager.Remove(New NodeRelationIdentificator(itemId))
            'LoadNodeRelation()
            BindWithSearch(gdw_NodesRelation)
            ActivePanelGrid()
        End If
    End Sub
    
    'gestisce l'edit  della relazione
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim NodeRelIdent As New NodeRelationIdentificator
        Dim NodeRelSearcher As New NodeRelationSearcher
        
        Dim itemId As Integer
        h2Title.InnerText = "Edit relation"
        btn_edit.Visible = False
        
        If sender.CommandName = "EditItem" Then
            itemId = sender.commandArgument()
            
            SelectedId = itemId
            NodeRelIdent.Id = itemId
            NodeRelSearcher.Key = NodeRelIdent
            Dim NodeRelValue As NodeRelationValue = CatalogManager.Read(NodeRelIdent)
            
            If Not (NodeRelValue Is Nothing) Then
                LoadRelationValue(NodeRelValue)
                ActivePanelDett()
            End If
        End If
    End Sub
      
    'rimanda alla lista delle classi virtuali
    Sub LoadArchive(ByVal s As Object, ByVal e As EventArgs)
        'LoadNodeRelation()
        BindWithSearch(gdw_NodesRelation)
        ActivePanelGrid()
    End Sub
     
    'gestisce la creazione di una nuova classe virtuale
    Sub NewRow(ByVal s As Object, ByVal e As EventArgs)
        NewRelation()
    End Sub
    
    Sub NewRelation()
        SelectedId = 0
        
        LoadRelationValue(Nothing)
        ActivePanelDett()
        h2Title.InnerText = "New relation"
        btn_edit.Visible = False
    End Sub
    
    'carica i dati nel form
    Sub LoadRelationValue(ByVal noderel As NodeRelationValue)
        If Not (noderel Is Nothing) Then
            txtHiddenStartNode.Text = noderel.KeyStartNode.PrimaryKey
            txtStartNode.Text = getTitle(noderel.KeyStartNode.PrimaryKey)
            
            txtHiddenEndNode.Text = noderel.KeyEndNode.PrimaryKey
            txtEndNode.Text = getTitle(noderel.KeyEndNode.PrimaryKey)
                       
            txtHiddenLesson.Text = noderel.KeyLesson.PrimaryKey
            txtLesson.Text = getTitle(noderel.KeyLesson.PrimaryKey)
            
            txtRelDescription.Text = noderel.Description
            lOrder.Text = CType(noderel.Order, String)
            
            If (noderel.TypeRelation = NodeRelationValue.TypeRel.StartRelation) Then
                isStartRel.Checked = True
            Else
                isStartRel.Checked = False
            End If
            btnUpdate.Text = "Update"
        Else
            
            btnUpdate.Text = "Save"
            txtHiddenStartNode.Text = 0
            txtStartNode.Text = Nothing
            txtHiddenEndNode.Text = 0
            txtEndNode.Text = Nothing
            txtRelDescription.Text = Nothing
            txtHiddenLesson.Text = 0
            txtLesson.Text = Nothing
            lOrder.Text = 0
            isStartRel.Checked = False
        End If
    End Sub
    
    'gestisce la modifica/salvataggio di una relazione della mappa cognitiva
    Sub UpdateRow(ByVal s As Object, ByVal e As EventArgs)
        Dim NRelValue As New NodeRelationValue
       
        NRelValue.KeyStartNode.PrimaryKey = txtHiddenStartNode.Text
        NRelValue.KeyEndNode.PrimaryKey = txtHiddenEndNode.Text
        NRelValue.KeyLesson.PrimaryKey = txtHiddenLesson.Text
        NRelValue.Description = txtRelDescription.Text
        
        NRelValue.Order = CType(lOrder.Text, Integer)
        
        If (isStartRel.Checked) Then
            NRelValue.TypeRelation = NodeRelationValue.TypeRel.StartRelation
        End If
                
        If (SelectedId <> 0) Then
            NRelValue.Key.Id = SelectedId
            NRelValue = CatalogManager.Update(NRelValue)
        Else
            NRelValue = CatalogManager.Create(NRelValue)
        End If
        
        'LoadNodeRelation()
        BindWithSearch(gdw_NodesRelation)
        ActivePanelGrid()
    End Sub
   
    Sub NewNode(ByVal s As Object, ByVal e As EventArgs)
        NodePk = 0
        LoadNode(Nothing)
        
        pnlGrid.Visible = False
        pnlNodeGrid.Visible = False
        pnlNodeDett.Visible = True
    End Sub
    
    Sub SaveNode(ByVal sender As Object, ByVal e As EventArgs)
        Dim node As New NodeValue
        node.Title = Title.Text
        node.SubTitle = SubTitle.Text
        node.Abstract = Abstract.Content
        node.FullText = FullText.Content
        
        'gestione della lingua
        node.Key.IdLanguage = lSelector.Language
               
        'date
        node.DateCreate = DateUpdate.Value
        node.DateUpdate = DateUpdate.Value
        node.DatePublish = DatePublish.Value
                  
        node.ContentType.Key.Id = txtHiddenContentType.Text
       
        node.Objectives = Objectives.Text
        node.Duration = Duration.Text
        node.MinTime = MinTime.Text
        node.Active = chkActive.Checked
        node.Display = chkDisplay.Checked
            
        Select Case dwlClassNode.SelectedValue
            Case NodeValue.Classes.Argument
                node.ClassNode = NodeValue.Classes.Argument
            Case NodeValue.Classes.Bibliografic
                node.ClassNode = NodeValue.Classes.Bibliografic
            Case NodeValue.Classes.Chapter
                node.ClassNode = NodeValue.Classes.Chapter
            Case NodeValue.Classes.Paragraph
                node.ClassNode = NodeValue.Classes.Paragraph
        End Select
        
        'ApprovalStatus
        Select Case dwlApprovalStatus.SelectedItem.Value
            Case NodeValue.ApprovalWFStatus.Approved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.Approved
            Case ContentValue.ApprovalWFStatus.ToBeApproved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.ToBeApproved
            Case ContentValue.ApprovalWFStatus.NotApproved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.NotApproved
        End Select
        If (NodePk = 0) Then
            Dim NNode As NodeValue = CatalogManager.Create(node)
            
            If Not (NNode Is Nothing) AndAlso (NNode.Key.PrimaryKey > 0) Then
                '    'gestione associazione lezione/nodo
                '    Dim CatalogRelV As New CatalogRelationValue
                '    CatalogRelV.KeyContent.PrimaryKey = LessonPk
                '    CatalogRelV.KeyContentRelation.PrimaryKey = NNode.Key.PrimaryKey
                '    CatalogManager.CreateCatalogRelation(CatalogRelV)
                
                'gestione associazione sitearee/content
                SaveSiteArea(NNode.Key.Id, NNode.Key.IdLanguage)
            End If
        Else
            node.Key.PrimaryKey = NodePk
            CatalogManager.Update(node)
            
            'gestione associazione sitearee/content
            SaveSiteArea(ContentId, node.Key.IdLanguage)
        End If
               
        LoadNodes()
        ActiveAllArchive()
    End Sub
    
    Sub EditNode(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SelectNode") Then
            Dim argument As String = sender.CommandArgument()
            Dim keys As String() = argument.Split("|")
            Dim PK As Integer = keys(0)
            Dim ID As Integer = keys(1)
           
            NodePk = PK
            ContentId = ID
            
            CatalogManager.Cache = False
            Dim so As New CatalogSearcher
            so.key.PrimaryKey = PK
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All
        
            Dim nodes As NodeCollection = CatalogManager.ReadNodes(so)
            If Not (nodes Is Nothing) AndAlso (nodes.Count > 0) Then
                LoadNode(nodes(0))
                'ActiveNode()
                pnlGrid.Visible = False
                pnlNodeGrid.Visible = False
        
                pnlNodeDett.Visible = True
            End If
        End If
    End Sub
    
    'gestisce la cancellazione del nodo
    Sub DeleteNode(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "DeleteNode") Then
            Dim PK As String = sender.CommandArgument
            
            Dim bool As Boolean = CatalogManager.Delete(New CatalogIdentificator(PK))
            If (bool) Then
                CreateJsMessage("This node has been cancelled correctly.")
            Else
                CreateJsMessage("Impossible to delete this node.")
            End If
            
            LoadNodes()
            'ActiveAllArchive()
        End If
    End Sub
    
    Sub LoadNode(ByVal node As NodeValue)
        If Not (node Is Nothing) Then
            'carica i dati della lezione
            NodePk = node.Key.PrimaryKey
            ' ApprovalStatus 
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(node.ApprovalStatus))
            Title.Text = node.Title
            SubTitle.Text = node.SubTitle
            'Abstract.Value = node.Abstract
            Abstract.Content = node.Abstract
            'FullText.Value = node.FullText
            FullText.Content = node.FullText
            
            'gestione della lingua
            lSelector.LoadLanguageValue(node.Key.IdLanguage)
            
            'gestione aree
            Aree.GenericCollection = GetSiteArea(node.Key.Id, node.Key.IdLanguage)
            Aree.IsNew = False
            Aree.LoadControl()
                               
            'gestione siti
            oSite.GenericCollection = GetSite(node.Key.Id, node.Key.IdLanguage)
            oSite.IsNew = False
            oSite.LoadControl()
            
            'Caricamento delle date
            If node.DateCreate.HasValue Then
                DateCreate.Value = node.DateCreate.Value
            Else
                DateCreate.Clear()
            End If
                                    
            If node.DatePublish.HasValue Then
                DatePublish.Value = node.DatePublish.Value
            Else
                DatePublish.Clear()
            End If
            
            txtHiddenContentType.Text = node.ContentType.Key.Id
            txtContentType.Text = node.ContentType.Description
            Objectives.Text = node.Objectives
            Duration.Text = node.Duration
            MinTime.Text = node.MinTime
            chkActive.Checked = node.Active
            chkDisplay.Checked = node.Display
            
            'node value
            dwlClassNode.SelectedIndex = dwlClassNode.Items.IndexOf(dwlClassNode.Items.FindByValue(node.ClassNode))
        Else 'nel caso di un nuovo nodo
            ' ApprovalStatus 
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
            Title.Text = Nothing
            SubTitle.Text = Nothing
            'Abstract.Value = Nothing
            Abstract.Content = Nothing
            'FullText.Value = Nothing
            FullText.Content = Nothing
            'setta la lingua per un nuovo content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
            
            txtHiddenContentType.Text = 0
            txtContentType.Text = Nothing
            Objectives.Text = Nothing
            Duration.Text = 0
            MinTime.Text = 0
            chkActive.Checked = True
            chkDisplay.Checked = True
            
            dwlClassNode.SelectedIndex = dwlClassNode.Items.IndexOf(dwlClassNode.Items.FindByValue(NodeValue.Classes.Argument))
            
            'sito
            If (Request("S") <> Nothing) Then
                Dim site As New SiteAreaValue
                Dim coll As New SiteAreaCollection
                site = Me.BusinessSiteAreaManager.Read(Request("S"))
                coll.Add(site)
                
                oSite.GenericCollection = coll
            End If
            
            oSite.IsNew = True
            oSite.LoadControl()
            'aree
            Aree.IsNew = True
            Aree.LoadControl()
        End If
    End Sub
    
    Sub ContentRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ContentRel") Then
            Dim cPk As Integer = sender.CommandArgument
                
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "ContCatalg")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    'recupera tutti le sitearea associate ad un content (corso, lezione, nodo)
    Function GetSiteArea(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Area)
    End Function
    
    'recupera tutti le sitearea di tipo sito associate ad un content (corso, lezione, nodo)
    Function GetSite(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Site)
    End Function
    
    Sub LoadNodesArchive(ByVal sender As Object, ByVal e As EventArgs)
        LoadNodesArchive()
    End Sub
    
    Sub LoadNodesArchive()
        LoadNodes()
        ActiveAllArchive()
    End Sub
    
    'salva le associazioni content/sitearea
    Function SaveSiteArea(ByVal contentId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim ContentIdent As New ContentIdentificator(contentId, languageId)
        Dim res As Boolean = Me.BusinessContentManager.RemoveAllContentSiteArea(ContentIdent)
       
        If Not Aree.GetSelection Is Nothing AndAlso Aree.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = Aree.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
        
        If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = oSite.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
    End Function
    
    Sub TreeView(ByVal sender As Object, ByVal e As EventArgs)
        If Not (RadTreeCourse.IsEmpty) Then
            RadTreeCourse.Visible = True
            btn_refresh.Visible = True
        Else
            CreateJsMessage("Sorry, there aren't courses related to this site.")
        End If
        
    End Sub
    
    'gestisce la ricerca delle relazioni che compongono la mappa virtuale
    Sub SearchRelations(ByVal sender As Object, ByVal e As EventArgs)
        BindWithSearch(gdw_NodesRelation, True)
    End Sub
    
    'gestisce la ricerca delle relazioni che compongono la mappa virtuale
    Sub SearchNodes(ByVal sender As Object, ByVal e As EventArgs)
        If Not (Request("S") Is Nothing) Then
            BindWithSearch(gdw_Nodes)
        End If
    End Sub
    
    'caricamento della griglia delle relazioni 
    'Verifica la presenza di criteri di ricerca
    Sub BindWithSearch(ByVal objGrid As GridView, Optional ByVal search As Boolean = False)
        If (objGrid.ID = "gdw_NodesRelation") Then
            Dim NodeRelSearcher As New NodeRelationSearcher
            
            If txtId.Text <> "" Then NodeRelSearcher.Key.Id = txtId.Text
            If (ckStartRel.Checked = True) Then NodeRelSearcher.TypeRelation = NodeRelationSearcher.TypeRel.StartRelation
           
            If (search) Then
                If (dwlCourse.Items.Count > 0) AndAlso (dwlCourse.SelectedValue <> 0) Then
                    If (dwlLesson.SelectedValue <> 0) Then
                        If (dwlLesson.SelectedIndex > 0) Then NodeRelSearcher.KeyLesson.PrimaryKey = dwlLesson.SelectedItem.Value
                        If (dwlStartNode.SelectedIndex > 0) Then NodeRelSearcher.KeyStartNode.PrimaryKey = dwlStartNode.SelectedItem.Value
                        If (dwlEndNode.SelectedIndex > 0) Then NodeRelSearcher.KeyEndNode.PrimaryKey = dwlEndNode.SelectedItem.Value
                     
                        BindGrid(objGrid, NodeRelSearcher)
                    Else
                        CreateJsMessage("Select a lesson, please.")
                        Exit Sub
                    End If
                Else
                    CreateJsMessage("Select a course, please.")
                    Exit Sub
                End If
            End If
            
            Exit Sub
        End If
        
        If (objGrid.ID = "gdw_Nodes") Then
            Dim CatSearcher As New CatalogSearcher
                     
            If (srcID.Text <> "") Then CatSearcher.key.Id = srcID.Text
            If (srcPK.Text <> "") Then CatSearcher.key.PrimaryKey = srcPK.Text
            If (srcTitle.Text <> "") Then CatSearcher.SearchString = srcTitle.Text
           
            CatSearcher.key.IdLanguage = srcLanguage.Language
           
            'oSiteAreaValue = GetSiteSelection(srcSite)
            If Not (Request("S") Is Nothing) Then
                CatSearcher.KeySite.Id = Request("S")
            End If
            
            Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(srcArea)
            If (Not oSiteAreaValue Is Nothing) Then
                CatSearcher.KeySite.Id = oSiteAreaValue.Key.Id
            End If
            
            If srcContentType.SelectedValue <> -1 Then
                CatSearcher.KeyContentType.Id = srcContentType.SelectedItem.Value
            End If
                                  
            BindGridNode(objGrid, CatSearcher)
        End If
    End Sub
        
    'Esegue il bind della griglia
    'data la griglia stessa e l'eventuale searcher    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As NodeRelationSearcher = Nothing)
        Dim NodeRelCollection As New NodeRelationCollection
  
        If Searcher Is Nothing Then
            Searcher = New NodeRelationSearcher
        End If
        
        CatalogManager.Cache = False
        NodeRelCollection = CatalogManager.Read(Searcher)
        
        If Not (NodeRelCollection Is Nothing) AndAlso (NodeRelCollection.Count > 0) Then
            RadTreeCourse.Visible = True
            btn_refresh.Visible = True
        End If
        
        objGrid.DataSource = NodeRelCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindGridNode(ByVal objGrid As GridView, Optional ByVal Searcher As CatalogSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New CatalogSearcher
        End If
        
        Searcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Node
        Searcher.IdContentSubType = ContentValue.Subtypes.Course
        
        Searcher.Active = SelectOperation.All
        Searcher.Display = SelectOperation.All
        
        CatalogManager.Cache = False
        Dim nodeColl As NodeCollection = CatalogManager.ReadNodes(Searcher)
        
        gdw_Nodes.DataSource = nodeColl
        gdw_Nodes.DataBind()
    End Sub
    
    'retituisce il title del nodo o della lezione
    Function getTitle(ByVal key As Integer) As String
        If (key = 0) Then Return Nothing
        
        Dim catSearcher As New ContentSearcher
        catSearcher.key.PrimaryKey = key
        catSearcher.Active = SelectOperation.All
        catSearcher.Display = SelectOperation.All
        
        Me.BusinessContentManager.Cache = False
        Dim catColl As ContentCollection = Me.BusinessContentManager.Read(catSearcher)
        If Not (catColl Is Nothing) AndAlso (catColl.Count > 0) Then
            Return catColl(0).Title
        End If
       
        Return "Item Deleted"
    End Function
    
    Function getID(ByVal key As Integer) As String
        If (key = 0) Then Return Nothing
        
        Dim catSearcher As New ContentSearcher
        catSearcher.key.PrimaryKey = key
        catSearcher.Active = SelectOperation.All
        catSearcher.Display = SelectOperation.All
        
        Me.BusinessContentManager.Cache = False
        Dim catColl As ContentCollection = Me.BusinessContentManager.Read(catSearcher)
       
        If Not (catColl Is Nothing) AndAlso (catColl.Count > 0) Then
            Return "| " & catColl(0).Key.Id
        End If
        
        Return Nothing
    End Function
    
    Sub BindContentType(ByVal dwl As DropDownList)
        dwl.Items.Clear()
        Dim ContentTypeSearcher As New ContentTypeSearcher
        Dim contentTypeColl As ContentTypeCollection = Me.BusinessContentTypeManager.Read(ContentTypeSearcher)
    
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwl, contentTypeColl, "Description", "Key.Id")
        
        dwl.Items.Insert(0, New ListItem("---Select ContentType---", -1))
    End Sub
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        btn_newnode.Visible = True
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
        btn_newnode.Visible = False
    End Sub
    
    Sub ActiveAllArchive()
        pnlNodeGrid.Visible = True
        pnlNodeDett.Visible = False
        
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridListNodes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>



<asp:Panel id="pnlNodeGrid" runat="server">
<hr />
    <table class="topContentSearcher">
        <tr>
            <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
            <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Nodes Searcher</span></td>
        </tr>
    </table>
        
    <table class="form" style="background-color:#F3F3F3; width:100%">
        <tr> 
            <td><strong>ID</strong></td>
            <td><HP3:Text runat="server" ID="srcID" TypeControl="TextBox" Style="width:50px"/></td>
            <td><strong>PK</strong></td>
            <td><HP3:Text runat="server" ID="srcPK" TypeControl="TextBox" Style="width:50px"/></td>
            <td><strong>Title</strong></td>
            <td><HP3:Text runat="server" ID="srcTitle" TypeControl="TextBox" Style="width:350px"/></td>
       </tr>
       <tr>
            <td><strong>Language</strong></td>
            <td><HP3:contentLanguage ID="srcLanguage" runat="server"  Typecontrol="StandardControl" /></td>  
            <td><strong>Area</strong></td>
            <td><HP3:ctlSite runat="server" ID="srcArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
            <td><strong>ContentType</strong></td> 
            <td><asp:DropDownList ID="srcContentType" runat="server" /></td>
       </tr>
      <tr>
           <td><asp:button runat="server" CssClass="button" ID="btn_srcNodes" onclick="SearchNodes" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
      </tr>
    </table>
   
<hr />
<asp:button ID="btn_newnode" runat="server" Text="New Node" onclick="NewNode" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /><br /><br />
             <asp:gridview id="gdw_Nodes" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="10"
                    OnPageIndexChanging="gridListNodes_PageIndexChanging"
                    emptydatatext="No items available">
                  <Columns >
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate><img alt="Language: <%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" /></ItemTemplate> 
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderStyle-Width="70%" HeaderText="Title">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="6%">
                           <ItemTemplate>
                                <%--<asp:ImageButton id="lnkAddN" runat="server" ToolTip ="Add Node" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif" OnClick="AddNode" CommandName ="AddNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>--%>
                                <asp:ImageButton id="lnkSelectN" runat="server" ToolTip ="Edit" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditNode" CommandName ="SelectNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDeleteN" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteNode" CommandName ="DeleteNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <%--<asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton id="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteQuest" CommandName ="DeleteQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                           </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
            </asp:gridview>
<%--        </td>
    </tr> 
</table>--%>
</asp:Panel>

<hr/>

<asp:Panel id="pnlGrid" runat="server">
  <asp:button ID="btnNew" runat="server" Text="New Relation" onclick="NewRow" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /><br /><br />
 
     <%--FILTRI sulle relazioni--%>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Relation Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Is Start Relation</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:80px"/></td>    
                    <td><asp:CheckBox id="ckStartRel" runat="server"/></td>  
                </tr>
                <tr>
                    <td><strong>Language</strong></td>
                    <td><strong>Courses</strong></td>    
                    <td><strong>Lessons</strong></td>
                </tr>
                <tr>
                    <td><HP3:contentLanguage ID="srcNodeLang" runat="server"  Typecontrol="StandardControl" /></td>
                    <td><asp:DropDownList id="dwlCourse" runat="server" autopostback="true" OnSelectedIndexChanged="BindLessons"/></td>    
                    <td><asp:DropDownList id="dwlLesson" runat="server" autopostback="true" OnSelectedIndexChanged="BindNodes"/></td>    
                </tr>
                 <tr>
                    <td><strong>Start Nodes</strong></td>    
                    <td><strong>End Nodes</strong></td>
                </tr>
                <tr>
                    <td><asp:DropDownList id="dwlStartNode" runat="server"/></td>  
                    <td><asp:DropDownList id="dwlEndNode" runat="server"/></td> 
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btn_src" onclick="SearchRelations" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/> <asp:button runat="server" CssClass="button" ID="btn_tree" onclick="TreeView" Text="TreeView" style="margin-top:10px;"/></td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
 
 <table  style="margin-top:10px" class="form" width="99%">
        <tr>
            <td><asp:LinkButton id="btn_refresh" Text="refresh" OnClick="Refresh" runat="server" Visible="false"/></td>
            <td></td>
        </tr>
        <tr>
           <td style="width:30%;overflow:auto" valign ="top"><telerik:RadTreeView id="RadTreeCourse"  Visible="false" width="100%" height="400px"    
                onnodeexpand="RadTreeView1_NodeExpand" 
                runat="server"   
                enableviewstate="true"  OnNodeClick="ClickForAdd">
                </telerik:RadTreeView>
           </td>
           <td  style="width:80%">
                    <asp:gridview id="gdw_NodesRelation" 
                                runat="server"
                                AutoGenerateColumns="false" 
                                GridLines="None" 
                                CssClass="tbl1"
                                HeaderStyle-CssClass="header"
                                AlternatingRowStyle-BackColor="#E9EEF4"
                                AllowPaging="true"
                                PagerSettings-Position="TopAndBottom"  
                                PagerStyle-HorizontalAlign="Right"
                                PagerStyle-CssClass="Pager"             
                                AllowSorting="true"
                                OnPageIndexChanging="gridListUsers_PageIndexChanging"
                                PageSize ="10"
                                emptydatatext="No item available">
                              <Columns >
                                <%--<asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Lesson">
                                        <ItemTemplate><%#getTitle(Container.DataItem.KeyLesson.PrimaryKey)%> <%#getID(Container.DataItem.KeyLesson.PrimaryKey)%></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Start Node">
                                        <ItemTemplate><%#getTitle(Container.DataItem.KeyStartNode.PrimaryKey)%> <%#getID(Container.DataItem.KeyStartNode.PrimaryKey)%></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="End Node">
                                        <ItemTemplate><%#getTitle(Container.DataItem.KeyEndNode.PrimaryKey)%> <%#getID(Container.DataItem.KeyEndNode.PrimaryKey)%></ItemTemplate>
                                </asp:TemplateField>
                                <%--  <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Description">
                                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderStyle-Width="5%">
                                       <ItemTemplate>
                                           <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Restore item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                           <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                       </ItemTemplate>
                                </asp:TemplateField>
                              </Columns>
                 </asp:gridview>
            </td>
        </tr> 
    </table>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
  
 <br />
 <h2 id="h2Title" runat="server"  class="title"/>
 <div style="padding-left:550px"><asp:Button id ="btn_edit" runat="server" Text="Edit "  Visible="false" OnClick="ClickForEdit" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Ico/content_edit.gif');background-repeat:no-repeat;background-position:1px 1px;"/></div> 
 <table  style=" margin-top:10px" class="form" width="99%">
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStartNode&Help=cms_HelpStartNode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStartNode", "Start Node")%> </td>
        <%--<td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenStartNode" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtStartNode"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popNodes.aspx?sites=' +  document.getElementById('<%=txtHiddenStartNode.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenStartNode.clientid%>&ListId=<%=txtStartNode.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>--%>
         <td>
          <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenStartNode" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtStartNode"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popNodes.aspx?sites=' +  document.getElementById('<%=txtHiddenStartNode.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenStartNode.clientid%>&ListId=<%=txtStartNode.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEndNode&Help=cms_HelpEndNode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEndNode", "End Node")%> </td>
        <%--<td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenEndNode" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtEndNode"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popNodes.aspx?sites=' +  document.getElementById('<%=txtHiddenEndNode.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenEndNode.clientid%>&ListId=<%=txtEndNode.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>--%>
         <td>
          <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenEndNode" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtEndNode"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popNodes.aspx?sites=' +  document.getElementById('<%=txtHiddenEndNode.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenEndNode.clientid%>&ListId=<%=txtEndNode.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
     <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRelDesc&Help=cms_HelpRelDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelDesc", "Relation Description")%> </td>
        <td><HP3:Text id ="txtRelDescription" runat="server" TypeControl="TextBox"  MaxLength ="400" style="width:300px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLesson&Help=cms_HelpLesson&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLesson", "Lesson")%> </td>
        <td>
          <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenLesson" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtLesson"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popLessons.aspx?sites=' +  document.getElementById('<%=txtHiddenLesson.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenLesson.clientid%>&ListId=<%=txtLesson.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
     <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOrderMap&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%> </td>
        <td><HP3:Text id ="lOrder" runat="server" TypeControl="NumericText" style="width:300px"/></td>
     </tr>
     <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStartRel&Help=cms_HelpStartRel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStartRel", "Start Relation")%> </td>
        <td><asp:CheckBox id="isStartRel"  runat="server"/></td>
     </tr>
 </table>
</asp:Panel>

<asp:Panel id="pnlNodeDett" runat="server" visible="false">
 <asp:Button id ="btnArchive_N" runat="server" Text="Archive" OnClick="LoadNodesArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 <asp:Button id ="btnSave_N" runat="server" Text="Save" OnClick="SaveNode" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 
 
  <table  style="margin-top:10px" class="form" width="99%">
  <tr runat="server" id="tr_lSelector">
       <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentLang&Help=cms_HelpContentLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentLang", "Content Language")%></td>
            <td><HP3:contentLanguage ID="lSelector" runat="server"  Typecontrol="StandardControl" /></td>
       </tr>
       <!-- 28/10/08 MR -->
       <tr id="apprStatus" runat="server" >
           <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentApr&Help=cms_HelpContentApr&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentApr", "Content Approval")%></td>
            <td>
               <asp:DropDownList id="dwlApprovalStatus" runat="server">
                    <asp:ListItem  Text="To Be Approved" Value="2"></asp:ListItem>
                    <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
                    <asp:ListItem  Text="Not Approved" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
       
       <tr runat="server" id="tr_DateUpdate">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateUpdate&Help=cms_HelpDateUpdate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateUpdate", "Date Update")%></td>
            <td><HP3:Date runat="server" ID="DateUpdate" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateCreate">
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateCreate&Help=cms_HelpDateCreate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateCreate", "Date Create")%></td>
            <td><HP3:Date runat="server" ID="DateCreate" TypeControl="JsCalendar" ShowTime="true" /></td>
        </tr>
        <tr runat="server" id="tr_DatePublish">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDatePublish&Help=cms_HelpDatePublish&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDatePublish", "Date Publish")%></td>
            <td><HP3:Date runat="server" ID="DatePublish" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_oSite">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td><HP3:ctlSite runat="server" ID="oSite" TypeControl="GenericRelation" SiteType="Site" /></td>
        </tr>
        <tr runat="server" id="tr_Aree">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="Aree" TypeControl="GenericRelation" SiteType="area"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr runat="server" id="tr_Title">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
            <td><HP3:Text runat="server" ID="Title" TypeControl="TextBox" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSubTitle&Help=cms_HelpSubTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSubTitle", "SubTitle")%> </td>
            <td><HP3:Text runat ="server" id="SubTitle" TypeControl ="TextBox" MaxLength="400" style="width:600px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAbstract&Help=cms_HelpAbstract&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAbstract", "Abstract")%> </td>
            <td>
            
<%--            <FCKeditorV2:FCKeditor id="Abstract" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="Abstract" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullText&Help=cms_HelpFullText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullText", "Full Text")%> </td>
            <td>
            
<%--            <FCKeditorV2:FCKeditor id="FullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="FullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            
            </td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormObjectives&Help=cms_HelpObjectives&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormObjectives", "Objectives")%> </td>
            <td><HP3:Text runat ="server" id="Objectives" TypeControl ="TextArea" MaxLength="400" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDuration&Help=cms_HelpDuration&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDuration", "Duration")%> </td>
            <td><HP3:Text runat ="server" id="Duration" TypeControl="NumericText" style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMinTime&Help=cms_HelpMinTime&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMinTime", "Minimun Time")%> </td>
            <td><HP3:Text runat ="server" id="MinTime" TypeControl="NumericText" Style="width:600px"/></td>
        </tr>
         
        <tr runat="server" id="tr_chkActive">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="chkActive" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_chkDisplay">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="chkDisplay" runat="server"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormClassNode&Help=cms_HelpClassNode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormClassNode", "Class Node")%> </td>
            <td>
                <asp:DropDownList id="dwlClassNode" runat="server">
                    <asp:ListItem Text="Argument" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Bibliografic" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Chapter" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Paragraph" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </td>
       </tr>
 </table>
</asp:Panel>

<script type="text/javascript" language="javascript">

</script>


