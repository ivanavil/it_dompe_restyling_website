<%@ Control Language="VB" ClassName="ctlContentTypeManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"  %>

<script runat="server">
    Private oContentsTypeManager As ContentTypeManager
    Private oContentsTypeSearcher As ContentTypeSearcher
    Private oContentTypeCollection As ContentTypeCollection
    Private oGenericUtlity As New GenericUtility
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    
    Private oCollection As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    
    Private strDomain As String
    Private strJs As String
    Private _strDomain As String
    Private _language As String
    
    Private _sortType As ContentTypeGenericComparer.SortType = ContentTypeGenericComparer.SortType.ById
    Private _sortOrder As ContentTypeGenericComparer.SortOrder = ContentTypeGenericComparer.SortOrder.DESC
    Private _typecontrol As ControlType = ControlType.View
    
    Private utility As New WebUtility
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
       
    Private Property SDomain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdContentType() As Int32
        Get
            Return ViewState("SelectedIdContentType")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdContentType") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
     
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il ContentTypeValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadCTValue() As ContentTypeValue
        If SelectedIdContentType <> 0 Then
            Return oGenericUtlity.GetContentType(SelectedIdContentType)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Recupera i type delle siteareaa
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetContentTemplateValue(ByVal itemZero As String)
        Dim oCTmpManager As New ContentTemplateManager
        Dim sValue As ContentTemplateValue
        Dim sCollection As ContentTemplateCollection
        Dim oListItem As ListItem
        ddlContentsTemplate.Items.Clear()
        sCollection = oCTmpManager.Read(New ContentTemplateSearcher())
        If Not sCollection Is Nothing Then
            For Each sValue In sCollection
                oListItem = New ListItem(sValue.Description, sValue.Key.Id)
                ddlContentsTemplate.Items.Add(oListItem)
            Next
            If itemZero <> "" Then ddlContentsTemplate.Items.Insert(0, New ListItem(itemZero, 0))
        End If
    End Sub
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oCTValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oCTValue As ContentTypeValue)
        If Not oCTValue Is Nothing Then
            With oCTValue
                ContentTypeDescrizione.InnerText = " - " & .Key.Name
                ContentTypeId.InnerText = "ContentType ID: " & .Key.Id
                Domain.Text = .Key.Domain
                Name.Text = .Key.Name
                Description.Text = .Description
                ddlContentsTemplate.SelectedIndex = ddlContentsTemplate.Items.IndexOf(ddlContentsTemplate.Items.FindByValue(.KeyContentsTemplate.Id))
                If .IsIndexed Then isIndex.Checked = True
                Note.Text = .Note
                txtPage.Text = .Page

                ctlRoles.GenericCollection = oGenericUtlity.ReadRoles(.Key.Id, GenericUtility.TypeRolesConnection.WithContentsType)
                ctlRoles.LoadControl()
            End With
            
        End If
    End Sub
    
    ''' <summary>
    ''' Rimuovo tutti i vecchi accoppiamenti
    ''' </summary>
    ''' <param name="ContentTypeid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function RemoveAllRoles(ByVal ContentTypeid As ContentTypeIdentificator) As Boolean
        Dim oContentTypeManager As New ContentTypeManager
        Return oContentTypeManager.RemoveAllRoles(ContentTypeid)
    End Function
    
    ''' <summary>
    ''' Delete ContentType
    ''' </summary>
    ''' <param name="ContentTypeid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Delete(ByVal ContentTypeid As Integer) As Boolean
        Dim oContentTypeManager As New ContentTypeManager
        Return oContentTypeManager.Delete(New ContentTypeIdentificator(ContentTypeid))
    End Function
    
    ''' <summary>
    ''' Salvataggio dei ruoli associati al content Type
    ''' </summary>
    ''' <param name="ContentTypeid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function SaveRoles(ByVal ContentTypeid As ContentTypeIdentificator) As Boolean
        Try
            'Recupero la lista di ruoli nell controllo
            Dim oRoleCollection As Object = ctlRoles.GetSelection
            If Not oRoleCollection Is Nothing Then
                Dim oContentTypeManager As New ContentTypeManager
                
                'Tento la rimozione dei vecchi accoppiamenti
                If RemoveAllRoles(ContentTypeid) Then
                    For Each oRoleValue As Rolevalue In oRoleCollection
                        'Creo i nuovi
                        oContentTypeManager.CreateRole(ContentTypeid, oRoleValue.Key)
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
                
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = masterPageManager.GetLang.Id
        SDomain() = strDomain
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadCTValue)
            Case ControlType.View
                'rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContentType)                
                End If
            Case ControlType.Selection
                'rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContentType)                    
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollection = siteAreaManager.Read(siteAreaSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub Page_load()
        GetContentTemplateValue("Select Content Template")
        BindDomains()
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContentTypeSearcher = Nothing)
        oContentsTypeManager = New ContentTypeManager
        oContentsTypeManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New ContentTypeSearcher
        End If
        
        oContentTypeCollection = oContentsTypeManager.Read(Searcher)
        If Not oContentTypeCollection Is Nothing Then           
            oContentTypeCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = oContentTypeCollection
        objGrid.DataBind()
                
    End Sub
    
          
    ''' <summary>
    ''' Caricamento della griglia dei temi
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
                
        oContentsTypeSearcher = New ContentTypeSearcher        
        If txtId.Text <> "" Then oContentsTypeSearcher.Key = New ContentTypeIdentificator(txtId.Text)        
        If txtDescription.Text <> "" Then oContentsTypeSearcher.Description = txtDescription.Text
        If txtName.Text <> "" Then oContentsTypeSearcher.Key.Name = txtName.Text
        If OnlyContent.Checked = True Then oContentsTypeSearcher.OnlyAssociatedContents = True
        If dwlDomain.SelectedItem.Value <> "-1" Then oContentsTypeSearcher.Key.Domain = dwlDomain.SelectedItem.Value

        
        ReadSelectedItems()
        BindGrid(objGrid, oContentsTypeSearcher)
    End Sub
    
    Sub SearchContentType(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListContentType)
    End Sub
    
     
    ''' <summary>
    ''' Lettura delle descrizioni dei content type dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentTypeValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetContentType(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
         
    Private Property SortType() As ContentTypeGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContentTypeGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContentTypeGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ContentTypeGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ContentTypeGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ContentTypeGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListContentType.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litTheme")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ContentTypeCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curContentTypeValue As ContentTypeValue
                    Dim outContentTypeCollection As New ContentTypeCollection
            
                    For Each str As String In CheckedItems
                        curContentTypeValue = oGenericUtlity.GetContentType(Int32.Parse(str))
                        If Not curContentTypeValue Is Nothing Then
                            outContentTypeCollection.Add(curContentTypeValue)
                        End If
                    Next
            
                    Return outContentTypeCollection
            End Select
            Return Nothing
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedIdContentType = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListContentType)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        btnSave.Text = "Update"
        
        TypeControl = ControlType.Edit
        SelectedIdContentType = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
             
        Select Case s.commandname
            Case "SelectItem"
                NewItem = False
            Case "DeleteItem"
                TypeControl = ControlType.View
                Delete(SelectedIdContentType)
                BindWithSearch(gridListContentType)
            Case "CopyItem"
                btnSave.Text = "Save"
                NewItem = True
        End Select
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Save/Update del content type
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oContentTypeManager As New ContentTypeManager
        Dim oContentTypeValue As ContentTypeValue = ReadFormValues()
        
        If NewItem Then
            oContentTypeValue = oContentTypeManager.Create(oContentTypeValue)
        Else
            oContentTypeValue = oContentTypeManager.Update(oContentTypeValue)
        End If
        
        SaveRoles(oContentTypeValue.Key)
        
        If oContentTypeValue Is Nothing Then
            strJs = "alert('Error during saving')"
        Else
            strJs = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As ContentTypeValue
        Dim oContentTypeValue As New ContentTypeValue
        
        With oContentTypeValue
            .Key.Domain = Domain.Text
            .Key.Id = SelectedIdContentType
            .Key.Name = Name.Text
            If ddlContentsTemplate.SelectedValue <> "" Then .KeyContentsTemplate.Id = ddlContentsTemplate.SelectedValue
            .Description = Description.Text
            .IsIndexed = isIndex.Checked
            'Salvataggio percorso
            .Note = Note.Text
            .Page = txtPage.Text
        End With
        
        Return oContentTypeValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo content type
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewContentType(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedIdContentType = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is ctlRoleList Then
                ctl.clear()
            End If            
        Next
        
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momenti ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        ContentTypeId.InnerText = ""
        ContentTypeDescrizione.InnerText = "New Content Type"
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListContentType_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        'Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litTheme")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        'lnkCopy = e.Row.FindControl("lnkCopy")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False
                    'lnkCopy.Visible = False
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    lnkDelete.Visible = True
                    'lnkCopy.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False
                    'lnkCopy.Visible = False                    
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListContentType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListContentType_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListContentType.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Key"
                If SortType <> ContentTypeGenericComparer.SortType.ByKey Then
                    SortType = ContentTypeGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
                
            Case "Key.Id"
                If SortType <> ContentTypeGenericComparer.SortType.ById Then
                    SortType = ContentTypeGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> ContentTypeGenericComparer.SortType.ByDescription Then
                    SortType = ContentTypeGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Page"
                If SortType <> ContentTypeGenericComparer.SortType.ByPage Then
                    SortType = ContentTypeGenericComparer.SortType.ByPage
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>
<script type="text/javascript" >       
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
        <%=strJs%>
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<%--FILTRI SUI TEMI--%>
<asp:Panel id="pnlGrid" runat="server">
    <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewContentType" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">ContentType Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form" width="100%">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Name</strong></td>
                    <td><strong>Description</strong></td>
                    <td><strong>Only Associated Content</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>  
                    <td><HP3:Text runat="server" ID="txtName" TypeControl ="textbox" style="width:150px"/></td>   
                    <td><HP3:Text runat="server" ID="txtDescription" TypeControl ="textbox" style="width:300px"/></td>
                    <td><asp:CheckBox ID="OnlyContent" runat="server" /></td> 
                </tr>
                <tr>
                    <td style="width:40px">Domain</td>
                </tr>
                <tr>                
                    <td style="width:40px"><asp:DropDownList id="dwlDomain" runat="server"/></td>
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" onclick="SearchContentType" Text="Search" cssclass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
<%--FINE FILTRI SUI TEMI--%>

<%--LISTA TEMI--%>
    <asp:gridview ID="gridListContentType" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    AllowSorting="true"                
                    PageSize ="20"               
                    emptydatatext="No themes available"                                                          
                    OnRowDataBound="gridListContentType_RowDataBound" 
                    OnPageIndexChanging="gridListContentType_PageIndexChanging" 
                    OnSorting="gridListContentType_Sorting"
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
                  <Columns >
                     <asp:TemplateField>
                        <ItemTemplate>
                            <asp:radiobutton ID="chkSel" runat="server" />
                            <asp:literal ID="litTheme" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Id" SortExpression="Key.Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Identificator" SortExpression="Key">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
                    </asp:TemplateField>  
                    <asp:BoundField DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
                    <asp:BoundField DataField="Page" HeaderText="Page" SortExpression ="Page" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" ToolTip="Edit item" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>                                                
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkCopy" runat="server" ToolTip ="Copy item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="OnItemSelected" CommandName ="CopyItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>
<%--FINE LISTA TEMI--%>


<%--EDIT TEMI--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button id="btnBack" runat="server" Text="Archive" onclick="goBack" cssclass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" runat="server" Text="Save" onclick="Update" cssclass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    
    <span id="ContentTypeId" class="title" runat ="server"/>
    <span id="ContentTypeDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat="server" ID="Domain" TypeControl="TextBox" style="width:90px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat="server" ID="Name" TypeControl="TextBox" style="width:90px" MaxLength="10"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContentTypeDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemplate&Help=cms_HelpContentTemplate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemplate", "Content Template")%></td>
            <td><asp:DropDownList id="ddlContentsTemplate" DataTextField="Description" DataValueField="Description" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormPage&Help=cms_HelpPage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPage", "Page")%></td>
            <td><HP3:Text runat ="server" ID="txtPage" TypeControl ="PathSelector" style="width:300px"/></td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" ID="ctlRoles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
        <td><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormIsIndex&Help=cms_HelpIsIndex&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIsIndex", "Indexed")%></td>
        <td><asp:checkbox id="isIndex" runat="server" Checked="false"/></td>
    </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=SDomain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_HelpNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNote", "Note")%></td>
            <td valign="top"><HP3:Text runat ="server" ID="Note" TypeControl ="TextArea" style="width:300px"/></td>
        </tr>
    </table>
</asp:Panel>
<%--FINE EDIT TEMI--%>
