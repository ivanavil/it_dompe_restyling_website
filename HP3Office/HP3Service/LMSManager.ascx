<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlContentEditorial" Src ="~/hp3Office/HP3Parts/ctlContentEditorialList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagPrefix="FCKeditorV2" namespace="FredCK.FCKeditorV2" assembly="FredCK.FCKeditorV2" %>

<script runat="server">
    Private utility As New Core.Utility.WebUtility
    Private CatalogManager As New CatalogManager
    Private LangManager As New LanguageManager
    
    Private _strDomain As String
    Private _language As String
    Private strJS As String
    Private _isNew As Boolean = False
   
    Private BlendedLearning As Integer = CourseValue.CourseTypes.BlendedLearning
    Private DistanceLearning As Integer = CourseValue.CourseTypes.DistanceLearning
    Private ResidentialCourse As Integer = CourseValue.CourseTypes.ResidentialCourse
    
    Private PDF As String = "pdf"
    Private JPG As String = "jpg"
    
    Public Property ContentId() As Integer
        Get
            Return ViewState("contentId")
        End Get
        Set(ByVal value As Integer)
            ViewState("contentId") = value
        End Set
    End Property
    
    Public Property CoursePk() As Integer
        Get
            Return ViewState("CoursePk")
        End Get
        Set(ByVal value As Integer)
            ViewState("CoursePk") = value
        End Set
    End Property
    
    Public Property LessonPk() As Integer
        Get
            Return ViewState("LessonPk")
        End Get
        Set(ByVal value As Integer)
            ViewState("LessonPk") = value
        End Set
    End Property
    
    Public Property NodePk() As Integer
        Get
            Return ViewState("NodePk")
        End Get
        Set(ByVal value As Int32)
            ViewState("NodePk") = value
        End Set
    End Property
             
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
           
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Me.Page.IsPostBack) Then
            expSite.LoadControl()
            expArea.LoadControl()
            
            srcArea.LoadControl()
            BindContentType(srcContentType)
            
            impSite.LoadControl()
            impArea.LoadControl()
            LoadCourses()
            ActiveCourseGrid()
        End If
    End Sub
   
    'INIZIO GESTIONE DEI CORSI
    '---------------------------
    'recupera la lista dei corsi
    Sub LoadCourses()
        Dim CatSearcher As New CatalogSearcher
        Dim CoursesColl As CourseCollection
                
        If Not (Request("S") Is Nothing) Then
            'CatSearcher.KeySite.Id = Request("S")
            CatSearcher.key.IdLanguage = srcLanguage.Language
            CatSearcher.Active = SelectOperation.All
            CatSearcher.Display = SelectOperation.All
            
            CatalogManager.Cache = False
            CoursesColl = CatalogManager.ReadCourses(CatSearcher)
        End If
        
        gdw_Courses.DataSource = CoursesColl
        gdw_Courses.DataBind()
    End Sub
    
    'inserisce una nuovo corso
    Sub NewCourse(ByVal sender As Object, ByVal e As EventArgs)
        CoursePk = 0
        
        LoadCourse(Nothing)
        ActiveCourseDett()
    End Sub
    
    'si occupa di caricare i dati del corso nel form
    Sub LoadCourse(ByVal course As CourseValue)
        If Not (course Is Nothing) Then
            'carica i dati del corso
            CoursePk = course.Key.PrimaryKey
            Title.Text = course.Title
            SubTitle.Text = course.SubTitle
            'Abstract.Value = course.Abstract
            Abstract.Content = course.Abstract
            ' FullText.Value = course.FullText
            FullText.Content = course.FullText
            'gestione della lingua
            lSelector.LoadLanguageValue(course.Key.IdLanguage)
            'gestione ApprovalStatus
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(course.ApprovalStatus))
            'gestione aree
            Aree.GenericCollection = GetSiteArea(course.Key.Id, course.Key.IdLanguage)
            Aree.IsNew = False
            Aree.LoadControl()
                               
            'gestione siti
            oSite.GenericCollection = GetSite(course.Key.Id, course.Key.IdLanguage)
            oSite.IsNew = False
            oSite.LoadControl()
            
            'gestione editorial
            ctlCE.idRelated = course.Key.Id
            ctlCE.LoadControl()
            
            DateUpdate.Value = Date.Now
            'Caricamento delle date
            If course.DateCreate.HasValue Then
                DateCreate.Value = course.DateCreate.Value
            Else
                DateCreate.Clear()
            End If
                                  
            If course.DatePublish.HasValue Then
                DatePublish.Value = course.DatePublish.Value
            Else
                DatePublish.Clear()
            End If
            
            txtHiddenContentType.Text = course.ContentType.Key.Id
            txtContentType.Text = course.ContentType.Description
            Objectives.Text = course.Objectives
            Duration.Text = course.Duration
            MinTime.Text = course.MinTime
                      
            chkActive.Checked = course.Active
            chkDisplay.Checked = course.Display
                      
            'course object
            If course.StartDate.HasValue Then
                CrStartDate.Value = course.StartDate.Value
            Else
                CrStartDate.Clear()
            End If
            
            If course.EndDate.HasValue Then
                CrEndDate.Value = course.EndDate.Value
            Else
                CrEndDate.Clear()
            End If
            
            CrType.SelectedIndex = CrType.Items.IndexOf(CrType.Items.FindByValue(course.CourseType))
            
            CrPlanner.Text = course.CoursePlanner
            CrProvider.Text = course.Provider
            CrRecipients.Text = course.Recipients
            CrOrganizationalSegreteriat.Text = course.OrganizationalSegreteriat
            CrScientificResponsible.Text = course.ScientificResponsiblePersonnel
            CrScientificSegreteriat.Text = course.ScientificSegreteriat
            CrSponsor.Text = course.Sponsor
            CrPreview.Checked = course.Preview
            
            CrCodeRequired.Checked = course.IsCodeRequired
            CrOnlineRegistration.Checked = course.OnlineRegistration
            CrEntranceTest.Checked = course.EntranceTest
            CrFinalEvaluationTest.Checked = course.FinalEvaluationTest
           
            txtHiddenQuest.Text = course.KeyFinalTest.Id
            txtQuest.Text = ReadQuestionnaire(course.KeyFinalTest.Id)
            
        Else 'nel caso di un nuovo corso
            ' 28/10/08 MR
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
            Title.Text = Nothing
            SubTitle.Text = Nothing
            'Abstract.Value = Nothing
            Abstract.Content = Nothing
            'FullText.Value = Nothing
            FullText.Content = Nothing
            'setta la lingua per un nuovo content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
            
            txtHiddenContentType.Text = 0
            txtContentType.Text = Nothing
            Objectives.Text = Nothing
            Duration.Text = 0
            MinTime.Text = 0
            chkActive.Checked = True
            chkDisplay.Checked = True
            
            DateCreate.Value = DateTime.Now
            DatePublish.Value = DateTime.Now
            DateUpdate.Value = DateTime.Now
            CrStartDate.Value = Date.Now
            CrEndDate.Value = Date.Now
            
            'course object
            CrPlanner.Text = Nothing
            CrProvider.Text = Nothing
            CrRecipients.Text = Nothing
            CrOrganizationalSegreteriat.Text = Nothing
            CrScientificResponsible.Text = Nothing
            CrScientificSegreteriat.Text = Nothing
            CrSponsor.Text = Nothing
            CrType.SelectedValue = CourseValue.CourseTypes.DistanceLearning
            CrPreview.Text = Nothing
            
            CrCodeRequired.Checked = False
            CrOnlineRegistration.Checked = False
            CrEntranceTest.Checked = False
            CrFinalEvaluationTest.Checked = False
            
            txtQuest.Text = Nothing
            txtHiddenQuest.Text = 0
            
            'sito
            If (Request("S") <> Nothing) Then
                Dim site As New SiteAreaValue
                Dim coll As New SiteAreaCollection
                site = Me.BusinessSiteAreaManager.Read(Request("S"))
                coll.Add(site)
                
                oSite.GenericCollection = coll
            End If
            
            oSite.IsNew = True
            oSite.LoadControl()
            'aree
            Aree.IsNew = True
            Aree.LoadControl()
            'Teacher()
            'keyQuestionaire
        End If
    End Sub
       
    'gestisce l'edit del corso
    Sub EditCourse(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SelectCourse") Then
            Dim argument As String = sender.CommandArgument
            Dim keys As String() = argument.Split("|")
            Dim PK As Integer = keys(0)
            Dim ID As Integer = keys(1)
            
            CoursePk = PK
            ContentId = ID
            
            CatalogManager.Cache = False
            'Dim course As CourseValue = CatalogManager.ReadCourse(New CatalogIdentificator(PK))
            Dim so As New CatalogSearcher
            
            so.key.IdLanguage = srcLanguage.Language
            so.KeySite.Id = Request("S")
            
            so.key.PrimaryKey = PK
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All
            
            Dim courses As CourseCollection = CatalogManager.ReadCourses(so)
            If Not (courses Is Nothing) AndAlso (courses.Count > 0) Then
                LoadCourse(courses(0))
                ActiveCourseDett()
            End If
        End If
    End Sub
        
    
    'gestisce la cancellazione del corso
    Sub DeleteCourse(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "DeleteCourse") Then
            Dim PK As String = sender.CommandArgument
            
            Dim bool As Boolean = CatalogManager.Delete(New CatalogIdentificator(PK))
            If (bool) Then
                CreateJsMessage("This course has been cancelled correctly.")
            Else
                CreateJsMessage("Impossible to delete this course.")
            End If
            
            LoadCourses()
            ActiveCourseGrid()
        End If
    End Sub
    
    'gestisce la creazione di un nuovo corso e l'update
    Sub SaveCourse(ByVal sender As Object, ByVal e As EventArgs)
        Dim course As New CourseValue
        
        course.Title = Title.Text
        course.SubTitle = SubTitle.Text
        'course.Abstract = Abstract.Value
        course.Abstract = Abstract.Content
        'course.FullText = FullText.Value
        course.FullText = FullText.Content
        
        'gestione della lingua
        course.Key.IdLanguage = lSelector.Language
                   
        'date
        course.DateCreate = DateCreate.Value
        course.DateUpdate = DateUpdate.Value
        course.DatePublish = DatePublish.Value
                  
        course.ContentType.Key.Id = txtHiddenContentType.Text
       
        course.Objectives = Objectives.Text
        If (Duration.Text <> "") Then
            course.Duration = Duration.Text
        End If
       
        If (MinTime.Text <> "") Then
            course.MinTime = MinTime.Text
        End If
        course.Active = chkActive.Checked
        course.Display = chkDisplay.Checked
            
        'course object
        course.StartDate = CrStartDate.Value
        course.EndDate = CrEndDate.Value
                  
        Select Case CrType.SelectedValue
            Case DistanceLearning
                course.CourseType = CourseValue.CourseTypes.DistanceLearning
                
            Case ResidentialCourse
                course.CourseType = CourseValue.CourseTypes.ResidentialCourse
                
            Case BlendedLearning
                course.CourseType = CourseValue.CourseTypes.BlendedLearning
        End Select
        
        Select Case dwlApprovalStatus.SelectedItem.Value
            Case CourseValue.ApprovalWFStatus.Approved
                course.ApprovalStatus = CourseValue.ApprovalWFStatus.Approved
            Case CourseValue.ApprovalWFStatus.ToBeApproved
                course.ApprovalStatus = CourseValue.ApprovalWFStatus.ToBeApproved
            Case CourseValue.ApprovalWFStatus.NotApproved
                course.ApprovalStatus = CourseValue.ApprovalWFStatus.NotApproved
        End Select
      
        course.CoursePlanner = CrPlanner.Text
        course.Provider = CrProvider.Text
        course.Recipients = CrRecipients.Text
        course.OrganizationalSegreteriat = CrOrganizationalSegreteriat.Text
        course.ScientificResponsiblePersonnel = CrScientificResponsible.Text
        course.ScientificSegreteriat = CrScientificSegreteriat.Text
        course.Sponsor = CrSponsor.Text
        course.Preview = CrPreview.Checked
            
        course.IsCodeRequired = CrCodeRequired.Checked
        course.OnlineRegistration = CrOnlineRegistration.Checked
        course.EntranceTest = CrEntranceTest.Checked
        course.FinalEvaluationTest = CrFinalEvaluationTest.Checked
      
        If txtHiddenQuest.Text <> "" Then
            course.KeyFinalTest.Id = CType(txtHiddenQuest.Text, Integer)
        End If
        
        If (CoursePk = 0) Then
            Dim NCourse As CourseValue = CatalogManager.Create(course)
            
            If Not (NCourse Is Nothing) AndAlso (NCourse.Key.Id > 0) Then
                'gestione delle associazioni content/aree
                SaveSiteArea(NCourse.Key.Id, NCourse.Key.IdLanguage)
                
                'gestione delle associazioni content/editorial
                SaveEditorial(NCourse.Key.Id, NCourse.Key.IdLanguage)
            End If
        Else
            course.Key.PrimaryKey = CoursePk
            CatalogManager.Update(course)
            
            'gestione delle associazioni content/aree
            SaveSiteArea(ContentId, course.Key.IdLanguage)
            
            'gestione delle associazioni content/editorial
            SaveEditorial(ContentId, course.Key.IdLanguage)
        End If
        
        LoadCourses()
        ActiveCourseGrid()
    End Sub
    
    Sub LoadCourseArchive(ByVal sender As Object, ByVal e As EventArgs)
        LoadCourses()
        ActiveCourseGrid()
    End Sub
    
    Sub LoadLessonArchive(ByVal sender As Object, ByVal e As EventArgs)
        CoursePk = 0
        LessonPk = 0
        LoadLesson(Nothing)
        LoadLessons()
            
        ActiveLessonDett()
    End Sub
    
    'gestisce la creazione di una nuova lezione di un corso
    Sub AddLesson(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddLesson") Then
            Dim Cpk As Integer = sender.CommandArgument
          
            CoursePk = Cpk
            LessonPk = 0
           
            LoadLesson(Nothing)
            LoadLessons()
            
            ActiveLessonDett()
        End If
    End Sub
    
    'FINE GESTIONE DEI CORSI
    '---------------------------
    
    'INIZIO GESTIONE LEZIONI
    'carica la lista delle lezioni legate al corso
    Sub LoadLessons()
        Dim catalogSearcher As New CatalogSearcher
        catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
        catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
        
        If (CoursePk <> 0) Then
            catalogSearcher.key.PrimaryKey = CoursePk
        Else
            CoursePk = txtCoursePk.Text
            catalogSearcher.key.PrimaryKey = txtCoursePk.Text
        End If
               
        CatalogManager.Cache = False
        'CatSearcher.KeySiteArea.Id = 
        catalogSearcher.KeySite.Id = Request("S")
        catalogSearcher.key.IdLanguage = srcLanguage.Language
        catalogSearcher.Active = SelectOperation.All
        catalogSearcher.Display = SelectOperation.All
        Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
        
        gdw_Lessons.DataSource = catColl
        gdw_Lessons.DataBind()
    End Sub
    
    'gestisce il caricamento della lezione nel form
    Sub LoadLesson(ByVal lesson As LessonValue)
        If Not (lesson Is Nothing) Then
            'carica i dati della lezione
            CoursePk = lesson.Key.PrimaryKey
            ' ApprovalStatus 
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(lesson.ApprovalStatus))
            Title.Text = lesson.Title
            SubTitle.Text = lesson.SubTitle
            'Abstract.Value = lesson.Abstract
            Abstract.Content = lesson.Abstract
            'FullText.Value = lesson.FullText
            FullText.Content = lesson.FullText
            
            'gestione della lingua
            lSelector.LoadLanguageValue(lesson.Key.IdLanguage)
            
            'gestione aree
            Aree.GenericCollection = GetSiteArea(lesson.Key.Id, lesson.Key.IdLanguage)
            Aree.IsNew = False
            Aree.LoadControl()
                               
            'gestione siti
            oSite.GenericCollection = GetSite(lesson.Key.Id, lesson.Key.IdLanguage)
            oSite.IsNew = False
            oSite.LoadControl()
            
            'Caricamento delle date
            If lesson.DateCreate.HasValue Then
                DateCreate.Value = lesson.DateCreate.Value
            Else
                DateCreate.Clear()
            End If
                                   
            If lesson.DatePublish.HasValue Then
                DatePublish.Value = lesson.DatePublish.Value
            Else
                DatePublish.Clear()
            End If
            
            txtHiddenContentType.Text = lesson.ContentType.Key.Id
            txtContentType.Text = lesson.ContentType.Description
            Objectives.Text = lesson.Objectives
            Duration.Text = lesson.Duration
            MinTime.Text = lesson.MinTime
            chkActive.Checked = lesson.Active
            chkDisplay.Checked = lesson.Display
            
            'dati della lezione
            lessEvalTest.Checked = lesson.FinalEvaluationTest
            lessHiddenQuest.Text = lesson.KeyFinalTest.Id
            lessTxtQuest.Text = ReadQuestionnaire(lesson.KeyFinalTest.Id)
            
        Else 'nel caso di una nuova lezione
            Title.Text = Nothing
            SubTitle.Text = Nothing
            'Abstract.Value = Nothing
            Abstract.Content = Nothing
            'FullText.Value = Nothing
            FullText.Content = Nothing
            'setta la lingua per un nuovo content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
            ' setta ApprovalStatus=Approved
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
            txtHiddenContentType.Text = 0
            txtContentType.Text = Nothing
            Objectives.Text = Nothing
            Duration.Text = 0
            MinTime.Text = 0
            chkActive.Checked = True
            chkDisplay.Checked = True
            lessEvalTest.Checked = False
            lessHiddenQuest.Text = 0
            lessTxtQuest.Text = Nothing
            
            'sito
            If (Request("S") <> Nothing) Then
                Dim site As New SiteAreaValue
                Dim coll As New SiteAreaCollection
                
                site = Me.BusinessSiteAreaManager.Read(Request("S"))
                coll.Add(site)
                
                oSite.GenericCollection = coll
            End If
            
            oSite.IsNew = True
            oSite.LoadControl()
            'aree
            Aree.IsNew = True
            Aree.LoadControl()
        End If
    End Sub
    
    'gestisce l'edit del corso
    Sub EditLesson(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SelectLesson") Then
            Dim argument As String = sender.CommandArgument
            Dim keys As String() = argument.Split("|")
            Dim PK As Integer = keys(0)
            Dim ID As Integer = keys(1)
                       
            txtCoursePk.Text = CoursePk
            LessonPk = PK
            ContentId = ID
            
            CatalogManager.Cache = False
            Dim so As New CatalogSearcher
            so.key.PrimaryKey = PK
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All
            
            Dim lessons As LessonCollection = CatalogManager.ReadLessons(so)
            If Not (lessons Is Nothing) AndAlso (lessons.Count > 0) Then
                LoadLesson(lessons(0))
                ActiveLesson()
            End If
        End If
    End Sub
    
    'gestisce la cancellazione della lezione
    Sub DeleteLesson(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "DeleteLesson") Then
            Dim PK As String = sender.CommandArgument
            
            Dim bool As Boolean = CatalogManager.Delete(New CatalogIdentificator(PK))
            If (bool) Then
                CreateJsMessage("This lesson has been cancelled correctly.")
            Else
                CreateJsMessage("Impossible to delete this lesson.")
            End If
            
            LoadCourses()
            ActiveCourseGrid()
        End If
    End Sub
    
    Sub SaveLesson(ByVal sender As Object, ByVal e As EventArgs)
        Dim lesson As New LessonValue
        lesson.Title = Title.Text
        lesson.SubTitle = SubTitle.Text
        'lesson.Abstract = Abstract.Value
        lesson.Abstract = Abstract.Content
        'lesson.FullText = FullText.Value
        lesson.FullText = FullText.Content
        'gestione della lingua
        lesson.Key.IdLanguage = lSelector.Language
        ' Gestione ApprovalStatus
        Select Case dwlApprovalStatus.SelectedItem.Value
            Case LessonValue.ApprovalWFStatus.Approved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.Approved
            Case LessonValue.ApprovalWFStatus.ToBeApproved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.ToBeApproved
            Case LessonValue.ApprovalWFStatus.NotApproved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.NotApproved
        End Select
        'date
        lesson.DateCreate = DateUpdate.Value
        lesson.DateUpdate = DateUpdate.Value
        lesson.DatePublish = DatePublish.Value
                  
        lesson.ContentType.Key.Id = txtHiddenContentType.Text
       
        lesson.Objectives = Objectives.Text
        lesson.Duration = Duration.Text
        lesson.MinTime = MinTime.Text
        lesson.Active = chkActive.Checked
        lesson.Display = chkDisplay.Checked
        lesson.FinalEvaluationTest = lessEvalTest.Checked
        
        If lessHiddenQuest.Text <> "" Then
            lesson.KeyFinalTest.Id = CType(lessHiddenQuest.Text, Integer)
        End If
        
        If (LessonPk = 0) Then
            Dim NLesson As LessonValue = CatalogManager.Create(lesson)
                                  
            If Not (NLesson Is Nothing) AndAlso (NLesson.Key.PrimaryKey > 0) Then
                'gestione associazione corso/lezione
                Dim CatalogRelV As New CatalogRelationValue
                CatalogRelV.KeyContent.PrimaryKey = CoursePk
                CatalogRelV.KeyContentRelation.PrimaryKey = NLesson.Key.PrimaryKey
                CatalogManager.CreateCatalogRelation(CatalogRelV)
                
                'gestione associazione sitearee/content
                SaveSiteArea(NLesson.Key.Id, NLesson.Key.IdLanguage)
            End If
        Else
            lesson.Key.PrimaryKey = LessonPk
            CatalogManager.Update(lesson)
            
            'gestione associazione sitearee/content
            SaveSiteArea(ContentId, lesson.Key.IdLanguage)
        End If
                
        LoadCourses()
        ActiveCourseGrid()
    End Sub
    
    'gestisce la creazione di un nuovo nodo per un corso
    Sub AddNode(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddNode") Then
            Dim Lpk As Integer = sender.CommandArgument
                       
            LessonPk = Lpk
            NodePk = 0
            LoadNode(Nothing)
            LoadNodes()
            
            ActiveNodeDett()
        End If
    End Sub
    'FINE GESTIONE DELLE LEZIONI
    '----------------------------
   
    
    'INIZIO GESTIONE DELLE NODI
    '----------------------------
    'carica la lista dei nodi legati alla lezione
    Sub LoadNodes()
        Dim catalogSearcher As New CatalogSearcher
        catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Node
        catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course

        catalogSearcher.KeySite.Id = Request("S")
        catalogSearcher.key.IdLanguage = srcLanguage.Language
        catalogSearcher.key.PrimaryKey = LessonPk
        catalogSearcher.Active = SelectOperation.All
        catalogSearcher.Display = SelectOperation.All
        
        CatalogManager.Cache = False
        Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
        
        gdw_Nodes.DataSource = catColl
        gdw_Nodes.DataBind()
    End Sub
    
    'gestisce il caricamento del nodo nel form
    Sub LoadNode(ByVal node As NodeValue)
        If Not (node Is Nothing) Then
            'carica i dati della lezione
            NodePk = node.Key.PrimaryKey
            ' ApprovalStatus 
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(node.ApprovalStatus))
            Title.Text = node.Title
            SubTitle.Text = node.SubTitle
            'Abstract.Value = node.Abstract
            Abstract.Content = node.Abstract
            'FullText.Value = node.FullText
            FullText.Content = node.FullText
            
            'gestione della lingua
            lSelector.LoadLanguageValue(node.Key.IdLanguage)
            
            'gestione aree
            Aree.GenericCollection = GetSiteArea(node.Key.Id, node.Key.IdLanguage)
            Aree.IsNew = False
            Aree.LoadControl()
                               
            'gestione siti
            oSite.GenericCollection = GetSite(node.Key.Id, node.Key.IdLanguage)
            oSite.IsNew = False
            oSite.LoadControl()
            
            'Caricamento delle date
            If node.DateCreate.HasValue Then
                DateCreate.Value = node.DateCreate.Value
            Else
                DateCreate.Clear()
            End If
                                    
            If node.DatePublish.HasValue Then
                DatePublish.Value = node.DatePublish.Value
            Else
                DatePublish.Clear()
            End If
            
            txtHiddenContentType.Text = node.ContentType.Key.Id
            txtContentType.Text = node.ContentType.Description
            Objectives.Text = node.Objectives
            Duration.Text = node.Duration
            MinTime.Text = node.MinTime
            chkActive.Checked = node.Active
            chkDisplay.Checked = node.Display
            
            'node value
            dwlClassNode.SelectedIndex = dwlClassNode.Items.IndexOf(dwlClassNode.Items.FindByValue(node.ClassNode))
        Else 'nel caso di un nuovo nodo
            ' ApprovalStatus 
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
            Title.Text = Nothing
            SubTitle.Text = Nothing
            'Abstract.Value = Nothing
            Abstract.Content = Nothing
            'FullText.Value = Nothing
            FullText.Content = Nothing
            'setta la lingua per un nuovo content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
            
            txtHiddenContentType.Text = 0
            txtContentType.Text = Nothing
            Objectives.Text = Nothing
            Duration.Text = 0
            MinTime.Text = 0
            chkActive.Checked = True
            chkDisplay.Checked = True
            
            dwlClassNode.SelectedIndex = dwlClassNode.Items.IndexOf(dwlClassNode.Items.FindByValue(NodeValue.Classes.Argument))
            
            'sito
            If (Request("S") <> Nothing) Then
                Dim site As New SiteAreaValue
                Dim coll As New SiteAreaCollection
                site = Me.BusinessSiteAreaManager.Read(Request("S"))
                coll.Add(site)
               
                oSite.GenericCollection = coll
            End If
            
            oSite.IsNew = True
            oSite.LoadControl()
            'aree
            Aree.IsNew = True
            Aree.LoadControl()
        End If
    End Sub
    
    Sub EditNode(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SelectNode") Then
            Dim argument As String = sender.CommandArgument()
            Dim keys As String() = argument.Split("|")
            Dim PK As Integer = keys(0)
            Dim ID As Integer = keys(1)
           
            NodePk = PK
            ContentId = ID
            
            CatalogManager.Cache = False
            Dim so As New CatalogSearcher
            so.key.PrimaryKey = PK
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All
        
            Dim nodes As NodeCollection = CatalogManager.ReadNodes(so)
            If Not (nodes Is Nothing) AndAlso (nodes.Count > 0) Then
                LoadNode(nodes(0))
                ActiveNode()
            End If
        End If
    End Sub
    
    'gestisce la cancellazione del nodo
    Sub DeleteNode(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "DeleteNode") Then
            Dim PK As String = sender.CommandArgument
            
            Dim bool As Boolean = CatalogManager.Delete(New CatalogIdentificator(PK))
            If (bool) Then
                CreateJsMessage("This node has been cancelled correctly.")
            Else
                CreateJsMessage("Impossible to delete this node.")
            End If
            
            LoadCourses()
            ActiveCourseGrid()
        End If
    End Sub

    Sub SaveNode(ByVal sender As Object, ByVal e As EventArgs)
        Dim node As New NodeValue
        node.Title = Title.Text
        node.SubTitle = SubTitle.Text
        node.Abstract = Abstract.Content
        node.FullText = FullText.Content
        
        'gestione della lingua
        node.Key.IdLanguage = lSelector.Language
               
        'date
        node.DateCreate = DateUpdate.Value
        node.DateUpdate = DateUpdate.Value
        node.DatePublish = DatePublish.Value
                  
        node.ContentType.Key.Id = txtHiddenContentType.Text
       
        node.Objectives = Objectives.Text
        node.Duration = Duration.Text
        node.MinTime = MinTime.Text
        node.Active = chkActive.Checked
        node.Display = chkDisplay.Checked
            
        Select Case dwlClassNode.SelectedValue
            Case NodeValue.Classes.Argument
                node.ClassNode = NodeValue.Classes.Argument
            Case NodeValue.Classes.Bibliografic
                node.ClassNode = NodeValue.Classes.Bibliografic
            Case NodeValue.Classes.Chapter
                node.ClassNode = NodeValue.Classes.Chapter
            Case NodeValue.Classes.Paragraph
                node.ClassNode = NodeValue.Classes.Paragraph
        End Select
        
        'ApprovalStatus
        Select Case dwlApprovalStatus.SelectedItem.Value
            Case NodeValue.ApprovalWFStatus.Approved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.Approved
            Case ContentValue.ApprovalWFStatus.ToBeApproved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.ToBeApproved
            Case ContentValue.ApprovalWFStatus.NotApproved
                node.ApprovalStatus = NodeValue.ApprovalWFStatus.NotApproved
        End Select
        If (NodePk = 0) Then
            Dim NNode As NodeValue = CatalogManager.Create(node)
            
            If Not (NNode Is Nothing) AndAlso (NNode.Key.PrimaryKey > 0) Then
                'gestione associazione lezione/nodo
                Dim CatalogRelV As New CatalogRelationValue
                CatalogRelV.KeyContent.PrimaryKey = LessonPk
                CatalogRelV.KeyContentRelation.PrimaryKey = NNode.Key.PrimaryKey
                CatalogManager.CreateCatalogRelation(CatalogRelV)
                
                'gestione associazione sitearee/content
                SaveSiteArea(NNode.Key.Id, NNode.Key.IdLanguage)
            End If
        Else
            node.Key.PrimaryKey = NodePk
            CatalogManager.Update(node)
            
            'gestione associazione sitearee/content
            SaveSiteArea(ContentId, node.Key.IdLanguage)
        End If
                
        LoadCourses()
        ActiveCourseGrid()
    End Sub
    'FINE GESTIONE DEI NODI
    '----------------------------
            
    'restituisce la descrizione del questionario
    Function ReadQuestionnaire(ByVal id As Integer) As String
        If id = 0 Then Return Nothing
        
        Dim QuestManager As New QuestionnaireManager
        
        Dim QuestV As QuestionnaireValue = QuestManager.Read(New QuestionnaireIdentificator(id))
        If Not (QuestV Is Nothing) Then
            Return QuestV.Description
        End If
        
        Return Nothing
    End Function
    
    'salva le associazioni content/editorial
    Function SaveEditorial(ByVal contentId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim ContentIdent As New ContentIdentificator(contentId, languageId)
        Dim res As Boolean = Me.BusinessContentManager.RemoveAllContentEditorial(ContentIdent)
       
        If Not ctlCE.GetSelection Is Nothing AndAlso ctlCE.GetSelection.Count > 0 Then
            Dim EditorialColl As New Object
            EditorialColl = ctlCE.GetSelection
           
            For Each editorial As ContentEditorialValue In EditorialColl
                Dim editorialRel = New ContentContentEditorialValue

                editorialRel.KeyContent = ContentIdent
                editorialRel.IdContentEditorial = editorial.Id

                Me.BusinessContentManager.CreateContentEditorial(editorialRel)
            Next
        End If
    End Function
    
    'salva le associazioni content/sitearea
    Function SaveSiteArea(ByVal contentId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim ContentIdent As New ContentIdentificator(contentId, languageId)
        Dim res As Boolean = Me.BusinessContentManager.RemoveAllContentSiteArea(ContentIdent)
       
        If Not Aree.GetSelection Is Nothing AndAlso Aree.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = Aree.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
        
        If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = oSite.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
    End Function
      
        
    'recupera tutti le sitearea associate ad un content (corso, lezione, nodo)
    Function GetSiteArea(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Area)
    End Function
      
    'recupera tutti le sitearea di tipo sito associate ad un content (corso, lezione, nodo)
    Function GetSite(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Site)
    End Function
    
    'restituisce il tipo del corso
    Function getCourseType(ByVal course As CourseValue) As String
        Select Case course.CourseType
            Case CourseValue.CourseTypes.BlendedLearning
                Return "Blended Learning"
            Case CourseValue.CourseTypes.DistanceLearning
                Return "Distance Learning"
            Case CourseValue.CourseTypes.ResidentialCourse
                Return "Residential Course"
        End Select
        Return ""
    End Function
    
    Sub ContentRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ContentRel") Then
            Dim cPk As Integer = sender.CommandArgument
                
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "ContCatalg")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    Sub ChapterRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ChapterRel") Then
            Dim cPk As Integer = sender.CommandArgument
                
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "ChaptRel")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
   
    Sub AddUserRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddUser") Then
            Dim argument As String = sender.CommandArgument
            Dim keys As String() = argument.Split("|")
            
            Dim cPk As Integer = keys(0)
            Dim cId As Integer = keys(1)
            
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "CourseUser")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    Sub ShowPresentation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ShowPres") Then
            Dim argument As String = sender.CommandArgument
            Dim cPk As Integer = CType(argument, Integer)
                                  
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "showPpt")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
        
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveCourseGrid()
        pnlCoursesGrid.Visible = True
        
        divSaveCourse.Visible = False
        pnlCatalogDett.Visible = False
        pnlCourseDett.Visible = False
        pnlLessonDett.Visible = False
        divSaveLesson.Visible = False
        pnlLessonGrid.Visible = False
        divSaveNode.Visible = False
        pnlNodeDett.Visible = False
        pnlNodeGrid.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveCourseDett()
        divSaveCourse.Visible = True
        pnlCatalogDett.Visible = True
        pnlCourseDett.Visible = True
        
        pnlCoursesGrid.Visible = False
        divSaveLesson.Visible = False
        pnlLessonGrid.Visible = False
        pnlLessonDett.Visible = False
    End Sub
    
    'usata nella gestione dell'edit
    Sub ActiveLesson()
        divSaveLesson.Visible = True
        btnArchive_LEdit.Visible = True
        
        btnArchive_LNew.Visible = False
        pnlCatalogDett.Visible = True
        pnlLessonDett.Visible = True
        
        pnlLessonGrid.Visible = False
        pnlCoursesGrid.Visible = False
        divSaveCourse.Visible = False
        pnlCourseDett.Visible = False
        pnlNodeGrid.Visible = False
        btnAnc_L.Visible = False
    End Sub
    
    Sub ActiveLessonDett()
        divSaveLesson.Visible = True
        btnArchive_LNew.Visible = True
       
        btnArchive_LEdit.Visible = False
        pnlCatalogDett.Visible = True
        pnlLessonDett.Visible = True
        pnlLessonGrid.Visible = True
        
        pnlCoursesGrid.Visible = False
        divSaveCourse.Visible = False
        pnlCourseDett.Visible = False
        pnlNodeGrid.Visible = False
        pnlNodeGrid.Visible = False
        divSaveNode.Visible = False
        pnlNodeDett.Visible = False
        pnlNodeGrid.Visible = False
    End Sub
    
    'usata nella gestione dell'edit
    Sub ActiveNode()
        divSaveNode.Visible = True
        pnlCatalogDett.Visible = True
        pnlNodeDett.Visible = True
        
        pnlNodeGrid.Visible = False
        pnlLessonGrid.Visible = False
        divSaveLesson.Visible = False
        pnlCoursesGrid.Visible = False
        pnlCourseDett.Visible = False
        pnlLessonDett.Visible = False
        divSaveCourse.Visible = False
        btnAnc_N.Visible = False
    End Sub
    
    Sub ActiveNodeDett()
        divSaveNode.Visible = True
        pnlCatalogDett.Visible = True
        pnlNodeDett.Visible = True
        pnlNodeGrid.Visible = True
        
        pnlLessonGrid.Visible = False
        divSaveLesson.Visible = False
        pnlLessonDett.Visible = False
        pnlCoursesGrid.Visible = False
        pnlCourseDett.Visible = False
        divSaveCourse.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
   
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'gestisce la ricerca degli corsi 
    Sub SearchCourse(ByVal sender As Object, ByVal e As EventArgs)
        If Not (Request("S") Is Nothing) Then
            BindWithSearch(gdw_Courses)
        End If
    End Sub
    
    ''' <summary>
    ''' Caricamento della griglia degli utenti
    ''' Verifica la presenza di criteri di ricerca
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim courseSearcher As New CatalogSearcher
        
        If CourId.Text <> "" Then courseSearcher.key.Id = CourId.Text()
        If CourPk.Text <> "" Then courseSearcher.key.PrimaryKey = CourPk.Text
        If CourTitle.Text <> "" Then courseSearcher.SearchString = CourTitle.Text
       
        courseSearcher.key.IdLanguage = srcLanguage.Language
        courseSearcher.KeySite.Id = Request("S")
        
        Dim oSiteAreaValue As SiteAreaValue
        oSiteAreaValue = GetSiteSelection(srcArea)
        If (Not oSiteAreaValue Is Nothing) Then
            courseSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
        End If
            
        If (srcContentType.SelectedItem.Value <> -1) Then
            courseSearcher.KeyContentType.Id = srcContentType.SelectedItem.Value
        End If
        
        If (dwlTypeCourse.SelectedItem.Value <> 0) Then
            courseSearcher.CourseType = dwlTypeCourse.SelectedItem.Value
        End If
                     
        BindGrid(objGrid, courseSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As CatalogSearcher = Nothing)
        Dim CourseCollection As New CourseCollection
        If Searcher Is Nothing Then
            Searcher = New CatalogSearcher
        End If
        
        CatalogManager.Cache = False
        CourseCollection = CatalogManager.ReadCourses(Searcher)
        'If Not CourseCollection Is Nothing Then
           
        '    CourseCollection.Sort(SortType, SortOrder)
        'End If
        
        objGrid.DataSource = CourseCollection
        objGrid.DataBind()
    End Sub
        
    Sub TakeDownload(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim pk As String = sender.CommandArgument()
        Dim filename As String = pk & ".pdf"
        
        Dim path As String = "/" & ConfigurationsManager.DownloadPath & "/PPT/" & pk & "/" & filename
      
        If StreamUtility.FileExists(SystemUtility.CurrentMapPath("/" & path)) Then
            System.Web.HttpContext.Current.Response.Clear()
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" & filename)
            System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream"
            System.Web.HttpContext.Current.Server.Transfer(path)
          
        End If
    End Sub
    
    Sub ExportCatalogs(ByVal sender As Object, ByVal e As EventArgs)
        BindContentType(expContentType)
        
        If Not (Request("S") Is Nothing) Then
            expSite.SetSelectedValue(Request("S"))
        End If
        
        pnlExport.Visible = True
        pnlImport.Visible = False
    End Sub
    
    Sub ImportCatalogs(ByVal sender As Object, ByVal e As EventArgs)
        BindContentType(impContentType)
        If Not (Request("S") Is Nothing) Then
            impSite.SetSelectedValue(Request("S"))
        End If
        
        pnlExport.Visible = False
        pnlImport.Visible = True
    End Sub
    
    Sub CloseImport(ByVal sender As Object, ByVal e As EventArgs)
        pnlImport.Visible = False
    End Sub
    
    Sub Import(ByVal sender As Object, ByVal e As EventArgs)
        Dim Path As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.DownloadPath & "\tmpDownloadPath"
        
        Dim oSite As SiteAreaValue = GetSiteSelection(impSite)
        Dim oSiteArea As SiteAreaValue = GetSiteSelection(impArea)
        
        If (UploadXml.HasFile) And (Not oSite Is Nothing) And (Not oSiteArea Is Nothing) And (impContentType.SelectedValue <> -1) Then
            If Not IO.File.Exists(Path) Then
                IO.Directory.CreateDirectory(Path)
            End If
        
            Path = Path & "\" & UploadXml.FileName
            UploadXml.PostedFile.SaveAs(Path)
              
            Dim col As Object
            Dim vo As Object
            Dim type As String = Nothing
            
            Select Case impType.SelectedValue
                Case CatalogValue.CatalogSubTypes.Course
                    col = New CourseCollection
                    vo = New CourseValue
                    type = vo.GetType.Name
                    
                Case CatalogValue.CatalogSubTypes.Lesson
                    col = New LessonCollection
                    vo = New LessonValue
                    type = vo.GetType.Name
                    
                Case CatalogValue.CatalogSubTypes.Node
                    col = New NodeCollection
                    vo = New NodeValue
                    type = vo.GetType.Name
            End Select
        
            Dim obj As Object
            Dim objFile As IO.StreamReader = IO.File.OpenText(Path)
            Dim strFile As String = objFile.ReadToEnd()
            objFile.Close()

            IO.File.Delete(Path)
            If (strFile.ToString.Contains(type)) Then
                obj = ClassUtility.DeserializeXmlStringToObject(strFile, col.GetType)
        
                If Not (obj Is Nothing) Then
                    For Each vo In obj
                        
                        If Not (impckUpdate.Checked) Then
                            vo.Key.PrimaryKey = 0
                            vo.Key.Id = 0
                           
                            vo.DateCreate = DateTime.Now
                            vo.DateInsert = DateTime.Now
                            vo.DatePublish = DateTime.Now
                            vo.DateInsert = DateTime.Now
                            vo.DatePublish = DateTime.Now
                    
                            vo.ContentType.Key.Id = impContentType.SelectedValue
                            vo.Key.IdLanguage = impLanguage.Language
                    
                            vo = CatalogManager.Create(vo)
                    
                            If Not (vo Is Nothing) AndAlso (vo.Key.Id > 0) Then
                                Dim _oContentSiteArea As New ContentSiteAreaValue
                                _oContentSiteArea.KeySiteArea = oSiteArea.Key
                                _oContentSiteArea.KeyContent = vo.Key
                                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
                        
                                _oContentSiteArea.KeySiteArea = oSite.Key
                                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
                            End If
                        Else
                            'nell' update non gestisce la modifica di area e site
                            vo.ContentType.Key.Id = impContentType.SelectedValue
                            vo.Key.IdLanguage = impLanguage.Language
                            
                            vo.DateInsert = DateTime.Now
                            vo.DatePublish = DateTime.Now
                           
                            vo = CatalogManager.Update(vo)
                        End If
                    Next
                
                    LoadCourses()
                    CreateJsMessage("Import completed successfully!")
                End If
            Else
                CreateJsMessage("There is an inconsistency between the uploaded file and selected type")
            End If
            
        Else
            CreateJsMessage("Sorry, select all import criteria and try again!")
        End If
    End Sub
    
    Sub CloseExport(ByVal sender As Object, ByVal e As EventArgs)
        pnlExport.Visible = False
    End Sub
    
    Sub Export(ByVal sender As Object, ByVal e As EventArgs)
        Dim CatSearcher As New CatalogSearcher
        Dim oSiteAreaValue As SiteAreaValue
        
        oSiteAreaValue = GetSiteSelection(expSite)
        If (Not oSiteAreaValue Is Nothing) Then
            CatSearcher.KeySite.Id = oSiteAreaValue.Key.Id
        End If
                
        oSiteAreaValue = GetSiteSelection(expArea)
        If (Not oSiteAreaValue Is Nothing) Then
            CatSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
        End If
      
        If expContentType.SelectedValue <> -1 Then
            CatSearcher.KeyContentType.Id = expContentType.SelectedItem.Value
        End If
            
        CatSearcher.key.IdLanguage = expLanguage.Language
        'Response.Write("prima" & expID.Text)
        If (expID.Text <> Nothing) Then
            'Response.Write("in" & CatSearcher.key.Id & "-" & CatSearcher.key.PrimaryKey & "</br>")
            CatSearcher.key.Id = expID.Text
            CatSearcher.key.PrimaryKey = Me.BusinessContentManager.ReadPrimaryKey(CType(expID.Text, Integer), CatSearcher.key.IdLanguage)
        End If
        
        CatSearcher.Active = SelectOperation.All
        CatSearcher.Display = SelectOperation.All
        
        Dim objColl As Object
        CatalogManager.Cache = False
        
        Dim Type As String = Nothing
        Select Case expType.SelectedValue
            Case CatalogValue.CatalogSubTypes.Course
                Type = CatalogValue.CatalogSubTypes.Course.ToString
                objColl = CatalogManager.ReadCourses(CatSearcher)
                
            Case CatalogValue.CatalogSubTypes.Lesson
                Type = CatalogValue.CatalogSubTypes.Lesson.ToString
                
                If (expRefCourse.Text <> Nothing) Then 'recupera tutte le lezioni relazionate ald un certo corso
                    CatSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
                    CatSearcher.IdContentSubType = ContentValue.Subtypes.Course
                    
                    Dim vo As ContentValue = Me.BusinessContentManager.Read(CType(expRefCourse.Text, Integer), CatSearcher.key.IdLanguage)
                    
                    If Not (vo Is Nothing) Then
                        CatalogManager.Cache = False
                        CatSearcher.key.PrimaryKey = vo.Key.PrimaryKey
                        Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(CatSearcher)
                       
                        If Not (catColl Is Nothing) AndAlso (catColl.Count > 0) Then
                            Dim lessColl As New LessonCollection
                            
                            Dim catalogSearcher As New CatalogSearcher
                            catalogSearcher.Active = SelectOperation.All
                            catalogSearcher.Display = SelectOperation.All
                            
                            For Each elem As CatalogValue In catColl
                                catalogSearcher.key.PrimaryKey = elem.Key.PrimaryKey
                                Dim lessC As LessonCollection = CatalogManager.ReadLessons(catalogSearcher)
                                
                                If Not (lessC Is Nothing) AndAlso (lessC.Count > 0) Then
                                    lessColl.Add(lessC(0))
                                End If
                            Next
                            
                            objColl = lessColl
                        End If
                    End If
                Else ' recupera tutte le lezioni
                    
                    objColl = CatalogManager.ReadLessons(CatSearcher)
                End If
                
            Case CatalogValue.CatalogSubTypes.Node
                Type = CatalogValue.CatalogSubTypes.Node.ToString
               
                'If (expRefLesson.Text <> Nothing) Then 'recupera tutte le lezioni relazionate ald un certo corso
                '    Dim MapNodesSearcher As New NodeRelationSearcher
                '    CatalogManager.Cache = False
                '    Dim vo As ContentValue = Me.BusinessContentManager.Read(CType(expRefLesson.Text, Integer), CatSearcher.key.IdLanguage)
                                    
                '    If Not (vo Is Nothing) Then
                '        MapNodesSearcher.KeyLesson.PrimaryKey = vo.Key.PrimaryKey
                '        CatalogManager.Cache = False
                '        Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
                        
                '        If Not (nodesRelColl Is Nothing) AndAlso (nodesRelColl.Count > 0) Then
                '            Dim nodeColl As New NodeCollection
                '            For Each elem As NodeRelationValue In nodesRelColl
                '                CatSearcher.key.PrimaryKey = elem.KeyEndNode.PrimaryKey
                '                Dim nodeC As NodeCollection = CatalogManager.ReadNodes(CatSearcher)
                                
                '                If Not (nodeC Is Nothing) AndAlso (nodeC.Count > 0) Then
                '                    nodeColl.Add(nodeC(0))
                '                End If
                '            Next
                            
                '            objColl = nodeColl
                '        End If
                '    End If
                'Else 'recupera tutte le lezioni relazionate ald un certo corso
                    
                '    objColl = CatalogManager.ReadNodes(CatSearcher)
                'End If
                
                If (expRefCourse.Text <> Nothing) Then 'recupera tutte le lezioni relazionate ald un certo corso
                    CatSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
                    CatSearcher.IdContentSubType = ContentValue.Subtypes.Course
                    
                    Dim vo As ContentValue = Me.BusinessContentManager.Read(CType(expRefCourse.Text, Integer), CatSearcher.key.IdLanguage)
                    
                    If Not (vo Is Nothing) Then
                        CatalogManager.Cache = False
                        CatSearcher.key.PrimaryKey = vo.Key.PrimaryKey
                        Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(CatSearcher)
                        
                        Dim MapNodesSearcher As NodeRelationSearcher
                        If Not (catColl Is Nothing) AndAlso (catColl.Count > 0) Then
                            Dim nodeColl As NodeCollection = Nothing
                            
                            For Each lesson As CatalogValue In catColl
                                MapNodesSearcher = New NodeRelationSearcher
                            
                                MapNodesSearcher.KeyLesson.PrimaryKey = lesson.Key.PrimaryKey
                                CatalogManager.Cache = False
                                Dim nodesRelColl As NodeRelationCollection = CatalogManager.Read(MapNodesSearcher)
                          
                                If Not (nodesRelColl Is Nothing) AndAlso (nodesRelColl.Count > 0) Then
                                    For Each node As NodeRelationValue In nodesRelColl
                                        CatSearcher.key.PrimaryKey = node.KeyEndNode.PrimaryKey
                                        CatSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Node
                                        CatSearcher.IdContentSubType = ContentValue.Subtypes.Course
                                    
                                        Dim nodeC As NodeCollection = CatalogManager.ReadNodes(CatSearcher)
                                
                                        If Not (nodeC Is Nothing) AndAlso (nodeC.Count > 0) And (nodeColl Is Nothing) Then
                                            nodeColl = New NodeCollection
                                        End If
                                        
                                        If Not (nodeC Is Nothing) AndAlso (nodeC.Count > 0) Then
                                            nodeColl.Add(nodeC(0))
                                        End If
                                    Next
                                End If
                            Next
                        
                            objColl = nodeColl
                        End If
                    End If
                Else 'recupera tutte le lezioni relazionate ald un certo corso
                    objColl = CatalogManager.ReadNodes(CatSearcher)
                End If
        End Select
                
        'crea il file se la collection di oggetti � piena  
        If (Not objColl Is Nothing) Then
            Dim filename As String = "HP3Export-" + Type + "-" + LangManager.GetCodeLanguage(expLanguage.Language) + "-" + DateTime.Now.ToUniversalTime.ToString("yyyy-MM-dd hhmmss") + ".xml"
            Dim str As String = ClassUtility.SerializeObjectToXmlString(objColl, Encoding.UTF8)
            Response.Clear()
            Response.AddHeader("content-disposition", "fileattachment;filename=" + filename)
            Response.ContentType = "text/xml"
            Response.Write(str)
            Response.End()
        Else
            CreateJsMessage("Sorry, there aren't items to export.Select the right search criteria and try again!")
        End If
    End Sub
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    'popola la dwl per i contentType
    Sub BindContentType(ByVal dwl As DropDownList)
        Dim ContentTypeSearcher As New ContentTypeSearcher
        Dim contentTypeColl As ContentTypeCollection = Me.BusinessContentTypeManager.Read(ContentTypeSearcher)
    
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwl, contentTypeColl, "Description", "Key.Id")
        
        dwl.Items.Insert(0, New ListItem("---Select ContentType---", -1))
    End Sub
    
    Function GetVisibility(ByVal pk As Integer, ByVal ext As String) As Boolean
        Dim filename As String = ""
        Select Case ext
            Case JPG
                filename = "1" & "." & ext
            Case PDF
                filename = pk & "." & ext
        End Select
       
        Dim path As String = "/" & ConfigurationsManager.DownloadPath & "/PPT/" & pk & "/" & filename
        If StreamUtility.FileExists(SystemUtility.CurrentMapPath("/" & path)) Then
            Return True
        Else
            Return False
        End If
    End Function
 
    Function getClassNode(ByVal pk As Integer) As String
        Dim vo As NodeValue = CatalogManager.ReadNode(New ContentIdentificator(pk))
        
        If Not (vo Is Nothing) Then
            Return vo.ClassNode.ToString
        End If
        Return Nothing
    End Function
    
    Sub ActivePanel(ByVal sender As Object, ByVal e As System.EventArgs)
        If (expType.SelectedValue = CatalogValue.CatalogSubTypes.Course) Then
            divRefCourse.Visible = False
            divRefLess.Visible = False
            
            expRefLesson.Text = Nothing
            expRefCourse.Text = Nothing
        End If
        
        If (expType.SelectedValue = CatalogValue.CatalogSubTypes.Lesson) Then
            expID.Text = Nothing
            expRefCourse.Text = Nothing
            
            'divRefCourse.Visible = True
            'divRefLess.Visible = False
            
            divRefLess.Visible = False
            divRefCourse.Visible = True
        End If
        
        If (expType.SelectedValue = CatalogValue.CatalogSubTypes.Node) Then
            expID.Text = Nothing
            expRefLesson.Text = Nothing
                      
            'divRefLess.Visible = True
            'divRefCourse.Visible = False
            
            'divRefLess.Visible = True
            divRefCourse.Visible = True
        End If
    End Sub
  </script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<script type="text/jscript">
function goToLAnchor()
{
    location.href="#LessonList"
    return false;
}

function goToNAnchor()
{
    location.href="#NodesList"
    return false;
}
</script>
<hr/>

<!--Pannello dei corsi (archivio)-->
<asp:Label id="txtCoursePk" runat="server" Visible="false"></asp:Label>
<asp:Panel id="pnlCoursesGrid" runat="server" visible="false"> 
 <div>
   <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewCourse"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
     <table class="topContentSearcher">
         <tr>
              <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
              <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Course Searcher</span></td>
         </tr>
       </table>
       <fieldset class="_Search">
       <table class="form">
          <tr>
             <td style="width:50px"><strong>ID</strong></td>      
             <td style="width:50px"><strong>PK</strong></td>    
             <td><strong>Title</strong></td>
          </tr>
          <tr>
             <td style="width:50px"><HP3:Text runat="server" ID="CourId" TypeControl="TextBox" Style="width:50px"/></td>
             <td style="width:50px"><HP3:Text runat="server" ID="CourPk" TypeControl="TextBox" Style="width:50px"/></td>
             <td><HP3:Text runat="server" ID="CourTitle" TypeControl="TextBox" Style="width:400px"/></td>  
      </table>
      <table class="form">    
          <tr>
              <td><strong>Language</strong></td>
              <td><strong>Area</strong></td>
              <td><strong>ContentType</strong></td>
              <td><strong>Type</strong></td> 
          </tr>
          <tr>
              <td><HP3:contentLanguage ID="srcLanguage" runat="server" Typecontrol="StandardControl" /></td>  
              <td><HP3:ctlSite runat="server" ID="srcArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
              <td><asp:DropDownList ID="srcContentType" runat="server" /></td>
              <td>
                <asp:DropDownList id="dwlTypeCourse" runat="server">
                    <asp:ListItem Text="Distance Learning" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Residential Course" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Blended Learning" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
          </tr>
      </table>
      <table class="form">   
          <tr>
              <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchCourse" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
          </tr>
          <tr>
          <td><asp:Button ID="btnExport" Text="Export" runat="server" OnClick="ExportCatalogs"  CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/import_export.png');background-repeat:no-repeat;background-position:1px 1px;" /></td>
             <td><asp:Button ID="btnImport" Text="Import" runat="server" OnClick="ImportCatalogs"  CssClass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/import_export.png');background-repeat:no-repeat;background-position:1px 1px;" /></td>
          </tr>
    </table>
      </fieldset>
      <hr />
  </asp:Panel>
  
  <asp:Panel ID="pnlExport" visible="false" runat="server">
        <hr />
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_import_export.png');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Export Courses</span></td>
            </tr>
            </table>
                
            <table class="form" style="background-color:#F3F3F3; width:100%">
                <tr> 
                    <td><strong>Type</strong></td>
                    <td>
                        <asp:DropDownList id="expType" runat="server" OnSelectedIndexChanged="ActivePanel" AutoPostBack="true">
                            <asp:ListItem Text="Courses" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Lessons" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Nodes" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td><strong>Site</strong></td>
                    <td><HP3:ctlSite runat="server" ID="expSite" TypeControl ="combo" SiteType="Site" ItemZeroMessage="---Select Site---"/></td>
                    <td><strong>Area</strong></td>
                    <td><HP3:ctlSite runat="server" ID="expArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
               </tr>
               <tr>
                    <td><strong>ContentType</strong></td> 
                    <td><asp:DropDownList ID="expContentType" runat="server" /></td>
                    <td><strong>Language</strong></td>
                    <td><HP3:contentLanguage ID="expLanguage" runat="server"  Typecontrol="StandardControl" /></td>
               </tr>
               <tr>
                    <td><strong>ID</strong></td> 
                    <td><HP3:Text runat="server" ID="expID" TypeControl="TextBox" Style="width:50px"/></td>
                    <div id="divRefCourse" runat="server" visible="false">
                        <td><strong>Ref. Course</strong></td> 
                        <td><HP3:Text runat="server" ID="expRefCourse" TypeControl="TextBox" Style="width:50px"/></td>
                    </div>
                    <div id="divRefLess" runat="server" visible="false"> 
                        <td><strong>Ref. Lesson</strong></td> 
                        <td><HP3:Text runat="server" ID="expRefLesson" TypeControl="TextBox" Style="width:50px"/></td>
                    </div>
               </tr>
            </table>
            <table class="form">
               <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btn_export"  Text="Start Export"  OnClick="Export" style="margin-top:10px"/></td>
                    <td><asp:button runat="server" CssClass="button" ID="btn_closeexport" Text="Close" OnClick="CloseExport" style="margin-top:10px"/></td>
                </tr>
            </table>
        <hr />
    </asp:Panel>

    <asp:Panel ID="pnlImport" visible="false" runat="server">
        <hr />
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_import_export.png');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Import Courses</span></td>
            </tr>
            </table>
                
            <table class="form" style="background-color:#F3F3F3; width:100%">
                <tr> 
                    <td><strong>Type</strong></td>
                    <td>
                        <asp:DropDownList id="impType" runat="server">
                            <asp:ListItem Text="Courses" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Lessons" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Nodes" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td><strong>Site</strong></td>
                    <td><HP3:ctlSite runat="server" ID="impSite" TypeControl ="combo" SiteType="Site" ItemZeroMessage="---Select Site---"/></td>
                    <td><strong>Area</strong></td>
                    <td><HP3:ctlSite runat="server" ID="impArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
               </tr>
               <tr>
                    <td><strong>ContentType</strong></td> 
                    <td><asp:DropDownList ID="impContentType" runat="server" /></td>
                    <td><strong>Language</strong></td>
                    <td><HP3:contentLanguage ID="impLanguage" runat="server"  Typecontrol="StandardControl" /></td>
                </tr>
                <tr>
                    <td><strong>Is Update</strong></td>
                    <td><asp:CheckBox id="impckUpdate" runat="server"/></td>
                </tr>
            </table>
            <table class="form" style="background-color:#F3F3F3; width:100%">
                <tr>
                    <td><strong>Upload File</strong></td>
                    <td><asp:FileUpload ID="UploadXml"  Width="500px" runat="server" /></td>
                </tr>
            </table>   
            <table class="form">
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btn_import"  Text="Start Import"  OnClick="Import" style="margin-top:10px"/></td>
                    <td><asp:button runat="server" CssClass="button" ID="btn_closeimport" Text="Close" OnClick="CloseImport" style="margin-top:10px"/></td>
                </tr>
            </table>
        <hr />
    </asp:Panel>
    
 <asp:Label CssClass="title" runat="server" id="sectionTit">Courses List</asp:Label>
 <asp:gridview id="gdw_Courses" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No items available">
              <Columns >
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate><img alt="Language: <%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" /></ItemTemplate> 
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Title">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="4%" HeaderText="">
                        <ItemTemplate>
                             <asp:imagebutton ID="imgPdf" ImageUrl="~/hp3Office/HP3Image/ico/file_pdf.gif" onclick="TakeDownload" runat="server"   visible="<%#GetVisibility(Container.DataItem.Key.PrimaryKey, PDF) %>" CommandArgument ='<%#Container.DataItem.Key.PrimaryKey%>'/>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Type Course">
                        <ItemTemplate><%#getCourseType(Container.DataItem)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%">
                       <ItemTemplate>
                            <asp:ImageButton id="lnkAddL" runat="server" ToolTip ="Add lesson" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif" OnClick="AddLesson" CommandName ="AddLesson" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                            <asp:ImageButton id="lnkSelectC" runat="server" ToolTip ="Edit" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditCourse" CommandName ="SelectCourse" CommandArgument= '<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDeleteC" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteCourse" CommandName ="DeleteCourse" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                            <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                            <asp:ImageButton ID="lnkUserRelation" ToolTip ="Add Users" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_user.gif" onClick="AddUserRelation" CommandName ="AddUser" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                            <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageCoverUpload.aspx?codeLang=<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>&idLang=<%#Container.DataItem.Key.IdLanguage%>&idContent=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','CoverImage','width=550,height=350')" style="cursor:pointer" title="Image Cover" alt="" />
                            <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/banner_add.gif" onclick="window.open('<%=Domain()%>/Popups/popUploadPresentation.aspx?pkContent=<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>','Presentation','width=550,height=350')" style="cursor:pointer" title="Upload Presentation" alt="" />
                            <asp:ImageButton ID="lnkShowPresent" ToolTip ="Show Presentation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/show_presentation.png"  visible="<%#GetVisibility(Container.DataItem.Key.PrimaryKey, JPG) %>" onClick="ShowPresentation" CommandName ="ShowPres" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>' />
                            <%--<asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton id="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteQuest" CommandName ="DeleteQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<!--Div per i bottoni Save/Archive della gestione dei corsi -->
<div id="divSaveCourse" runat="server" visible="false">
    <asp:Button id ="btnArchive_C" runat="server" Text="Archive" OnClick="LoadCourseArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnSave_C" runat="server" Text="Save" OnClick="SaveCourse" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 <hr />
</div>

<!--Div per i bottoni Save/Archive della gestione delle lezioni -->
<div id="divSaveLesson" runat="server" visible="false">
    <asp:Button id ="btnArchive_LNew" runat="server" Text="Archive" OnClick="LoadCourseArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnArchive_LEdit" runat="server" Text="Archive" OnClick="LoadLessonArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnSave_L" runat="server" Text="Save" OnClick="SaveLesson" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button ID="btnAnc_L" runat="server" text="Go Lessons List" EnableViewState="false" CssClass="button" OnClientClick="return goToLAnchor()" style="margin-top:10px;margin-bottom:5px" />
<hr />
</div>

<!--Div per i bottoni Save/Archive della gestione dei Nodi -->
<div id="divSaveNode" runat="server" visible="false">
    <asp:Button id ="btnArchive_N" runat="server" Text="Archive" OnClick="LoadCourseArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnSave_N" runat="server" Text="Save" OnClick="SaveNode" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button ID="btnAnc_N" runat="server" text="Go Nodes List" EnableViewState="false" CssClass="button" OnClientClick="return goToNAnchor()" style="margin-top:10px;margin-bottom:5px" />
 <hr />
</div>

<!--Pannello del dettaglio del catalog object e della lezione(generico)-->
<asp:Panel id="pnlCatalogDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="99%">
       <tr runat="server" id="tr_lSelector">
            <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentLang&Help=cms_HelpContentLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentLang", "Content Language")%></td>
            <td><HP3:contentLanguage ID="lSelector" runat="server"  Typecontrol="StandardControl" /></td>
       </tr>
       <!-- 28/10/08 MR -->
               <tr id="apprStatus" runat="server" >
           <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentApr&Help=cms_HelpContentApr&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentApr", "Content Approval")%></td>
            <td>
               <asp:DropDownList id="dwlApprovalStatus" runat="server">
                    <asp:ListItem  Text="To Be Approved" Value="2"></asp:ListItem>
                    <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
                    <asp:ListItem  Text="Not Approved" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
       
       <tr runat="server" id="tr_DateUpdate">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateUpdate&Help=cms_HelpDateUpdate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateUpdate", "Date Update")%></td>
            <td><HP3:Date runat="server" ID="DateUpdate" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateCreate">
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateCreate&Help=cms_HelpDateCreate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateCreate", "Date Create")%></td>
            <td><HP3:Date runat="server" ID="DateCreate" TypeControl="JsCalendar" ShowTime="true" /></td>
        </tr>
        <tr runat="server" id="tr_DatePublish">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDatePublish&Help=cms_HelpDatePublish&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDatePublish", "Date Publish")%></td>
            <td><HP3:Date runat="server" ID="DatePublish" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_oSite">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td><HP3:ctlSite runat="server" ID="oSite" TypeControl="GenericRelation" SiteType="Site" /></td>
        </tr>
        <tr runat="server" id="tr_Aree">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="Aree" TypeControl="GenericRelation" SiteType="area"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr runat="server" id="tr_Title">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
            <td><HP3:Text runat="server" ID="Title" TypeControl="TextBox" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSubTitle&Help=cms_HelpSubTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSubTitle", "SubTitle")%> </td>
            <td><HP3:Text runat ="server" id="SubTitle" TypeControl ="TextBox" MaxLength="400" style="width:600px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAbstract&Help=cms_HelpAbstract&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAbstract", "Abstract")%> </td>
            <td>
            
<%--            <FCKeditorV2:FCKeditor id="Abstract" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="Abstract" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullText&Help=cms_HelpFullText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullText", "Full Text")%> </td>
            <td>
            
<%--            <FCKeditorV2:FCKeditor id="FullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="FullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            
            </td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormObjectives&Help=cms_HelpObjectives&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormObjectives", "Objectives")%> </td>
            <td><HP3:Text runat ="server" id="Objectives" TypeControl ="TextArea" MaxLength="400" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDuration&Help=cms_HelpDuration&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDuration", "Duration")%> </td>
            <td><HP3:Text runat ="server" id="Duration" TypeControl="NumericText" style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMinTime&Help=cms_HelpMinTime&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMinTime", "Minimun Time")%> </td>
            <td><HP3:Text runat ="server" id="MinTime" TypeControl="NumericText" Style="width:600px"/></td>
        </tr>
         
        <tr runat="server" id="tr_chkActive">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="chkActive" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_chkDisplay">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="chkDisplay" runat="server"/></td>
        </tr>
 </table>
</asp:Panel>

<!--Pannello del dettaglio del corso-->
<asp:Panel id="pnlCourseDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="95%">
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCourseType&Help=cms_HelpCourseType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCourseType", "Course Type")%> </td>
        <td>
            <asp:DropDownList id="CrType" runat="server">
                <asp:ListItem Text="Distance Learning" Value="1"></asp:ListItem>
                <asp:ListItem Text="Residential Course" Value="2"></asp:ListItem>
                <asp:ListItem Text="Blended Learning" Value="3"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProvider&Help=cms_HelpProvider&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProvider", "Provider")%> </td>
        <td><HP3:Text runat ="server" id="CrProvider" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCoursePlanner&Help=cms_HelpCoursePlanner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCoursePlanner", "Course Planner")%> </td>
        <td><HP3:Text runat ="server" id="CrPlanner" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRecipients&Help=cms_HelpRecipients&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRecipients", "Recipients")%> </td>
        <td><HP3:Text runat ="server" id="CrRecipients" TypeControl ="TextArea" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStartDate&Help=cms_HelpStartDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStartDate", "Start Date")%> </td>
        <td><HP3:Date runat="server" id="CrStartDate" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEndDate&Help=cms_HelpEndDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEndDate", "End Date")%> </td>
        <td><HP3:Date runat="server" id="CrEndDate" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSponsor&Help=cms_HelpSponsor&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSponsor", "Sponsor")%> </td>
        <td><HP3:Text runat ="server" id="CrSponsor" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormScientificResponsible&Help=cms_HelpScientificResponsible&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormScientificResponsible", "Scientific Responsible")%> </td>
        <td><HP3:Text runat ="server" id="CrScientificResponsible" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormScientificSegreteriat&Help=cms_HelpScientificSegreteriat&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormScientificSegreteriat", "Scientific Segreteriat")%> </td>
        <td><HP3:Text runat ="server" id="CrScientificSegreteriat" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrganizationalSegreteriat&Help=cms_HelpOrganizationalSegreteriat&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrganizationalSegreteriat", "Organizational Segreteriat")%> </td>
        <td><HP3:Text runat ="server" id="CrOrganizationalSegreteriat" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCodeRequired&Help=cms_HelpCodeRequired&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCodeRequired", "Code Required")%> </td>
        <td><asp:CheckBox id="CrCodeRequired" runat="server"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOnlineRegistration&Help=cms_HelpOnlineRegistration&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOnlineRegistration", "Online Registration")%> </td>
        <td><asp:CheckBox id="CrOnlineRegistration" runat="server"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEntranceTest&Help=cms_HelpEntranceTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEntranceTest", "Entrance Test Required")%> </td>
        <td><asp:CheckBox id="CrEntranceTest" runat="server"/></td>
    </tr>
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFinalTest&Help=cms_HelpFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFinalTest", "Final Evaluation Test Required")%> </td>
        <td><asp:CheckBox id="CrFinalEvaluationTest" runat="server"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyFinalTest&Help=cms_HelpKeyFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyFinalTest", "Final Evaluation Test")%> </td>
        <td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenQuest" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtQuest"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popQuestList.aspx?sites=' +  document.getElementById('<%=txtHiddenQuest.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenQuest.clientid%>&ListId=<%=txtQuest.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTeacher&Help=cms_HelpTeacher&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTeacher", "Teacher")%> </td>
        <td><HP3:ctlContentEditorial id="ctlCE" typecontrol="GenericRelation" runat="server" /></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPreview&Help=cms_HelpPreview&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPreview", "Preview Required")%> </td>
        <td><asp:CheckBox id="CrPreview" runat="server"/></td>
    </tr>
 </table>
</asp:Panel>

<asp:Panel id="pnlLessonDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="63%">
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFinalTest&Help=cms_HelpFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFinalTest", "Final Evaluation Test Required")%> </td>
        <td><asp:CheckBox id="lessEvalTest" runat="server"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyFinalTest&Help=cms_HelpKeyFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyFinalTest", "Final Evaluation Test")%> </td>
        <td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="lessHiddenQuest" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="lessTxtQuest"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popQuestList.aspx?sites=' +  document.getElementById('<%=lessHiddenQuest.clientid%>').value + '&SingleSel=1&HiddenId=<%=lessHiddenQuest.clientid%>&ListId=<%=lessTxtQuest.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
   </table>
</asp:Panel> 

<!--Pannello della lista delle lezioni-->
<a name="LessonList"></a>
<asp:Panel id="pnlLessonGrid" runat="server" Visible="false">
<hr />
<table  style="margin-top:10px" class="form" width="99%">
        <tr>
           <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLessonsList&Help=cms_HelpLessonsList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLessonsList", "Lessons List")%> </td>
           <td  style="width:85%">
              <asp:gridview id="gdw_Lessons" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="20"
                    emptydatatext="No items available">
                  <Columns >
                   <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate><img alt="Language: <%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" /></ItemTemplate> 
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Title">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%">
                           <ItemTemplate>
                                <asp:ImageButton id="lnkAddC" runat="server" ToolTip ="Chapter Relation" ImageUrl="~/hp3Office/HP3Image/ico/content_newlanguage.gif" OnClick="ChapterRelation" CommandName ="ChapterRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton id="lnkAddN" runat="server" ToolTip ="Add Node" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif" OnClick="AddNode" CommandName ="AddNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton id="lnkSelectL" runat="server" ToolTip ="Edit" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditLesson" CommandName ="SelectLesson" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDeleteL" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteLesson" CommandName ="DeleteLesson" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <%--<asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton id="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteQuest" CommandName ="DeleteQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                           </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
            </asp:gridview>
        </td>
    </tr> 
</table>
</asp:Panel>

<!--Pannello del dettaglio del nodo-->
<asp:Panel id="pnlNodeDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="80%">
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormClassNode&Help=cms_HelpClassNode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormClassNode", "Class Node")%> </td>
        <td>
            <asp:DropDownList id="dwlClassNode" runat="server">
                <asp:ListItem Text="Argument" Value="1"></asp:ListItem>
                <asp:ListItem Text="Bibliografic" Value="2"></asp:ListItem>
                <asp:ListItem Text="Chapter" Value="3"></asp:ListItem>
                <asp:ListItem Text="Paragraph" Value="4"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
 </table>
</asp:Panel>

<!--Pannello della lista delle lezioni-->
<a  name="NodesList"></a>
<asp:Panel id="pnlNodeGrid" runat="server" Visible="false">
<hr />
<table  style="margin-top:10px" class="form" width="99%">
        <tr>
           <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNodesList&Help=cms_HelpNodesList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNodesList", "Nodes List")%> </td>
           <td  style="width:85%">
              <asp:gridview id="gdw_Nodes" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="20"
                    emptydatatext="No items available">
                  <Columns >
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate><img alt="Language: <%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" /></ItemTemplate> 
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Title">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Class Node">
                            <ItemTemplate><%#getClassNode(Container.DataItem.Key.PrimaryKey)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%">
                           <ItemTemplate>
                                <%--<asp:ImageButton id="lnkAddN" runat="server" ToolTip ="Add Node" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif" OnClick="AddNode" CommandName ="AddNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>--%>
                                <asp:ImageButton id="lnkSelectN" runat="server" ToolTip ="Edit" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditNode" CommandName ="SelectNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDeleteN" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteNode" CommandName ="DeleteNode" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <%--<asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton id="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteQuest" CommandName ="DeleteQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                           </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
            </asp:gridview>
        </td>
    </tr> 
</table>
</asp:Panel>
