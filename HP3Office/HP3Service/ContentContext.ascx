<%@ Control Language="VB" ClassName="ContentContexts" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>

<script runat="server">
    
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    Private Domain As String = String.Empty
    Private Language As String = String.Empty
    
    Private _webUtility As WebUtility
    Private dateUtility As New StringUtility
    
    Private oLManager As New LanguageManager()
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    Private comboContextGroupBinded As Boolean = False
    
    
    Private oCollection As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    ' il datasource corrente
    Private _dataSource As ContextCollection = Nothing
    ' la lista degli id che hanno una relazione con il content
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue
    
    Private utility As New WebUtility

    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                _keyContent = New ContentIdentificator(objQs.ContentId, objQs.Language_id)
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
        End Set
    End Property
    
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As New ContentCollection
     
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        contentCollection = _contentManager.Read(so)
        
        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function

    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property
    
    Public ReadOnly Property Related() As List(Of Integer)
        Get
            If _related Is Nothing Then
                If Not UseInPopup Then
                    _related = readRelated(KeyContent)
                Else
                    _related = readSelection(KeyContent)
                End If
            End If
            Return _related
        End Get
    End Property

    Private ReadOnly Property KeyChanges() As String
        Get
            Dim _key As String
            _key = ViewState("RelatedChangesKey")
            If _key Is Nothing Then
                ' Genera una chiave random per assicurarsi che lo stato
                ' delle modifiche cambi tra una richiesta e l'altra
                _key = SimpleHash.GenerateUUID(32)
                ViewState("RelatedChangesKey") = _key
            End If
            Return _key
        End Get
    End Property

    Public Property RelatedChanges() As Dictionary(Of Integer, Boolean)
        Get
            If _changes Is Nothing Then
                _changes = Cache.Item(KeyChanges)
                If _changes Is Nothing Then
                    _changes = New Dictionary(Of Integer, Boolean)
                End If
            End If
            Return _changes
        End Get
        Set(ByVal value As Dictionary(Of Integer, Boolean))
            _changes = value
            Cache.Insert(KeyChanges, _changes, DateTime.Now.AddMinutes(10), True)
        End Set
    End Property

    Public Function checkRelated(ByVal idRelation As Integer) As Boolean
        Return checkRelated(idRelation, False)
    End Function

    Public Function checkRelated(ByVal idRelation As Integer, ByVal onlyPreviouslyRelated As Boolean) As Boolean
        ' Verifica se il datasource degli elementi relazionati contiene la chiave corrente
        Dim ris As Boolean = False
        
        If Not Related Is Nothing AndAlso Related.Contains(idRelation) Then
            ' se si verifica che non sia stata deselezionata dall'utente
            If Not onlyPreviouslyRelated AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
                ris = RelatedChanges.Item(idRelation)
            Else
                ' non � stata deselezionata
                ris = True
            End If
        ElseIf Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
            ' Se la chiave non era precedentemente selezionata, verifica se � stata selezionata dall'utente
            If Not onlyPreviouslyRelated Then
                ris = RelatedChanges.Item(idRelation)
            End If
        End If
        Return ris
    End Function
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)

        ' Legge il datasource completo 
        getDataSource(clearCache)

       
        ' Effettua il bind del datasource sulla gridview
        gridContext.DataSource = _dataSource
        gridContext.DataBind()
    End Sub

    Public Sub SaveRelatedStatus()
        ' flag che indica se � stato apportato un cambiamento alle relazioni
        Dim isChanges As Boolean = False
                
        ' controlla le modifiche effettuate sulla griglia
        If gridContext.Rows.Count = 0 Then Return
        Dim check As CheckBox
        Dim radio As RadioButton
        Dim idRelation As Integer
        Dim checked As Boolean = False
        ' legge ogni riga della griglia
        For Each row As GridViewRow In gridContext.Rows
            ' azzera idrelation
            idRelation = -1
            ' estrae il checkbox
            If AllowEdit Then
                check = row.FindControl("chkRelation")
                If Not check Is Nothing AndAlso check.Text <> "" Then
                    ' la propriet� text del checkbox contiene l'id della relazione
                    idRelation = Integer.Parse(check.Text)
                    checked = check.Checked
                End If
            Else
                radio = row.FindControl("optRelation")
                If Not radio Is Nothing AndAlso radio.Text <> "" Then
                    ' la propriet� text del checkbox contiene l'id della relazione
                    idRelation = Integer.Parse(radio.Text)
                    checked = radio.Checked
                End If
            End If

            If idRelation > -1 Then
                ' verifica se lo stato della relazione attuale � cambiato rispetto a quello iniziale
                If (checkRelated(idRelation, True) AndAlso Not checked) OrElse (Not checkRelated(idRelation, True) AndAlso checked) Then
                    ' aggiorna l'hashmap dei cambiamenti
                    If RelatedChanges.ContainsKey(idRelation) Then
                        RelatedChanges.Item(idRelation) = checked
                    Else
                        If Not AllowEdit And checked Then
                            RelatedChanges.Clear()
                            RelatedChanges.Add(idRelation, checked)
                        ElseIf AllowEdit Then
                            RelatedChanges.Add(idRelation, checked)
                        End If
                    End If
                    ' imposta il flag
                    isChanges = True
                ElseIf checkRelated(idRelation, True) <> checkRelated(idRelation, False) Then
                    ' aggiorna l'hashmap dei cambiamenti
                    RelatedChanges.Remove(idRelation)
                    ' imposta il flag
                    isChanges = True
                End If
            End If
        Next

        ' se � stato cambiato qualcosa nelle relazioni ...
        If isChanges Then
            ' ... fa si che il valore della property venga cachato
            RelatedChanges = RelatedChanges
        End If
        ' abilita il tasto apply associations a lato client (js)
        If AllowEdit AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then
            Dim strJs As String = "<script type='text/javascript'>enableApplyButton();<" & "/script>"
            Page.ClientScript.RegisterStartupScript(GetType(String), "enableApplyButton", strJs)
        End If
    End Sub
    
   
 
    'End Sub

    Protected Sub gridRelations_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridContext.DataBound

        If gridContext.FooterRow Is Nothing Then Return
     
        For cellNum As Integer = gridContext.Columns.Count - 1 To 1 Step -1
            Try
                gridContext.FooterRow.Cells.RemoveAt(cellNum)
            Catch ex As Exception
            End Try
        Next
        gridContext.FooterRow.Cells(0).ColumnSpan = gridContext.Columns.Count
        gridContext.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        If Not _dataSource Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
            Dim startIndex As Integer = gridContext.PageSize * gridContext.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            gridContext.FooterRow.Cells(0).Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", _
                s, startIndex + 1, startIndex + gridContext.Rows.Count, gridViewTotalCount, s)
        Else
            gridContext.FooterRow.Cells(0).Text = "No items found."
        End If
    End Sub

    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridContext
        End Get
    End Property

    Private Sub LoadContentSummary()
        If KeyContent.Id > 0 Then
            
            If Not (ContentValue Is Nothing) Then
                lblContentId.Text = ContentValue.Key.Id
                If ContentValue.DatePublish.HasValue Then
                    lblContentDatePublish.Text = getCorrectedFormatDate(ContentValue.DatePublish)
                Else
                    lblContentDatePublish.Text = ""
                End If
                imgContentLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
                imgContentLanguage.Visible = True
                lblContentTitle.Text = ContentValue.Title
            End If
        Else
            imgContentLanguage.Visible = False
        End If

    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollection = siteAreaManager.Read(siteAreaSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        
        Domain = WebUtility.MyPage(Me).DomainInfo
        Language = Me.BusinessMasterPageManager.GetLang.Id
        
        
        If Not AllowEdit Then
            'pnlHeader.Visible = False
            ContentSummary.Visible = False
        End If
       
        If Not Page.IsPostBack Then
            LoadContentSummary()
            ReadRelations()
            BindComboGroup()
            BindDomains()
            
            If (Request("s") <> 0) Then
                Dim domain As String = GetSiteDomain()
                dwlDomain.SelectedIndex = dwlDomain.Items.IndexOf(dwlDomain.Items.FindByText(domain))
                'dwlDomain.Enabled = False
            End If
           
        End If
        
        If objQs.ContentId = 0 And Not UseInPopup Then
            Dim _webUtility As New WebUtility
            'SearchEngine.DropDownContentType.Items.Clear()
            '_webUtility.LoadListControl(SearchEngine.DropDownContentType, ContentTypes, "Description", "Key.Id")
            SearchEngine.Visible = True
            pnlRelationEdit.Visible = False
        Else
            SearchEngine.Visible = False
 
        End If
       
    End Sub
    
    Function GetSiteDomain() As String
        siteAreaManager = New SiteAreaManager
        Dim siteareavalue As SiteAreaValue = siteAreaManager.Read(New SiteAreaIdentificator(Request("s")))
        If Not (siteareavalue Is Nothing) Then Return siteareavalue.Key.Domain
        Return Nothing
    End Function
    
    Private ReadOnly Property ContentTypes() As ContentTypeCollection
        Get
            If _contentTypeCollection Is Nothing Then
                Dim _contentTypeManager As New ContentTypeManager
                Dim _contentTypeSearcher As New ContentTypeSearcher
                _contentTypeSearcher.OnlyAssociatedContents = True
                _contentTypeCollection = _contentTypeManager.Read(_contentTypeSearcher)
            End If
            Return _contentTypeCollection
        End Get
    End Property

    Sub BindComboGroup()
        If comboContextGroupBinded Then Exit Sub
        
        Dim _oContextManager As New ContextManager()
        Dim _oContextGroupCollection As ContextGroupCollection
        Dim _oContextGroupSearcher As New ContextGroupSearcher
        _oContextGroupSearcher.Key.Language = New LanguageIdentificator()
        _oContextGroupCollection = _oContextManager.ReadGroup(_oContextGroupSearcher)

        ContextGroup.Items.Clear()
        ContextGroup.Items.Add(New ListItem("--Select Context--", 0))
        ContextGroup.Items.Add(New ListItem("Context without group", -1))
        ContextGroup.Items.Add(New ListItem("All contexts", 0))
        If Not _oContextGroupCollection Is Nothing Then
            Dim _oContextGroup As ContextGroupValue
            ContextGroup.Items.Add(New ListItem("---------------------------------------------------------------", -2))
            
            For Each _oContextGroup In _oContextGroupCollection
                ContextGroup.Items.Add(New ListItem(_oContextGroup.Description, _oContextGroup.Key.Id))
            Next
            
            ContextGroup.SelectedIndex = 0
        End If
        
        comboContextGroupBinded = True

    End Sub

    Private Function getDataSource() As ContextCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As ContextCollection
        If _dataSource Is Nothing Then
            Dim _ContextSearcher As New ContextSearcher()
                      
            
            If Not ContextGroup.SelectedValue Is Nothing Then
                _ContextSearcher.KeyContextGroup.Id = ContextGroup.SelectedValue
            End If
            If Not DataValueFilter Is Nothing AndAlso DataValueFilter <> "" Then
                _ContextSearcher.Key = New ContextIdentificator(Integer.Parse(DataValueFilter))
            End If
            If Not DataTextFilter Is Nothing AndAlso DataTextFilter <> "" Then
                _ContextSearcher.Description = "%" & DataTextFilter & "%"
            End If
            
            If dwlDomain.SelectedItem.Value <> "-1" Then _ContextSearcher.Key.Domain = dwlDomain.SelectedItem.Value
            
            Dim _oContextManager As New ContextManager()
          
            _oContextManager.Cache = False
            _dataSource = _oContextManager.Read(_ContextSearcher)
            
           
 
            
            
        End If
        Return _dataSource
    End Function
    
    Private Function readSelection(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        If Not Request("selItem") Is Nothing AndAlso Request("selItem") <> "" Then
            _related = New List(Of Integer)
            _related.Add(Request("selItem"))
            RelatedChanges.Clear()
            RelatedChanges.Add(Request("selItem"), True)
            'CType(WebUtility.MyPage(Me).TextHidden, TextBox).Text = findClientId(Request("SelItem"))
            Return _related
        Else
            Return Nothing
        End If
        
    End Function

 
    
    Private Function readRelated(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        
        If _related Is Nothing AndAlso Not keyContent Is Nothing Then
            Dim _oContextRelatedCollection As ContextCollection = Nothing
            Dim _oContentManager As New ContentManager
            _oContentManager.Cache = False
            _oContextRelatedCollection = _oContentManager.ReadContentContext(keyContent)
            If Not _oContextRelatedCollection Is Nothing Then
                _related = New List(Of Integer)
                Dim _context As ContextValue
                For Each _context In _oContextRelatedCollection
                    _related.Add(_context.Key.Id)
                Next
            End If
        End If
        Return _related
    End Function
 
    Private Overloads Sub LoadControl()
        Dim _oContextCollection As ContextCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
        
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)

        readRelated(_oKeyContent)
        
        BindGrid()
        pnlRelationEdit.Visible = True

    End Sub
    
    Protected Sub SearchEngine_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arr() As String = sender.CommandArgument.split("_")
        Dim id As Int32 = arr(0)
        Dim idLanguage As Int32 = arr(1)
        
        objQs.ContentId = id
        objQs.Language_id = idLanguage
        webRequest.customParam.append(objQs)
        Dim mPageManager As New MasterPageManager
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub

    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
 
    
    'aggiunge una nuova relazione context/content
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oKeyContext As New ContextIdentificator()
        _oKeyContext.Id = key
        
        Me.BusinessContentManager.CreateContentContext(_oKeyContent, _oKeyContext)
         
        ReadRelations()
    End Sub
     
    'rimuove una relazione context/content
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        'Key della relazione
        Dim key As Integer = sender.commandArgument.ToString
        Me.BusinessContentManager.RemoveContentContext(New Identificator(key))
        ReadRelations()
    End Sub
    
    'legge le relazioni context/content
    Sub ReadRelations()
       
        If (objQs.ContentId <> 0) Then
            Dim _oKeyContent As New ContentIdentificator
            '1149
            _oKeyContent = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
           
            Dim oSearcher As New ContentContextSearcher
            oSearcher.KeyContent = _oKeyContent
            oSearcher.LoadContextTitle = True
            oSearcher.LoadRelationTitle = True
    
             Me.BusinessContentManager.Cache = False
            Dim _oContextCollection As ContentContextCollection = Me.BusinessContentManager.ReadContentContext(oSearcher)
                        
            gridActiveRelation.DataSource = _oContextCollection
            gridActiveRelation.DataBind()
        End If
    End Sub
    
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridContext.PageIndexChanging
        'SaveRelatedStatus()
        gridContext.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    Private Sub ActiveEdit(ByVal active As Boolean)
        pnlEdit.Visible = active
        pnlRelatedSearch.Visible = Not active
        pnlActiveRelations.Visible = Not active
        gridContext.Visible = Not active
    End Sub
    
    'Bind della dwl relativa ai relation type
    Sub BindRelationType()
        Dim relationTypeSearcher As New ContentRelationTypeSearcher
        Dim relationTypeColl As ContentRelationTypeCollection
        
        
        If dwlRelationType.Items.Count = 0 Then
        
            relationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(relationTypeSearcher)
        
            If Not relationTypeColl Is Nothing Then
                For index As Integer = 0 To relationTypeColl.Count - 1
                    Dim text As String = relationTypeColl(index).Description
                    If (relationTypeColl(index).Domain <> String.Empty And relationTypeColl(index).Name <> String.Empty) Then text = text & " " & "(" & relationTypeColl(index).Domain & "-" & relationTypeColl(index).Name & ")"
                    dwlRelationType.Items.Insert(index, New ListItem(text, relationTypeColl(index).Key.Id))
                Next
                ' _webUtility.LoadListControl(dwlRelationType, relationTypeColl, "Description", "Key.Id")
                dwlRelationType.Items.Insert(0, New ListItem("--No Type--", "0"))
            End If
        End If
        
    End Sub
    
    Private Function GetRelationValue(ByVal id As Integer, ByVal loadContextTitle As Boolean) As ContentContextValue
        Dim coll As ContentContextCollection
        Dim so As New ContentContextSearcher
        Dim vo As ContentContextValue = Nothing
        so.LoadContextTitle = loadContextTitle
        so.Key = New Identificator(id)
        BusinessContentManager.Cache = False
        coll = Me.BusinessContentManager.ReadContentContext(so)
        
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            vo = coll(0)
        End If
        Return vo
    End Function
    
    
    Protected Sub editRelation(ByVal sender As Object, ByVal e As System.EventArgs)
        ActiveEdit(True)
        '
        Dim key As Integer = sender.commandArgument.ToString

        
        Dim vo As ContentContextValue = GetRelationValue(key, True)
        
        If Not vo Is Nothing Then
            txtOrder.Text = vo.Order
            txtKeyRelation.Text = vo.Key.Id
            lblContextName.Text = vo.ContextTitle
            BindRelationType()
 
                
            If (vo.KeyRelationType.Id > 0) Then
                dwlRelationType.SelectedIndex = dwlRelationType.Items.IndexOf(dwlRelationType.Items.FindByValue(vo.KeyRelationType.Id))
            End If
            
            
        End If
             
    End Sub
    Protected Sub saveRelation(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vo As ContentContextValue = GetRelationValue(txtKeyRelation.Text, False)
 
  
        If txtOrder.Text <> "" Then vo.Order = Int32.Parse(txtOrder.Text)
        vo.KeyRelationType.Id = dwlRelationType.SelectedItem.Value
 
        Me.BusinessContentManager.UpdateContentContext(vo)
        
        ActiveEdit(False)
        ReadRelations()
    End Sub
    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        'SaveRelatedStatus()
        filterDataSource()
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        
        'If Not AllowEdit Then Return
        'Response.Redirect(GetBackUrl)
    End Sub

    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue

        objQs.ContentId = 0

        webRequest.customParam.append(objQs)
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
</script>

<%--<script type="text/javascript">
    function enableApplyButton(state) {
        if (state == null) state = true;
        try {
            document.getElementById('<%=btApply.ClientId %>').disabled = !state;
        } catch (e) {
        }
    }
</script>--%>
<hr />
<HP3:SearchEngine ID="SearchEngine" visible="false" UseInRelation ="true"  AllowEdit="false" AllowSelect="true" ShowContentType="true" runat="server" OnSelectedEvent="SearchEngine_SelectedEvent" />
<hr />

<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="Button1" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
    <%--Pannello del Content Selezionato--%>   
    <asp:Panel ID="ContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div><br />
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:20%">Date Publish</th>
                <th style="width:5%"></th>
                <th style="width:70%">Title</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
            </tr>
        </table>
    <hr />
    <br />
    </asp:Panel>

     <%--Pannello delle Relazioni attive--%>
      <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div><br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Context Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "KeyContext.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                              <%#DataBinder.Eval(Container.DataItem, "ContextTitle")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="RelationType" HeaderStyle-Width="5%" ItemStyle-Width="20%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "RelationTitle")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                            <asp:ImageButton ID="btn_edit" runat="server" ToolTip ="Edit relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="editRelation" CommandName ="SelectItem" CommandArgument ='<%#Container.Dataitem.Key.Id%>'/>
                    
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
                <hr />
     
     <br />
    </asp:Panel>

    <%-- Pannello del Context Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Context Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Description</td>
                <td>Context Group</td>
                <td>Domain</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" Style="width:100px;" runat="server" TypeControl="NumericText" /></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></td>
                <td><asp:DropDownList ID="ContextGroup" runat="server" /></td>
                <td style="width:40px"><asp:DropDownList id="dwlDomain" runat="server" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    <hr />
      <br />
    </asp:Panel>

    <%--Griglia dei Context--%>
    <asp:GridView ID="gridContext" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server"
        >
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                       <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Description")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>


       <%--Pannello per l'edit di una relazione  --%>  
    <asp:Panel ID="pnlEdit" Width="100%" runat="server" Visible="false">
    <asp:Button id ="btn_SaveRelation" runat="server" Text="Save" OnClick="saveRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
        <div class="title" style="margin-bottom:20px">Relation between Content - Context </div>
        <br />
                <asp:TextBox ID="txtKeyRelation" runat="server" Visible="false"></asp:TextBox>
        <br />
        <table class="form" width="99%">
              <tr>
              <td>Context </td>
              <td><asp:Label runat = "server" ID="lblContextName"></asp:Label></td>
              </tr>
              <tr>
                <td><a href="#"  onclick="window.open('<%=Domain%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Relation Type")%></td>
                <td><asp:DropDownList id="dwlRelationType" runat="server"/></td>
             </tr>
             <tr>
                <td><a href="#"  onclick="window.open('<%=Domain%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpContextOrder&Language=<%=Language%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                <td><HP3:ctlText runat="server" ID="txtOrder" TypeControl="TextBox" style="width:300px" /></td>
             </tr>
        </table>
    </asp:Panel>

</asp:Panel>