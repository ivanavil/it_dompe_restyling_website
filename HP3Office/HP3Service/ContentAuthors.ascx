﻿<%@ Control Language="VB" ClassName="ContentAuthors" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>

<script runat="server">

 
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    
    Private _webUtility As WebUtility
    Private dateUtility As New StringUtility
       Private _strDomain As String
      Private _language As String
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    
    ' il datasource corrente
    Private _dataSource As AuthorCollection = Nothing
    
    ' la lista degli id che hanno una relazione con il content
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
 

    
    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue
     
    Private manAuthor As New ContentAuthorManager
    
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
 
    
  
    ' ===================================================================
    ' ===================================================================
    ' ===================================================================
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
  
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        If Not AllowEdit Then
            ContentSummary.Visible = False
        End If

        If Not Page.IsPostBack Then
            LoadContentSummary()
            ReadRelations()
        End If
         
    End Sub
    Private Sub LoadContentSummary()
        Dim oLManager As New LanguageManager
        If KeyContent.Id > 0 Then

            lblContentId.Text = ContentValue.Key.Id
            If ContentValue.DatePublish.HasValue Then
                lblContentDatePublish.Text = getCorrectedFormatDate(ContentValue.DatePublish)
            Else
                lblContentDatePublish.Text = ""
            End If
            imgContentLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
            imgContentLanguage.Visible = True
            lblContentTitle.Text = ContentValue.Title
        Else
            imgContentLanguage.Visible = False
        End If
 
        
    End Sub
    
    'Bind della dwl relativa ai relation type
    Sub BindRelationType()
        Dim relationTypeSearcher As New ContentRelationTypeSearcher
        Dim relationTypeColl As ContentRelationTypeCollection
        
        
        If dwlRelationType.Items.Count = 0 Then
        
            relationTypeColl = Me.BusinessContentManager.ReadContentRelationtType(relationTypeSearcher)
        
            If Not relationTypeColl Is Nothing Then
                For index As Integer = 0 To relationTypeColl.Count - 1
                    Dim text As String = relationTypeColl(index).Description
                    If (relationTypeColl(index).Domain <> String.Empty And relationTypeColl(index).Name <> String.Empty) Then text = text & " " & "(" & relationTypeColl(index).Domain & "-" & relationTypeColl(index).Name & ")"
                    dwlRelationType.Items.Insert(index, New ListItem(text, relationTypeColl(index).Key.Id))
                Next
                ' _webUtility.LoadListControl(dwlRelationType, relationTypeColl, "Description", "Key.Id")
                dwlRelationType.Items.Insert(0, New ListItem("--No Type--", "0"))
            End If
        End If
        
    End Sub
    
    Sub DisablePanelRelation(ByVal visible As Boolean)
        ContentSummary.Visible = visible
        pnlActiveRelations.Visible = visible
        pnlRelatedSearch.Visible = visible
 
    End Sub
     
    
   
    
    
    'legge le relazioni user/author
    Sub ReadRelations()
 
        
        If Not KeyContent Is Nothing Then
            
          
            Dim soAuthor As New ContentAuthorSearcher
            soAuthor.KeyContent = KeyContent
            manAuthor.Cache = False
            
            Dim collAuthor As ContentAuthorCollection = manAuthor.ReadContentAuthor(soAuthor)
            
           
            gridActiveRelation.DataSource = collAuthor
            gridActiveRelation.DataBind()
         
        End If
    End Sub
 
    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                _keyContent = New ContentIdentificator(objQs.ContentId, objQs.Language_id)
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
        End Set
    End Property
    
    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As ContentCollection
        
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        contentCollection = _contentManager.Read(so)
        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
 
     
     
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilterID() As String
        Get
            Return txtId.Text
        End Get
    End Property

    Public ReadOnly Property DataValueFilterName() As String
        Get
            Return txtName.Text
        End Get
    End Property
    Public ReadOnly Property DataValueFilterSurname() As String
        Get
            Return txtSurname.Text
        End Get
    End Property
    Public ReadOnly Property DataValueFilterSearch() As String
        Get
            Return txtSearch.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)

        ' Legge il datasource completo 
        getDataSource(clearCache)

        gridAuthor.DataSource = _dataSource
        gridAuthor.DataBind()
    End Sub

    

    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridAuthor
        End Get
    End Property

  
 

    Private Function getDataSource() As AuthorCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As AuthorCollection
        If _dataSource Is Nothing Then
           
            Dim _UserSearcher As New ContentAuthorSearcher
            If Not DataValueFilterID Is Nothing AndAlso DataValueFilterID.Trim <> String.Empty Then
                _UserSearcher.Key.Id = Integer.Parse(DataValueFilterID)
            End If
            If Not DataValueFilterName Is Nothing AndAlso DataValueFilterName.Trim <> String.Empty Then
                _UserSearcher.Name = DataValueFilterName
            End If
            If Not DataValueFilterSurname Is Nothing AndAlso DataValueFilterSurname.Trim <> String.Empty Then
                _UserSearcher.Surname = DataValueFilterSurname
            End If
            If Not DataValueFilterSearch Is Nothing AndAlso DataValueFilterSearch <> "" Then
                _UserSearcher.SearchString = DataValueFilterSearch
            End If
            
            
            Dim oAuthorManager As New ContentAuthorManager()
            oAuthorManager.Cache = clearCache
            _dataSource = oAuthorManager.Read(_UserSearcher)
 
        
            
        End If
        Return _dataSource
    End Function
    
    'permette di impostare il tipo ad una relazione Content/Content  
    Sub saveRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim contentRelationValue As ContentAuthorRelationValue = Nothing
              
          
        Dim key As Integer = txtAuthorId.Text
 
        
        Dim so As New ContentAuthorRelationSearcher
        so.Key.Id = key
        Dim mContentManager As New ContentManager
        mContentManager.Cache = False
        Dim coll As ContentAuthorRelationCollection = mContentManager.ReadContentAuthorRelation(so)
        
        If Not coll Is Nothing And coll.Count > 0 Then
            contentRelationValue = coll(0)
 
            contentRelationValue.KeyRelationType.Id = dwlRelationType.SelectedItem.Value
            mContentManager.UpdateContentAuthor(contentRelationValue)
        End If
 
        DisablePanelRelation(True)
        pnl_Edit.Visible = False
        ReadRelations()
      
    End Sub
    
    Protected Sub editRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim parameters As String = sender.commandArgument.ToString
    
        Dim parametersArray() As String
      
       
        parametersArray = parameters.Split("|")
 
        Dim KeyRelation As Integer = parametersArray(0)
        Dim TypeId As Integer = parametersArray(1)
        
        txtAuthorId.Text = KeyRelation
                
        BindRelationType()
        DisablePanelRelation(False)
        pnl_Edit.Visible = True
                
        If (TypeId <> 0) Then
            dwlRelationType.SelectedIndex = dwlRelationType.Items.IndexOf(dwlRelationType.Items.FindByValue(TypeId))
        End If
    End Sub
   
    
    'aggiunge una nuova relazione user/content
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As Integer = sender.commandArgument.ToString
        Dim oRel As New ContentAuthorRelationValue
        oRel.KeyAuthor.Id = key
        oRel.KeyContent = _oKeyContent
      
 
 
        
        Me.BusinessContentManager.CreateContentAuthor(oRel)
        ReadRelations()
    End Sub
     
    'rimuove una relazione role/content
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim keyRelazion As Integer = sender.commandArgument.ToString
                
        Dim oRel As New ContentAuthorRelationValue
        oRel.Key.Id = keyRelazion
        
        Me.BusinessContentManager.RemoveContentAuthor(oRel)
        
        
        ReadRelations()
    End Sub
    
    Private Overloads Sub LoadControl()
 
        getDataSource(True)

       
        ' Effettua il bind del datasource sulla gridview
        gridAuthor.DataSource = _dataSource
        gridAuthor.DataBind()
        
    End Sub
    
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        
        'If Not AllowEdit Then Return
        'Response.Redirect(GetBackUrl)
    End Sub

    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue

        objQs.ContentId = 0

        webRequest.customParam.append(objQs)
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridAuthor.PageIndexChanging
        'SaveRelatedStatus()
        gridAuthor.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    'OK
    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        filterDataSource()
    End Sub
    'OK
    Public Sub filterDataSource()
        LoadControl()
    End Sub

    'OK
    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtId.Text = ""
        txtName.Text = ""
        txtSearch.Text = ""
        txtSurname.Text = ""
 
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    'OK
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
    Function GetKeyAndType(ByVal oAuthor As ContentAuthorValue) As String
        Return oAuthor.RelationKey.Id.ToString + "|" + oAuthor.RelationType.Id.ToString
    End Function
    
    Function GetTypeRelation(ByVal oAuthor As ContentAuthorValue) As String
        If Not oAuthor Is Nothing Then Return oAuthor.RelationType.Description
        Return String.Empty
    End Function
</script>

<%--<script type="text/javascript">
    function enableApplyButton(state) {
        if (state == null) state = true;
        try {
            document.getElementById('<%=btApply.ClientId %>').disabled = !state;
        } catch (e) {
        }
    }
</script>--%>
 
 
 
<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="Button1" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
   
       <%--Pannello per l'edit di una relazione  --%>  
    <asp:Panel ID="pnl_Edit" Width="100%" runat="server" Visible="false">
    <asp:Button id ="btn_SaveRelation" runat="server" Text="Save" OnClick="saveRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
        <div class="title" style="margin-bottom:20px">Relation between Content - Author</div>
        <br />
        <asp:TextBox ID="txtAuthorId" runat="server" Visible="false"></asp:TextBox>
        <br />
        <table class="form" width="99%">
              <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Relation Type")%></td>
                <td><asp:DropDownList id="dwlRelationType" runat="server"/></td>
             </tr>
        </table>
    </asp:Panel>



    <%--Pannello del Content Selezionato--%>   
    <asp:Panel ID="ContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div> 
        <br />
       <div>
        <table class="tbl1">
            <tr>
                <th style="width:5%">Id</th>
                <th style="width:20%">Date Publish</th>
                <th style="width:5%"></th>
                <th style="width:70%">Title</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
            </tr>
        </table>
        </div>
    </asp:Panel>
    <hr />
    <br /><br />
    <%--Pannello delle Relazioni attive--%>
    <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div><br /><br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id Author" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surname">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                        </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Name")%>      
                        </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Type">
                        <ItemTemplate> 
                            <%#GetTypeRelation(Container.DataItem)%> 
                        </ItemTemplate>
                  </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="4%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_edit" runat="server" ToolTip ="Edit relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="editRelation" CommandName ="SelectItem" CommandArgument ='<%#GetKeyAndType(Container.DataItem)%>'/>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.RelationKey.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <%-- Pannello Author Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Author Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Surname</td>
                 <td>Search String</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtId" runat="server" TypeControl="NumericText" Style="width:50px;" /></td>
                <td><HP3:ctlText ID="txtName" runat="server" typecontrol="TextBox" /></td>
                <td><HP3:ctlText ID="txtSurname" runat="server" typecontrol="TextBox" /></td>
                <td><HP3:ctlText ID="txtSearch" runat="server" typecontrol="TextBox" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
 
    <hr />
    
 
    
    <asp:GridView ID="gridAuthor" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server"
        >
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <%--<%If AllowEdit Then%>
                        <asp:CheckBox ID="chkRelation" onClick="enableApplyButton()" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%Else%>
                        <asp:RadioButton ID="optRelation" onClick="GestClick(this)" GroupName="optRelations" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%End If%>--%>
                     <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Surname">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Name")%>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>
        </asp:Panel>

</asp:Panel>