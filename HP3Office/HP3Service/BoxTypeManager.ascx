<%@ Control Language="VB" ClassName="ctlBoxTypeManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
    Private oCollection As BoxTypeCollection
    Private oCManager As BoxManager
    Private oSearcher As BoxSearcher
      
    Private _sortType As BoxTypeGenericComparer.SortType
    Private _sortOrder As BoxTypeGenericComparer.SortOrder
    
    Private _typecontrol As ControlType = ControlType.View
    
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
        
    End Property
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As BoxTypeValue
        If SelectedId <> 0 Then
            oCManager = New BoxManager
            oCManager.Cache = False
            Return oCManager.ReadBoxType(SelectedId)(0)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente un BoxType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New BoxManager
            Dim oValue As New BoxTypeValue
            oValue.Id = SelectedId
            ret = oCManager.RemoveBoxType(oValue)
            ' inserire controllo se esistono box con questo type
        End If
        Return ret
    End Function
    
    
    
    Sub LoadFormDett(ByVal oValue As BoxTypeValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Id
                Description.Text = .Description
            End With
        Else
            lblDescrizione.InnerText = "New BoxType"
            lblId.InnerText = ""
            Description.Text = Nothing
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New BoxTypeValue
        With (oValue)
            If SelectedId > 0 Then .Id = SelectedId
            .Description = Description.Text
        End With

        oCManager = New BoxManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As BoxSearcher = Nothing)
        oCManager = New BoxManager
        oCollection = New BoxTypeCollection
        
        oCManager.Cache = False
        oCollection = oCManager.ReadBoxType(0)
        If Not oCollection Is Nothing Then
            'oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        'oSearcher = New BoxSearcher
        'If txtId.Text <> "" Then oSearcher.Id = txtId.Text
        'If ddlType.SelectedValue <> "" Then oSearcher.BoxType.Id = ddlType.SelectedValue
        BindGrid(objGrid, Nothing)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> BoxTypeGenericComparer.SortType.ById Then
                    SortType = BoxTypeGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Name"
                If SortType <> BoxTypeGenericComparer.SortType.ByDescription Then
                    SortType = BoxTypeGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As BoxTypeGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = BoxTypeGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As BoxTypeGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As BoxTypeGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = BoxTypeGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As BoxTypeGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoToBoxManager(ByVal s As Object, ByVal e As EventArgs)
        Dim oWRV As New WebRequestValue
        Dim oMPM As New MasterPageManager()
        Dim oQS As New ObjQueryString
        oWRV.KeycontentType = New ContentTypeIdentificator("CMS", "boxmanager")
        oWRV.customParam.append(oQS)
        Response.Redirect(oMPM.FormatRequest(oWRV))
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>

<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid"  runat="server">
    <asp:button ID="btnNew" runat="server" Text="New" onclick="NewItem" cssclass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:button cssclass="button" ID="btnGoBoxManager" runat="server" text="Go Box Manager" Style ="width:130px" onclick="GoToBoxManager" />  
    <hr />
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    AllowSorting="true"
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridList_Sorting"
                    emptydatatext="No item available"                
                    OnRowDataBound="gridList_RowDataBound"
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                    >
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="85%" DataField="Description" HeaderText="Description" SortExpression="Description" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBoxTypeDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
    </table>
</asp:Panel>