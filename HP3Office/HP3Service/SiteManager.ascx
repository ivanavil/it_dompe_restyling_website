<%@ Control Language="VB" ClassName="SiteManagerControll" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%--<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>--%>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlCultures" Src ="~/hp3Office/HP3Parts/ctlCulturesList.ascx" %>

<script runat="server">
     
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
    Public siteManager As New SiteManager()
    Private oLManager As New LanguageManager()
    Private objCMSUtility As New GenericUtility
    Private oGenericUtlity As New GenericUtility
    Private oSiteSearcher As New SiteSearcher
    Private oSiteManager As SiteManager
    Private oSiteValue As SiteValue
    Private strDomain As String
    Private strJS As String
    Private strName As String 
    Private utility As New WebUtility
    Private _strDomain As String
    Private _language As String
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Domain() = strDomain
        
        If Not (Page.IsPostBack) Then
            BindCultures()
            
            If mPageManager.GetContent.Id = 0 Then
                ActivePanelGrid()
                LoadArchive()
            End If
        End If
    End Sub
   
    'recupera la lista dei siti
    Sub LoadArchive()
        Dim oSiteSearcher As New SiteSearcher
        Dim oSiteCollection As New SiteCollection
        oSiteManager = New SiteManager
              
        oSiteManager.Cache = False
        oSiteCollection = oSiteManager.Read(oSiteSearcher)
       
        gdw_siteList.DataSource = oSiteCollection
        gdw_siteList.DataBind()
    End Sub
    
    ' modifica un singolo record
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim oSiteValue As New SiteValue
        Dim ositeSearcher As New SiteSearcher
        Dim domainId As Integer
        
        oSiteManager = New SiteManager
        
        If sender.CommandName = "SelectItem" Then
            domainId = sender.CommandArgument
            SelectedId = domainId
            
            oSiteManager.Cache = False
            ositeSearcher.Key.Id = domainId
            oSiteValue = oSiteManager.Read(ositeSearcher)(0)
       
            If Not oSiteValue Is Nothing Then
                LoadDett(oSiteValue)
                ActivePanelDett()
            End If
                       
        End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim oSiteValue As New SiteValue
        Dim oSiteAreaValue As New SiteAreaValue
        oSiteManager = New SiteManager
        
        If (IsNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            With (oSiteValue)
                .Domain = siteDomain.Text
                .mainTheme = txtIdFather.Text
                .Folder = siteRoot.Text
                .RedirectUrl = siteRedirect.Text
                .SiteLabel = domainSiteLabel.Text
                .Label = domainLabel.Text
                .AltDomain = altDomain.Text
                .SessionLength = domainSessionLength.Text
                .UserName = domainUserName.Text
                .Password = domainPassword.Text
                '.DefLanguage.Id = lSelector.Language
                .DefLanguage.Id = lSelector.Language
                .IPFilterCountryRedirect = countryRedirect.Text
                .Status = siteStatus.Checked
                .Security = domainSecurity.Checked
                .IsCountryRedirect = isCountryRedirect.Checked
                .IsSiteTest = isSiteTest.Checked
                .AccessType = AccessType.Checked
                
                If txtHideSite.Text <> "" Then
                    .KeySiteArea.Id = txtHideSite.Text
                End If
                
                If dwlCulture.SelectedItem.Value = "-1" Then
                    .SiteCulture = Nothing
                Else
                    .SiteCulture = dwlCulture.SelectedItem.Value
                End If
                
            End With
         
            If SelectedId = 0 Then
                oSiteManager.Create(oSiteValue)
                LoadArchive()
                ActivePanelGrid()
                                 
            Else
                oSiteValue.Key = New SiteIdentificator(SelectedId)
                oSiteManager.Update(oSiteValue)
                LoadArchive()
                ActivePanelGrid()
            
            End If
        
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Sub
    
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
    
    'popola i campi di testo con le informazioni contenute nel SiteValue
    Sub LoadDett(ByVal oSiteValue As SiteValue)
      
        If Not oSiteValue Is Nothing Then
            siteDomain.Text = oSiteValue.Domain
            txtIdFather.Text = oSiteValue.mainTheme
            Father.Text = ReadFatherTheme(oSiteValue.mainTheme)
            siteRoot.Text = oSiteValue.Folder
            siteRedirect.Text = oSiteValue.RedirectUrl
            domainSiteLabel.Text = oSiteValue.SiteLabel
            domainLabel.Text = oSiteValue.Label
            altDomain.Text = oSiteValue.AltDomain
            domainSessionLength.Text = oSiteValue.SessionLength
            domainUserName.Text = oSiteValue.UserName
            domainPassword.Text = oSiteValue.Password
            countryRedirect.Text = oSiteValue.IPFilterCountryRedirect
            
            'lSelector.DefaultValue = oSiteValue.DefLanguage.Id 'objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
            lSelector.LoadLanguageValue(oSiteValue.DefLanguage.Id)
            
            txtHideSite.Text = oSiteValue.KeySiteArea.Id
            txtSite.Text = ReadSiteValue(oSiteValue.KeySiteArea.Id)
            
            If oSiteValue.SiteCulture <> "" Then
                dwlCulture.SelectedIndex = dwlCulture.Items.IndexOf(dwlCulture.Items.FindByValue(oSiteValue.SiteCulture))
            Else
                dwlCulture.SelectedIndex = -1
            End If
            
            If (oSiteValue.Status) Then
                siteStatus.Checked = True
            End If
        
            If (oSiteValue.Security) Then
                domainSecurity.Checked = True
            End If
            
            If (oSiteValue.IsCountryRedirect) Then
                isCountryRedirect.Checked = True
            Else
                isCountryRedirect.Checked = False
            End If
            
            If (oSiteValue.IsSiteTest) Then
                isSiteTest.Checked = True
            Else
                isSiteTest.Checked = False
            End If
            
            If (oSiteValue.AccessType) Then
                AccessType.Checked = True
            Else
                AccessType.Checked = False
            End If
                    
        Else
            siteDomain.Text = Nothing
            txtIdFather.Text = 0
            siteRoot.Text = Nothing
            siteRedirect.Text = Nothing
            domainSiteLabel.Text = Nothing
            domainLabel.Text = Nothing
            altDomain.Text = Nothing
            domainSessionLength.Text = Nothing
            domainUserName.Text = Nothing
            domainPassword.Text = Nothing
            countryRedirect.Text = Nothing
            Father.Text = Nothing
            
            siteStatus.Checked = True
            domainSecurity.Checked = False
            isCountryRedirect.Checked = False
            isSiteTest.Checked = False
            AccessType.Checked = False
            dwlCulture.SelectedIndex = -1
            'lSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
                         
        End If
    End Sub
    
    Function getStatus(ByVal sValue As SiteValue) As String
        Dim _status As String = ""
        If (sValue.Status) Then
            _status = "content_active"
        Else
            _status = "content_noactive"
        End If
        Return _status
    End Function
    
    Function getSecurity(ByVal sValue As SiteValue) As String
        Dim _security As String = ""
        If (sValue.Security) Then
            _security = "padlock_close"
        Else
            _security = "padlock_open"
        End If
        Return _security
    End Function
 
    'restituisce il sito selezionato 
    'Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
    '    Dim oSiteAreaCollection As Object = olistItem.GetSelection
    '    If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
    '        Return oSiteAreaCollection.item(0)
    '    End If
    '    Return Nothing
    'End Function
        
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = objCMSUtility.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
    
    'Lettura delle descrizioni dei themi dato l'id
    Function ReadFatherTheme(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetThemeValue(id)
        
        If ov Is Nothing Then
            Return Nothing
        Else
            Return ov.Description
        End If
    End Function
    
    'controlla che i campi di testo non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (siteDomain.Text = String.Empty) Then strName = "Site Domain"
               
        If (siteRoot.Text = String.Empty) Then strName = "Root"
        
        'If (siteRedirect.Text = String.Empty) Then strName = "Site Redirect"
                      
        If ((siteDomain.Text <> String.Empty) And (siteRoot.Text <> String.Empty)) Then
            _return = True
        End If
        
        Return _return
    End Function
     
    'popola la dwl delle cultures
    Sub BindCultures()
        Dim cultureCollection As CultureCollection
        Dim CultureManager As New CultureManager
        Dim oCultureSearcher As New CultureSearcher
             
        cultureCollection = CultureManager.Read(oCultureSearcher)
        
        utility.LoadListControl(dwlCulture, cultureCollection, "Code", "Code")
        dwlCulture.Items.Insert(0, New ListItem("--Select Culture--", -1))
    End Sub
    
    Protected Sub gdw_siteList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindGrid(sender)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView)
        Dim oSiteSearcher As New SiteSearcher
        Dim oSiteCollection As New SiteCollection
        oSiteManager = New SiteManager
              
        oSiteManager.Cache = False
        oSiteCollection = oSiteManager.Read(oSiteSearcher)
              
        objGrid.DataSource = oSiteCollection
        objGrid.DataBind()
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
       
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
   
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:Panel id="pnlGrid" runat="server" visible="false"> 
 <div>
    <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <asp:Label CssClass="title" runat="server" id="sectionTit">Site List</asp:Label>
 <asp:gridview id="gdw_siteList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                OnPageIndexChanging="gdw_siteList_PageIndexChanging"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="3%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate><img alt="Language: <%#oLManager.GetCodeLanguage(Container.DataItem.DefLanguage.Id)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.DefLanguage.Id)%>_small.gif" /></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Domain">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Domain")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Folder">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Folder")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Redirect Url">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "RedirectUrl")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <img id="imgStatus" src='<%#"/HP3Office/HP3Image/Ico/" & getStatus(Container.DataItem) & ".gif"%>'  title="Status" alt=""/>
                            <img id="imgSecurity" src='<%#"/HP3Office/HP3Image/Ico/" & getSecurity(Container.DataItem) & ".gif"%>'  title="Security" alt=""/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <table  style=" margin-top:10px" class="form" width="99%">
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteLanguage&Help=cms_HelpSiteLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSiteLanguage", "Site Language")%> </td>
        <td><HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDefaultCulture&Help=cms_HelpDefaultCulture&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDefaultCulture", "Default Culture")%> </td>
        <td><asp:DropDownList id="dwlCulture"  runat="server"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteDomain&Help=cms_HelpSiteDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSiteDomain", "Site Domain")%> </td>
        <td><HP3:Text runat ="server" id="siteDomain" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
        <td>
            <table width="100%">
                <tr>
                    <td width="50px">
                        <asp:TextBox id="txtHideSite" runat="server" style="display:none"/>
                        <HP3:Text runat ="server" id="txtSite"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                    </td>
                    <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSite.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSite.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Site%>&ListId=<%=txtSite.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
               </tr>
            </table>        
        <%--<HP3:ctlSite runat="server" ID="objSite" TypeControl="combo" SiteType="Site" ItemZeroMessage="Select Site"/>--%>
        </td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRoot&Help=cms_HelpRoot&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRoot", "Root")%></td>
        <td><HP3:Text runat ="server" id="siteRoot" TypeControl ="TextBox" style="width:200px" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteRedirect&Help=cms_HelpSiteRedirect&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSiteRedirect", "Site Redirect")%></td>
        <td><HP3:Text runat ="server" id="siteRedirect" TypeControl ="TextBox" style="width:300px" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAltDomain&Help=cms_HelpAltDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAltDomain", "Alt Domain")%></td>
        <td><HP3:Text runat ="server" id="altDomain" TypeControl ="TextBox" style="width:200px" /></td>
    </tr>
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteStatus&Help=cms_HelpSiteStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSiteStatus", "Site Status")%></td>
        <td><asp:checkbox id="siteStatus" runat="server"  Checked="false"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDomainSiteLabel&Help=cms_HelpDomainSiteLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDomainSiteLabel", "Domain Site Label")%></td>
        <td><HP3:Text runat ="server" id="domainSiteLabel" TypeControl ="TextBox" style="width:200px" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDomainLabel&Help=cms_HelpDomainLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDomainLabel", "Domain Label")%></td>
        <td><HP3:Text runat ="server" id="domainLabel" TypeControl ="TextBox" style="width:200px" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDomainThemes&Help=cms_HelpDomainThemes&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDomainThemes", "Domain Themes")%></td>
         <td ><table width="100%">
                <tr>
                <td width="50px">
                    <asp:TextBox id="txtIdFather" runat="server" style="display:none"/>
                    <HP3:Text runat ="server" id="Father"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>    
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popThemes.aspx?SelItem=' + document.getElementById('<%=txtIdFather.clientid%>').value + '&ctlHidden=<%=txtIdFather.clientid%>&ctlVisible=<%=Father.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                </tr>
             </table>        
        </td>
    </tr>
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSessionLength&Help=cms_HelpSessionLength&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSessionLength", "Session Length")%></td>
        <td><HP3:Text runat ="server" id="domainSessionLength" TypeControl="NumericText" style="width:90px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDomainSecurity&Help=cms_HelpDomainSecurity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDomainSecurity", "Domain Security")%></td>
        <td><asp:checkbox id="domainSecurity" runat="server"  Checked="false"/></td>
    </tr>
     <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserName&Help=cms_HelpUserName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserName", "UserName")%></td>
        <td><HP3:Text runat ="server" id="domainUserName" TypeControl ="TextBox" style="width:90px" MaxLength="10"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPassword&Help=cms_HelpPassword&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPassword", "Password")%></td>
        <td><HP3:Text runat ="server" id="domainPassword" TypeControl ="TextBox" style="width:90px" MaxLength="10"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCountryRedirect&Help=cms_HelpCountryRedirect&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCountryRedirect", "IsCountryRedirect")%></td>
        <td><asp:checkbox id="isCountryRedirect" runat="server"  Checked="false"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFilterCountryRedirect&Help=cms_HelpFilterCountryRedirect&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFilterCountryRedirect", "IP Filter CountryRedirect")%></td>
        <td><HP3:Text runat ="server" id="countryRedirect" TypeControl ="TextBox" style="width:200px" MaxLength="1000"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIsSiteTest&Help=cms_HelpIsSiteTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIsSiteTest", "IsSiteTest")%></td>
        <td><asp:checkbox id="isSiteTest" runat="server"  Checked="false"/></td>
    </tr>
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAccessType&Help=cms_HelpIsAccessType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormAccessType", "Access Type")%></td>
        <td><asp:checkbox id="AccessType" runat="server"  Checked="false"/></td>
    </tr>
 </table>
</asp:Panel>

