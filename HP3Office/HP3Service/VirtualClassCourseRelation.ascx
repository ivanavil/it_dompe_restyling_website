<%@ Control Language="VB" ClassName="ContentQuestionnaire" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.VirtualClass"%>
<%@ Import Namespace="Healthware.HP3.LMS.VirtualClass.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagName="ctlLanguage" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"%>

<script runat="server">
    Private oLManager As New LanguageManager
    Private VCManager As New VirtualClassManager
    Private ContentManager As New ContentManager
    Private CatalogManager As New Healthware.HP3.LMS.Catalog.CatalogManager
    Private MasterPageManager As New MasterPageManager
    
    Sub Page_load()
        If Not Page.IsPostBack Then
            ReadVirtualClass()
            ReadRelations()
        End If
    End Sub
    
    'recupera la classe selezionata
    Sub ReadVirtualClass()
        Dim VCSearcher As New VirtualClassSearcher
        VCSearcher.Key.Id = Request("V")
        
        Dim VCColl As VirtualClassCollection = VCManager.Read(VCSearcher)
        
        If Not (VCColl Is Nothing) AndAlso (VCColl.Count > 0) Then
            Dim CVValue As VirtualClassValue = VCColl(0)
            
            lblVCDescription.Text = CVValue.Description
            lblVCId.Text = CVValue.Key.Id
        End If
    End Sub
    
    'recupera le relazioni virtual class / course 
    Sub ReadRelations()
        VCManager.Cache = False
        Dim CourseColl As CourseCollection = VCManager.ReadCourseClassRelation(New VirtualClassIdentificator(Request("V")))
        
        gridRelations.DataSource() = CourseColl
        gridRelations.DataBind()
    End Sub
    
    'aggiunge una nuova relazione
    Sub AddRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Add") Then
            Dim coursePk As Integer = sender.commandArgument
                                   
            VCManager.CreateCourseClassRelation(New CatalogIdentificator(coursePk), New VirtualClassIdentificator(Request("V")))
            ReadRelations()
        End If
    End Sub
    
    'elimina una relazione virtual class / course 
    Sub RemoveRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Remove") Then
            Dim coursePk As Integer = sender.commandArgument
            
            VCManager.RemoveCourseClassRelation(New CatalogIdentificator(coursePk), New VirtualClassIdentificator(Request("V")))
            ReadRelations()
        End If
    End Sub
    
    'ricerca i content in base ai valori selezionati nel form
    Sub SearchCourse(ByVal sender As Object, ByVal e As EventArgs)
        Dim courseSearcher As New CatalogSearcher
        
        If CourPk.Text <> "" Then courseSearcher.key = New CatalogIdentificator(CourPk.Text)
        If CourTitle.Text <> "" Then courseSearcher.SearchString = CourTitle.Text
       
        If (dwlTypeCourse.SelectedItem.Value <> 0) Then
            courseSearcher.CourseType = dwlTypeCourse.SelectedItem.Value
        End If
        
        CatalogManager.Cache = False
        Dim CourseColl As CourseCollection = CatalogManager.ReadCourses(courseSearcher)
        
        gridCourse.DataSource() = CourseColl
        gridCourse.DataBind()
    End Sub
    
    'fa un reset del form di ricerca
    Sub ResetResearch(ByVal sender As Object, ByVal e As EventArgs)
        CourPk.Text = Nothing
        CourTitle.Text = Nothing
        dwlTypeCourse.SelectedValue = 1
    End Sub
    
    'ritorna alla lista delle classi virtuali
    Sub BackToManager(ByVal sender As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        Dim objQs As New ObjQueryString
        
        webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "VClass")
        webRequest.customParam.append(objQs)
        Response.Redirect(MasterPageManager.FormatRequest(webRequest))
    End Sub
</script>

<script type="text/javascript">
   
</script>

<hr />
<asp:Panel ID="pnlHeader" runat="server">
    <asp:Button ID="btBackToManager" Text="Archive" CssClass="button" runat="server" OnClick="BackToManager" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
</asp:Panel>
<hr />

<asp:Panel ID="pnlCourseVC" runat="server">
   <asp:Panel ID="vcSummary" Width="100%" runat="server">
        <div class="title">Virtual Class Selected</div>
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:30%">Title</th>
            </tr>
            <tr>
                 <td><asp:Label ID="lblVCId" runat="server" /></td>
                <td><asp:Label ID="lblVCDescription" runat="server" /></td>
           </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="true" runat="server">
            <div class="title">Active Relations</div>
            <asp:GridView ID="gridRelations"
                AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
                ShowHeader="true" HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" width="100%"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Pk" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblPk" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="RemoveRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Course Searcher</span></td>
            </tr>
        </table>
        <asp:Table ID="tableSearchParameters" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>PK</asp:TableCell>
                <asp:TableCell>Title</asp:TableCell>
                <asp:TableCell>Type</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="CourPk" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="CourTitle" runat="server" typecontrol="TextBox" /></asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList id="dwlTypeCourse" runat="server">
                        <asp:ListItem Text="Distance Learning" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Residential Course" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Blended Learning" Value="3"></asp:ListItem>
                    </asp:DropDownList>
             </asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchParameters2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchRelated" Enabled="true" OnClick="SearchCourse" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
                <asp:TableCell><asp:Button ID="btnClearSearch" Enabled="true"  OnClick="ResetResearch" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <hr />
   
    <asp:Panel ID="pnlGridCourse" Width="100%" Visible="true" runat="server">
    <asp:Label runat="server" ID="msg"/>
        <asp:GridView ID="gridCourse" 
            AutoGenerateColumns="false"
            AllowPaging="true" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager" PageSize="20"
            AllowSorting="false"
            ShowHeader="true" HeaderStyle-CssClass="header"
            ShowFooter="true" FooterStyle-CssClass="gridFooter"
            GridLines="None" AlternatingRowStyle-BackColor="#eeefef" width="100%"
            runat="server">
            <Columns>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Language: <%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <asp:ImageButton ID="btn_add" ToolTip="Add content relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/insert.gif" onClick="AddRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
 </asp:Panel>   