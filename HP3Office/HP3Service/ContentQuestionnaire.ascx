<%@ Control Language="VB" ClassName="ContentQuestionnaire" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagName="ctlLanguage" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"%>

<script runat="server">
    Private oLManager As New LanguageManager
    Private QManager As New QuestionnaireManager
    Private ContentManager As New ContentManager
    Private MasterPageManager As New MasterPageManager
    
    Sub Page_load()
        If Not Page.IsPostBack Then
            ContentType.LoadControl()
            ReadQuestionnaire()
            ReadRelations()
        End If
    End Sub
    
    'recupera il questionario selezionato
    Sub ReadQuestionnaire()
        Dim QValue As QuestionnaireValue = QManager.Read(New QuestionnaireIdentificator(Request("Q")))
        
        If Not QValue Is Nothing Then
            lbQuestId.Text = QValue.Key.Id
            lblQuestDescription.Text = QValue.Description
        End If
        'OnRowDataBound="gridRelations_RowDataBound"
    End Sub
    
    'recupera le relazioni content/questionnaire
    Sub ReadRelations()
        Dim ContentColl As New ContentCollection
        
        Dim CQSearcher As New QuestionnaireContentSearcher
        CQSearcher.KeyQuestionnaire.Id = Request("Q")
        
        QManager.Cache = False
        Dim CQColl As QuestionnaireContentCollection = QManager.Read(CQSearcher)
                       
        gridRelations.DataSource() = CQColl
        gridRelations.DataBind()
    End Sub
    
    'aggiunge una nuova relazione
    Sub AddRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Add") Then
            Dim conPK As Integer = sender.commandArgument
            
            Dim CQValue As New QuestionnaireContentValue
            CQValue.KeyContent.PrimaryKey = conPK
            CQValue.KeyQuestionnaire.Id = Request("Q")
            
            QManager.Create(CQValue)
            ReadRelations()
        End If
    End Sub
    
    'elimina una relazione content/questionnaire
    Sub RemoveRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Remove") Then
            Dim relId As Integer = sender.commandArgument
            
            QManager.Remove(New QuestionnaireContentIdentificator(relId))
            ReadRelations()
        End If
    End Sub
    
    'ricerca i content in base ai valori selezionati nel form
    Sub SearchContent(ByVal sender As Object, ByVal e As EventArgs)
        Dim contentSearcher As New ContentSearcher
       
        If (txtDataValueFilter.Text <> String.Empty) Then contentSearcher.key.Id = txtDataValueFilter.Text
        If (txtDataTextFilter.Text <> String.Empty) Then contentSearcher.SearchString = txtDataTextFilter.Text
       
        Dim ctc As ContentTypeCollection = ContentType.GetSelection()
        'se viene scelto il contentType dalla dwnList
        If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
            contentSearcher.KeyContentType.Id = ctc.Item(0).Key.Id
        End If
        
        Dim contentColl As ContentCollection = ContentManager.Read(contentSearcher)
        
        gridContents.DataSource() = contentColl
        gridContents.DataBind()
    End Sub
    
    'fa un reset del form di ricerca
    Sub ResetResearch(ByVal sender As Object, ByVal e As EventArgs)
        txtDataValueFilter.Text = Nothing
        txtDataTextFilter.Text = Nothing
        ContentType.SetSelectedIndex(0)
    End Sub
    
    'ritorna alla lista dei questionari
    Sub BackToQuestionnaireManager(ByVal sender As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        Dim objQs As New ObjQueryString
        
        webRequest.KeycontentType = New ContentTypeIdentificator("HP3", "Quest")
        webRequest.customParam.append(objQs)
        Response.Redirect(MasterPageManager.FormatRequest(webRequest))
    End Sub
</script>

<script type="text/javascript">
   
</script>

<hr />
<asp:Panel ID="pnlHeader" runat="server">
    <asp:Button ID="btBackToBoxManager" Text="New Relation" CssClass="button" runat="server" OnClick="BackToQuestionnaireManager" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/box_manager.gif');background-repeat:no-repeat;background-position:1px 1px;" />
</asp:Panel>
<hr />

<asp:Panel ID="pnlContentQuestionnaire" runat="server">
    <asp:Panel ID="QuestSummary" Width="100%" runat="server">
        <div class="title">Questionnaire Selected</div>
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:30%">Description</th>
            </tr>
            <tr>
                <td><asp:Label ID="lbQuestId" runat="server" /></td>
                <td><asp:Label ID="lblQuestDescription" runat="server" /></td>
           </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="true" runat="server">
            <div class="title">Active Relations</div>
            <asp:GridView ID="gridRelations"
                AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
                ShowHeader="true" HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" width="100%"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContent.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Pk" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblPk" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContent.PrimaryKey")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "KeyContent.IdLanguage"))%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ContentTitle")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="RemoveRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
            </tr>
        </table>
        <asp:Table ID="tableSearchParameters" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>Id</asp:TableCell>
                <asp:TableCell>Title</asp:TableCell>
                <asp:TableCell>Content Type</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></asp:TableCell>
                <asp:TableCell><HP3:ctlContentType ID="ContentType" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchParameters2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchRelated" Enabled="true" OnClick="SearchContent" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
                <asp:TableCell><asp:Button ID="btnClearSearch" Enabled="true"  OnClick="ResetResearch" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <hr />
   
    <asp:Panel ID="pnlGridContents" Width="100%" Visible="true" runat="server">
    <asp:Label runat="server" ID="msg"/>
        <asp:GridView ID="gridContents" 
            AutoGenerateColumns="false"
            AllowPaging="true" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager" PageSize="20"
            AllowSorting="false"
            ShowHeader="true" HeaderStyle-CssClass="header"
            ShowFooter="true" FooterStyle-CssClass="gridFooter"
            GridLines="None" AlternatingRowStyle-BackColor="#eeefef" width="100%"
            runat="server">
            <Columns>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Language: <%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <asp:ImageButton ID="btn_add" ToolTip="Add content relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/insert.gif" onClick="AddRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
 </asp:Panel>   