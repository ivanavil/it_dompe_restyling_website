<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>   
<%@ Import Namespace="Healthware.HP3.Core.Community"%>
<%@ Import Namespace="Healthware.HP3.Core.Community.ObjectValues"%>


<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %> 
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlExtraField" Src ="~/hp3Office/HP3Parts/ctlExtraField.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlProfiles" Src ="~/hp3Office/HP3Parts/ctlProfilesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Localization" Src ="~/hp3Office/HP3Parts/ctlGeo.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlUserGroups" Src ="~/hp3Office/HP3Parts/ctlUserGroups.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlContext" Src ="~/hp3Office/HP3Parts/ctlContextList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlCommunity" Src ="~/hp3Office/HP3Parts/ctlCommunityList.ascx"%>
<script runat="server">
    Private systemUtility As New SystemUtility()
    Private mPageManager As New MasterPageManager()
    Private userManager As New UserManager()
    Private userHCPManager As New UserHCPManager
    Private oGenericUtlity As New GenericUtility
    Private profilingManager As New ProfilingManager
    Private dictionaryManager As New DictionaryManager
    Private userGroupManager As New UserGroupManager
    Private ContextManager As New ContextManager
    
    Private omailTempalteManager As New MailTemplateManager
    Private omailTemplateValue As New MailTemplateValue
    Private omailTemplateColl As New MailTemplateCollection
    Private omailTemplateSearcher As New MailTemplateSearcher
    
    Private contentSearcher As New ContentSearcher
    Private _sortType As UserGenericComparer.SortType
    Private _sortOrder As UserGenericComparer.SortOrder
    Private extFManager As New ExtraFieldsManager
    
    Private utility As New WebUtility
       
    Private mailValue As New Healthware.HP3.Core.Utility.ObjectValues.MailValue
    Private _mySortType As GeoGenericComparer.SortType = GeoGenericComparer.SortType.ByDescription
    
    Private strJS As String
    Private strDomain As String
    Private strName As String
    Private siteLabel As String
    Private _strDomain As String
    Private _language As String
    
    Private PrivateData As Boolean = True
    Private _userValue As Integer
    Private serviceColl As New ServiceCollection
    Private communitySelected As Integer
    
    Const _Single As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._single
    Const Married As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._married
    Const Divorced As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._divorced
    Const Widowed As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._widowed
    Const Separated As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._separated
    Const Living_with_partner As Integer = Healthware.HP3.Core.User.ObjectValues.UserValue.MaritalStatusType._living_with_partner
    
    Private _oGeoCollection As GeoCollection
    Private _HCPGeoCollection As GeoCollection
    Private _oGeoManager As New GeoManager
    
    Private Const PageSize As Integer = 20
    Private Const NPageVis As Integer = 6
   
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property User() As Integer
        Get
            Return _userValue
        End Get
        Set(ByVal value As Integer)
            _userValue = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property SortType() As UserGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = UserGenericComparer.SortType.ByKey ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As UserGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As UserGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = UserGenericComparer.SortOrder.ASC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As UserGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
       
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = Me.PageObjectGetLang.Id
        Domain() = strDomain
             
        If Not (Page.IsPostBack) Then
            ActivePanelGrid()
            'LoadArchive()
            BindCommunity()
           
        Else
            BindSpeciality()
            'Carica se stesso con un type diverso
            
            'oLocalization.LoadControl()
            'hcpLocalization.LoadControl()
        End If
        
        BindAllUserGeo(-1, "user")
        BindAllUserGeo(-1, "hcp")
    End Sub
    
    Function ReadSons(ByVal item As Int32) As GeoCollection
        _oGeoManager.Cache = False
        Dim _oGeoSearcher = New GeoSearcher
        _oGeoSearcher.SonsOf = item
        
        Dim _oGeoCollection = _oGeoManager.Read(_oGeoSearcher)
        Return _oGeoCollection
    End Function

    Sub BindAllUserGeo(ByVal geoParent As Int16, ByVal type As String)
        Dim _oGeoCollection As GeoCollection = ReadSons(geoParent)
      
        If Not _oGeoCollection Is Nothing Then
            _oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
            Select Case type.ToLower
                Case "user"
                    For Each geoItem As GeoValue In _oGeoCollection
                        pnlGeoTree.Controls.Add(New LiteralControl(AddGetoToTree(geoItem, "USER").ToString))
                    Next
                Case "hcp"
                    For Each geoItem As GeoValue In _oGeoCollection
                        pnlGeoHCPTree.Controls.Add(New LiteralControl(AddGetoToTree(geoItem, "HCP").ToString))
                    Next
            End Select
        End If
    End Sub
    
    Function AddGetoToTree(ByVal item As GeoValue, ByVal type As String) As StringBuilder
        Dim outDiv As New StringBuilder
        
        Dim oGeoCollection As GeoCollection = ReadSons(item.Key.Id)
           
        Dim nCount As Int16
        Dim oGeoItem As GeoValue
        Dim bHasSons As Boolean
            
        If oGeoCollection Is Nothing Then
            nCount = 0
            bHasSons = False
        Else
            oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
            nCount = oGeoCollection.Count
            bHasSons = True
        End If
           
        'divMain
        outDiv.Append("<Div style='margin:10px' id='divMain'>" & vbCrLf)
            
        'divLink
        outDiv.Append("<Div id='divLink' onclick='DisplayDiv(""divSons_" & type & item.Key.Id & """,document.getElementById(""linkPlus_" & type & item.Key.Id & """));SelItem(this,""" & item.Key.Id & """,""" & item.Description.Replace("'", Chr(60)) & """, """ & type & """)' onmouseout='RestoreColor(this)' onmouseover='ChangeColor(this)' >" & vbCrLf)
        If nCount > 0 Then
            outDiv.Append("<a id='linkPlus_" & type & item.Key.Id & "'>+&nbsp;</a>")
        Else
            outDiv.Append("<a id='linkPlus_" & type & item.Key.Id & "'>&nbsp;&nbsp;&nbsp;</a>")
        End If
        outDiv.Append("<a id='linkDescr'>" & item.Description & "</a>" & vbCrLf)
            
        'divLink
        outDiv.Append("</Div>" & vbCrLf)
        '************************divLink********************************
            
        'divSons
        If bHasSons Then
            outDiv.Append("<Div id='divSons_" & type & item.Key.Id & "' style='display:none'>" & vbCrLf)
            For Each oGeoItem In oGeoCollection
                outDiv.Append(AddGetoToTree(oGeoItem, type))
            Next
            outDiv.Append("</Div>" & vbCrLf)
        End If
            
        outDiv.Append("</Div>" & vbCrLf)
               
        Return outDiv
    End Function
    
    Sub Page_prerender()
       
        If (ctlCommunity.ListIndex > 0) Then
            communitySelected = ctlCommunity.ListIndex
            Dim contentIdentificator As New ContentIdentificator()
            contentIdentificator.PrimaryKey = communitySelected
            
            Dim contentValue As ContentValue = Me.BusinessContentManager.Read(contentIdentificator)
            communityId.Text = contentValue.Key.Id
            communityLanguageId.Text = contentValue.Key.IdLanguage
           
            If (LoadService(communityId.Text, communityLanguageId.Text)) Then
                divCommunity.Visible = True
                LoadServiceUserPermission()
            End If
        End If
    End Sub
      
    'recupera la lista dei siti
    Sub LoadArchive()
        'Dim userSearcher As New UserSearcher
        'Dim userColl As New UserCollection
                      
        'userManager.Cache = False
        'userColl = userManager.Read(userSearcher)
       
        ' sectionTit.Visible = True
        ' gdw_usersList.DataSource = userColl
        ' gdw_usersList.DataBind()
        
       BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
       
        
    Sub AddItemHCPFatherText(ByVal s As Object, ByVal e As EventArgs)
        Dim geoId As String
               
        geoId = txtHCPFatherHidden.Text
        If geoId <> "" Then
            HCPFather.Text = ReadGeo(Int32.Parse(geoId))
        Else
            strJS = "alert('Select an element from the list hcp')"
        End If
    End Sub
    
    Sub AddItemGeoText(ByVal s As Object, ByVal e As EventArgs)
        Dim geoId As String
               
        geoId = _txtFatherHidden.Text
        If geoId <> "" Then
            _Father.Text = ReadGeo(Int32.Parse(geoId))
        Else
            strJS = "alert('Select an element from the list hcp')"
        End If
    End Sub
    
    Function ReadGeo(ByVal item As Integer) As String
        _oGeoManager = New GeoManager
        _oGeoManager.Cache = False 'necessario
            
        Dim oGeoValue As GeoValue = _oGeoManager.Read(New GeoIdentificator(item))
        If Not (oGeoValue) Is Nothing Then
            Return oGeoValue.Description
        End If
        
        Return Nothing
    End Function
    
    ' Edit di un utente di tipo User generico
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userValue As New UserValue
        Dim userSearcher As New UserSearcher
        Dim UserId As Integer
        divCommunity.Visible = False
        
        If sender.CommandName = "SelectItem" Then
            UserId = sender.CommandArgument
            SelectedId = UserId
           
            userManager.Cache = False
            userValue = userManager.Read(New UserIdentificator(UserId))
            If Not userValue Is Nothing Then
                
                If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.HCP) Then
                    EditHCP()
                End If
                
                If (IsSiteSelected()) Then
                    btnSendMail.Visible = True
                       
                End If
                'se lo status � Pending
                If (userValue.Status = 2) And (IsSiteSelected()) Then 'se � stato selezionato un sito 
                    btnRegistration_1.Visible = True
                    btnRegistration_2.Visible = True
                Else
                    btnRegistration_1.Visible = False
                    btnRegistration_2.Visible = False
                End If
                
                User = userValue.Key.Id
                LoadUserDett(userValue)
                ActivePanelDett()
                
                'extra fields
                Dim coll As ExtraFieldsCollection = UserExtFields()
                If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
                    rp_extrafield.DataSource = coll
                    rp_extrafield.DataBind()
               
                    divExtraFields.Visible = True
                End If
                
            End If
        End If
        
        If sender.CommandName = "ActiveUser" Then
            UserId = sender.CommandArgument
            SelectedId = UserId
           
            userManager.Cache = False
            userValue = userManager.Read(New UserIdentificator(UserId))
            If Not userValue Is Nothing Then
                userValue.Status = 1
                userManager.Update(userValue)
            End If
            BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
            'LoadArchive()
        End If
    End Sub
    
    'Edit di un utente di tipo HCP
    Sub EditHCP()
        Dim userHCPValue As New UserHCPValue
        Dim userHCPSearcher As New UserHCPSearcher
                     
        userManager.Cache = False
        userHCPSearcher.Key.Id = SelectedId
        userHCPManager.Cache = False
        userHCPValue = userHCPManager.Read(userHCPSearcher)(0)
       
        If Not userHCPValue Is Nothing Then
            LoadHCPDett(userHCPValue)
        End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
        'LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
        Dim userHCPValue As New UserHCPValue
        Dim user As New UserValue
        Dim userHcp As New UserHCPValue
                
        If SelectedId = 0 Then
           
            If (ddlUserType.SelectedItem.Text = "User") Then
                userValue = SetUserDett()
                If Not (userValue Is Nothing) Then
                    user = userManager.Create(userValue)
                    SaveExtraFields(user.Key.Id)
                    
                    'LoadArchive()
                    BindWithSearch(gdw_usersList)
                    ActivePanelGrid()
                End If
                'gestione dei ruoli e dei profili
                If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then ProfilesManager(user.Key)
                If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then RolesManager(user.Key)
                
                'gestione dei gruppi
                If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then GroupsManager(user.Key)
                 
                'gestione dei gruppi
                If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then ContextListManager(user.Key)
                
                ''gestione dell'associazione user/community
                'CommunityManager(user.Key)
                
                ''gestione community/service/permission
                'SaveServiceUserPermission(user.Key)
            End If
           
            If (ddlUserType.SelectedItem.Text = "HCP") Then
                userHCPValue = SetHCPDett()
                If Not (userHCPValue Is Nothing) Then
                    userHcp = userHCPManager.Create(userHCPValue)
                    SaveExtraFields(userHcp.Key.Id)
                    
                    AssociateUserContext(userHcp)
                    'LoadArchive()
                    BindWithSearch(gdw_usersList)
                    ActivePanelGrid()
                End If
                'gestione dei ruoli e dei profili
                If (Not userHcp.Key Is Nothing) AndAlso (userHcp.Key.Id > 0) Then ProfilesManager(userHcp.Key)
                If (Not userHcp.Key Is Nothing AndAlso userHcp.Key.Id > 0) Then RolesManager(userHcp.Key)
                                    
                'gestione dei gruppi
                If (Not userHcp.Key Is Nothing) AndAlso (userHcp.Key.Id > 0) Then GroupsManager(userHcp.Key)
                
                'gestione dei gruppi
                If (Not userHcp.Key Is Nothing) AndAlso (userHcp.Key.Id > 0) Then ContextListManager(userHcp.Key)
                
                ''gestione dell'associazione user/community
                'CommunityManager(userHcp.Key)
                
                ''gestione community/service/permission
                'SaveServiceUserPermission(user.Key)
            End If
            
        Else
            'update
            userValue = New UserValue
            userValue = userManager.Read(New UserIdentificator(SelectedId))
            user = userValue
           
            Dim userType As Integer = userValue.UserType
            
            'se stiamo mofificando un Utente di tipo User
            If (ddlUserType.SelectedItem.Text = "User") Then
                userValue = New UserValue
                userValue = SetUserDett()
                If Not (userValue Is Nothing) Then
                    userValue.Key = New UserIdentificator(SelectedId)
                    If Not (userType = 1) Then 'se cambiamo il tipo Utente da HCP ad USER
                        'userHCPManager.Remove(New UserIdentificator(SelectedId))
                        userManager.Update(userValue) ''oppure creare una remove che cancella solo uno user hcp
                        'update extrafields
                        SaveExtraFields(userValue.Key.Id)
                        RemoveUserContext(userValue.Key)
                    
                    Else
                        userManager.Update(userValue) ''dovrei fare la Create invece dell'apdate
                        SaveExtraFields(userValue.Key.Id)
                    End If
                    'LoadArchive()
                    BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
                    ActivePanelGrid()
                End If
            End If
            
            'se stiamo mofificando un Utente di tipo HCP
            If (ddlUserType.SelectedItem.Text = "HCP") Then
                userHCPValue = SetHCPDett()
                If Not (userHCPValue Is Nothing) Then
                    If Not (userType = 1) Then 'HCP=>HCP
                        userHCPValue.Key.Id = SelectedId
                        userHCPManager.Update(userHCPValue)
                        'update extrafields
                        SaveExtraFields(userValue.Key.Id)
                        'update associazione user-context  
                        UpdateUserContext(userHCPValue)
                        
                    Else 'USER=>HCP
                        'userManager.Remove(New UserIdentificator(SelectedId))
                        userHCPValue.Key.Id = SelectedId
                        userHCPManager.Create(userHCPValue)
                        
                        SaveExtraFields(userValue.Key.Id)
                        'creo associazione user -context
                        AssociateUserContext(userHCPValue)
                        
                    End If
                    'LoadArchive()
                    BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
                    ActivePanelGrid()
                End If
            End If
            
            'gestione dei ruoli e dei profili
            If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then RolesManager(user.Key)
            If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then ProfilesManager(user.Key)
                      
            'gestione dei gruppi
            If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then GroupsManager(user.Key)
            
            'gestione dei gruppi
            If (Not user.Key Is Nothing) AndAlso (user.Key.Id > 0) Then ContextListManager(user.Key)
            
            ''gestione dell'associazione user/community
            'CommunityManager(user.Key)
            
            ''gestione community/service/permission
            'SaveServiceUserPermission(user.Key)
                   
        End If
    End Sub
    
    'gestisce l'associazione dell'utente ad un certo context
    Function AssociateUserContext(ByVal hcpValue As UserHCPValue) As Boolean
        If (dwlHcpSpecialty.SelectedValue <> -1) Then
            Return Me.BusinessUserManager.CreateUserContext(hcpValue.Key.Id, dwlHcpSpecialty.SelectedItem.Value)
        End If
       
        Return False
    End Function
   
    'legge le associazioni utente - context
    Sub readUserContext(ByVal hcpValue As UserHCPValue)
        Dim contextColl As New ContextCollection
        Dim groupId = Integer.Parse(ConfigurationManager.AppSettings("GroupContextUT"))
        
        contextColl = Me.BusinessContextManager.ReadContextUser(hcpValue.Key, New GroupIdentificator(groupId))
        
        If Not (contextColl Is Nothing) AndAlso (contextColl.Count > 0) Then
            Dim contextId = contextColl(0).Key.Id
            dwlHcpSpecialty.SelectedIndex = dwlHcpSpecialty.Items.IndexOf(dwlHcpSpecialty.Items.FindByValue(contextId))
        End If
    End Sub
    
    'Per ogni context del gruppo, controllo che ci sia un associazione utente/context se esiste la cancello 
    'Infine, creo una nuova relazione utente/context
    Function UpdateUserContext(ByVal hcpValue As UserHCPValue) As Boolean
        Dim contextSearcher As New ContextSearcher
       
        contextSearcher.KeyContextGroup.Id = Integer.Parse(ConfigurationManager.AppSettings("GroupContextUT"))
        'contextSearcher.KeyFather.Id = 0
        'recupero i context di un certo gruppo
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        
        'cancello tutte le relazioni context/user 
        If Not (contextColl Is Nothing) Then
            For Each context As ContextValue In contextColl
                Me.BusinessUserManager.RemoveUserContext(hcpValue.Key.Id, context.Key.Id)
            Next
            
            'gestisce l'associazione utente-context
            If (dwlHcpSpecialty.SelectedValue <> -1) Then
                Return Me.BusinessUserManager.CreateUserContext(hcpValue.Key.Id, dwlHcpSpecialty.SelectedItem.Value)
            End If
            
        End If
    End Function
    
    'rimuove una associazione  user-context
    Function RemoveUserContext(ByVal userKey As UserIdentificator) As Boolean
        Dim contextSearcher As New ContextSearcher
        Dim _return As Boolean = False
        
        contextSearcher.KeyContextGroup.Id = Integer.Parse(ConfigurationManager.AppSettings("GroupContextUT"))
        'contextSearcher.KeyFather.Id = 0
        'recupero i context di un certo gruppo
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        
        'cancello tutte le relazioni context/user 
        If Not (contextColl Is Nothing) Then
            For Each context As ContextValue In contextColl
                _return = Me.BusinessUserManager.RemoveUserContext(userKey.Id, context.Key.Id)
            Next
        End If
        
        Return _return
    End Function
    
    'carica il controllo che gestisce l'associazioni Community/Utente
    Sub LoadCommunityControl(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim Userident As Integer
        Dim UserValue As UserValue
        
        divCommunity.Visible = False
        If sender.CommandName = "Community" Then
            Userident = sender.CommandArgument
            ActivePanelCommunity()
            
            UserValue = userManager.Read(New UserIdentificator(Userident))
            If Not UserValue Is Nothing Then
                'gestione community
                userId.Text = UserValue.Key.Id
                
                ctlCommunity.GenericCollection = GetUserCommunityValue(UserValue.Key.Id)
                'Response.Write(CType(ctlCommunity.GenericCollection, CommunityCollection).Count)
                ctlCommunity.LoadControl()
            End If
           
        End If
    End Sub
    
    'gestisce il salvataggio dell'associazione community/utente
    Sub SaveCommunityRelation(ByVal sender As Object, ByVal e As EventArgs)
        'gestione dell'associazione user/community
        Dim userIdent As New UserIdentificator()
        userIdent.Id = userId.Text
        
        CommunityManager(userIdent)
        strJS = "alert('Select a Community to Continue!')"
        
        'btSaveRelation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    '
    Sub SaveSegmentation(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
    
    'gestisce il salvataggio dell'associazione community/utente/permessi
    Sub SaveUserPermission(ByVal sender As Object, ByVal e As EventArgs)
        Dim userIdent As New UserIdentificator()
        userIdent.Id = userId.Text
        
        SaveServiceUserPermission(userIdent)
        
        'LoadArchive()
        BindWithSearch(gdw_usersList)
        ActivePanelGrid()
    End Sub
    
    'gestione dei ruoli
    Sub RolesManager(ByVal key As UserIdentificator)
        profilingManager.RemoveAllUserRole(key)
        If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
            Dim Rc As New Object
            Dim role As New RoleValue
            Rc = ctlRoles.GetSelection
            For Each role In Rc
                profilingManager.CreateUserRole(key, New RoleIdentificator(role.Key.Id))
            Next
        End If
    End Sub
    
    'gestione dei profili
    Sub ProfilesManager(ByVal key As UserIdentificator)
        
        profilingManager.RemoveAllUserProfile(key)
        If Not ctlProfiles.GetSelection Is Nothing AndAlso ctlProfiles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim profile As New ProfileValue
            Pc = ctlProfiles.GetSelection
            For Each profile In Pc
                 profilingManager.CreateUserProfile(key, New ProfileIdentificator(profile.Key.Id))
            Next
        End If
    End Sub
    
    'gestione dei gruppi
    Sub GroupsManager(ByVal key As UserIdentificator)
        userGroupManager.RemoveUserRelation(key)
        
        If Not ctluserGroups.GetSelection Is Nothing AndAlso ctluserGroups.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim group As New UserGroupValue
            Pc = ctluserGroups.GetSelection
            For Each group In Pc
                userGroupManager.CreateUserGroupRelation(key, New UserGroupIdentificator(group.Key.Id))
            Next
        End If
    End Sub
    
    'gestione dei context
    Sub ContextListManager(ByVal key As UserIdentificator)
        Dim ContextColl As ContextCollection = GetUserContextValue(key.Id)
        
        If Not (ContextColl Is Nothing) AndAlso (ContextColl.Count > 0) Then
            For Each elam As ContextValue In ContextColl
                userManager.RemoveUserContext(key.Id, elam.Key.Id)
            Next
        End If
        
        If Not ctlContext.GetSelection Is Nothing AndAlso ctlContext.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim context As New ContextValue
            Pc = ctlContext.GetSelection
            For Each context In Pc
                userManager.CreateUserContext(key.Id, context.Key.Id)
            Next
        End If
    End Sub
    
    'gestione community
    Sub CommunityManager(ByVal key As UserIdentificator)
        Me.BusinessCommunityManager.RemoveAllUser(key)
        
        If Not ctlCommunity.GetSelection Is Nothing AndAlso ctlCommunity.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim community As New CommunityValue
            Pc = ctlCommunity.GetSelection
            For Each community In Pc
                Me.BusinessCommunityManager.AddUser(New CommunityIdentificator(community.Key.Id, community.Key.IdLanguage), key)
            Next
        End If
    End Sub
       
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        'extra fields 
        Dim coll As ExtraFieldsCollection = UserExtFields()
        If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
            rp_extrafield.DataSource = coll
            rp_extrafield.DataBind()
               
            divExtraFields.Visible = True
        End If
        
        LoadUserDett(Nothing)
        LoadHCPDett(Nothing)
    End Sub
    
    Function UserExtFields() As ExtraFieldsCollection
        Dim extFieldManager As New ExtraFieldsManager
        Dim extFieldSearcher As New ExtraFieldsSearcher
        extFieldSearcher.Template = ExtraFieldsValue.ExtraFieldsTemplate.UserTemplate
        
        extFieldManager.Cache = False
        Dim coll As ExtraFieldsCollection = extFieldManager.Read(extFieldSearcher)
        
        Return coll
    End Function
    
      
    Sub LoadExtraFields(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim extraFId As Literal = CType(e.Item.FindControl("extFId"), Literal)
        Dim textField As ctlExtraField = CType(e.Item.FindControl("extrF"), ctlExtraField)
        
        If Not extraFId Is Nothing AndAlso Not textField Is Nothing Then
            Select Case textField.TypeControl
                Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                    textField.Text() = ReadExtraFieldValue(extraFId.Text)
                    
                Case ctlExtraField.ControlType.BooleanType
                    Dim extFcheck As String = ReadExtraFieldValue(extraFId.Text)
                   
                    If (extFcheck = "True") Then
                        textField.Checked() = True
                    ElseIf (extFcheck = "False") Then
                        textField.Checked() = False
                    End If
            End Select

        End If
        
        'If e.Item.ItemType = ListItemType.Item Then
    End Sub
         
    Function ReadExtraFieldValue(ByVal extFId As String) As String
        If (extFId <> String.Empty) And (SelectedId <> 0) Then
            Dim extFStoreSearcher As New ExtraFieldsStoreSearcher
        
            extFStoreSearcher.KeyExtraField.Id = CType(extFId, Integer)
            extFStoreSearcher.KeyUser.Id = SelectedId
            
            extFManager.Cache = False
            Dim extrFColl As ExtraFieldsStoreCollection = extFManager.Read(extFStoreSearcher)
            If Not (extrFColl Is Nothing) AndAlso (extrFColl.Count > 0) Then
                Dim extFieldValue As ExtraFieldsStoreValue = extFManager.Read(extFStoreSearcher)(0)
                Return extFieldValue.Value
            End If
        End If
        
        Return Nothing
    End Function
    
    'DA FARE L'UPDATE
    Sub SaveExtraFields(ByVal userId As Integer)
        If (rp_extrafield.Items.Count > 0) Then
                      
            For Each elem As RepeaterItem In rp_extrafield.Items
                Dim extraFId As Literal = CType(elem.FindControl("extFId"), Literal)
                Dim textField As ctlExtraField = CType(elem.FindControl("extrF"), ctlExtraField)
                
                Dim extFValue As New ExtraFieldsStoreValue
                extFValue.KeyExtraField.Id = CType(extraFId.Text, Integer)
                extFValue.KeyUser.Id = userId
                
                Select Case textField.TypeControl
                    Case ctlExtraField.ControlType.StringType, ctlExtraField.ControlType.MultiFieldType, ctlExtraField.ControlType.IntegerType, ctlExtraField.ControlType.DecimalType
                        extFValue.Value = textField.Text
                    Case ctlExtraField.ControlType.BooleanType
                        If (textField.Checked = True) Then
                            extFValue.Value = "True"
                        ElseIf (textField.Checked = False) Then
                            extFValue.Value = "False"
                        End If
                End Select
               
                If SelectedId = 0 Then ' create user
                    'If (textField.Text <> "") Then
                    extFManager.Create(extFValue)
                    'End If
                    
                Else 'update user
                    Dim extrFSearcher As New ExtraFieldsStoreSearcher
                    extrFSearcher.KeyExtraField.Id = CType(extraFId.Text, Integer)
                    extrFSearcher.KeyUser.Id = userId
                        
                    extFManager.Cache = False
                    Dim extFielColl As ExtraFieldsStoreCollection = extFManager.Read(extrFSearcher)
                    If Not (extFielColl Is Nothing) AndAlso (extFielColl.Count > 0) Then
                        extFValue.Key = extFielColl(0).Key
                        extFManager.Update(extFValue)
                    Else
                        extFManager.Create(extFValue)
                    End If
                End If
            Next
       
        End If
    End Sub
    
    'cancella una riga 
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim removeItemId As Integer
        Dim risRemove As Boolean
        Dim userValue As New UserValue
        
        'siteAreaManager = New SiteAreaManager
        If sender.CommandName = "DeleteItem" Then
            removeItemId = sender.CommandArgument
            SelectedId = removeItemId
             
            userValue = userManager.Read(New UserIdentificator(removeItemId))
           
            If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.User) Then
                risRemove = userManager.Delete(New UserIdentificator(removeItemId))
                'Response.Write(risRemove)
                'Response.End()
            End If
            
            If (userValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.HCP) Then
                risRemove = userHCPManager.Delete(New UserIdentificator(removeItemId))
            End If
            
            'LoadArchive()
            BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
            ActivePanelGrid()
            
            'Gestione dei valori di ritorno
            If Not risRemove Then
                CreateJsMessage("Impossible to delete this User")
            End If
        End If
    End Sub
    
    'popola i campi di testo con le informazioni contenute nello UserValue
    Sub LoadUserDett(ByVal userValue As UserValue)
        If Not userValue Is Nothing Then
            'userId.Text = userValue.Key.Id
            userName.Text = userValue.Name
            userSurname.Text = userValue.Surname
            userTitle.Text = userValue.Title
            userAddress.Text = userValue.Address.GetString
            userCap.Text = userValue.Cap
            userCell.Text = userValue.Cellular.GetString
            userCF.Text = userValue.CF
            userCity.Text = userValue.City
            userEmail.Text = userValue.Email.GetString
            userFax.Text = userValue.Fax.GetString
            
            'oLocalization.UserGeoId = userValue.Geo.Key.Id
            'oLocalization.BindGeo()
            
            If userValue.Geo.Key.Id <> 0 Then
                _txtFatherHidden.Text = userValue.Geo.Key.Id
                _Father.Text = oGenericUtlity.GetGeoValue(_txtFatherHidden.Text).Description
            End If
            
            siteSourceDesc.Text = userValue.SiteAcquaintedThrougt
            siteSourceId.Text = userValue.SiteAcquaintedThrougtId
            siteSourceStatus.Text = userValue.SiteAcquaintedThrougtStatus
                      
            If userValue.StatusActivationDate.HasValue Then
                siteActivationDate.Value = userValue.StatusActivationDate
            Else
                siteActivationDate.Clear()
            End If
            
            userProvince.Text = userValue.Province
            userPassword.Text = userValue.Password
            userTel.Text = userValue.Telephone.GetString
            userUserName.Text = userValue.Username
            
            'setta il tipo utente 
            getUserType(userValue)
                      
            txtHideSite.Text = userValue.RegistrationSite.Id
            txtSite.Text = ReadSiteValue(userValue.RegistrationSite.Id)
             
            'setta lo status
            getUserStatus(userValue)
                
            userGenter.SelectedIndex = userGenter.Items.IndexOf(userGenter.Items.FindByText(userValue.Gender))
            dwlMaritalStatus.SelectedIndex = dwlMaritalStatus.Items.IndexOf(dwlMaritalStatus.Items.FindByValue(userValue.MaritalStatus))
            
            If (userValue.CanReceiveNewsletter) Then
                flagNewsletter.Checked = True
            Else
                flagNewsletter.Checked = False
            End If
                              
            
            If userValue.Birthday.HasValue Then
                userBirthday.Value = userValue.Birthday.Value
            Else
                userBirthday.Clear()
            End If
            
            If userValue.DegreeDate.HasValue Then
                userDegreeDate.Value = userValue.DegreeDate.Value
            Else
                userDegreeDate.Clear()
            End If
            
            If userValue.RegistrationDate.HasValue Then
                userRegistrationDate.Value = userValue.RegistrationDate.Value
                'Else
                'userRegistrationDate.Clear()
            End If
            
            If userValue.StatusDataRichiesta.HasValue Then
                userRequestDate.Value = userValue.StatusDataRichiesta.Value
                'Else
                'userRequestDate.Clear()
            End If
            
            If userValue.StatusDataAzione.HasValue Then
                userActionDate.Value = userValue.StatusDataAzione.Value
                'Else
                'userActionDate.Clear()
            End If
                        
            If (ddlUserType.SelectedItem.Text = "HCP") Then
                ActivateHCPElements()
            Else
                EnableHCPElements()
            End If
            
            ''gestione ruoli
            ctlRoles.GenericCollection = GetUserRolesValue(userValue.Key.Id)
            ctlRoles.LoadControl()
              
            'gestione profili
            ctlProfiles.GenericCollection = GetUserProfilesValue(userValue.Key.Id)
            ctlProfiles.LoadControl()
            
            'gestione gruppi di utenti
            ctluserGroups.GenericCollection = GetUserGroupsValue(userValue.Key.Id)
            ctluserGroups.LoadControl()
            
            'gestione context 
            ctlContext.GenericCollection = GetUserContextValue(userValue.Key.Id)
            ctlContext.LoadControl()
            
            ''gestione community
            'ctlCommunity.GenericCollection = GetUserCommunityValue(userValue.Key.Id)
            'ctlCommunity.LoadControl()
              
            
        Else
            userName.Text = Nothing
            userSurname.Text = Nothing
            userTitle.Text = Nothing
            userAddress.Text = Nothing
            userCap.Text = Nothing
            userCell.Text = Nothing
            userCF.Text = Nothing
            userCity.Text = Nothing
            userEmail.Text = Nothing
            userFax.Text = Nothing
            
            'oLocalization.ClearList()
            _txtFatherHidden.Text = Nothing
            _Father.Text = Nothing
            
            'userGeoId.Text = Nothing
            siteSourceDesc.Text = Nothing
            siteSourceId.Text = Nothing
            siteSourceStatus.Text = Nothing
            siteActivationDate.Clear()
            
            userPassword.Text = Nothing
            userProvince.Text = Nothing
            userTel.Text = Nothing
            userUserName.Text = Nothing
               
            flagNewsletter.Checked = True
            
            ctlRoles.LoadControl()
            ctlProfiles.LoadControl()
            ctluserGroups.LoadControl()
            ctlContext.LoadControl()
            ctlCommunity.LoadControl()
            
            ddluserStatus.SelectedIndex = ddluserStatus.Items.IndexOf(ddluserStatus.Items.FindByValue(1))
            userGenter.SelectedIndex = userGenter.Items.IndexOf(userGenter.Items.FindByValue(1))
            ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(ddlUserType.Items.FindByText(1))
            dwlMaritalStatus.SelectedIndex = 0
            txtSite.Text = Nothing
            txtHideSite.Text = Nothing
            
            btnRegistration_1.Visible = False
            btnRegistration_2.Visible = False
            btnSendMail.Visible = False
            
            lbServicePermission.Items.Clear()
            
            'userActionDate.Clear()
            'userBirthday.Clear()
            'userDegreeDate.Clear()
            'userRegistrationDate.Clear()
            'userRequestDate.Clear()
                                
            If (ddlUserType.SelectedItem.Text = "HCP") Then
                ActivateHCPElements()
                LoadHCPDett(Nothing)
            Else
                EnableHCPElements()
            End If
        End If
    End Sub
    
    'popola i campi di testo con le informazioni contenute nello UserHCPValue
    Sub LoadHCPDett(ByVal userHCPValue As UserHCPValue)
      
        If Not userHCPValue Is Nothing Then
            hcpAddress.Text = userHCPValue.HCPAddress.GetString
            hcpCap.Text = userHCPValue.HCPCap
            hcpCell.Text = userHCPValue.HCPCellular.GetString
            hcpCity.Text = userHCPValue.HCPCity
            hcpEmail.Text = userHCPValue.HCPEmail.GetString
            hcpFax.Text = userHCPValue.HCPFax.GetString
            hcpTel.Text = userHCPValue.HCPTelephone.GetString
            hcpProvince.Text = userHCPValue.HCPProvince
            hcpRegistration.Text = userHCPValue.HCPRegistration
            hcpOtherInfo.Text = userHCPValue.HCPOtherInfo.GetString
            hcpMoreInfo.Text = userHCPValue.HCPMoreInfo.GetString
            hcpStructure.Text = userHCPValue.HCPStructure
            hcpWard.Text = userHCPValue.HCPWard
          
            'hcpLocalization.UserGeoId = userHCPValue.HCPGeo.Key.Id
            'hcpLocalization.BindGeo()
            
            If userHCPValue.HCPGeo.Key.Id <> 0 Then
                txtHCPFatherHidden.Text = userHCPValue.HCPGeo.Key.Id
                HCPFather.Text = oGenericUtlity.GetGeoValue(txtHCPFatherHidden.Text).Description
            End If
            
            readUserContext(userHCPValue)
        Else
            hcpAddress.Text = Nothing
            hcpCap.Text = Nothing
            hcpCell.Text = Nothing
            hcpCity.Text = Nothing
            hcpEmail.Text = Nothing
            hcpFax.Text = Nothing
            hcpProvince.Text = Nothing
            hcpTel.Text = Nothing
            hcpRegistration.Text = Nothing
            hcpOtherInfo.Text = Nothing
            hcpMoreInfo.Text = Nothing
            hcpStructure.Text = Nothing
            hcpWard.Text = Nothing
            dwlHcpSpecialty.SelectedIndex = -1
            'hcpLocalization.ClearList()
            txtHCPFatherHidden.Text = Nothing
            HCPFather.Text = Nothing
        End If
    End Sub
    
    'Setta tutte le informazioni di un utente User in un UserValue
    Function SetUserDett() As UserValue
        Dim UserValue As New UserValue
        Try
            
       
        If (IsUserDettNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            If (IsEmails(userEmail.Text)) Then
                With (UserValue)
                    .Name = userName.Text
                    .Surname = userSurname.Text
                    .Title = userTitle.Text
                    .Address.GetString = userAddress.Text
                    .Cap = userCap.Text
                    .Cellular.GetString = userCell.Text
                    .CF = userCF.Text
                    .City = userCity.Text
                    .Email.GetString = userEmail.Text
                    .Fax.GetString = userFax.Text
                    .Gender = userGenter.SelectedItem.Text
                    '.Geo.Key.Id = GetGeoSelection(oLocalization)
                    
                    If _txtFatherHidden.Text <> "" Then
                        .Geo.Key.Id = _txtFatherHidden.Text
                    End If
                        .Province = userProvince.Text
                        .SiteAcquaintedThrougt = siteSourceDesc.Text
                        If Not String.IsNullOrEmpty(siteSourceId.Text) Then .SiteAcquaintedThrougtId = siteSourceId.Text
                        If Not String.IsNullOrEmpty(siteSourceStatus.Text) Then .SiteAcquaintedThrougtStatus = siteSourceStatus.Text
                        .StatusActivationDate = siteActivationDate.Value
                    
                        .Password = userPassword.Text
                        .Telephone.GetString = userTel.Text
                                         
                        If (flagNewsletter.Checked) Then
                            .CanReceiveNewsletter = True
                        Else
                            .CanReceiveNewsletter = False
                        End If
                      
                        .Status = ddluserStatus.SelectedItem.Value
                        .Birthday = userBirthday.Value
                        .DegreeDate = userDegreeDate.Value
                        .RegistrationDate = userRegistrationDate.Value
                        .StatusDataRichiesta = userRequestDate.Value
                        .StatusDataAzione = userActionDate.Value
                        
                        If txtHideSite.Text <> "" Then
                            .RegistrationSite.Id = CType(txtHideSite.Text, Integer)
                        End If
                                                           
                        Select Case dwlMaritalStatus.SelectedItem.Value
                            Case 0
                                .MaritalStatus = 0
                            Case _Single
                                .MaritalStatus = _Single
                            Case Married
                                .MaritalStatus = Married
                            Case Divorced
                                .MaritalStatus = Divorced
                            Case Widowed
                                .MaritalStatus = Widowed
                            Case Separated
                                .MaritalStatus = Separated
                            Case Living_with_partner
                                .MaritalStatus = Living_with_partner
                        End Select
                   
                    End With
       
                Return UserValue
            Else
                CreateJsMessage("Inserire l'email nel giusto formato!")
            End If
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    'Setta tutte le informazioni per un Utente HCP in un UserHCPValue
    Function SetHCPDett() As UserHCPValue
        Dim userHCPValue As New UserHCPValue
        
        If (IsHCPDettNotNull() And (IsUserDettNotNull())) Then
            If (IsEmails(userEmail.Text) And (IsEmails(hcpEmail.Text))) Then
                With (userHCPValue)
                    .Name = userName.Text
                    .Surname = userSurname.Text
                    .Title = userTitle.Text
                    .Address.GetString = userAddress.Text
                    .Cap = userCap.Text
                    .Cellular.GetString = userCell.Text
                    .CF = userCF.Text
                    .City = userCity.Text
                    .Email.GetString = userEmail.Text
                    .Fax.GetString = userFax.Text
                    .Gender = userGenter.SelectedItem.Text
                    '.Geo.Key.Id = GetGeoSelection(oLocalization)
                    
                    If _txtFatherHidden.Text <> "" Then
                        .Geo.Key.Id = _txtFatherHidden.Text
                    End If
                    
                    .Province = userProvince.Text
                    .Password = userPassword.Text
                    
                    .SiteAcquaintedThrougt = siteSourceDesc.Text
                    If Not String.IsNullOrEmpty(siteSourceId.Text) Then .SiteAcquaintedThrougtId = siteSourceId.Text
                    If Not String.IsNullOrEmpty(siteSourceId.Text) Then .SiteAcquaintedThrougtStatus = siteSourceStatus.Text
                    .StatusActivationDate = siteActivationDate.Value
                    
                    .Telephone.GetString = userTel.Text
                    '.Username = userUserName.Text
                     
                    If (flagNewsletter.Checked) Then
                        .CanReceiveNewsletter = True
                    Else
                        .CanReceiveNewsletter = False
                    End If
                    
                    Select Case dwlMaritalStatus.SelectedItem.Value
                        Case 0
                            .MaritalStatus = 0
                        Case _Single
                            .MaritalStatus = _Single
                        Case Married
                            .MaritalStatus = Married
                        Case Divorced
                            .MaritalStatus = Divorced
                        Case Widowed
                            .MaritalStatus = Widowed
                        Case Separated
                            .MaritalStatus = Separated
                        Case Living_with_partner
                            .MaritalStatus = Living_with_partner
                    End Select
                   
                    
                    
                    .Status = ddluserStatus.SelectedItem.Value
                      
                    .Birthday = userBirthday.Value
                    .DegreeDate = userDegreeDate.Value
                    .RegistrationDate = userRegistrationDate.Value
                    .StatusDataRichiesta = userRequestDate.Value
                    .StatusDataAzione = userActionDate.Value
                       
                    If txtHideSite.Text <> "" Then
                        .RegistrationSite.Id = CType(txtHideSite.Text, Integer)
                    End If
                    
                    .HCPAddress.GetString = hcpAddress.Text
                    .HCPCap = hcpCap.Text
                    .HCPCellular.GetString = hcpCell.Text
                    .HCPCity = hcpCity.Text
                    .HCPEmail.GetString = hcpEmail.Text
                    .HCPFax.GetString = hcpFax.Text
                    '.HCPGeo.Key.Id = GetGeoSelection(hcpLocalization)
                    If txtHCPFatherHidden.Text <> "" Then
                        .HCPGeo.Key.Id = txtHCPFatherHidden.Text
                    End If
                    
                    .HCPTelephone.GetString = hcpTel.Text
                    .HCPStructure = hcpStructure.Text
                    .HCPWard = hcpWard.Text
                    .HCPRegistration = hcpRegistration.Text
                    .HCPOtherInfo.GetString = hcpOtherInfo.Text
                    .HCPMoreInfo.GetString = hcpMoreInfo.Text
                    .HCPProvince = hcpProvince.Text
                End With
        
                Return userHCPValue
            Else
                CreateJsMessage("Inserire l'email nel giusto formato!")
            End If
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Function
    
    'Gestisce il RecoveryPasword  
    Sub RecoveryPassword(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userValue As New UserValue
        If sender.CommandName = "RecoveryPassword" Then
            If (IsSiteSelected()) Then
               
                userValue = userManager.Read(New UserIdentificator(sender.CommandArgument))
                omailTemplateValue = getMailTemplate("RecoveryPasswordByEmail")
                SendMail(userValue)
            Else
                CreateJsMessage("Select a Site!")
            End If
        End If
    End Sub
    
    'Gestisce il Recovery Password 
    Sub RecoveryPassword(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
              
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RecoveryPasswordByEmail")
        SendMail(userValue)
        
        ActivePanelGrid()
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
        'LoadArchive()
    End Sub
    
    Sub ConfirmRegistration(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
              
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RegistrationConfirmed")
        SendMail(userValue)
        
        'ActivePanelGrid()
        'LoadArchive()
    End Sub
    
    Sub RejectRegistration(ByVal sender As Object, ByVal e As EventArgs)
        Dim userValue As New UserValue
        PrivateData = False
        
        userValue = userManager.Read(New UserIdentificator(SelectedId)) 'dovrebbe prendere solo la prima email
        omailTemplateValue = getMailTemplate("RegistrationRejected")
        SendMail(userValue)
        
        'ActivePanelGrid()
        'LoadArchive()
    End Sub
    
    'Si occupa del recoveri password 
    'Carica il gluisto MailTemplate e spedicse una mail all'utente con la sua Password e UserId
    Sub SendMail(ByVal userValue As UserValue)
        If Not (userValue Is Nothing) Then
            Try
                'omailTemplateValue = getMailTemplate("RecoveryPasswordByEmail")
                If Not (omailTemplateValue Is Nothing) Then
                    mailValue.MailTo.Add(New MailAddress(getFirstElem(userValue.Email)))
                    mailValue.MailFrom = New MailAddress(omailTemplateValue.SenderAddress, RepleceSender(omailTemplateValue.SenderName))
            
                    mailValue.MailBody = RepleceBody(omailTemplateValue.Body, userValue)
                    mailValue.MailSubject = RepleceSubject(omailTemplateValue.Subject)
                    mailValue.MailPriority = Healthware.HP3.Core.Utility.ObjectValues.MailValue.Priority.Normal
                    
                    If (omailTemplateValue.BccRecipients <> String.Empty) Then
                        mailValue.MailBcc.Add(New MailAddress(omailTemplateValue.BccRecipients))
                    End If
                    
                    If (omailTemplateValue.FormatType = 1) Then
                        mailValue.MailIsBodyHtml = True
                    Else
                        mailValue.MailIsBodyHtml = False
                    End If
             
                    'invio della mail
                    systemUtility.SendMail(mailValue)
                    CreateJsMessage("L'email � stata inviata correttamente!")
                Else
                    CreateJsMessage("MailTemplate non attivo! L'email non � stata inviata correttamente!")
                End If
                 
            Catch Err As System.Net.Mail.SmtpFailedRecipientException
                CreateJsMessage("L'email non � stata inviata correttamente!")
            End Try
            
        Else 'se non esiste 
            CreateJsMessage("I dati relativi a questo utente potrebbero non essere esatti!")
        End If
    End Sub
    
    'restituisce tutti i servizi associati ad una community
    Function getService(ByVal community As CommunityIdentificator) As ServiceCollection
        Return Me.BusinessCommunityManager.ReadServices(community)
    End Function
    
    'gestisce Community/Service/Permission
    Sub AddPermission(ByVal sender As Object, ByVal e As EventArgs)
        Dim serviceValue As Integer
        Dim permissionValue As Integer
        Dim serviceText As String
        Dim permissionText As String
        
        If (dwlService.SelectedIndex <> 0) Then
            serviceValue = CType(dwlService.SelectedItem.Value(), Integer)
            serviceText = dwlService.SelectedItem.Text
            
            permissionValue = dwlPermission.SelectedItem.Value()
            permissionText = dwlPermission.SelectedItem.Text
                                         
            lbServicePermission.Items.Add(New ListItem(serviceText & " --> " & permissionText, serviceValue & "|" & permissionValue))
        Else
            strJS = "alert('Select a Service')"
        End If
    End Sub
    
    'gestisce Community/Service/Permission
    Sub DeleteServicePermission(ByVal sender As Object, ByVal e As EventArgs)
        If Not (lbServicePermission.SelectedItem Is Nothing) Then
            lbServicePermission.Items.RemoveAt(lbServicePermission.SelectedIndex)
        Else
            strJS = "alert('Select an Service to remove ')"
        End If
    End Sub
    
    'carica i valori community/service/permission
    Sub LoadServiceUserPermission()
        Dim userIdentificator As New UserIdentificator()
        userIdentificator.Id = userId.Text
                
        If Not (serviceColl Is Nothing) Then
            lbServicePermission.Items.Clear()
            For Each serviceValue As ServiceValue In serviceColl
                'Response.Write("loadService:" & "community" & communitySelected & "user" & userIdentificator.Id & "service" & serviceValue.Key)
                Dim permission As BaseServiceValue.Permission = Me.BusinessCommunityManager.CheckServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), userIdentificator, serviceValue.Key)
              
                Select Case permission
                    Case BaseServiceValue.Permission.Read_Write
                        lbServicePermission.Items.Add(New ListItem(serviceValue.Description & " --> " & "Read_Write", serviceValue.Key & "|" & BaseServiceValue.Permission.Read_Write))
                            
                    Case BaseServiceValue.Permission.Read
                        lbServicePermission.Items.Add(New ListItem(serviceValue.Description & " --> " & "Read", serviceValue.Key & "|" & BaseServiceValue.Permission.Read))
                           
                End Select
               
            Next
        Else
            lbServicePermission.Items.Clear()
        End If
    End Sub
    
    
    'crea le associazioni community/service/permission
    Function SaveServiceUserPermission(ByVal key As UserIdentificator) As Boolean
        Try
            Dim servicePermissionCollection As Object = lbServicePermission.Items
                     
            Dim res As Boolean = Me.BusinessCommunityManager.RemoveAllServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), key)
            If Not (servicePermissionCollection Is Nothing) Then
                If (servicePermissionCollection.Count > 0) Then
                    For Each servicePermissionItem As ListItem In servicePermissionCollection
                        Dim resultStrings() As String = servicePermissionItem.Value.Split("|")
                                          
                        Select Case resultStrings(1)
                            Case 1
                                'Response.Write("Save" & "communitySelected" &  & "resultStrings(0)" & resultStrings(0) & "key" & key.Id)
                                Dim result As Boolean = Me.BusinessCommunityManager.AddServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), resultStrings(0), key, BaseServiceValue.Permission.Read)
                            Case 2
                                'Response.Write("Save" & "communitySelected" & CType(communityId.Text, Integer) & "resultStrings(0)" & resultStrings(0) & "key" & key.Id)
                                Dim result As Boolean = Me.BusinessCommunityManager.AddServicePermission(New CommunityIdentificator(CType(communityId.Text, Integer), CType(communityLanguageId.Text, Integer)), resultStrings(0), key, BaseServiceValue.Permission.Read_Write)
                        End Select
                                          
                    Next
                End If
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
   
    'popola la dwl delle community nel pannello della ricerca
    Sub BindCommunity()
        Dim communitySearcher As New CommunitySearcher
        Dim communityColl As New CommunityCollection
                      
        communityColl = Me.BusinessCommunityManager.Read(communitySearcher)
       
        utility.LoadListControl(dwlComm, communityColl, "Title", "Key.PrimaryKey")
        dwlComm.Items.Insert(0, New ListItem("--Select--", 0))
     
    End Sub
       
    
    'gestisce la ricerca degli Utente 
    Sub SearchUser(ByVal sender As Object, ByVal e As EventArgs)
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
       
    
    ''' <summary>
    ''' Caricamento della griglia degli utenti
    ''' Verifica la presenza di criteri di ricerca
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView, Optional ByVal page As Integer = 1)
        'Dim oSiteValue As SiteValue
        Dim userSearcher As New UserSearcher
               
        userSearcher.PageSize = PageSize
        userSearcher.PageNumber = page
        
        If txtId.Text <> "" Then userSearcher.Key = New UserIdentificator(txtId.Text)
        If txtSurname.Text <> "" Then userSearcher.Surname = txtSurname.Text
        If txtName.Text <> "" Then userSearcher.Name = txtName.Text
        If (dwlComm.SelectedItem.Value <> 0) Then
            userSearcher.Community = dwlComm.SelectedItem.Value
        End If

        If (drpUserType.SelectedValue <> 0) Then
            If (drpUserType.SelectedItem.Text = "User") Then
                userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.User
            ElseIf (drpUserType.SelectedItem.Text = "HCP") Then
                userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.HCP
            ElseIf (drpUserType.SelectedItem.Text = "SA") Then
                userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.SA
            End If
        End If
       
        If (dwlStatus.SelectedValue <> 0) Then
            userSearcher.Status = dwlStatus.SelectedItem.Value
        End If
        
        'oSiteValue = GetSiteSelection(Site)
        'If Not oSiteValue Is Nothing Then
        '    userSearcher.RegistrationSite.Id = oSiteValue.Key.Id
        'End If
          
        If (txtId.Text = "") And (txtSurname.Text = "") And (txtName.Text = "") And (dwlComm.SelectedItem.Value = 0) And (userSearcher.UserType = 0) And (userSearcher.Status = 0) Then
            BindGrid(objGrid, Nothing)
        Else
            
            BindGrid(objGrid, userSearcher, page)
        End If
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserSearcher = Nothing, Optional ByVal page As Integer = 1)
        Dim userCollection As UserCollection
  
        'If Searcher Is Nothing Then
        'Searcher = New UserSearcher
        'End If
        
        If Not (Searcher) Is Nothing Then
            Searcher.SortType = SortType
            Searcher.SortOrder = SortOrder
            
            Me.BusinessUserManager.Cache = False
            userCollection = Me.BusinessUserManager.Read(Searcher)
            
            'If Not userCollection Is Nothing Then
            '    userCollection.Sort(SortType, SortOrder)
            'End If
            
            sectionTit.Visible = True
            
            'carica il controllo per la paginazione
            LoadPager(userCollection, page)
            
            objGrid.DataSource = userCollection
            objGrid.DataBind()
        Else
            'ActivePanelGrid()
        End If
    End Sub
         
    'gestiste la paginazione
    Sub LoadPager(ByRef userColl As UserCollection, Optional ByVal Page As Integer = 1)
        'txtActPage2.Value = Page
        'Response.Write(Not userColl Is Nothing)
        'rpPager.DataSource = Nothing
        If Not userColl Is Nothing AndAlso userColl.Count > 0 Then
            Dim pageList As New ArrayList
            Dim i As Integer = 0
            
            Dim ite As New ListItem
            Actualpage.value = Page
            
            ite.Text = "&laquo;"
            ite.Value = "1"
            pageList.Add(ite)
            
            For i = 0 To userColl.PagerTotalNumber - 1
                If (i + 1 > (Page - NPageVis)) AndAlso (i + 1 < (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = i + 1
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("add" & ite.Value)
                ElseIf (i + 1 = (Page - NPageVis)) Or (i + 1 = (Page + NPageVis)) Then
                    ite = New ListItem
                    ite.Text = "..."
                    ite.Value = i + 1
                    pageList.Add(ite)
                    'Response.Write("addelse" & ite.Value)
                End If
            Next
            
            ite = New ListItem
            ite.Text = "&raquo;"
            ite.Value = userColl.PagerTotalNumber
            pageList.Add(ite)
            
            'Response.Write("tot" & pageList.Count)
            If i > 1 Then
                rpPager.DataSource = pageList
                rpPager.DataBind()
                'Response.Write("popolo" & pageList.Count)
                'rptContactPagerBottom.DataSource = pageList
            Else
                rpPager.DataSource = Nothing
                rpPager.DataBind()
                ' Response.Write("popolo Nothing")
                'rptContactPagerBottom.DataSource = Nothing
            End If
        
            'Dim nDa As Integer = ((userColl.PagerTotalNumber - 1) * userColl.PagerTotalCount) + 1
            'Dim nA As Integer = (nDa + userColl.Count) - 1
            'Dim nTot As Integer = userColl.PagerTotalCount
            'lblContactpager.Text = nDa.ToString & " - " & _
            '(nA).ToString & " on " & _
            '(nTot).ToString
        Else
            rpPager.DataSource = Nothing
            rpPager.DataBind()
        End If
             
    End Sub
    
    'restituisce il Tipo Utente
    Sub getUserType(ByVal userValue As UserValue)
        ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(ddlUserType.Items.FindByValue(userValue.UserType))
    End Sub
    
    'restituisce lo Status dell'utente
    Sub getUserStatus(ByVal userValue As UserValue)
        ddluserStatus.SelectedIndex = ddluserStatus.Items.IndexOf(ddluserStatus.Items.FindByValue(userValue.Status))
    End Sub
              
    'recupera tutti i ruoli associati ad un Utente
    Function GetUserRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        profilingManager.Cache = False
        Return profilingManager.ReadRoles(key)
    End Function
       
    'recupera tutti i profili associati ad un Utente
    Function GetUserProfilesValue(ByVal id As Int32) As ProfileCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        profilingManager.Cache = False
        Return profilingManager.ReadProfiles(key)
    End Function
    
    'recupera tutti i gruppi associati ad un Utente
    Function GetUserGroupsValue(ByVal id As Int32) As UserGroupCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        
        userGroupManager.Cache = False
        Return userGroupManager.ReadUserRelated(key)
    End Function
   
    'recupera tutti i context associati ad un Utente
    Function GetUserContextValue(ByVal id As Integer) As ContextCollection
        If id = 0 Then Return Nothing
        Dim key As New UserIdentificator
        key.Id = id
        
        ContextManager.Cache = False
        Return ContextManager.ReadContextUser(key, New GroupIdentificator())
    End Function
    
    'recupera tutti i profili associati ad un Utente
    Function GetUserCommunityValue(ByVal id As Int32) As CommunityCollection
        Dim communitySearcher As New CommunitySearcher
        
        If id = 0 Then Return Nothing
        'Dim key As New UserIdentificator
        'key.Id = id
        communitySearcher.KeyUser.Id = id
        Me.BusinessCommunityManager.Cache = False
        Return Me.BusinessCommunityManager.Read(communitySearcher)
    End Function
       
    'recupera il Mail Template Corretto
    'dati la Lingua, il Nome e il Sito
    Function getMailTemplate(ByVal templateName As String) As MailTemplateValue
        omailTemplateSearcher.Key.KeyLanguage = New LanguageIdentificator(Me.BusinessMasterPageManager.GetLang.Id)
        omailTemplateSearcher.KeySite.Id = Me.BusinessMasterPageManager.GetSite.Id
        omailTemplateSearcher.Key.MailTemplateName = templateName
               
        omailTempalteManager.Cache = False
        Dim coll As MailTemplateCollection = omailTempalteManager.Read(omailTemplateSearcher)
        If Not (coll Is Nothing) AndAlso (coll.Count > 0) Then
            omailTemplateValue = coll(0)
            Return omailTemplateValue
        End If
                      
        Return Nothing
    End Function
    
    'fa il replace di alcuni parametri nel Body del MailTemplate
    Function RepleceBody(ByVal body As String, ByVal userValue As UserValue) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        Dim completeName As String = userValue.Name & " " & userValue.Surname
        If (PrivateData) Then
            body = body.Replace("[PASSWORD]", userValue.Password).Replace("[USERID]", userValue.Username)
        End If
        
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
        Else
            siteLabel = ""
        End If
       
        Return body.Replace("[UTENTE]", completeName).Replace("[SITE]", siteLabel)
    End Function
    
    'fa il replace di alcuni parametri nel Subject del MailTemplate
    Function RepleceSubject(ByVal subject As String) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
        Else
            siteLabel = ""
        End If
        
        Return subject.Replace("[SITE]", siteLabel)
    End Function
    
    'fa il replace di alcuni parametri nel Sender del MailTemplate
    Function RepleceSender(ByVal subject As String) As String
        'siteLabel = Me.BusinessSiteManager.getDomainInfo.Label
        If (Request("S") <> String.Empty) Then
            siteLabel = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(Request("S"))).Label
           
        Else
            siteLabel = ""
        End If
       
        Return subject.Replace("[SITE]", siteLabel)
    End Function
    
     
    'controlla l'evento che si scatena quando si seleziona un Tipo Utente
    Sub SelectChanged(ByVal Sender As Object, ByVal e As EventArgs)
        If ddlUserType.SelectedItem.Text = "HCP" Then
            ActivateHCPElements()
            LoadHCPDett(Nothing)
        ElseIf ddlUserType.SelectedItem.Text = "User" Then
            EnableHCPElements()
        End If
    End Sub
    
    'restituisce la label del Site dato il suo id
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
   
    'restituisce il primo elemento 
    Function getFirstElem(ByRef mutiField As Healthware.HP3.Core.Base.ObjectValues.MultiFieldType) As String
        Return mutiField.Item(0)
    End Function
    
    'restituisce il sito seletionato
    'Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteValue
    '    Dim siteAreaValue As New SiteAreaValue
    '    Dim siteValue As New SiteValue
    '    Dim siteManager As New SiteManager
    '    Dim siteSearcher As New SiteSearcher
    '    Dim oSiteAreaCollection As Object = olistItem.GetSelection
        
    '    If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
    '        siteAreaValue = oSiteAreaCollection.item(0)
    '        siteSearcher.KeySiteArea.Id = siteAreaValue.Key.Id
    '        siteValue = siteManager.Read(siteSearcher)(0)
            
    '        If Not (siteValue Is Nothing) Then
    '            Return siteValue
    '        End If
    '    End If
               
    '    Return Nothing
    'End Function
    
    'carica i servizi della community nella dwl
    Function LoadService(ByVal communityId As Integer, ByVal communityLanguageId As Integer) As Boolean
        
        Dim _return As Boolean = False
        
        Dim communityValue As CommunityValue = Me.BusinessCommunityManager.Read(New CommunityIdentificator(communityId, communityLanguageId))
        
        If Not (communityValue Is Nothing) Then
            serviceColl = Me.BusinessCommunityManager.ReadServices(communityValue.KeyCommunity)
            
            If Not (serviceColl Is Nothing) Then
                If (serviceColl.Count > 0) Then
                    _return = True
                End If
            End If
            
            utility.LoadListControl(dwlService, serviceColl, "Description", "Key")
            dwlService.Items.Insert(0, New ListItem("--Select--", 0))
        End If
        
        Return _return
    End Function
    
    'restiuisce il Geo selezionato
    Function GetGeoSelection(ByVal oLocalization As ctlLocalization) As Integer
        Dim geoColl As New GeoCollection
        Dim geoValue As New GeoValue
        
        geoColl = oLocalization.GeoList()
        If Not (geoColl Is Nothing) AndAlso (geoColl.Count > 0) Then
            geoValue = geoColl(0)
            If Not (geoValue Is Nothing) Then
                Return geoValue.Key.Id
            End If
        End If
        
        Return 0
    End Function
    
    'controlla che i campi non siano nulli
    Function IsUserDettNotNull() As Boolean
        Dim _return As Boolean = False
        
        'If Not (userRequestDate.Value.HasValue) Then strName = "Request Date"
        'If Not (userActionDate.Value.HasValue) Then strName = "Action Date"
        'If Not (userDegreeDate.Value.HasValue) Then strName = "DegreeDate"
        'If Not (userRegistrationDate.Value.HasValue) Then strName = "Registration Date"
       
        'txtSite
        If (txtHideSite.Text = String.Empty) Then strName = "Site"
        'If Not (ctlProfiles.GetSelection.Count > 0) Then strName = "Profile"
        'If (userAddress.Text = String.Empty) Then strName = "Address"
        'If (userCap.Text = String.Empty) Then strName = "Cap"
        'If (userCity.Text = String.Empty) Then strName = "City"
        'If (userProvince.Text = String.Empty) Then strName = "Province"
        'If (userTel.Text = String.Empty) Then strName = "Tel"
        If (userEmail.Text = String.Empty) Then strName = "Email"
        If (userName.Text = String.Empty) Then strName = "Name"
        If (userSurname.Text = String.Empty) Then strName = "Surname"
        
        'If (userUserName.Text = String.Empty) Then strName = "UserName"
        'If (userFax.Text = String.Empty) Then strName = "Fax"
        'If (userCell.Text = String.Empty) Then strName = "Cell"
        'If (userCF.Text = String.Empty) Then strName = "CF"
        'If Not (userBirthday.Value.HasValue) Then strName = "Birthday"
        
        'If ((userName.Text <> String.Empty) And (userSurname.Text <> String.Empty) And (userCap.Text <> String.Empty) And (userCity.Text <> String.Empty) And (userTel.Text <> String.Empty) And (userEmail.Text <> String.Empty) And (userProvince.Text <> String.Empty) And (ctlProfiles.GetSelection.Count > 0) And (txtHideSite.Text <> String.Empty)) Then
        '    _return = True
        'End If
        If ((userName.Text <> String.Empty) And (userSurname.Text <> String.Empty) And (userEmail.Text <> String.Empty) And (txtHideSite.Text <> String.Empty)) Then
            _return = True
        End If
        
        Return _return
    End Function
    
    'controlla che i campi non siano nulli
    Function IsHCPDettNotNull() As Boolean
        Dim _return As Boolean = False
        
        'If (hcpRegistration.Text = String.Empty) Then strName = "HCP Registration"
        'If (hcpWard.Text = String.Empty) Then strName = "HCP Ward"
        'If (hcpStructure.Text = String.Empty) Then strName = "HCP Structure"
        If (hcpEmail.Text = String.Empty) Then strName = "HCP Email"
        'If (hcpFax.Text = String.Empty) Then strName = "HCP Fax"
        'If (hcpProvince.Text = String.Empty) Then strName = "HCP Province"
        'If (hcpCell.Text = String.Empty) Then strName = "HCP Cell"
        'If (hcpTel.Text = String.Empty) Then strName = "HCP Tel"
        'If (hcpCity.Text = String.Empty) Then strName = "HCP City"
        'If (hcpCap.Text = String.Empty) Then strName = "HCP Cap"
        'If (hcpAddress.Text = String.Empty) Then strName = "HCP Address"
        
          
        'If ((hcpAddress.Text <> String.Empty) And (hcpCap.Text <> String.Empty) And (hcpCity.Text <> String.Empty) And (hcpTel.Text <> String.Empty) And (hcpProvince.Text <> String.Empty)) And (hcpEmail.Text <> String.Empty) And (hcpStructure.Text <> String.Empty) And (hcpWard.Text <> String.Empty) And (hcpRegistration.Text <> String.Empty) Then
        '    _return = True
        'End If
        
        If ((hcpEmail.Text <> String.Empty)) Then
            _return = True
        End If
        
        Return _return
    End Function
    
    ''controlla che tutte le mail inserite siano in un formato corretto
    Function IsEmails(ByVal emails As String) As Boolean
        Dim EmailsArray() As String = emails.Split("|")
        Dim elem As String
        Dim _result As Boolean
        
        For Each elem In EmailsArray
            _result = IsEMail(elem)
            If Not (_result) Then
                CreateJsMessage("Email is not in a correct format ")
                Return False
            End If
        Next
        
        Return True
    End Function
    
    'controlla che la email sia nel corretto formato
    Function IsEMail(ByVal email As String) As Boolean
        Dim emailFormat As String = "([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})"
        Return (Regex.IsMatch(email, emailFormat))
    End Function
    
    'True, se � stato selezionato un Sito
    'False, Altrimenti
    Function IsSiteSelected() As Boolean
        Dim objQs As New ObjQueryString
        
        If (objQs.Site_Id <> 0) Then 'se � stato selezionato un sito 
            Return True
        Else
            Return False
        End If
        
    End Function
    
    'restituisce la giusta icona 
    'per il recovery password
    Function getIcon() As String
        Dim str_out As String
        
        If (IsSiteSelected()) Then 'se � stato selezionato un sito 
            str_out = "iMail_on"
        Else
            str_out = "iMail_off"
        End If
        
        Return str_out
    End Function
       
    'restituisce lo status dell'utente
    Function getStatus(ByVal userValue As UserValue) As String
        Dim out_str As String = ""
        
        Select Case userValue.Status
            Case 1
                out_str = "Active"
            Case 2
                out_str = "Pending"
            Case 3
                out_str = "No Active"
            Case 4
                out_str = "Disabled"
            Case 5
                out_str = "Deleted"
        End Select
        
        Return out_str
    End Function
     
    'popola la dwl delle speciality 
    Sub BindSpeciality()
        Dim contextSearcher As New ContextSearcher
       
        contextSearcher.KeyContextGroup.Id = Integer.Parse(ConfigurationManager.AppSettings("GroupContextUT"))
        'contextSearcher.KeyFather.Id = 0
        
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        'dwlHcpSpecialty.Items.Clear()
        utility.LoadListControl(dwlHcpSpecialty, contextColl, "Description", "Key.Id")
        dwlHcpSpecialty.Items.Insert(0, New ListItem("Select Specialty", -1))
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        panelCommunity.Visible = False
        ' panelSegmentation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        divExtraFields.Visible = False
        pnlDett.Visible = True
        pnlGrid.Visible = False
        panelCommunity.Visible = False
        'panelSegmentation.Visible = False
        divCommunity.Visible = False
    End Sub
    
    Sub ActivePanelCommunity()
        panelCommunity.Visible = True
        'panelSegmentation.Visible = False
        pnlGrid.Visible = False
    End Sub
    
    Sub ActivePanelSegmentation()
        'panelSegmentation.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    Sub ActivateHCPElements()
        divHCP.Visible = True
    End Sub
    
    'Disattiva gli elementi per un utente HCP
    Sub EnableHCPElements()
        divHCP.Visible = False
    End Sub
           
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
     
    ''' <summary>
    ''' Ordinemento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListUsers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        'gdw_usersList.PageIndex = 0
        'impostare il PageNumber 
        'Dim Page As Integer = 1
        
        Select Case e.SortExpression
            Case "Key.Id"
                If SortType <> UserGenericComparer.SortType.ByKey Then
                    SortType = UserGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Surname"
                If SortType <> UserGenericComparer.SortType.BySurName Then
                    SortType = UserGenericComparer.SortType.BySurName
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
                
        BindWithSearch(gdw_usersList, CInt(Actualpage.Value))
    End Sub
        
    'Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    sender.PageIndex = e.NewPageIndex
    '    BindWithSearch(sender, e.NewPageIndex)
    'End Sub
       
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Sub linkPager_PageIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        'evento scatenato dal paginatore degli utenti filtrati
        BindWithSearch(gdw_usersList, sender.CommandArgument)
        ' LoadUser(CInt(sender.CommandArgument), ctxFilter.Value, txtSortOrder.Value, txtOrderType.Value)
    End Sub
    
    
    Sub ItemCommand(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim param = s.commandArgument.split(",")
        Dim commandType As String = param(0)
        Dim contentID As String = param(1)
        ' Dim contentLangId As String = param(2)

        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
      
        wr.KeycontentType = New ContentTypeIdentificator("HP3", commandType)
        '        objQs.Language_id = contentLangId
        'objQs = Int32.Parse(contentID)
        wr.customParam.append(objQs)
        wr.customParam.append("us", contentID.ToString())
        Response.Redirect(mPage.FormatRequest(wr))
    End Sub
    
    
    Function tooltipinformation(ByVal userid As Integer) As String
        
        Dim so As New UserSearcher
        Dim userStatusSearch As New StatusSiteSearcher
        userStatusSearch.User.Id = userid
        userStatusSearch.StatusId = 1
        Me.BusinessUserManager.Cache = False
        Dim coll As StatusSiteCollection = Me.BusinessUserManager.ReadStatus(userStatusSearch)
        Dim toolMess As String
        toolMess = "Il seguente utente ha zero associazioni attive"
        If (Not coll Is Nothing AndAlso coll.Any AndAlso coll(0).KeySiteArea.Id > 0) Then
            
            toolMess = "Il seguente utente ha" & " " & coll.Count & " " & "associazioni attive"
            Return toolMess
        Else
            
            Return toolMess
        End If
        
            
        
        
    End Function
</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
 <asp:HiddenField  ID="Actualpage"  runat="server" Value="1"/>
<asp:Panel id="pnlGrid" runat="server" visible="false">
 <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style=" margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <%--FILTRI SUGLI UTENTI--%>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Surname</strong></td>
                    <td><strong>Name</strong></td>
                    <td><strong>User Type</strong></td>
                    <td><strong>Status</strong></td> 
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat ="server" id="txtSurname" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                    <td><HP3:Text runat ="server" id="txtName" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                    <td>    
                        <asp:dropdownlist ID="drpUserType" runat="server">
                          <asp:ListItem Selected= "True" Text="Select Type" Value="0"></asp:ListItem>
                          <asp:ListItem  Text="User" Value="1"></asp:ListItem>
                          <asp:ListItem  Text="HCP" Value="2"></asp:ListItem>
                          <asp:ListItem  Text="SA" Value="3"></asp:ListItem>
                        </asp:dropdownlist>
                    </td>
                     <td>
                         <asp:dropdownlist id="dwlStatus" runat="server">
                             <asp:ListItem  Text="--Select Status--" Value="0"></asp:ListItem>
                            <asp:ListItem  Text="Active" Value="1"></asp:ListItem>
                            <asp:ListItem  Text="Pending" Value="2"></asp:ListItem>
                            <asp:ListItem  Text="No active" Value="3"></asp:ListItem>
                            <asp:ListItem  Text="Disabled" Value="4"></asp:ListItem>
                            <asp:ListItem  Text="Deleted" Value="5"></asp:ListItem>
                         </asp:dropdownlist>
                    </td> 
                </tr>
                <tr>
                    <td><strong>Community</strong></td> 
                    <td><asp:dropdownlist id="dwlComm" runat="server"/></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchUser" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
<%--FINE--%>    


 <%--<div style="margin-bottom:10px">
    <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>--%>
 
 <asp:Label CssClass="title" runat="server"  visible="false" id="sectionTit">Users List</asp:Label>
 <div style ="float:right; text-align:right;">
     <asp:Repeater ID="rpPager" runat="server" Visible="true" >
        <ItemTemplate>
          
        <%#IIf(Actualpage.Value = DataBinder.Eval(Container.DataItem, "value"), " <span class='bold'>" & DataBinder.Eval(Container.DataItem, "text") & "</span>", "")%>
                        <asp:LinkButton ID="linkPager" runat="server" OnClick="linkPager_PageIndex" 
                        Visible='<%#iif (Actualpage.value=DataBinder.Eval(Container.DataItem, "value"),"false","true") %>' 
                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "value")%>' ><%#DataBinder.Eval(Container.DataItem, "text")%></asp:LinkButton>
        </ItemTemplate>
     </asp:Repeater>
</div>
 
 <asp:label  id="lblContactpager" runat="server"></asp:label>
 
 <asp:gridview id="gdw_usersList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                OnSorting="gridListUsers_Sorting"
                emptydatatext="No item available">
                                
              <Columns >
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Key.Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%> </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField HeaderStyle-Width="40%" DataField ="Name" HeaderText ="Name" />
                <asp:BoundField HeaderStyle-Width="30%" DataField ="Surname" HeaderText ="Surname" SortExpression="Surname" />
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Status">
                       <ItemTemplate><%#getStatus(Container.DataItem)%> </ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="15%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <%--<asp:ImageButton ID="lnkSegmentation" runat="server" ToolTip ="Segmentation" ImageUrl="~/hp3Office/HP3Image/Ico/content_context.gif"  OnClick="Segmentation" CommandName ="Segmentation" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>--%>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkActive" runat="server" ToolTip ="Active User" ImageUrl="~/hp3Office/HP3Image/ico/content_active.gif" OnClick="EditRow" CommandName ="ActiveUser" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageUserUpload.aspx?idUser=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','CoverImage','width=550,height=350')" style="cursor:pointer" title="Image Cover" alt="" />
                        <asp:ImageButton ID="lnkSendMail" runat="server" ToolTip ="Recovery Password" ImageUrl='<%#"~/hp3Office/HP3Image/ico/" & getIcon() & ".gif"%>'  OnClick="RecoveryPassword" CommandName ="RecoveryPassword" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkCommunity" runat="server" ToolTip ="Community" ImageUrl="~/hp3Office/HP3Image/ico/Forum.gif"   OnClick = "LoadCommunityControl" CommandName ="Community" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="Siteassociation" runat="server" ToolTip = <%#tooltipinformation(Container.Dataitem.Key.Id) %> ImageUrl="~/hp3Office/HP3Image/ico/content_content.gif"    onClick="ItemCommand"  CommandArgument ='<%#"UserSiteSt," & Container.Dataitem.Key.Id.toString() %>'/>
                   
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server">
   
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button runat="server" ID="btnSave" onclick="UpdateRow" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <div>
        <asp:Button id ="btnSendMail" runat="server" Text="Recovery Password" OnClick="RecoveryPassword"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
        <asp:Button id ="btnRegistration_1" runat="server" Text="Confirm Registration" OnClick="ConfirmRegistration"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
        <asp:Button id ="btnRegistration_2" runat="server" Text="Reject Registration" OnClick="RejectRegistration"  CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/iMail.gif');background-repeat:no-repeat;background-position:1px 1px;" Visible="false"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%" >
        <tr>
            <td></td>
            <td><HP3:Text runat ="server" id="userId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
            <td><HP3:Text runat ="server" id="communityId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
            <td><HP3:Text runat ="server" id="communityLanguageId" TypeControl ="TextBox" style="width:200px;display:none"  isReadOnly ="true" MaxLength="50"/></td>
        </tr>
        <tr>
            <td width="220px"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpUserTName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text runat ="server" id="userName" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserSurname&Help=cms_HelpUserSurname&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserSurname", "Surname")%></td>
            <td><HP3:Text runat ="server" id="userSurname" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserGender&Help=cms_HelpUserGender&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserGender", "Gender")%></td>
            <td>
                <asp:DropDownList id="userGenter" runat="server"  Width="50px">
                    <asp:ListItem  Selected= "True" Text="M" Value="1"></asp:ListItem>
                    <asp:ListItem Text="F" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpUserTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
            <td><HP3:Text runat ="server" id="userTitle" TypeControl ="TextBox" style="width:100px" MaxLength="15"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserMaritalStatus&Help=cms_HelpUserMaritalStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserMaritalStatus", "Marital Status")%></td>
            <td>
             <asp:DropDownList  id="dwlMaritalStatus" cssClass="select" runat="server"  >
                 <asp:ListItem  Selected="True" Text="Select status" Value="0"></asp:ListItem>
                 <asp:ListItem  Text="Single" Value="2"></asp:ListItem>
                 <asp:ListItem  Text="Married" Value="1"></asp:ListItem>
                 <asp:ListItem  Text="Divorced" Value="4"></asp:ListItem>
                 <asp:ListItem  Text="Widowed" Value="5"></asp:ListItem>
                 <asp:ListItem  Text="Separated" Value="3"></asp:ListItem>
                 <asp:ListItem  Text="Living with partner" Value="11"></asp:ListItem>
            </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserBirthday&Help=cms_HelpUserBirthday&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserBirthday", "Birthday")%></td>
            <td><HP3:Date runat="server" id="userBirthday"  TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCF&Help=cms_HelpUserCF&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCF", "CF")%></td>
            <td><HP3:Text runat ="server" id="userCF" TypeControl ="TextBox" style="width:200px" MaxLength="20"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserAddress&Help=cms_HelpUserAddress&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserAddress", "Address")%>*</td>
            <td><HP3:Text runat ="server" id="userAddress" TypeControl ="TextBox" style="width:300px" MaxLength="400"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCap&Help=cms_HelpUserCap&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCap", "Cap")%></td>
            <td><HP3:Text runat ="server" id="userCap" TypeControl ="TextBox" style="width:60px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCity&Help=cms_HelpUserCity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCity", "City")%></td>
            <td><HP3:Text runat ="server" id="userCity" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProvince&Help=cms_FormUserProvince&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProvince", "Province")%></td>
            <td><HP3:Text runat ="server" id="userProvince" TypeControl ="TextBox" style="width:30px" MaxLength="2"/></td>
        </tr> 
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLocalization&Help=cms_HelpLocalization&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLocalization", "Localization")%></td>
            <td><%--<HP3:Localization id="oLocalization" runat="server" TypeControl="AccContentGeo" />--%>
            <table width="100%">    
                    <tr>
                        <td style="width:300px">
                            <asp:textbox ID="_txtFatherHidden" runat="Server" style="display:none"/>
                            <HP3:Text runat="server" ID="_Father" TypeControl="TextBox" style="width:300px" />
                        </td>
                        <td style="width:25px">
                            <input type="button"  class ="button" id="_btnSelect" value="Select" onclick="ShowMyPanel()" /> 
                        </td>
                    </tr>
                </table>
                
                <div id="divGEO" style="display:none">
                    <table>
                        <tr>
                            <td><asp:panel ID="pnlGeoTree" runat="server" style="overflow:auto;height:300px;border:1px solid #000;width:300px"/></td>
                            <td><asp:button ID="_btnAddSelect" runat="server" CssClass="button" text="ADD" OnClick="AddItemGeoText" style="display:none"/></td>
                        </tr>   
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserTel&Help=cms_HelpUserTel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserTel", "Tel")%>*</td>
            <td><HP3:Text runat ="server" id="userTel" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCell&Help=cms_HelpUserCell&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCell", "Cell")%>*</td>
            <td><HP3:Text runat ="server" id="userCell" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserFax&Help=cms_HelpUserFax&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserFax", "Fax")%>*</td>
            <td><HP3:Text runat ="server" id="userFax" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEmail&Help=cms_HelpUserEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEmail", "Email")%>*</td>
            <td><HP3:Text runat ="server" id="userEmail" TypeControl ="TextBox" style="width:300px" MaxLength="200"/></td>
        </tr>
        <tr>
             <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserType&Help=cms_HelpUserType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserType", "User Type")%></td>
             <td>
                 <asp:DropDownList id="ddlUserType" DataTextField="Name" DataValueField="Key.Id" runat="server" autopostback="true" OnSelectedIndexChanged="SelectChanged">
                      <asp:ListItem Selected= "True" Text="User" Value="1"></asp:ListItem>
                      <asp:ListItem  Text="HCP" Value="2"></asp:ListItem>
                 </asp:DropDownList>
             </td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDegreeDate&Help=cms_HelpDegreeDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDegreeDate", "Degree Date")%></td>
            <td><HP3:Date runat="server" id="userDegreeDate"  TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRegistrationDate&Help=cms_HelpRegistrationDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRegistrationDate", "Registration Date")%></td>
            <td><HP3:Date runat="server" id="userRegistrationDate"  TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserName&Help=cms_HelpUserUName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserName", "UserName")%></td>
            <td><HP3:Text runat ="server" id="userUserName"  TypeControl ="TextBox" style="width:150px"  isReadOnly ="true" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPassword&Help=cms_HelpUserPassword&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPassword", "Password")%></td>
            <td><HP3:Text runat ="server" id="userPassword" TypeControl ="TextBox" style="width:150px" MaxLength="50"/></td>
        </tr>

        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRegistrationSite&Help=cms_HelpRegistrationSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRegistrationSite", "Registration Site")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td width="50px">
                            <asp:TextBox id="txtHideSite" runat="server" style="display:none"/>
                            <HP3:Text runat ="server" id="txtSite"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                        </td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSite.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSite.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Site%>&ListId=<%=txtSite.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr> 
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListProfiles&Help=cms_HelpListProfiles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListProfiles", "List Profiles")%></td>
            <td><HP3:ctlProfiles runat ="server" id="ctlProfiles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" id="ctlRoles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
           <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListGroups&Help=cms_HelpListGroups&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListGroups", "List Groups")%></td>
          <td><HP3:ctlUserGroups runat ="server" id="ctluserGroups" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListContext&Help=cms_HelpListContext&Language=<%=Language()%>','Help','width=800,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListContext", "List Context")%></td>
            <td><HP3:ctlContext runat ="server" id="ctlContext" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormReceiveNewsletter&Help=cms_HelpReceiveNewsletter&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormReceiveNewsletter", "Receive Newsletter")%></td>
            <td><asp:checkbox id="flagNewsletter" runat="server" /></td>
        </tr>
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteAcquaintedThrougt&Help=cms_HelpSiteAcquaintedThrougt&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormSiteAcquaintedThrougt", "Source Site Domain")%></td>
            <td><HP3:Text runat ="server" id="siteSourceDesc" TypeControl ="TextBox" style="width:300px" MaxLength="100"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSourceSiteId&Help=cms_HelpSourceSiteId&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormSourceSiteId", "Source Site Id")%></td>
            <td><HP3:Text runat ="server" id="siteSourceId" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteAcquaintedThrougtId&Help=cms_HelpSiteAcquaintedThrougtId&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormSiteSourceStatus", "Source Site Status")%></td>
            <td><HP3:Text runat ="server" id="siteSourceStatus" TypeControl ="TextBox"  style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActivationDate&Help=cms_HelpActivationDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormActivationDate", "Activation Date")%></td>
            <td><HP3:Date runat="server" id="siteActivationDate"  TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserStatus&Help=cms_HelpUserStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormUserStatus", "Actual Status")%></td>
            <td>
                <asp:DropDownList id="ddluserStatus" runat="server">
                          <asp:ListItem   Selected ="True" Text="Active" Value="1"></asp:ListItem>
                          <asp:ListItem  Text="Pending" Value="2"></asp:ListItem>
                          <asp:ListItem  Text="No active" Value="3"></asp:ListItem>
                          <asp:ListItem  Text="Disabled" Value="4"></asp:ListItem>
                          <asp:ListItem  Text="Deleted" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStatusRequestDate&Help=cms_HelpStatusRequestDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStatusRequestDate", "Status Request Date")%></td>
            <td><HP3:Date runat="server" id="userRequestDate"  TypeControl="JsCalendar"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStatusActionDate&Help=cms_HelpStatusActionDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStatusActionDate", "Status Action Date")%></td>
            <td><HP3:Date runat="server" id="userActionDate"  TypeControl="JsCalendar"/></td>
            <td><asp:Literal id="lbMsg"  runat ="server" Text="[The field marked (*) are a (MultiType Field) multiple values. It's possible to insert more value separated by '|'. Example: (34344343434|9889789787|90873473473)]"></asp:Literal></td>
        </tr>
        <%--<tr>
            <td>Community</td>
            <td><HP3:ctlCommunity runat ="server" id="ctlCommunity" typecontrol="GenericRelation" /></td>
        </tr> --%>
     </table>
 
    <div id="divExtraFields" runat="server" visible="false" >
        <asp:Repeater id="rp_extrafield" OnItemDataBound="LoadExtraFields"  runat="server">
	           <HeaderTemplate><table class="form" style="width:75%" ></HeaderTemplate>
	           <FooterTemplate></table></FooterTemplate>
               <ItemTemplate>                
                   <tr>
                        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraField&Help=cms_HelpExtraField&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%#DataBinder.Eval(Container.DataItem, "Name") %></td>
                        <td>
                            <asp:Literal id="extFId" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' runat="server" visible="false" />
                            <HP3:ctlExtraField id="extrF"  MaxLength='<%#DataBinder.Eval(Container.DataItem, "Length") %>' TypeControl='<%#DataBinder.Eval(Container.DataItem, "Type") %>' runat="server"/>
                        </td>
                  </tr>
               </ItemTemplate>
          </asp:Repeater>
    </div>
    
    <div id="divHCP" runat="server" visible="false">
        <table class="form" style="width:90%" >
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPAddress&Help=cms_HelpHCPAddress&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPAddress", "HCP Address")%>*</td>
            <td><HP3:Text runat ="server" id="hcpAddress" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr>
        <tr>
           <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCap&Help=cms_HelpUserCap&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCap", "Cap")%>*</td>
           <td><HP3:Text runat ="server" id="hcpCap" TypeControl ="TextBox" style="width:60px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCity&Help=cms_HelpUserCity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCity", "City")%></td>
            <td><HP3:Text runat ="server" id="hcpCity" TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserProvince&Help=cms_HelpUserProvince&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserProvince", "Province")%></td>
            <td><HP3:Text runat ="server" id="hcpProvince" TypeControl ="TextBox" style="width:50px" MaxLength="2" /></td>
        </tr> 
       <%-- <tr>
            <td>Geo Id:</td>
            <td><HP3:Text runat ="server" id="hcpGeoId" TypeControl ="TextBox" style="width:30px" MaxLength="10"/></td>
        </tr>--%>
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLocalization&Help=cms_HelpLocalization&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLocalization", "Localization")%></td>
            <td><%--<HP3:Localization id="hcpLocalization" runat="server" TypeControl="AccContentGeo" />--%>
              <table width="100%">    
                    <tr>
                        <td style="width:300px">
                            <asp:textbox ID="txtHCPFatherHidden" runat="Server" style="display:none"/>
                            <HP3:Text runat="server" ID="HCPFather" TypeControl="TextBox" style="width:300px" />
                        </td>
                        <td>
                            <input type="button"  class ="button" id="btSelect" value="Select" onclick="ShowHCPPanel()" /> 
                        </td>
                       
                    </tr>
                </table>
                
                <div id="divHCPTree" style="display:none">
                    <table>
                        <tr>
                            <td><asp:panel ID="pnlGeoHCPTree" runat="server" style="overflow:auto;height:300px;border:1px solid #000;width:300px"/></td>
                            <td><asp:button ID="btAddSelect" runat="server" CssClass="button" text="ADD" OnClick="AddItemHCPFatherText" style="display:none"/></td>
                        </tr>   
                    </table>
                </div>
             </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserTel&Help=cms_HelpUserTel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserTel", "Tel")%>*</td>
            <td><HP3:Text runat ="server" id="hcpTel" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserCell&Help=cms_HelpUserCell&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserCell", "Cell")%>*</td>
            <td><HP3:Text runat ="server" id="hcpCell" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserFax&Help=cms_HelpUserFax&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserFax", "Fax")%>*</td>
            <td><HP3:Text runat ="server" id="hcpFax" TypeControl ="TextBox" style="width:200px" MaxLength="100"/></td>
        </tr> 
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEmail&Help=cms_HelpUserEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEmail", "Email")%>*</td>
            <td><HP3:Text runat ="server" id="hcpEmail" TypeControl ="TextBox" style="width:300px" MaxLength="200"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPSpeciality&Help=cms_HelpHCPSpeciality&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPSpeciality", "Speciality")%></td>
            <td><asp:DropDownList id="dwlHcpSpecialty" runat="server" /></td>
        </tr>
         
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPStructure&Help=cms_HelpHCPStructure&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPStructure", "HCP Structure")%></td>
            <td><HP3:Text runat ="server" id="hcpStructure" TypeControl ="TextBox" style="width:300px" MaxLength="200"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPWard&Help=cms_HelpHCPWard&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPWard", "HCP Ward")%></td>
            <td><HP3:Text runat ="server" id="hcpWard" TypeControl ="TextBox" style="width:300px" MaxLength="200"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPRegistration&Help=cms_HelpHCPRegistration&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPRegistration", "HCP Registration")%></td>
            <td><HP3:Text runat ="server" id="hcpRegistration" TypeControl ="TextBox" style="width:200px" MaxLength="200"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOtherInfo&Help=cms_HelpOtherInfo&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOtherInfo", "Other Info")%></td>
            <td><HP3:Text runat ="server" id="hcpOtherInfo" TypeControl ="TextArea"  Style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMoreInfo&Help=cms_HelpMoreInfo&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMoreInfo", "Other Info")%></td>
            <td><HP3:Text runat ="server" id="hcpMoreInfo" TypeControl ="TextArea"  Style="width:300px"/></td>
        </tr>
       </table>   
    </div>   
    </span></span>   
</asp:Panel>

<asp:Panel id="panelCommunity" runat="server" Visible="false">
    <asp:button ID="btLoad" runat="server" Text="Archive" onclick="LoadArchive" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px; font-size:12px;" />
    <asp:button runat="server" ID="btSave" onclick="SaveUserPermission" Text="Save" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px; font-size:12px;"/><br /> <br />
    <hr />
    <table class="form">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunity&Help=cms_HelpCommunity&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunity", "Community")%></td>
            <td><HP3:ctlCommunity runat ="server" id="ctlCommunity" typecontrol="GenericRelation" /></td>
        </tr> 
        <tr>
            <td><asp:button ID="btSaveRelation" runat="server" Text="Save Relations" onclick="SaveCommunityRelation" CssClass="button" /></td> 
        </tr>
    </table>
</asp:Panel> 
    
 <asp:Panel id="divCommunity" runat="server" visible="false">
    <table class="form" >
        <tr>
            <td style="width:145px"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserPermissions&Help=cms_HelpUserPermissions&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUserPermissions", "User Permissions on Service")%></td>
            <td><asp:ListBox id="lbServicePermission"  width="200px" runat="server"></asp:ListBox> </td>
            <td><asp:button ID="btDelete" CssClass="button" runat="server" Text="Delete" Style ="width:100px"  OnClick="DeleteServicePermission" /></td>
        </tr>
        <tr>
             <td>&nbsp;</td>
             <td><asp:dropdownlist id="dwlService" runat="server"></asp:dropdownlist></td>
             <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPermission&Help=cms_HelpPermission&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPermission", "Permission")%></td>
             <td>       
                <asp:dropdownlist id="dwlPermission" runat="server">
                   <asp:ListItem Selected="True" Text="Read/Write" Value="2" />
                   <asp:ListItem Text="Read" Value="1" />
                </asp:dropdownlist></td>
              <td> <asp:button ID="btPermission" CssClass="button" runat="server" Text="Add" onclick="AddPermission" /></td> 
        </tr>
    </table>
</asp:Panel>

<script type="text/javascript">
    function ShowMyPanel()
        {
            var pnl1 = document.getElementById ('divGEO')
            var btn1 = document.getElementById ('<%=_btnAddSelect.clientid%>')
         
            if ((pnl1 != null) && (btn1 != null))
                if (pnl1.style.display == 'none')
                    {
                        pnl1.style.display='';
                        btn1.style.display='';
                    }
                else
                    {
                        pnl1.style.display='none';
                        btn1.style.display='none';
                    }
        }
        
    function ShowHCPPanel()
        {
            
            var pnl = document.getElementById ('divHCPTree')
            var btn = document.getElementById ('<%=btAddSelect.clientid%>')
          
            if ((pnl != null) && (btn != null))
            {
               if (pnl.style.display == 'none')
                    {
                        pnl.style.display='';
                        btn.style.display='';
                    }
                else
                    {
                        pnl.style.display='none';
                        btn.style.display='none';
                    }
             }else{
             }
        }
        
        //Gestisce la selezione degli Item nel Tree
    function SelItem(objDiv,id,Desc,type)
        {   
        var obj;
        if (type == 'USER'){
            obj = document.getElementById ('<%=_txtFatherHidden.clientid%>');
        }else{
            obj = document.getElementById ('<%=txtHCPFatherHidden.clientid%>'); 
        }           
                      
           obj.value = id
         
           var ancor 
           if (SelDiv != null)
            {                
              ancor = SelDiv.getElementsByTagName('a')  
              SelDiv.style.backgroundColor=bgcolor;
              ancor[0].style.color  = fcolor;          
              ancor[1].style.color  = fcolor;  
            }
           
           ancor = objDiv.getElementsByTagName('a')  
           objDiv.style.backgroundColor=MouseClickColor;
         
            ancor[0].style.color  = MouseClickTextColor;          
            ancor[1].style.color  = MouseClickTextColor;  
           
           SelDiv = objDiv;
        }
</script>