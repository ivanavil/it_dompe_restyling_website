<%@ Control Language="VB" ClassName="ThemesManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="System.Linq"%>
<%@ Import Namespace="System.Data.Linq"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="ServiceOptionExtra" Src ="~/hp3Office/HP3Parts/ctlServiceOptionExtra.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"  %>

<script runat="server">
    Private oThemesManager As ThemeManager
    Private oThemesSearcher As ThemeSearcher
    Private oThemesCollection As ThemeCollection
    
    Private oGenericUtlity As New GenericUtility
    Private utility As New WebUtility
    
    Private _sortType As ThemeGenericComparer.SortType
    Private _sortOrder As ThemeGenericComparer.SortOrder   
    
    Private contentTypeSearcher As New ContentTypeSearcher
    Private contentTypeManager As New ContentTypeManager
    
    Private optManager As ContentOptionService
    Private optSearcher As ContentOptionSearcher
    Private optCollection As ContentOptionCollection
    
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private _language As String
    
    Private strDomain As String
    Private strJs As String
    Private COLL As New Collection
    
    Private ht As New Hashtable()
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    Private oGenericUtility As New GenericUtility
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdThemes() As Int32
        Get
            Return ViewState("SelectedIdThemes")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdThemes") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property Domain() As String
        Get
            Return ViewState("domain")
        End Get
        Set(ByVal value As String)
            ViewState("domain") = value
        End Set
    End Property
    
        
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    ''' <summary>
    ''' Carica gli oggetti necessari alla ricerca del thema
    ''' </summary>
    ''' <remarks></remarks>
    Sub BindFilerObject()
        Site.LoadControl()
        Area.LoadControl()
        LoadFatherControl()
        
    End Sub
 
    Sub LoadFatherControl()
        Dim oThemeManager As New ThemeManager
        Dim oThemeSearcher As New ThemeSearcher
        Dim oThemeCollection As New ThemeCollection
        Dim oSiteAreaValue As SiteAreaValue
        Dim oWebUtil As New WebUtility
        oThemeSearcher.OnlyFather = True

        oSiteAreaValue = GetSiteSelection(Site)
        If Not oSiteAreaValue Is Nothing Then
            oThemeSearcher.KeySite.Id = oSiteAreaValue.Key.Id
        End If

        oThemeCollection = oThemeManager.Read(oThemeSearcher)
        
        'Combo dei themi per la selezione del padre
        drpThemeFather.Items.Clear()
        oWebUtil.LoadListControl(drpThemeFather, oThemeCollection, "Description", "Key.Id")
        drpThemeFather.Items.Insert(0, New ListItem("---Select Father---", 0))
    End Sub
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il themeValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadThemesValue() As ThemeValue
        If SelectedIdThemes <> 0 Then
            Return oGenericUtlity.GetThemeValue(SelectedIdThemes)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oThemeValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oThemeValue As ThemeValue)
        Dim oThemeOptionColl As New ThemeOptionExtraCollection
        
        If Not oThemeValue Is Nothing Then
            With oThemeValue
                ThemesDescrizione.InnerText = " - " & .Description
                ThemesId.InnerText = "Theme ID: " & .Key.Id
  
                Description.Text = .Description
                Theme_Domain.Text = .Key.Domain
                Theme_Name.Text = .Key.Name
                SeoName.Text = .SeoName
                'Launch.Value = .Launch
                Launch.Content = .Launch
                MetaKeywords.Text = .MetaKeywords
                MetaDescription.Text = .MetaDescription
                Order.Text = .Order
                Image.Text = .Image
                txtIdFather.Text = .KeyFather.Id
                Father.Text = ReadFatherTheme(.KeyFather.Id)
                txtHideSiteArea.Text = .KeySiteArea.Id
                txtSiteArea.Text = ReadSiteValue(.KeySiteArea.Id)
                txtHideSite.Text = .KeySite.Id
                txtSite.Text = ReadSiteValue(.KeySite.Id)
                txtHiddenContentType.Text = .KeyContentType.Id
                txtContentType.Text = ReadContentTypeValue(.KeyContentType.Id)
                Note.Text = .Note
                active.Checked = .Active
                ckHttps.Checked = .IsHttps
                display.Checked = .Display
                txtContent.Text = .KeyContent.Id
                txtHiddenContent.Text = .KeyContent.Id
                txtHiddenEvent.Text = .KeyTraceEvent.Id
                txtEvent.Text = GetDescriptionEvent(.KeyTraceEvent.Id)
                
                txtHiddenMasterPage.Text = .KeyMasterPage.Id
                txtMasterPage.Text = GetDescriptionMasterPage(.KeyMasterPage.Id)
                
                ElencoContent.InnerText = ReadContentDescription(.KeyContent.Id)
                
                ctlRoles.GenericCollection = oGenericUtlity.ReadRoles(.Key.Id, GenericUtility.TypeRolesConnection.WithThemes)
                ctlRoles.LoadControl()
                
                dwlWorkFlows.SelectedIndex = dwlWorkFlows.Items.IndexOf(dwlWorkFlows.Items.FindByValue(.KeyWorkFlow.Id))
                
                oThemeOptionColl = getOptionExtra(oThemeValue.Key)
                If Not (oThemeOptionColl Is Nothing) Then
                    lbOption.Items.Clear()
                    For Each ThemeOption As ThemeOptionExtraValue In oThemeOptionColl
                        Select Case ThemeOption.Visible
                            Case ThemeOptionExtraValue.VisibleType.Always
                                lbOption.Items.Add(New ListItem(ThemeOption.OptionLabel & " --> " & "Always", ThemeOption.KeyOptionExtra.Id & "|" & ThemeOption.Visible))
                            
                            Case ThemeOptionExtraValue.VisibleType.Content
                                lbOption.Items.Add(New ListItem(ThemeOption.OptionLabel & " --> " & "Content", ThemeOption.KeyOptionExtra.Id & "|" & ThemeOption.Visible))
                            
                            Case ThemeOptionExtraValue.VisibleType.Service
                                lbOption.Items.Add(New ListItem(ThemeOption.OptionLabel & " --> " & "Service", ThemeOption.KeyOptionExtra.Id & "|" & ThemeOption.Visible))
                        End Select
                                              
                    Next
                Else
                    lbOption.Items.Clear()
                End If
                
                If .ConsolleRole > 0 Then
                    Dim oRoles As New RoleManager()
                    Dim oSearcher As New RoleSearcher()
                    oSearcher.Key.Id = .ConsolleRole
                    ctlConsolleRole.GenericCollection = oRoles.Read(oSearcher)
                Else
                    ctlConsolleRole.GenericCollection = Nothing
                End If
                ctlConsolleRole.LoadControl()
            End With
        End If
    End Sub
           
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        Language = masterPageManager.GetLang.Id
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Domain = strDomain
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadThemesValue)
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridListThemes)
                    BindFilerObject()
                End If
            Case ControlType.Selection
                rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridListThemes)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
   
      
    Sub Page_load()
        BindContentType()
        
        If Not Page.IsPostBack Then
            BindComboOptions()
            BindComboWorkFlow()
            ShowRightPanel()
            BindDomains()
        End If
        
        
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ThemeSearcher = Nothing)
        oThemesManager = New ThemeManager
        oThemesManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New ThemeSearcher
        End If
                
        oThemesCollection = oThemesManager.Read(Searcher)
        If Not oThemesCollection Is Nothing AndAlso oThemesCollection.Count > 0 Then
            oThemesCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = oThemesCollection
        objGrid.DataBind()
                
    End Sub
    
    ' Ritorna la descrizione di un Event
    Function GetDescriptionEvent(ByVal idEvent As Integer) As String
        If Not (idEvent = 0) Then
            Dim oTEManager As New TraceEventManager
            Return oTEManager.Read(New TraceEventIdentificator(idEvent)).Description
        End If
        
        Return Nothing
    End Function
    
    ' Ritorna la descrizione di un Event
    Function GetDescriptionMasterPage(ByVal idMasterPage As Integer) As String
        Try
            If Not (idMasterPage = 0) Then
                Dim oMPManager As New MasterPageManager
                Return oMPManager.ReadMasterPage(idMasterPage).Description
            End If
        Catch ex As Exception
        End Try
        Return Nothing
    End Function
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
        
    ''' <summary>
    ''' Caricamento della griglia dei temi
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSiteAreaValue As SiteAreaValue
        
        oThemesSearcher = New ThemeSearcher
        If txtId.Text <> "" Then oThemesSearcher.Key = New ThemeIdentificator(txtId.Text)
        oThemesSearcher.Active = SelectOperation.All
        oThemesSearcher.Display = SelectOperation.All
        'oThemesSearcher.Delete = SelectOperation.All
        
        oSiteAreaValue = GetSiteSelection(Site)
        If Not oSiteAreaValue Is Nothing Then
            oThemesSearcher.KeySite.Id = oSiteAreaValue.Key.Id
            'LoadFatherControl()
        End If
        
        oSiteAreaValue = GetSiteSelection(Area)
        If Not oSiteAreaValue Is Nothing Then
            oThemesSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
            'BindFilerObject()
        End If
        
        If Not drpThemeFather.SelectedItem Is Nothing AndAlso drpThemeFather.SelectedIndex <> 0 Then
            oThemesSearcher.KeyFather.Id = drpThemeFather.SelectedItem.Value
        End If
        
        If (textDescription.Text <> "") Then
            oThemesSearcher.Description = textDescription.Text
        End If
        
        If (txtNameArea.Text <> "") Then
            oThemesSearcher.Key.Name = txtNameArea.Text
        End If
       
        If dwlDomain.SelectedItem.Value <> "-1" Then
            oThemesSearcher.Key.Domain = dwlDomain.SelectedItem.Value
        End If
        
        If dwlContentType.SelectedValue <> -1 Then
            oThemesSearcher.KeyContentType.Id = dwlContentType.SelectedItem.Value
        End If
              
        ReadSelectedItems()
        BindGrid(objGrid, oThemesSearcher)
    End Sub
    
    Sub SearchThemes(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListThemes)
    End Sub
    
    Protected Sub gridListThemes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)            
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    ''' <summary>
    ''' Lettura della descrizione dei content dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetContentValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.Title
        End If
    End Function
    
    ''' <summary>
    ''' Lettura delle descrizioni dei content type dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentTypeValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetContentType(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
    
    ''' <summary>
    ''' Lettura delle descrizioni dei siti dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
    
    ''' <summary>
    ''' Lettura delle descrizioni dei themi dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFatherTheme(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetThemeValue(id)
        
        If ov Is Nothing Then
            Return Nothing
        Else
            Return ov.Description
        End If
    End Function
 
    ''' <summary>
    ''' Ordinemento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListThemes_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridListThemes.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Key.Id"
                If SortType <> ThemeGenericComparer.SortType.ById Then
                    SortType = ThemeGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> ThemeGenericComparer.SortType.ByLabel Then
                    SortType = ThemeGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "KeySite.Id"
                If SortType <> ThemeGenericComparer.SortType.BySite Then
                    SortType = ThemeGenericComparer.SortType.BySite
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "KeySiteArea.Id"
                If SortType <> ThemeGenericComparer.SortType.BySiteArea Then
                    SortType = ThemeGenericComparer.SortType.BySiteArea
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "KeyFather.Id"
                If SortType <> ThemeGenericComparer.SortType.ByFather Then
                    SortType = ThemeGenericComparer.SortType.ByFather
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
                
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As ThemeGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ThemeGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ThemeGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ThemeGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ThemeGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ThemeGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListThemes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litTheme")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        lnkCopy = e.Row.FindControl("lnkCopy")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False
                    lnkCopy.Visible = False
                    
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    lnkDelete.Visible = True
                    lnkCopy.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False
                    lnkCopy.Visible = False
            End Select
        End If
    End Sub
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListThemes.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litTheme")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then                       
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)                   
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ThemeCollection
        Get
            Select Case TypeControl                    
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curThemeValue As ThemeValue
                    Dim outThemesCollection As New ThemeCollection
            
                    For Each str As String In CheckedItems
                        curThemeValue = oGenericUtlity.GetThemeValue(Int32.Parse(str))
                        If Not curThemeValue Is Nothing Then
                            outThemesCollection.Add(curThemeValue)
                        End If
                    Next
            
                    Return outThemesCollection
            End Select
            Return Nothing
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedIdThemes = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListThemes)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sui bottoni Select e Copy nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        
        TypeControl = ControlType.Edit
        SelectedIdThemes = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
             
        Select Case s.commandname
            Case "SelectItem"
                btnSave.Text = "Update"
                NewItem = False
            Case "CopyItem"
                btnSave.Text = "Save"
                NewItem = True
            Case "DeleteItem"
                TypeControl = ControlType.View
                Delete(SelectedIdThemes)
                BindWithSearch(gridListThemes)
        End Select
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Save/Update del tema
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oThemeManager As New ThemeManager
        Dim othemeValue As ThemeValue = ReadFormValues()
        'se valido
        
        
        If valido(othemeValue.Key.Id) Then
                  
            If NewItem Then
                
                othemeValue = oThemeManager.Create(othemeValue)
            Else
            
                othemeValue = oThemeManager.Update(othemeValue)
                   
            End If
        
            SaveRoles(othemeValue.Key)
            SaveOptionExtra(othemeValue.Key)
            BindDomains()
            If othemeValue Is Nothing Then
                strJs = "alert('Error during saving')"
            Else
                strJs = "alert('Saved successfully')"
                goBack(Nothing, Nothing)
            End If
            'else
            'errore
            
        
        End If
    End Sub
    
    'Bug 2286
    Function valido(ByVal id As Integer) As Boolean
        
        If txtSite.Text = "" Then
            strJs = "alert('Site is a required field')"
            Return False
        End If
        
        If SeoName.Text <> "" Then
            Dim manTheme As New ThemeManager
            Dim themeColl As ThemeCollection
            Dim searchtheme As New ThemeSearcher
            Dim CurValue As New ThemeValue
            
           
            
           
            searchtheme.SeoName = SeoName.Text
            searchtheme.KeySite.Id = CType(txtHideSite.Text, Integer)
            manTheme.Cache = False
            themeColl = manTheme.Read(searchtheme)
           
            
            If (Not themeColl Is Nothing AndAlso themeColl.Count > 0) Then
               
                If themeColl(0).Key.Id <> id Then
                
                
                   
                    strJs = "alert('the seoName already exist for this site')"
                    Return False
                End If
            End If
          
        End If
    
        
        If Theme_Name.Text <> "" And Theme_Domain.Text <> "" Then
            
            Dim manTheme As New ThemeManager
            Dim themeColl As ThemeCollection
            Dim searchtheme As New ThemeSearcher
           
            searchtheme.Key.Domain = Theme_Domain.Text
            searchtheme.Key.Name = Theme_Name.Text
            manTheme.Cache = False
            themeColl = manTheme.Read(searchtheme)
           
            If (Not themeColl Is Nothing AndAlso themeColl.Count > 0) Then
                If themeColl(0).Key.Id <> id Then
                    strJs = "alert('The \""Domain - Name""  combination must be unique  ')"
                    Return False
                End If
            End If
        End If
        
        
        Return True
    End Function
    
    
    
    ''' <summary>
    ''' Rimuovo tutti i vecchi accoppiamenti
    ''' </summary>
    ''' <param name="themesid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function RemoveAllRoles(ByVal themesid As ThemeIdentificator) As Boolean
        Dim oThemeManager As New ThemeManager
        Return oThemeManager.RemoveAllRoles(themesid)
    End Function
    
    ''' <summary>
    ''' Delete Theme
    ''' </summary>
    ''' <param name="themesid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Delete(ByVal themesid As Integer) As Boolean
        Dim oThemeManager As New ThemeManager
        Return oThemeManager.Delete(New ThemeIdentificator(themesid))
    End Function
    
    ''' <summary>
    ''' Salvataggio dei ruoli associati al tema
    ''' </summary>
    ''' <param name="themesid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function SaveRoles(ByVal themesid As ThemeIdentificator) As Boolean
        Try
            'Recupero la lista di ruoli nell controllo
            Dim oRoleCollection As Object = ctlRoles.GetSelection
            If Not oRoleCollection Is Nothing Then
                Dim oThemeManager As New ThemeManager
                
                'Tento la rimozione dei vecchi accoppiamenti
                If RemoveAllRoles(themesid) Then
                    For Each oRoleValue As RoleValue In oRoleCollection
                        'Creo i nuovi
                        oThemeManager.CreateRole(themesid, oRoleValue.Key)
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    'restitiusce tutte le option Extra legate ad un Theme 
    Function getOptionExtra(ByVal themesid As ThemeIdentificator) As ThemeOptionExtraCollection
        Dim oThemeManager As New ThemeManager
        Dim oOptionExtraColl As New ThemeOptionExtraCollection
        Dim oOptionExtraSearcher As New ThemeOptionExtraSearcher
        
        oThemeManager.Cache = False
        oOptionExtraSearcher.KeyTheme = themesid
        oOptionExtraColl = oThemeManager.ReadThemeOptionExtra(oOptionExtraSearcher)
       
        Return oOptionExtraColl
    End Function
    
    'salva gli optionExtra associati al Theme
    Function SaveOptionExtra(ByVal themesid As ThemeIdentificator) As Boolean
        Dim TOEValue As New ThemeOptionExtraValue
        Dim oThemeManager As New ThemeManager
     
        Try
                       
            Dim optionCollection As Object = lbOption.Items
            TOEValue.KeyTheme = themesid
            
            oThemeManager.RemoveAllThemeOptionExtraRelation(themesid)
            
            If Not (optionCollection Is Nothing) Then
                If (optionCollection.Count > 0) Then
                                   
                    For Each optionItem As ListItem In optionCollection
                        Dim resultStrings() As String = optionItem.Value.Split("|")
                                               
                        TOEValue.KeyOptionExtra = New ContentOptionIdentificator(resultStrings(0))
                        Select Case resultStrings(1)
                            Case 1
                                TOEValue.Visible = ThemeOptionExtraValue.VisibleType.Service
                                
                            Case 2
                                TOEValue.Visible = ThemeOptionExtraValue.VisibleType.Content
                            Case 3
                                TOEValue.Visible = ThemeOptionExtraValue.VisibleType.Always
                        End Select
                        
                        oThemeManager.CreateThemeOptionExtra(TOEValue)
                    Next
                End If
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
        
    End Function
    
    'popola la dropdownlist delle OptionExtra
    Sub BindComboOptions()
        Dim optionValue As ContentOptionValue
        Dim item As ListItem
        
        optManager = New ContentOptionService
        optSearcher = New ContentOptionSearcher
               
        optCollection = optManager.Read(optSearcher)
        If Not optCollection Is Nothing Then
                       
            For Each optionValue In optCollection
                item = New ListItem
                item.Text = optionValue.Label
                item.Value = optionValue.Key.Id
                
                dwlOptionExtra.Items.Add(item)
            Next
            dwlOptionExtra.Items.Insert(0, New ListItem("Select option", 0))
        End If
    End Sub
    
    'popola la dropdownlist relativa ai workflow
    Sub BindComboWorkFlow()
        Dim workFlowManager As New WorkFlowManager
        Dim workFlowSearcher As New WorkFlowSearcher
        
        Dim workFlowColl As WorkFlowCollection = workFlowManager.Read(workFlowSearcher)
   
        For Each elem As WorkFlowValue In workFlowColl
            dwlWorkFlows.Items.Add(New ListItem(elem.Description, elem.Key.Id))
        Next
        dwlWorkFlows.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    
    'aggiunge un optionExtra alla lista
    Sub AddOptionExtra(ByVal sender As Object, ByVal e As EventArgs)
        Dim optionValue As Integer
        Dim optionVisibleValue As Integer
        Dim optionText As String
        Dim optionVisibleText As String
        
        If (dwlOptionExtra.SelectedIndex <> 0) Then
            optionValue = CType(dwlOptionExtra.SelectedItem.Value(), Integer)
            optionText = dwlOptionExtra.SelectedItem.Text
            
            optionVisibleValue = dwlVisible.SelectedItem.Value()
            optionVisibleText = dwlVisible.SelectedItem.Text
                    
                     
            lbOption.Items.Add(New ListItem(optionText & " --> " & optionVisibleText, optionValue & "|" & optionVisibleValue))
        Else
            strJs = "alert('Select an Option')"
                  
        End If
    End Sub
    
    'elimina un option extra dalla lista
    Sub DeleteOptionExtra(ByVal sender As Object, ByVal e As EventArgs)
        'Response.Write("Elem sel" & lbOption.SelectedIndex)
        If Not (lbOption.SelectedItem Is Nothing) Then
            lbOption.Items.RemoveAt(lbOption.SelectedIndex)
        Else
            strJs = "alert('Select an Option to remove ')"
        End If
    End Sub
    
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As ThemeValue
        Dim othemeValue As New ThemeValue
        
        With othemeValue
            .Description = Description.Text
            .Key.Domain = Theme_Domain.Text
            .Key.Name = Theme_Name.Text
            .SeoName = SeoName.Text
            '.Launch = Launch.Value
            .Launch = Launch.Content
            .MetaDescription = MetaDescription.Text
            .MetaKeywords = MetaKeywords.Text
            .Order = Order.Text
            .Image = Image.Text
            
            If txtIdFather.Text <> "" Then
                .KeyFather.Id = txtIdFather.Text
            End If
            
            If txtHideSiteArea.Text <> "" Then
                .KeySiteArea.Id = txtHideSiteArea.Text
            End If
            
            If txtHideSite.Text <> "" Then
                .KeySite.Id = txtHideSite.Text
            End If
            
            If txtHiddenContentType.Text <> "" Then
                .KeyContentType.Id = txtHiddenContentType.Text
            End If
            
            If txtHiddenEvent.Text <> "" Then
                .KeyTraceEvent.Id = txtHiddenEvent.Text
            End If
            
            If txtHiddenMasterPage.Text <> "" Then
                .KeyMasterPage.Id = txtHiddenMasterPage.Text
            End If
               
            .KeyWorkFlow.Id = dwlWorkFlows.SelectedValue
            .KeyContent.Id = IIf(txtHiddenContent.Text = "", 0, txtHiddenContent.Text)
                        
            If Not NewItem Then
                .Key.Id = SelectedIdThemes
            End If
                                             
            .Active = active.Checked
            .Display = display.Checked
            .IsHttps = ckHttps.Checked
            
            .Note = Note.Text
            If Not ctlConsolleRole.GetSelection Is Nothing AndAlso ctlConsolleRole.GetSelection.Count > 0 Then .ConsolleRole = ctlConsolleRole.GetSelection(0).Key.Id
        End With
        
        Return othemeValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo thema
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewTheme(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedIdThemes = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
             
        Order.Text = 1
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is ctlRoleList Then
                ctl.clear()
            End If
        Next
        
        lbOption.Items.Clear()
        'Launch.Value = Nothing
        Launch.Content = Nothing
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momenti ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        ThemesId.InnerText = ""
        ThemesDescrizione.InnerText = "New Theme"
    End Sub
    
    ''' <summary>
    ''' Legge la stringa descrittiva relativa al contentid associato al tema
    ''' </summary>
    ''' <param name="contentId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentDescription(ByVal contentId As Int32) As String
        If contentId = 0 Then Return ""
        Dim oContentCollection As ContentCollection = oGenericUtility.GetContentList(contentId)
        Dim strOut As String = String.Empty
        Dim oLanguageValue As LanguageValue
        
        If Not oContentCollection Is Nothing AndAlso oContentCollection.Count > 0 Then
            For Each oContentValue As ContentValue In oContentCollection
                oLanguageValue = oGenericUtility.ReadLanguageValue(oContentValue.Key.IdLanguage)
                
                If Not oLanguageValue Is Nothing Then
                    strOut += oContentValue.Title & " (" & oLanguageValue.Description & "),"
                End If
            Next
        End If
        
        If strOut <> "" Then
            Return strOut.Substring(0, strOut.Length - 1)
        End If
        Return Nothing
    End Function
    
    'Recupera i 
    Public Function GetOptionExtraValue(ByVal id As Int32) As ContentOptionCollection
        
        If id = 0 Then Return Nothing
        Dim key As New ThemeIdentificator
        key.Id = id
        
        Dim ThemeManager As New ThemeManager
        ThemeManager.Cache = False
        Return ThemeManager.ReadThemeOptionExtra(key)
    End Function
    
    'popola la dwl per i contentType
    Sub BindContentType()
        Dim contentTypeColl As ContentTypeCollection = contentTypeManager.Read(contentTypeSearcher)
    
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwlContentType, contentTypeColl, "Description", "Key.Id")

        dwlContentType.Items.Insert(0, New ListItem("---Select ContentType---", -1))
    End Sub
     
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
   
    ' Bug 2286
    Sub BindDomains()
        oThemesCollection = New ThemeCollection
        oThemesSearcher = New ThemeSearcher
        oThemesManager = New ThemeManager
        oThemesManager.Cache = False
        oThemesSearcher.Properties.Add("Key.Domain")
  
        oThemesCollection = oThemesManager.Read(oThemesSearcher)
        
     
        
        
        Dim query = (From t As ThemeValue In oThemesCollection Select t.Key.Domain).ToArray().Distinct
          
        Dim strListelem As String
        For Each strListelem In query
            
            If strListelem <> "" Then
                dwlDomain.Items.Add(strListelem)
            End If
        Next
       
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
       
    End Sub
    
    
    
    
    
    
    
</script>
<script type="text/javascript">       
   
    var msg
              
        function AddList(List)              
            {                
                var oDiv = document.getElementById ('<%=ElencoContent.clientid%>')
                if (oDiv!=null)
                    {
                        oDiv.innerHTML = List
                    }
            }
            
        function getTextVal(strText)
        {            
            var oTxt = document.getElementById (strText)
            return  oTxt.value
        }    
        
        function addRow(ContentsKey,strValId,strValDesc,strTxtDest2,strTxtDest1,strTxtDest3,ListLanguages)
	    {	        
	   
	        var arr = strTxtDest2.split('_')
	        var id = arr[arr.length -1];
	        
	        if (ExistsId(strValId,strTxtDest2))
	            alert('Content already has been inserted ');
	        else
	            {    
	              
	                if (strTxtDest1!='')
	                    {
							
	                        var oTxtId = document.getElementById (strTxtDest1);
	                        oTxtId.value=strValDesc;
	                    }
        	        
	                if (strTxtDest2!='')
	                    {	                        
	                        var oTxtDesc = document.getElementById (strTxtDest2);
	                        oTxtDesc.value=strValId;
	                        
	                         var oTxtId = document.getElementById (strTxtDest1);
	                        oTxtId.value=strValId;
	                    }
        	        
	                if (strTxtDest3!='')
	                    {
	                       
	                        var oTxtCK  = document.getElementById (strTxtDest3);
	                        oTxtCK.value = ContentsKey;	
	                    }
	                    
	                AddList(ListLanguages)
	        	}	       	         
	    }
	    
	   	        
	    function ExistsId(val,field)
		    {
		      var oTxtId1 = document.getElementById (field);
		        		        
		        return ((val == oTxtId1.value))// || (val == oTxtId2.value))  
		    }
 
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
       function ClearContent()
        {
            document.getElementById('<%=ElencoContent.clientid%>').innerHTML = ''
            document.getElementById('<%=txtHiddenContent.clientid%>').value = ''
            document.getElementById('<%=txtContent.clientid%>').value = '0'
        }
        
        <%=strJs%>                
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<%--FILTRI SUI TEMI--%>
<asp:Panel id="pnlGrid" runat="server">
    
    <table class="form">
        <tr id="rowToolbar" runat="server">
                <td>
                    <asp:button ID="btnNew" CssClass="button" runat="server" Text="New" onclick="NewTheme" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
               </td>
        </tr>
    </table>  
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Themes Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>
                    <td><strong>Description</strong></td>
                    <td><strong>Site</strong></td> 
                   
                </tr>
                
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                   
                    <td><HP3:Text runat="server" ID="textDescription" TypeControl ="TextBox" style="width:320px"/></td>    
                    <td><HP3:ctlSite runat="server" ID="Site" TypeControl ="combo" SiteType="Site" ItemZeroMessage="---Select Site---"/></td>  
                </tr>
              </table>
              <table class="form">
                <tr> 
                    <td><strong>Area</strong></td>
                    <td><strong>Father</strong></td>
                    <td><strong>ContentType</strong></td> 
                </tr>
                <tr>
                    <td><HP3:ctlSite runat="server" ID="Area" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
                    <td><asp:dropdownlist ID="drpThemeFather" runat="server" /></td>
                    <td><asp:DropDownList ID="dwlContentType" runat="server" /></td>
                </tr>
                  <tr> 
                    <td><strong>Name</strong></td>
                    <td><strong>Domain</strong></td>
                 
                </tr>
                <tr>
                    <td><HP3:Text runat ="server" id="txtNameArea" TypeControl ="TextBox" style="width:250px" /></td>  
                    <td><asp:DropDownList id="dwlDomain" runat="server"/></td>  
                </tr>


                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchThemes" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
<%--FINE FILTRI SUI TEMI--%>

    <%--LISTA TEMI--%>
    <asp:gridview ID="gridListThemes" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"                        
                    AllowSorting="true"
                    OnPageIndexChanging="gridListThemes_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridListThemes_Sorting"
                    emptydatatext="No themes available"                
                    OnRowDataBound="gridListThemes_RowDataBound">
                  <Columns>
                     <asp:TemplateField>
                        <ItemTemplate>
                           <asp:radiobutton ID="chkSel"   runat="server" />
                           <asp:literal ID="litTheme" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Key.Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="16%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />
                     <asp:TemplateField HeaderStyle-Width="20%" HeaderText="ContentType" >
                            <ItemTemplate>
                                <%#ReadContentTypeValue(Container.DataItem.KeyContentType.Id)%>
                            </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Site" SortExpression="KeySite.Id">
                            <ItemTemplate><%#ReadSiteValue(Container.DataItem.KeySite.Id)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Area" SortExpression="KeySiteArea.Id">
                            <ItemTemplate><%#ReadSiteValue(Container.DataItem.KeySiteArea.Id)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Father" SortExpression="KeyFather.Id">
                            <ItemTemplate><%#ReadFatherTheme(Container.DataItem.KeyFather.Id)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkCopy" runat="server" ToolTip ="Copy item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="OnItemSelected" CommandName ="CopyItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>
<%--FINE LISTA TEMI--%>


<%--EDIT TEMI--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnBack" CssClass="button" runat="server" Text="Archive" onclick="goBack" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" CssClass="button" runat="server" Text="Save" onclick="Update"    style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        
    </div>
    <span id="ThemesId" class="title" runat ="server"/><span id="ThemesDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td style="width: 20%" valign="top">
                <a href="#" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=Domain&Help=cms_HelpThemesDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')">
                    <img src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a>
                <%= getLabel("form_1313", "Domain")%>
            </td>
            <td>
                <HP3:Text runat="server" ID="Theme_Domain" TypeControl="TextBox" MaxLength="5" Style="width: 600px" />
               </td>
             
                    
                
 
            
        </tr>

        <tr>
            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=Name&Help=cms_HelpThemesName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("form_1314", "Name")%></td>
            <td><HP3:Text runat ="server" ID="Theme_Name" TypeControl ="TextBox" MaxLength ="10" style="width:600px"/></td>
        </tr>
        
        
        
         <tr>
            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpThemesDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:600px"/></td>
        </tr>
     
        <tr>
            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=seoname&Help=cms_HelpThemesSeoname&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("form_1315", "SeoName")%></td>
            <td><HP3:Text runat ="server" ID="SeoName" TypeControl ="TextBox" MaxLength ="30" style="width:600px"/></td>
        </tr>


        <tr>
            <td style="width:20%"  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormThemeLaunch&Help=cms_HelpThemeLaunch&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormThemeLaunch", "Launch")%></td>
            <td>
<%--                <FCKeditorV2:FCKeditor id="Launch" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="Launch" NewLineBr="false" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                     <Content></Content>
                     <Snippets>
                         <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                     </Snippets>
                     <Languages>
                        <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                        <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                        <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                        <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                     </Languages>

                     <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     
                     
                     <Links>
                        <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                        <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                       </telerik:EditorLink>
                     </Links>
                     <Links>
                       <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                     </Links>        
                                         
                </telerik:RadEditor>
           </td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=csm_FormPMK&Help=csm_HelpPMK&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("csm_FormPMK", "Page Meta Keywords")%></td>
            <td><HP3:Text runat="server" ID="MetaKeywords" TypeControl="Textbox" Style = "width:600px"/></td>
        </tr>

        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPMD&Help=cms_HelpPMD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPMD", "Page Meta Description")%></td>
            <td><HP3:Text runat="server" ID="MetaDescription" TypeControl="TextArea" Style = "width:600px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpThemesOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
            <td><HP3:Text runat ="server" ID="Order" TypeControl ="numericText" style="width:50px"/></td>
        </tr>
        <!--<tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormThemesImage&Help=cms_HelpThemesImage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormThemesImage", "Image")%></td>
            <td><HP3:Text runat ="server" ID="Image" TypeControl ="TextBox" style="width:600px"/></td>
        </tr>-->
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormThemesFather&Help=cms_HelpThemesFather&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormThemesFather", "Father")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtIdFather" runat="server" style="display:none"/><HP3:Text runat ="server" ID="Father"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popThemes.aspx?SelItem=' + document.getElementById('<%=txtIdFather.clientid%>').value + '&ctlHidden=<%=txtIdFather.clientid%>&ctlVisible=<%=Father.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHideSiteArea" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtSiteArea"  TypeControl="TextBox" style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" value="..." class="button" onclick="window.open('<%=Domain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSiteArea.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSiteArea.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Area %>&ListId=<%=txtSiteArea.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHideSite" runat="server" style="display:none"/><asp:TextBox runat ="server" ID="txtSite"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSite.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSite.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Site%>&ListId=<%=txtSite.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEvent&Help=cms_HelpEvent&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEvent", "Event")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox id="txtHiddenEvent" runat="server" style="display:none"/><HP3:Text runat ="server" id="txtEvent"  TypeControl="TextBox"  style="width:300px" isReadOnly="true" /></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popTraceEvent.aspx?SelItem=' + document.getElementById('<%=txtHiddenEvent.clientid%>').value + '&ctlHidden=<%=txtHiddenEvent.clientid%>&ctlVisible=<%=txtEvent.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                 </table>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMasterPage&Help=cms_HelpMasterPage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMasterPage", "MasterPage")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox id="txtHiddenMasterPage" runat="server" style="display:none"/><HP3:Text runat ="server" id="txtMasterPage"  TypeControl="TextBox"  style="width:300px" isReadOnly="true" /></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popMasterPage.aspx?SelItem=' + document.getElementById('<%=txtHiddenMasterPage.clientid%>').value + '&ctlHidden=<%=txtHiddenMasterPage.clientid%>&ctlVisible=<%=txtMasterPage.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                 </table>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormApprovalWF&Help=cms_HelpApprovalWF&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormApprovalWF", "Approval WorkFlow")%></td>
            <td><asp:DropDownList id="dwlWorkFlows" runat="server"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentID&Help=cms_HelpContentID&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentID", "Content ID")%></td>
            <td>
                <table width="100%">
                    <tr>
                        <td style="width:50px"><asp:TextBox ID="txtHiddenContent" runat="server" style="display:none"/> <HP3:Text runat ="server" ID="txtContent"  TypeControl="numericText"  style="width:50px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentContent.aspx?txtHidden=&txtDestField1=<%=txtContent.clientid%>&txtDestField3=&txtDestField2=<%=txtHiddenContent.clientid%>','','width=780,height=480,scrollbars=yes')" /> <input type="button" class="button" value="Clear" onclick="javascript:ClearContent()" /></td>        
                    </tr>   
                </table>        
                <div id="ElencoContent" style="width:100%" runat="server"/>
            </td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" ID="ctlRoles" typecontrol="GenericRelation"  EnableViewState="true"/></td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormConsolleRole&Help=cms_HelpConsolleRole&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormConsolleRole", "Consolle Role")%></td>
            <td><HP3:ctlRoles runat ="server" ID="ctlConsolleRole" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOptions&Help=cms_HelpOptions&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOptions", "Options")%></td>
               <td><asp:ListBox id="lbOption" runat="server" width="200px"></asp:ListBox><asp:button ID="btDelete" CssClass="button" runat="server" Text="Delete" Style ="width:100px" onClick="DeleteOptionExtra"/></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="4">
            <asp:dropdownlist id="dwlOptionExtra" runat="server"></asp:dropdownlist>
            &nbsp;Visible&nbsp;
            <asp:dropdownlist id="dwlVisible" runat="server">
                    <asp:ListItem Selected="True" Text="Always" Value="3" />
                    <asp:ListItem Text="Content" Value="2" />
                    <asp:ListItem Text="Service" Value="1" />
            </asp:dropdownlist>&nbsp;
            <asp:button ID="addOption" CssClass="button" runat="server" Text="Add Option"  Style ="width:100px" onclick="AddOptionExtra" />
            </td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_HelpNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNote", "Note")%></td>
                <td><HP3:Text runat ="server" ID="Note"  TypeControl="TextArea"   style="width:300px"/></td>
        </tr>       
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="active" runat="server" Checked="true"/></td>
        </tr>     
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="display" runat="server" Checked="true"/></td>
        </tr>
        <tr>
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHttps&Help=cms_HelpHttps&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHttps", "Https")%></td>
            <td><asp:checkbox ID="ckHttps" runat="server" Checked="false"/></td>
        </tr>          
    </table>
</asp:Panel>
<%--FINE EDIT TEMI--%>
