<%@ Control Language="VB" ClassName="ChooseBusiness" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Product"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>
<%@ Register TagPrefix="HP3" TagName="ctlLanguage" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>

<script runat="server">
    Private contextBussManager As New ContextBusinessManager
    Private businessSearcher As BusinessSearcher
    Private businessIdent As BusinessIdentificator
    Private webRequest As New WebRequestValue
    Private strJS As String
    
    Private _strDomain As String
    Private _language As String
    
    'Const Categories As String = "Categories"
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property ContextId() As Integer
        Get
            Return ViewState("contextId")
        End Get
        Set(ByVal value As Int32)
            ViewState("contextId") = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        Dim lang As String = Request("L")
        Dim business As String = Request("B")
        Dim ctxbuss As String = Request("Cb")
                      
        'il controllo gestisce la scelta del mercato e della lingua
        plHBusinness.Controls.Add(LoadControl("/hp3Office/Hp3Common/ChooseBusiness.ascx"))
        divNewItem.Visible = False
        ChangeLayout()
        
        'se � stato eseguito correttamente il primo step cio� la scelta del mercato e della lingua,
        'il controllo non viene pi� visualizzato
        If (lang <> String.Empty) And (business <> String.Empty) Then
            plHBusinness.Visible = False
            divNewItem.Visible = True
            
            'faccio il bind delle dwls 
            ctlSiteArea.LoadControl()
            relContentType.LoadControl()
            
            plHCatTree.Controls.Clear()
            plHCatTree.Controls.Add(LoadControl("/hp3Office/Hp3Common/CategoriesTree.ascx"))
                     
            
            'Dim catTree As Object = plHCatTree.Controls(0)
            'CType(catTree, Object).Type = Categories
            
            'nel caso in cui si � scelto il mercato e la lingua e si vuole modificare un ramo dell'albero delle categorie 
            'il sistema rimanda alla gestione dll'edit del context, del gruppo e del contextbusiness per la categoria da modificare.
            If (ctxbuss <> String.Empty) Then
                EditCategory()
            End If
        End If
    End Sub
    
   
    'cambio la visualizzazione del layout della pagina
    Sub ChangeLayout()
        'non visualizzo pi� la dwl per la scelta del sito e l'abero dei temi
        Dim dwlControl = Me.Parent.Parent.Parent.Parent.FindControl("SiteSelect")
        dwlControl.Visible = False
        Dim themesControl = Me.Parent.Parent.Parent.Parent.FindControl("ThemesMenu")
        themesControl.visible = False
        'modifico il layout della pagina
        Dim classControl = Me.Parent.Parent.Parent.Parent.FindControl("dPage")
        classControl.Attributes.Remove("class")
    End Sub
    
    'gestisce la creazione di una nuova categoria
    Sub NewCategory(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelDett()
                
        SelectedId = 0
        LoadCtxDett(Nothing)
        btnNew.Visible = False
        btnDelete.Visible = False
    End Sub
    
    'gestisce l'edit di una categoria
    Sub EditCategory()
        ActivePanelDett()
        Dim contextId As Integer = Request("Cx")
        SelectedId = contextId
                
        BusinessContextManager.Cache = False
        Dim contextValue As ContextValue = Me.BusinessContextManager.Read(New ContextIdentificator(contextId))
                  
        LoadCtxDett(contextValue)
        btnNew.Visible = False
    End Sub
    
    'visualizza i valori nel form relativo al context
    Sub LoadCtxDett(ByVal ctxValue As ContextValue)
        Dim ctl As Object
        
        If Not (ctxValue Is Nothing) Then
            
            txtKeyName.Text = ctxValue.Key.KeyName
            txtDomain.Text = ctxValue.Key.Domain
            txtName.Text = ctxValue.Key.Name
            languageSelector.LoadLanguageValue(ctxValue.Key.Language.Id)
            languageSelector.Enabled = False
            
            txtDescription.Text = ctxValue.Description
            txtText.Text = ctxValue.Text
            txtHiddenFather.Text = ctxValue.KeyFather.Id
            txtFather.Text = ReadContextValue(ctxValue.KeyFather.Id)
            txtOrder.Text = ctxValue.Order
            relContentType.SetSelectedValue(ctxValue.KeyContentType.Id)
            ctlSiteArea.SetSelectedValue(ctxValue.KeySiteArea.Id)
            
        Else
            'pulisce il form dai vecchi valori
            For Each ctl In pnlDett.Controls
                If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                    ctl.text = ""
                ElseIf TypeOf (ctl) Is CheckBox Then
                    CType(ctl, CheckBox).Checked = False
                End If
            Next
        
            txtDomain.Text = "CVT"
            
            relContentType.SetSelectedIndex(0)
            ctlSiteArea.SetSelectedIndex(0)
            
            languageSelector.LoadLanguageValue(Request("L"))
            languageSelector.Enabled = False
        End If
    End Sub
    
    'visualizza i valori per il form relatiova al contextbusiness 
    Sub LoadCtxBDett(ByVal ctxBussValue As ContextBusinessValue)
        Dim ctl As Object
        
        If Not (ctxBussValue Is Nothing) Then
            txtCBLabel.Text = ctxBussValue.Label
            txtCBDescription.Text = ctxBussValue.Description
            txtCBConsumerLongDescr.Text = ctxBussValue.ConsumerLongDescription
            txtCBConsumerShortDescr.Text = ctxBussValue.ConsumerShortDescription
            txtCBHCPLongDescr.Text = ctxBussValue.HCPLongDescription
            txtCBHCPShortDescr.Text = ctxBussValue.HCPShortDescription
            txtCBMetaDescription.Text = ctxBussValue.Metadescription
            txtCBMetaKeywords.Text = ctxBussValue.Metakeyword
            
            txtCBOrder.Text = ctxBussValue.Order
            txtContextGroupId.Text = Request("Cg")
            txtContextGroup.Text = ReadContextGroupValue(Request("Cg"))
            
        Else
            'pulisce il form dai vecchi valori
            For Each ctl In pnlDett.Controls
                If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                    ctl.text = ""
                End If
            Next
            
        End If
    End Sub
    
    Sub GoBack(ByVal sender As Object, ByVal e As EventArgs)
        RedirectUrl()
    End Sub
   
    'si occupa di rimuovere la categoria selezionata e cio�
    'di rimuovere il context, il contextbusiness e l'associazione con il contextgroup 
    Sub DeleteCategory(ByVal sender As Object, ByVal e As EventArgs)
        Dim contextId As Integer = Request("Cx")
        Dim ctxbussId As Integer = Request("Cb")
        Dim contextgroupId As Integer = Request("Cg")
        
        Dim ctxBIdent As New ContextBusinessIdentificator
        ctxBIdent.Id = ctxbussId
                    
        Dim resCtx As Boolean = Me.BusinessContextManager.Delete(New ContextIdentificator(contextId))
        Dim resCtxB As Boolean = contextBussManager.Remove(ctxBIdent)
        Dim resCtcG As Boolean = Me.BusinessContextManager.RemoveGroupContextRelation(New ContextGroupIdentificator(contextgroupId), New ContextIdentificator(contextId))
    
        If (resCtx) And (resCtxB) And (resCtcG) Then
        
            CreateJsMessage("Deletion seccessful.")
            pnlDett.Visible = False
            RedirectUrl()
        Else
            CreateJsMessage("Sorry, an error happened during the deletion.")
            RedirectUrl()
        End If
       
    End Sub
    
    'gestisce il creazione/update dei context e dei contextbusiness
    Sub SaveCategory(ByVal sender As Object, ByVal e As EventArgs)
        Dim oContextValue As ContextValue
        
        'gestione del context
        If (sender.commandname = "Step1") Then
            'gestisco update/save per la form relativa al context
            oContextValue = New ContextValue
            oContextValue = ReadContextValues()
            
            If SelectedId = 0 Then
                'creazione nuovo context
                oContextValue = Me.BusinessContextManager.Create(oContextValue)
                If Not (oContextValue Is Nothing) Then
                    
                    'setto il contextId dopo aver creato il context
                    ContextId() = oContextValue.Key.Id
            
                    divContextDett.Visible = False
                    divContextBusDett.Visible = True
                    btnSave.Visible = True
                    btnArchive.Visible = False
                    btnDelete.Visible = False
                    litContext.Text = "ID:" & oContextValue.Key.Id & " - " & oContextValue.Key.Domain & "/" & oContextValue.Key.Name
                Else
                    'creazione context non riuscita
                    CreateJsMessage("Sorry, an error happened during the context creation.")
                End If
                
            Else
                'update context
                oContextValue.Key.Id = SelectedId
                oContextValue = Me.BusinessContextManager.Update(oContextValue)
                
                'setto il contextId dopo aver creato il context
                ContextId() = oContextValue.Key.Id
            
                divContextDett.Visible = False
                divContextBusDett.Visible = True
                btnSave.Visible = True
                btnArchive.Visible = False
                btnDelete.Visible = False
                
                litContext.Text = "ID:" & oContextValue.Key.Id & " - " & oContextValue.Key.Domain & "/" & oContextValue.Key.Name
            
                Dim ctxBValue As ContextBusinessValue = ReadContextBusinessValue()
                LoadCtxBDett(ctxBValue)
            End If
        End If
        
        'gestione dell'associazione del context con il gruppo
        'e dell'associazione del context con il mercato e la lingua (ContextBusiness)
        If (sender.commandname = "Step2") Then
            Dim oContextBussValue As New ContextBusinessValue
            oContextBussValue = ReadContextBusinessValues()
            
            If SelectedId = 0 Then
                'creazione dell'associazione context/contextgroup
                Dim creatGroup As Boolean = CreateContextGroupRelation()
               
                If (creatGroup) Then
                    'creazione nuovo contextbusiness
                    oContextBussValue = contextBussManager.Create(oContextBussValue)
                    If Not (oContextBussValue Is Nothing) Then
                                       
                        'creazione contextbusiness riuscita
                        CreateJsMessage("Saved successful.")
                                                     
                        pnlDett.Visible = False
                        RedirectUrl()
                        
                    Else 'else se la creazione de contextbusiness non � andata a buon fine
                        'creazione contextbusiness non riuscita
                        CreateJsMessage("Sorry, an error happened during the creation.")
                    End If
                Else 'else se il gruppo non � stato creato
                    CreateJsMessage("Please, select a group.")
                    
                End If
                
            Else 'else selectId <> 0 siamo in update contextbusiness
                
                'update dell'associazione context/contextgroup
                Dim updateGroup As Boolean = UpdateContextGroupRelation()
                                             
                oContextBussValue.Key.Id = Request("Cb")
                oContextBussValue = contextBussManager.Update(oContextBussValue)
                
                If Not (oContextBussValue Is Nothing) Then
                    'update contextbusiness riuscita
                    CreateJsMessage("Update successful.")
                                       
                    pnlDett.Visible = False
                    RedirectUrl()
                Else
                    'update contextbusiness non riuscita
                    CreateJsMessage("Sorry, an error happened during the update.")
                    pnlDett.Visible = False
                End If
            End If
        End If
    End Sub
          
    
    'Lettura dei dati dalla form per il context
    Function ReadContextValues() As ContextValue
        Dim oContextValue As New ContextValue
        
        With oContextValue
            .Key.Domain = txtDomain.Text
            .Key.Name = txtName.Text
            .Key.KeyName = txtKeyName.Text
            .Description = txtDescription.Text
            .Text = txtText.Text
            If txtHiddenFather.Text <> "" Then .KeyFather.Id = Int32.Parse(txtHiddenFather.Text)
            If txtOrder.Text <> "" Then .Order = Int32.Parse(txtOrder.Text)

            Dim ctc As ContentTypeCollection = relContentType.GetSelection()
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                .KeyContentType.Id = ctc.Item(0).Key.Id
            End If

            Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(ctlSiteArea)
            If Not oSiteAreaValue Is Nothing Then
                .KeySiteArea = oSiteAreaValue.Key
            End If

            .Key.Language.Id = languageSelector.Language
        End With
        
        Return oContextValue
    End Function
    
    'Lettura dei dati dalla form per il contextbusiness
    Function ReadContextBusinessValues() As ContextBusinessValue
        Dim oContextBussValue As New ContextBusinessValue
        
        With oContextBussValue
            .Label = txtCBLabel.Text
            .Description = txtCBDescription.Text
            .ConsumerLongDescription = txtCBConsumerLongDescr.Text
            .ConsumerShortDescription = txtCBConsumerShortDescr.Text
            .HCPLongDescription = txtCBHCPLongDescr.Text
            .HCPShortDescription = txtCBHCPShortDescr.Text
            .Metadescription = txtCBMetaDescription.Text
            .Metakeyword = txtCBMetaKeywords.Text
            If txtOrder.Text <> "" Then
                .Order = Int32.Parse(txtOrder.Text)
            Else
                .Order = 0
            End If
            
            .KeyContextBusiness.Id = ContextId()
            .KeyContextBusiness.IdBusiness = Request("B")
            .KeyContextBusiness.IdLanguage = Request("L")
        End With
        
        Return oContextBussValue
    End Function
    
    Sub RedirectUrl()
        webRequest = New WebRequestValue
        webRequest.customParam.append("B", Request("b"))
        webRequest.customParam.append("L", Request("L"))
        
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
    'update dell'associazione context/contextgroup
    Function UpdateContextGroupRelation() As Boolean
        Dim contextId As Integer = Request("Cx")
        Dim ctxGroupId As Integer = Request("Cg")
        
        Me.BusinessContextManager.RemoveGroupContextRelation(New ContextGroupIdentificator(ctxGroupId), New ContextIdentificator(contextId))
          
        Return Me.BusinessContextManager.CreateGroupContextRelation(New ContextGroupIdentificator(Integer.Parse(txtContextGroupId.Text)), New ContextIdentificator(contextId))
    End Function
    
    'gestisce l'associazione context/contextgroup
    Function CreateContextGroupRelation() As Boolean
        If (txtContextGroupId.Text <> String.Empty) Then
            Dim contextGroupId As Integer = Integer.Parse(txtContextGroupId.Text)
            Dim _contextId As Integer = ContextId()
        
            Return Me.BusinessContextManager.CreateGroupContextRelation(New ContextGroupIdentificator(contextGroupId), New ContextIdentificator(_contextId))
        End If
        
        Return False
    End Function
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    'restituisce un contextbusinessvalue dato il suo Id
    Function ReadContextBusinessValue() As ContextBusinessValue
        Dim ctxBusId As Integer = Request("Cb")
        
        Dim ctxBusIdent As New ContextBusinessIdentificator()
        ctxBusIdent.Id = ctxBusId
        
        contextBussManager.Cache = False
        Return contextBussManager.Read(ctxBusIdent)
    End Function
    
    'Lettura delle descrizioni dei context type dato l'id
    Function ReadContextValue(ByVal id As Int32) As String
        Dim ov As Object = Me.BusinessContextManager.Read(New ContextIdentificator(id))
               
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
    
    'Lettura delle descrizioni dei contentgroup  dato l'id
    Function ReadContextGroupValue(ByVal id As Integer) As String
        Dim ov As ContextGroupValue = Me.BusinessContextManager.ReadGroup(New ContextGroupIdentificator(id))
       
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.Description
        End If
    End Function
    
       
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
    End Sub
       
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<div id="divchooseBusiness">
     <asp:PlaceHolder id="plHBusinness" runat="server" />
</div>

<div class="dMain">
	<div class="dColumn">
	    <asp:PlaceHolder id="plHCatTree" runat="server" />
	</div>
	<div class="dContent">
	    <div id="divNewItem" runat="server">
            <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewCategory"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        </div>

        <asp:Panel id="pnlDett"  runat="server" visible="false"  width="99%">
             <div>
                <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="GoBack" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
                <asp:Button id ="btnDelete" runat="server" Text="Delete" OnClientClick="return confirm('Confirm Delete?')"  OnClick="DeleteCategory" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/delete.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
            </div>

             <div id="divContextDett"  runat="server">
                 <%--Form per i Context --%>
                 <table  style="margin-top:10px" class="form" width="99%">
                        <tr>
                            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextKeyName&Help=cms_HelpContextKeyName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextKeyName", "Key Name")%></td>
                            <td><HP3:Text runat="server" ID="txtKeyName" TypeControl="TextBox" MaxLength="30" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
                            <td><HP3:Text runat="server" ID="txtDomain" TypeControl="TextBox" MaxLength="5" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
                            <td><HP3:Text runat="server" ID="txtName" TypeControl="TextBox" MaxLength="10" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextLanguage&Help=cms_HelpContextLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextLanguage", "Context Language")%></td>
                            <td><HP3:ctlLanguage ID="languageSelector" TypeControl="AlternativeControl" runat="server" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContextDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                            <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormText&Help=cms_HelpText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormText", "Text")%></td>
                            <td><HP3:Text runat="server" ID="txtText" TypeControl="TextArea" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextFather&Help=cms_HelpContextFather&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextFather", "Father")%></td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td  style="width:50px"><asp:TextBox ID="txtHiddenFather" style="display:none" runat="server" /><HP3:Text runat="server" ID="txtFather" TypeControl="TextBox" style="width:300px" isReadOnly="true" /></td>
                                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popContentContext.aspx?sites=' + document.getElementById('<%=txtHiddenFather.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenFather.clientId%>&ListId=<%=txtFather.clientId%>','','width=600,height=500,scrollbars=yes')" /></td> 
                                    </tr>
                                </table>        
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpContextOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                            <td><HP3:Text runat="server" ID="txtOrder" TypeControl="TextBox" style="width:300px" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
                            <td><HP3:ctlContentType ID="relContentType" TypeControl="combo" ItemZeroMessage="Select ContentType" runat="server" /></td>
                        </tr>
                        <tr>
                            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
                            <td><HP3:ctlSite runat="server" ID="ctlSiteArea" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align:right; width:300px"><asp:Button id ="btnUpdate" runat="server" Text="Step 2" OnClick="SaveCategory" CssClass="button"  CommandName="Step1" /></td>
                        </tr>
                    </table>
                </div>
                <%--End form per i Context --%>
                
                <div id="divContextBusDett" runat="server" visible="false">
                     <div><asp:button ID="btnSave" runat="server" CssClass="button" Text="Save" onclick="SaveCategory"  CommandName="Step2"  Visible="false" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></div>
                     <asp:Label id="litContext"   CssClass="title" runat="server"/>
                     
                         
                     <%--Form per i ContextBusiness --%>
                     <table  style="margin-top:10px" class="form" width="99%">
                          <%--ContextGroup --%>
                          <tr>
                                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContextGroup&Help=cms_HelpContextGroupName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContextGroup", "Context Group Name")%></td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td  style="width:50px"><asp:TextBox ID="txtContextGroupId" style="display:none" runat="server" /><HP3:Text runat="server" ID="txtContextGroup" TypeControl="TextBox" style="width:300px" isReadOnly="true" /></td>
                                            <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popContextGroup.aspx?sites=' + document.getElementById('<%=txtContextGroupId.clientId%>').value + '&SingleSel=1&HiddenId=<%=txtContextGroupId.clientId%>&ListId=<%=txtContextGroup.clientId%>','','width=600,height=500,scrollbars=yes')" /></td> 
                                        </tr>
                                    </table>        
                                </td>
                            </tr>
                            <%--End ContextGroup --%>

                            <tr>
                                <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpContextBussLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
                                <td><HP3:Text runat="server" ID="txtCBLabel" TypeControl="TextBox" MaxLength="50" style="width:300px" /></td>
                            </tr>
                            <tr>
                                <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContextBussDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBDescription" TypeControl="TextArea"   MaxLength="800" style="width:600px" /></td>
                            </tr>
                            <tr>
                                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormConsLD&Help=cms_HelpContextBussConsLD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormConsLD", "Consumer Long Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBConsumerLongDescr" TypeControl="TextArea"  style="width:600px" /></td>
                            </tr>
                            <tr>
                                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormConsSD&Help=cms_HelpContextBussConsSD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormConsSD", "Consumer Short Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBConsumerShortDescr" TypeControl="TextArea"   MaxLength="500" style="width:600px" /></td>
                            </tr>
                            <tr>
                                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPLD&Help=cms_HelpContextBussHCPLD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPLD", "HCP Long Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBHCPLongDescr" TypeControl="TextArea" style="width:600px" /></td>
                            </tr>
                            <tr>
                                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPSD&Help=cms_HelpContextBussHCPSD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPSD", "HCP Short Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBHCPShortDescr" TypeControl="TextArea" MaxLength="500" style="width:600px" /></td>
                            </tr>
                            <tr>
                                <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=csm_FormPMK&Help=cms_HelpContextBussPMK&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("csm_FormPMK", "Page Meta Keywords")%></td>
                                <td><HP3:Text runat="server" ID="txtCBMetaKeywords" TypeControl="TextArea" MaxLength="400" Style = "width:600px"/></td>
                            </tr>
                            <tr>
                                <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPMD&Help=cms_HelpContextBussPMD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPMD", "Page Meta Description")%></td>
                                <td><HP3:Text runat="server" ID="txtCBMetaDescription" TypeControl="TextArea" Style = "width:600px"/></td>
                            </tr>
                            <tr>
                                <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpContextBussOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                                <td><HP3:Text runat="server" ID="txtCBOrder" TypeControl="TextBox"   Style = "width:50px"/></td>
                            </tr>
                            
                       </table>
                       <%--End Form per i ContextBusiness --%>
                </div>
        </asp:Panel>
	</div>
</div>