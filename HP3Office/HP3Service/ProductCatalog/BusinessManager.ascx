<%@ Control Language="VB" ClassName="ChooseBusiness" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Product"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private dictionaryManager As New DictionaryManager
    Private businessManager As New BusinessManager
    Private mPageManager As New MasterPageManager
    Private businessSearcher As BusinessSearcher
    Private businessIdent As BusinessIdentificator
    
    Private strJS As String
    
    Private _strDomain As String
    Private _language As String
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack) Then
            LoadBusinessList()
            LoadSites()
        End If
    End Sub
   
    'recupera la lista dei mercati 
    Sub LoadBusinessList()
        businessSearcher = New BusinessSearcher
        
        businessManager.Cache = False
        Dim businessColl As BusinessCollection = businessManager.Read(businessSearcher)
               
        gdw_businessList.DataSource = businessColl
        gdw_businessList.DataBind()
    End Sub
     
    Sub LoadSites()
        Dim curIdSite As Int32 = Me.ObjectSiteDomain.KeySiteArea.Id
               
        Dim oSiteCollection As SiteCollection = Me.BusinessSiteManager.Read(Me.ObjectTicket.Roles)
        
        Dim siteColl As New SiteCollection
        siteColl = oSiteCollection.DistinctBy("KeySiteArea.Id")
        
        dwlSite.Items.Clear()
        For Each oSiteValue As SiteValue In siteColl
            If curIdSite <> oSiteValue.KeySiteArea.Id Then
                dwlSite.Items.Add(New ListItem(oSiteValue.Label, oSiteValue.KeySiteArea.Id))
            End If
        Next
        dwlSite.Items.Insert(0, New ListItem("Select Site", -1))
    End Sub
    
    Sub LoadThemes(ByVal sender As Object, ByVal e As EventArgs)
        Dim themeSearcher As New ThemeSearcher
        
        If (dwlSite.SelectedItem.Value <> -1) Then
            themeSearcher.KeySite.Id = dwlSite.SelectedItem.Value
            Dim themeCollection As ThemeCollection = Me.BusinessThemeManager.Read(themeSearcher)
             
            dwlTheme.Items.Clear()
            For Each othemeValue As ThemeValue In themeCollection
                dwlTheme.Items.Add(New ListItem(othemeValue.Description, othemeValue.Key.Id))
            Next
        
            dwlTheme.Items.Insert(0, New ListItem("Select Theme", -1))
        Else
            dwlTheme.Items.Clear()
        End If
    End Sub
   
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadBusinessList()
    End Sub
    
    'effettia l'update 
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim businessValue As New BusinessValue
        
        If (SelectedId <> 0) Then
            businessValue.Description = businessDescription.Text
            businessValue.Key.Id = SelectedId
        
            If (checkForm()) Then
                businessManager.Update(businessValue)
            
                SaveBusinessRelation(SelectedId)
                CreateJsMessage("Update succeeded")
                
                LoadBusinessList()
                ActivePanelGrid()
            Else
                CreateJsMessage("The form fields may not be empty.")
            End If
        End If
             
            'creao un nuovo mercato
        If (SelectedId = 0) Then
            businessValue.Description = businessDescription.Text
           
            If (checkForm()) Then
                businessValue = businessManager.Create(businessValue)
                SaveBusinessRelation(businessValue.Key.Id)
                
                CreateJsMessage("Saved successful")
                
                LoadBusinessList()
                ActivePanelGrid()
            Else
                        
                CreateJsMessage("The form fields may not be empty.")
            End If
        End If
        
       
    End Sub
    
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        dwlTheme.Items.Clear()
        dwlSite.SelectedIndex = dwlSite.Items.IndexOf(dwlSite.Items.FindByValue(0))
        LoadDett(Nothing)
    End Sub
   
    'cancellazione fisica di un mercato
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        businessIdent = New BusinessIdentificator()
        
        Dim itemId As Integer = 0
        If sender.CommandName = "DeleteItem" Then
            itemId = sender.commandArgument()
            
            businessIdent.Id = itemId
            businessManager.Remove(businessIdent)
            
            LoadBusinessList()
        End If
    End Sub
        
    'gestisce l'edit di un mercato
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        ActivePanelDett()
        dwlTheme.Items.Clear()
        
        dwlSite.SelectedIndex = dwlSite.Items.IndexOf(dwlSite.Items.FindByValue(0))
        businessIdent = New BusinessIdentificator()
        
        Dim itemId As Integer = 0
        If sender.CommandName = "EditItem" Then
            itemId = sender.commandArgument()
            
            businessIdent.Id = itemId
            SelectedId = itemId
            Dim businessValue As BusinessValue = businessManager.Read(businessIdent)
            
            
            LoadDett(businessValue)
        End If
    End Sub
    
    'visualizza i valori nel form
    Sub LoadDett(ByVal businessValue As BusinessValue)
        If Not (businessValue Is Nothing) Then
            
            businessDescription.Text = businessValue.Description
            
            Dim oBusinessRelColl As BusinessRelationCollection = getBusinessRelation(businessValue.Key.Id)
            
            If Not (oBusinessRelColl Is Nothing) Then
                
                lbBusinessRelation.Items.Clear()
                For Each businessRelOption As BusinessRelationValue In oBusinessRelColl
                    lbBusinessRelation.Items.Add(New ListItem(getSiteDescription(businessRelOption.KeySite.Id) & " --> " & getThemeDescription(businessRelOption.KeyTheme.Id), businessRelOption.KeySite.Id & "|" & businessRelOption.KeyTheme.Id))
               
                Next
            Else
                lbBusinessRelation.Items.Clear()
            End If
        Else
            
            businessDescription.Text = Nothing
            lbBusinessRelation.Items.Clear()
        End If
    End Sub
    
    'restituisce la descrizione di un sito dato l'id
    Function getSiteDescription(ByVal siteId As Integer) As String
        Dim siteAreaValue As SiteAreaValue = Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(siteId))
        
        If Not (siteAreaValue Is Nothing) Then Return siteAreaValue.Name
    
        Return ""
    End Function
    
    'controlla che tutti i campi del form siano inseriti
    Function checkForm() As Boolean
        If (lbBusinessRelation.Items.Count > 0) And (businessDescription.Text <> "") Then
            Return True
        End If
        
        Return False
    End Function
    
    'restituisce la descrizione di un tema dato l'id
    Function getThemeDescription(ByVal themeid As Integer) As String
        Dim themeValue As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(themeid))
        If Not (themeValue Is Nothing) Then Return themeValue.Description
    
        Return ""
    End Function
    
    'restituisce tutte le relazioni(sito/tema) con un mercato
    Function getBusinessRelation(ByVal bussId As Integer) As BusinessRelationCollection
        Dim businessManager As New BusinessManager
        Dim businessRelSearcher As New BusinessRelationSearcher
        
        businessRelSearcher.KeyBusiness.Id = bussId
        Return businessManager.ReadBusinessRelation(businessRelSearcher)
    End Function
    
    'aggiunge un optionExtra alla lista
    Sub AddBusinessRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim siteValue As Integer
        Dim themeValue As Integer
        Dim siteText As String
        Dim optionVisibleText As String
        
        If (dwlSite.SelectedIndex <> 0) Then
            If (dwlTheme.SelectedIndex <> 0) Then
                siteValue = CType(dwlSite.SelectedItem.Value(), Integer)
                siteText = dwlSite.SelectedItem.Text
            
                themeValue = dwlTheme.SelectedItem.Value()
                optionVisibleText = dwlTheme.SelectedItem.Text
                    
                     
                lbBusinessRelation.Items.Add(New ListItem(siteText & " --> " & optionVisibleText, siteValue & "|" & themeValue))
            Else
                strJS = "alert('Select a theme.')"
                  
            End If
        Else
            
            strJS = "alert('Select a site.')"
        End If
    End Sub
    
    'elimina un relazione dalla lista
    Sub DeleteBusinessRelation(ByVal sender As Object, ByVal e As EventArgs)
        If Not (lbBusinessRelation.SelectedItem Is Nothing) Then
            lbBusinessRelation.Items.RemoveAt(lbBusinessRelation.SelectedIndex)
        Else
            strJS = "alert('Select a relation to remove ')"
        End If
    End Sub
    
    'salva le relazioni del tema e sito con il mercato
    Function SaveBusinessRelation(ByVal busid As Integer) As Boolean
        Dim BRValue As New BusinessRelationValue
        Dim businessManager As New BusinessManager
     
        Try
            Dim busRelCollection As Object = lbBusinessRelation.Items
            BRValue.KeyBusiness.Id = busid
            
            businessManager.RemoveBusinessRelation(New BusinessRelationIdentificator(busid))
            
            If Not (busRelCollection Is Nothing) Then
                If (busRelCollection.Count > 0) Then
                                   
                    For Each brItem As ListItem In busRelCollection
                        Dim resultStrings() As String = brItem.Value.Split("|")
                        BRValue.KeySite.Id = resultStrings(0)
                        BRValue.KeyTheme.Id = resultStrings(1)
                                                                      
                        businessManager.CreateBusinessRelation(BRValue)
                    Next
                End If
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
       
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<asp:Panel id="pnlGrid" runat="server">
  <div>
    <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <asp:Label CssClass="title" runat="server" id="sectionTit">Business List</asp:Label>
  <hr />
  
 <asp:gridview id="gdw_businessList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField   HeaderStyle-Width="40%" HeaderText="Description">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Restore item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>
 
 <asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <table  style=" margin-top:10px" class="form" width="99%">
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBusinessDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
            <td><HP3:Text runat ="server" id="businessDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSiteTheme&Help=cms_HelpSiteTheme&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSiteTheme", "Site/Theme")%></td>
               <td><asp:ListBox id="lbBusinessRelation" runat="server" width="200px"></asp:ListBox><asp:button ID="btDelete" CssClass="button" runat="server" Text="Delete" Style ="width:100px" onClick="DeleteBusinessRelation"/></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="4">
            <asp:dropdownlist id="dwlSite" runat="server" OnSelectedIndexChanged="LoadThemes" AutoPostBack="true"></asp:dropdownlist>
            &nbsp;Theme&nbsp;
            <asp:dropdownlist id="dwlTheme" runat="server" ></asp:dropdownlist>&nbsp;
            <asp:button ID="addRelation" CssClass="button" runat="server" Text="Add"  Style ="width:100px" onclick="AddBusinessRelation" />
            </td>
        </tr>
 </table>
</asp:Panel>

