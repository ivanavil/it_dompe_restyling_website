<%@ Control Language="VB" ClassName="ChooseBusiness" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Product"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Convatec.Product"%>
<%@ Import Namespace="Healthware.HP3.Convatec.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Product"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="CVTProduct" Src ="~/hp3Office/HP3Common/CVTProduct.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlCategories" Src ="~/hp3Office/HP3Parts/ctlCategoriesList.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>

<script runat="server">
    Private productManager As New ProductManager
    Private productCVTManager As New ProductCVTManager
            
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    Private _select As Int32
    'Private _themeValue As ThemeValue
    
    Const Products As String = "Products"
    
    'la costante � impostata a True per la gestione dei prodotti specifica per Convatec
    'False, nel caso della gestione dei prodotti standard
    Const isCVTProduct As Boolean = True
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    'Private Property ThemeValue() As ThemeValue
    '    Get
    '        Return _themeValue
    '    End Get
    '    Set(ByVal value As ThemeValue)
    '        _themeValue = value
    '    End Set
    'End Property
    
    Public Property SelectedPK() As Int32
        Get
            Return ViewState("selectedPK")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedPK") = value
        End Set
    End Property
            
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Sub Page_load()
        productManager.Cache = False
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        Dim lang As String = Request("L")
        Dim business As String = Request("B")
        Dim ctxbuss As String = Request("Cb")
        
        'il controllo gestisce la scelta del mercato e della lingua
        plHBusinness.Controls.Add(LoadControl("/hp3Office/Hp3Common/ChooseBusiness.ascx"))
        Dim plHb As Object = plHBusinness.Controls(0)
        CType(plHb, Object).ChooseSite() = True
        
        ChangeLayout()
                          
        'se � stato eseguito correttamente il primo step cio� la scelta del mercato e della lingua,
        'il controllo non viene pi� visualizzato
        If (lang <> String.Empty) And (business <> String.Empty) Then
            plHBusinness.Visible = False
            pnlSearcher.Visible = True
            btnNew.Visible = True
                                    
            plHCatTree.Controls.Clear()
            plHCatTree.Controls.Add(LoadControl("/hp3Office/Hp3Common/CategoriesTree.ascx"))
              
            If (ctxbuss = 0) Then LoadLastProducts()
            
            If Not Page.IsPostBack Then
                'nel caso in cui si � scelto il mercato e la lingua e si vuole modificare un ramo dell'albero delle categorie 
                'il sistema rimanda alla gestione della ricerca
                If (ctxbuss <> 0) Then SearchProductsByCategory(Request("Cx"))
            End If
        End If
    End Sub
   
    'cambio la visualizzazione del layout della pagina
    Sub ChangeLayout()
        'non visualizzo pi� la dwl per la scelta del sito e l'abero dei temi
        Dim dwlControl = Me.Parent.Parent.Parent.Parent.FindControl("SiteSelect")
        dwlControl.Visible = False
        Dim themesControl = Me.Parent.Parent.Parent.Parent.FindControl("ThemesMenu")
        themesControl.visible = False
        'modifico il layout della pagina
        'Dim classControl = Me.Parent.Parent.Parent.Parent.FindControl("dPage")
        'classControl.Attributes.Remove("class")
    End Sub
     
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        LoadArchive()
    End Sub
    
    Sub LoadArchive()
        If (Request("Cx") = 0) Then
            LoadLastProducts()
        Else
            SearchProductsByCategory(Request("Cx"))
        End If
        
        'If (SelectedPK <> 0) Then
        '    ActivePanelGrid()
        'Else
        '    pnlDett.Visible = False
        'End If
    End Sub
    
    'effettua creazione/update di un prodotto
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        'update di un prodotto 
        If (SelectedPK <> 0) Then
            'gestione convatec
            If (isCVTProduct) Then
                Dim PCVTValue As New ProductCVTValue
                  
                PCVTValue.Key.Id = SelectedId
                PCVTValue.Key.PrimaryKey = SelectedPK
                PCVTValue.Title = txtName.Text

                PCVTValue.SubName = txtSubName.Text
                'PCVTValue.Launch = Launch.Value
                PCVTValue.Launch = Launch.Content
                PCVTValue.Key.IdLanguage = ProdLanguage.Language
                PCVTValue.Key.IdBusiness = Request("B")
                PCVTValue.Key.IdGlobal = txtGKey.Text
                'PCVTValue.KeyProductFather.PrimaryKey = 12352
                'PCVTValue.KeyProductNew.PrimaryKey = 12462
               
                PCVTValue.ContentType.Key.Id = txtContentType.Text
                PCVTValue.BarCode = txtBarCode.Text
                PCVTValue.ProductCode = txtProductCode.Text
                PCVTValue.ExtProductCode = txtExtProductCode.Text
                PCVTValue.Description = txtDescription.Text
                PCVTValue.ConsumerLongDescription = txtConsLongDesc.Text
                PCVTValue.ConsumerShortDescription = txtConsShortDesc.Text
                PCVTValue.HCPLongDescription = txtHcpLongDesc.Text
                PCVTValue.HCPShortDescription = txtHcpShortDesc.Text
                PCVTValue.MetaKeywords = MetaKeywords.Text
                PCVTValue.MetaDescription = MetaDescription.Text
                PCVTValue.Note = txtNote.Text
                PCVTValue.LinkLabel = txtLinkLabel.Text
                PCVTValue.LinkUrl = txtLinkUrl.Text
                
                If txtOrder.Text = String.Empty Then
                    PCVTValue.Order = 0
                Else
                    PCVTValue.Order = txtOrder.Text
                End If
                
                PCVTValue.Active = chkActive.Checked
                PCVTValue.Display = chkDisplay.Checked
             
                PCVTValue.LatextStatement = CVTProd.Latext
                PCVTValue.Package = CVTProd.Package
                PCVTValue.Sample = CVTProd.Sample
                PCVTValue.Preferred = CVTProd.Preferred
                
                PCVTValue.KeyFranchise.Id = CVTProd.DeafaultFranchiseId
                PCVTValue.KeyBrand.Id = CVTProd.DeafaultBrandId
                PCVTValue.FranchiseDescription = ReadDescription(CVTProd.DeafaultFranchiseId)
                PCVTValue.BrandDescription = ReadDescription(CVTProd.DeafaultBrandId)
                                
                PCVTValue = productCVTManager.Update(PCVTValue)
                                                                          
                'gestione delle relazioni tra categorie/prodotto
                UpdateCategoriesProductRelation(PCVTValue.Key.Id)
                
                CreateJsMessage("Update successful")
            Else 'gestione standard
                Dim ProdValue As New ProductValue
                
                ProdValue.Key.Id = SelectedId
                ProdValue.Key.PrimaryKey = SelectedPK
                ProdValue.Title = txtName.Text
                ProdValue.SubName = txtSubName.Text
                'ProdValue.Launch = Launch.Value
                ProdValue.Launch = Launch.Content
                ProdValue.Key.IdLanguage = Request("L")
                ProdValue.Key.IdBusiness = Request("B")
                ProdValue.Key.IdGlobal = txtGKey.Text
                'ProdValue.KeyProductFather.PrimaryKey = 12352
                'ProdValue.KeyProductNew.PrimaryKey = 12462
                                               
                ProdValue.ContentType.Key.Id = txtContentType.Text
                ProdValue.BarCode = txtBarCode.Text
                ProdValue.ProductCode = txtProductCode.Text
                ProdValue.ExtProductCode = txtExtProductCode.Text
                ProdValue.Description = txtDescription.Text
                ProdValue.ConsumerLongDescription = txtConsLongDesc.Text
                ProdValue.ConsumerShortDescription = txtConsShortDesc.Text
                ProdValue.HCPLongDescription = txtHcpLongDesc.Text
                ProdValue.HCPShortDescription = txtHcpShortDesc.Text
                ProdValue.MetaKeywords = MetaKeywords.Text
                ProdValue.MetaDescription = MetaDescription.Text
                ProdValue.Note = txtNote.Text
                ProdValue.LinkLabel = txtLinkLabel.Text
                ProdValue.LinkUrl = txtLinkUrl.Text
                
                If txtOrder.Text = String.Empty Then
                    ProdValue.Order = 0
                Else
                    ProdValue.Order = txtOrder.Text
                End If
                             
                ProdValue.Active = chkActive.Checked
                ProdValue.Display = chkDisplay.Checked
                
                ProdValue = productManager.Update(ProdValue)
                
                'gestione delle relazioni tra categorie/prodotto
                UpdateCategoriesProductRelation(ProdValue.Key.Id)
                
                CreateJsMessage("Update successful")
            End If
            
            LoadArchive()
        End If
             
        'crea un nuovo prodotto
        If (SelectedPK = 0) Then
            'recupera il tema di default associato al catalogo prodotti
            Dim themeValue As ThemeValue = getProductTheme()
                       
            'gestione convatec
            If (isCVTProduct) Then
                Dim PCVTValue As New ProductCVTValue
                
                PCVTValue.Title = txtName.Text
                PCVTValue.SubName = txtSubName.Text
                'PCVTValue.Launch = Launch.Value
                PCVTValue.Launch = Launch.Content
                PCVTValue.Key.IdLanguage = ProdLanguage.Language
                PCVTValue.Key.IdBusiness = Request("B")
                PCVTValue.Key.IdGlobal = txtGKey.Text
                
                'PCVTValue.KeyProductFather.PrimaryKey = 12352
                'PCVTValue.KeyProductNew.PrimaryKey = 12462
                
                'gestisce la creazione tra contentType/prodotto
                PCVTValue.ContentType.Key.Id = themeValue.KeyContentType.Id
                                          
                'gestisce la creazione  
                PCVTValue.BarCode = txtBarCode.Text
                PCVTValue.ProductCode = txtProductCode.Text
                PCVTValue.ExtProductCode = txtExtProductCode.Text
                PCVTValue.Description = txtDescription.Text
                PCVTValue.ConsumerLongDescription = txtConsLongDesc.Text
                PCVTValue.ConsumerShortDescription = txtConsShortDesc.Text
                PCVTValue.HCPLongDescription = txtHcpLongDesc.Text
                PCVTValue.HCPShortDescription = txtHcpShortDesc.Text
                PCVTValue.MetaKeywords = MetaKeywords.Text
                PCVTValue.MetaDescription = MetaDescription.Text
                PCVTValue.LinkLabel = txtLinkLabel.Text
                PCVTValue.LinkUrl = txtLinkUrl.Text
                
                If txtOrder.Text = String.Empty Then
                    PCVTValue.Order = 0
                Else
                    PCVTValue.Order = txtOrder.Text
                End If
                
                PCVTValue.Active = chkActive.Checked
                PCVTValue.Display = chkDisplay.Checked
             
                PCVTValue.LatextStatement = CVTProd.Latext
                PCVTValue.Package = CVTProd.Package
                PCVTValue.FranchiseDescription = ReadDescription(CVTProd.DeafaultFranchiseId)
                PCVTValue.BrandDescription = ReadDescription(CVTProd.DeafaultBrandId)
                PCVTValue.Sample = CVTProd.Sample
                PCVTValue.Preferred = CVTProd.Preferred
                PCVTValue.KeyFranchise.Id = CVTProd.DeafaultFranchiseId
                PCVTValue.KeyBrand.Id = CVTProd.DeafaultBrandId
                               
                PCVTValue = productCVTManager.Create(PCVTValue)
                
                'gestisce la creazione della relazione tra sitearea/prodotto
                CreateProductSiteAreaRelation(PCVTValue.Key.Id, themeValue)
                               
                'gestisce la relazione tra site/prodotto
                CreateProductSiteRelation(PCVTValue.Key.Id)
                
                'gestione della creazione delle relazioni tra categorie/prodotto
                CreateCategoriesProductRelation(PCVTValue.Key.Id)
                
                CreateJsMessage("Saved successful")
            Else 'gestione standard
                Dim ProdValue As New ProductValue
                
                ProdValue.Title = txtName.Text
                ProdValue.SubName = txtSubName.Text
                'ProdValue.Launch = Launch.Value
                ProdValue.Launch = Launch.Content
                ProdValue.Key.IdLanguage = Request("L")
                ProdValue.Key.IdBusiness = Request("B")
                ProdValue.Key.IdGlobal = txtGKey.Text
                'ProdValue.KeyProductFather.PrimaryKey = 12352
                'ProdValue.KeyProductNew.PrimaryKey = 12462
                                              
                'gestisce la creazione tra contentType/prodotto
                ProdValue.ContentType.Key.Id = themeValue.KeyContentType.Id
                            
                ProdValue.BarCode = txtBarCode.Text
                ProdValue.ProductCode = txtProductCode.Text
                ProdValue.ExtProductCode = txtExtProductCode.Text
                ProdValue.Description = txtDescription.Text
                ProdValue.ConsumerLongDescription = txtConsLongDesc.Text
                ProdValue.ConsumerShortDescription = txtConsShortDesc.Text
                ProdValue.HCPLongDescription = txtHcpLongDesc.Text
                ProdValue.HCPShortDescription = txtHcpShortDesc.Text
                ProdValue.MetaKeywords = MetaKeywords.Text
                ProdValue.MetaDescription = MetaDescription.Text
                ProdValue.LinkLabel = txtLinkLabel.Text
                ProdValue.LinkUrl = txtLinkUrl.Text
                
                If txtOrder.Text = String.Empty Then
                    ProdValue.Order = 0
                Else
                    ProdValue.Order = txtOrder.Text
                End If
                                     
                ProdValue.Active = chkActive.Checked
                ProdValue.Display = chkDisplay.Checked
                
                ProdValue = productManager.Create(ProdValue)
                           
                'gestisce la creazione della relazione tra sitearea/prodotto
                CreateProductSiteAreaRelation(ProdValue.Key.Id, themeValue)
                
                'gestisce la creazione della relazione tra site/prodotto
                CreateProductSiteRelation(ProdValue.Key.Id)
                
                'gestione della creazione delle relazioni tra categorie/prodotto
                CreateCategoriesProductRelation(ProdValue.Key.Id)
                
                CreateJsMessage("Saved successful")
            End If
                        
            LoadArchive()
            pnlDett.Visible = False
        End If
    End Sub
    
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedPK = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
   
    ''cancellazione di un prodotto
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim prodIdent As New ProductIdentificator()
        
        Dim itemPK As Integer = 0
        If sender.CommandName = "DeleteItem" Then
            
            itemPK = sender.commandArgument()
            prodIdent.PrimaryKey = itemPK
            
            If (isCVTProduct) Then 'gestione dei prodotti cvt
                productCVTManager.Remove(prodIdent)
            Else 'gestione dei prodotti standard
                productManager.Delete(prodIdent)
            End If
            
            LoadArchive()
        End If
    End Sub
        
    'gestisce l'edit di un prodotto
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        ActivePanelDett()
        Dim item As String = ""
        
        If sender.CommandName = "EditItem" Then
            item = sender.commandArgument()
            
            Dim resultStrings() As String = item.Split("|")
            Dim itemPK As Integer = resultStrings(0)
            Dim itemId As Integer = resultStrings(1)
                       
            SelectedPK = itemPK
            SelectedId = itemId
            
            productCVTManager.Cache = False
            If (isCVTProduct) Then 'gestione prodotti convatec
                Dim prodCVT As ProductCVTValue = productCVTManager.Read(New ProductIdentificator(itemPK))
                LoadDett(prodCVT)
            Else 'gestione prodotti standard
                Dim prodValue As ProductValue = productManager.Read(New ProductIdentificator(itemPK))
                LoadDett(prodValue)
            End If
        End If
    End Sub
    
    'visualizza i valori nel form
    Sub LoadDett(ByVal prodValue As Object)
        If Not (prodValue Is Nothing) Then
            Dim prodV As ProductValue = CType(prodValue, ProductValue)
            
            labTitle.Text = "ID: " & prodV.Key.Id & " - " & "PK: " & prodV.Key.PrimaryKey & " - " & "Global Key: " & prodV.Key.IdGlobal
            'da modificare 
            ProdLanguage.LoadLanguageValue(prodV.Key.IdLanguage)
            txtContentType.Text = prodV.ContentType.Key.Id
            txtGKey.Text = prodV.Key.IdGlobal
            txtName.Text = prodV.Title
            txtSubName.Text = prodV.SubName
            'Launch.Value = prodV.Launch
            Launch.Content = prodV.Launch
            txtBarCode.Text = prodV.BarCode
            txtProductCode.Text = prodV.ProductCode
            txtExtProductCode.Text = prodV.ExtProductCode
            txtDescription.Text = prodV.Description
            txtConsShortDesc.Text = prodV.ConsumerShortDescription
            txtConsLongDesc.Text = prodV.ConsumerLongDescription
            txtHcpShortDesc.Text = prodV.HCPShortDescription
            txtHcpLongDesc.Text = prodV.HCPLongDescription
            MetaKeywords.Text = prodV.MetaKeywords
            MetaDescription.Text = prodV.MetaDescription
            txtLinkLabel.Text = prodV.LinkLabel
            txtLinkUrl.Text = prodV.LinkUrl
          
            txtOrder.Text = prodV.Order
            txtNote.Text = prodV.Note
            
            'controllo relativo all'associazione categorie/prodotto
            ctlCategories.GenericCollection = GetCategoriesProductValue(prodV.Key.Id)
            ctlCategories.LoadControl()
            
            If (prodV.Active) Then
                chkActive.Checked = True
            Else
                chkActive.Checked = False
            End If

            If (prodV.Active) Then
                chkDisplay.Checked = True
            Else
                chkDisplay.Checked = False
            End If
                     
            If isCVTProduct = True Then
                'elementi del form specifico convatec
                Dim cvtValue As ProductCVTValue = CType(prodValue, ProductCVTValue)
                
                If (cvtValue.LatextStatement) Then
                    CVTProd.Latext = True
                Else
                    CVTProd.Latext = False
                End If
                
                If (cvtValue.Package) Then
                    CVTProd.Package = True
                Else
                    CVTProd.Package = False
                End If
                
                CVTProd.DeafaultBrandId = cvtValue.KeyBrand.Id
                CVTProd.DeafaultFranchiseId = cvtValue.KeyFranchise.Id
                
                CVTProd.DeafaultFranchise = cvtValue.FranchiseDescription
                CVTProd.DeafaultBrand = cvtValue.BrandDescription
                
                If (cvtValue.Preferred) Then
                    CVTProd.Preferred = True
                Else
                    CVTProd.Preferred = False
                End If
                
                If (cvtValue.Sample) Then
                    CVTProd.Sample = True
                Else
                    CVTProd.Sample = False
                End If
                
            End If
        Else
            txtGKey.Text = Nothing
            txtName.Text = Nothing
            txtSubName.Text = Nothing
            'Launch.Value = Nothing
            Launch.Content = Nothing
            txtBarCode.Text = Nothing
            txtProductCode.Text = Nothing
            txtExtProductCode.Text = Nothing
            txtDescription.Text = Nothing
            txtConsShortDesc.Text = Nothing
            txtConsLongDesc.Text = Nothing
            txtHcpShortDesc.Text = Nothing
            txtHcpLongDesc.Text = Nothing
            MetaKeywords.Text = Nothing
            MetaDescription.Text = Nothing
            txtLinkLabel.Text = Nothing
            txtLinkUrl.Text = Nothing
          
            txtOrder.Text = Nothing
            chkActive.Checked = True
            chkDisplay.Checked = True
            txtNote.Text = Nothing
            
            If (Request("Cb") <> 0) Then
                ctlCategories.GenericCollection = ReadContextBusiness(Request("Cb"))
                ctlCategories.LoadControl()
            Else
                ctlCategories.clear()
            End If
           
            If isCVTProduct = True Then
                'elementi del form specifico convatec
                CVTProd.Latext = False
                CVTProd.Package = False
                
                CVTProd.DeafaultBrandId = 0
                CVTProd.DeafaultFranchiseId = 0
                
                CVTProd.DeafaultBrand = Nothing
                CVTProd.DeafaultFranchise = Nothing
            End If
        End If
    End Sub
    
    'recupera le descrizione dei context filtrando per mercato e lingua
    Function getContextBusinessColl(ByRef contextColl As ContextCollection) As ContextBusinessCollection
        Dim ContextBusinessManager As New ContextBusinessManager
        Dim ctxBusiness As New ContextBusinessCollection
        Dim ctxBusinessColl As New ContextBusinessCollection
        
        For Each context As ContextValue In contextColl
            'recupero le descrizioni del context in base all lingua, mercato e idContext                        
            Dim contextBusinessSearcher As New ContextBusinessSearcher
            contextBusinessSearcher.KeyContextBusiness.Id = context.Key.Id
            contextBusinessSearcher.KeyContextBusiness.IdLanguage = Request("L")
            contextBusinessSearcher.KeyContextBusiness.IdBusiness = Request("B")
               
            ContextBusinessManager.Cache = False
            ctxBusiness = ContextBusinessManager.Read(contextBusinessSearcher)
                
            If Not (ctxBusiness Is Nothing) AndAlso (ctxBusiness.Count > 0) Then
                ctxBusinessColl.Add(ctxBusiness(0))
            End If
        Next
        Return ctxBusinessColl
    End Function
    
    'recupera il tema di default associato al sito e al mercato
    Function getProductTheme() As ThemeValue
        Dim businessRelationSearcher As New BusinessRelationSearcher
        Dim businessManager As New BusinessManager
        
        businessRelationSearcher.KeySite.Id = Request("S")
        businessRelationSearcher.KeyBusiness.Id = Request("B")
        Dim businessRelColl As BusinessRelationCollection = businessManager.ReadBusinessRelation(businessRelationSearcher)
        
        If Not (businessRelColl Is Nothing) And (businessRelColl.Count > 0) Then
            Dim theme As Integer = businessRelColl(0).KeyTheme.Id
            
            Return Me.BusinessThemeManager.Read(New ThemeIdentificator(theme))
           
        End If
        
        Return Nothing
    End Function
    
    Function GetCategoriesProductValue(ByVal prodId As Integer) As ContextBusinessCollection
        Dim prod As ProductIdentificator = New ProductIdentificator()
        prod.Id = prodId
        
        productManager.Cache = False
        
        Dim ctxColl As ContextCollection = productManager.ReadCategoryByProduct(prod)
        If Not (ctxColl Is Nothing) And (ctxColl.Count > 0) Then
            Return getContextBusinessColl(ctxColl)
        End If
        
        Return Nothing
    End Function
    
    'gestisce l'update della relazione tra categorie/prodotto
    Function UpdateCategoriesProductRelation(ByVal prodId As Integer) As Boolean
        'cancello tutte le categorie legate ad un prodotto 
        Dim contId As New ContentIdentificator()
        contId.Id = SelectedId
        
        BusinessContentManager.Cache = False
        Dim CxtColl As ContextCollection = Me.BusinessContentManager.ReadContentContext(contId)
       
        If Not (CxtColl Is Nothing) And (CxtColl.Count > 0) Then
            For Each context As ContextValue In CxtColl
                Dim contentCxtValue As New ContentContextValue
            
                contentCxtValue.KeyContent.Id = SelectedId
                contentCxtValue.KeyContext.Id = context.Key.Id
                Me.BusinessContentManager.RemoveContentContext(contentCxtValue)
            Next
        End If
                       
                     
        'inserisco le nuove associazioni
        Dim ctxBussColl As ContextBusinessCollection = ctlCategories.GetSelection
        
        If Not ctlCategories.GetSelection Is Nothing AndAlso ctlCategories.GetSelection.Count > 0 Then
            Dim ctxBColl As New Object
            Dim cCxtValue As New ContextBusinessValue
            ctxBColl = ctlCategories.GetSelection
                        
            For Each cCxtValue In ctxBColl
                Dim contentCxtValue As New ContentContextValue
                contentCxtValue.KeyContent.Id = SelectedId
                contentCxtValue.KeyContext.Id = cCxtValue.KeyContextBusiness.Id
                               
                Me.BusinessContentManager.CreateContentContext(contentCxtValue)
            Next
        End If
                        
        Return False
    End Function
    
    'gestisce la creazione della relazione tra categorie/prodotto
    Function CreateCategoriesProductRelation(ByVal prodId As Integer) As Boolean
        
        If Not (ctlCategories.GetSelection Is Nothing) And (ctlCategories.GetSelection.Count > 0) Then
            For Each elem As ContextBusinessValue In ctlCategories.GetSelection
                
                Dim contentCXTValue As New ContentContextValue
                contentCXTValue.KeyContent.Id = prodId
                contentCXTValue.KeyContext.Id = elem.KeyContextBusiness.Id
                Return Me.BusinessContentManager.CreateContentContext(contentCXTValue)
            Next
        End If
        
        Return False
    End Function
    
    'gestisce la creazione dell'associazione del prodotto con la sitearea del sito selezionato
    Function CreateProductSiteRelation(ByVal prodId As Integer) As Boolean
        Dim contentSAValue As New ContentSiteAreaValue
        
        contentSAValue.KeyContent.Id = prodId
        contentSAValue.KeySiteArea.Id = Request("S")
        
        Return Me.BusinessContentManager.CreateContentSiteArea(contentSAValue)
    End Function
      
    
    'gestisce la creazione dell'associazione tra prodotto/sitearea del servizio catalogo prodotti.
    Function CreateProductSiteAreaRelation(ByVal prodId As Integer, ByVal themeValue As ThemeValue) As Boolean
        Dim contentSAValue As New ContentSiteAreaValue
        
        contentSAValue.KeyContent.Id = prodId
        contentSAValue.KeySiteArea.Id = themeValue.KeySiteArea.Id
                       
        Return Me.BusinessContentManager.CreateContentSiteArea(contentSAValue)
    End Function
    
    'restituisce la Label del contextBusiness dato il suo id
    Function ReadDescription(ByVal id As Integer) As String
        Return Me.BusinessContextManager.Read(New ContextIdentificator(id)).Description
    End Function
    
    'restituisce la Label del contextBusiness dato il suo id
    Function ReadContextBusiness(ByVal id As Integer) As ContextBusinessCollection
        Dim ctxBSearcher As New ContextBusinessSearcher
        Dim ContextBusinessManager As New ContextBusinessManager
        
        ctxBSearcher.Key.Id = id
        Return ContextBusinessManager.Read(ctxBSearcher)
    End Function
    
    'gestisce la ricerca dei prodotti in base alla categoria
    Sub SearchProductsByCategory(ByVal category As Integer)
        Dim CIdent As New ContextIdentificator
        CIdent.Id = category
        
        productManager.Cache = False
        Dim productColl As ProductCollection = productManager.ReadProductsByCategory(CIdent)
        gdw_productsList.DataSource = productColl
        gdw_productsList.DataBind()
        
        ActivePanelGrid()
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
        pnlSearcher.Visible = False
        btnNew.Visible = False
    End Sub
       
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'gestisce la visualizzazione degli ultimi 20 prodotti
    Sub LoadLastProducts()
        If (isCVTProduct) Then
            Dim prodcvtSearcher As New ProductCVTSearcher
            
            prodcvtSearcher.SetMaxRow = 20
            BindGrid(gdw_productsList, prodcvtSearcher)
        Else
            Dim prodSearcher As New ProductSearcher
            
            prodSearcher.SetMaxRow = 20
            BindGrid(gdw_productsList, prodSearcher)
        End If
        
        lastProds.Visible = True
    End Sub
    
      
    'gestisce la ricerca dei prodotti
    Sub ListProducts(ByVal sender As Object, ByVal e As EventArgs)
        RedirectUrl()
    End Sub
    
    'gestisce la ricerca dei prodotti
    Sub SearchProducts(ByVal sender As Object, ByVal e As EventArgs)
        If (txtId.Text <> "") Or (txtProdName.Text <> "") Then
            If (isCVTProduct) Then
                Dim prodcvtSearcher As New ProductCVTSearcher
                
                If txtId.Text <> "" Then prodcvtSearcher.key.Id = txtId.Text
                'If txtIdentifyCode.Text <> "" Then prodcvtSearcher.   = txtIdentifyCode.Text
                If txtProdName.Text <> "" Then prodcvtSearcher.SearchString = txtProdName.Text
                If (Request("Cx") <> 0) Then prodcvtSearcher.KeyContext.Id = Request("Cx")
                
                BindGrid(gdw_productsList, prodcvtSearcher)
            Else
                Dim prodSearcher As New ProductSearcher
                
                If txtId.Text <> "" Then prodSearcher.key.Id = txtId.Text
                'If txtIdentifyCode.Text <> "" Then prodcvtSearcher.   = txtIdentifyCode.Text
                If txtProdName.Text <> "" Then prodSearcher.SearchString = txtProdName.Text
                If (Request("Cx") <> 0) Then prodSearcher.KeyContext.Id = Request("Cx")
                
                BindGrid(gdw_productsList, prodSearcher)
            End If
        End If
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As Object = Nothing)
       
        If Not Searcher Is Nothing Then
            If (isCVTProduct) Then
                Dim prodCVTColl As New ProductCVTCollection
                
                Dim cvtSearcher As ProductCVTSearcher = CType(Searcher, ProductCVTSearcher)
                productCVTManager.Cache = False
                prodCVTColl = productCVTManager.Read(cvtSearcher)
                
                objGrid.DataSource = prodCVTColl
                objGrid.DataBind()
            Else
                Dim prodColl As New ProductCollection
                
                Dim prodSearcher As ProductSearcher = CType(Searcher, ProductSearcher)
                productManager.Cache = False
                prodColl = productManager.Read(prodSearcher)
                
                objGrid.DataSource = prodColl
                objGrid.DataBind()
            End If
            
            ActivePanelGrid()
        End If
    End Sub
    
    Sub RedirectUrl()
        Dim webRequest As New WebRequestValue
        webRequest.customParam.append("B", Request("b"))
        webRequest.customParam.append("L", Request("L"))
        webRequest.customParam.append("S", Request("S"))
        webRequest.customParam.append("Cx", 0)
        webRequest.customParam.append("Cb", 0)
        webRequest.customParam.append("Cg", 0)
               
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<div id="divchooseBusiness">
     <asp:PlaceHolder id="plHBusinness" runat="server" />
</div>

<div class="dMain">
	<div class="dColumn">
	    <asp:PlaceHolder id="plHCatTree" runat="server" />
	</div>
	<div class="dContent">
	     <%--FILTRO SUI PRODOTTI--%>
        <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="false">
            <table class="topContentSearcher">
                <tr>
                    <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                    <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Products Searcher</span></td>
                </tr>
            </table>
            <fieldset class="_Search">
                <table class="form">
                    <tr>
                        <td><strong>ID</strong></td> 
                        <td><strong>Identify Code</strong></td>   
                        <td><strong>Name</strong></td>
                        
                    </tr>
                    <tr>
                        <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                        <td><HP3:Text runat="server" ID="txtIdentifyCode" TypeControl ="NumericText" style="width:100px"/></td>   
                        <td><HP3:Text runat ="server" id="txtProdName" TypeControl ="TextBox" style="width:400px" MaxLength="400"/></td>  
                    </tr>
                    <tr>
                        <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchProducts" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                        <td><asp:button runat="server" CssClass="button" ID="btnListProd" onclick="ListProducts" Text="List Products" style="margin-top:10px;padding-left:16px;width:120px;background-image:url('/HP3Office/HP3Image/Button/search_allcontent.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                    </tr>
                 </table>
            </fieldset>
            <hr />
        </asp:Panel>
    <%--FINE--%>    
	
	
	    <div id="divNew">
           <asp:Button id ="btnNew" runat="server"  Visible="false" Text="New" OnClick="NewRow"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        </div>
	    <asp:Panel id="pnlGrid" runat="server" Visible="false">
             <asp:Label CssClass="title" runat="server" id="lastProds" Visible="False">Last Products</asp:Label>
             <hr />
              
             <asp:gridview id="gdw_productsList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="20"
                            emptydatatext="No item available">
                          <Columns >
                           <asp:TemplateField HeaderStyle-Width="5%" HeaderText="GlobalKay">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.IdGlobal")%></ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderStyle-Width="40%" HeaderText="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="7%">
                                   <ItemTemplate>
                                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                   </ItemTemplate>
                            </asp:TemplateField>
                          </Columns>
             </asp:gridview>
          </asp:Panel>
             
          <asp:Panel id="pnlDett" runat="server" visible="false">
             <div>
                <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
                <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
             </div>
             
             <asp:Label id="labTitle"  CssClass="title" runat="server"/>
             <table  style="margin-top:10px" class="form" width="99%">
             <%--Form Product Standard--%>
                <tr>
                    <td style="width:25%" valign ="top"></td>
                    <td><HP3:Text ID ="txtContentType" runat="server"  TypeControl="TextBox"  Visible="false"/></td>
                </tr>
                <tr>
                    <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguage&Help=cms_HelpProductLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguage", "Language")%></td>
                    <td><HP3:contentLanguage ID ="ProdLanguage" runat="server"  Typecontrol="StandardControl" /></td>
                </tr>
                <tr>
                    <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyGlobal&Help=cms_HelpKeyGlobal&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyGlobal", "Global Key")%></td>
                    <td><HP3:Text runat="server" id="txtGKey" TypeControl="TextBox"  MaxLength="50" Style = "width:50px"/></td>
                </tr>
                <tr>
                    <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpProductName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
                    <td><HP3:Text runat="server" id="txtName" TypeControl="TextBox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdSubName&Help=cms_HelpProdSubName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdSubName", "SubName")%></td>
                    <td><HP3:Text runat="server" ID="txtSubName" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr runat="server" id="tr_Launch">
                    <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLaunch&Help=cms_HelpProductLaunch&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLaunch", "Launch")%></td>
                    <td>
                        <table>
                            <tr>
                                 <td style="position:relative"><div id="divTxtEditor" class="disable"  style="width:600px;height:300px"  visible="false" runat="server"></div>
<%--                                    <FCKeditorV2:FCKeditor  id="Launch"  Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                                    <telerik:RadEditor ID="Launch" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                         <Content></Content>
                                         <Snippets>
                                             <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                         </Snippets>
                                         <Languages>
                                            <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                            <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                            <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                            <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                         </Languages>

                                         <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                         <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                         <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                         
                                         
                                         <Links>
                                            <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                                            <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                                           </telerik:EditorLink>
                                         </Links>
                                         <Links>
                                           <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                         </Links>        
                                                             
                                    </telerik:RadEditor>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><a href="#" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListCategories&Help=cms_HelpListCategories&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListCategories", "Categories belonging")%></td>
                    <td><HP3:ctlCategories runat ="server" id="ctlCategories"  /></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdBarCode&Help=cms_HelpProdBarCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdBarCode", "Bar Code")%></td>
                    <td><HP3:Text runat="server" ID="txtBarCode" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProdProductCode&Help=cms_HelpProdProductCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProdProductCode", "Product Code")%></td>
                    <td><HP3:Text runat="server" ID="txtProductCode" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtProdCode&Help=cms_HelpExtProdCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormExtProdCode", "Ext Product Code")%></td>
                    <td><HP3:Text runat="server" ID="txtExtProductCode" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpProdDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                    <td><HP3:Text runat="server" ID="txtDescription" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormConsSD&Help=cms_HelpProdConsShortDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormConsSD", "Consumer Short Description")%></td>
                    <td><HP3:Text runat="server" ID="txtConsShortDesc" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                 <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormConsLD&Help=cms_HelpProdConsLongDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormConsLD", "Consumer Long Description")%></td>
                    <td><HP3:Text runat="server" ID="txtConsLongDesc" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPSD&Help=cms_HelpProdHCPShortDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPSD", "HCP Short Description")%></td>
                    <td><HP3:Text runat="server" ID="txtHcpShortDesc" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormHCPLD&Help=cms_HelpProdHCPLongDesc&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormHCPLD", "HCP Long Description")%></td>
                    <td><HP3:Text runat="server" ID="txtHcpLongDesc" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=csm_FormPMK&Help=csm_HelpPMK&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("csm_FormPMK", "Page Meta Keywords")%></td>
                    <td><HP3:Text runat="server" ID="MetaKeywords" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPMD&Help=cms_HelpPMD&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPMD", "Page Meta Description")%></td>
                    <td><HP3:Text runat="server" ID="MetaDescription" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpProdOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                    <td><HP3:Text runat="server" id="txtOrder" TypeControl="TextBox" Style = "width:600px"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
                    <td><asp:checkbox ID="chkActive" runat="server"  /></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
                    <td><asp:checkbox ID="chkDisplay" runat="server"/></td>
                </tr>
                <tr>
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_HelpProdNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNote", "Note")%></td>
                    <td><HP3:Text runat="server" ID="txtNote" TypeControl="TextArea" Style = "width:600px"/></td>
                </tr>
                <tr runat="server" id="tr_txtLinkLabel">
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinklabel&Help=cms_HelpLinklabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinklabel", "Link label")%></td>
                    <td><HP3:Text runat="server" ID="txtLinkLabel" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>
                <tr runat="server" id="tr_txtLinkUrl">
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
                    <td><HP3:Text runat="server" ID="txtLinkUrl" TypeControl="Textbox" Style = "width:600px"/></td>
                </tr>                
            </table>
            <%--End Form Product Standard--%>
            
            <%--Form Product CVT--%>
                <%If isCVTProduct = True Then%>
                    <HP3:CVTProduct runat="server" ID="CVTProd" />
                <%End If%>
            <%--End Form Product CVT--%>
          </asp:Panel>
	</div>
</div>