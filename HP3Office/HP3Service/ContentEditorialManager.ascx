<%@ Control Language="VB" ClassName="ctlContentEditorialManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="ctlDate" Src ="~/hp3Office/HP3Parts/ctlDate.ascx"  %>

<script runat="server">
    Private oContentsEditorialManager As ContentEditorialManager
    Private oContentsEditorialSearcher As ContentEditorialSearcher
    Private oContentEditorialCollection As ContentEditorialCollection
    Private oGenericUtlity As New GenericUtility
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
    
    Private _sortType As ContentEditorialGenericComparer.SortType = ContentEditorialGenericComparer.SortType.ById
    Private _sortOrder As ContentEditorialGenericComparer.SortOrder = ContentEditorialGenericComparer.SortOrder.DESC
   
    Private strDomain As String
    Private strJs As String
    Private LastUrl As String = ""
    
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdContentEditorial() As Int32
        Get
            Return ViewState("SelectedIdContentEditorial")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdContentEditorial") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
       
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il ContentEditorialValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadCEValue() As ContentEditorialValue
        If SelectedIdContentEditorial <> 0 Then
            Return oGenericUtlity.GetContentEditorial(SelectedIdContentEditorial)
        End If
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oCTValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oCTValue As ContentEditorialValue)
        If Not oCTValue Is Nothing Then
            With oCTValue
              
                If .DataRif.HasValue Then
                    DataRif.Value = .DataRif.Value
                End If
                Description.Text = .Description
                email.Text = .EMail
                
                txtHideEditorialType.Text = .IdType
                EditorialType.Text = .DescriptionType
                ContentEditorialDescrizione.InnerText = " - " & .DescriptionType
                ContentEditorialId.InnerText = "Content Editorial ID: " & .Id
                LinkURL.Text = .LinkUrl
            End With
            
        End If
    End Sub
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                tdTypeEditorial.Visible = False
                LoadFormDett(LoadCEValue)
                BindComboType()
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContentEditorial)
                End If
            Case ControlType.Selection
                rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContentEditorial)
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        'Accedo alla pagina da un link esterno
        If Not Request("TypeControl") Is Nothing Then
            'Recupero l'aspetto che il controllo deve avere nonche l'id dell'item da editare
            TypeControl = Request("TypeControl")
            Dim objQs As New ObjQueryString
            
            SelectedIdContentEditorial = objQs.ContentId
            If SelectedIdContentEditorial = 0 Then
                NewItem = True
            Else
                NewItem = False
            End If
            If GenericDataContainer.Exists("LastUrl") Then
                LastUrl = GenericDataContainer.Item("LastUrl").ToString
            End If
            
        End If
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContentEditorialSearcher = Nothing)
        oContentsEditorialManager = New ContentEditorialManager
        oContentsEditorialManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New ContentEditorialSearcher
        End If
        
        oContentEditorialCollection = oContentsEditorialManager.Read(Searcher)
        If Not oContentEditorialCollection Is Nothing Then
            oContentEditorialCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = oContentEditorialCollection
        objGrid.DataBind()
                
    End Sub
    
          
    ''' <summary>
    ''' Caricamento della griglia dei temi
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
                
        oContentsEditorialManager = New ContentEditorialManager
               
        ReadSelectedItems()
        BindGrid(objGrid, oContentsEditorialSearcher)
    End Sub
    
    Sub SearchThemes(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListContentEditorial)
    End Sub
    
     
    ''' <summary>
    ''' Lettura delle descrizioni dei content editorial dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadContentTypeValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetContentEditorial(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
         
    Private Property SortType() As ContentEditorialGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContentTypeGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContentEditorialGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ContentEditorialGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ContentTypeGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ContentEditorialGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListContentEditorial.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litTheme")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ContentEditorialCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curContentEditorialValue As ContentEditorialValue
                    Dim outContentEditorialCollection As New ContentEditorialCollection
            
                    For Each str As String In CheckedItems
                        curContentEditorialValue = oGenericUtlity.GetContentEditorial(Int32.Parse(str))
                        If Not curContentEditorialValue Is Nothing Then
                            outContentEditorialCollection.Add(curContentEditorialValue)
                        End If
                    Next
            
                    Return outContentEditorialCollection
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        If LastUrl <> "" Then
            Response.Redirect(LastUrl)
            Return
        End If
        
        TypeControl = ControlType.View
        SelectedIdContentEditorial = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListContentEditorial)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ClearForm()
        Dim id As Int32 = s.commandargument
        btnSave.Text = "Update"
                
        SelectedIdContentEditorial = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""

        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                NewItem = False
                
                ShowRightPanel()
            Case "DeleteItem"
                Dim oContentEditorialManager As New ContentEditorialManager
                Dim oContentEditorialIdentificator As New ContentEditorialIdentificator
                oContentEditorialIdentificator.Id = SelectedIdContentEditorial
                oContentEditorialManager.Remove(oContentEditorialIdentificator)
                
                BindWithSearch(gridListContentEditorial)
        End Select
    End Sub
    
    ''' <summary>
    ''' Save/Update del content type
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oContentEditorialManager As New ContentEditorialManager
        Dim oContentEditorialValue As ContentEditorialValue = ReadFormValues()
        
        If NewItem Then
            oContentEditorialValue = oContentEditorialManager.Create(oContentEditorialValue)
        Else            
            oContentEditorialValue = oContentEditorialManager.Update(oContentEditorialValue)
        End If
        
        If oContentEditorialValue Is Nothing Then
            strJs = "alert('Error during saving')"
        Else
            strJs = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As ContentEditorialValue
        Dim oContentEditorialValue As New ContentEditorialValue
        
        With oContentEditorialValue
            .DataRif = DataRif.Value
            .Description = Description.Text
            .EMail = email.Text
            If txtHideEditorialType.Text <> "" Then
                .IdType = txtHideEditorialType.Text
                .DescriptionType = EditorialType.Text
            End If
            .Id = SelectedIdContentEditorial
            .LinkUrl = LinkURL.Text
        End With
        
        Return oContentEditorialValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo content editorial
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewContentEditorial(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedIdContentEditorial = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is ctlDate Then
                ctl.clear()
            End If
        Next
        
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momenti ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        ContentEditorialId.InnerText = ""
        ContentEditorialDescrizione.InnerText = "New Content Type"
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litTheme")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkCopy = e.Row.FindControl("lnkCopy")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListContentEditorial.PageIndex = 0
       
        Select Case e.SortExpression
            Case "Id"
                If SortType <> ContentEditorialGenericComparer.SortType.ById Then
                    SortType = ContentEditorialGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If                         
            Case "Description"
                If SortType <> ContentEditorialGenericComparer.SortType.ByDescription Then
                    SortType = ContentEditorialGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "DescriptionType"
                If SortType <> ContentEditorialGenericComparer.SortType.ByDescriptionType Then
                    SortType = ContentEditorialGenericComparer.SortType.ByDescriptionType
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
    
    Sub ShowForm(ByVal s As Object, ByVal e As EventArgs)                                       
        tdTypeEditorial.Visible = True
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
    
    ''' <summary>
    ''' Salvataggio di un Content Editorial
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Gest_SaveType(ByVal s As Object, ByVal e As EventArgs)
        If txtTypeEditorial.Text <> "" Then
            oContentsEditorialManager = New ContentEditorialManager
            Dim oContentEditorialtypeValue As New ContentEditorialTypeValue
            oContentEditorialtypeValue.Description = txtTypeEditorial.Text
            oContentsEditorialManager.Create(oContentEditorialtypeValue)
            
            BindComboType(oContentEditorialtypeValue.Id)
            tdTypeEditorial.Visible = True
            txtTypeEditorial.Text = ""
        End If
    End Sub
    
    ''' <summary>
    ''' Riempie il combo dei tipi
    ''' </summary>
    ''' <param name="newId"></param>
    ''' <remarks></remarks>
    Sub BindComboType(Optional ByVal newId As Int32 = 0)
        drbListType.Items.Clear()
        oContentsEditorialManager = New ContentEditorialManager
        'Dim idEditorial As New ContentEditorialIdentificator()
        Dim idEditorial As Integer = 0
        
        Dim oWebUtility As New WebUtility
        oWebUtility.LoadListControl(drbListType, oContentsEditorialManager.ReadType(idEditorial), "Description", "Id")
               
        If newId <> 0 Then
            For Each Item As ListItem In drbListType.Items
                If Item.Value = newId Then
                    drbListType.SelectedIndex = drbListType.Items.IndexOf(Item)
                    Exit For
                End If
            Next            
        End If
    End Sub
    
    
</script>
<script type="text/javascript" >  
    function SelectTypeEdit()
        {
            var txtDesc,txtId,cmb,oPnl
            cmb=document.getElementById ('<%=drbListType.clientid%>')
            txtId=document.getElementById ('<%=txtHideEditorialType.clientid%>')
            txtDesc=document.getElementById ('<%=EditorialType.clientid%>')
            oPnl =document.getElementById ('<%=tdTypeEditorial.clientid%>')
            txtDesc.value = cmb.options[cmb.selectedIndex].text;
            txtId.value = cmb.value;
            
            document.getElementById('divNewType').style.display='none'
            oPnl.style.display='none'
        }
             
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
        <%=strJs%>
</script>
    <asp:textbox id="txtHidden" runat="server" style="display:none"/>
    <hr />
<asp:Panel id="pnlGrid" runat="server">
    <table class="form">
        <tr id="rowToolbar" runat="server">
            <td  colspan="4">
                <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewContentEditorial" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
            </td>
        </tr>
    </table>
    <hr />
    
<%--LISTA CONTENTS EDITORIAL--%>
<asp:gridview ID="gridListContentEditorial" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"                
                AllowSorting="true"                
                PageSize ="20"               
                emptydatatext="No themes available"                                                          
                OnRowDataBound="gridRowDataBound" 
                OnPageIndexChanging="gridPageIndexChanging" 
                OnSorting="gridSorting"
                AlternatingRowStyle-BackColor="#e9eef4"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
              <Columns >
                 <asp:TemplateField>
                    <ItemTemplate>
                        <asp:radiobutton ID="chkSel" runat="server" />
                        <asp:literal ID="litTheme" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                    </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Id")%></ItemTemplate>
                </asp:TemplateField>               
                <asp:BoundField HeaderStyle-Width="60%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
                <asp:BoundField HeaderStyle-Width="30%" DataField ="DescriptionType" HeaderText ="Description Type" SortExpression="DescriptionType" />                
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton  ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Conform Delete?')"  OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>                        
                        <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageEditorialUpload.aspx?idEditorial=<%#DataBinder.Eval(Container.DataItem, "Id")%>','CoverImage','width=550,height=350')" style="cursor:pointer" title="Image Cover" alt="" />
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:Panel>
<%--FINE LISTA CONTENTS EDITORIAL--%>


<%--EDIT CONTENTS EDITORIAL--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnBack" runat="server" Text="Archive"  onclick="goBack" cssclass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" runat="server" Text="Save" onclick="Update" cssclass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="ContentEditorialId" class="title" runat ="server"/>
    <span id="ContentEditorialDescrizione" class="title" runat ="server"/>
    <hr />

    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpEditorialDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat="server" ID="Description" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpEditorialLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
            <td><HP3:Text runat="server" ID="LinkURL" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEmail&Help=cms_HelpEditorialEmail&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEmail", "Email")%></td>
            <td><HP3:Text runat ="server" ID="email" TypeControl ="textemail" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEditorialDataRif&Help=cms_HelpEditorialDataRif&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEditorialDataRif", "Data Rif")%></td>
            <td><HP3:ctlDate runat ="server" ID="DataRif" TypeControl ="JsCalendar" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEditorialType&Help=cms_HelpEditorialType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEditorialType", "Editorial Type")%></td>
            <td><HP3:Text runat ="server" ID="EditorialType" TypeControl ="TextBox"  style="width:200px" isReadOnly ="True"/> <asp:Button ID="NewType" runat="server" text="Select" OnClick="ShowForm" CssClass="button"/></td> 
            <td><asp:Textbox  id="txtHideEditorialType" runat="server" style="display:none"/></td>
        </tr>
     </table>
  </asp:Panel>
 
<asp:panel id="tdTypeEditorial" runat="server" visible ="false">   
  <table class="form">
       <tr>
            <td style="width:200px;"><strong><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTypeEditorial&Help=cms_HelpTypeEditorial&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTypeEditorial", "Type Editorial")%></strong></td>
            <td><asp:dropdownlist ID="drbListType" runat ="server" style="width:150px"/> </td>
            <td><input type="button" class="button" value = "Select" onclick="SelectTypeEdit()" /></td>
            <td><input type="button" class="button" value="Insert New Type" style="width:150px" onclick="document.getElementById('divNewType').style.display='';document.getElementById('<%=txtTypeEditorial.clientid%>').focus();document.getElementById('<%=txtTypeEditorial.clientid%>').value=''"/><br/></td>
       </tr>
   </table>
   
   <div id="divNewType" style="display:none">
        <table class="form">
            <tr>
                <td style="width:200px;"><strong><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNewEditorialType&Help=cms_HelpNewEditorialType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewEditorialType", "New Editorial Type")%></strong></td>
                <td><asp:textbox runat ="server" ID="txtTypeEditorial" TypeControl ="TextBox"  style="width:150px" /> <asp:button cssclass="button" ID="saveType" runat="server" Text ="Save Type" OnClick="Gest_SaveType"/></td>
           </tr>
      </table>
   </div>
</asp:panel> 
   
