<%@ Control Language="VB" ClassName="ContentCatalogObject"%>

<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagName="ctlLanguage" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"%>

<script runat="server">
    Private oLManager As New LanguageManager
    Private CatalogManager As New CatalogManager
    Private ContentManager As New ContentManager
    Private MasterPageManager As New MasterPageManager
    Private DictionaryManager As New DictionaryManager
    Private ThemeManager As New ThemeManager
    
    Private typeDefault As Integer = 1
    Private typeOther As Integer = 4
    
    Private _strDomain As String
    Private _language As String
    
    Private Title As String = ""
    Private IdLang As Int32 = 0
    Private IdSubType As ContentValue.Subtypes
    Private IdContent As Integer = 0
    Private PKContent As Integer = 0
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        If Not Page.IsPostBack Then
            Language() = MasterPageManager.GetLang.Id
            Domain() = WebUtility.MyPage(Me).DomainInfo
            
            ContentType.LoadControl()
            ReadCatalog()
            ReadRelations()
        End If
    End Sub
        
    'recupera i tipi per le relazioni
    'con DOMINIO 'LMS'
    Sub ReadRelationType()
        'VECCHIA IMPLEMENTAZIONE CON I TIPI DEFAULT E HP3
        '    Dim conRelTypeSearcher As ContentRelationTypeSearcher
        '    Dim contRelTypeColl As ContentRelationTypeCollection
        '    'tipo default
        
        '    conRelTypeSearcher = New ContentRelationTypeSearcher
        '    'conRelTypeSearcher.Description = "Default Relation"
        '    conRelTypeSearcher.Domain = "HP3"
        '    conRelTypeSearcher.Name = "default"
        
        '    contRelTypeColl = ContentManager.ReadContentRelationtType(conRelTypeSearcher)
        '    If Not (contRelTypeColl Is Nothing) AndAlso (contRelTypeColl.Count > 0) Then
        '        typeDefault = contRelTypeColl(0).Key.Id
        '    End If
        
        '    'tipo other
        '    conRelTypeSearcher = New ContentRelationTypeSearcher
        '    'conRelTypeSearcher.Description = "Other Relation"
        '    conRelTypeSearcher.Domain = "HP3"
        '    conRelTypeSearcher.Name = "other"
        '    contRelTypeColl = ContentManager.ReadContentRelationtType(conRelTypeSearcher)
        '    If Not (contRelTypeColl Is Nothing) AndAlso (contRelTypeColl.Count > 0) Then
        '        typeOther = contRelTypeColl(0).Key.Id
        '    End If
        
        Dim conRelTypeSearcher As ContentRelationTypeSearcher
        Dim contRelTypeColl As ContentRelationTypeCollection
        
        conRelTypeSearcher = New ContentRelationTypeSearcher
        'conRelTypeSearcher.Domain = "LMS"
        contRelTypeColl = ContentManager.ReadContentRelationtType(conRelTypeSearcher)
        
        Dim utility As New WebUtility
        utility.LoadListControl(dwlRelationType, contRelTypeColl, "Description", "Key.Id")
    End Sub
        
    'recupera il catalog object selezionato
    Sub ReadCatalog()
        Dim CValue As CatalogValue = CatalogManager.Read(New CatalogIdentificator(Request("Cat")))
        
        If Not CValue Is Nothing Then
            catalogObjType.Text = CValue.IdContentSubCategory
            lbCatalogObjectId.Text = CValue.Key.Id
            lblCatalogObjectDescr.Text = CValue.Title
        End If
    End Sub
    
    'recupera le relazioni content/catalog object
    Sub ReadRelations()
        'VECCHIA IMPLEMENTAZIONE
        '    Dim CatRelColl As New ContentRelationCollection
        '    Dim pk As Integer = Request("Cat")
        '    Dim ContRelSearcher As ContentRelationSearcher
        
        '    ContentManager.Cache = False
        '    ContRelSearcher = New ContentRelationSearcher
        '    ContRelSearcher.KeyContent.PrimaryKey = pk
        '    ContRelSearcher.KeyRelationType.Id = typeDefault
        '    CatRelColl = ContentManager.ReadContentRelation(ContRelSearcher)
                
        '    ContRelSearcher = New ContentRelationSearcher
        '    ContRelSearcher.KeyContent.PrimaryKey = pk
        '    ContRelSearcher.KeyRelationType.Id = typeOther
        '    Dim CatRelOtherColl As ContentRelationCollection = ContentManager.ReadContentRelation(ContRelSearcher)
             
        '    If Not (CatRelOtherColl Is Nothing) AndAlso (CatRelOtherColl.Count > 0) Then
        '        For Each value As ContentRelationValue In CatRelOtherColl
        '            CatRelColl.Add(value)
        '        Next
        '    End If
        
        '    gridRelations.DataSource() = CatRelColl
        '    gridRelations.DataBind()
        
        'NUOVA IMPLEMENTAZIONE
        Dim CatRelColl As New ContentRelationCollection
        Dim pk As Integer = Request("Cat")
        Dim ContRelSearcher As ContentRelationSearcher
        
        ContentManager.Cache = False
        ContRelSearcher = New ContentRelationSearcher
        ContRelSearcher.KeyContent.PrimaryKey = pk
        
        CatRelColl = ContentManager.ReadContentRelation(ContRelSearcher)
       
        gridRelations.DataSource() = CatRelColl
        gridRelations.DataBind()
    End Sub
    
    'visualizza il pannello delle relazioni prima di permettere di salvare una associazione
    Sub AddRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Add") Then
            ContRelated.Text = sender.commandArgument
            
            dwlRelationType.Items.Clear()
            ReadRelationType()
            '    If (ExistDefaultRelation()) Then
            '        dwlRelationType.Items.Insert(0, New ListItem("Other Relation", typeOther))
            '    Else
            '        dwlRelationType.Items.Insert(0, New ListItem("Default Relation", typeDefault))
            '        dwlRelationType.Items.Insert(1, New ListItem("Other Relation", typeOther))
            '    End If
            
            pnlRelatedSearch.Visible = False
            pnlGridContents.Visible = False
            pnl_Edit.Visible = True
        End If
    End Sub
    
    Sub SaveRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim CRValue As New CatalogRelationValue
        CRValue.KeyContent.PrimaryKey = Request("Cat")
        
        CRValue.KeyContentRelation.PrimaryKey = ContRelated.Text
        CRValue.KeyRelationType.Id = dwlRelationType.SelectedItem.Value
        
        Dim bool As Boolean = CatalogManager.CreateCatalogRelation(CRValue)
        ReadRelations()
            
        pnlRelatedSearch.Visible = True
        pnlGridContents.Visible = True
        pnl_Edit.Visible = False
    End Sub
    
    'elimina una relazione content/Catalog Object
    Sub RemoveRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Remove") Then
            Dim contRelPk As Integer = sender.commandArgument
            
            Dim CRValue As New CatalogRelationValue
            CRValue.KeyContent.PrimaryKey = Request("Cat")
            CRValue.KeyContentRelation.PrimaryKey = contRelPk
            
            CatalogManager.RemoveCatalogRelation(CRValue)
            ReadRelations()
        End If
    End Sub
    
    'Function ExistDefaultRelation() As Boolean
    '    Dim catRelSearcher As New CatalogRelationSearcher
        
    '    catRelSearcher.KeyContent.PrimaryKey = Request("Cat")
    '    'catRelSearcher.KeyContentRelated.PrimaryKey = ContRelated.Text
    '    catRelSearcher.KeyRelationType.Id = typeDefault
        
    '    CatalogManager.Cache = False
    '    Dim catColl As CatalogRelationCollection = CatalogManager.ReadCatalogRelation(catRelSearcher)
    '    If Not (catColl Is Nothing) AndAlso catColl.Count > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    
    'ricerca i content in base ai valori selezionati nel form
    Sub SearchContent(ByVal sender As Object, ByVal e As EventArgs)
        Dim contentSearcher As New ContentSearcher
       
        If (txtDataValueFilter.Text <> String.Empty) Then contentSearcher.key.Id = txtDataValueFilter.Text
        If (txtDataTextFilter.Text <> String.Empty) Then contentSearcher.SearchString = txtDataTextFilter.Text
       
        Dim ctc As ContentTypeCollection = ContentType.GetSelection()
        'se viene scelto il contentType dalla dwnList
        If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
            contentSearcher.KeyContentType.Id = ctc.Item(0).Key.Id
        End If
        
        Dim contentColl As ContentCollection = ContentManager.Read(contentSearcher)
        
        gridContents.DataSource() = contentColl
        gridContents.DataBind()
    End Sub
    
    'fa un reset del form di ricerca
    Sub ResetResearch(ByVal sender As Object, ByVal e As EventArgs)
        txtDataValueFilter.Text = Nothing
        txtDataTextFilter.Text = Nothing
        ContentType.SetSelectedIndex(0)
    End Sub
    
    'ritorna alla lista dei CORSI
    Sub GoCourseArchive(ByVal sender As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        Dim objQs As New ObjQueryString
      
        webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "LMS")
        
        Dim themeValue As ThemeValue = ThemeManager.Read(New ThemeIdentificator(Request("T")))
        If Not (themeValue Is Nothing) Then webRequest.KeycontentType = New ContentTypeIdentificator(themeValue.KeyContentType.Id)
                
        webRequest.customParam.append(objQs)
        Response.Redirect(MasterPageManager.FormatRequest(webRequest))
    End Sub
    
    'restituisce il tiop della relazione 
    Function getRelationType(ByVal contentRel As ContentRelationValue) As String
        Dim ContRelTypeSearcher As New ContentRelationTypeSearcher
        ContRelTypeSearcher.Key.Id = contentRel.KeyRelationType.Id
        
        Dim ContRelType As ContentRelationTypeCollection = ContentManager.ReadContentRelationtType(ContRelTypeSearcher)
        If Not (ContRelType Is Nothing) AndAlso (ContRelType.Count > 0) Then
            Return ContRelType(0).Description
        End If
        
        Return ""
    End Function
    
    'restituisce il Titolo del contenuto associato 
    Function getTitle(ByVal contentRel As ContentRelationValue) As String
        Dim content As ContentValue = ContentManager.Read(New ContentIdentificator(contentRel.KeyContentRelation.PrimaryKey))
        
        If Not (content Is Nothing) Then
            IdLang = content.Key.IdLanguage
            IdSubType = content.IdContentSubType
            IdContent = content.Key.Id
            PKContent = content.Key.PrimaryKey
            
            Return content.Title
        End If
        
        Return ""
    End Function
     
    'restitisce la lingua
    Function getLanguage(ByVal contentRel As ContentRelationValue) As String
        Dim content As ContentValue = ContentManager.Read(New ContentIdentificator(contentRel.KeyContentRelation.PrimaryKey))
       
        If Not (content Is Nothing) Then
            Return oLManager.GetCodeLanguage(content.Key.IdLanguage)
        End If
        Return ""
    End Function
        
    'restitisce il subtype
    Function getSubType(ByVal contentRel As ContentRelationValue) As String
        Dim content As ContentValue = ContentManager.Read(New ContentIdentificator(contentRel.KeyContentRelation.PrimaryKey))
       
        If Not (content Is Nothing) Then
            Return content.IdContentSubType.ToString()
        End If
        Return ""
    End Function
    
    'restitisce l'id del content
    Function getId(ByVal contentRel As ContentRelationValue) As Integer
        Dim content As ContentValue = ContentManager.Read(New ContentIdentificator(contentRel.KeyContentRelation.PrimaryKey))
       
        If Not (content Is Nothing) Then
            Return content.Key.Id
        End If
        Return Nothing
    End Function
    
    'restitisce la pk del content
    Function getPK(ByVal contentRel As ContentRelationValue) As Integer
        Dim content As ContentValue = ContentManager.Read(New ContentIdentificator(contentRel.KeyContentRelation.PrimaryKey))
       
        If Not (content Is Nothing) Then
            Return content.Key.PrimaryKey
        End If
        Return Nothing
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return DictionaryManager.Read(labelName, MasterPageManager.GetLang.Id, description)
    End Function
    
    
</script>

<script type="text/javascript">
</script>

<asp:Label id ="catalogObjType" runat="server" Visible="false"></asp:Label>
<asp:Label id ="ContRelated" runat="server" Visible="false"></asp:Label>

<hr />
<asp:Panel ID="pnlHeader" runat="server">
    <asp:Button ID="btBackToArchive" Text="Archive" CssClass="button" runat="server" OnClick="GoCourseArchive" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
</asp:Panel>
<hr />

<asp:Panel ID="pnlItem" runat="server">
    <asp:Panel ID="QuestSummary" Width="100%" runat="server">
        <div class="title">Item Selected</div>
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:30%">Description</th>
            </tr>
            <tr>
                <td><asp:Label id="lbCatalogObjectId" runat="server" /></td>
                <td><asp:Label id="lblCatalogObjectDescr" runat="server" /></td>
           </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="true" runat="server">
            <div class="title">Active Relations</div>
            <asp:GridView ID="gridRelations"
                AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
                ShowHeader="true" HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" width="100%"
                runat="server" >
                <Columns>
                     <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="Type <%#getSubType(Container.DataItem)%>" src="/HP3Office/HP3Image/Ico/type_<%#getSubType(Container.DataItem)%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblId" runat="server" Text='<%#getId(Container.DataItem)%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Pk" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Literal ID="lblPk" runat="server" Text='<%#getPK(Container.DataItem)%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#getLanguage(Container.DataItem)%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#getTitle(Container.DataItem)%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Literal ID="lblRelType" runat="server" Text='<%#getRelationType(Container.DataItem)%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="RemoveRelation" CommandArgument ='<%#Container.Dataitem.KeyContentRelation.PrimaryKey%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <%--Pannello per l'edit di una relazione  --%>  
    <asp:Panel ID="pnl_Edit" Width="100%" runat="server" Visible="false">
    <asp:Button id ="btn_SaveRelation" runat="server" Text="Save" OnClick="SaveRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
        <div class="title" style="margin-bottom:20px">Relation Type</div>
        <table class="form" width="99%">
              <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Relation Type")%></td>
                <td><asp:DropDownList id="dwlRelationType" runat="server"/></td>
             </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
            </tr>
        </table>
        <asp:Table ID="tableSearchParameters" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>Id</asp:TableCell>
                <asp:TableCell>Title</asp:TableCell>
                <asp:TableCell>Content Type</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></asp:TableCell>
                <asp:TableCell><HP3:ctlContentType ID="ContentType" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchParameters2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchRelated" Enabled="true" OnClick="SearchContent" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
                <asp:TableCell><asp:Button ID="btnClearSearch" Enabled="true"  OnClick="ResetResearch" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <hr />
   
    <asp:Panel ID="pnlGridContents" Width="100%" Visible="true" runat="server">
    <asp:Label runat="server" ID="msg"/>
        <asp:GridView ID="gridContents" 
            AutoGenerateColumns="false"
            AllowPaging="true" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager" PageSize="20"
            AllowSorting="false"
            ShowHeader="true" HeaderStyle-CssClass="header"
            ShowFooter="true" FooterStyle-CssClass="gridFooter"
            GridLines="None" AlternatingRowStyle-BackColor="#eeefef" width="100%"
            runat="server">
            <Columns>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="Language: <%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <asp:ImageButton ID="btn_add" ToolTip="Add content relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/insert.gif" onClick="AddRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
 </asp:Panel>   