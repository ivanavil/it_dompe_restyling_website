<%@ Control Language="VB" ClassName="ContentService" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.USer.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Forms.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing" %>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Forms" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility" %>

<%@ Register TagName="SearchEngine" TagPrefix="HP3" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="GridContent" TagPrefix="HP3" Src="~/hp3Office/HP3Common/GridContent.ascx"  %>

<script runat="server">
    Private oGenericUtility As New GenericUtility
    Private versionManager As New VersioningManager
    Private webRequest As New WebRequestValue
    Private objQs As New ObjQueryString
    Private iTheme As ThemeIdentificator
    
    Private SiteFolder As String
    Private strJS As String
        
    Public isNew As Boolean
    Public idContent As Integer
    Public idContentType As Integer
    
    Public CTypeManager As New ContentTypeManager
    Public CTypeValue As New ContentTypeValue
    
    Sub Page_Load()
        isNew = WebUtility.MyPage(Me).isNew
        idContent = objQs.ContentId
        idContentType = objQs.ContentType_Id
        'controllo se il content da visualizzare � una versione
        'o il content attuale
        Dim versionId As String = ""
        webRequest.customParam.LoadQueryString()
       
       
        versionId = webRequest.customParam.item("V")
        'se � una versione
        If (versionId <> String.Empty) Then
            DisableButton()
        End If
                   
        Try
            'Nessun content 
            If idContent = 0 And Not isNew Then
                'mostro il motore di ricerca
                SearchEngine.Visible = True
            
                GestButton("btnSalva", False, "")
            Else
                'Carico le pagine Master(contentsDetails) e Slave(in base al tipo di template
                Dim strQSParam As String = WebUtility.MyPage(Me).idThemes
        
                SiteFolder = WebUtility.MyPage(Me).SiteFolder
                iTheme = New ThemeIdentificator(Int32.Parse(IIf(strQSParam = "", 0, strQSParam)))
        
                LoadRightAscx(MasterPage, GetMasterPage())
                If SlavePage.Controls.Count > 0 Then SlavePage.Controls.Clear()
                LoadRightAscx(SlavePage, GetSlavePage)
            End If
        Catch ex As Exception
            Response.Write("ContentService<br/>" & ex.ToString.Replace("'", "''"))
        End Try
        
    End Sub
    
    ''' <summary>
    ''' Funzione chiamata dalla pagina master
    ''' Necessaria per gestire lo stato dei bottoni    
    ''' </summary>
    ''' <param name="strButton"></param>
    ''' <param name="visibility"></param>
    ''' <param name="Text"></param>
    ''' <remarks></remarks>
    Public Sub GestButton(ByVal strButton As String, ByVal visibility As Boolean, ByVal Text As String)
        Dim ctlBtn As Button = FindControl(strButton)
        If Not ctlBtn Is Nothing Then
            ctlBtn.Visible = visibility
            ctlBtn.Text = Text
        End If
    End Sub
    
    Function GetMasterPage() As String
        Try
            If GetContentTemplateValue.KeyCMSMaster.Id > 0 Then
                CTypeValue = CTypeManager.Read(GetContentTemplateValue.KeyCMSMaster.Id)
                If Not CTypeValue Is Nothing Then
                    Return CTypeValue.Page
                End If
            End If
        Catch ex As Exception
            Response.Write("ContentService.ascx<br/>" & ex.Message)
        End Try
        Return Nothing
    End Function
    
    Function GetSlavePage() As String
        Try
            If GetContentTemplateValue.KeyCMSSlave.Id > 0 Then
                CTypeValue = CTypeManager.Read(GetContentTemplateValue.KeyCMSSlave.Id)
                If Not CTypeValue Is Nothing Then
                    Return CTypeValue.Page
                End If
            End If
        Catch ex As Exception
            Response.Write("ContentService.ascx<br/>" & ex.Message)
        End Try
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Ritorna le informazioni sul template utilizzato per il servizio
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetContentTemplateValue() As ContentTemplateValue
        Dim CTypeManager As New ContentTypeManager
        Dim CTemplateManger As New ContentTemplateManager
        Dim CTypeValue As New ContentTypeValue
        Dim CTemplateValue As New ContentTemplateValue
        
        CTypeValue = CTypeManager.Read(idContentType)
        If CTypeValue.KeyContentsTemplate.Id > 0 Then
            CTemplateValue = CTemplateManger.Read(CTypeValue.KeyContentsTemplate.Id)
        End If
        Return CTemplateValue
    End Function
    
    Function LoadRightAscx(ByVal plcObject As PlaceHolder, ByVal strPage As String) As Boolean
        Try
            If strPage = String.Empty Then Return False
           
            Dim obj As Object = LoadControl("~/" & SiteFolder + "/" + strPage)
            
            If Not obj Is Nothing Then
                plcObject.Controls.Add(obj)
            End If
            
            Return True
        Catch ex As Exception
            Response.Write("ContentService.ascx<br/>" & ex.Message)
            Return False
        End Try
    End Function
    
    Sub Back(ByVal s As Object, ByVal e As EventArgs)
        Response.Redirect(GetBackUrl)
    End Sub
    
    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue
        
        objQs.IsNew = 0
        objQs.ContentId = 0
        webRequest.customParam.append("page", CType(Request("page"), Integer))
        webRequest.customParam.append(objQs)
        
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    Sub New_Content(ByVal s As Object, ByVal e As EventArgs)
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue
        
        objQs.IsNew = 1
        objQs.Language_id = 0
        objQs.ContentId = 0
        
        webRequest.customParam.append(objQs)
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
    
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
        
    ''' <summary>
    ''' Effettua il salvataggio degli oggetti presenti nel placeholder master e slave.
    ''' Da qui parte tutto il processo.
    ''' La pagina master cerca di salvare i dati e li bufferizza in un oggetto di dipo ContentStoreValue
    ''' Se per� esiste un controllo slave, � questo che si prende carico del salvataggio anche del content da cui
    ''' eredita    
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Save(ByVal s As Object, ByVal e As EventArgs)
        
        If MasterPage.Controls.Count > 0 Then
            Try
                'Recupero il controllo dal placeHolder
                Dim ctlMaster As Object = MasterPage.Controls(0)
                                                
                'Recupero il buffer
                'Se non esiste alcun controllo slave, il controllo Master effettua il salvataggio vero e proprio
                Dim outType As Object = ctlMaster.Save()
               
                'Se esiste il controllo slave
                If ExistSlave Then
                    'Viene delegato del salvataggio
                    outType = CType(SlavePage.Controls(0), Object).save(outType)
                End If
                
                'Gestione dei valori di ritorno
                If Not outType Is Nothing Then
                    ContentCheckIn(outType.Content.Key.PrimaryKey)
                    'Response.Write(outType.ContentVersion Is Nothing)
                    
                    ContentVersioning(outType.ContentVersion)
                  
                    ContentIndex(outType.Content.Key)
                    ContentExtraFielads(outType.Content.Key.PrimaryKey)
                    CreateJsMessage("Content saved!")
                    strJS += vbCrLf + "location.href='" + GetBackUrl() + "'"
                Else
                    CreateJsMessage("Error! Content not saved")
                End If
                
            Catch ex As Exception
                Response.Write(ex.ToString)
            End Try
        End If
    End Sub
    
    ''' <summary>
    ''' Consente alla pagina master di accertarsi dell' esistenza della pagina slave
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ExistSlave() As Boolean
        Get
            Return SlavePage.Controls.Count = 1
        End Get
    End Property
    
    Public Sub ShowSlave(ByVal bVisible As Boolean)
        If ExistSlave Then
            SlavePage.Controls(0).Visible = bVisible
        End If
    End Sub
    
    Public Sub LoadMasterPage(ByVal oContentValue As ContentValue, ByRef isNew As Boolean)
        If MasterPage.Controls.Count > 0 Then
            CType(MasterPage.Controls(0), Object).LoadControl(oContentValue, isNew)
        End If
    End Sub
    
    Sub RowOperation(ByVal s As Object, ByVal e As EventArgs)
        Dim arr() As String = s.CommandArgument.split("_")
        Dim id As Int32
        Dim idLanguage As Int32
        If arr.Length > 1 Then
            id = arr(0)
            idLanguage = arr(1)
        End If
                
        Select Case s.CommandName
            Case "UpdateRow"
                Dim webRequest As New WebRequestValue
                Dim mPageManager As New MasterPageManager
                objQs.IsNew = 0
                objQs.Language_id = idLanguage
                objQs.ContentId = id
                
                webRequest.customParam.append("page", CType(Request("page"), Integer))
                webRequest.customParam.append(objQs)
                
                'effettua il check-out silente
                Dim contentManager As New ContentManager
                'Response.Write(id & idLanguage & )
                'Response.End()
                ContentCheckOut(contentManager.ReadPrimaryKey(id, idLanguage))
                
                Response.Redirect(mPageManager.FormatRequest(webRequest))
            Case "NewRow"
                Dim webRequest As New WebRequestValue
                Dim mPageManager As New MasterPageManager
                objQs.IsNew = 1
                objQs.Language_id = idLanguage
                objQs.ContentId = id
                webRequest.customParam.append(objQs)
                Response.Redirect(mPageManager.FormatRequest(webRequest))
            Case "LogicalDelete"
                Dim oContentManager As New ContentManager
                Dim contIdent As New ContentIdentificator()
                contIdent.PrimaryKey = oContentManager.ReadPrimaryKey(id, idLanguage)
                
                oContentManager.Delete(New ContentIdentificator(id, idLanguage))
                'deindicizzazione
                ContentDeIndex(contIdent)
                
                SearchEngine.doSearch()
            Case "LogicalRestore"
                Dim oContentManager As New ContentManager
                Dim contIdent As New ContentIdentificator()
                contIdent.PrimaryKey = oContentManager.ReadPrimaryKey(id, idLanguage)
                
                oContentManager.Update(contIdent, "Delete", "0")
                'indicizzazione
                ContentIndex(contIdent)
                SearchEngine.doSearch()
            Case "SearchRefresh"
                SearchEngine.doSearch()
        End Select
    End Sub
    
    
    'effettua il check-out di un content
    'data la sua primaryKey
    Sub ContentCheckOut(ByVal pkContent As Integer)
        If Me.BusinessStatusManager.IsEditCheckOut(pkContent) Then
            Exit Sub
        End If
        Dim contentEdit As New ContentEditStatusValue
        contentEdit.EditStatus = ContentEditStatusValue.ContentEditStatus.CheckOut
        contentEdit.KeyContent.PrimaryKey = pkContent
        contentEdit.KeyUser.Id = Me.BusinessMasterPageManager.GetTicket.User.Key.Id
        Me.BusinessStatusManager.Create(contentEdit)
    End Sub
    
    'effettua il check-in di un content
    'data la sua primaryKey
    Sub ContentCheckIn(ByVal pkContent As Integer)
        If Me.BusinessStatusManager.IsEditCheckOut(pkContent) Then
            Dim contentEdit As New ContentEditStatusValue
            contentEdit.EditStatus = ContentEditStatusValue.ContentEditStatus.CheckIn
            contentEdit.KeyContent.PrimaryKey = pkContent
            contentEdit.KeyUser.Id = Me.BusinessMasterPageManager.GetTicket.User.Key.Id
            Me.BusinessStatusManager.Create(contentEdit)
        End If
    End Sub
      
    'si occupa di creare una nuova versione del content
    Sub ContentVersioning(ByVal obj As Object)
        Dim versionValue As New VersionValue
        'Response.Write(obj Is Nothing)
        
        If Not (obj Is Nothing) Then
            'Response.Write(obj Is Nothing)
          
            versionValue.KeyContent.PrimaryKey = obj.Key.PrimaryKey
            versionValue.KeyUser.Id = Me.BusinessAccessService.GetTicket.User.Key.Id
            versionValue.DateInsert = Date.Now
            versionValue.StoredObject = obj
        
            Dim vo As VersionValue = versionManager.Create(versionValue)
          
        End If
        'Response.End()
    End Sub
    
    'gestisce i campi extrafields
    Sub ContentExtraFielads(ByVal pkContent As Integer)
        'Recupero il controllo dal placeHolder
        Dim ctlMaster As Object = MasterPage.Controls(0)
        ctlMaster.SaveExtraFields(pkContent)
       
    End Sub
    
    'si occupa della indicizzazione dei contenuti
    Sub ContentIndex(ByVal contentIdent As ContentIdentificator)
        Try
            Dim idSite As Integer = Request("S")
            'Dim index As IndexerModule = IndexerModule.GetInstance
            Dim index As New IndexerModule()
            Dim PositionV As New PositionValue
        
            PositionV.Site = New SiteIdentificator(idSite)
            PositionV.Language = New LanguageIdentificator(contentIdent.IdLanguage)
        
            index.AddContent(contentIdent, PositionV)
        Catch ex As Exception
        End Try
    End Sub
    
    'si occupa della deindicizzazione dei contenuti
    Sub ContentDeIndex(ByVal contentIdent As ContentIdentificator)
        'Dim index As IndexerModule = IndexerModule.GetInstance
        Dim index As New IndexerModule()
        Dim idSite As Integer = Request("S")
        Dim PositionV As New PositionValue
        
        PositionV.Site = New SiteIdentificator(idSite)
        PositionV.Language = New LanguageIdentificator(contentIdent.IdLanguage)
        index.RemoveContent(contentIdent, PositionV)
        
    End Sub
    
    Sub DisableButton()
        'btnNew.Enabled = False
        btnSalva.Enabled = False
    End Sub
    
    Public Sub EnableButton()
        'btnNew.Enabled = False
        btnSalva.Enabled = True
    End Sub
    
    </script>
<script type ="text/javascript" >
    <%=strJS%>
</script>
<div style="margin-top:10px; margin-bottom:10px">
    <asp:Button ID ="btnNew" runat="server" Text="New" OnClick="New_Content"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button ID ="btnLista" runat="server" Text="Archive" OnClick="Back" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button ID ="btnSalva" runat="server" Text="Save" OnClick="Save" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
</div>
<HP3:SearchEngine ID="SearchEngine" runat="server" visible="false" onUpdateEvent="RowOperation" />
<br/>

<asp:PlaceHolder ID="MasterPage" runat="server" />
<asp:PlaceHolder ID="SlavePage" runat="server" />
