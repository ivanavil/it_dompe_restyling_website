<%@ Control Language="VB" ClassName="ctlProfileManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx" %>


<script runat="server">
    Private oCManager As ProfilingManager
    Private oSearcher As ProfileSearcher
    Private oCollection As ProfileCollection
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    
    Private _typecontrol As ControlType = ControlType.View
    Private _sortType As ProfileGenericComparer.SortType
    Private _sortOrder As ProfileGenericComparer.SortOrder
    
    Private oCollectionSiteArea As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    
    Private utility As New WebUtility
    
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
        
    End Property
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
    End Sub

    Function LoadValue() As ProfileValue
        If SelectedId <> 0 Then
            oCManager = New ProfilingManager
            oSearcher = New ProfileSearcher
            oSearcher.Active = SelectOperation.All
            oSearcher.Delete = SelectOperation.All
            oSearcher.Key.Id = SelectedId
            oCManager.Cache = False
            Return oCManager.Read(oSearcher)(0)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente il profilo selezionato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New ProfilingManager
            Dim key As New ProfileIdentificator(SelectedId)
            ret = oCManager.Remove(key)
        End If
        Return ret
    End Function
    
    ''' <summary>
    ''' Recupera i ruoli collegati alla sitearea
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetProfileRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        oCManager = New ProfilingManager
        Dim key As New ProfileIdentificator(id)
        oCManager.Cache = False
        Return oCManager.ReadRoles(key)
    End Function
    
    Sub LoadFormDett(ByVal oValue As ProfileValue)
        
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                keyDomain.Text = .Key.Domain
                keyName.Text = .Key.Name
                Description.Text = .Description
                ctlRoles.GenericCollection = GetProfileRolesValue(.Key.Id)
                ctlRoles.LoadControl()
                Delete.Checked = .Delete
                Active.Checked = .Active
            End With
        Else
            ctlRoles.LoadControl()
            lblDescrizione.InnerText = "New Profile"
            lblId.InnerText = ""
            keyDomain.Text = Nothing
            keyName.Text = Nothing
            Description.Text = Nothing
            Delete.Checked = False
            Active.Checked = True
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New ProfileValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Key.Domain = keyDomain.Text
            .Key.Name = keyName.Text
            .Description = Description.Text
            .Delete = Delete.Checked
            .Active = active.checked
        End With

        oCManager = New ProfilingManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If
       
        If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
            Dim Rc As New Object
            Rc = ctlRoles.GetSelection
            If oCManager.RemoveAllProfileRoles(oValue.Key) Then
                oCManager.CreateProfileRole(oValue.Key, Rc)
            End If
        End If
        
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollectionSiteArea = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollectionSiteArea = siteAreaManager.Read(siteAreaSearcher)
        oCollectionSiteArea = oCollectionSiteArea.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollectionSiteArea, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub Page_load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        BindDomains()
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ProfileSearcher = Nothing)
        oCManager = New ProfilingManager
        oCollection = New ProfileCollection
        
        If Searcher Is Nothing Then
            Searcher = New ProfileSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New ProfileSearcher
        oSearcher.Delete = SelectOperation.All
        oSearcher.Active = SelectOperation.All
        If txtId.Text <> "" Then oSearcher.Key = New ProfileIdentificator(txtId.Text)
        If txtDescription.Text <> "" Then oSearcher.SearchString = txtDescription.Text
        If dwlDomain.SelectedItem.Value <> "-1" Then oSearcher.Key.Domain = dwlDomain.SelectedItem.Value

        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> ProfileGenericComparer.SortType.ById Then
                    SortType = ProfileGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> ProfileGenericComparer.SortType.ByDescription Then
                    SortType = ProfileGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> ProfileGenericComparer.SortType.ByKey Then
                    SortType = ProfileGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As ProfileGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ProfileGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ProfileGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ProfileGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ProfileGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ProfileGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        'Dim ltl As Literal
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        'ltl = e.Row.FindControl("litBanner")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid" runat="server">
    <asp:button ID="btnNew" runat="server" CssClass="button" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
            <table class="topContentSearcher">
                <tr>
                    <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                    <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Profile Searcher</span></td>
                </tr>
            </table>
            <fieldset class="_Search">
                <table class="form">
                    <tr>
                        <td><strong>ID</strong></td>    
                        <td><strong>Descrizione</strong></td>
                        <td><strong>Domain</strong></td>
                        <td><strong></strong></td>
                        <td><strong></strong></td>
                        <td><strong></strong></td>
                    </tr>
                    <tr>
                        <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:100px"/></td>
                        <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:150px"/></td>  
                        <td><asp:DropDownList id="dwlDomain" runat="server"/></td>  
                        <td></td>  
                        <td></td>  
                    </tr>
                    <tr>
                       <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                    </tr>
                </table>
            </fieldset>
            <hr />
      </asp:Panel>
    
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"                            
                    AllowSorting="true"
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridList_Sorting"
                    emptydatatext="No item available"                
                    OnRowDataBound="gridList_RowDataBound"
                    >
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Identificator" SortExpression="Key">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
                    </asp:TemplateField>  
                    <asp:BoundField HeaderStyle-Width="50%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />
                    <asp:BoundField HeaderStyle-Width="7%" DataField ="Active" HeaderText ="Active" />
                    <asp:BoundField HeaderStyle-Width="7%" DataField ="Delete" HeaderText ="Delete" />
                    <asp:TemplateField HeaderStyle-Width="6%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" CssClass="button" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button runat="server" CssClass="button" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/><br/><br />
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat ="server" ID="keyDomain" TypeControl ="TextBox" style="width:90px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_FormIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat ="server" ID="keyName" TypeControl ="TextBox" style="width:90px" MaxLength="10"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpProfileDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" ID="ctlRoles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDelete&Help=cms_HelpDelete&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDelete", "Delete")%></td>
            <td><asp:checkbox ID="Delete" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="Active" runat="server" /></td>
        </tr>
     </table>
</asp:Panel>