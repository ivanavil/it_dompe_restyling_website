<%@ Control Language="VB" ClassName="ctlProvaPPT" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>

<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Aspose.Slides" %>
<%@ Import Namespace="system.IO" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Graphics" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Drawing.Imaging" %>

<script runat="server">
    Dim sourcepath As String = SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.DownloadPath & "\" & "200442.ppt"
    Dim finalpath As String = ConfigurationsManager.DownloadPath & "\" & "PPT"
    
    Private pk As String = 0
    Private imagePath As String = ""
    
    Sub Page_Load()
        If (Request("Cat") <> "") Then
            pk = Request("Cat")
            imagePath = "/" & ConfigurationsManager.DownloadPath & "/PPT/" & pk
            LoadSlidesList()
        End If
    End Sub
       
    Sub Convert()
        Dim lic As Aspose.Slides.License = New Aspose.Slides.License()
        lic.SetLicense(Context.Server.MapPath("~\bin\Aspose.Slides.lic"))

        Dim pres As New Presentation(sourcepath)
        'All slides
        Dim oSlides As Slides = pres.Slides
        Dim slide As Slide
        
        Dim image As System.Drawing.Image = Nothing
        Dim MemStream As New MemoryStream()
        For i As Integer = 1 To oSlides.Count - 1
            slide = oSlides.Item(i)
            image = slide.GetThumbnail(New Drawing.Size(300, 200))
            Dim image2 As System.Drawing.Image = image
            'Response.Write(Server.MapPath(finalpath & "\" & i & ".jpg"))
            'image.Save(SystemUtility.CurrentPhysicalApplicationPath & "\" & finalpath & "\" & i & ".jpg", ImageFormat.Jpeg)
            Dim pathImg As String = SystemUtility.CurrentMapPath("/" & ConfigurationsManager.DownloadPath & "/PPT/" & i & ".jpg")
            image.Save(pathImg, ImageFormat.Jpeg)
        Next

        'Dim oDir As New System.IO.DirectoryInfo(SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.DownloadPath & "\" & "PPT")
        'If Not oDir.GetFiles("*.jpg") Is Nothing Then
        
        'For Each obj As FileInfo In oDir.GetFiles("*.jpg")
        '    Response.Write("<img src=""" & finalpath & "/" & obj.ToString & """><hr />")
        'Next
        'End If
        
        For i As Integer = 1 To oSlides.Count - 1
            'Response.Write(Me.ObjectSiteDomain.Domain)
            Response.Write("<img src=""http://" & Me.ObjectSiteDomain.Domain & "/" & ConfigurationsManager.DownloadPath & "/PPT/" & i & ".jpg" & """><hr />")
        Next
    End Sub
    
    Sub SaveImgSlides()
        Dim lic As Aspose.Slides.License = New Aspose.Slides.License()
        lic.SetLicense(Context.Server.MapPath("~\bin\Aspose.Slides.lic"))

        Dim pres As New Presentation(sourcepath)
        'All slides
        Dim oSlides As Slides = pres.Slides
        Dim slide As Slide
        
        Dim image As System.Drawing.Image = Nothing
        Dim image2 As System.Drawing.Image = Nothing
        
        For i As Integer = 1 To oSlides.Count - 1
            slide = oSlides.Item(i)
            image = slide.GetThumbnail(New Drawing.Size(500, 400))
            image2 = slide.GetThumbnail(New Drawing.Size(200, 150))
            'Response.Write(Server.MapPath(finalpath & "\" & i & ".jpg"))
            'image.Save(SystemUtility.CurrentPhysicalApplicationPath & "\" & finalpath & "\" & i & ".jpg", ImageFormat.Jpeg)
            Dim pathImg As String = SystemUtility.CurrentMapPath("/" & ConfigurationsManager.DownloadPath & "/PPT/" & i & "_b.jpg")
            Dim pathImg1 As String = SystemUtility.CurrentMapPath("/" & ConfigurationsManager.DownloadPath & "/PPT/" & i & ".jpg")
            image.Save(pathImg, ImageFormat.Jpeg)
            image2.Save(pathImg1, ImageFormat.Jpeg)
        Next
    End Sub
    
    
    Sub ConvertToPdf()
        Dim pres As New Presentation(sourcepath)
        pres.SaveToPdf(SystemUtility.CurrentPhysicalApplicationPath & "\" & finalpath & "\" & "prova.pdf")
    End Sub
    
    Function isFile(ByVal Path As String) As Boolean
        Try
            Return IO.File.Exists(Path)
        Catch ex As Exception
            Return False
        End Try
    End Function
    
    Sub LoadSlidesList()
        rpSlides.DataSource = LoadSlides()
        rpSlides.DataBind()
        
        repIndex.DataSource = LoadSlides()
        repIndex.DataBind()
    End Sub
    
    Function LoadSlides() As ArrayList
        Dim file As String = pk
        Dim sourcepath2 As String = SystemUtility.CurrentMapPath(imagePath & "/" & file & ".ppt")
        
        Dim Ar As New ArrayList
        Dim pres As New Presentation(sourcepath2)
        Dim oSlides As Slides = pres.Slides
      
        If Not (oSlides Is Nothing) AndAlso (oSlides.Count > 0) Then
            For i As Integer = 1 To oSlides.Count - 1
                Ar.Add(i)
            Next
        End If
        
        Return Ar
    End Function
    
</script>
<script type="text/javascript" language="javascript" src="/Js/jquery.js"></script>
<style type="text/css">
    .border{ border:4px solid #336699;}
</style>
<script type="text/javascript">
   /*function showSlide(id){ 
       document.getElementById('imgSlide').src = "http://hp3office.healthware.it/HP3Download/PPT/"+ id + "_b.jpg";
        document.getElementById('labIndex').setAttribute("visible",true);
    }*/
    
    $(document).ready(function(){
        $(".thumb").click(function(){
            var currentTime = new Date();
            var image;
            image = '<%=imagePath %>' + "/";
            $(".thumb").removeClass("border")
            $(this).addClass("border");
            $("#imgSlide").attr("src", image + $(this).attr("id") + "_b.jpg" +  "?data=" + currentTime);
           
            $("#imgSlide").show();
        })
    })
    
</script>
<%--<img src='<%#"/HP3Download/PPT/" & Container.DataItem & ".jpg"%>' alt="" class="thumb" id="<%#Container.DataItem %>" >--%>
<asp:Panel id="showPresentation" runat="server">
<div id="divSlides" style="background-color:#E2E2E2;width:26%;height:500px;overflow:scroll;float:left">
    <asp:Repeater id="rpSlides" runat="server">
	      <HeaderTemplate></HeaderTemplate>
	      <ItemTemplate>
	            <div style="color:#336699;font-weight:bold;margin-left:4px;float:left"><%#Container.DataItem %> </div><div style="margin-top:5px;margin-left:18px"> <img src='<%#imagePath & "/" & Container.DataItem & ".jpg" & "?data=" & Date.Now()%>' alt="" class="thumb" id="<%#Container.DataItem %>" ></div>
	     </ItemTemplate>
	</asp:Repeater>
</div>

<div id="divSlide" style="background-color:#E2E2E2;width:65%;height:450px; margin-left:33%; margin-bottom:10px;">
    <div style="text-align:center;vertical-align:middle"><img id="imgSlide" style="margin-top:25px;margin-bottom:25px;display:none" src ="" alt="" /></div>
</div>

<div style="text-align:right; margin-right:20px">
    <asp:Repeater id="repIndex" runat="server">
	     <HeaderTemplate></HeaderTemplate>
	     <ItemTemplate>
         	<%--<a href="javascript:showSlide('<%#Container.DataItem%>')"><label id="labIndex" visible="false"><%#Container.DataItem %></label></a>--%>
	     </ItemTemplate>
    </asp:Repeater>
</div>
</asp:Panel> 
 
	
