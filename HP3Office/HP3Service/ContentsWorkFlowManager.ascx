<%@ Control Language="VB" ClassName="ContentsWorkFlowManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private stepSearcher As StepSearcher
    Private wfSearcher As WorkFlowSearcher
    Private workFlowManager As New WorkFlowManager
    
    Private strName As String
    Private siteLabel As String
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    Private _userType As Integer = -1
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property StepUserId() As Integer
        Get
            Return ViewState("stepId")
        End Get
        Set(ByVal value As Integer)
            ViewState("stepId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property UserType() As Integer
        Get
            Return _userType
        End Get
        Set(ByVal value As Integer)
            _userType = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo()
               
        If Not (Page.IsPostBack) Then
            If Me.BusinessMasterPageManager.GetContent.Id = 0 Then
                BindComboWorkFlow()
                ActiveWFPanelGrid()
                LoadWFArchive()
            End If
        End If
    End Sub
   
    'recupera la lista degli step di un workflow
    Sub LoadArchive()
        stepSearcher = New StepSearcher
        
        workFlowManager.Cache = False
        Dim stepColl As StepCollection = workFlowManager.Read(stepSearcher)
       
        gdw_stepList.DataSource = stepColl
        gdw_stepList.DataBind()
        
        dwlWflow.SelectedIndex = 0
    End Sub
    
    'recupera la lista dei workflow
    Sub LoadWFArchive()
        wfSearcher = New WorkFlowSearcher
        
        workFlowManager.Cache = False
        Dim wfColl As WorkFlowCollection = workFlowManager.Read(wfSearcher)
       
        grid_workflows.DataSource = wfColl
        grid_workflows.DataBind()
    End Sub
    
    'modifica un singolo step 
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim stepIdent As New StepIdentificator
                      
        If sender.CommandName = "SelectItem" Then
            SelectedId = sender.CommandArgument
            
            workFlowManager.Cache = False
            stepIdent.Id = SelectedId
            
            Dim oStepValue As StepValue = workFlowManager.Read(stepIdent)
       
            If Not oStepValue Is Nothing Then
                LoadDett(oStepValue)
                ActivePanelDett()
            End If
        End If
    End Sub
    
    'modifica un singolo workflow
    Sub EditWorkFlow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim workfIdent As New WorkFlowIdentificator
                      
        If sender.CommandName = "SelectItem" Then
            SelectedId = sender.CommandArgument
            
            workFlowManager.Cache = False
            workfIdent.Id = SelectedId
            
            Dim oWFValue As WorkFlowValue = workFlowManager.Read(workfIdent)
       
            If Not oWFValue Is Nothing Then
                LoadWFDett(oWFValue)
                ActiveWFPanelDett()
            End If
        End If
    End Sub
    
    'rimanda alla gestione dei workflowstep
    Sub WorkFlowManagement(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        ActivePanelGrid()
        pnlWorkFlowStep.Visible = True
        pnlWorkFlow.Visible = False
        
        LoadArchive()
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
           
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    Sub LoadWFArchive(ByVal sender As Object, ByVal e As EventArgs)
        LoadWFArchive()
        ActiveWFPanelGrid()
    End Sub
    
    'modifica le propietÓ di un singolo step
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim oStepValue As New StepValue
         
        If (IsNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            With (oStepValue)
                .Description = stepDescription.Text
                .Order = stepOrder.Text
                .Priority = dwlPriority.SelectedItem.Value
                .KeyWorkFlow.Id = dwlWorkFlows.SelectedValue
            End With
         
            If SelectedId = 0 Then
                workFlowManager.Create(oStepValue)
                LoadArchive()
                ActivePanelGrid()
                                 
            Else
                oStepValue.Key.Id = SelectedId
                workFlowManager.Update(oStepValue)
                
                LoadArchive()
                ActivePanelGrid()
            End If
        
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Sub
    
    'modifica le proprietÓ di un singolo workflow
    Sub UpdateWorkFlow(ByVal sender As Object, ByVal e As EventArgs)
        Dim wfValue As New WorkFlowValue
        
        With (wfValue)
            .Description = wfDescription.Text
        End With
         
        If SelectedId = 0 Then
            workFlowManager.Create(wfValue)
            LoadWFArchive()
            ActiveWFPanelGrid()
                                 
        Else
            wfValue.Key.Id = SelectedId
            workFlowManager.Update(wfValue)
                
            LoadWFArchive()
            ActiveWFPanelGrid()
        End If
    End Sub
    
    'inserisce un nuovo step
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
    
    'inserisce un nuovo workflow
    Sub NewWorkFlow(ByVal sender As Object, ByVal e As EventArgs)
        btnUp.Text = "Save"
        SelectedId = 0
        
        ActiveWFPanelDett()
        LoadWFDett(Nothing)
    End Sub
    
    'cancella un singolo step
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If sender.CommandName = "DeleteItem" Then
            SelectedId = sender.CommandArgument
           
            Dim stepIdent As New StepIdentificator
            stepIdent.Id = SelectedId
             
            workFlowManager.Remove(stepIdent)
        End If
        
        LoadArchive()
        ActivePanelGrid()
    End Sub
    
    'cancella un singolo workflow
    Sub DeleteWorkFlow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If sender.CommandName = "DeleteItem" Then
            SelectedId = sender.CommandArgument
           
            Dim wfIdent As New WorkFlowIdentificator
            wfIdent.Id = SelectedId
             
            workFlowManager.Remove(wfIdent)
        End If
        
        LoadWFArchive()
        ActiveWFPanelGrid()
    End Sub
    
    'popola i campi di testo con le informazioni contenute nello stepValue
    Sub LoadDett(ByVal oStepValue As StepValue)
        If Not oStepValue Is Nothing Then
            stepDescription.Text = oStepValue.Description
            stepOrder.Text = oStepValue.Order
            
            If (oStepValue.Priority = StepValue.PriorityEnum.Facultative) Then
                dwlPriority.SelectedIndex = dwlPriority.Items.IndexOf(dwlPriority.Items.FindByValue(StepValue.PriorityEnum.Facultative))
            ElseIf (oStepValue.Priority = StepValue.PriorityEnum.Necessary) Then
                dwlPriority.SelectedIndex = dwlPriority.Items.IndexOf(dwlPriority.Items.FindByValue(StepValue.PriorityEnum.Necessary))
            Else
                dwlPriority.SelectedIndex = -1
            End If
            
            dwlWorkFlows.SelectedIndex = dwlWorkFlows.Items.IndexOf(dwlWorkFlows.Items.FindByValue(oStepValue.KeyWorkFlow.Id))
        Else
            dwlWorkFlows.SelectedIndex = 0
            stepDescription.Text = Nothing
            stepOrder.Text = Nothing
            dwlPriority.SelectedIndex = -1
        End If
    End Sub
  
    'popola i campi di testo con le informazioni contenute nel workFlowValue
    Sub LoadWFDett(ByVal oWorkFlowValue As WorkFlowValue)
        If Not oWorkFlowValue Is Nothing Then
            wfDescription.Text = oWorkFlowValue.Description
        
        Else
            wfDescription.Text = Nothing
        End If
    End Sub
    
    'gestisce l'associazione tra uno step e gli utenti 
    Sub StepUserRelationsManager(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim stepId As Integer = sender.CommandArgument()
        SelectedId() = stepId
        
        ReadStepInformation()
        ReadRelations()
        LoadUsers()
        
        ActivePanelRelation()
    End Sub
    
    'recupera le informazioni relative allo step
    Sub ReadStepInformation()
        Dim stepId As New StepIdentificator()
        stepId.Id = SelectedId()
        
        Dim stepValue As StepValue = workFlowManager.Read(stepId)
        lblStepId.Text = stepValue.Key.Id
        lblStepDescription.Text = stepValue.Description
    End Sub
    
    'recupera tutti gli utenti
    Sub LoadUsers()
        Dim userSearcher As New UserSearcher
        
        Dim userColl As UserCollection = Me.BusinessUserManager.Read(userSearcher)
        
        gridUsers.DataSource() = userColl
        gridUsers.DataBind()
    End Sub
    
    'salva le modifiche sulle relazioni step/user
    Sub SaveRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim stepUserIdent As New StepUserIdentificator
        stepUserIdent.Id = StepUserId()
       
        Dim selUserType As Integer = dwlUseType.SelectedItem.Value
        workFlowManager.Update(stepUserIdent, "StepUserType", selUserType)
        
        ReadRelations()
        
        pnlEditRel.Visible = False
        pnlActiveRelations.Visible = True
    End Sub
    
    Sub LoadRelations(ByVal sender As Object, ByVal e As EventArgs)
         
        pnlEditRel.Visible = False
        pnlActiveRelations.Visible = True
    End Sub
    
    'aggiunge una nuova relazione user/content
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim _oKeyUser As New UserIdentificator()
        _oKeyUser.Id = sender.commandArgument.ToString
        
        Dim stepUserV As New StepUserValue
        stepUserV.KeyStep.Id = SelectedId()
        stepUserV.KeyUser = _oKeyUser
        
        workFlowManager.Create(stepUserV)
        
        ReadRelations()
    End Sub
    
    'rimuove una relazione user/step
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim key As String = sender.commandArgument.ToString
        
        Dim stepUserIdent As New StepUserIdentificator()
        stepUserIdent.Id = key
                            
        workFlowManager.Remove(stepUserIdent)
        
        ReadRelations()
    End Sub
    
    'permette di editare la relazione e modificare lo userType
    Sub EditRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim item As String = sender.CommandArgument()
        
        Dim resultStrings() As String = item.Split("|")
        StepUserId() = resultStrings(0)
        SelectedId() = resultStrings(1)
                
        dwlUseType.SelectedIndex = dwlUseType.Items.IndexOf(dwlUseType.Items.FindByValue(resultStrings(2)))
        
        pnlEditRel.Visible = True
        pnlActiveRelations.Visible = False
    End Sub
    
    'legge le relazioni user/step
    Sub ReadRelations()
        If (SelectedId() <> 0) Then
            Dim stepUserSearcher As New StepUserSearcher
            stepUserSearcher.KeyStep.Id = SelectedId()
            
            workFlowManager.Cache = False
            Dim _stepUserColl As StepUserCollection = workFlowManager.Read(stepUserSearcher)
           
            gridActiveRelation.DataSource = _stepUserColl
            gridActiveRelation.DataBind()
        End If
    End Sub
    
    Function getStep(ByVal stepId As Integer) As String
        Dim stepIdent As New StepIdentificator
        stepIdent.Id = stepId
        
        Dim stepValue As StepValue = workFlowManager.Read(stepIdent)
        Return stepValue.Description
    End Function
    
    Function getUser(ByVal userId As Integer) As String
        Dim userIdent As New UserIdentificator
        userIdent.Id = userId
        
        Dim userValue As UserValue = Me.BusinessUserManager.Read(userIdent)
        Return userValue.CompleteName
    End Function
    
    Function getUserType(ByVal type As Integer) As String
        'Response.Write("type" & type)
        If (type = StepUserValue.StepUserTypeEnum.ANDType) Then
            UserType() = StepUserValue.StepUserTypeEnum.ANDType
            Return "AND"
        ElseIf (type = StepUserValue.StepUserTypeEnum.ORType) Then
            UserType() = StepUserValue.StepUserTypeEnum.ANDType
            Return "OR"
        End If
 
        Return " -- "
    End Function
    
    'controlla che i campi di testo non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (stepDescription.Text = String.Empty) Then strName = "Description"
        If (stepOrder.Text = String.Empty) Then strName = "Order"
        'If (dwlPriority.SelectedIndex = -1) Then strName = "Priority"
                      
        If ((stepDescription.Text <> String.Empty) And (stepOrder.Text <> String.Empty)) Then
            _return = True
        End If
        
        Return _return
    End Function
     
    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtId.Text = ""
        txtName.Text = ""
        txtSurname.Text = ""
    End Sub
    
    'gestisce la ricerca dell'utente
    Sub filterDataSource()
        Dim userSearcher As New UserSearcher
        
        If (txtId.Text <> "") Then userSearcher.Key.Id = txtId.Text
        If (txtName.Text <> "") Then userSearcher.Name = txtName.Text
        If (txtSurname.Text <> "") Then userSearcher.Surname = txtSurname.Text
        
        Dim userColl As UserCollection = Me.BusinessUserManager.Read(userSearcher)
        
        gridUsers.DataSource() = userColl
        gridUsers.DataBind()
    End Sub
         
    'popola la dropdownlist relativa ai workflow
    Sub BindComboWorkFlow()
        Dim workFlowManager As New WorkFlowManager
        Dim workFlowSearcher As New WorkFlowSearcher
        
        Dim workFlowColl As WorkFlowCollection = workFlowManager.Read(workFlowSearcher)
   
        dwlWflow.Items.Clear()
        For Each elem As WorkFlowValue In workFlowColl
            dwlWorkFlows.Items.Add(New ListItem(elem.Description, elem.Key.Id))
            dwlWflow.Items.Add(New ListItem(elem.Description, elem.Key.Id))
        Next
        
        dwlWorkFlows.Items.Insert(0, New ListItem("--Select--", "0"))
        dwlWflow.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    
    'riporta alla gestione del workflow
    Sub BackToList(ByVal sender As Object, ByVal e As EventArgs)
        DisablePanelRelation()
    End Sub
    
    'disattiva il pannello per la visualizzazione delle relazioni tra utenti e step
    Sub DisablePanelRelation()
        dwlWflow.SelectedIndex = 0
        pnlUserRel.Visible = False
        pnlGrid.Visible = True
    End Sub
    
    'attiva il pannello per la visualizzazione delle relazioni tra utenti e step
    Sub ActivePanelRelation()
        pnlUserRel.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        pnlUserRel.Visible = False
    End Sub
    
    Sub ActiveWFPanelGrid()
        GridWF.Visible = True
        DettWF.Visible = False
        pnlUserRel.Visible = False
        pnlWorkFlowStep.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
        pnlUserRel.Visible = False
    End Sub
       
    Sub ActiveWFPanelDett()
        DettWF.Visible = True
        GridWF.Visible = False
        pnlUserRel.Visible = False
        pnlWorkFlowStep.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
   
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Sub GoWFManagement(ByVal sender As Object, ByVal e As EventArgs)
        pnlWorkFlowStep.Visible = False
        pnlWorkFlow.Visible = True
        
        LoadWFArchive()
    End Sub
    
      
    'gestisce la ricerca degli steps
    Sub SearchSteps(ByVal sender As Object, ByVal e As EventArgs)
        Dim stepSearcher As New StepSearcher
        
        If (txtStepId.Text <> String.Empty) Then stepSearcher.Key.Id = txtStepId.Text
        If (dwlWflow.SelectedValue <> 0) Then stepSearcher.KeyWorkFlow.Id = dwlWflow.SelectedValue
             
        Dim stepColl As StepCollection = workFlowManager.Read(stepSearcher)
        gdw_stepList.DataSource() = stepColl
        gdw_stepList.DataBind()
    End Sub
</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:Panel id="pnlWorkFlow" runat="server"> 
     <asp:Panel id="GridWF" runat="server" visible="false"> 
     <div>
        <asp:Button id ="btnNewWF" runat="server" Text="New" OnClick="NewWorkFlow"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
     </div>
     <asp:Label CssClass="title" runat="server" id="Label1">Approval WorkFlows</asp:Label>
     <br /> 
      
        <asp:gridview id="grid_workflows" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="20"
                    emptydatatext="No item available">
                  <Columns >
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                           <ItemTemplate>
                                <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditWorkFlow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif"  OnClientClick="return confirm('Confirm Delete?')"  OnClick="DeleteWorkFlow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="btn_content_user" ToolTip ="Steps Management" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/wf_step_relation.gif" onClick="WorkFlowManagement" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
     </asp:gridview>
    </asp:Panel>
    
    <asp:Panel id="DettWF" runat="server" visible="false">
     <div>
        <asp:Button id ="btnArc" runat="server" Text="Archive" OnClick="LoadWFArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btnUp" runat="server" Text="Update" OnClick="UpdateWorkFlow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
     </div>
     <table  style=" margin-top:10px" class="form" width="99%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpWFDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
            <td><HP3:Text runat ="server" id="wfDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
        </tr>
     </table>
    </asp:Panel>
</asp:Panel>

<asp:Panel id="pnlWorkFlowStep" runat="server" visible="false">
      <asp:Panel id="pnlGrid" runat="server" visible="false"> 
     <div>
        <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btnGoBack" runat="server" Text="Go Back" OnClick="GoWFManagement"  CssClass="button"  style="margin-bottom:10px"/>
     </div>
     
       <%-- Pannello Step Searcher--%>
    <asp:Panel ID="pnlStepS" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Step Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Approval WorkFlow</td>
            </tr>
            <tr>
                <td><HP3:Text ID="txtStepId" runat="server" TypeControl="NumericText" Style="width:50px;" /></td>
                <td><asp:DropDownList id="dwlWflow" runat="server"/></td>
           </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSear" Enabled="true" CssClass="button" OnClick="SearchSteps" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
           </tr>
        </table>
    </asp:Panel>
    <hr />
    
     <asp:Label CssClass="title" runat="server" id="sectionTit">WorkFlow Steps List</asp:Label>
     <asp:gridview id="gdw_stepList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="20"
                    emptydatatext="No item available">
                  <Columns >
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Order">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Order")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="7%">
                           <ItemTemplate>
                                <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif"  OnClientClick="return confirm('Confirm Delete?')"  OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="btn_content_user" ToolTip ="Step User Relation" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_user.gif" onClick="StepUserRelationsManager" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
     </asp:gridview>
     </asp:Panel>

    <asp:Panel id="pnlDett" runat="server" visible="false">
     <div>
        <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
     </div>
     <table  style=" margin-top:10px" class="form" width="99%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpStepWFDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
            <td><HP3:Text runat ="server" id="stepDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOrderStepWF&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
            <td><HP3:Text runat ="server" id="stepOrder" TypeControl ="TextBox" style="width:50px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPriority&Help=cms_HelpPriority&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPriority", "Priority")%></td>
            <td>
                <asp:DropDownList id="dwlPriority" runat="server">
                    <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                    <asp:ListItem  Text="Facultative" Value="1"></asp:ListItem>
                    <asp:ListItem  Text="Necessary" Value="2"></asp:ListItem>
                </asp:DropDownList>
           </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormApprovalWF&Help=cms_HelpApprovalWF&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormApprovalWF", "Approval WorkFlow")%></td>
            <td><asp:DropDownList id="dwlWorkFlows" runat="server"/></td>
        </tr>
    </table>
    </asp:Panel>
</asp:Panel>

<asp:Panel id="pnlUserRel" runat="server" visible="false">
 <asp:Button ID="btnNRelation" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
    <%--Pannello del Content Selezionato--%>   
    <asp:Panel ID="StepSummary" Width="100%" runat="server">
        <div class="title">Step Selected</div>
        <table class="tbl1">
            <tr>
                <th style="width:5%">Id</th>
                <th style="width:20%">Description</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblStepId" runat="server" /></td>
                <td><asp:Label ID="lblStepDescription" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />

    <%--Pannello delle Relazioni attive--%>
    <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div>
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Step">
                        <ItemTemplate>
                            <%#getStep(DataBinder.Eval(Container.DataItem, "KeyStep.Id"))%>
                        </ItemTemplate>
                  </asp:TemplateField>
                <asp:TemplateField HeaderText="User">
                        <ItemTemplate>
                            <%#getUser(DataBinder.Eval(Container.DataItem, "KeyUser.Id"))%>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="User Type">
                        <ItemTemplate>
                            <%#getUserType(DataBinder.Eval(Container.DataItem, "StepUserType"))%>
                        </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="7%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRelation" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")& "|" & DataBinder.Eval(Container.DataItem, "KeyStep.Id") & "|" & DataBinder.Eval(Container.DataItem, "StepUserType")%>'/>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
            <hr />
    </asp:Panel>
    
    
     <%-- Pannello per l'edit delle relazioni--%>
    <asp:Panel ID="pnlEditRel" runat="server" Visible="false" width="100%" Height="100">
        <asp:Button id ="btSArchive" runat="server" Text="Archive" OnClick="LoadRelations" CssClass="button" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btSEdit" runat="server" Text="Save" OnClick="SaveRow" CssClass="button" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
        <table class="form" width="99%">
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpStepUserType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%> </td>
                <td>
                    <asp:DropDownList id="dwlUseType" runat="server">
                        <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                        <asp:ListItem  Text="And" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Or" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
         </table>
    </asp:Panel>
    
    <%-- Pannello User Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Surname</td>
            </tr>
            <tr>
                <td><HP3:Text ID="txtId" runat="server" TypeControl="NumericText" Style="width:50px;" /></td>
                <td><HP3:Text ID="txtName" runat="server" typecontrol="TextBox" /></td>
                <td><HP3:Text ID="txtSurname" runat="server" typecontrol="TextBox" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
     
    <asp:GridView ID="gridUsers" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server"
        >
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                      <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Name")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Surname">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Panel>