<%@ Control Language="VB" ClassName="ContentUsers" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>

<script runat="server">
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    
    Private _webUtility As WebUtility
    Private dateUtility As New StringUtility
    
    Private oLManager As New LanguageManager()
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    
    ' il datasource corrente
    Private _dataSource As UserCollection = Nothing
    ' la lista degli id che hanno una relazione con il content
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue
     

    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                _keyContent = New ContentIdentificator(objQs.ContentId, objQs.Language_id)
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
        End Set
    End Property
    
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As New ContentCollection
        
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        contentCollection = _contentManager.Read(so)
        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function

    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property
    
    Public ReadOnly Property Related() As List(Of Integer)
        Get
            If _related Is Nothing Then
                If Not UseInPopup Then
                    _related = readRelated(KeyContent)
                Else
                    _related = readSelection(KeyContent)
                End If
            End If
            Return _related
        End Get
    End Property

    Private ReadOnly Property KeyChanges() As String
        Get
            Dim _key As String
            _key = ViewState("RelatedChangesKey")
            If _key Is Nothing Then
                ' Genera una chiave random per assicurarsi che lo stato
                ' delle modifiche cambi tra una richiesta e l'altra
                _key = SimpleHash.GenerateUUID(32)
                ViewState("RelatedChangesKey") = _key
            End If
            Return _key
        End Get
    End Property

    Public Property RelatedChanges() As Dictionary(Of Integer, Boolean)
        Get
            If _changes Is Nothing Then
                _changes = Cache.Item(KeyChanges)
                If _changes Is Nothing Then
                    _changes = New Dictionary(Of Integer, Boolean)
                End If
            End If
            Return _changes
        End Get
        Set(ByVal value As Dictionary(Of Integer, Boolean))
            _changes = value
            Cache.Insert(KeyChanges, _changes, DateTime.Now.AddMinutes(10), True)
        End Set
    End Property

    Public Function checkRelated(ByVal idRelation As Integer) As Boolean
        Return checkRelated(idRelation, False)
    End Function

    Public Function checkRelated(ByVal idRelation As Integer, ByVal onlyPreviouslyRelated As Boolean) As Boolean
        ' Verifica se il datasource degli elementi relazionati contiene la chiave corrente
        If Not Related Is Nothing AndAlso Related.Contains(idRelation) Then
            ' se si verifica che non sia stata deselezionata dall'utente
            If Not onlyPreviouslyRelated AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
                Return RelatedChanges.Item(idRelation)
            Else
                ' non � stata deselezionata
                Return True
            End If
        ElseIf Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
            ' Se la chiave non era precedentemente selezionata, verifica se � stata selezionata dall'utente
            If Not onlyPreviouslyRelated Then
                Return RelatedChanges.Item(idRelation)
            Else
                Return False
            End If
        End If
    End Function
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)

        ' Legge il datasource completo 
        getDataSource(clearCache)

        '' Controlla se visualizzare solo gli elementi con relazioni
        'If chkShowOnlyRelatedItems.Checked Then

        '    ' procede con un ciclo per escludere gli elementi non relazionati 
        '    ' (al momento non � possibile realizzare tale operazione in maniera pi� efficiente)
        '    Dim idRelation As Integer
        '    Dim removeIndex As Integer = 0
        '    Dim item As Object
        '    For i As Integer = 0 To _dataSource.Count - 1
        '        item = _dataSource.Item(removeIndex)
        '        idRelation = DataBinder.Eval(item, "Key.Id")
        '        ' se l'elemento non � relazionato ...
        '        If Not checkRelated(idRelation) Then
        '            ' ... lo esclude
        '            _dataSource.Remove(item)
        '        Else
        '            ' passa all'elemento successivo
        '            removeIndex = removeIndex + 1
        '        End If
        '    Next
        'End If

        ' Effettua il bind del datasource sulla gridview
        gridUsers.DataSource = _dataSource
        gridUsers.DataBind()
    End Sub

    'Public Sub SaveRelatedStatus()
    '    ' flag che indica se � stato apportato un cambiamento alle relazioni
    '    Dim isChanges As Boolean = False
                
    '    ' controlla le modifiche effettuate sulla griglia
    '    If gridUsers.Rows.Count = 0 Then Return
    '    Dim check As CheckBox
    '    Dim radio As RadioButton
    '    Dim idRelation As Integer
    '    Dim checked As Boolean = False
    '    ' legge ogni riga della griglia
    '    For Each row As GridViewRow In gridUsers.Rows
    '        ' azzera idrelation
    '        idRelation = -1
    '        ' estrae il checkbox
    '        If AllowEdit Then
    '            check = row.FindControl("chkRelation")
    '            If Not check Is Nothing AndAlso check.Text <> "" Then
    '                ' la propriet� text del checkbox contiene l'id della relazione
    '                idRelation = Integer.Parse(check.Text)
    '                checked = check.Checked
    '            End If
    '        Else
    '            radio = row.FindControl("optRelation")
    '            If Not radio Is Nothing AndAlso radio.Text <> "" Then
    '                ' la propriet� text del checkbox contiene l'id della relazione
    '                idRelation = Integer.Parse(radio.Text)
    '                checked = radio.Checked
    '            End If
    '        End If

    '        If idRelation > -1 Then
    '            ' verifica se lo stato della relazione attuale � cambiato rispetto a quello iniziale
    '            If (checkRelated(idRelation, True) AndAlso Not checked) OrElse (Not checkRelated(idRelation, True) AndAlso checked) Then
    '                ' aggiorna l'hashmap dei cambiamenti
    '                If RelatedChanges.ContainsKey(idRelation) Then
    '                    RelatedChanges.Item(idRelation) = checked
    '                Else
    '                    If Not AllowEdit And checked Then
    '                        RelatedChanges.Clear()
    '                        RelatedChanges.Add(idRelation, checked)
    '                    ElseIf AllowEdit Then
    '                        RelatedChanges.Add(idRelation, checked)
    '                    End If
    '                End If
    '                ' imposta il flag
    '                isChanges = True
    '            ElseIf checkRelated(idRelation, True) <> checkRelated(idRelation, False) Then
    '                ' aggiorna l'hashmap dei cambiamenti
    '                RelatedChanges.Remove(idRelation)
    '                ' imposta il flag
    '                isChanges = True
    '            End If
    '        End If
    '    Next

    '    ' se � stato cambiato qualcosa nelle relazioni ...
    '    If isChanges Then
    '        ' ... fa si che il valore della property venga cachato
    '        RelatedChanges = RelatedChanges
    '    End If
    '    ' abilita il tasto apply associations a lato client (js)
    '    If AllowEdit AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then
    '        Dim strJs As String = "<script type='text/javascript'>enableApplyButton();<" & "/script>"
    '        Page.ClientScript.RegisterStartupScript(GetType(String), "enableApplyButton", strJs)
    '    End If
    'End Sub
    
    'Protected Sub chkShowOnlyRelatedItems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowOnlyRelatedItems.CheckedChanged
    '    If Not AllowEdit Then Return

    '    'SaveRelatedStatus()

    '    _dataSource = Nothing
        
    '    ' memorizza l'indice della pagina per ricaricarlo quando si cambia il tipo di visualizzazione
    '    ' tra quello completo (full) e quello contenente solo le relazioni attive (onlyRelated)
    '    If chkShowOnlyRelatedItems.Checked Then
    '        ViewState("fullPageIndex") = gridUsers.PageIndex
    '        gridUsers.PageIndex = ViewState("onlyRelatedPageIndex")
    '    Else
    '        ViewState("onlyRelatedPageIndex") = gridUsers.PageIndex
    '        gridUsers.PageIndex = ViewState("fullPageIndex")
    '    End If
    '    BindGrid(True)
    'End Sub

    'Protected Sub gridRelations_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridUsers.DataBound

    '    If gridUsers.FooterRow Is Nothing Then Return
     
    '    For cellNum As Integer = gridUsers.Columns.Count - 1 To 1 Step -1
    '        Try
    '            gridUsers.FooterRow.Cells.RemoveAt(cellNum)
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    gridUsers.FooterRow.Cells(0).ColumnSpan = gridUsers.Columns.Count
    '    gridUsers.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
    '    If Not _dataSource Is Nothing Then
    '        Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
    '        Dim startIndex As Integer = gridUsers.PageSize * gridUsers.PageIndex
    '        Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
    '        gridUsers.FooterRow.Cells(0).Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", _
    '            s, startIndex + 1, startIndex + gridUsers.Rows.Count, gridViewTotalCount, s)
    '    Else
    '        gridUsers.FooterRow.Cells(0).Text = "No items found."
    '    End If
    'End Sub

   

    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridUsers
        End Get
    End Property

    Private Sub LoadContentSummary()
        
        If KeyContent.Id > 0 Then

            lblContentId.Text = ContentValue.Key.Id
            If ContentValue.DatePublish.HasValue Then
                lblContentDatePublish.Text = getCorrectedFormatDate(ContentValue.DatePublish)
            Else
                lblContentDatePublish.Text = ""
            End If
            imgContentLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
            imgContentLanguage.Visible = True
            lblContentTitle.Text = ContentValue.Title
        Else
            imgContentLanguage.Visible = False
        End If

    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        If Not AllowEdit Then
            'pnlHeader.Visible = False
            ContentSummary.Visible = False
        End If

        If Not Page.IsPostBack Then
            LoadContentSummary()
            ReadRelations()
        End If
        
        If objQs.ContentId = 0 And Not UseInPopup Then
            Dim _webUtility As New WebUtility
            'SearchEngine.DropDownContentType.Items.Clear()
            '_webUtility.LoadListControl(SearchEngine.DropDownContentType, ContentTypes, "Description", "Key.Id")
            SearchEngine.Visible = True
            pnlRelationEdit.Visible = False
        Else
            SearchEngine.Visible = False
            If Not Page.IsPostBack Then
                'LoadControl()
            End If
        End If
       
    End Sub
    
    Private ReadOnly Property ContentTypes() As ContentTypeCollection
        Get
            If _contentTypeCollection Is Nothing Then
                Dim _contentTypeManager As New ContentTypeManager
                Dim _contentTypeSearcher As New ContentTypeSearcher
                _contentTypeSearcher.OnlyAssociatedContents = True
                _contentTypeCollection = _contentTypeManager.Read(_contentTypeSearcher)
            End If
            Return _contentTypeCollection
        End Get
    End Property

    Private Function getDataSource() As UserCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As UserCollection
        If _dataSource Is Nothing Then
            Dim _UserSearcher As New UserSearcher()
            If Not DataValueFilter Is Nothing AndAlso DataValueFilter <> "" Then
                _UserSearcher.Key = New UserIdentificator(Integer.Parse(DataValueFilter))
            End If
            If Not DataTextFilter Is Nothing AndAlso DataTextFilter <> "" Then
                _UserSearcher.Name = "%" & DataTextFilter & "%"
            End If
            If Not txtSurname.Text Is Nothing AndAlso txtSurname.Text <> "" Then
                _UserSearcher.Surname = "%" & txtSurname.Text & "%"
            End If
            If Not txtMail.Text Is Nothing AndAlso txtMail.Text <> "" Then
                _UserSearcher.Email = "%" & txtMail.Text & "%"
            End If
            
            
            Dim _oUserManager As New UserManager()
            _oUserManager.Cache = Not clearCache
            _dataSource = _oUserManager.Read(_UserSearcher)
        End If
        Return _dataSource
    End Function
    
    Private Function readSelection(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        If Not Request("txtHidden") Is Nothing AndAlso Request("txtHidden") <> "" Then
            _related = New List(Of Integer)
            _related.Add(Request("txtHidden"))
            RelatedChanges.Clear()
            RelatedChanges.Add(Request("txtHidden"), True)
            'CType(WebUtility.MyPage(Me).TextHidden, TextBox).Text = findClientId(Request("SelItem"))
            Return _related
        Else
            Return Nothing
        End If
        
    End Function

   
    Private Function readRelated(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        If _related Is Nothing AndAlso Not keyContent Is Nothing Then
            Dim _oUserRelatedCollection As UserCollection = Nothing
            Dim _oContentManager As New ContentManager
            _oContentManager.Cache = False
            _oUserRelatedCollection = _oContentManager.ReadContentUser(keyContent)
            If Not _oUserRelatedCollection Is Nothing Then
                _related = New List(Of Integer)
                Dim _user As UserValue
                For Each _user In _oUserRelatedCollection
                    _related.Add(_user.Key.Id)
                Next
            End If
        End If
        Return _related
    End Function
    
    Private Overloads Sub LoadControl()
        Dim _oUserCollection As UserCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
        'Dim _oContentManager As New ContentManager
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)

        readRelated(_oKeyContent)
        
        BindGrid()
        pnlRelationEdit.Visible = True

    End Sub
    
    Protected Sub SearchEngine_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arr() As String = sender.CommandArgument.split("_")
        Dim id As Int32 = arr(0)
        Dim idLanguage As Int32 = arr(1)
        
        objQs.ContentId = id
        objQs.Language_id = idLanguage
        webRequest.customParam.append(objQs)
        Dim mPageManager As New MasterPageManager
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub

    'Protected Sub ApplyAssociations(ByVal sender As Object, ByVal e As System.EventArgs)
        
    '    If Not AllowEdit Then Return
        
    '    SaveRelatedStatus()
        
    '    If Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then

    '        Dim _oContentManager As New ContentManager
    '        Dim _oKeyContent As ContentIdentificator = _oContentManager.getContentIdentificator(objQs.ContentId, objQs.Language_id)
    '        Dim key As Integer
    '        Dim value As Boolean
    '        Dim _oKeyUser As UserIdentificator
    '        For Each key In RelatedChanges.Keys
    '            value = RelatedChanges.Item(key)
    '            _oKeyUser = New UserIdentificator(key)
    '            If value Then
    '                _oContentManager.CreateContentUser(_oKeyContent, _oKeyUser)
    '            Else
    '                _oContentManager.RemoveContentUser(_oKeyContent, _oKeyUser)
    '            End If
    '        Next
            
    '        readRelated(_oKeyContent)

    '        'Return True
    '    End If
        
    '    'Return False
    'End Sub
    
    'aggiunge una nuova relazione user/content
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oKeyUser As New UserIdentificator()
        _oKeyUser.Id = key
        
        Me.BusinessContentManager.CreateContentUser(_oKeyContent, _oKeyUser)
        ReadRelations()
    End Sub
     
    'rimuove una relazione role/content
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As String = sender.commandArgument.ToString
        Dim _oKeyUser As New UserIdentificator()
        _oKeyUser.Id = key
        
        Me.BusinessContentManager.RemoveContentUser(_oKeyContent, _oKeyUser)
        
        ReadRelations()
    End Sub
    
    'legge le relazioni user/content
    Sub ReadRelations()
        If (objQs.ContentId <> 0) Then
            ''1149
            Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
            
            Me.BusinessContentManager.Cache = False
            Dim _oUserCollection As UserCollection = Me.BusinessContentManager.ReadContentUser(_oKeyContent)
            If Not _oUserCollection Is Nothing Then
                gridActiveRelation.DataSource = _oUserCollection
                gridActiveRelation.DataBind()
            End If
        End If
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        
        'If Not AllowEdit Then Return
        'Response.Redirect(GetBackUrl)
    End Sub

    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue

        objQs.ContentId = 0

        webRequest.customParam.append(objQs)
        Return mPageManager.FormatRequest(webRequest)
    End Function
    
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridUsers.PageIndexChanging
        'SaveRelatedStatus()
        gridUsers.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        txtSurname.Text = ""
        'SaveRelatedStatus()
        filterDataSource()
    End Sub
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
</script>

<%--<script type="text/javascript">
    function enableApplyButton(state) {
        if (state == null) state = true;
        try {
            document.getElementById('<%=btApply.ClientId %>').disabled = !state;
        } catch (e) {
        }
    }
</script>--%>

<hr />
<HP3:SearchEngine ID="SearchEngine" visible="false" UseInRelation ="true"  AllowEdit="false" AllowSelect="true" ShowContentType="true" runat="server" OnSelectedEvent="SearchEngine_SelectedEvent" />
<hr />

<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="Button1" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
    <%--Pannello del Content Selezionato--%>   
    <asp:Panel ID="ContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div> 
        <br />
       <div>
        <table class="tbl1">
            <tr>
                <th style="width:5%">Id</th>
                <th style="width:20%">Date Publish</th>
                <th style="width:5%"></th>
                <th style="width:70%">Title</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
            </tr>
        </table>
        </div>
    </asp:Panel>
    <hr />


    <%--Pannello delle Relazioni attive--%>
    <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div>
              <br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Name")%>
                        </ItemTemplate>
                  </asp:TemplateField>
                <asp:TemplateField HeaderText="Surname">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                        </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="2%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />

    <%-- Pannello User Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Surname</td>
                <td>E-mail</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" Style="width:50px;" /></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></td>
                <td><HP3:ctlText ID="txtSurname" runat="server" typecontrol="TextBox" /></td>
                <td><HP3:ctlText ID="txtMail" runat="server" typecontrol="TextBox" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <%--<div class="title">Related Users</div>
    <asp:Panel ID="pnlHeader" runat="server">
        <asp:CheckBox ID="chkShowOnlyRelatedItems" Text="Show only active relations" runat="server" Visible="<%#AllowEdit.toString() %>" AutoPostBack="true" />
        <asp:Button ID="btBackToList" Text="Back to list" CssClass="button" runat="server" OnClick="BackToList" />
        <asp:Button ID="btApply" Text="Apply Associations" CssClass="button" runat="server" Enabled="false" OnClick="ApplyAssociations" />
    </asp:Panel>--%>

    
    <asp:GridView ID="gridUsers" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server"
        >
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                    <%--<%If AllowEdit Then%>
                        <asp:CheckBox ID="chkRelation" onClick="enableApplyButton()" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%Else%>
                        <asp:RadioButton ID="optRelation" onClick="GestClick(this)" GroupName="optRelations" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%End If%>--%>
                     <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Name")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Surname">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Panel>