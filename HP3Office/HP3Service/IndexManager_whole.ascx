<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private mPageManager As New MasterPageManager
    Private contentManager As New ContentManager
       
    Private contentTypeSearcher As New ContentTypeSearcher
    
    Private webRequest As New WebRequestValue
    Private utility As New WebUtility
    
    Private _sortOrder As SiteAreaGenericComparer.SortOrder = SiteAreaGenericComparer.SortOrder.ASC
    Private _sortType As SiteAreaGenericComparer.SortType = SiteAreaGenericComparer.SortType.ByName
    Private strJS As String
    
    Sub Page_load()
           
        If Not Page.IsPostBack Then
            BindContentType(False)
            BindSite()
            ctlArea.LoadControl()
        End If
    End Sub
   
    Sub GoSpider(ByVal sender As Object, ByVal ev As EventArgs)
        Dim index As IndexerModule = IndexerModule.GetInstance
        Dim oSiteAreaValue As New SiteAreaValue
        Dim contentSearcher As New ContentSearcher
        Dim contentCollection As New ContentCollection
              
        'oSiteAreaValue = GetSiteSelection(ctlSite)
        If dwlSite.SelectedItem.Value <> -1 Then
            contentSearcher.KeySite.Id = dwlSite.SelectedItem.Value
            
            oSiteAreaValue = GetSiteSelection(ctlArea)
            
            If Not oSiteAreaValue Is Nothing Then
                contentSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
            End If
            
            If (dwlContentType.SelectedItem.Value <> -1) Then
                contentSearcher.KeyContentType.Id = dwlContentType.SelectedItem.Value
            End If
                       
            'index.ActSpider(contentSearcher)
            contentCollection = Me.BusinessContentManager.Read(contentSearcher)
            If Not contentCollection Is Nothing AndAlso contentCollection.Count > 0 Then
                For i As Integer = 0 To contentCollection.Count - 1
                    Response.Write("<br>" & contentCollection(i).Key.Id & " " & i & "/" & (contentCollection.Count - 1))
                    index.AddContent(contentCollection(i).Key)
                Next
            End If
            CreateJsMessage("Successful operation!")
        Else
            CreateJsMessage("Select a Site, please.")
        End If
    End Sub
    
    'gestisce il refresh di index
    Sub RefreshIndex(ByVal sender As Object, ByVal ev As EventArgs)
        Dim index As IndexerModule = IndexerModule.GetInstance
        index.LoadIndex()
        
        CreateJsMessage("Index updated successfully!")
    End Sub
    
    Function GetTotalWords() As String
        Dim index As IndexerModule = IndexerModule.GetInstance
        Try
            Return index.GetIndex.Count.ToString
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    
    'annulla tutta la indicizzazione
    Sub ResetIndex(ByVal sender As Object, ByVal ev As EventArgs)
        Dim index As IndexerModule = IndexerModule.GetInstance
        Dim ret As Boolean = index.ResetIndex()
        
        If (ret) Then
            CreateJsMessage("Index reset successfully!")
        Else
            CreateJsMessage("Operation not realized!")
        End If
        
    End Sub
    
    'restituisce il valore selezionato nel controllo
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    'popola la combo dei siti
    Sub BindSite()
        Dim oSiteManager As New SiteAreaManager
        oSiteManager.Cache = False
        Dim oSiteSearcher As New SiteAreaSearcher
               
        oSiteSearcher.Status = 1
        oSiteSearcher.IdType = SiteAreaCollection.SiteAreaType.Site
        Dim _sCollection As SiteAreaCollection = Me.BusinessSiteAreaManager.Read(oSiteSearcher)
    
        If Not (_sCollection Is Nothing) Then _sCollection.Sort(_sortType, _sortOrder)
        utility.LoadListControl(dwlSite, _sCollection, "Name", "Key.Id")
        dwlSite.Items.Insert(0, New ListItem("---Select Site---", -1))
    End Sub
    
    'popola la dwl per i contentType
    Sub BindContentType(ByVal filterBySite As Boolean)
        contentTypeSearcher.OnlyAssociatedContents = True
        Dim contentTypeColl As ContentTypeCollection = Me.BusinessContentTypeManager.Read(contentTypeSearcher)
        
        If (filterBySite = True) Then contentTypeSearcher.KeySite.Id = dwlSite.SelectedItem.Value
               
        If Not (contentTypeColl Is Nothing) Then contentTypeColl.Sort(ContentTypeGenericComparer.SortType.ByDescription, ContentTypeGenericComparer.SortOrder.ASC)
        utility.LoadListControl(dwlContentType, contentTypeColl, "Description", "Key.Id")

        dwlContentType.Items.Insert(0, New ListItem("---Select ContentType---", -1))
    End Sub
    
    'controlla l'evento che si scatena quando si seleziona un Country
    Sub SelectSiteChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindContentType(True)
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<asp:Panel id="pnl_index" runat="server">
  <asp:Button id ="btnRefresh" runat="server" Text="Refresh Index"  OnClick="RefreshIndex" CssClass="button"  style="margin-bottom:10px;"/>
  <asp:Button id ="btnReset" runat="server" Text="Reset Index"  OnClick="ResetIndex" CssClass="button"  style="margin-bottom:10px;"/>
     
    <%--Spider--%>
    <asp:Panel id="pnlSpider" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Spider (<%=GetTotalWords()%>)</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>Site</strong></td>    
                    <td><strong>Area</strong></td>
                    <td><strong>ContentType</strong></td>
                </tr>
                <tr>
                    <td><asp:DropDownList id="dwlSite" runat="server"  autopostback="true" OnSelectedIndexChanged="SelectSiteChanged"/></td>
                    <td><HP3:ctlSite runat="server" ID="ctlArea" TypeControl ="combo" SiteType="Area" ItemZeroMessage="---Select Area---"/></td>  
                    <td><asp:DropDownList ID="dwlContentType" runat="server" /></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnGo"  OnClick="GoSpider" Text="Go" style="margin-top:10px;"/></td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
 </asp:Panel>

