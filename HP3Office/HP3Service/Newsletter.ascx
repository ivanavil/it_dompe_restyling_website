<%@ Control Language="VB" ClassName="Newsletter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlNlist" Src ="~/hp3Office/HP3Parts/ctlNList.ascx"%>

<script runat="server">
    Private objQs As New ObjQueryString
    Private isNew As Boolean
    Private idContent As Int32
    Private idLanguage As Int16
    Private objCMSUtility As New GenericUtility
    Private _oNewsletterManager As NewsletterManager
    Private _oNewsletterValue As NewsletterValue
    Private _oContentManager As ContentAccManager
    Private strDomain As String
    
    Private _strDomain As String
    Private _language As String
    
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_Load()
        'Recupero dei parametri
        isNew = WebUtility.MyPage(Me).isNew
        idContent = objQs.ContentId
        idLanguage = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        strDomain = WebUtility.MyPage(Me).DomainInfo
        
        Language() = idLanguage
        Domain() = strDomain
        
        If idContent <> 0 Then
            'Carico la newsletter
            _oNewsletterManager = New NewsletterManager
            _oNewsletterManager.Cache = False
            
            _oNewsletterManager.Cache = False
            _oNewsletterValue = _oNewsletterManager.Read(New NewsletterIdentificator(idContent, idLanguage))
            
            If Not _oNewsletterValue Is Nothing Then
                'Carico i controlli che derivano da content
                LoadControl()
            End If
            
            'Tramite contentService carico i controlli nella pagina master
            'Carico il content da cui eredita content
            CType(Me.Parent.Parent, Object).LoadMasterPage(_oNewsletterValue, isNew)
        End If
    End Sub
    
    Sub LoadControl()
        With _oNewsletterValue
            'Carico la form dal newslettervalue
            'HTML.Value = .FullTextHtml
            HTML.Content = .FullTextHtml
            'web.Value = .FullTextWeb
            web.Content = .FullTextWeb
            'Plain.Value = .FullTextPlain
            Plain.Content = .FullTextPlain
            
            ctlNlists.GenericCollection = GetNLValue(.KeyNewsletter)
            ctlNlists.LoadControl()
        End With
        
    End Sub
    
    Function GetNLValue(ByVal key As NewsletterIdentificator) As NewsletterListCollection
        Dim Manager As New NewsletterManager
        Manager.Cache = False
        
        Return Manager.ReadNewsletterListRelation(key)
    End Function
    
    'gestione dei newsletter list
    Sub NListManager(ByVal key As NewsletterIdentificator)
        Dim nlManager As New NewsletterManager()
        nlManager.RemoveAllNewsletterListRelation(key)
        
        If Not ctlNlists.GetSelection Is Nothing AndAlso ctlNlists.GetSelection.Count > 0 Then
            Dim nl As New Object
            nl = ctlNlists.GetSelection
            
            For Each nlist As NewsletterListValue In nl
                nlManager.Create(key, nlist.Key)
            Next
        End If
    End Sub
    
    ''' <summary>
    ''' Esegue il salvataggio dell'oggetto
    ''' </summary>
    ''' <param name="objStore"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function save(ByVal objStore As ContentStoreValue) As ContentStoreValue
        Try
            _oNewsletterManager = New NewsletterManager
            Dim oNewsletterValue As New NewsletterValue
            
            'Esiste il content passatomi nel buffer
            If Not objStore.Content Is Nothing Then
               
                'Riverso il contentvalue nel newslettervalue
                ClassUtility.Copy(objStore.Content, oNewsletterValue)
                'Copia manuale della chiave.
                oNewsletterValue.Key = New NewsletterIdentificator(objStore.Content.Key.Id, objStore.Content.Key.IdLanguage)
               
                'Popolo l' oggetto con i dati nella form
                With oNewsletterValue
                    '.FullTextHtml = HTML.Value
                    .FullTextHtml = HTML.Content
                    '.FullTextWeb = web.Value
                    .FullTextWeb = web.Content
                    '.FullTextPlain = Plain.Value
                    .FullTextPlain = Plain.Content
                    '.RefTemplate = 3
                   
                End With
               
                'Salvo l'oggetto
                If isNew Then
                    oNewsletterValue = _oNewsletterManager.Create(oNewsletterValue)
                    NListManager(oNewsletterValue.KeyNewsletter)
                Else
                    
                    oNewsletterValue = _oNewsletterManager.Update(oNewsletterValue)
                    Dim NLvalue As NewsletterValue = _oNewsletterManager.Read(oNewsletterValue.Key)
                                     
                    NListManager(NLvalue.KeyNewsletter)
                End If
                                                        
                If Not oNewsletterValue Is Nothing Then
                    
                    'Salvataggio avvenuto con successo
                    'Salvo gli accoppiamenti
                    _oContentManager = New ContentAccManager
                    objStore = _oContentManager.Create(objStore, oNewsletterValue.Key)
                   
                   
                Else
                    objStore = Nothing
                End If
            End If
                       
        Catch ex As Exception
            Throw ex
        End Try
        
        Return objStore
    End Function
    
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>
<table style="width:80%" class="form">
    <tr>
        <td style="width:25%" valign="top"></td>
        <td></td>
   </tr>
    <tr>
        <td valign="top">
                <%If Not isNew Then%>
                             <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=&Help=cms_HelpNesletterDynamicCreation&Language=<%=Language()%>','Help','width=700,height=200,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a><input type="button" class="button" title ="Create Newsletter Wizard" value="Dynamic Creation" onclick="window.open('<%=strDomain%>/Popups/DynamicNewsletter_New.aspx?idLingua=<%=idLanguage%>&idNl=<%=idContent%>&ctlSrc=<%=HTML.clientid%>','','toolbar=0,location=0,directories=0,status=yes,menubar=0,scrollbars=yes,resizable=yes,width=600,height=500');"/>
                <%End If%>
        </td>
    </tr>
    
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListNList&Help=cms_HelpListNList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListNList", "Newsletter List")%></td>
        <td><HP3:ctlNlist runat ="server" id="ctlNlists" typecontrol="GenericRelation" /></td>
    </tr> 
    <tr>
        <td style="width:40%" valign ="top">
            <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFulltextHTML&Help=cms_HelpFulltextHTML&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFulltextHTML", "Fulltext HTML")%>
        </td>
        <td>
<%--    <FCKeditorV2:FCKeditor id="HTML" Height="300px" width="580px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>    <telerik:RadEditor ID="HTML" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
             <Content></Content>
             <Snippets>
                 <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
             </Snippets>
             <Languages>
                <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
             </Languages>

             <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             
             
             <Links>
                <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
               </telerik:EditorLink>
             </Links>
             <Links>
               <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
             </Links>        
        </telerik:RadEditor>
       </td>
     </tr>
     
    

    <tr>
        <td style="width:40%" valign ="top">    
           <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFulltextPlain&Help=cms_HelpFulltextPlain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFulltextPlain", "Fulltext Plain")%>
        </td>
    <td>
<%--        <FCKeditorV2:FCKeditor id="Plain" Height="300px" width="580px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>        <telerik:RadEditor ID="Plain" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
        
        <%--<HP3:Text ID="Plain" runat ="server" TypeControl ="TextArea" style="width:300px" />--%>
    </td>
</tr>
<tr>
    <td style="width:40%" valign ="top">    
        <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFulltextWeb&Help=cms_HelpFulltextWeb&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFulltextWeb", "Fulltext Web")%>
    </td>
    <td>
<%--        <FCKeditorV2:FCKeditor id="web" Height="300px" width="580px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>        <telerik:RadEditor ID="web" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
             <Content></Content>
             <Snippets>
                 <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
             </Snippets>
             <Languages>
                <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
             </Languages>

             <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
             
             
             <Links>
                <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
               </telerik:EditorLink>
             </Links>
             <Links>
               <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
             </Links>        
                                 
        </telerik:RadEditor>
        <%--<HP3:Text ID="web" runat ="server" TypeControl ="TextArea" style="width:300px" />--%>
    </td>
</tr>
</table>