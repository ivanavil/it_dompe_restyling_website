<%@ Control Language="VB" ClassName="ctlContextManager"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagPrefix="HP3" TagName="ctlLanguage" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private _oContextManager As ContextManager
    Private _oContextGroupSearcher As ContextGroupSearcher
    Private _oContextGroupCollection As ContextGroupCollection
    Private _oGenericUtility As New GenericUtility
    Private dictionaryManager As New DictionaryManager
    Private mPageManager As New MasterPageManager()
    Private oLManager As New LanguageManager()
    
    Private _sortType As ContextGroupGenericComparer.SortType = ContextGroupGenericComparer.SortType.ById
    Private _sortOrder As ContextGroupGenericComparer.SortOrder = ContextGroupGenericComparer.SortOrder.DESC
    
    Private strDomain As String
    Private strJs As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property

    Public Property SelectedContextGroup() As ContextGroupIdentificator
        Get
            Return ViewState("SelectedContextGroup")
        End Get
        Set(ByVal value As ContextGroupIdentificator)
            ViewState("SelectedContextGroup") = value
        End Set
    End Property

    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il ContextGroupValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadContextGroupValue() As ContextGroupValue
        If Not SelectedContextGroup Is Nothing Then
            Dim _oContextGroupValue As ContextGroupValue = _oGenericUtility.GetContextGroup(SelectedContextGroup.Id, SelectedContextGroup.Language.Id)
            Return _oContextGroupValue
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oContextGroupValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oContextGroupValue As ContextGroupValue)
        If Not oContextGroupValue Is Nothing Then
            With oContextGroupValue
                txtDomain.Text = .Key.Domain
                txtName.Text = .Key.Name
                'languageSelector.DefaultValue = .Key.Language.Id
                languageSelector.LoadLanguageValue(.Key.Language.Id)
                txtDescription.Text = .Description
                relContentType.SetSelectedValue(.KeyContentType.Id)
                ctlSiteArea.SetSelectedValue(.KeySiteArea.Id)
                ContextGroupDescrizione.InnerText = " - " & .Description
                ContextGroupId.InnerText = "ContextGroup ID: " & .Key.Id
            End With
        End If
    End Sub
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                If Not SelectedContextGroup Is Nothing AndAlso SelectedContextGroup.Id > 0 Then
                    languageSelector.Enabled = False
                Else
                    languageSelector.Enabled = True
                End If
                LoadFormDett(LoadContextGroupValue)
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContextGroup)
                End If
            Case ControlType.Selection
                rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListContextGroup)
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not Page.IsPostBack Then
            ctlSiteArea.LoadControl()
            relContentType.LoadControl()
        End If
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContextGroupSearcher = Nothing)
        _oContextManager = New ContextManager
        _oContextManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New ContextGroupSearcher
        End If
        
        _oContextGroupCollection = _oContextManager.ReadGroup(Searcher)
        If Not _oContextGroupCollection Is Nothing Then
            _oContextGroupCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = _oContextGroupCollection
        objGrid.DataBind()
    End Sub
          
    ''' <summary>
    ''' Caricamento della griglia dei ContextGroups
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
        
        '_oContextManager = New ContextManager
        _oContextGroupSearcher = New ContextGroupSearcher()
        If txtFilterId.Text <> "" Then _oContextGroupSearcher.Key = New ContextGroupIdentificator(txtFilterId.Text)
        If txtFilterDescription.Text <> "" Then _oContextGroupSearcher.Description = "%" & txtFilterDescription.Text & "%"
        
        ReadSelectedItems()
        BindGrid(objGrid, _oContextGroupSearcher)
    End Sub
    
    Sub SearchContextGroups(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListContextGroup)
    End Sub
    
    Private Property SortType() As ContextGroupGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContextGroupGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContextGroupGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As ContextGroupGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = ContextGroupGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As ContextGroupGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim ctlLang As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListContextGroup.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litContextGroupId")
            ctlLang = oRow.FindControl("litLanguage")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text & "_" & ctlLang.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ContextGroupCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curContextGroupValue As ContextGroupValue
                    Dim outContextGroupCollection As New ContextGroupCollection
            
                    For Each str As String In CheckedItems
                        Dim ctxId As Integer = str.Split("_")(0)
                        Dim ctxLang As Integer = str.Split("_")(1)
                        curContextGroupValue = _oGenericUtility.GetContextGroup(ctxId, ctxLang)
                        If Not curContextGroupValue Is Nothing Then
                            outContextGroupCollection.Add(curContextGroupValue)
                        End If
                    Next
            
                    Return outContextGroupCollection
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedContextGroup = Nothing
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListContextGroup)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        
        Dim ctxId As Integer = s.commandArgument.ToString.Split("_")(0)
        Dim ctxLang As Integer = s.commandArgument.ToString.Split("_")(1)

        ClearForm()
        Dim oContextGroupId As New ContextGroupIdentificator()
        oContextGroupId.Id = ctxId
        oContextGroupId.Language = New LanguageIdentificator(ctxLang)

        btnSave.Text = "Update"
        
        TypeControl = ControlType.Edit
        SelectedContextGroup = oContextGroupId
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""

        Select Case s.commandname
            Case "SelectItem"
                NewItem = False
        End Select
        
        ShowRightPanel()
    End Sub

    ''' <summary>
    ''' Gestione del click sul bottone Delete nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnDeleteItem(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim oContextGroupId As New ContextGroupIdentificator()
        oContextGroupId.Id = s.commandArgument.ToString.Split("_")(0)
        oContextGroupId.Language = New LanguageIdentificator(s.commandArgument.ToString.Split("_")(1))
        Dim oContextManager As New ContextManager
        oContextManager.DeleteGroup(oContextGroupId)
        goBack(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Save/Update del ContextGroup
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oContextManager As New ContextManager
        Dim oContextGroupValue As ContextGroupValue = ReadFormValues()
        
        If NewItem Then
            oContextGroupValue = oContextManager.CreateGroup(oContextGroupValue)
        Else
            oContextGroupValue = oContextManager.UpdateGroup(oContextGroupValue)
        End If
        If oContextGroupValue Is Nothing Then
            strJs = "alert('Error during saving')"
        Else
            strJs = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As ContextGroupValue
        Dim oContextGroupValue As New ContextGroupValue
        
        With oContextGroupValue
            .Key.Domain = txtDomain.Text
            .Key.Name = txtName.Text
            .Description = txtDescription.Text

            Dim ctc As ContentTypeCollection = relContentType.GetSelection()
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                .KeyContentType.Id = ctc.Item(0).Key.Id
            End If

            Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(ctlSiteArea)
            If Not oSiteAreaValue Is Nothing Then
                .KeySiteArea = oSiteAreaValue.Key
            End If

            .Key.Id = SelectedContextGroup.Id
            .Key.Language = New LanguageIdentificator(languageSelector.Language)
        End With
        
        Return oContextGroupValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo ContextGroup
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewContextGroup(ByVal s As Object, ByVal e As EventArgs)
        Dim objCMSUtility As New GenericUtility

        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedContextGroup = New ContextGroupIdentificator(0)
        SelectedContextGroup.Language = New LanguageIdentificator()
        'languageSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is CheckBox Then
                CType(ctl, CheckBox).Checked = False
            End If
        Next
        
        relContentType.SetSelectedIndex(0)
        ctlSiteArea.SetSelectedIndex(0)
        
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momento ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        ContextGroupId.InnerText = ""
        ContextGroupDescrizione.InnerText = "New ContextGroup"
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim ltlLang As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litContextGroupId")
        ltlLang = e.Row.FindControl("litLanguage")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        lnkCopy = e.Row.FindControl("lnkCopy")

        If Not lnkDelete Is Nothing Then
            lnkDelete.Attributes.Add("onClick", "if (!ConfirmDelete()) return false;")
        End If
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text & "_" & ltlLang.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListContextGroup.PageIndex = 0
        Select Case e.SortExpression
            Case "Id"
                If SortType <> ContextGroupGenericComparer.SortType.ById Then
                    SortType = ContextGroupGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> ContextGroupGenericComparer.SortType.ByDescription Then
                    SortType = ContextGroupGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> ContextGroupGenericComparer.SortType.ByKey Then
                    SortType = ContextGroupGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>
<script type="text/javascript" >  
    function GestClick(obj) {         
        var selRadio
        txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
        if (txtVal.value != '') {
            selRadio=document.getElementById (txtVal.value);
            if (selRadio!= null) selRadio.checked = false;
        }
                
        txtVal.value= obj.id                
    }
    
    function getTextVal(strText) {            
        var oTxt = document.getElementById(strText);
        return oTxt.value;
    }    

    function ConfirmDelete() {
        return confirm('Are you sure to delete this item?');
    }

    <%=strJs%>
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<%--FILTRI SUI ContextGroup--%>
<asp:Panel id="pnlGrid" runat="server">
    <table class="form">
        <tr id="rowToolbar" runat="server">
            <td  colspan="4"><asp:button ID="btnNew" runat="server" CssClass="button" Text="New" onclick="NewContextGroup" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
        </tr>
    </table>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Context Group Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td ><strong>Description</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtFilterId" TypeControl="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat="server" ID="txtFilterDescription" TypeControl="TextBox" style="width:300px"/></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" CssClass="button" onclick="SearchContextGroups" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
<%--FINE FILTRI SUI ContextGroup--%>

<%--LISTA ContextGroup--%>
<%--<asp:Table ID="gridListContextGroup" Width="100%" GridLines="Both" runat="server">
</asp:Table>
--%>

    <asp:gridview ID="gridListContextGroup" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"
        AllowSorting="true"
        PageSize ="20"        
        emptydatatext="No contextGroups available"                                                          
        OnRowDataBound="gridRowDataBound" 
        OnPageIndexChanging="gridPageIndexChanging" 
        OnSorting="gridSorting">
        
        <Columns >
            <asp:TemplateField HeaderStyle-Width="2%" ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:radiobutton ID="chkSel" runat="server" />
                    <asp:literal ID="litContextGroupId" runat="server" visible="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    <asp:literal ID="litLanguage" runat="server" Visible="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>' />
                </ItemTemplate>
            </asp:TemplateField>               
            <asp:TemplateField HeaderStyle-Width="2%" ItemStyle-Width="5%" HeaderText="Id" SortExpression="Id">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>               
            <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Domain" SortExpression="Key">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.Key.Language.Id)%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderStyle-Width="50%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
            <asp:TemplateField HeaderStyle-Width="10%">
                <ItemTemplate>
                    <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>'/>
                    <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attenzione! Sicuro di voler cancellare?')" OnClick="OnDeleteItem" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "_" & DataBinder.Eval(Container.DataItem, "Key.Language.Id")%>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:gridview>
</asp:Panel>
<%--FINE LISTA ContextGroup--%>


<%--EDIT ContextGroup--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnBack" runat="server" CssClass="button" Text="Archive" onclick="goBack" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" runat="server" CssClass="button" Text="Save" onclick="Update" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div> 
    <span id="ContextGroupId" class="title" runat ="server"/>
    <span id="ContextGroupDescrizione" class="title" runat ="server"/>
    <hr/>
    <table class="form" style="width:100%">
        <tr>
            <td width ="20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat="server" ID="txtDomain" TypeControl="TextBox" MaxLength="5" style="width:50px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat="server" ID="txtName" TypeControl="TextBox" MaxLength="10" style="width:100px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguage&Help=cms_HelpContextGroupLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguage", "Language")%></td>
            <td><HP3:ctlLanguage ID="languageSelector" Enabled="true" TypeControl="AlternativeControl" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpContextDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td><HP3:ctlContentType ID="relContentType" TypeControl="combo" ItemZeroMessage="Select ContentType" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td><HP3:ctlSite runat="server" ID="ctlSiteArea" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></td>
        </tr>
    </table>
</asp:Panel>
<%--FINE EDIT ContextGroup--%>
