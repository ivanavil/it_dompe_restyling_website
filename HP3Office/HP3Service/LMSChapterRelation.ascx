<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlContentEditorial" Src ="~/hp3Office/HP3Parts/ctlContentEditorialList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagPrefix="FCKeditorV2" namespace="FredCK.FCKeditorV2" assembly="FredCK.FCKeditorV2" %>

<script runat="server">
    Private utility As New Core.Utility.WebUtility
    Private CatalogManager As New CatalogManager
    Private LangManager As New LanguageManager
    Private MasterPageManager As New MasterPageManager
    
    Private _strDomain As String
    Private _language As String
    Private strJS As String
    Private _isNew As Boolean = False
   
    Private BlendedLearning As Integer = CourseValue.CourseTypes.BlendedLearning
    Private DistanceLearning As Integer = CourseValue.CourseTypes.DistanceLearning
    Private ResidentialCourse As Integer = CourseValue.CourseTypes.ResidentialCourse
    
    Public Property ContentId() As Integer
        Get
            Return ViewState("contentId")
        End Get
        Set(ByVal value As Integer)
            ViewState("contentId") = value
        End Set
    End Property
    
    Public Property CoursePk() As Integer
        Get
            Return ViewState("CoursePk")
        End Get
        Set(ByVal value As Integer)
            ViewState("CoursePk") = value
        End Set
    End Property
    
    Public Property LessonPk() As Integer
        Get
            Return ViewState("LessonPk")
        End Get
        Set(ByVal value As Integer)
            ViewState("LessonPk") = value
        End Set
    End Property
    
    Public Property NodePk() As Integer
        Get
            Return ViewState("NodePk")
        End Get
        Set(ByVal value As Int32)
            ViewState("NodePk") = value
        End Set
    End Property
             
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
           
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Me.Page.IsPostBack) Then
            ChapterManagement()
        End If
    End Sub
      
    
    Sub ChapterManagement()
        LoadChapter(Nothing)
        LessonPk = 0
        ActiveChapter()
        LoadChapters()
    End Sub
          
    'carica la lista dei capitoli legate alla lezione
    Sub LoadChapters()
        Dim catalogSearcher As New CatalogSearcher
        catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
        catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
        catalogSearcher.key.PrimaryKey = Request("Cat")
                      
        CatalogManager.Cache = False
        Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
       
        gdw_chapters.DataSource = catColl
        gdw_chapters.DataBind()
    End Sub
    
    'gestisce il caricamento del capitolo nel form
    Sub LoadChapter(ByVal chapter As LessonValue)
        If Not (chapter Is Nothing) Then
            'carica i dati del capitolo
            'CoursePk = chapter.Key.PrimaryKey
            'gestione ApprovalStatus
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(chapter.ApprovalStatus))

            Title.Text = chapter.Title
            SubTitle.Text = chapter.SubTitle
            Abstract.Content = chapter.Abstract
            FullText.Content = chapter.FullText
            
            'gestione della lingua
            lSelector.LoadLanguageValue(chapter.Key.IdLanguage)
            
            'gestione aree
            Aree.GenericCollection = GetSiteArea(chapter.Key.Id, chapter.Key.IdLanguage)
            Aree.IsNew = False
            Aree.LoadControl()
                               
            'gestione siti
            oSite.GenericCollection = GetSite(chapter.Key.Id, chapter.Key.IdLanguage)
            oSite.IsNew = False
            oSite.LoadControl()
            
            'Caricamento delle date
            If chapter.DateCreate.HasValue Then
                DateCreate.Value = chapter.DateCreate.Value
            Else
                DateCreate.Clear()
            End If
                                   
            If chapter.DatePublish.HasValue Then
                DatePublish.Value = chapter.DatePublish.Value
            Else
                DatePublish.Clear()
            End If
            
            txtHiddenContentType.Text = chapter.ContentType.Key.Id
            txtContentType.Text = chapter.ContentType.Description
            Objectives.Text = chapter.Objectives
            Duration.Text = chapter.Duration
            MinTime.Text = chapter.MinTime
            chkActive.Checked = chapter.Active
            chkDisplay.Checked = chapter.Display
            
            'dati del capitolo
            lessEvalTest.Checked = chapter.FinalEvaluationTest
            lessHiddenQuest.Text = chapter.KeyFinalTest.Id
            lessTxtQuest.Text = ReadQuestionnaire(chapter.KeyFinalTest.Id)
            
        Else 'nel caso di una nuovo capitolo
            'gestione ApprovalStatus
            dwlApprovalStatus.SelectedIndex = dwlApprovalStatus.Items.IndexOf(dwlApprovalStatus.Items.FindByValue(1))
            Title.Text = Nothing
            SubTitle.Text = Nothing
            Abstract.Content = Nothing
            FullText.Content = Nothing
            
            'setta la lingua per un nuovo content
            lSelector.IsNew = True
            lSelector.LoadDefaultValue()
            
            txtHiddenContentType.Text = 0
            txtContentType.Text = Nothing
            Objectives.Text = Nothing
            Duration.Text = 0
            MinTime.Text = 0
            chkActive.Checked = True
            chkDisplay.Checked = True
            lessEvalTest.Checked = False
            lessHiddenQuest.Text = 0
            lessTxtQuest.Text = Nothing
            
            'sito
            oSite.IsNew = True
            oSite.LoadControl()
            'aree
            Aree.IsNew = True
            Aree.LoadControl()
        End If
    End Sub
    
    'gestisce l'edit del capitolo
    Sub EditChapter(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SelectChapter") Then
            Dim argument As String = sender.CommandArgument
            Dim keys As String() = argument.Split("|")
            Dim PK As Integer = keys(0)
            Dim ID As Integer = keys(1)
            
            LessonPk = PK
            ContentId = ID
            
            CatalogManager.Cache = False
            Dim chapter As LessonValue = CatalogManager.ReadLesson(New CatalogIdentificator(PK))
            LoadChapter(chapter)
            ActiveChapter()
        End If
    End Sub
    
    'gestisce la cancellazione del capitolo 
    Sub DeleteChapter(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "DeleteChapter") Then
            Dim PK As String = sender.CommandArgument
            
            Dim bool As Boolean = CatalogManager.Delete(New CatalogIdentificator(PK))
            If (bool) Then
                CreateJsMessage("This chapter has been cancelled correctly.")
            Else
                CreateJsMessage("Impossible to delete this chapter.")
            End If
            
            ChapterManagement()
        End If
    End Sub
    
    Sub SaveChapter(ByVal sender As Object, ByVal e As EventArgs)
        Dim lesson As New LessonValue
        lesson.Title = Title.Text
        lesson.SubTitle = SubTitle.Text
        lesson.Abstract = Abstract.Content
        lesson.FullText = FullText.Content
        
        'gestione della lingua
        lesson.Key.IdLanguage = lSelector.Language
        ' Gestione ApprovalStatus
        Select Case dwlApprovalStatus.SelectedItem.Value
            Case LessonValue.ApprovalWFStatus.Approved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.Approved
            Case LessonValue.ApprovalWFStatus.ToBeApproved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.ToBeApproved
            Case LessonValue.ApprovalWFStatus.NotApproved
                lesson.ApprovalStatus = LessonValue.ApprovalWFStatus.NotApproved
        End Select
                     
        'date
        lesson.DateCreate = DateUpdate.Value
        lesson.DateUpdate = DateUpdate.Value
        lesson.DatePublish = DatePublish.Value
                  
        lesson.ContentType.Key.Id = txtHiddenContentType.Text
       
        lesson.Objectives = Objectives.Text
        lesson.Duration = Duration.Text
        lesson.MinTime = MinTime.Text
        lesson.Active = chkActive.Checked
        lesson.Display = chkDisplay.Checked
        lesson.FinalEvaluationTest = lessEvalTest.Checked
        
        If lessHiddenQuest.Text <> "" Then
            lesson.KeyFinalTest.Id = CType(lessHiddenQuest.Text, Integer)
        End If
        
        If (LessonPk = 0) Then
            Dim NLesson As LessonValue = CatalogManager.Create(lesson)
                                  
            If Not (NLesson Is Nothing) AndAlso (NLesson.Key.PrimaryKey > 0) Then
                Dim conRelTypeSearcher As ContentRelationTypeSearcher
                Dim contRelTypeColl As ContentRelationTypeCollection
        
                conRelTypeSearcher = New ContentRelationTypeSearcher
                conRelTypeSearcher.Domain = "LMS"
                conRelTypeSearcher.Name = "chapters"
                contRelTypeColl = Me.BusinessContentManager.ReadContentRelationtType(conRelTypeSearcher)
                
                'gestione associazione corso/lezione
                Dim CatalogRelV As New CatalogRelationValue
                CatalogRelV.KeyContent.PrimaryKey = Request("Cat")
                CatalogRelV.KeyContentRelation.PrimaryKey = NLesson.Key.PrimaryKey
                If Not (contRelTypeColl Is Nothing) AndAlso (contRelTypeColl.Count > 0) Then CatalogRelV.KeyRelationType.Id = contRelTypeColl(0).Key.Id
                CatalogManager.CreateCatalogRelation(CatalogRelV)
                
                'gestione associazione sitearee/content
                SaveSiteArea(NLesson.Key.Id, NLesson.Key.IdLanguage)
            End If
        Else
            lesson.Key.PrimaryKey = LessonPk
            CatalogManager.Update(lesson)
            
            'gestione associazione sitearee/content
            SaveSiteArea(ContentId, lesson.Key.IdLanguage)
        End If
                
        ChapterManagement()
    End Sub
   
              
    'restituisce la descrizione del questionario
    Function ReadQuestionnaire(ByVal id As Integer) As String
        If id = 0 Then Return Nothing
        
        Dim QuestManager As New QuestionnaireManager
        
        Dim QuestV As QuestionnaireValue = QuestManager.Read(New QuestionnaireIdentificator(id))
        If Not (QuestV Is Nothing) Then
            Return QuestV.Description
        End If
        
        Return Nothing
    End Function
    
    'salva le associazioni content/editorial
    Function SaveEditorial(ByVal contentId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim ContentIdent As New ContentIdentificator(contentId, languageId)
        Dim res As Boolean = Me.BusinessContentManager.RemoveAllContentEditorial(ContentIdent)
       
        If Not ctlCE.GetSelection Is Nothing AndAlso ctlCE.GetSelection.Count > 0 Then
            Dim EditorialColl As New Object
            EditorialColl = ctlCE.GetSelection
           
            For Each editorial As ContentEditorialValue In EditorialColl
                Dim editorialRel = New ContentContentEditorialValue

                editorialRel.KeyContent = ContentIdent
                editorialRel.IdContentEditorial = editorial.Id

                Me.BusinessContentManager.CreateContentEditorial(editorialRel)
            Next
        End If
    End Function
    
    'salva le associazioni content/sitearea
    Function SaveSiteArea(ByVal contentId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim ContentIdent As New ContentIdentificator(contentId, languageId)
        Dim res As Boolean = Me.BusinessContentManager.RemoveAllContentSiteArea(ContentIdent)
       
        If Not Aree.GetSelection Is Nothing AndAlso Aree.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = Aree.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
        
        If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = oSite.GetSelection
           
            For Each siteArea In AreaColl
                Dim _oContentSiteArea As New ContentSiteAreaValue
                _oContentSiteArea.KeySiteArea = siteArea.Key
                _oContentSiteArea.KeyContent = ContentIdent

                Me.BusinessContentManager.CreateContentSiteArea(_oContentSiteArea)
            Next
        End If
    End Function
      
        
    'recupera tutti le sitearea associate ad un content (corso, lezione, nodo)
    Function GetSiteArea(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Area)
    End Function
      
    'recupera tutti le sitearea di tipo sito associate ad un content (corso, lezione, nodo)
    Function GetSite(ByVal id As Int32, ByVal lang As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
        
        Dim key As New ContentIdentificator(id, lang)
        Dim siteAreaColl As New SiteAreaCollection
        siteAreaColl = Me.BusinessContentManager.ReadContentSiteArea(key)
       
        Return siteAreaColl.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Site)
    End Function
    
    'restituisce il tipo del corso
    Function getCourseType(ByVal course As CourseValue) As String
        Select Case course.CourseType
            Case CourseValue.CourseTypes.BlendedLearning
                Return "Blended Learning"
            Case CourseValue.CourseTypes.DistanceLearning
                Return "Distance Learning"
            Case CourseValue.CourseTypes.ResidentialCourse
                Return "Residential Course"
        End Select
        Return ""
    End Function
    
    Sub ContentRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ContentRel") Then
            Dim cPk As Integer = sender.CommandArgument
                
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "ContCatalg")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    Sub AddUserRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddUser") Then
            Dim argument As String = sender.CommandArgument
            Dim keys As String() = argument.Split("|")
            
            Dim cPk As Integer = keys(0)
            Dim cId As Integer = keys(1)
            
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "CourseUser")
            webRequest.customParam.append("Cat", cPk)
            webRequest.customParam.append(objQs)
            
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    'ritorna alla lista dei CORSI
    Sub GoCourseArchive(ByVal sender As Object, ByVal e As EventArgs)
        Dim webRequest As New WebRequestValue
        Dim objQs As New ObjQueryString
        
        webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "LMS")
        webRequest.customParam.append(objQs)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
    End Sub
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveCourseGrid()
        'pnlCoursesGrid.Visible = True
        
        'divSaveCourse.Visible = False
        pnlCatalogDett.Visible = False
        pnlCourseDett.Visible = False
        pnlLessonDett.Visible = False
        divSaveLesson.Visible = False
        pnlChaptersGrid.Visible = False
        'divSaveNode.Visible = False
        'pnlNodeDett.Visible = False
        'pnlNodeGrid.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveCourseDett()
        'divSaveCourse.Visible = True
        pnlCatalogDett.Visible = True
        pnlCourseDett.Visible = True
        
        'pnlCoursesGrid.Visible = False
        divSaveLesson.Visible = False
        pnlChaptersGrid.Visible = False
        pnlLessonDett.Visible = False
    End Sub
    
    'usata nella gestione dell'edit
    Sub ActiveChapter()
        divSaveLesson.Visible = True
        btnArchive_LEdit.Visible = True
        
        'btnArchive_LNew.Visible = False
        pnlCatalogDett.Visible = True
        pnlLessonDett.Visible = True
        pnlChaptersGrid.Visible = True
        
        'pnlCoursesGrid.Visible = False
        'divSaveCourse.Visible = False
        pnlCourseDett.Visible = False
        'pnlNodeGrid.Visible = False
        btnAnc_L.Visible = True
    End Sub
    
    Sub ActiveLessonDett()
        divSaveLesson.Visible = True
        'btnArchive_LNew.Visible = True
       
        btnArchive_LEdit.Visible = False
        pnlCatalogDett.Visible = True
        pnlLessonDett.Visible = True
        pnlChaptersGrid.Visible = True
        
        'pnlCoursesGrid.Visible = False
        'divSaveCourse.Visible = False
        pnlCourseDett.Visible = False
        'pnlNodeGrid.Visible = False
        'pnlNodeGrid.Visible = False
        'divSaveNode.Visible = False
        'pnlNodeDett.Visible = False
        'pnlNodeGrid.Visible = False
    End Sub
    
    'usata nella gestione dell'edit
    Sub ActiveNode()
        'divSaveNode.Visible = True
        pnlCatalogDett.Visible = True
        'pnlNodeDett.Visible = True
        
        'pnlNodeGrid.Visible = False
        pnlChaptersGrid.Visible = False
        divSaveLesson.Visible = False
        'pnlCoursesGrid.Visible = False
        pnlCourseDett.Visible = False
        pnlLessonDett.Visible = False
        'divSaveCourse.Visible = False
        'btnAnc_N.Visible = False
    End Sub
    
    Sub ActiveNodeDett()
        'divSaveNode.Visible = True
        pnlCatalogDett.Visible = True
        'pnlNodeDett.Visible = True
        'pnlNodeGrid.Visible = True
        
        pnlChaptersGrid.Visible = False
        divSaveLesson.Visible = False
        pnlLessonDett.Visible = False
        'pnlCoursesGrid.Visible = False
        pnlCourseDett.Visible = False
        'divSaveCourse.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
   
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'gestisce la ricerca degli corsi 
    'Sub SearchCourse(ByVal sender As Object, ByVal e As EventArgs)
    '    BindWithSearch(gdw_Courses)
    'End Sub
    
    
    'Sub BindWithSearch(ByVal objGrid As GridView)
    '    Dim courseSearcher As New CatalogSearcher
        
    '    If CourPk.Text <> "" Then courseSearcher.key = New CatalogIdentificator(CourPk.Text)
    '    If CourTitle.Text <> "" Then courseSearcher.SearchString = CourTitle.Text
       
    '    If (dwlTypeCourse.SelectedItem.Value <> 0) Then
    '        courseSearcher.CourseType = dwlTypeCourse.SelectedItem.Value
    '    End If
                     
    '    BindGrid(objGrid, courseSearcher)
    'End Sub
    
    'Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As CatalogSearcher = Nothing)
    '    Dim CourseCollection As New CourseCollection
    '    If Searcher Is Nothing Then
    '        Searcher = New CatalogSearcher
    '    End If
        
    '    CatalogManager.Cache = False
    '    CourseCollection = CatalogManager.ReadCourses(Searcher)
    '    'If Not CourseCollection Is Nothing Then
           
    '    '    CourseCollection.Sort(SortType, SortOrder)
    '    'End If
        
    '    objGrid.DataSource = CourseCollection
    '    objGrid.DataBind()
    'End Sub
    
  </script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<script type="text/jscript">
function goToChAnchor()
{
    location.href="#ChapterList"
    return false;
}

</script>
<hr/>
<!--Div per i bottoni Save/Archive della gestione dei capitoli -->
<div id="divSaveLesson" runat="server" visible="false">
    <asp:Button id ="btnArchive_LEdit" runat="server" Text="Archive" OnClick="GoCourseArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnSave_L" runat="server" Text="Save" OnClick="SaveChapter" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button ID="btnAnc_L" runat="server" text="Go Chapters List" EnableViewState="false" CssClass="button" OnClientClick="return goToChAnchor()" style="margin-top:10px;margin-bottom:5px" />
<hr />
</div>

<!--Pannello del dettaglio del catalog object e della lezione(generico)-->
<asp:Panel id="pnlCatalogDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="99%">
  <tr runat="server" id="tr_lSelector">
       <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentLang&Help=cms_HelpContentLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentLang", "Content Language")%></td>
            <td><HP3:contentLanguage ID="lSelector" runat="server"  Typecontrol="StandardControl" /></td>
       </tr>
          <tr id="apprStatus" runat="server" >
           <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentApr&Help=cms_HelpContentApr&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentApr", "Content Approval")%></td>
            <td>
               <asp:DropDownList id="dwlApprovalStatus" runat="server">
                    <asp:ListItem  Text="To Be Approved" Value="2"></asp:ListItem>
                    <asp:ListItem  Text="Approved" Value="1"></asp:ListItem>
                    <asp:ListItem  Text="Not Approved" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
       <tr runat="server" id="tr_DateUpdate">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateUpdate&Help=cms_HelpDateUpdate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateUpdate", "Date Update")%></td>
            <td><HP3:Date runat="server" ID="DateUpdate" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_DateCreate">
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateCreate&Help=cms_HelpDateCreate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateCreate", "Date Create")%></td>
            <td><HP3:Date runat="server" ID="DateCreate" TypeControl="JsCalendar" ShowTime="true" /></td>
        </tr>
        <tr runat="server" id="tr_DatePublish">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDatePublish&Help=cms_HelpDatePublish&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDatePublish", "Date Publish")%></td>
            <td><HP3:Date runat="server" ID="DatePublish" TypeControl="JsCalendar" ShowTime="true"/></td>
        </tr>
        <tr runat="server" id="tr_oSite">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td><HP3:ctlSite runat="server" ID="oSite" TypeControl="GenericRelation" SiteType="Site" /></td>
        </tr>
        <tr runat="server" id="tr_Aree">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="Aree" TypeControl="GenericRelation" SiteType="area"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr runat="server" id="tr_Title">
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTitle&Help=cms_HelpTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%></td>
            <td><HP3:Text runat="server" ID="Title" TypeControl="TextBox" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSubTitle&Help=cms_HelpSubTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSubTitle", "SubTitle")%> </td>
            <td><HP3:Text runat ="server" id="SubTitle" TypeControl ="TextBox" MaxLength="400" style="width:600px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAbstract&Help=cms_HelpAbstract&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAbstract", "Abstract")%> </td>
            <td>
            
<%--            <FCKeditorV2:FCKeditor id="Abstract" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>            <telerik:RadEditor ID="Abstract" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFullText&Help=cms_HelpFullText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFullText", "Full Text")%> </td>
            <td>
            
            <%--<FCKeditorV2:FCKeditor id="FullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />--%>            
                <telerik:RadEditor ID="FullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
            
            </td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormObjectives&Help=cms_HelpObjectives&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormObjectives", "Objectives")%> </td>
            <td><HP3:Text runat ="server" id="Objectives" TypeControl ="TextArea" MaxLength="400" Style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOffset&Help=cms_HelpOffset&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOffset", "Offset")%> </td>
            <td><HP3:Text runat ="server" id="Duration" TypeControl="NumericText" style="width:600px"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMinTime&Help=cms_HelpMinTime&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMinTime", "Minimun Time")%> </td>
            <td><HP3:Text runat ="server" id="MinTime" TypeControl="NumericText" Style="width:600px"/></td>
        </tr>
         
        <tr runat="server" id="tr_chkActive">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="chkActive" runat="server" /></td>
        </tr>
        <tr runat="server" id="tr_chkDisplay">
            <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="chkDisplay" runat="server"/></td>
        </tr>
 </table>
</asp:Panel>

<!--Pannello del dettaglio del corso-->
<asp:Panel id="pnlCourseDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="95%">
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCourseType&Help=cms_HelpCourseType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCourseType", "Course Type")%> </td>
        <td>
            <asp:DropDownList id="CrType" runat="server">
                <asp:ListItem Text="Distance Learning" Value="1"></asp:ListItem>
                <asp:ListItem Text="Residential Course" Value="2"></asp:ListItem>
                <asp:ListItem Text="Blended Learning" Value="3"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProvider&Help=cms_HelpProvider&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProvider", "Provider")%> </td>
        <td><HP3:Text runat ="server" id="CrProvider" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCoursePlanner&Help=cms_HelpCoursePlanner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCoursePlanner", "Course Planner")%> </td>
        <td><HP3:Text runat ="server" id="CrPlanner" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRecipients&Help=cms_HelpRecipients&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRecipients", "Recipients")%> </td>
        <td><HP3:Text runat ="server" id="CrRecipients" TypeControl ="TextArea" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormStartDate&Help=cms_HelpStartDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormStartDate", "Start Date")%> </td>
        <td><HP3:Date runat="server" id="CrStartDate" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEndDate&Help=cms_HelpEndDate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEndDate", "End Date")%> </td>
        <td><HP3:Date runat="server" id="CrEndDate" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSponsor&Help=cms_HelpSponsor&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSponsor", "Sponsor")%> </td>
        <td><HP3:Text runat ="server" id="CrSponsor" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormScientificResponsible&Help=cms_HelpScientificResponsible&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormScientificResponsible", "Scientific Responsible")%> </td>
        <td><HP3:Text runat ="server" id="CrScientificResponsible" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormScientificSegreteriat&Help=cms_HelpScientificSegreteriat&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormScientificSegreteriat", "Scientific Segreteriat")%> </td>
        <td><HP3:Text runat ="server" id="CrScientificSegreteriat" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrganizationalSegreteriat&Help=cms_HelpOrganizationalSegreteriat&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrganizationalSegreteriat", "Organizational Segreteriat")%> </td>
        <td><HP3:Text runat ="server" id="CrOrganizationalSegreteriat" TypeControl ="TextBox" MaxLength="400" Style="width:600px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCodeRequired&Help=cms_HelpCodeRequired&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCodeRequired", "Code Required")%> </td>
        <td><asp:CheckBox id="CrCodeRequired" runat="server"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOnlineRegistration&Help=cms_HelpOnlineRegistration&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOnlineRegistration", "Online Registration")%> </td>
        <td><asp:CheckBox id="CrOnlineRegistration" runat="server"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEntranceTest&Help=cms_HelpEntranceTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEntranceTest", "Entrance Test Required")%> </td>
        <td><asp:CheckBox id="CrEntranceTest" runat="server"/></td>
    </tr>
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFinalTest&Help=cms_HelpFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFinalTest", "Final Evaluation Test Required")%> </td>
        <td><asp:CheckBox id="CrFinalEvaluationTest" runat="server"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyFinalTest&Help=cms_HelpKeyFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyFinalTest", "Final Evaluation Test")%> </td>
        <td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="txtHiddenQuest" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="txtQuest"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popQuestList.aspx?sites=' +  document.getElementById('<%=txtHiddenQuest.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHiddenQuest.clientid%>&ListId=<%=txtQuest.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormTeacher&Help=cms_HelpTeacher&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTeacher", "Teacher")%> </td>
        <td><HP3:ctlContentEditorial id="ctlCE" typecontrol="GenericRelation" runat="server" /></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPreview&Help=cms_HelpPreview&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPreview", "Preview Required")%> </td>
        <td><asp:CheckBox id="CrPreview" runat="server"/></td>
    </tr>
 </table>
</asp:Panel>

<asp:Panel id="pnlLessonDett" runat="server" visible="false">
  <table  style="margin-top:10px" class="form" width="63%">
     <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFinalTest&Help=cms_HelpFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFinalTest", "Final Evaluation Test Required")%> </td>
        <td><asp:CheckBox id="lessEvalTest" runat="server"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormKeyFinalTest&Help=cms_HelpKeyFinalTest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormKeyFinalTest", "Final Evaluation Test")%> </td>
        <td>
            <table width="100%">
             <tr>
                <td style="width:50px">
                   <asp:TextBox id="lessHiddenQuest" runat="server" style="display:none"/>
                   <HP3:Text runat ="server" id="lessTxtQuest"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                </td>
                <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain()%>/Popups/popQuestList.aspx?sites=' +  document.getElementById('<%=lessHiddenQuest.clientid%>').value + '&SingleSel=1&HiddenId=<%=lessHiddenQuest.clientid%>&ListId=<%=lessTxtQuest.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
             </tr>
            </table>        
         </td>
     </tr> 
   </table>
</asp:Panel> 

<!--Pannello della lista dei capitoli-->
<a name="ChapterList"></a>
<asp:Panel id="pnlChaptersGrid" runat="server" visible="false" >
<hr />
<table  style="margin-top:10px" class="form" width="99%">
        <tr>
           <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_Form&Help=cms_Help&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_", "Chapter List")%> </td>
           <td  style="width:85%">
              <asp:gridview id="gdw_chapters" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    PageSize ="20"
                    emptydatatext="No items available">
                  <Columns >
                   <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderStyle-Width="5%" HeaderText="PK">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate><img alt="Language: <%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>" src="/HP3Office/HP3Image/Language/ico_flag_<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" /></ItemTemplate> 
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Title">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%">
                           <ItemTemplate>
                                <asp:ImageButton id="lnkSelectL" runat="server" ToolTip ="Edit" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditChapter" CommandName ="SelectChapter" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                <asp:ImageButton ID="lnkDeleteL" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteChapter" CommandName ="DeleteChapter" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>'/>
                                <img src="/<%#WebUtility.MyPage(Me).SiteFolder%>/HP3Image/ico/content_cover.gif" onclick="window.open('/<%#WebUtility.MyPage(Me).SiteFolder %>/popups/ImageCoverUpload.aspx?codeLang=<%#LangManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>&idLang=<%#Container.DataItem.Key.IdLanguage%>&idContent=<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>','CoverImage','width=550,height=350')" style="cursor:pointer" title="Image Cover" alt="" />
                           </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
            </asp:gridview>
        </td>
    </tr> 
</table>
</asp:Panel>



