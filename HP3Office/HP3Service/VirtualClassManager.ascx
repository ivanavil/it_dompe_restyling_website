<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace=" Healthware.HP3.LMS.VirtualClass.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.VirtualClass"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlProfiles" Src ="~/hp3Office/HP3Parts/ctlProfilesList.ascx"%>

<script runat="server">
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    
    Private VirtualClassManager As New VirtualClassManager
    Private UserGroupManager As New UserGroupManager
    
    Private Property SelectedId() As Integer
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
               
        If Not (Page.IsPostBack) Then
            LoadVirtualClass()
        End If
    End Sub
   
    'recupera la lista delle classi virtuali
    Sub LoadVirtualClass()
        Dim VCSearcher As New VirtualClassSearcher
        
        VirtualClassManager.Cache = False
        VCSearcher.Type = UserGroupSearcher.GroupType.VirtualClass
        Dim virtualClassColl As VirtualClassCollection = VirtualClassManager.Read(VCSearcher)
        
        gdw_VirtualClassList.DataSource = virtualClassColl
        gdw_VirtualClassList.DataBind()
        
        ActivePanelGrid()
    End Sub
             
    'cancellazione di una virtual class
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim virtualClassIdent As New UserGroupIdentificator
        Dim itemId As Integer
        
        If sender.CommandName = "DeleteItem" Then
            itemId = sender.commandArgument()
            
            virtualClassIdent.Id = itemId
            UserGroupManager.Remove(virtualClassIdent)
            
            LoadVirtualClass()
        End If
    End Sub
    
    'gestisce l'edit  della classe virtuale
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim VirtualCIdent As New UserGroupIdentificator
        Dim VCSearcher As New VirtualClassSearcher
        
        Dim itemId As Integer
        
        If sender.CommandName = "EditItem" Then
            itemId = sender.commandArgument()
            
            SelectedId = itemId
            VirtualCIdent.Id = itemId
            VCSearcher.Key = VirtualCIdent
            Dim VCColl As VirtualClassCollection = VirtualClassManager.Read(VCSearcher)
            
            If Not (VCColl Is Nothing) AndAlso (VCColl.Count > 0) Then
                Dim VCValue As VirtualClassValue = VCColl(0)
                
                If Not (VCValue Is Nothing) Then
                    h2Title.InnerHtml = "VirtualClassId - " & VCValue.Key.Id
                    lDescription.Text = VCValue.Description
                
                    'gestione ruoli
                    ctlRoles.GenericCollection = GetUserGroupRolesValue(VCValue.Key.Id)
                    ctlRoles.LoadControl()
              
                    'gestione profili
                    ctlProfiles.GenericCollection = GetUserGroupProfilesValue(VCValue.Key.Id)
                    ctlProfiles.LoadControl()
                
                    'gestione utenti
                    'ctlUsers.GenericCollection = GetUserValue(VCValue.Key.Id)
                    'ctlUsers.LoadControl()
                
                    ActivePanelDett()
                End If
            End If
        End If
    End Sub
      
    'rimanda alla lista delle classi virtuali
    Sub LoadArchive(ByVal s As Object, ByVal e As EventArgs)
        LoadVirtualClass()
    End Sub
     
    'gestisce la creazione di una nuova classe virtuale
    Sub NewRow(ByVal s As Object, ByVal e As EventArgs)
        SelectedId = 0
        
        ActivePanelDett()
        lDescription.Text = Nothing
        
        ctlRoles.LoadControl()
        ctlProfiles.LoadControl()
        'ctlUsers.LoadControl()
    End Sub
    
    'gestisce la modifica/salvataggio di una classe virtuale
    Sub UpdateRow(ByVal s As Object, ByVal e As EventArgs)
        Dim VCValue As New VirtualClassValue
       
        VCValue.Description = lDescription.Text
        VCValue.Type = UserGroupValue.GroupType.VirtualClass
        If (SelectedId <> 0) Then
            VCValue.Key.Id = SelectedId
            VCValue = VirtualClassManager.Update(VCValue)
            
            'gestione profili
            ProfilesManager(VCValue.Key)
            'gestione ruoli
            RolesManager(VCValue.Key)
            'gestione user
            'UsersManager(VCValue.Key)
        Else
            VCValue = VirtualClassManager.Create(VCValue)
            'gestione profili
            ProfilesManager(VCValue.Key)
            'gestione ruoli
            RolesManager(VCValue.Key)
            'gestione user
            'UsersManager(VCValue.Key)
        End If
        
        LoadVirtualClass()
    End Sub
    
    'gestione delle associazioni profili-gruppo
    Sub ProfilesManager(ByVal key As UserGroupIdentificator)
        'cancella tutte le relazioni usergroup-profili dato un certo gruppo
        UserGroupManager.RemoveUserGroupProfileRelation(key)
        
        If Not ctlProfiles.GetSelection Is Nothing AndAlso ctlProfiles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim profile As New ProfileValue
            Pc = ctlProfiles.GetSelection
            
            For Each profile In Pc
                userGroupManager.CreateUserGroupProfileRelation(key, New ProfileIdentificator(profile.Key.Id))
            Next
        End If
    End Sub
    
    'gestione delle associazioni ruoli-gruppo 
    Sub RolesManager(ByVal key As UserGroupIdentificator)
        'cancella tutte le relazioni usergroup-ruoli dato un certo gruppo
        userGroupManager.RemoveUserGroupRoleRelation(key)
        
        If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim role As New RoleValue
            Pc = ctlRoles.GetSelection
            
            For Each role In Pc
                UserGroupManager.CreateUserGroupRoleRelation(key, New RoleIdentificator(role.Key.Id))
            Next
        End If
    End Sub
    
    'gestione delle associazioni utenti-gruppo
    'Sub UsersManager(ByVal key As UserGroupIdentificator)
    '    'cancella tutte le relazioni usergroup-utente dato un certo gruppo
    '    userGroupManager.RemoveUserRelation(key)
        
    '    If Not ctlUsers.GetSelection Is Nothing AndAlso ctlUsers.GetSelection.Count > 0 Then
    '        Dim Pc As New Object
    '        Dim user As New UserValue
    '        Pc = ctlUsers.GetSelection
            
    '        For Each user In Pc
    '            userGroupManager.CreateUserGroupRelation(New UserIdentificator(user.Key.Id), key)
    '        Next
    '    End If
    'End Sub
    
    'recupera tutti i ruoli associati ad un gruppo di utenti
    Function GetUserGroupRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        Me.BusinessProfilingManager.Cache = False
        Return Me.BusinessProfilingManager.ReadUserGroupRoles(key)
    End Function
       
    'recupera tutti i profili associati ad un gruppo di utenti
    Function GetUserGroupProfilesValue(ByVal id As Int32) As ProfileCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        Me.BusinessProfilingManager.Cache = False
        Return Me.BusinessProfilingManager.ReadUserGroupProfiles(key)
    End Function
        
    'recupera tutti gli utenti associati ad un gruppo 
    Function GetUserValue(ByVal id As Int32) As UserCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        userGroupManager.Cache = False
        Return userGroupManager.ReadUserRelated(key)
    End Function
    
    Sub AddUserRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddUser") Then
            Dim vcId As String = sender.CommandArgument
                      
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "VClassUser")
            webRequest.customParam.append("V", vcId)
            webRequest.customParam.append(objQs)
            
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    Sub AddCourseRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddCourse") Then
            Dim vcId As String = sender.CommandArgument
                      
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "VClassCourse")
            webRequest.customParam.append("V", vcId)
            webRequest.customParam.append(objQs)
            
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    'gestisce la ricerca delle virtual class
    Sub SearchClass(ByVal sender As Object, ByVal e As EventArgs)
        BindWithSearch(gdw_VirtualClassList)
    End Sub
    
    'Caricamento della griglia delle classi virtuali
    'Verifica la presenza di criteri di ricerca
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim VCSearcher As New VirtualClassSearcher
        
        If txtId.Text <> "" Then VCSearcher.Key = New UserGroupIdentificator(txtId.Text)
        If txtDescription.Text <> "" Then VCSearcher.Description = txtDescription.Text
        VCSearcher.Type = UserGroupSearcher.GroupType.VirtualClass
        
        BindGrid(objGrid, VCSearcher)
    End Sub
        
    'Esegue il bind della griglia
    'data la griglia stessa e l'eventuale searcher    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As VirtualClassSearcher = Nothing)
        Dim VCCollection As New VirtualClassCollection
  
        If Searcher Is Nothing Then
            Searcher = New VirtualClassSearcher
        End If
        
        VirtualClassManager.Cache = False
        VCCollection = VirtualClassManager.Read(Searcher)
               
        objGrid.DataSource = VCCollection
        objGrid.DataBind()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<asp:Panel id="pnlGrid" runat="server">
  <asp:button ID="btnNew" runat="server" Text="New" onclick="NewRow" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /><br /><br />
 
     <%--FILTRI sulle classi virtuali--%>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Virtual Class Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Name</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat ="server" id="txtDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchClass" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview id="gdw_VirtualClassList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                OnPageIndexChanging="gridListUsers_PageIndexChanging"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Name">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Restore item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkUserRelation" ToolTip ="Add Users" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_user.gif" onClick="AddUserRelation" CommandName ="AddUser" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                            <asp:ImageButton ID="lnkCourseRelation" ToolTip ="Add Courses" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/type_course_small.gif" onClick="AddCourseRelation" CommandName ="AddCourse" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
  
  <br /><h2 id="h2Title" runat="server"  class="title"/>
 <table  style=" margin-top:10px" class="form" width="99%">
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpUserGrDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Description")%> </td>
        <td><HP3:Text ID ="lDescription" runat="server"  TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProfiles&Help=cms_HelpUserGroupProfile&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProfiles", "Profiles")%></td>
        <td><HP3:ctlProfiles runat ="server" id="ctlProfiles" typecontrol="GenericRelation" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpUserGroupRole&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
        <td><HP3:ctlRoles runat ="server" id="ctlRoles" typecontrol="GenericRelation" /></td>
   </tr>
 </table>
</asp:Panel>
