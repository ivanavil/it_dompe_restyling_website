<%@ Page Language="VB" ClassName="myAsync" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Search" %>
<%@ Import Namespace="Healthware.HP3.Core.Search.ObjectValues" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
    Sub Page_Load()
        Try
            If Not Request("asyncType") Is Nothing AndAlso Request("asyncType") <> String.Empty Then
                Dim strRequest As String = Request("asyncType")
                Select Case strRequest.ToLower()
                                      
                    Case "indexstatus"
                        IndexStatus()
                      
                End Select
            Else
                Response.End()
            End If
        Catch ex As Exception
            Response.Write("Errore nel page load -> " & ex.ToString)
        End Try
    End Sub
    
    
    Sub IndexStatus()
        Dim SearchEngine As New SearchEngine
        Dim strUtility As New StringManipolator
        
        Dim so As New ContentSearcher
            
        Dim siteId As String = Request("siteId")
        Dim contentTypeId As String = Request("conTypeId")
        Dim siteAreaId As String = Request("siteAreaId")
        Dim LanguageId As String = Request("LanguageId")
        Dim keysContentType As String = Request("KeysCtype")
        
        
        'Response.Write("siteId " & siteId & " " & contentTypeId & " " & siteAreaId & " " & LanguageId & " " & keysContentType)
        'Response.End()
        Try
            If (LanguageId <> "-1") Then so.key.IdLanguage = LanguageId
            
            If (contentTypeId <> "-1") Then
                so.KeyContentType.Id = contentTypeId
                
            ElseIf (keysContentType <> "-1") Then
                so.KeysContentType = keysContentType.ToString
            End If
           
            If (siteAreaId <> "0") Then so.KeySiteArea.Id = siteAreaId
            
            If (siteId <> "-1") Then
                so.KeySite.Id = siteId
                    
                Dim item As Integer = SearchEngine.ReadLogItemsToBeProcessed(so)
               
                If item = 0 Then
                    Response.Write("0")
                ElseIf item > 0 Then
                    Response.Write("In progress...! Remain " & item.ToString & " contents to be processed. ")
                End If
            End If
            
           
        Catch ex As Exception
            Response.Write("-1")
        End Try
    End Sub
    
    
    
 
    
     
</script>

