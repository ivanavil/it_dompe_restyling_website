<%@ Control Language="VB" ClassName="ContextContextGroups" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Context"%>
<%@ Import Namespace="Healthware.HP3.Core.Context.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Context"%>
<%@ Import Namespace="Healthware.HP3.Core.Context.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue
    Private _webUtility As WebUtility
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    
    Private oLManager As New LanguageManager()
    
    ' il datasource corrente
    Private _dataSource As ContextGroupCollection = Nothing
    ' la lista degli id che hanno una relazione con il context
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    ' l'id del context corrente
    Private _keyContext As ContextIdentificator
    ' l'istanza del Context corrente
    Private _contextValue As ContextValue


    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    Public Property KeyContext() As ContextIdentificator
        Get
            If _keyContext Is Nothing Then
                _keyContext = New ContextIdentificator()
                webRequest.customParam.LoadQueryString()
                If webRequest.customParam.item("KeyContext") <> String.Empty Then _keyContext.Id = Integer.Parse(webRequest.customParam.item("KeyContext"))
                If webRequest.customParam.item("ContextLanguage") <> String.Empty Then _keyContext.Language = New LanguageIdentificator(Integer.Parse(webRequest.customParam.item("ContextLanguage")))
            End If
            Return _keyContext
        End Get
        Set(ByVal value As ContextIdentificator)
            _keyContext = value
        End Set
    End Property
    
    Private Sub readContextValue()
        Dim _contextManager As New ContextManager
        _contextManager.Cache = False
        _contextValue = _contextManager.Read(KeyContext)
    End Sub

    Public Property ContextValue() As ContextValue
        Get
            If (_contextValue Is Nothing OrElse _contextValue.Key.Id = 0) AndAlso Not KeyContext Is Nothing Then
                readContextValue()
            End If
            Return _contextValue
        End Get
        Set(ByVal value As ContextValue)
            _contextValue = value
        End Set
    End Property
    
    Public ReadOnly Property Related() As List(Of Integer)
        Get
            If _related Is Nothing Then
                If Not UseInPopup Then
                    _related = readRelated(KeyContext)
                Else
                    _related = readSelection(KeyContext)
                End If
            End If
            Return _related
        End Get
    End Property

    Private ReadOnly Property KeyChanges() As String
        Get
            Dim _key As String
            _key = ViewState("RelatedChangesKey")
            If _key Is Nothing Then
                ' Genera una chiave random per assicurarsi che lo stato
                ' delle modifiche cambi tra una richiesta e l'altra
                _key = SimpleHash.GenerateUUID(32)
                ViewState("RelatedChangesKey") = _key
            End If
            Return _key
        End Get
    End Property

    Public Property RelatedChanges() As Dictionary(Of Integer, Boolean)
        Get
            If _changes Is Nothing Then
                _changes = Cache.Item(KeyChanges)
                If _changes Is Nothing Then
                    _changes = New Dictionary(Of Integer, Boolean)
                End If
            End If
            Return _changes
        End Get
        Set(ByVal value As Dictionary(Of Integer, Boolean))
            _changes = value
            Cache.Insert(KeyChanges, _changes, DateTime.Now.AddMinutes(10), True)
        End Set
    End Property

    Public Function checkRelated(ByVal idRelation As Integer) As Boolean
        Return checkRelated(idRelation, False)
    End Function

    Public Function checkRelated(ByVal idRelation As Integer, ByVal onlyPreviouslyRelated As Boolean) As Boolean
        ' Verifica se il datasource degli elementi relazionati contiene la chiave corrente
        If Not Related Is Nothing AndAlso Related.Contains(idRelation) Then
            ' se si verifica che non sia stata deselezionata dall'utente
            If Not onlyPreviouslyRelated AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
                Return RelatedChanges.Item(idRelation)
            Else
                ' non � stata deselezionata
                Return True
            End If
        ElseIf Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
            ' Se la chiave non era precedentemente selezionata, verifica se � stata selezionata dall'utente
            If Not onlyPreviouslyRelated Then
                Return RelatedChanges.Item(idRelation)
            Else
                Return False
            End If
        End If
    End Function
    
    'Public Property ShowRelatedSearch() As Boolean
    '    Get
    '        Return pnlRelatedSearch.Visible
    '    End Get
    '    Set(ByVal value As Boolean)
    '        pnlRelatedSearch.Visible = value
    '    End Set
    'End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(True)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)

        ' Legge il datasource completo 
        getDataSource(clearCache)

        ' Controlla se visualizzare solo gli elementi con relazioni
        'If chkShowOnlyRelatedItems.Checked Then

        '    ' procede con un ciclo per escludere gli elementi non relazionati 
        '    ' (al momento non � possibile realizzare tale operazione in maniera pi� efficiente)
        '    Dim idRelation As Integer
        '    Dim removeIndex As Integer = 0
        '    Dim item As Object
        '    For i As Integer = 0 To _dataSource.Count - 1
        '        item = _dataSource.Item(removeIndex)
        '        idRelation = DataBinder.Eval(item, "Key.Id")
        '        ' se l'elemento non � relazionato ...
        '        If Not checkRelated(idRelation) Then
        '            ' ... lo esclude
        '            _dataSource.Remove(item)
        '        Else
        '            ' passa all'elemento successivo
        '            removeIndex = removeIndex + 1
        '        End If
        '    Next
        'End If

        ' Effettua il bind del datasource sulla gridview
        gridContextGroup.DataSource = _dataSource
        gridContextGroup.DataBind()
    End Sub

    'Public Sub SaveRelatedStatus()
    '    ' flag che indica se � stato apportato un cambiamento alle relazioni
    '    Dim isChanges As Boolean = False
                
    '    ' controlla le modifiche effettuate sulla griglia
    '    If gridContextGroup.Rows.Count = 0 Then Return
    '    Dim check As CheckBox
    '    Dim radio As RadioButton
    '    Dim idRelation As Integer
    '    Dim checked As Boolean = False
    '    ' legge ogni riga della griglia
    '    For Each row As GridViewRow In gridContextGroup.Rows
    '        ' azzera idrelation
    '        idRelation = -1
    '        ' estrae il checkbox
    '        If AllowEdit Then
    '            check = row.FindControl("chkRelation")
    '            If Not check Is Nothing AndAlso check.Text <> "" Then
    '                ' la propriet� text del checkbox contiene l'id della relazione
    '                idRelation = Integer.Parse(check.Text)
    '                checked = check.Checked
    '            End If
    '        Else
    '            radio = row.FindControl("optRelation")
    '            If Not radio Is Nothing AndAlso radio.Text <> "" Then
    '                ' la propriet� text del checkbox contiene l'id della relazione
    '                idRelation = Integer.Parse(radio.Text)
    '                checked = radio.Checked
    '            End If
    '        End If

    '        If idRelation > -1 Then
    '            ' verifica se lo stato della relazione attuale � cambiato rispetto a quello iniziale
    '            If (checkRelated(idRelation, True) AndAlso Not checked) OrElse (Not checkRelated(idRelation, True) AndAlso checked) Then
    '                ' aggiorna l'hashmap dei cambiamenti
    '                If RelatedChanges.ContainsKey(idRelation) Then
    '                    RelatedChanges.Item(idRelation) = checked
    '                Else
    '                    If Not AllowEdit And checked Then
    '                        RelatedChanges.Clear()
    '                        RelatedChanges.Add(idRelation, checked)
    '                    ElseIf AllowEdit Then
    '                        RelatedChanges.Add(idRelation, checked)
    '                    End If
    '                End If
    '                ' imposta il flag
    '                isChanges = True
    '            ElseIf checkRelated(idRelation, True) <> checkRelated(idRelation, False) Then
    '                ' aggiorna l'hashmap dei cambiamenti
    '                RelatedChanges.Remove(idRelation)
    '                ' imposta il flag
    '                isChanges = True
    '            End If
    '        End If
    '    Next

    '    ' se � stato cambiato qualcosa nelle relazioni ...
    '    If isChanges Then
    '        ' ... fa si che il valore della property venga cachato
    '        RelatedChanges = RelatedChanges
    '    End If
    '    ' abilita il tasto apply associations a lato client (js)
    '    If AllowEdit AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then
    '        Dim strJs As String = "<script type='text/javascript'>enableApplyButton();<" & "/script>"
    '        Page.ClientScript.RegisterStartupScript(GetType(String), "enableApplyButton", strJs)
    '    End If
    'End Sub
    
   

    'Protected Sub chkShowOnlyRelatedItems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowOnlyRelatedItems.CheckedChanged
    '    If Not AllowEdit Then Return

    '    SaveRelatedStatus()

    '    _dataSource = Nothing
        
    '    ' memorizza l'indice della pagina per ricaricarlo quando si cambia il tipo di visualizzazione
    '    ' tra quello completo (full) e quello contenente solo le relazioni attive (onlyRelated)
    '    If chkShowOnlyRelatedItems.Checked Then
    '        ViewState("fullPageIndex") = gridContextGroup.PageIndex
    '        gridContextGroup.PageIndex = ViewState("onlyRelatedPageIndex")
    '    Else
    '        ViewState("onlyRelatedPageIndex") = gridContextGroup.PageIndex
    '        gridContextGroup.PageIndex = ViewState("fullPageIndex")
    '    End If
    '    BindGrid(True)
    'End Sub

    Protected Sub gridRelations_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridContextGroup.DataBound

        If gridContextGroup.FooterRow Is Nothing Then Return
     
        For cellNum As Integer = gridContextGroup.Columns.Count - 1 To 1 Step -1
            Try
                gridContextGroup.FooterRow.Cells.RemoveAt(cellNum)
            Catch ex As Exception
            End Try
        Next
        gridContextGroup.FooterRow.Cells(0).ColumnSpan = gridContextGroup.Columns.Count
        gridContextGroup.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        If Not _dataSource Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
            Dim startIndex As Integer = gridContextGroup.PageSize * gridContextGroup.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            gridContextGroup.FooterRow.Cells(0).Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", _
                s, startIndex + 1, startIndex + gridContextGroup.Rows.Count, gridViewTotalCount, s)
        Else
            gridContextGroup.FooterRow.Cells(0).Text = "No items found."
        End If
    End Sub

   

    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridContextGroup
        End Get
    End Property

    Private Sub LoadContextSummary()
        
        If KeyContext.Id > 0 Then
            lblContextId.Text = ContextValue.Key.Id
            If Not ContextValue.Key.Language Is Nothing Then
                imgContextLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContextValue.Key.Language.Id) & "_small.gif"
                imgContextLanguage.Visible = True
            Else
                imgContextLanguage.Visible = False
            End If
            lblContextDescription.Text = ContextValue.Description
        Else
            imgContextLanguage.Visible = False
        End If

    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not AllowEdit Then
            'pnlHeader.Visible = False
            ContextSummary.Visible = False
        End If
        
        If Not Page.IsPostBack Then
            LoadContextSummary()
            ReadRelations()
        End If
        
        If KeyContext.Id = 0 And Not UseInPopup Then
            Dim _webUtility As New WebUtility
            pnlRelationEdit.Visible = False
        Else
            If Not Page.IsPostBack Then
                LoadControl()
            End If
        End If
       
    End Sub
    
    Private Function getDataSource() As ContextGroupCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As ContextGroupCollection
        'If _dataSource Is Nothing Then
        Dim _ContextGroupSearcher As New ContextGroupSearcher()
        If Not DataValueFilter Is Nothing AndAlso DataValueFilter.Trim <> "" Then
            _ContextGroupSearcher.Key = New ContextGroupIdentificator(Integer.Parse(DataValueFilter))
        End If
        If Not DataTextFilter Is Nothing AndAlso DataTextFilter.Trim <> "" Then
            _ContextGroupSearcher.Description = "%" & DataTextFilter & "%"
        End If
        Dim _oContextManager As New ContextManager()
        _oContextManager.Cache = False
        _dataSource = _oContextManager.ReadGroup(_ContextGroupSearcher)
 
        'End If
        Return _dataSource
    End Function
    
    Private Function readSelection(ByVal keyContext As ContextIdentificator) As System.Collections.Generic.List(Of Integer)
        If Not Request("txtHidden") Is Nothing AndAlso Request("txtHidden") <> "" Then
            _related = New List(Of Integer)
            _related.Add(Request("txtHidden"))
            RelatedChanges.Clear()
            RelatedChanges.Add(Request("txtHidden"), True)
            Return _related
        Else
            Return Nothing
        End If
        
    End Function

    Private Function readRelated(ByVal keyContext As ContextIdentificator) As System.Collections.Generic.List(Of Integer)
        
        If _related Is Nothing AndAlso Not keyContext Is Nothing Then
            Dim _oContextGroupRelatedCollection As ContextGroupCollection = Nothing
            Dim _oContextManager As New ContextManager
            _oContextManager.Cache = False
            _oContextGroupRelatedCollection = _oContextManager.ReadGroupContextRelation(keyContext)
            If Not _oContextGroupRelatedCollection Is Nothing Then
                _related = New List(Of Integer)
                Dim _contextGroup As ContextGroupValue
                For Each _contextGroup In _oContextGroupRelatedCollection
                    _related.Add(_contextGroup.Key.Id)
                Next
            End If
        End If
        Return _related
    End Function
    
    Private Overloads Sub LoadControl()
        Dim _oContextGroupCollection As ContextGroupCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
        Dim _oContextManager As New ContextManager

        readRelated(KeyContext)
        
        bindGrid()
        pnlRelationEdit.Visible = True

    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub

    'Protected Sub ApplyAssociations(ByVal sender As Object, ByVal e As System.EventArgs)
        
    '    If Not AllowEdit Then Return
        
    '    SaveRelatedStatus()
        
    '    If Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then

    '        Dim _oContextManager As New ContextManager
    '        Dim key As Integer
    '        Dim value As Boolean
    '        Dim _oKeyContextGroup As ContextGroupIdentificator
    '        For Each key In RelatedChanges.Keys
    '            value = RelatedChanges.Item(key)
    '            _oKeyContextGroup = New ContextGroupIdentificator(key)
    '            If value Then
    '                _oContextManager.CreateGroupContextRelation(_oKeyContextGroup, KeyContext)
    '            Else
    '                _oContextManager.RemoveGroupContextRelation(_oKeyContextGroup, KeyContext)
    '            End If
    '        Next
            
    '        readRelated(KeyContext)

    '        'Return True
    '    End If
        
    '    'Return False
    'End Sub
    
    'aggiunge una nuova relazione contextgroup/context
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oKeyContextGroup As ContextGroupIdentificator = New ContextGroupIdentificator(key)
              
        Me.BusinessContextManager.CreateGroupContextRelation(_oKeyContextGroup, KeyContext)
        ReadRelations()
    End Sub
     
    'rimuove una relazione contextgroup/context
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim key As String = sender.commandArgument.ToString
        Dim _oKeyContextGroup As ContextGroupIdentificator = New ContextGroupIdentificator(key)
        
        Me.BusinessContextManager.RemoveGroupContextRelation(_oKeyContextGroup, KeyContext)
        
        ReadRelations()
    End Sub
    
    'legge le relazioni contextgroup/context
    Sub ReadRelations()
        BusinessContextManager.Cache = False
        Dim _oContextGroupRelatedCollection As ContextGroupCollection = Me.BusinessContextManager.ReadGroupContextRelation(KeyContext)
               
        gridActiveRelation.DataSource = _oContextGroupRelatedCollection
        gridActiveRelation.DataBind()
      
       
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        'setto webRequest con i parametri corretti 
        'in modo da rimandare l'utente al servizio da cui proviene 
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
       
        'Dim wr As New WebRequestValue
        'Dim ctm As New ContentTypeManager()
        'Dim mPage As New MasterPageManager()
        'Dim objQs As New ObjQueryString
        'wr.KeycontentType = New ContentTypeIdentificator("CMS", "ContextM")
        'wr.customParam.append(objQs)
        'Response.Redirect(mPage.FormatRequest(wr))
    End Sub
    
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridContextGroup.PageIndexChanging
        'SaveRelatedStatus()
        gridContextGroup.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        'SaveRelatedStatus()
        filterDataSource()
    End Sub
</script>

<%--<script type="text/javascript">
    function enableApplyButton(state) {
        if (state == null) state = true;
        try {
            document.getElementById('<%=btApply.ClientId %>').disabled = !state;
        } catch (e) {
        }
    }
</script>--%>

<hr />
<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="btNewRelation" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
     <%--Pannello del Content Selezionato--%>  
     <asp:Panel ID="ContextSummary" Width="100%" runat="server">
        <div class="title">Context Selected</div> <br />
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:5%"></th>
                <th style="width:90%">Description</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContextId" runat="server" /></td>
                <td><asp:Image ID="imgContextLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContextDescription" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <%--Pannello delle Relazioni attive--%>
    <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div>  <br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Description")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="2%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />
    
    <%-- Pannello User Searcher--%>
    <asp:Panel ID="Panel1" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">ContextGroup Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Description</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText"  Style="width:50px"/></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" Style="width:300px"/></td>
           </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <%-- <div class="title">Related ContextGroups</div>

    <asp:Panel ID="pnlHeader" runat="server">
        <asp:CheckBox ID="chkShowOnlyRelatedItems" Text="Show only active relations" runat="server" Visible="<%#AllowEdit.toString() %>" AutoPostBack="true" />
        <asp:Button ID="btBackToContextManager" Text="Context Manager" CssClass="button" runat="server" OnClick="BackToContextManager" />
        <asp:Button ID="btApply" Text="Apply Associations" CssClass="button" runat="server" Enabled="false" OnClick="ApplyAssociations" />
    </asp:Panel>--%>

    <asp:GridView ID="gridContextGroup" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server">
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                   <%-- <%If AllowEdit Then%>
                        <asp:CheckBox ID="chkRelation" onClick="enableApplyButton()" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%Else%>
                        <asp:RadioButton ID="optRelation" onClick="GestClick(this)" GroupName="optRelations" runat="server" Checked='<%#CheckRelated(DataBinder.Eval(Container.DataItem, "Key.Id")) %>' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' />
                    <%End If%>--%>
                     <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <%--<asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Description")%>' />--%>
                    <%#DataBinder.Eval(Container.DataItem, "Description")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Panel>