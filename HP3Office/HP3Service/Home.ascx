<%@ Control Language="VB" Debug="true" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%> 
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<script language="VB" runat="server">
    
    Public webRequest As New WebRequestValue
    
    
    Public mPageManager As New MasterPageManager()
    Public accService As New AccessService()
   
 
    Public idCurrentLang As Integer = accService.GetTicket.Travel.KeyLang.Id
    
    Public oContentManager As New ContentManager()
     
    Private idsite As Integer
    Private sortType As ContentGenericComparer.SortType = ContentGenericComparer.SortType.ById
    Private sortOrder As ContentGenericComparer.SortOrder = ContentGenericComparer.SortOrder.DESC
    
    Private oLManager As New LanguageManager()
        
    

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        
            
            idsite = Request.QueryString("s")
            Dim oContentSearcher As New ContentSearcher()
            'oContentSearcher.Properties.Add("Title")
        oContentSearcher.SetMaxRow = 5
        'oContentSearcher.PageSize = 5
        'oContentSearcher.PageNumber = 1
        
            oContentSearcher.KeySite.Id = idsite
		
		
            'oContentSearcher.KeySiteArea.Name = "pm"
            oContentSearcher.SortOrder = ContentGenericComparer.SortOrder.DESC
            oContentSearcher.SortType = ContentGenericComparer.SortType.ById
        
            'oContentSearcher.SetMaxRow = 15
        Dim oContentCollection As ContentCollection
       oContentManager.Cache = False
            oContentCollection = oContentManager.Read(oContentSearcher)
            If Not oContentCollection Is Nothing Then
                GridContents.DataSource = oContentCollection
                GridContents.DataBind()
            End If
    End Sub
    
    'Sub ItemCommand(ByVal s As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        
    '    Dim param = s.commandArgument.split(",")
    '    Dim lingua As String = param(0)
    '    Dim primarykey As String = param(1)
    '    Dim IDD As String = param(2)
    '    Dim SiteareaID As String = param(3)
    '    Dim ContentTypeID As String = param(4)
    '    Dim siteAreaName As String = param(5)
    '    Dim siteName As String = param(6)
        
        
    '    Response.Write(lingua & "-")
    '    Response.Write(primarykey & "-")
    '    Response.Write(IDD & "-")
    '    Response.Write(SiteareaID & "-")
    '    Response.Write(ContentTypeID & "-")
    '    Response.Write(siteAreaName & "-")
    '    Response.Write(siteName & "-")
    '    Response.Write(idsite)
        
    '    Dim themeMan As New ThemeManager
    '    Dim siteArea As New SiteAreaIdentificator
    '    Dim contentId As New ContentTypeIdentificator
        
    '    'Dim th As ThemeValue = Me.BusinessThemeManager.Read(New SiteAreaIdentificator(SiteareaID), New ContentTypeIdentificator(ContentTypeID))
    
        
    '    Dim vo As New ContentValue
      
    '    Dim wr As New WebRequestValue
    '    'Dim ctm As New ContentTypeManager()
    '    Dim mPage As New MasterPageManager()
    '    Dim objQs As New ObjQueryString
    '    'Dim vo As New ContentValue
       
    '    wr.KeyLang = Me.PageObjectGetLang
    '    wr.KeycontentType = New ContentTypeIdentificator(ContentTypeID)
    '    wr.KeysiteArea = New SiteAreaIdentificator(SiteareaID)
    
    '    '?page=1&l=1&ct=11&c=4927&sa=3&s=1&t=16&n=0        
    '    wr.customParam.append("Page", "1")
    '    objQs.Language_id = lingua
    '    objQs.ContentType_Id = ContentTypeID
    '    objQs.ContentId = IDD
    '    objQs.SiteArea_Id = SiteareaID
    '    objQs.Site_Id = idsite
    '    ' objQs.Theme_id = th.Key.Id
        
    '    ''objQs = Int32.Parse(contentID)
    '    wr.customParam.append(objQs)
    '    wr.customParam.append("n", "0")
    '    Response.Redirect(mPage.FormatRequest(wr))
    'End Sub
    
   
</script>

<div class="dMain">
<div>&nbsp;</div>
	<div class="dMiddleBox">
	    <div style="float:left;font-size:x-large;width:50%;line-height:50px;">
    	<%--<p><strong>5</strong> Site</p>
    	<p><strong>798</strong> Content</p>
    	<p><strong>157</strong> Product</p>--%>
    	<p>Last Content Loaded:</p>
	    </div>
	
	    <div class="" style="font-size:x-large;width:40%;line-height:50px;">
	    <%--<p><strong>514</strong> Registered Users</p>
	    <p><strong>45</strong> On-Line Users</p>
	    <p><strong>365</strong> Cached Object</p>--%>
	    <p>&nbsp;</p>
	    </div>
	    <div class="clearer">&nbsp;</div>
		<div class="" style="float:left;">			
			<asp:GridView         
            ID="GridContents" AutoGenerateColumns="false" CssClass="tbl1"
            AlternatingRowStyle-BackColor="#EEEFEF"
            AllowPaging="false"
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            EnableViewState="false"
            emptydatatext="No content available"
            

            
            GridLines="None">
            <Columns>
             <asp:TemplateField HeaderText=" " ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DatePublish" HeaderText="Date Pub." SortExpression="DatePublish" HeaderStyle-Width="10%" ItemStyle-Width="10%" HtmlEncode="False" DataFormatString="{0:d}"/>
            <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(Container.DataItem.Key.IdLanguage)%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HtmlEncode="false" DataField="Title" HeaderText="Title" SortExpression="Title" HeaderStyle-Width="60%" ItemStyle-Width="60%"/>

            <asp:TemplateField HeaderText="Service" HeaderStyle-Width="40%" ItemStyle-Width="20%">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ContentType.Description")%></ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderStyle-Width="15%">
                    <ItemTemplate>
                        
                        <asp:ImageButton ID="Siteassociation" runat="server"  ImageUrl="~/hp3Office/HP3Image/ico/content_content.gif"    onClick="ItemCommand"  CommandArgument ='<%#Container.DataItem.Key.IdLanguage.toString()&"," & Container.Dataitem.Key.primarykey.toString()&","& Container.Dataitem.Key.Id.toString()&","& Container.Dataitem.SiteArea.Key.Id.toString()&","& Container.Dataitem.ContentType.Key.Id.toString()&","& Container.Dataitem.SiteArea.Key.Name.toString()&","& Container.Dataitem.ContentType.Key.Name.toString() %>'/>
                   
                    </ItemTemplate>
                </asp:TemplateField>--%>

            </Columns>
            </asp:GridView>
	        
		</div>
		<div align="center"></div>
        	
	</div>
</div>



<div class="dFooterBoxesContainer">	
	<div class="clearer"></div>
</div>
	





