<%@ Control Language="VB" Debug="true" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<script runat="Server">
    Public cTypeManager As New ContentTypeManager
    Public siteManager As New SiteManager
	Sub Page_Load()
        Dim accService As New AccessService
        
        If accService.IsAuthenticated Then
            lTxt.Text = "Non hai i permessi di accesso per quest'area"
        Else
            lTxt.Text = "Non sei autenticato, quest'area � riservata agli utenti registrati"
            Dim SiteFolder As String = siteManager.getDomainInfo.Folder.Split("/")(0)
            Dim cTvalue As New ContentTypeValue()
            'Carica login
            cTvalue = cTypeManager.Read(New ContentTypeIdentificator("HP3", "Login"))
            phMain.Controls.Add(LoadControl("~/" + SiteFolder + "/" + cTvalue.Page))
        End If
	End Sub
	

</script>
<div class="dMain">	
	<asp:PlaceHolder ID="phBreadcrumbs" runat="server" />
	<asp:PlaceHolder id="phNavigationColumn" runat="server" />
	
	<div class="dContent">
		<h2 class="title">
		    <asp:Label ID="lTxt" runat="server" />
		</h2>

		<asp:Panel id="pnlError" visible="false" cssclass="error" runat="server">
			<asp:Label ID="lblError" runat="server" />
		</asp:Panel>

		<asp:PlaceHolder id="phMain" runat="server" />	
	</div>
</div>

