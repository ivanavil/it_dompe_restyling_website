<%@ Control Language="VB" ClassName="ctlBoxManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private siteAreaManager As SiteAreaManager
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
    Private oCManager As BoxManager
    Private oSearcher As BoxSearcher
    Private oCollection As BoxCollection
   
    Private _typecontrol As ControlType = ControlType.View
    Private _sortType As BoxGenericComparer.SortType
    Private _sortOrder As BoxGenericComparer.SortOrder
    
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As BoxValue
        If SelectedId <> 0 Then
            oCManager = New BoxManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
    
  
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        'Dim ret As Boolean = False
        'If SelectedId <> 0 Then
        '    oCManager = New SiteAreaManager
        '    Dim key As New SiteAreaIdentificator
        '    key.Id = SelectedId
        '    ret = oCManager.remove(key)
        '    If ret Then
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Site)
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Area)
        '        oAdvManager.RemoveAllBannerContentType(keyBanner)
        '        oAdvManager.RemoveAllBannerBox(keyBanner)
        '    End If
        'End If
        'Return ret
    End Function
    
    
    
    ''' <summary>
    ''' Recupera i type delle siteareaa
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetTypeBoxValue(ByVal itemZero As String, ByVal list As Integer)
        oCManager = New BoxManager
        Dim sValue As BoxTypeValue
        Dim sCollection As BoxTypeCollection
        Dim oListItem As ListItem
        If list = 1 Then ddlType.Items.Clear()
        'If list = 2 Then ddlTypeEdit.Items.Clear()
        sCollection = oCManager.ReadBoxType(0)
        If Not sCollection Is Nothing Then
            For Each sValue In sCollection
                oListItem = New ListItem(sValue.Description, sValue.Id)
                If list = 1 Then ddlType.Items.Add(oListItem)
                If list = 2 Then ddlTypeEdit.Items.Add(oListItem)
            Next
            If itemZero <> "" And list = 1 Then ddlType.Items.Insert(0, New ListItem(itemZero, 0))
            If itemZero <> "" And list = 2 Then ddlTypeEdit.Items.Insert(0, New ListItem(itemZero, 0))
        End If
    End Sub
    
    Sub LoadFormDett(ByVal oValue As BoxValue)
        GetTypeBoxValue("", 2)
        
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .IdBox
                Name.Text = .Name
                Description.Text = .Description
                ddlTypeEdit.SelectedIndex = ddlTypeEdit.Items.IndexOf(ddlTypeEdit.Items.FindByValue(.BoxType.Id))
                Active.Checked = .Active
                
                'gestione aree
                Aree.GenericCollection = GetBoxSiteAreaValue(.IdBox)
                Aree.IsNew = True
                Aree.LoadControl()
            End With
        Else
            
            lblDescrizione.InnerText = "New SiteArea"
            lblId.InnerText = ""
            Name.Text = Nothing
            Description.Text = Nothing
            Active.Checked = True
                                
            Aree.LoadControl()
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New BoxValue
        With (oValue)
            If SelectedId > 0 Then .IdBox = SelectedId
            .Name = Name.Text
            .Description = Description.Text
            If ddlTypeEdit.SelectedValue <> "" Then .BoxType.Id = ddlTypeEdit.SelectedValue
            .Active = Active.Checked
        End With

        oCManager = New BoxManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        'gestione delle aree
        oCManager.RemoveAllBoxSiteArea(New BoxIdentificator(oValue.IdBox))
        If Not Aree.GetSelection Is Nothing AndAlso Aree.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = Aree.GetSelection
           
            For Each siteArea In AreaColl
                oCManager.CreateBoxSiteArea(New BoxIdentificator(oValue.IdBox), New SiteAreaIdentificator(siteArea.Key.Id))
            Next
        End If
        
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As BoxSearcher = Nothing)
        oCManager = New BoxManager
        oCollection = New BoxCollection
        
        If Searcher Is Nothing Then
            Searcher = New BoxSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
       
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    'recupera tutti le sitearea associate ad un Box
    Function GetBoxSiteAreaValue(ByVal id As Int32) As SiteAreaCollection
        If id = 0 Then Return Nothing
        Dim key As New BoxIdentificator
        key.Id = id
        oCManager.Cache = False
        Return oCManager.ReadBoxSiteArea(key)
    End Function
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New BoxSearcher
        If txtId.Text <> "" Then oSearcher.Id = txtId.Text
        If ddlType.SelectedValue <> "" Then oSearcher.BoxType.Id = ddlType.SelectedValue
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> BoxGenericComparer.SortType.ById Then
                    SortType = BoxGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Name"
                If SortType <> BoxGenericComparer.SortType.ByName Then
                    SortType = BoxGenericComparer.SortType.ByName
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As BoxGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = BoxGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As BoxGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As BoxGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = BoxGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As BoxGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
            Case "BoxContent"
                SelectedId = id
                
                Dim wr As New WebRequestValue
                Dim ctm As New ContentTypeManager()
                Dim mPage As New MasterPageManager()
                Dim objQs As New ObjQueryString
                wr.KeycontentType = New ContentTypeIdentificator("CMS", "BoxContent")
                wr.customParam.append("KeyBox", id)
                wr.customParam.append(objQs)
                Response.Redirect(mPage.FormatRequest(wr))
                
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoToBoxType(ByVal s As Object, ByVal e As EventArgs)
        Dim oWRV As New WebRequestValue
        Dim oMPM As New MasterPageManager()
        Dim oQS As New ObjQueryString
        oWRV.KeycontentType = New ContentTypeIdentificator("CMS", "boxtypem")
        oWRV.customParam.append(oQS)
        Response.Redirect(oMPM.FormatRequest(oWRV))
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
    
<asp:Panel id="pnlGrid" runat="server">
<asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/> <asp:button cssclass="button" ID="btnGoBoxType" runat="server" text="Go Box Type" Style ="margin-bottom:10px;width:100px" onclick="GoToBoxType" />
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Box Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Type</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><asp:DropDownList id="ddlType" DataTextField="Description" DataValueField="Id" runat="server"></asp:DropDownList></td>  
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
           </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"             
        AllowSorting="true"
        OnPageIndexChanging="gridList_PageIndexChanging"
        PageSize ="20"
        OnSorting="gridList_Sorting"
        emptydatatext="No item available"                
        OnRowDataBound="gridList_RowDataBound">
            <Columns >
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "IdBox")%></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="15%" DataField="Name" HeaderText ="Name" SortExpression="Name" />
                <asp:BoundField HeaderStyle-Width="55%" DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Box Type">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "BoxType.Description")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "IdBox")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "IdBox")%>'/>
                        <asp:ImageButton ID="lnkBoxContent" runat="server" ToolTip ="Content Relation" ImageUrl="~/HP3Office/HP3Image/ico/content_content.gif" OnClick="OnItemSelected" CommandName="BoxContent" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "IdBox")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive"  onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>   
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpBoxLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
            <td><HP3:Text runat ="server" ID="Name" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBoxDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td><HP3:ctlSite runat="server" id="Aree" TypeControl="GenericRelation" SiteType="area"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBoxType&Help=cms_HelpBoxType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBoxType", "Box Type")%></td>
            <td><asp:DropDownList id="ddlTypeEdit" DataTextField="Description" DataValueField="Id" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="Active" runat="server" /></td>
        </tr>
     </table>
</asp:Panel>