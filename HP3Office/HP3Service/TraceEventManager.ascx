<%@ Control Language="VB" ClassName="ctlTraceEventManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private oCManager As TraceEventManager
    Private oSearcher As TraceEventSearcher
    Private oCollection As TraceEventCollection
    Private _sortType As TraceEventGenericComparer.SortType
    Private _sortOrder As TraceEventGenericComparer.SortOrder
    Private strDomain As String
    Private _typecontrol As ControlType = ControlType.View
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
    End Sub

    Function LoadValue() As TraceEventValue
        If SelectedId <> 0 Then
            oCManager = New TraceEventManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente il profilo selezionato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New TraceEventManager
            Dim key As New TraceEventIdentificator(SelectedId)
            ret = oCManager.Remove(key)
        End If
        Return ret
    End Function
    
    Sub LoadFormDett(ByVal oValue As TraceEventValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                keyDomain.Text = .Key.Domain
                keyName.Text = .Key.Name
                Label.Text = .Label
                Description.Text = .Description
            End With
        Else
            lblDescrizione.InnerText = "New Event"
            lblId.InnerText = ""
            keyDomain.Text = Nothing
            keyName.Text = Nothing
            Description.Text = Nothing
            Label.Text = Nothing
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New TraceEventValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Key.Domain = keyDomain.Text
            .Key.Name = keyName.Text
            .Description = Description.Text
            .Label = Label.Text
        End With

        oCManager = New TraceEventManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
        
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As TraceEventSearcher = Nothing)
        oCManager = New TraceEventManager
        oCollection = New TraceEventCollection
        
        If Searcher Is Nothing Then
            Searcher = New TraceEventSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New TraceEventSearcher
        If txtId.Text <> "" Then oSearcher.keyEvent = New TraceEventIdentificator(txtId.Text)
        If txtLabel.Text <> "" Then oSearcher.SearchString = txtLabel.Text
        
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> TraceEventGenericComparer.SortType.ById Then
                    SortType = TraceEventGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Label"
                If SortType <> TraceEventGenericComparer.SortType.ByLabel Then
                    SortType = TraceEventGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> TraceEventGenericComparer.SortType.ByKey Then
                    SortType = TraceEventGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As TraceEventGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = TraceEventGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As TraceEventGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As TraceEventGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = TraceEventGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As TraceEventGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As TraceEventCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curValue As TraceEventValue
                    Dim outCollection As New TraceEventCollection
            
                    For Each str As String In CheckedItems
                        oCManager = New TraceEventManager
                        oCManager.Cache = False
                        curValue = oCManager.Read(Int32.Parse(str))
                        If Not curValue Is Nothing Then
                            outCollection.Add(curValue)
                        End If
                    Next
                    Return outCollection
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridList.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litsel")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkDelete As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkDelete = e.Row.FindControl("lnkDelete")
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                    lnkDelete.Visible = False

                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>

<script type="text/javascript" >       
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
        
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Trace Event Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Label</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><HP3:Text runat="server" ID="txtLabel" TypeControl="TextBox" style="width:200px"/></td>  
                    <td></td>  
                    <td></td>  
                    <td></td>  
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    AllowSorting="true"
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridList_Sorting"
                    emptydatatext="No item available"                
                    OnRowDataBound="gridList_RowDataBound"
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                    >
                  <Columns >
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:radiobutton ID="chkSel" runat="server" />
                            <asp:literal ID="litSel" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="18%" HeaderText="Identificator" SortExpression="Key">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
                    </asp:TemplateField>  
                    <asp:BoundField HeaderStyle-Width="20%" DataField ="Label" HeaderText ="Label" SortExpression="Label" />
                    <asp:BoundField HeaderStyle-Width="50%" DataField ="Description" HeaderText ="Description" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/><br /><br />
    </div>  
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
        <table class="form" style="width:100%">
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
                <td><HP3:Text runat ="server" ID="keyDomain" TypeControl ="TextBox" style="width:90px" MaxLength="5"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
                <td><HP3:Text runat ="server" ID="keyName" TypeControl ="TextBox" style="width:90px" MaxLength="10"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpTraceLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
                <td><HP3:Text runat ="server" ID="Label" TypeControl ="TextBox" style="width:300px"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpTraceDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextArea" style="width:300px"/></td>
            </tr>
       </table>
</asp:Panel>