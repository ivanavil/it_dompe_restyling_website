<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Document"%>
<%@ Import Namespace="Healthware.HP3.Core.Document.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Dim DocumentServiceManager As New DocumentServiceManager
    Private oGenericUtlity As New GenericUtility
    
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    
    Public Property SelectedId() As Integer
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    ''' <summary>
    ''' Lettura delle descrizioni dei siti dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSiteValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetSiteAreaValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.name
        End If
    End Function
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack) Then
            LoadDocumentService()
        End If
    End Sub
   
    'recupera tutti i document service
    Sub LoadDocumentService()
        Dim docServiceSearcher As New DocumentServiceSearcher
        
        DocumentServiceManager.Cache = False
        Dim docServiceColl As DocumentServiceCollection = DocumentServiceManager.Read(docServiceSearcher)
      
        gdw_documentserviceList.DataSource = docServiceColl
        gdw_documentserviceList.DataBind()
    End Sub
    
    Sub NewItem(ByVal sender As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        SelectedId() = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
        
    'popola i campi di testo con le informazioni contenute nel docservicevalue
    Sub LoadDett(ByVal oDocServiceValue As DocumentServiceValue)
        If Not oDocServiceValue Is Nothing Then
            Dim autext As String = ""
            
            serviceName.Text = oDocServiceValue.Name
            serviceDescription.Text = oDocServiceValue.Description
                        
            serviceMaxSpace.Text = Long.Parse((Math.Round(oDocServiceValue.MaxSpace / 1048576 * 100000) / 100000).ToString)
            serviceMaxFileSize.Text = Long.Parse((Math.Round(oDocServiceValue.MaxFileSize / 1048576 * 100000) / 100000).ToString)
            
            serviceAutExt.Text = oDocServiceValue.AuthorizedExtensions.GetString
            serviceDir.Text = oDocServiceValue.StorageDirectory
            serviceNotif.Text = oDocServiceValue.FromAddress
            txtHideSiteArea.Text = oDocServiceValue.SiteAreaId
            txtSiteArea.Text = ReadSiteValue(oDocServiceValue.SiteAreaId)
        Else
            txtHideSiteArea.Text = Nothing
            txtSiteArea.Text = Nothing
            serviceName.Text = Nothing
            serviceDescription.Text = Nothing
            serviceMaxSpace.Text = Nothing
            serviceMaxFileSize.Text = Nothing
            serviceAutExt.Text = Nothing
            serviceDir.Text = Nothing
            serviceNotif.Text = Nothing
        End If
    End Sub
    
    'cancellazione logica 
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim idService As Integer = 0
        
        If sender.CommandName = "DeleteItem" Then
            idService = sender.commandArgument()
            DocumentServiceManager.Remove(idService)
            
            LoadDocumentService()
        End If
    End Sub
        
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim docServiceSearcher As New DocumentServiceSearcher
        Dim serviceId As Integer = 0
               
        If (sender.CommandName = "EditItem") Then
            serviceId = sender.CommandArgument
            SelectedId() = serviceId
            
            DocumentServiceManager.Cache = False
            docServiceSearcher.Id = serviceId
            Dim docServiceColl As DocumentServiceCollection = DocumentServiceManager.Read(docServiceSearcher)
            Dim docServiceValue As DocumentServiceValue = docServiceColl(0)
            
            LoadDett(docServiceValue)
            ActivePanelDett()
        End If
    End Sub
    
   
    Sub SaveRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim oDocServiceValue As New DocumentServiceValue
              
        oDocServiceValue.Name = serviceName.Text
        oDocServiceValue.Description = serviceDescription.Text
        
        If (serviceMaxSpace.Text <> "") And (serviceMaxFileSize.Text <> "") Then
            oDocServiceValue.MaxSpace = Long.Parse(Math.Round((CType(serviceMaxSpace.Text, Double) * 1048576 * 100000) / 100000).ToString)
            oDocServiceValue.MaxFileSize = Long.Parse(Math.Round((CType(serviceMaxFileSize.Text, Double) * 1048576 * 100000) / 100000).ToString)
        End If
        
        oDocServiceValue.AuthorizedExtensions.GetString = serviceAutExt.Text
        oDocServiceValue.StorageDirectory = serviceDir.Text
        
        If txtHideSiteArea.Text <> "" Then
            oDocServiceValue.SiteAreaId = txtHideSiteArea.Text
        End If
        
        If CheckMailAddress(serviceNotif.Text) Then
            oDocServiceValue.FromAddress = serviceNotif.Text
               
            If (SelectedId() = 0) Then
                DocumentServiceManager.Create(oDocServiceValue)
                CreateJsMessage("Saved successful!")
            Else
                oDocServiceValue.Id = SelectedId()
                DocumentServiceManager.Update(oDocServiceValue)
                CreateJsMessage("Updated successful!")
            End If
            
            LoadDocumentService()
            ActivePanelGrid()
        Else
            CreateJsMessage("The email could be empty or not well formed!")
        End If
    End Sub
      
    Function CheckMailAddress(ByVal mail As String) As Boolean
        If (mail <> "") Then
            If Regex.IsMatch(mail, "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$") Then
                Return True
            End If
        End If
        
        Return False
    End Function
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadDocumentService()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<asp:Panel id="pnlGrid" runat="server">
 <asp:Button ID="Button2" CssClass="button" runat="server" Text="New" onclick="NewItem" style=" margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
 <asp:gridview id="gdw_documentserviceList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns>
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Name">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Name")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Id")%>'/>
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server" Visible="false">
    <div>
        <asp:button ID="Button1" runat="server"  onclick="GoList" Text="Archive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button ID="btnSave" runat="server"  onclick="SaveRow" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>  
      
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=	cms_FormName&Help=cms_HelpServiceName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text  runat ="server" id="serviceName" TypeControl ="TextBox" style="width:600px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpServiceDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><asp:TextBox TextMode="MultiLine" Width="600px" Rows="10" Columns="100" ID="serviceDescription" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpServiceArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHideSiteArea" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtSiteArea"  TypeControl="TextBox" style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" value="..." class="button" onclick="window.open('<%=Domain%>/Popups/popSite.aspx?sites=' +  document.getElementById('<%=txtHideSiteArea.clientid%>').value + '&SingleSel=1&HiddenId=<%=txtHideSiteArea.clientid%>&SiteAreaType=<%= SiteAreaCollection.SiteAreaType.Area %>&ListId=<%=txtSiteArea.clientid%>','','width=600,height=500,scrollbars=yes')" /></td>        
                    </tr>
                </table>        
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormServiceMaxSpace&Help=cms_HelpServiceMaxSpace&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormServiceMaxSpace", "Max Space (MB)")%></td>
            <td><HP3:Text runat ="server" id="serviceMaxSpace"  style="width:120px" MaxLength="10" TypeControl="NumericText"/></td>
            <td><asp:Label id="Label1"  runat ="server" Text="MB" style="color: Gray" ></asp:Label></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormServiceMaxFSize&Help=cms_HelpServiceMaxFSize&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormServiceMaxFSize", "Max File Size (MB)")%></td>
            <td><HP3:Text runat ="server" id="serviceMaxFileSize"  style="width:120px" MaxLength="10" TypeControl="NumericText"/></td>
            <td><asp:Label id="Label2"  runat ="server" Text="MB" style="color: Gray" ></asp:Label></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormServiceAuEx&Help=cms_HelpServiceAuEx&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormServiceAuEx", "Authorized Extension")%></td>
            <td><HP3:Text runat ="server" id="serviceAutExt" TypeControl ="TextBox" style="width:600px" MaxLength="100"/></td>
            <td><asp:Label id="lbMsg"  runat ="server" Text="[* More than one extension could be insert separated with '|' ex. ppt|pdf...]" style="color: Gray" ></asp:Label></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormServiceStoreDir&Help=cms_HelpServiceStoreDir&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormServiceStoreDir", "Storage Directory")%></td>
            <td><HP3:Text runat ="server" id="serviceDir" TypeControl ="TextBox" style="width:600px" MaxLength="256"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormServiceEmNotif&Help=cms_HelpServiceEmNotif&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormServiceEmNotif", "Email Address Notification")%></td>
            <td><HP3:Text runat ="server" id="serviceNotif" TypeControl ="TextBox" style="width:600px" MaxLength="256"/></td>
        </tr>
    </table>
</asp:Panel>
