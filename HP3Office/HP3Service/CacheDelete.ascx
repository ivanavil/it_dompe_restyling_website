<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script language="VB" runat="server">
    
    Dim dt As New DataTable("cache")
    Dim listKey As ArrayList
    Function PersistentCount() As Integer
        Dim persistent As New PersistentCache
        Return persistent.Count()
    End Function
    
    
    Sub RemoveAllCache(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim commentManager As New Healthware.HP3.Core.Content.CommentManager
        Select Case cacheDrop.SelectedValue
            Case "All"
                CacheManager.RemoveAllCache()
                Description.InnerHtml = "All the Cache has been removed with success"
          
            Case "Download"
                Me.BusinessDownloadManager.RemoveCache()
                Description.InnerHtml = "The Download Cache has been removed with success"
       
            Case "Comment"
                commentManager.RemoveCache()
                Description.InnerHtml = "The Comment Cache has been removed with success"
            
            Case "Congress"
                Me.BusinessCongressManager.RemoveCache()
                Description.InnerHtml = "The Congress Cache has been removed with success"
            
            Case "ContentExtra"
                Me.BusinessContentExtraManager.RemoveCache()
                Description.InnerHtml = "The ContentExtra Cache has been removed with success"
           
            Case "Content"
                Me.BusinessContentManager.RemoveCache()
                Description.InnerHtml = "The Content Cache has been removed with success"
            
            Case "QeA"
                Me.BusinessQeAManager.RemoveCache()
                Description.InnerHtml = "The QeA Cache has been removed with success"
                
            Case "Context"
                Me.BusinessContextManager.RemoveCache()
                Description.InnerHtml = "The Context Cache has been removed with success"
                  
        End Select

        Binding()
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        Binding()
    End Sub
    
    Sub Binding()
        dt.Columns.Add(New DataColumn("Key"))
        dt.Columns.Add(New DataColumn("Value"))
        Dim dr As DataRow
        Dim keyString As String

        Dim CacheEnum As IDictionaryEnumerator = HttpRuntime.Cache.GetEnumerator()
        While CacheEnum.MoveNext()
            dr = dt.NewRow
            keyString = CacheEnum.Key
            If keyString.Length > 60 Then
                keyString = keyString.Substring(0, 60)
            End If
                        
            dr(0) = keyString
            dr(1) = CacheEnum.Value
            dt.Rows.Add(dr)
        End While
       
        gridList.DataSource = dt
        gridList.DataBind()
    End Sub
    
    
    
    
    
    '#2326 Ama -24-11-2011
    Sub BindingButton(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim stringFromtxt As String
        stringFromtxt = txtsearch.Text
   
        listKey = New ArrayList()
        If Not stringFromtxt Is Nothing AndAlso stringFromtxt <> "" Then
            dt.Columns.Add(New DataColumn("Key"))
            dt.Columns.Add(New DataColumn("Value"))
            Dim dr As DataRow
            Dim keyString As String
            Dim keyStringIntera As String

            Dim CacheEnum As IDictionaryEnumerator = HttpRuntime.Cache.GetEnumerator()
            While CacheEnum.MoveNext()
                dr = dt.NewRow
                keyStringIntera = CacheEnum.Key
                keyString = keyStringIntera
                If keyString.Length > 60 Then
                    keyString = keyString.Substring(0, 60)
                End If
                If keyStringIntera.Contains(stringFromtxt) Then
                    dr(0) = keyString
                    dr(1) = CacheEnum.Value
                    listKey.Add(keyStringIntera)
            
                    dt.Rows.Add(dr)
                End If
            End While
        
            gridList.DataSource = dt
            gridList.DataBind()
        Else
            Binding()
        End If
        If Not listKey Is Nothing Then ViewState.Add("listKey", listKey)
    End Sub
    
    
    '#2326 Ama -24-11-2011
    Protected Sub removeFromSearch_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        listKey = ViewState("listKey")
        If Not listKey Is Nothing Then
            For Each elem As String In listKey
               
                CacheManager.RemoveByKey(elem)
            Next
        End If
        
        txtsearch.Text = ""
        Binding()
            
    End Sub
    
       
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not Page.IsPostBack Then
            Binding()
        End If
           
    End Sub
    'Response.Redirect("/")
   
</script>
<div>&nbsp;</div>
<asp:DropDownList ID="cacheDrop" runat="server">
  <asp:ListItem Text="All" Selected="True" Value="All"></asp:ListItem>
    <asp:ListItem Text="Download" Value="Download"></asp:ListItem>
    <asp:ListItem Text="Comment" Value="Comment"></asp:ListItem>
    <asp:ListItem Text="Congress" Value="Congress"></asp:ListItem>
    <asp:ListItem Text="ContentExtra" Value="ContentExtra"></asp:ListItem>
    <asp:ListItem Text="Content" Value="Content"></asp:ListItem>
    <asp:ListItem Text="QeA" Value="QeA"></asp:ListItem>
    <asp:ListItem Text="Context" Value="Context"></asp:ListItem>
</asp:DropDownList>
<asp:Button ID="CacheDel" cssclass="button" Text="Remove Cache" OnClick="RemoveAllCache" runat="server" />
<div>&nbsp;</div>
<h3 id="Description" runat ="server"/>
 <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Cache Search</span></td>
            </tr>
        </table>
<fieldset class="boxSearcher" style="background-color: #EFEFEF;">
    <div>
        <b>Key</b></div>
    <div>
        <asp:TextBox ID="txtsearch" runat="server" Width="40%"></asp:TextBox>
        <asp:Button runat="server" CssClass="button" ID="searchbykey" OnClick="BindingButton"
            Text="Search" Style="margin-top: 10px; padding-left: 16px; width: 80px; background-image: url('/HP3Office/HP3Image/Button/search_content.gif');
            background-repeat: no-repeat; background-position: 1px 1px;" />
        <asp:Button ID="removeFromSearch" CssClass="button" runat="server" Text="Remove current cache search"
            OnClick="removeFromSearch_OnClick" />
    </div>
</fieldset>

<div>Object present in cache:<br />First Level: <%=dt.Rows.Count()%> - Second Level: <%=PersistentCount()%></div>
<div>&nbsp;</div>

<asp:gridview ID="gridList" 
    runat="server"
    style="width:99%"
    AllowPaging="true"
    PageSize ="100"
    OnPageIndexChanging="gridList_PageIndexChanging"
    emptydatatext="No item available"                
    AlternatingRowStyle-BackColor="#e9eef4"
    HeaderStyle-CssClass="header"           
    PagerSettings-Position="TopAndBottom"
    PagerStyle-HorizontalAlign="Right"  
    PagerStyle-CssClass="Pager" >
</asp:gridview>