<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlProfiles" Src ="~/hp3Office/HP3Parts/ctlProfilesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlUsers" Src ="~/hp3Office/HP3Parts/ctlUsersList.ascx"%>

<script runat="server">
    Private strJS As String
    Private _strDomain As String
    Private _language As String
    
    Private userGroupManager As New UserGroupManager
      
    Private Property SelectedId() As Integer
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
               
        If Not (Page.IsPostBack) Then
            LoadUserGroup()
        End If
    End Sub
   
    'recupera la lista dei gruppi di utenti
    Sub LoadUserGroup()
        Dim userGroupSearcher As New UserGroupSearcher
        
        userGroupManager.Cache = False
        Dim userGroupColl As UserGroupCollection = userGroupManager.Read(userGroupSearcher)
        
        gdw_userGroupList.DataSource = userGroupColl
        gdw_userGroupList.DataBind()
        
        ActivePanelGrid()
    End Sub
             
    'cancellazione di un gruppo di utenti
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userGroupIdent As New UserGroupIdentificator
        Dim itemId As Integer
        
        If sender.CommandName = "DeleteItem" Then
            itemId = sender.commandArgument()
            
            userGroupIdent.Id = itemId
            userGroupManager.Remove(userGroupIdent)
            
            LoadUserGroup()
        End If
    End Sub
    
    'gestisce l'edit di un gruppo di utenti
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim userGroupIdent As New UserGroupIdentificator
        Dim itemId As Integer
        
        If sender.CommandName = "EditItem" Then
            itemId = sender.commandArgument()
            
            SelectedId = itemId
            userGroupIdent.Id = itemId
            Dim userGroupValue As UserGroupValue = userGroupManager.Read(userGroupIdent)
            
            If Not (userGroupValue Is Nothing) Then
                h2Title.InnerHtml = "UserGroupId - " & userGroupValue.Key.Id
                lDescription.Text = userGroupValue.Description
                
                'gestione ruoli
                ctlRoles.GenericCollection = GetUserGroupRolesValue(userGroupValue.Key.Id)
                ctlRoles.LoadControl()
              
                'gestione profili
                ctlProfiles.GenericCollection = GetUserGroupProfilesValue(userGroupValue.Key.Id)
                ctlProfiles.LoadControl()
                
                'gestione utenti
                ctlUsers.GenericCollection = GetUserValue(userGroupValue.Key.Id)
                ctlUsers.LoadControl()
                
                ActivePanelDett()
            End If
        End If
    End Sub
      
    'rimanda alla lista dei gruppi di utenti    
    Sub LoadArchive(ByVal s As Object, ByVal e As EventArgs)
        LoadUserGroup()
    End Sub
     
    'gestisce la creazione di un nuovo gruppo
    Sub NewRow(ByVal s As Object, ByVal e As EventArgs)
        SelectedId = 0
        
        ActivePanelDett()
        lDescription.Text = Nothing
        
        ctlRoles.LoadControl()
        ctlProfiles.LoadControl()
        ctlUsers.LoadControl()
    End Sub
    
    'gestisce la modifica/salvataggio di un gruppo di utenti
    Sub UpdateRow(ByVal s As Object, ByVal e As EventArgs)
        Dim userGroupValue As New UserGroupValue
       
        userGroupValue.Description = lDescription.Text
        userGroupValue.Type = userGroupValue.GroupType.Standard
        If (SelectedId <> 0) Then
            userGroupValue.Key.Id = SelectedId
            userGroupValue = userGroupManager.Update(userGroupValue)
            
            'gestione profili
            ProfilesManager(userGroupValue.Key)
            'gestione ruoli
            RolesManager(userGroupValue.Key)
            'gestione user
            UsersManager(userGroupValue.Key)
        Else
            userGroupValue = userGroupManager.Create(userGroupValue)
            'gestione profili
            ProfilesManager(userGroupValue.Key)
            'gestione ruoli
            RolesManager(userGroupValue.Key)
            'gestione user
            UsersManager(userGroupValue.Key)
        End If
        
        LoadUserGroup()
    End Sub
    
    'gestione delle associazioni profili-gruppo
    Sub ProfilesManager(ByVal key As UserGroupIdentificator)
        'cancella tutte le relazioni usergroup-profili dato un certo gruppo
        userGroupManager.RemoveUserGroupProfileRelation(key)
        
        If Not ctlProfiles.GetSelection Is Nothing AndAlso ctlProfiles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim profile As New ProfileValue
            Pc = ctlProfiles.GetSelection
            
            For Each profile In Pc
                userGroupManager.CreateUserGroupProfileRelation(key, New ProfileIdentificator(profile.Key.Id))
            Next
        End If
    End Sub
    
    'gestione delle associazioni ruoli-gruppo 
    Sub RolesManager(ByVal key As UserGroupIdentificator)
        'cancella tutte le relazioni usergroup-ruoli dato un certo gruppo
        userGroupManager.RemoveUserGroupRoleRelation(key)
        
        If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim role As New RoleValue
            Pc = ctlRoles.GetSelection
            
            For Each role In Pc
                userGroupManager.CreateUserGroupRoleRelation(key, New RoleIdentificator(role.Key.Id))
            Next
        End If
    End Sub
    
    'gestione delle associazioni utenti-gruppo
    Sub UsersManager(ByVal key As UserGroupIdentificator)
        'cancella tutte le relazioni usergroup-utente dato un certo gruppo
        userGroupManager.RemoveUserRelation(key)
        
        If Not ctlUsers.GetSelection Is Nothing AndAlso ctlUsers.GetSelection.Count > 0 Then
            Dim Pc As New Object
            Dim user As New UserValue
            Pc = ctlUsers.GetSelection
            
            For Each user In Pc
                userGroupManager.CreateUserGroupRelation(New UserIdentificator(user.Key.Id), key)
            Next
        End If
    End Sub
    
    'recupera tutti i ruoli associati ad un gruppo di utenti
    Function GetUserGroupRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        Me.BusinessProfilingManager.Cache = False
        Return Me.BusinessProfilingManager.ReadUserGroupRoles(key)
    End Function
       
    'recupera tutti i profili associati ad un gruppo di utenti
    Function GetUserGroupProfilesValue(ByVal id As Int32) As ProfileCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        Me.BusinessProfilingManager.Cache = False
        Return Me.BusinessProfilingManager.ReadUserGroupProfiles(key)
    End Function
        
    'recupera tutti gli utenti associati ad un gruppo 
    Function GetUserValue(ByVal id As Int32) As UserCollection
        If id = 0 Then Return Nothing
        Dim key As New UserGroupIdentificator
        key.Id = id
        
        userGroupManager.Cache = False
        Return userGroupManager.ReadUserRelated(key)
    End Function
    
    'gestisce la ricerca degli gruppi di utenti 
    Sub SearchUserGroup(ByVal sender As Object, ByVal e As EventArgs)
        BindWithSearch(gdw_userGroupList)
    End Sub
    
    'Caricamento della griglia dei gruppi di utenti
    'Verifica la presenza di criteri di ricerca
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim userGroupSearcher As New UserGroupSearcher
        
        If txtId.Text <> "" Then userGroupSearcher.Key = New UserGroupIdentificator(txtId.Text)
        If txtDescription.Text <> "" Then userGroupSearcher.Description = txtDescription.Text
        userGroupSearcher.Type = Healthware.HP3.Core.User.ObjectValues.UserGroupSearcher.GroupType.Standard
        
        BindGrid(objGrid, userGroupSearcher)
    End Sub
        
    'Esegue il bind della griglia
    'data la griglia stessa e l'eventuale searcher    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserGroupSearcher = Nothing)
        Dim userGroupCollection As New UserGroupCollection
  
        If Searcher Is Nothing Then
            Searcher = New UserGroupSearcher
        End If
        
        userGroupManager.Cache = False
        userGroupCollection = userGroupManager.Read(Searcher)
               
        objGrid.DataSource = userGroupCollection
        objGrid.DataBind()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>
<hr/>

<asp:Panel id="pnlGrid" runat="server">
  <asp:button ID="btnNew" runat="server" Text="New" onclick="NewRow" CssClass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /><br /><br />
 
     <%--FILTRI SUGLI UTENTI--%>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">UserGroup Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Description</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat ="server" id="txtDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>  
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchUserGroup" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
             </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview id="gdw_userGroupList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                OnPageIndexChanging="gridListUsers_PageIndexChanging"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Description">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="7%">
                       <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Restore item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
  
  <br /><h2 id="h2Title" runat="server"  class="title"/>
 <table  style=" margin-top:10px" class="form" width="99%">
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpUserGrDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
        <td><HP3:Text ID ="lDescription" runat="server"  TypeControl ="TextBox" style="width:300px" MaxLength="50"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormProfiles&Help=cms_HelpUserGroupProfile&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormProfiles", "Profiles")%></td>
        <td><HP3:ctlProfiles runat ="server" id="ctlProfiles" typecontrol="GenericRelation" /></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpUserGroupRole&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
        <td><HP3:ctlRoles runat ="server" id="ctlRoles" typecontrol="GenericRelation" /></td>
   </tr>
   <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUsers&Help=cms_HelpUsers&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUsers", "Users")%></td>
        <td><HP3:ctlUsers runat ="server" id="ctlUsers" typecontrol="GenericRelation" /></td>
   </tr>
 </table>
</asp:Panel>