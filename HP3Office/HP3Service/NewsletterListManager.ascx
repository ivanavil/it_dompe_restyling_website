<%@ Control Language="VB" ClassName="ctlNewsletterListManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">

    Private oCol As NewsletterListCollection
    Private _typecontrol As ControlType = ControlType.View
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    'Sub BindFilerObject()
    '    'GetTypeBoxValue("Select BoxType", 1)
    'End Sub

    Function LoadValue() As NewsletterListValue
        If SelectedId <> 0 Then
            Me.BusinessNewsletterManager.Cache = False
            Return Me.BusinessNewsletterManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente un BoxType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        If SelectedId <> 0 Then
            Dim oValue As New NewsletterListValue
            oValue = Me.BusinessNewsletterManager.Read(SelectedId)
            oValue.Delete = True
            oValue = Me.BusinessNewsletterManager.Update(oValue)
            If oValue.Delete Then Return True
        End If
        Return False
    End Function
    
    Sub LoadFormDett(ByVal oValue As NewsletterListValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Label
                lblId.InnerText = "ID: " & .Key
                Label.Text = .Label
                Description.Text = .Description
                Delete.Checked = .Delete
            End With
        Else
            lblDescrizione.InnerText = "New NewsletterList"
            lblId.InnerText = ""
            Description.Text = Nothing
            Label.Text = Nothing
            Delete.Checked = False
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New NewsletterListValue
        With (oValue)
            If SelectedId > 0 Then .Key = SelectedId
            .Description = Description.Text
            .Label = Label.Text
            .Delete = Delete.Checked
        End With

        If SelectedId <= 0 Then
            oValue = Me.BusinessNewsletterManager.Create(oValue)
        Else
            oValue = Me.BusinessNewsletterManager.Update(oValue)
        End If
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        'strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    'BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal so As NewsletterListSearcher = Nothing)
        Me.BusinessNewsletterManager.Cache = False
        oCol = Me.BusinessNewsletterManager.Read(so)
        objGrid.DataSource = oCol
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        'oSearcher = New BoxSearcher
        'If txtId.Text <> "" Then oSearcher.Id = txtId.Text
        'If ddlType.SelectedValue <> "" Then oSearcher.BoxType.Id = ddlType.SelectedValue
        BindGrid(objGrid, Nothing)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
</script>

<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid"  runat="server">
    <asp:button ID="btnNew" runat="server" Text="New" onclick="NewItem" cssclass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    emptydatatext="No item available"                
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"  
                    PagerStyle-CssClass="Pager"
                    >
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="30%" DataField="Label" HeaderText="Label" />
                    <asp:BoundField HeaderStyle-Width="58%" DataField="Description" HeaderText="Description" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpNewsletterListLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
            <td><HP3:Text runat ="server" ID="Label" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpNewsletterListDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDelete&Help=cms_HelpNewsletterListDelete&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDelete", "Delete")%></td>
            <td><asp:checkbox id="Delete" runat="server"  Checked="false"/></td>
        </tr>
    </table>
</asp:Panel>