<%@ Control Language="VB" ClassName="ctlOptionExtraManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private oCManager As ContentOptionService
    Private oSearcher As ContentOptionSearcher
    Private oCollection As ContentOptionCollection
    Private mPageManager As New MasterPageManager()
    Private dictionaryManager As New DictionaryManager
    'Private _sortType As ProfileGenericComparer.SortType
    'Private _sortOrder As ProfileGenericComparer.SortOrder
    Private strDomain As String
    Private _typecontrol As ControlType = ControlType.View
    
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
    End Sub

    Function LoadValue() As ContentOptionValue
        If SelectedId <> 0 Then
            oCManager = New ContentOptionService
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente il profilo selezionato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New ContentOptionService
            Dim key As New ContentOptionIdentificator(SelectedId)
            ret = oCManager.Remove(key)
        End If
        Return ret
    End Function
    
    ''' <summary>
    ''' Rtitorna la descrizione del contentType
    ''' </summary>
    ''' <param name="idContentType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetDescriptionContentType(ByVal idContentType As Integer) As String
        Dim oCTManager As New ContentTypeManager
        Return oCTManager.Read(New ContentTypeIdentificator(idContentType)).Description
    End Function
    
    ''' <summary>
    ''' Ritorna la descrizione di un Event
    ''' </summary>
    ''' <param name="idEvent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetDescriptionEvent(ByVal idEvent As Integer) As String
        Dim oTEManager As New TraceEventManager
        Return oTEManager.Read(New TraceEventIdentificator(idEvent)).Description
    End Function
    
    Sub LoadFormDett(ByVal oValue As ContentOptionValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                Description.Text = .Description
                Label.Text = .Label
                Order.Text = .Order
                Target.Text = .Target
                UrlType.Checked = .UrlType
                txtHiddenContentType.Text = .KeyContentType.Id
                txtContentType.Text = GetDescriptionContentType(.KeyContentType.Id)
                txtHiddenEvent.Text = .KeyEvent.Id
                txtEvent.Text = GetDescriptionEvent(.KeyEvent.Id)
            End With
        Else
            lblDescrizione.InnerText = ""
            lblId.InnerText = ""
            Description.Text = Nothing
            Label.Text = Nothing
            Order.Text = Nothing
            Target.Text = Nothing
            UrlType.Checked = False
            txtHiddenContentType.Text = Nothing
            txtHiddenEvent.Text = Nothing
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New ContentOptionValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Description = Description.Text
            .Label = Label.Text
            If Order.Text <> "" Then .Order = Order.Text
            .Target = Target.Text
            .UrlType = UrlType.Checked
            If txtHiddenContentType.Text <> "" Then .KeyContentType.Id = txtHiddenContentType.Text
            If txtHiddenEvent.Text <> "" Then .KeyEvent.Id = txtHiddenEvent.Text
        End With

        oCManager = New ContentOptionService
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If
       
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContentOptionSearcher = Nothing)
        oCManager = New ContentOptionService
        oCollection = New ContentOptionCollection
        
        If Searcher Is Nothing Then
            Searcher = New ContentOptionSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        'If Not oCollection Is Nothing Then
        '    oCollection.Sort(SortType, SortOrder)
        'End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)

        
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      

        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        'Dim ltl As Literal
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        'ltl = e.Row.FindControl("litBanner")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid" runat="server">
    <div style="margin-bottom:10px"><asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewItem"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></div>
    <%--<asp:button cssclass="button" ID="btnNew" runat="server" Text="New Content Option" Style ="width:150px" onclick="NewItem" />--%>
    <hr />
    <asp:gridview ID="gridList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"                
                AllowSorting="true"
                OnPageIndexChanging="gridList_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridList_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                >
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="25%" DataField ="Label" HeaderText ="Label" />
                <asp:BoundField HeaderStyle-Width="60%" DataField ="Description" HeaderText ="Description" />
              <%--  <asp:BoundField HeaderStyle-Width="10%" DataField ="Target" HeaderText ="Target" />--%>
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
                
              </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:Button id ="btnList" runat="server" Text="Archive" OnClick="GoList" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="btnSave" runat="server" Text="Save" OnClick="SaveValue" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>  
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100% ; border:1">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpOptionLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text runat ="server" ID="Label" TypeControl ="TextBox" style="width:150px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpOptionLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOptionOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
            <td><HP3:Text runat ="server" ID="Order" TypeControl="NumericText" style="width:50px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOptionTarget&Help=cms_HelpOptionTarget&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOptionTarget", "Target")%></td>
            <td><HP3:Text runat ="server" ID="Target" TypeControl="TextBox" style="width:90px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOptionUrlType&Help=cms_HelpOptionUrlType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOptionUrlType", "UrlType")%></td>
            <td><asp:checkbox ID="UrlType" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td>
                <table>
                    <tr><td>
                        <asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/>
                        <HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>
                    </td><td>
                        <input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" />
                    </td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormEvent&Help=cms_HelpEvent&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormEvent", "Event")%></td>
            <td>
                <table>
                    <tr>
                        <td><asp:TextBox ID="txtHiddenEvent" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtEvent"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                        <td><input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popTraceEvent.aspx?SelItem=' + document.getElementById('<%=txtHiddenEvent.clientid%>').value + '&ctlHidden=<%=txtHiddenEvent.clientid%>&ctlVisible=<%=txtEvent.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>