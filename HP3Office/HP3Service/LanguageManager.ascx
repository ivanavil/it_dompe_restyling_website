<%@ Control Language="VB" ClassName="ctlLanguageManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private oCManager As LanguageManager
    Private oSearcher As LanguageSearcher
    Private oCollection As LanguageCollection
    Private mPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    Private objCMSUtility As New GenericUtility
    Private oLManager As New LanguageManager
    
    Private _sortType As LanguageGenericComparer.SortType
    Private _sortOrder As LanguageGenericComparer.SortOrder
    
    Private _typecontrol As ControlType = ControlType.View
    
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
        
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'GetTypeBoxValue("Select BoxType", 1)
    End Sub

    Function LoadValue() As LanguageValue
        If SelectedId <> 0 Then
            oCManager = New LanguageManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
  
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New LanguageManager
            ret = oCManager.Remove(SelectedId)
        End If
        Return ret
    End Function
    
    Sub LoadFormDett(ByVal oValue As LanguageValue)
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Key.Code & " - " & .Description
                lblId.InnerText = "ID: " & .Key.Id
                Code.Text = .Key.Code
                Description.Text = .Description
                Culture.Text = .Culture
                Active.Checked = .Active
                MinWordL.Text = .MinWordLengthToIndex
            End With
        Else
            lblDescrizione.InnerText = "New Language"
            lblId.InnerText = ""
            Code.Text = Nothing
            Description.Text = Nothing
            Culture.Text = Nothing
            MinWordL.Text = "0"
            Active.Checked = True
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New LanguageValue
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .Key.Code = Code.Text
            .Description = Description.Text
            If (MinWordL.Text = String.Empty) Then MinWordL.Text = "0"
                     
            .MinWordLengthToIndex = MinWordL.Text
            .Culture = Culture.Text
            .Active = Active.Checked
        End With

        oCManager = New LanguageManager
        If SelectedId <= 0 Then
            oValue = oCManager.Create(oValue)
        Else
            oValue = oCManager.Update(oValue)
        End If

        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As LanguageSearcher = Nothing)
        oCManager = New LanguageManager
        oCollection = New LanguageCollection
        
        If Searcher Is Nothing Then
            Searcher = New LanguageSearcher
            Searcher.Active = LanguageSearcher.Status.All
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oSearcher = New LanguageSearcher
        If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
        If txtCode.Text <> "" Then oSearcher.Key.Code = txtCode.Text
        'If chkActive.Checked Then
        '    oSearcher.Active = LanguageSearcher.Status.Yes
        'Else
        '    oSearcher.Active = LanguageSearcher.Status.No
        'End If
        
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> LanguageGenericComparer.SortType.ById Then
                    SortType = LanguageGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Code"
                If SortType <> LanguageGenericComparer.SortType.ByCode Then
                    SortType = LanguageGenericComparer.SortType.ByCode
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As LanguageGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = LanguageGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As LanguageGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As LanguageGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = LanguageGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As LanguageGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemRemove()
                BindWithSearch(gridList)
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid" runat="server">
    <asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Language Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Code</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><HP3:Text runat="server" ID="txtCode" TypeControl="TextBox" style="width:100px"/></td>  
                    <td></td>  
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
 
    <asp:gridview ID="gridList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"
                AllowSorting="true"
                OnPageIndexChanging="gridList_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridList_Sorting"
                emptydatatext="No item available"                
                OnRowDataBound="gridList_RowDataBound"
                AlternatingRowStyle-BackColor="#e9eef4"
                HeaderStyle-CssClass="header"           
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                >
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderText="Language" >
                        <ItemTemplate><img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.Id"))%>_small.gif" /></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="9%" HeaderText="Code" SortExpression="Code">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Code")%></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderStyle-Width="10%" DataField="Culture" HeaderText="Culture" />
                <asp:BoundField HeaderStyle-Width="50%" DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Right" />
                <asp:BoundField HeaderStyle-Width="10%" DataField="Active" HeaderText="Active" />
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button cssclass="button" runat="server" ID="btnSave" onclick="SaveValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCode&Help=cms_HelpLanguageCode&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCode", "Code")%></td>
            <td><HP3:Text runat ="server" ID="Code" TypeControl ="TextBox" style="width:60px" MaxLength="3"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpLanguageDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextArea" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCulture&Help=cms_HelpLanguageCulture&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCulture", "Culture")%></td>
            <td><HP3:Text runat ="server" ID="Culture" TypeControl ="TextBox" style="width:60px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMinWor&Help=cms_HelpLanguageMinWor&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMinWor", "Min Word Length to Index")%></td>
            <td><HP3:Text runat ="server" ID="MinWordL" TypeControl ="TextBox" style="width:60px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="Active" runat="server" /></td>
        </tr>
        
     </table>
</asp:Panel>