<%@ Control Language="VB" ClassName="ctlRoleManager" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private _oRoleManager As RoleManager
    Private _oRoleSearcher As RoleSearcher
    Private _oRoleCollection As RoleCollection
    Private _oGenericUtility As New GenericUtility
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    
    Private strDomain As String
    Private strJs As String
    Private _strDomain As String
    Private _language As String
    
    Private oCollection As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    
    Private _sortType As RoleGenericComparer.SortType = RoleGenericComparer.SortType.ById
    Private _sortOrder As RoleGenericComparer.SortOrder = RoleGenericComparer.SortOrder.DESC
    
    Private utility As New WebUtility
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)           
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdRole() As Int32
        Get
            Return ViewState("SelectedIdRole")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdRole") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
       
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il RoleValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadRoleValue() As RoleValue
        If SelectedIdRole <> 0 Then
            Return _oGenericUtility.GetRole(SelectedIdRole)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oRoleValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oRoleValue As RoleValue)
        If Not oRoleValue Is Nothing Then
            With oRoleValue
                txtDomain.Text = .Key.Domain
                txtName.Text = .Key.Name
                txtDescription.Text = .Description
                chkActive.Checked = .Active
                RoleDescrizione.InnerText = " - " & .Description
                RoleId.InnerText = "Role ID: " & .Key.Id
            End With
        End If
    End Sub
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadRoleValue)
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridListRole)
                End If
            Case ControlType.Selection
                rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListRole)
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager
        
        oCollection = siteAreaManager.Read(siteAreaSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Sub Page_load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        BindDomains()
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As RoleSearcher = Nothing)
        _oRoleManager = New RoleManager
        _oRoleManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New RoleSearcher
        End If
        
        _oRoleCollection = _oRoleManager.Read(Searcher)
        If Not _oRoleCollection Is Nothing Then
            _oRoleCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = _oRoleCollection
        objGrid.DataBind()
    End Sub
    
          
    ''' <summary>
    ''' Caricamento della griglia dei roles
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
                
        '_oRoleManager = New RoleManager
        _oRoleSearcher = New RoleSearcher()
        If txtFilterId.Text <> "" Then _oRoleSearcher.Key = New RoleIdentificator(txtFilterId.Text)
        If txtFilterDescription.Text <> "" Then _oRoleSearcher.Description = "%" & txtFilterDescription.Text & "%"
        
        If dwlDomain.SelectedItem.Value <> "-1" Then _oRoleSearcher.Key.Domain = dwlDomain.SelectedItem.Value

        
        ReadSelectedItems()
        BindGrid(objGrid, _oRoleSearcher)
    End Sub
    
    Sub SearchRoles(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListRole)
    End Sub
    
    Private Property SortType() As RoleGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = RoleGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As RoleGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As RoleGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = RoleGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As RoleGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListRole.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litRoleId")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As RoleCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curRoleValue As RoleValue
                    Dim outRoleCollection As New RoleCollection
            
                    For Each str As String In CheckedItems
                        curRoleValue = _oGenericUtility.GetRole(Int32.Parse(str))
                        If Not curRoleValue Is Nothing Then
                            outRoleCollection.Add(curRoleValue)
                        End If
                    Next
            
                    Return outRoleCollection
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedIdRole = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListRole)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ClearForm()
        Dim id As Int32 = s.commandargument
        btnSave.Text = "Update"
        
        TypeControl = ControlType.Edit
        SelectedIdRole = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
             
        Select Case s.commandname
            Case "SelectItem"
                NewItem = False
        End Select
        
        ShowRightPanel()
    End Sub

    ''' <summary>
    ''' Gestione del click sul bottone Delete nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnDeleteItem(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim oRoleId As New RoleIdentificator(s.commandArgument)
        Dim oRoleManager As New RoleManager
        oRoleManager.Delete(oRoleId)
        goBack(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Save/Update del role
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oRoleManager As New RoleManager
        Dim oRoleValue As RoleValue = ReadFormValues()
        
        If NewItem Then
            oRoleValue = oRoleManager.Create(oRoleValue)
        Else            
            oRoleValue = oRoleManager.Update(oRoleValue)
        End If
        
        If oRoleValue Is Nothing Then
            strJs = "alert('Error during saving')"
        Else
            strJs = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As RoleValue
        Dim oRoleValue As New RoleValue
        
        With oRoleValue
            .Key.Domain = txtDomain.Text
            .Key.Name = txtName.Text
            .Description = txtDescription.Text
            .Key.Id = SelectedIdRole
            .Active = chkActive.Checked
        End With
        
        Return oRoleValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo Role
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewRole(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        chkActive.Checked = True ' default attivo
        
        TypeControl = ControlType.Edit
        SelectedIdRole = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is CheckBox Then
                CType(ctl, CheckBox).Checked = False
            End If
        Next
        
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momento ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        RoleId.InnerText = ""
        RoleDescrizione.InnerText = "New Role"
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkCopy As LinkButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litRoleId")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkCopy = e.Row.FindControl("lnkCopy")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub

    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListRole.PageIndex = 0
       
        Select Case e.SortExpression
            Case "Id"
                If SortType <> RoleGenericComparer.SortType.ById Then
                    SortType = RoleGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> RoleGenericComparer.SortType.ByDescription Then
                    SortType = RoleGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> RoleGenericComparer.SortType.ByKey Then
                    SortType = RoleGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
</script>
<script type="text/javascript" >  
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
        <%=strJs%>
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<%--FILTRI SUI ROLE--%>
<asp:Panel id="pnlGrid" runat="server">
     <table class="form">
        <tr id="rowToolbar" runat="server">
            <td  colspan="4"><asp:button ID="btnNew" runat="server" CssClass="button" Text="New" onclick="NewRole" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
        </tr>
    </table>
        
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
            <table class="topContentSearcher">
                <tr>
                    <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                    <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Role Searcher</span></td>
                </tr>
            </table>
            
            <fieldset class="_Search">
                <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td ><strong>Description</strong></td>
                    <td ><strong>Domain</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtFilterId" TypeControl="NumericText" style="width:50px"/></td>    
                    <td><HP3:Text runat="server" ID="txtFilterDescription" TypeControl="TextBox" style="width:300px"/></td>  
                    <td><asp:DropDownList id="dwlDomain" runat="server"/></td>
                </tr>
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="SearchRoles" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel>
<%--FINE FILTRI SUI ROLE--%>

   
    <%--LISTA ROLE--%>
    <asp:gridview ID="gridListRole" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"                      
        AllowSorting="true"                
        PageSize ="20"               
        emptydatatext="No roles available"                                                          
        OnRowDataBound="gridRowDataBound" 
        OnPageIndexChanging="gridPageIndexChanging" 
        OnSorting="gridSorting">
        
        <Columns >
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:radiobutton ID="chkSel" runat="server" />
                    <asp:literal ID="litRoleId" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
            </asp:TemplateField>               
            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Identificator" SortExpression="Key">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
            </asp:TemplateField>  
            <asp:BoundField HeaderStyle-Width="70%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
            <asp:TemplateField HeaderStyle-Width="8%">
                <ItemTemplate>
                    <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnDeleteItem" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:gridview>
    
</asp:Panel>
<%--FINE LISTA ROLE--%>


<%--EDIT ROLE--%>
<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnBack" CssClass="button" runat="server" Text="Archive" onclick="goBack" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button ID="btnSave" CssClass="button" runat="server" Text="Save"  onclick="Update" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div> 
    
    <span id="RoleId" class="title" runat ="server"/>
    <span id="RoleDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat="server" ID="txtDomain" TypeControl="TextBox" MaxLength="5" style="width:50px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Name")%></td>
            <td><HP3:Text runat="server" ID="txtName" TypeControl="TextBox" MaxLength="10" style="width:100px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpRoleDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat="server" ID="txtDescription" TypeControl="TextBox" style="width:300px" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:CheckBox ID="chkActive" runat="server" /></td>
        </tr>
    </table>
</asp:Panel>
<%--FINE EDIT ROLE--%>
