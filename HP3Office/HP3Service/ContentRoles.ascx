<%@ Control Language="VB" ClassName="ContentRoles" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register TagPrefix="HP3" TagName="SearchEngine" Src="~/hp3Office/HP3Common/SearchEngine.ascx"  %>
<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx" %>

<script runat="server">
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    
    Private _webUtility As WebUtility
    Private dateUtility As New StringUtility
    
    Private oLManager As New LanguageManager()
    Private _contentTypeCollection As ContentTypeCollection = Nothing
    
    ' il datasource corrente
    Private _dataSource As RoleCollection = Nothing
    ' la lista degli id che hanno una relazione con il content
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As New ContentValue


    Public Property AllowEdit() As Boolean
        Get
            Return _allowEdit
        End Get
        Set(ByVal value As Boolean)
            _allowEdit = value
        End Set
    End Property
    
    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                _keyContent = New ContentIdentificator(objQs.ContentId, objQs.Language_id)
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
        End Set
    End Property
    
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As New ContentCollection
        
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        contentCollection = _contentManager.Read(so)
        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function
    
    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property
    
    Public ReadOnly Property Related() As List(Of Integer)
        Get
            If _related Is Nothing Then
                If Not UseInPopup Then
                    _related = readRelated(KeyContent)
                Else
                    _related = readSelection(KeyContent)
                End If
            End If
            Return _related
        End Get
    End Property

    Private ReadOnly Property KeyChanges() As String
        Get
            Dim _key As String
            _key = ViewState("RelatedChangesKey")
            If _key Is Nothing Then
                ' Genera una chiave random per assicurarsi che lo stato
                ' delle modifiche cambi tra una richiesta e l'altra
                _key = SimpleHash.GenerateUUID(32)
                ViewState("RelatedChangesKey") = _key
            End If
            Return _key
        End Get
    End Property

    Public Property RelatedChanges() As Dictionary(Of Integer, Boolean)
        Get
            If _changes Is Nothing Then
                _changes = Cache.Item(KeyChanges)
                If _changes Is Nothing Then
                    _changes = New Dictionary(Of Integer, Boolean)
                End If
            End If
            Return _changes
        End Get
        Set(ByVal value As Dictionary(Of Integer, Boolean))
            _changes = value
            Cache.Insert(KeyChanges, _changes, DateTime.Now.AddMinutes(10), True)
        End Set
    End Property

    'Public Function checkRelated(ByVal idRelation As Integer) As Boolean
    '    Return checkRelated(idRelation, False)
    'End Function

    'Public Function checkRelated(ByVal idRelation As Integer, ByVal onlyPreviouslyRelated As Boolean) As Boolean
    '    ' Verifica se il datasource degli elementi relazionati contiene la chiave corrente
    '    If Not Related Is Nothing AndAlso Related.Contains(idRelation) Then
    '        ' se si verifica che non sia stata deselezionata dall'utente
    '        If Not onlyPreviouslyRelated AndAlso Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
    '            Return RelatedChanges.Item(idRelation)
    '        Else
    '            ' non � stata deselezionata
    '            Return True
    '        End If
    '    ElseIf Not RelatedChanges Is Nothing AndAlso RelatedChanges.ContainsKey(idRelation) Then
    '        ' Se la chiave non era precedentemente selezionata, verifica se � stata selezionata dall'utente
    '        If Not onlyPreviouslyRelated Then
    '            Return RelatedChanges.Item(idRelation)
    '        Else
    '            Return False
    '        End If
    '    End If
    'End Function
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public Sub BindGrid()
        BindGrid(False)
    End Sub
    
    Public Sub BindGrid(ByVal clearCache As Boolean)
        ' Legge il datasource completo 
        getDataSource(clearCache)

        ' Effettua il bind del datasource sulla gridview
        gridRoles.DataSource = _dataSource
        gridRoles.DataBind()
    End Sub

    Public ReadOnly Property getGridRelations() As GridView
        Get
            Return gridRoles
        End Get
    End Property

    Private Sub LoadContentSummary()
        If KeyContent.Id > 0 Then

            lblContentId.Text = ContentValue.Key.Id
            If ContentValue.DatePublish.HasValue Then
                lblContentDatePublish.Text = getCorrectedFormatDate(ContentValue.DatePublish)
            Else
                lblContentDatePublish.Text = ""
            End If
            imgContentLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
            imgContentLanguage.Visible = True
            lblContentTitle.Text = ContentValue.Title
        Else
            imgContentLanguage.Visible = False
        End If

    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        If Not AllowEdit Then
            ContentSummary.Visible = False
        End If

        If Not Page.IsPostBack Then
            LoadContentSummary()
            ReadRelations()
        End If
        
        If objQs.ContentId = 0 And Not UseInPopup Then
            Dim _webUtility As New WebUtility
            'SearchEngine.DropDownContentType.Items.Clear()
            '_webUtility.LoadListControl(SearchEngine.DropDownContentType, ContentTypes, "Description", "Key.Id")
            SearchEngine.Visible = True
            pnlRelationEdit.Visible = False
        Else
            SearchEngine.Visible = False
            If Not Page.IsPostBack Then
                LoadControl()
            End If
        End If
       
    End Sub
    
    Private ReadOnly Property ContentTypes() As ContentTypeCollection
        Get
            If _contentTypeCollection Is Nothing Then
                Dim _contentTypeManager As New ContentTypeManager
                Dim _contentTypeSearcher As New ContentTypeSearcher
                _contentTypeSearcher.OnlyAssociatedContents = True
                _contentTypeCollection = _contentTypeManager.Read(_contentTypeSearcher)
            End If
            Return _contentTypeCollection
        End Get
    End Property

    Private Function getDataSource() As RoleCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As RoleCollection
        If _dataSource Is Nothing Then
            Dim _RoleSearcher As New RoleSearcher()
            If Not DataValueFilter Is Nothing AndAlso DataValueFilter <> "" Then
                _RoleSearcher.Key = New RoleIdentificator(Integer.Parse(DataValueFilter))
            End If
            If Not DataTextFilter Is Nothing AndAlso DataTextFilter <> "" Then
                _RoleSearcher.Description = "%" & DataTextFilter & "%"
            End If
            Dim _oRoleManager As New RoleManager()
            _oRoleManager.Cache = Not clearCache
            _dataSource = _oRoleManager.Read(_RoleSearcher)
        End If
        Return _dataSource
    End Function
    
    Private Function readSelection(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        If Not Request("txtHidden") Is Nothing AndAlso Request("txtHidden") <> "" Then
            _related = New List(Of Integer)
            _related.Add(Request("txtHidden"))
            RelatedChanges.Clear()
            RelatedChanges.Add(Request("txtHidden"), True)
            'CType(WebUtility.MyPage(Me).TextHidden, TextBox).Text = findClientId(Request("SelItem"))
            Return _related
        Else
            Return Nothing
        End If
        
    End Function

    'Private Function findClientId(ByVal idRelation As String) As String
    '    Response.Write("<h3>findclientid(" & idRelation & ")</h3>")
    '    Response.Write("rows = " & RelationEdit.getGridRelations.Rows.Count & "<br>")
    '    Dim radio As RadioButton
    '    For Each row As TableRow In RelationEdit.getGridRelations.Rows
    '        radio = row.FindControl("optRelation")
    '        If Not radio Is Nothing AndAlso radio.Text <> "" Then
    '            If radio.Text = idRelation Then
    '                Return radio.ClientID
    '            End If
    '        End If
    '    Next
    '    Return ""
    
    'End Function
    
    Private Function readRelated(ByVal keyContent As Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator) As System.Collections.Generic.List(Of Integer)
        
        If _related Is Nothing AndAlso Not keyContent Is Nothing Then
            Dim _oRoleRelatedCollection As RoleCollection = Nothing
            Dim _oContentManager As New ContentManager
            _oContentManager.Cache = False
            _oRoleRelatedCollection = _oContentManager.ReadContentRole(keyContent)
            If Not _oRoleRelatedCollection Is Nothing Then
                _related = New List(Of Integer)
                Dim _role As RoleValue
                For Each _role In _oRoleRelatedCollection
                    _related.Add(_role.Key.Id)
                Next
            End If
        End If
        Return _related
    End Function
    
    Private Overloads Sub LoadControl()
        Dim _oRoleCollection As RoleCollection = getDataSource() 'RelationEdit.DataValueFilter, "%" & RelationEdit.DataTextFilter & "%")
    
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)

        readRelated(_oKeyContent)
        
        BindGrid()
        pnlRelationEdit.Visible = True

    End Sub
    
    Protected Sub SearchEngine_SelectedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arr() As String = sender.CommandArgument.split("_")
        Dim id As Int32 = arr(0)
        Dim idLanguage As Int32 = arr(1)
        
        objQs.ContentId = id
        objQs.Language_id = idLanguage
        webRequest.customParam.append(objQs)
        Dim mPageManager As New MasterPageManager
        Response.Redirect(mPageManager.FormatRequest(webRequest))
    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub
 
    'Protected Sub ApplyAssociations(ByVal sender As Object, ByVal e As EventArgs)
        
    '    If Not AllowEdit Then Return
        
    '    SaveRelatedStatus()
        
    '    If Not RelatedChanges Is Nothing AndAlso RelatedChanges.Count > 0 Then

    '        Dim _oContentManager As New ContentManager
    '        Dim _oKeyContent As ContentIdentificator = _oContentManager.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
    '        Dim key As Integer
    '        Dim value As Boolean
    '        Dim _oKeyRole As RoleIdentificator
    '        For Each key In RelatedChanges.Keys
    '            value = RelatedChanges.Item(key)
    '            _oKeyRole = New RoleIdentificator(key)
    '            If value Then
    '                _oContentManager.CreateContentRole(_oKeyContent, _oKeyRole)
    '            Else
    '                _oContentManager.RemoveContentRole(_oKeyContent, _oKeyRole)
    '            End If
    '        Next
            
    '        readRelated(_oKeyContent)

    '        'Return True
    '    End If
        
    '    'Return False
    'End Sub
    
    'aggiunge una nuova relazione role/content
    Protected Sub addRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As Integer = sender.commandArgument.ToString
        Dim _oKeyRole As New RoleIdentificator()
        _oKeyRole.Id = key
        
        Me.BusinessContentManager.CreateContentRole(_oKeyContent, _oKeyRole)
        ReadRelations()
    End Sub
     
    'rimuove una relazione role/content
    Sub removeRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        '1149
        Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
        
        Dim key As String = sender.commandArgument.ToString
        Dim _oKeyRole As New RoleIdentificator()
        _oKeyRole.Id = key
        
        Me.BusinessContentManager.RemoveContentRole(_oKeyContent, _oKeyRole)
        
        ReadRelations()
    End Sub
    
    'legge le relazioni role/content
    Sub ReadRelations()
        If (objQs.ContentId <> 0) Then
            '1149
            Dim _oKeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(objQs.ContentId, objQs.Language_id)
            Me.BusinessContentManager.Cache = False
            Dim _oRoleCollection As RoleCollection = Me.BusinessContentManager.ReadContentRole(_oKeyContent)
            If Not _oRoleCollection Is Nothing Then
                gridActiveRelation.DataSource = _oRoleCollection
                gridActiveRelation.DataBind()
            End If
        End If
    End Sub
    
    'gestisce la paginazione del GridView 
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridRoles.PageIndexChanging
        'SaveRelatedStatus()
        gridRoles.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    'gestisce la ricerca dei ruoli 
    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        'SaveRelatedStatus()
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        'SaveRelatedStatus()
        filterDataSource()
    End Sub
    
    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        webRequest.customParam.LoadQueryString()
        Dim contentType = webRequest.customParam.item("Ct")
        
        webRequest.KeycontentType.Id = contentType
        webRequest.customParam.append("C", 0)
        webRequest.customParam.append("L", Me.BusinessMasterPageManager.GetLang.Id)
        Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        
        'Response.Redirect(GetBackUrl)
    End Sub

    Function GetBackUrl() As String
        Dim mPageManager As New MasterPageManager()
        Dim webRequest As New WebRequestValue

        objQs.ContentId = 0

        webRequest.customParam.append(objQs)
        Return mPageManager.FormatRequest(webRequest)
    End Function

    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        Return dateUtility.FormatDateFromLanguage(DatePublish, Me.BusinessMasterPageManager.GetLang.Code)
    End Function
</script>
<hr />
<HP3:SearchEngine ID="SearchEngine" visible="false" UseInRelation ="true"  AllowEdit="false" AllowSelect="true" ShowContentType="true" runat="server" OnSelectedEvent="SearchEngine_SelectedEvent" />
<hr />

<asp:Panel ID="pnlRelationEdit" runat="server">
    <asp:Button ID="btBackToList" Text="New Relation" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;margin-bottom:10px;background-image:url('/HP3Office/HP3Image/Button/new_relation.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
    <%--Pannello del Content Selezionato--%>    
    <asp:Panel ID="ContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div>
              <br />
        <table class="tbContentSelected">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:20%">Date Publish</th>
                <th style="width:5%"></th>
                <th style="width:70%">Title</th>
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
     <%--Pannello delle Relazioni attive--%>
   <asp:Panel ID="pnlActiveRelations" runat="server">
        <div class="title">Active Relations</div>
        <br />
            <asp:GridView ID="gridActiveRelation"
                AutoGenerateColumns="false" 
                AllowPaging="false" 
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" 
                AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" 
                width="100%"
              
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                              <%#DataBinder.Eval(Container.DataItem, "Description")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="2%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No relations found.
                </EmptyDataTemplate>
            </asp:GridView>
    </asp:Panel>
    <hr />
      
     <%-- Pannello del Role Searcher--%>
    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Role Searcher</span></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td>Id</td>
                <td>Description</td>
            </tr>
            <tr>
                <td><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" Style="width:50px;" /></td>
                <td><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></td>
            </tr>
        </table>
        <table class="form">
            <tr>
                <td><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
                <td><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
  
   <%-- <asp:Button ID="btApply"  runat="server" Visible="false"/>--%>
   
    <asp:GridView ID="gridRoles" 
        AutoGenerateColumns="false"
        AllowPaging="true" 
        PagerSettings-Position="TopAndBottom" 
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager" 
        PageSize="20"
        AllowSorting="false"
        ShowHeader="true" 
        HeaderStyle-CssClass="header"
        ShowFooter="true" 
        FooterStyle-CssClass="gridFooter"
        GridLines="None" 
        AlternatingRowStyle-BackColor="#eeefef" 
        width="100%"
        runat="server">
        
        <Columns>
            <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                <ItemTemplate>
                     <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "Description")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="2%">
                <ItemTemplate>
                     <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.Id%>' CommandName ="Add" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No items found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Panel>