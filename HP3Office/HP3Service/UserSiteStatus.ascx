﻿<%@ Control Language="VB" ClassName="UserSiteStatus" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%> 

<script runat="server">
    Private utility As New WebUtility
    Private userId As New Integer
    Private currentsiteID As New Integer
    Private dictionaryManager As New Healthware.HP3.Core.Localization.DictionaryManager
    Private _strDomain As String
    Private _language As String
    Private strDomain As String
    Private strJs As String
    
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = Me.PageObjectGetLang.Id
        Domain() = strDomain
        userId = Request.QueryString("us")
   
        LoadCurrentUser(userId)
        loadMySite(userId)
       
              
    End Sub
    
    Sub LoadCurrentUser(ByVal id As Integer)
       
        Dim userCollection As UserCollection
        Dim SearcherUser As New UserSearcher
        Dim userValues As New UserValue
       
     
        SearcherUser.Key.Id = id
        SearcherUser.Properties.Add("Key.id")
        SearcherUser.Properties.Add("Name")
        SearcherUser.Properties.Add("Surname")
        SearcherUser.Properties.Add("Status")
        
       
        userCollection = Me.BusinessUserManager.Read(SearcherUser)
        
        If (Not userCollection Is Nothing AndAlso userCollection.Any) Then
            userValues = userCollection(0)
            Label1.Text = userValues.Name
            Label2.Text = userValues.Surname
            Label3.Text = id
            Label4.Text = getStatus(userValues)
            
        End If
        
    End Sub
    Sub loadMySite(ByVal userid As Integer)
        
      
        Dim so As New UserSearcher
        Dim userStatusSearch As New StatusSiteSearcher
        userStatusSearch.User.Id = userid
        Me.BusinessUserManager.Cache = False
        Dim coll As StatusSiteCollection = Me.BusinessUserManager.ReadStatus(userStatusSearch)
        'AndAlso coll(0).KeySiteArea.Id > 0
       
        If (Not coll Is Nothing AndAlso coll.Any) Then
            
            
            gdw_siteList.DataSource = coll
            gdw_siteList.DataBind()
        Else
            gdw_siteList.DataSource = Nothing
            gdw_siteList.DataBind()
        End If
    End Sub
    
    Sub loadViewStatus(ByVal currentsite As Integer)
        Dim stutusvalue As New StatusSiteValue
          
        Dim userm As New UserManager
        Dim so As New StatusSiteHistorySearcher
     
        so.User.Id = userId  'Current id user
        so.SiteArea.Id = currentsite 'current site
        userm.Cache = False
        Dim coll As StatusSiteCollection = userm.ReadStatusHistory(so)
       
        If (Not coll Is Nothing AndAlso coll.Any) Then
            siteNameview.Text = "Trace Site View: " &coll(0).SiteAreaDescription
            siteNameview.Visible = True
            viewInfosito.Visible = True
            gdw_viewStatusHistory.DataSource = coll
            gdw_viewStatusHistory.DataBind()
        End If
    End Sub
    
 
    Sub insertStatusSitevalue(ByVal sender As Object, ByVal e As EventArgs)
       
        If dwlSite.SelectedValue <> 0 Then
            Dim userm As New UserManager
            Dim vo As New BaseStatusSiteValue
            vo.KeySiteArea.Id = dwlSite.SelectedValue
            vo.Note = Note.Text
            vo.StatusId = dwlStatus.SelectedValue
            vo.UserOwner.Key.Id = Me.ObjectTicket.User.Key.Id   'id utente che gestisce l'attivazione
        
            
            Dim bo As Boolean = userm.CreateUserSiteStatus(New UserIdentificator(userId), vo)
          
       
            userId = Request.QueryString("us")
   
            LoadCurrentUser(userId)
            loadMySite(userId)
        
             Archivie()
            infosito.Visible = False
            statusNote.Visible = False
            btnSave.Visible = False
            btnArchive.Visible = False
            viewInfosito.Visible = False
            goTouser.Visible = True
            pnlGrid.Visible = True
            btnNew.Visible = True
        Else
            If dwlSite.Items.Count = 1 Then
                strJs = "alert('There are no sites to associate to this user!!! Please read the Site info in the helper pop-up')"
            End If
            If dwlSite.Items.Count > 1 Then
                strJs = "alert('Site is a required field!!')"
            End If
            
        End If
    End Sub
    
    
    Sub updatestatusitevalue(ByVal sender As Object, ByVal e As EventArgs)
        
        currentsiteID = sender.CommandArgument
        Dim userm As New UserManager
        Dim vo As New BaseStatusSiteValue
        vo.KeySiteArea.Id = currentsiteID  'sitearea di tipo sito FISSO perchè in update
        vo.Note = Note.Text
        vo.StatusId = dwlStatus.SelectedValue
        vo.UserOwner.Key.Id = Me.ObjectTicket.User.Key.Id   'id utente che gestisce l'attivazione
       
        Dim bo As Boolean = userm.UpdateUserSiteStatus(New UserIdentificator(userId), vo)
        loadMySite(userId)
        Archivie()

    End Sub
    
    
    'restituisce lo status dell'utente
    Function getStatus(ByVal userValue As UserValue) As String
        Dim out_str As String = ""
        
        Select Case userValue.Status
            Case 1
                out_str = "Active"
            Case 2
                out_str = "Pending"
            Case 3
                out_str = "No Active"
            Case 4
                out_str = "Disabled"
            Case 5
                out_str = "Deleted"
        End Select
        
        Return out_str
    End Function
    
    Sub loadDropdownlistSite()
        Dim siteManager As New SiteManager
        Dim siteSitesearch As New SiteSearcher
        Dim collSitesearch As New SiteCollection
        Dim siteValue As New SiteValue
        
        
        
        Dim collSiteAppoggio As New SiteCollection
        siteSitesearch.AccessType = True
        siteManager.Cache = False
        collSitesearch = siteManager.Read(siteSitesearch) 'da aggiungere il filtro che deve essere creato per accsess type
        
        collSitesearch = collSitesearch.DistinctBy("KeySiteArea.Id")
       
       
        collSiteAppoggio.AddRange(collSitesearch)
        
        
        Dim userSatusManager As New UserManager
        Dim userstatussearch As New StatusSiteSearcher
        Dim userstatusitecoll As New StatusSiteCollection
        userstatussearch.User.Id = userId 'Current user id
        userSatusManager.Cache = False
        Dim coll As StatusSiteCollection = Me.BusinessUserManager.ReadStatus(userstatussearch)
        
       
        If (Not coll Is Nothing AndAlso coll.Any) Then
          
            For Each CollValue As StatusSiteValue In coll
              
                For Each cvalue As SiteValue In collSiteAppoggio
                    
                    If CollValue.KeySiteArea.Id = cvalue.KeySiteArea.Id Then
                        collSitesearch.Remove(cvalue)
                    End If
                Next
               
              
                        
            Next
           
            utility.LoadListControl(dwlSite, collSitesearch, "Label", "KeySiteArea.Id")
            dwlSite.Items.Insert(0, New ListItem("--Select--", 0))
       
                
        End If
        
        
    End Sub
    
    Sub newStatusAssign()
        btnIDnew.Visible = False
        pnlGrid.Visible = False
        infosito.Visible = True
        dwlSite.Visible = True
        btnSave.Visible = True
        btnArchive.Visible = True
        statusNote.Visible = True
        siteName.Visible = False
        viewInfosito.Visible = False
        loadDropdownlistSite()
    End Sub
    
    Sub Archivie()
        btnIDnew.Visible = True
        pnlGrid.Visible = True
        infosito.Visible = False
        dwlSite.Visible = False
        btnSave.Visible = False
        statusNote.Visible = False
        btnArchive.Visible = False
        viewStatususer.Visible = False
        btnUpdate.Visible = False
        viewInfosito.Visible = False
    End Sub
       
    Sub Loadview(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        
        Dim siteAreaId As Integer
        
        siteAreaId = sender.CommandArgument
        
        loadViewStatus(siteAreaId)
        
        btnIDnew.Visible = False
        pnlGrid.Visible = False
        infosito.Visible = False
        dwlSite.Visible = False
        btnSave.Visible = False
        statusNote.Visible = False
        btnArchive.Visible = True
        viewStatususer.Visible = True
        btnUpdate.Visible = False
    End Sub
    
    Sub LoadEditButtom(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim siteAreaId As Integer
        
        siteAreaId = sender.CommandArgument
       
    
        Me.BusinessUserManager.Cache = False
        Dim vo As StatusSiteValue = Me.BusinessUserManager.ReadStatusBySite(New UserIdentificator(userId), New SiteAreaIdentificator(siteAreaId))
        
        currentsiteID = vo.KeySiteArea.Id
       
        siteName.Text = vo.SiteAreaDescription
        dwlStatus.SelectedValue = vo.StatusId
        Note.Text = vo.Note
                
        btnIDnew.Visible = False
        siteName.Visible = True
        pnlGrid.Visible = False
        infosito.Visible = True
        dwlSite.Visible = True
        btnSave.Visible = False
        btnArchive.Visible = True
        statusNote.Visible = True
        btnUpdate.Visible = True
        btnUpdate.CommandArgument = currentsiteID
        dwlSite.Visible = False
    End Sub
    
    Sub goTousurview(ByVal s As Object, ByVal e As EventArgs)
    
        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
        
        wr.KeycontentType = New ContentTypeIdentificator("CMS", "User")
        wr.customParam.append(objQs)
        wr.customParam.append("us", "0")
        Response.Redirect(mPage.FormatRequest(wr))
    End Sub
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    
    
</script>
<script type="text/javascript">
       <%=strJs%>                
</script>

<style type="text/css">
    
legend {
    font-size:  1.4em;
    font-weight:  bold;
    position:  relative;
    top:  -.4em;
}

td {
   
    padding: 4px;
    text-align: left;
}

</style>
<div align="left">
<asp:button ID="btnArchive" runat="server" visible="false" Text="Archive" onclick="Archivie"  CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
 <asp:button runat="server" ID="btnSave" visible="false" onclick="insertStatusSitevalue" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 <asp:button runat="server" ID="btnUpdate" visible="false" onclick="updatestatusitevalue" Text="Update" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
<div id="btnIDnew" runat="server">
   
    <h4><asp:button ID="goTouser" runat="server" visible="true" Text="Archive"  OnClick="goTousurview" CssClass="button"  style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    <asp:Button id ="btnNew" runat="server" Text="New"  OnClick="newStatusAssign" CssClass="button"  style=" margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    
    </h4>
 </div>

 <hr />

<div id="userinfo" class="form">

<fieldset>
<legend style= 'color:#336699'> User Info:</legend>
(<asp:Label ID="Label3" runat="server"    Text="" Font-Bold="True"  ForeColor="#336699" ToolTip="ID"></asp:Label>)
<asp:Label ID="Label1" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Name"></asp:Label>
<asp:Label ID="Label2" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Surname" ></asp:Label>|
<asp:Label ID="Label4" runat="server"  Text="" Font-Bold="True" ForeColor="#336699" ToolTip="Status" ></asp:Label>
</fieldset>

</div>
<hr />
<br /> <br /> <br /> 
<asp:Panel id="pnlGrid" runat="server" visible="true"> 
 
 <asp:Label CssClass="title" runat="server" id="sectionTit">My Site:</asp:Label>
 <br /> 
 <asp:gridview id="gdw_siteList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
              
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="ID Site">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "KeySiteArea.id")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Site">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "SiteAreaDescription")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Status site">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "SiteStatusLabel")%></ItemTemplate>
                </asp:TemplateField>  
                 <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Operation Date">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "RegistrationDate")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="20%">
                       <ItemTemplate>
                            <asp:ImageButton ID="Edit" runat="server" OnClick="LoadEditButtom"  ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeySiteArea.Id")%>'/>
                            <asp:ImageButton ID="View" runat="server" OnClick="Loadview" ToolTip ="View item" ImageUrl="~/hp3Office/HP3Image/Ico/application_view_tile.png"  CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeySiteArea.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>
<div id="viewInfosito" runat="server" visible="false">
    <fieldset>
       <legend style='color: #336699'> </legend>
        
        <asp:Label ID="siteNameview" runat="server" Visible="false"  ForeColor="#336699" Text=""></asp:Label>
       
    </fieldset>
</div>
<div id="infosito" runat="server" visible="false">
    <table class="form" style="width: 100%">
        <tr>
        <td width="229px" ><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSiteDrop&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormSite", "Site")%></td>
            
            <td>
                <asp:DropDownList ID="dwlSite" runat="server" Visible="false" />
                <asp:Label ID="siteName" runat="server" Visible="false" Text=""></asp:Label>
            </td>
      
        </tr>
    </table>
</div>
<div id="statusNote" runat="server" visible="false">
    <table class="form" style="width: 100%">
        <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUserStatus&Help=cms_FormUserStatus&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormUserStatus", "Status")%></td>
            
            <td>
                <asp:DropDownList ID="dwlStatus" runat="server">
                    <asp:ListItem Text="--Select Status--" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Pending" Value="2"></asp:ListItem>
                    <asp:ListItem Text="No active" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Disabled" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Deleted" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
   
        <tr>
           <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormNote&Help=cms_FormNote&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormNote", "Note")%></td>
            <td>
                <asp:TextBox ID="Note" runat="server" Columns="22" Rows="8" TextMode="MultiLine"></asp:TextBox>
            </td>
        
            <%--<div> Operazione gestita da: <asp:Label ID="ownerUserName" runat="server"  Text="" Font-Bold="True"></asp:Label>|<asp:Label ID="ownerUserSurname" runat="server"  Text="" Font-Bold="True"></asp:Label></div>--%>
        </tr>
    </table>
</div>
 

 <asp:Panel id="viewStatususer" runat="server" visible="false"> 
 
 <%--<asp:Label CssClass="title" runat="server" id="Label6">View</asp:Label>--%>
 <asp:gridview id="gdw_viewStatusHistory" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                 <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Status">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "SiteStatusLabel")%></ItemTemplate>
                </asp:TemplateField>
                
                 <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Owner">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "UserOwner.Name")%>-<%# DataBinder.Eval(Container.DataItem, "UserOwner.Surname")%></ItemTemplate>
                </asp:TemplateField>
               
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Note">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "Note")%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Operation Date">
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "OperationDate")%></ItemTemplate>
                </asp:TemplateField>

             
                
              </Columns>
 </asp:gridview>
 </asp:Panel>

 