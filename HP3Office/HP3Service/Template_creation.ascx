<%@ Control Language="VB" ClassName="Template_creation" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<script runat="server">
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private strJs As String
    Private _typecontrol As ControlType = ControlType.View
    Private oGenericUtlity As New GenericUtility
    Private strDomain As String
    Private _sortType As TemplateNesletterGenericComparer.SortType
    Private _sortOrder As TemplateNesletterGenericComparer.SortOrder
    Private _strDomain As String
    Private _language As String
    
    Private masterPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdTemplate() As Int32
        Get
            Return ViewState("SelectedIdTemplate")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdTemplate") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
           
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna l'oggetto relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadTemplateValue() As TemplateNewsletterValue
        If SelectedIdTemplate <> 0 Then
            Return oGenericUtlity.GetTemplateValue(SelectedIdTemplate)
        End If
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oTNValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oTNValue As TemplateNewsletterValue)
        If Not oTNValue Is Nothing Then
            With oTNValue
                Description.Text = .Description
                Path.Text = .Path
                                               
            End With
            
        End If
    End Sub
    
    ''' <summary>
    ''' Questo metodo effettua il caricamento vero e proprio del controllo.    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")                
                LoadFormDett(LoadTemplateValue)
                
            Case ControlType.View
                rowToolbar.Visible = True
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListTemplate)
                End If
            Case ControlType.Selection
                rowToolbar.Visible = False
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListTemplate)
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = masterPageManager.GetLang.Id
        
        pnlMsg.Visible = False
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView)
        Dim oNewsletterManager As New NewsletterManager
        oNewsletterManager.Cache = False
                        
        Dim oTemplateNewsletterCollection As TemplateNesletterCollection = oNewsletterManager.Read
        
        If Not oTemplateNewsletterCollection Is Nothing Then
            oTemplateNewsletterCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = oTemplateNewsletterCollection
        objGrid.DataBind()
                
    End Sub
    
          
    ''' <summary>
    ''' Caricamento della griglia dei temi
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
                
        Dim oNewsletterManager As New NewsletterManager
               
        ReadSelectedItems()
        BindGrid(objGrid)
    End Sub
    
    Sub SearchThemes(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListTemplate)
    End Sub
    
     
    ''' <summary>
    ''' Ritorna il value object del template in base all'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadTemplateNewsletterValue(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetTemplateValue(id)
        
        If ov Is Nothing Then
            Return ""
        Else
            Return ov.description
        End If
    End Function
         
    ''' <summary>
    ''' Gestione dell'ordinamento e impostazione dell'ordinamento di default
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SortType() As TemplateNesletterGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = TemplateNesletterGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As TemplateNesletterGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    ''' <summary>
    ''' Gestione dell'ordinamento ASC / DESC e impostazione di default
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SortOrder() As TemplateNesletterGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = TemplateNesletterGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As TemplateNesletterGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListTemplate.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litTheme")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Imposta l'option button selezionato per default
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDefaultChecked() As String
        Set(ByVal value As String)
            Dim arr As ArrayList = CheckedItems
            arr.Clear()
            arr.Add(value)
            CheckedItems = arr
        End Set
    End Property
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    ''' <summary>
    ''' Legge la selezione effettuata nella griglia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As TemplateNesletterCollection
        Get
            Select Case TypeControl
                Case ControlType.Selection
                    ReadSelectedItems()
                    
                    Dim curTemplateValue As TemplateNewsletterValue
                    Dim outTemplateCollection As New TemplateNesletterCollection
            
                    For Each str As String In CheckedItems
                        curTemplateValue = oGenericUtlity.GetTemplateValue(Int32.Parse(str))
                        If Not curTemplateValue Is Nothing Then
                            outTemplateCollection.Add(curTemplateValue)
                        End If
                    Next
            
                    Return outTemplateCollection
            End Select
        End Get
    End Property
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
              
        TypeControl = ControlType.View
        SelectedIdTemplate = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListTemplate)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ClearForm()
        Dim id As Int32 = s.commandargument
        btnSave.Text = "Update"
                
        SelectedIdTemplate = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""

        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                NewItem = False
                
                ShowRightPanel()
            Case "DeleteItem"
                If RemoveFile(SelectedIdTemplate) Then
                    Dim oNewsletterManager As New NewsletterManager
                    Dim oTemplateIdentificator As New TemplateNesletterIdentificator
                    oTemplateIdentificator.Id = SelectedIdTemplate
                    oNewsletterManager.Remove(oTemplateIdentificator)
                End If
                
                BindWithSearch(gridListTemplate)
        End Select
    End Sub
    
    Function RemoveFile(ByVal id As Int32) As Boolean
        Try
            Dim sFileName As String
            Dim oTemplateValue As TemplateNewsletterValue = oGenericUtlity.GetTemplateValue(id)
            
            If Not oTemplateValue Is Nothing Then
                sFileName = oTemplateValue.Path
            End If
            
            Dim strFullFolder As String = TemplatePath & "\" & sFileName
            If TemplateExist(sFileName) Then
                IO.File.Delete(strFullFolder)
                Return True
            Else
                strJs = "alert('Template File not found. Remove record from database whatever.')"
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
    
    ReadOnly Property FileName()
        Get
            If Path.Text <> "" Then
                Return Path.Text & ".ascx"
            Else
                Return ""
            End If
        End Get
    End Property
    
    ''' <summary>
    ''' Save/Update del Template
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        If TemplateExist(FileName) Then
            pnlMsg.Visible = True
            litMsg.Text = "File already exist"
            Return
        End If
        
        If FileName = "" Then
            pnlMsg.Visible = True
            litMsg.Text = "File name missing"
            Return
        End If
                   
        If SaveFile(FileName) Then
            Dim oNewsletterManager As New NewsletterManager
            Dim oTemplateValue As TemplateNewsletterValue = ReadFormValues()
        
            If NewItem Then
                oTemplateValue = oNewsletterManager.Create(oTemplateValue)
            Else
                oTemplateValue = oNewsletterManager.Update(oTemplateValue)
            End If
        
            If oTemplateValue Is Nothing Then
                strJs = "alert('Error during saving')"
            Else
                strJs = "alert('Save successful')"
                goBack(Nothing, Nothing)
            End If
        End If
    End Sub
    
    ''' <summary>
    ''' Salva il template
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function SaveFile(ByVal sFileName As String) As Boolean
        Dim strFullFolder As String = TemplatePath & "\" & sFileName
        
        'Controllo se esiste il file di template
        If Not TemplateExist(ConfigurationsManager.TemplateFile_Name) Then
            strJs = "alert('Standard Template not found in: " & TemplatePath & ". Abort process.')"
            goBack(Nothing, Nothing)
            Return False
        End If
        
        'Altrimenti eseguo una copia del file
        Dim oFIle As New IO.FileInfo(TemplatePath & "\" & ConfigurationsManager.TemplateFile_Name)
        oFIle.CopyTo(strFullFolder)
        Return True
    End Function
        
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As TemplateNewsletterValue
        Dim oTemplateValue As New TemplateNewsletterValue
       
        With oTemplateValue
            .Key.Id = SelectedIdTemplate
            .Description = Description.Text
            .Path = FileName
        End With
        
        Return oTemplateValue
    End Function
    
    ''' <summary>
    ''' Creazione nuovo template
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewTemplate(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.Edit
        SelectedIdTemplate = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlDett.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""           
            End If
        Next
        
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momenti ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        TemplateId.InnerText = ""
        TemplateDescrizione.InnerText = "New Template"
    End Sub

    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkCopy As ImageButton
        Dim lnkActive As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litTheme")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkCopy = e.Row.FindControl("lnkCopy")
        lnkActive = e.Row.FindControl("btnActive")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                    
                    If Not lnkActive Is Nothing Then
                        
                        If CType(e.Row.DataItem.Active, OperationType).Name = SelectOperation.Enabled.Name Then
                            'If e.Row.DataItem.Active Then
                            lnkActive.ImageUrl = "~/hp3Office/HP3Image/Ico/content_active.gif"
                            lnkActive.CommandName = "1"
                            lnkActive.ToolTip += ": Active"
                        Else
                            lnkActive.CommandName = "0"
                            lnkActive.ImageUrl = "~/hp3Office/HP3Image/Ico/content_noactive.gif"
                            lnkActive.ToolTip += ": Disactive"
                        End If
                        
                    End If
                Case ControlType.Edit, ControlType.View
                        ctl.Visible = False
                        lnkSelect.Visible = False
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListTemplate.PageIndex = 0
       
        Select Case e.SortExpression
            Case "Id"
                If SortType <> TemplateNesletterGenericComparer.SortType.ById Then
                    SortType = TemplateNesletterGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> TemplateNesletterGenericComparer.SortType.ByLabel Then
                    SortType = TemplateNesletterGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Path"
                If SortType <> TemplateNesletterGenericComparer.SortType.ByPath Then
                    SortType = TemplateNesletterGenericComparer.SortType.ByPath
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
    
    ''' <summary>
    ''' Verifica l'esistenza del template nel folder
    ''' </summary>
    ''' <param name="strPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function TemplateExist(ByVal strPath As String) As Boolean
        Return IO.File.Exists(TemplatePath & "\" & strPath)
    End Function
    
    ''' <summary>
    ''' Modifica lo stato del template
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub SetStatus(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim oNewsletterManager As New NewsletterManager
        Dim oTemplateValue As TemplateNewsletterValue = oGenericUtlity.GetTemplateValue(s.commandargument)
        
        If Not oTemplateValue Is Nothing Then
            If CType(oTemplateValue.Active, OperationType).Name = SelectOperation.Enabled.Name Then
                oTemplateValue.Active = False
            Else
                oTemplateValue.Active = True
            End If
            
            oNewsletterManager.Update(oTemplateValue)
            BindWithSearch(gridListTemplate)
        End If
    End Sub
    
    ''' <summary>
    ''' Percorso del template
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property TemplatePath() As String
        Get
            Return SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterTemplatePath
        End Get
    End Property
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
</script>

<script type="text/javascript" >                  
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
        <%=strJs%>
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid" runat="server">
    <table class="form">
        <tr id="rowToolbar" runat="server">
            <td  colspan="4"><asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewTemplate" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
        </tr>
    </table>
    <hr />
    
    <%--LISTA CONTENTS EDITORIAL--%>
    <asp:gridview ID="gridListTemplate" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    style="width:100%"
                    AllowPaging="true"                
                    AllowSorting="true"                
                    PageSize ="20"               
                    emptydatatext="No themes available"                                                          
                    OnRowDataBound="gridRowDataBound" 
                    OnPageIndexChanging="gridPageIndexChanging" 
                    OnSorting="gridSorting"
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager">
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="3%">
                        <ItemTemplate>
                            <asp:radiobutton ID="chkSel" runat="server" />
                            <asp:literal ID="litTheme" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:Image visible="<%#not TemplateExist(Container.DataItem.Path)%>" ID="imgWarning" runat="server" ToolTip ="Template File Not Found" ImageUrl="~/hp3Office/HP3Image/ico/error.gif" />                        
                        </ItemTemplate>
                     </asp:TemplateField>                 
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>               
                    <asp:BoundField HeaderStyle-Width="50%" DataField ="Description" HeaderText ="Description" SortExpression="Description" />                           
                    <asp:BoundField HeaderStyle-Width="25%" DataField ="Path" HeaderText ="Path" SortExpression="Path" />                
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton  ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Conform Delete?')"  OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>                                                
                            <asp:ImageButton ID="btnActive" runat="server" ToolTip="Template Status" onclick="SetStatus" commandargument='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>
<%--FINE LISTA CONTENTS EDITORIAL--%>


<%--EDIT CONTENTS EDITORIAL--%>
<asp:Panel id="pnlDett" runat="server">
    <asp:button ID="btnBack" runat="server" Text="Archive" onclick="goBack" cssclass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:button ID="btnSave" runat="server" Text="Save"  onclick="Update" cssclass="button" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/><br /><br />
    <span id="TemplateId" class="title" runat ="server"/>
    <span id="TemplateDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormPath&Help=cms_HelpNewsLetterServicePath&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormPath", "Path")%></td>
            <td>
                <HP3:Text runat="server" ID="Path" TypeControl="TextBox" style="width:300px" />
                <asp:Panel ID="pnlMsg" runat="server" Visible="false" ForeColor ="red">
                    <asp:image runat="server" ImageUrl ="~/hp3Office/HP3Image/Ico/error.gif"  ImageAlign ="Middle" />    
                    <asp:literal ID="litMsg" runat="server" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpNewsletSerDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat="server" ID="Description" TypeControl="TextBox" style="width:300px" /></td>
        </tr>

    </table>
</asp:Panel>
<%--FINE CONTENTS EDITORIAL--%>

