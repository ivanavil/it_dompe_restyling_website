<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>  
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>  
<%@ Import Namespace="Healthware.HP3.Core.Community" %>
<%@ Import Namespace="Healthware.HP3.Core.Community.ObjectValues" %>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<script runat="server">
    Private systemUtility As New SystemUtility()
    Private mPageManager As New MasterPageManager()
    Private objCMSUtility As New GenericUtility
    Private dictionaryManager As New DictionaryManager
    Private _strDomain As String
    Private _language As String
    
    Private strJS As String
    Private strDomain As String
    Private strName As String
    Private siteLabel As String
    Private selectedContentId As Integer
    Private _languageId As Integer
    Private PrivateData As Boolean = True
    Private communityManager As New CommunityManager
    
    Const Bacheca As Integer = BaseServiceValue.ServiceType.Board
    Const Newsgroup As Integer = BaseServiceValue.ServiceType.NewsGroup
    Const Message As Integer = BaseServiceValue.ServiceType.Message
    Const Link As Integer = BaseServiceValue.ServiceType.Link
    Const Material As Integer = BaseServiceValue.ServiceType.Material
    Const Forum As Integer = BaseServiceValue.ServiceType.Forum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
         
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language() = mPageManager.GetLang.Id
        Domain() = strDomain
            
        If Not (Page.IsPostBack) Then
            If mPageManager.GetContent.Id = 0 Then
                ActivePanelGrid()
                LoadArchive()
            End If
        End If
    End Sub
      
    'recupera la lista di Community
    Sub LoadArchive()
        Dim communitySearcher As New CommunitySearcher
        Dim communityColl As New CommunityCollection
          
        communityManager.Cache = False
        communityColl = communityManager.Read(communitySearcher)
        
        gdw_communityList.DataSource = communityColl
        gdw_communityList.DataBind()
    End Sub
    
    'visualizza le informazioni di una community
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim communityValue As New CommunityValue
        Dim communitySearcher As New CommunitySearcher
        Dim selectItem As String
        
        If sender.CommandName = "SelectItem" Then
            selectItem = sender.CommandArgument
            Dim resultStrings() As String = selectItem.Split("|")
            Dim itemId As Integer = CType(resultStrings(0), Integer)
            Dim itemLanguageId As Integer = CType(resultStrings(1), Integer)
           
            SelectedId = itemId
                        
            communityValue = communityManager.Read(New CommunityIdentificator(itemId, itemLanguageId))
            If Not communityValue Is Nothing Then
                LoadDett(communityValue)
                ActivePanelDett()
            End If
        End If
    End Sub
       
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim communityValue As New CommunityValue
        Dim outCommunityValue As New CommunityValue
                                 
        If (IsNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni
            With (communityValue)
                .Title = communityName.Text
                '.Launch = communityLaunch.Value
                .Launch = communityLaunch.Content
                
                'Response.Write("ct" & Me.BusinessMasterPageManager.GetContenType.Id)
                .ContentType.Key.Id = Me.BusinessMasterPageManager.GetContenType.Id
                .Key.IdLanguage = lLanguage.Language
                '.NotificationMessageBody = notificationMessageBody.Value
                .NotificationMessageBody = notificationMessageBody.Content
                '.NotificationMessageTitle = notificationMessageTitle.Value
                .NotificationMessageTitle = notificationMessageTitle.Content
                
                If (dwlType.SelectedItem.Value = 1) Then
                    .Type = Healthware.HP3.Core.Community.ObjectValues.CommunityValue.CommunityType.PublicCommunity
                Else
                    .Type = Healthware.HP3.Core.Community.ObjectValues.CommunityValue.CommunityType.PrivateCommunity
                End If
                
                .DateInsert = insertDate.Value
                .DatePublish = publishDate.Value
                
                If (chkActive.Checked = True) Then
                    .Active = True
                Else
                    .Active = False
                End If
                
                If (chkDisplay.Checked = True) Then
                    .Display = True
                Else
                    .Display = False
                End If
                
                'ctlSiteArea
            End With
            
            If SelectedId = 0 Then
                outCommunityValue = communityManager.Create(communityValue)
                LoadArchive()
                ActivePanelGrid()
                
                SaveCommunitySiteArea(outCommunityValue.Key.Id, outCommunityValue.Key.IdLanguage)
            Else
                Dim communityIdentificator As New CommunityIdentificator
                Dim commValue As New CommunityValue
               
                communityIdentificator.Id = SelectedId
                commValue = communityManager.Read(communityIdentificator)
               
                If Not (commValue Is Nothing) Then
                    communityValue.KeyCommunity.Key = commValue.KeyCommunity.Key
                    communityValue.Key.PrimaryKey = commValue.Key.PrimaryKey
                                      
                End If
                
                outCommunityValue = communityManager.Update(communityValue)
                
                SaveCommunitySiteArea(SelectedId, outCommunityValue.Key.IdLanguage)
                LoadArchive()
                ActivePanelGrid()
            End If
             
            SaveCommunityServiceRelation(outCommunityValue.KeyCommunity, outCommunityValue.Key.IdLanguage)
        
                       
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
       
    End Sub
        
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
    
    'cancella una riga 
    Sub DeleteRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim communityValue As New CommunityValue
        Dim removeItem As String
        Dim risRemove As Boolean
              
        If sender.CommandName = "DeleteItem" Then
            removeItem = sender.CommandArgument
            Dim resultStrings() As String = removeItem.Split("|")
            Dim itemId As Integer = CType(resultStrings(0), Integer)
            Dim itemLanguageId As Integer = CType(resultStrings(1), Integer)
                        
            SelectedId = itemId
            
            risRemove = communityManager.Delete(New CommunityIdentificator(itemId, itemLanguageId))
                                  
            LoadArchive()
            ActivePanelGrid()
            
            'Gestione dei valori di ritorno
            If Not risRemove Then
                CreateJsMessage("Impossible to delete this Community")
            End If
                       
        End If
    End Sub
    
    'popola i campi di testo con le informazioni contenute nello CommunityValue
    Sub LoadDett(ByVal communityValue As CommunityValue)
        Dim oserviceColl As New ServiceCollection
        
        If Not communityValue Is Nothing Then
            'lLanguage.DefaultValue = communityValue.Key.IdLanguage '
            lLanguage.LoadLanguageValue(communityValue.Key.IdLanguage)
            
            communityName.Text = communityValue.Title
            'communityLaunch.Value = communityValue.Launch
            communityLaunch.Content = communityValue.Launch
            
            dwlType.SelectedIndex = dwlType.Items.IndexOf(dwlType.Items.FindByValue(communityValue.Type))
            
            'notificationMessageTitle.Value = communityValue.NotificationMessageTitle
            notificationMessageTitle.Content = communityValue.NotificationMessageTitle
            'notificationMessageBody.Value = communityValue.NotificationMessageBody
            notificationMessageBody.Content = communityValue.NotificationMessageBody
              
            'da decidere se aggiungere
            'ctlSite
            
            'gestione aree
            ctlAree.GenericCollection = GetCommunitySiteAreaValue(communityValue.Key.Id)
            ctlAree.IsNew = True
            ctlAree.LoadControl()
            
            'gestione community/service
            oserviceColl = getService(communityValue.KeyCommunity)
            If Not (oserviceColl Is Nothing) Then
                lbService.Items.Clear()
                For Each serviceValue As ServiceValue In oserviceColl
                    Select Case serviceValue.Key
                        Case Bacheca
                            lbService.Items.Add(New ListItem("Bacheca", Bacheca))
                        Case Newsgroup
                            lbService.Items.Add(New ListItem("Newsgroup", Newsgroup))
                        Case Message
                            lbService.Items.Add(New ListItem("Message", Message))
                        Case Link
                            lbService.Items.Add(New ListItem("Link", Link))
                        Case Material
                            lbService.Items.Add(New ListItem("Material", Material))
                        Case Forum
                            lbService.Items.Add(New ListItem("Forum", Forum))
                    End Select
                 
                Next
            Else
                lbService.Items.Clear()
            End If
           
            If communityValue.DateInsert.HasValue Then
                insertDate.Value = communityValue.DateInsert.Value
            Else
                insertDate.Clear()
            End If
            
            If communityValue.DatePublish.HasValue Then
                publishDate.Value = communityValue.DatePublish.Value
            Else
                publishDate.Clear()
            End If
            
            If (communityValue.Active) Then
                chkActive.Checked = True
            Else
                chkActive.Checked = False
            End If
            
            If (communityValue.Display) Then
                chkDisplay.Checked = True
            Else
                chkDisplay.Checked = False
            End If
            
            users.Text = getCommunityUsers(communityValue)
        Else
            communityName.Text = Nothing
            'communityLaunch.Value = Nothing
            communityLaunch.Content = Nothing
            'notificationMessageTitle.Value = Nothing
            notificationMessageTitle.Content = Nothing
            'notificationMessageBody.Value = Nothing
            notificationMessageBody.Content = Nothing
            
            chkActive.Checked = True
            chkDisplay.Checked = True
            dwlType.SelectedValue = 1
            lbService.Items.Clear()
            ctlAree.LoadControl()
        End If
    End Sub
        
    'Aggiunge un servizio alla lista swi servizi della community
    Sub AddService(ByVal sender As Object, ByVal e As EventArgs)
        Dim serviceValue As Integer
        Dim serviceText As String
               
        If (dwlService.SelectedIndex <> 0) Then
            serviceValue = CType(dwlService.SelectedItem.Value(), Integer)
            serviceText = dwlService.SelectedItem.Text
                                
            lbService.Items.Add(New ListItem(serviceText, serviceValue))
        Else
            strJS = "alert('Select a Service')"
                  
        End If
    End Sub
    
    'Elimina un servizio dalla lista dei servizi della community
    Sub DeleteService(ByVal sender As Object, ByVal e As EventArgs)
        
        If Not (lbService.SelectedItem Is Nothing) Then
            lbService.Items.RemoveAt(lbService.SelectedIndex)
        Else
            strJS = "alert('Select a Service to remove ')"
        End If
    End Sub
    
    'restituisce tutti i servizi associati ad una community
    Function getService(ByVal community As CommunityIdentificator) As ServiceCollection
        Return communityManager.ReadServices(community)
    End Function
       
    
    'crea le associazioni community/service
    Function SaveCommunityServiceRelation(ByVal community As CommunityIdentificator, ByVal languageId As Integer) As Boolean
        Dim serviceId As Integer = 0
     
        Try
            Dim serviceCollection As Object = lbService.Items
           
            communityManager.RemoveAllService(New CommunityIdentificator(community.Key, languageId))
            If Not (serviceCollection Is Nothing) Then
                If (serviceCollection.Count > 0) Then
                                   
                    For Each serviceItem As ListItem In serviceCollection
                        Select Case serviceItem.Value
                            Case Bacheca
                                serviceId = Bacheca
                            
                            Case Newsgroup
                                serviceId = Newsgroup
                            
                            Case Message
                                serviceId = Message
                            
                            Case Link
                                serviceId = Link
                                
                            Case Material
                                serviceId = Material
                            
                            Case Forum
                                serviceId = Forum
                                
                        End Select
                        communityManager.AddService(community, serviceId)
                    Next
                                        
                End If
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
        
    'salva le associazioni community/sitearea
    Function SaveCommunitySiteArea(ByVal communityId As Integer, ByVal languageId As Integer) As Boolean
        'gestione delle aree
        Dim res As Boolean = communityManager.RemoveAllCommunitySiteArea(New CommunityIdentificator(communityId, languageId))
       
        If Not ctlAree.GetSelection Is Nothing AndAlso ctlAree.GetSelection.Count > 0 Then
            Dim AreaColl As New Object
            Dim siteArea As New SiteAreaValue
            AreaColl = ctlAree.GetSelection
           
            For Each siteArea In AreaColl
                communityManager.CreateCommunitySiteArea(New CommunityIdentificator(communityId, languageId), New SiteAreaIdentificator(siteArea.Key.Id))
            Next
        End If
    End Function
    
    'recupera tutti le sitearea associate ad una Community
    Function GetCommunitySiteAreaValue(ByVal id As Int32) As SiteAreaCollection
        Dim siteAreaColl As New SiteAreaCollection
        If id = 0 Then Return Nothing
        Dim key As New CommunityIdentificator
        key.Id = id
       
        Return communityManager.ReadCommunitySiteArea(key)
    End Function
    
    'restituisce il numero di utenti associati alla community
    Function getCommunityUsers(ByVal communityValue As CommunityValue) As Integer
        Dim userColl As UserCollection = communityManager.ReadUsers(New CommunityIdentificator(communityValue.Key.Id, communityValue.Key.IdLanguage))
       
        If Not (userColl Is Nothing) Then
            Return userColl.Count()
        End If
        
        Return 0
    End Function
    
    'setta l'url in modo da rimandare alla creazione di relazioni User/Community  
    Sub GotoUserRelation(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        
    End Sub
    
    'Sub BindWithSearch(ByVal objGrid As GridView)
    '    'Dim oSiteValue As SiteValue
    '    Dim userSearcher As New UserSearcher
        
    '    If txtId.Text <> "" Then userSearcher.Key = New UserIdentificator(txtId.Text)
        
    '    If txtSurname.Text <> "" Then userSearcher.Surname = txtSurname.Text
        
    '    If txtName.Text <> "" Then userSearcher.Name = txtName.Text
        
    '    If (drpUserType.SelectedValue <> 0) Then
    '        If (drpUserType.SelectedItem.Text = "User") Then
    '            userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.User
    '        ElseIf (drpUserType.SelectedItem.Text = "HCP") Then
    '            userSearcher.UserType = Healthware.HP3.Core.User.ObjectValues.UserSearcher.UsersType.HCP
    '        End If
    '    End If
       
    '    'oSiteValue = GetSiteSelection(Site)
    '    'If Not oSiteValue Is Nothing Then
    '    '    userSearcher.RegistrationSite.Id = oSiteValue.Key.Id
    '    'End If
               
    '    BindGrid(objGrid, userSearcher)
    'End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserSearcher = Nothing)
        Dim userManager As New UserManager
        Dim userCollection As New UserCollection
              
        If Searcher Is Nothing Then
            Searcher = New UserSearcher
        End If
        
        userManager.Cache = False
        userCollection = userManager.Read(Searcher)
        If Not userCollection Is Nothing Then
            'userCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = userCollection
        objGrid.DataBind()
                
    End Sub
  
    'controlla che i campi non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (lLanguage.Language = -1) Then strName = "Language"
        If (communityName.Text = String.Empty) Then strName = "Name"
        'If (communityLaunch.Value = String.Empty) Then strName = "Launch"
        If (communityLaunch.Content = String.Empty) Then strName = "Launch"
        If Not (insertDate.Value.HasValue) Then strName = "Insert Date"
        If Not (publishDate.Value.HasValue) Then strName = "Publich Date"
 
        'ctlAree
        'If ((communityName.Text <> String.Empty) And (communityLaunch.Value <> String.Empty) And (insertDate.Value.HasValue) And (publishDate.Value.HasValue) And (lLanguage.Language <> -1)) Then
        If ((communityName.Text <> String.Empty) And (communityLaunch.Content <> String.Empty) And (insertDate.Value.HasValue) And (publishDate.Value.HasValue) And (lLanguage.Language <> -1)) Then
            _return = True
        End If
        
        Return _return
    End Function
           
    'restituisce lo status dell'utente
    Function getCommunityType(ByVal communityValue As CommunityValue) As String
        Dim str_out As String
        
        If (communityValue.Type = Healthware.HP3.Core.Community.ObjectValues.CommunityValue.CommunityType.PrivateCommunity) Then 'se � stato selezionato un sito 
            str_out = "<img id=""imgCommunityType"" src=""/HP3Office/HP3Image/Ico/padlock_close.gif"" title=""Private"" alt=""""/>"
        Else
            str_out = "<img id=""imgCommunityType"" src=""/HP3Office/HP3Image/Ico/padlock_open.gif"" title=""Public"" alt=""""/>"
        End If
        
        Return str_out
    End Function
    
    ''restituisce lo status dell'utente
    'Function IsForum(ByVal communityValue As CommunityValue) As String
    '    Dim serviceColl As ServiceCollection
        
    '    serviceColl = getService(communityValue.KeyCommunity)
    '    If Not (serviceColl Is Nothing) Then
    '        For Each service As ServiceValue In serviceColl
    '            If (service.Key = Forum) Then
    '                Return "<img id=""imgForum"" src=""/HP3Office/HP3Image/Ico/forum.gif""  title=""Forum"" alt=""""""/>"
    '            End If
    '        Next
    '    End If
        
    '    Return ""
    'End Function
    
    Function AllService(ByVal communityValue As CommunityValue) As String
        Dim serviceColl As ServiceCollection
        Dim str_out As String = ""
        
        serviceColl = getService(communityValue.KeyCommunity)
        If Not (serviceColl Is Nothing) Then
            For Each service As ServiceValue In serviceColl
                str_out = str_out & "<img id=""" & service.Label & """ src=""/HP3Office/HP3Image/Ico/" & service.Label & ".gif"" title=""" & service.Label & """ & "" hspace=""2"" />" '"<img id=""" & service.Label & """ src=""/HP3Office/HP3Image/Ico/Forum.gif title=""" & service.Label & """ alt=""""""/>"
            Next
            Return str_out
        End If
        
        Return ""
    End Function
        
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
        
             
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
     
    ''' <summary>
    ''' Ordinemento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridListUsers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gdw_communityList.PageIndex = 0
        
        'Select Case e.SortExpression
        '    Case "Key.Id"
        '        If SortType <> UserGenericComparer.SortType.ByKey Then
        '            SortType = UserGenericComparer.SortType.ByKey
        '        Else
        '            SortOrder = SortOrder * -1
        '        End If
        '    Case "Surname"
        '        If SortType <> UserGenericComparer.SortType.BySurName Then
        '            SortType = UserGenericComparer.SortType.BySurName
        '        Else
        '            SortOrder = SortOrder * -1
        '        End If
        'End Select
                
        'BindWithSearch(sender)
    End Sub
        
    Protected Sub gridListUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        ' BindWithSearch(sender)
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:Panel id="pnlGrid" runat="server" visible="false"> 
    <table class="form">
         <tr id="rowToolbar" runat="server">
            <td  colspan="4">
                 <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
            </td>
        </tr>
    </table>
<hr />
  
 <asp:Label CssClass="title" runat="server" id="sectionTit">Community List</asp:Label>
 <asp:gridview id="gdw_communityList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                OnPageIndexChanging="gridListUsers_PageIndexChanging"
                PageSize ="20"
                OnSorting="gridListUsers_Sorting"
                emptydatatext="No item available">
                                
              <Columns >
                <asp:TemplateField HeaderStyle-Width="3%" HeaderText="Id" SortExpression="KeyCommunity.Key">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "KeyCommunity.Key")%> </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="3%" HeaderText="Type">
                        <ItemTemplate><%#getCommunityType(Container.DataItem)%><%--<img id="imgCommunityType" src='<%#"/HP3Office/HP3Image/Ico/" & getCommunityType(Container.DataItem) & ".gif"%>'  title="Community Type" alt=""/>--%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Name">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%> </ItemTemplate>
                </asp:TemplateField> 
                 <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Service">
                        <ItemTemplate><%#AllService(Container.DataItem)%></ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderStyle-Width="7%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "|" & DataBinder.Eval(Container.DataItem, "Key.IdLanguage")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "|" & DataBinder.Eval(Container.DataItem, "Key.IdLanguage")%>'/>
                        <%--<asp:ImageButton ID="btn_content_user" ToolTip ="Content User" runat="server" ImageUrl="~/HP3Office/HP3Image/ico/content_user.gif" onClick="GotoUserRelation" CommandArgument ='<%#"ContentUsr," & Container.Dataitem.Key.Id.toString() & "," & Container.Dataitem.Key.IdLanguage %>' />--%>
                   </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button runat="server" ID="btnSave" onclick="UpdateRow" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>    
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    
    <table class="form" style="width:99%" >
        <tr><td style="width:25%" valign="top"></td><td></td></tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunitylanguage&Help=cms_HelpCommunitylanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunitylanguage", "Language")%></td>
            <td><HP3:contentLanguage ID ="lLanguage" runat="server" Typecontrol="AlternativeControl" /></td>
        </tr>
        
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateInsert&Help=cms_HelpDateInsert&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateInsert", "Date Insert")%></td>
            <td><HP3:Date runat="server" id="insertDate"  TypeControl="JsCalendar"/></td>
        </tr>
        
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDatePublish&Help=cms_HelpDatePublish&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDatePublish", "Date Publish")%></td>
            <td><HP3:Date runat="server" id="publishDate"  TypeControl="JsCalendar"/></td>
        </tr>
        
        <%--da decidere se aggiungere--%>
        <%--<tr>
            <td>Site:</td>
            <td><HP3:ctlSite runat="server" ID="ctlSite" TypeControl="ContentsConnection" SiteType="Site" /></td>
        </tr>--%>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="ctlAree" TypeControl="GenericRelation" SiteType="area"/></td>
        </tr>
        
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpCommunityName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text runat ="server" id="communityName" TypeControl ="TextBox" style="width:600px" MaxLength="400"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLaunch&Help=cms_HelpLaunch&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLaunch", "Launch")%></td>
            <td>
<%--                <FCKeditorV2:FCKeditor id="communityLaunch" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                <telerik:RadEditor ID="communityLaunch" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 
                                 
                                 <Links>
                                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                                   </telerik:EditorLink>
                                 </Links>
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
            </td>
            <%--<HP3:Text runat="server" ID="communityLaunch" TypeControl="textArea" Style = "width:300px"/></td>--%>
        </tr>
               
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpCommunityType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%></td>
            <td>
                <asp:DropDownList id="dwlType" runat="server">
                    <asp:ListItem Text="Private" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Public"  Value="1"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunityService&Help=cms_HelpCommunityService&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunityService", "Service")%></td>
            <td><asp:ListBox id="lbService"   runat="server" width="200px" ></asp:ListBox> <asp:button ID="btDelete" CssClass="button" runat="server" Text="Delete" Style ="width:100px"  OnClick="DeleteService" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="4">
                 <asp:dropdownlist id="dwlService" runat="server">
                    <asp:ListItem  Selected="True" Text="--Select--" Value="0" />
                    <asp:ListItem Text="Bacheca" Value="1" />
                    <asp:ListItem Text="Newsgroup" Value="2" />
                    <asp:ListItem Text="Message" Value="3" />
                    <asp:ListItem Text="Link" Value="4" />
                    <asp:ListItem Text="Material" Value="5"/>
                    <asp:ListItem Text="Forum" Value="6" />
                 </asp:dropdownlist>&nbsp;
                 <asp:button ID="btAdd" CssClass="button" runat="server" Text="Add Service" Style ="width:100px" onclick="AddService" />
             </td>
        </tr>
        
         <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunityNUsers&Help=cms_HelpCommunityNUsers&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunityNUsers", "N� Users")%></td>
            <td><HP3:Text id="users"  runat="server" TypeControl="TextBox" Style ="width:50px" isReadOnly="true"/></td>
         </tr>
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunityNMTitle&Help=cms_HelpCommunityNMTitle&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunityNMTitle", "Notification Message - Title")%></td>
            <td>
<%--                <FCKeditorV2:FCKeditor id="notificationMessageTitle" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                <telerik:RadEditor ID="notificationMessageTitle" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                     <Content></Content>
                     <Snippets>
                         <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                     </Snippets>
                     <Languages>
                        <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                        <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                        <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                        <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                     </Languages>

                     <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     
                     
                     <Links>
                        <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                        <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                       </telerik:EditorLink>
                     </Links>
                     <Links>
                       <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                     </Links>        
                                         
                </telerik:RadEditor>
            </td>
            <%--<HP3:Text runat="server" ID="notificationMessageTitle" TypeControl="textArea" Style = "width:300px" MaxLength="50"/>--%>
        </tr>
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCommunityNMBody&Help=cms_HelpCommunityNMBody&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCommunityNMBody", "Notification Message - Body")%></td>
            <td>
<%--                <FCKeditorV2:FCKeditor id="notificationMessageBody" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                <telerik:RadEditor ID="notificationMessageBody" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                     <Content></Content>
                     <Snippets>
                         <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                     </Snippets>
                     <Languages>
                        <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                        <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                        <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                        <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                     </Languages>

                     <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                     
                     
                     <Links>
                        <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                        <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                       </telerik:EditorLink>
                     </Links>
                     <Links>
                       <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                     </Links>        
                                         
                </telerik:RadEditor>
            </td>
            <%--<HP3:Text runat="server" ID="notificationMessageBody" TypeControl="textArea" Style = "width:300px"/></td>--%>
        </tr>
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
            <td><asp:checkbox ID="chkActive" runat="server" /></td>
        </tr>
        
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDisplay&Help=cms_HelpDisplay &Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDisplay", "Display")%></td>
            <td><asp:checkbox ID="chkDisplay" runat="server" /></td>
        </tr>
    </table>
</asp:Panel>

 
    

   
        
   




