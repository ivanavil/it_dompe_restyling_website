<%@ Control Language="VB" ClassName="SiteManagerControll" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.web"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx"%>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>

<script runat="server">
     
    Private mPageManager As New MasterPageManager()
    Private siteAreaManager As SiteAreaManager
    Private dictionaryManager As New DictionaryManager
    Private _strDomain As String
    Private _language As String
    Private oGroupValue As GroupValue
    Private strJS As String
    Private strName As String
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = mPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Page.IsPostBack) Then
            ActivePanelGrid()
            LoadArchive()
      
        End If
    End Sub
   
    'recupera la lista dei gruppi 
    Sub LoadArchive()
        
        Dim oGroupSearcher As New GroupSearcher
        Dim oGroupCollection As New GroupSiteCollection
       
        siteAreaManager = New SiteAreaManager
        
        siteAreaManager.Cache = False
        oGroupCollection = siteAreaManager.ReadGroup(oGroupSearcher)
               
       
            gdw_groupList.DataSource = oGroupCollection
            gdw_groupList.DataBind()
       
    End Sub
    
    ' modifica un singolo record
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim oGroupValue As New GroupValue
        Dim domainId As Integer
        
        siteAreaManager = New SiteAreaManager
        
        If sender.CommandName = "SelectItem" Then
            domainId = sender.CommandArgument
            SelectedId = domainId
            
            siteAreaManager.Cache = False
            oGroupValue = siteAreaManager.ReadGroup(New GroupIdentificator(domainId))
       
            If Not oGroupValue Is Nothing Then
                LoadDett(oGroupValue)
                ActivePanelDett()
            End If
                       
        End If
    End Sub
    
    ' cancella un singolo record
    Sub RemoveRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim oGroupValue As New GroupValue
        Dim domainId As Integer
        Dim risRemove As Boolean
        
        siteAreaManager = New SiteAreaManager
        
        If sender.CommandName = "DeleteItem" Then
            domainId = sender.CommandArgument
            SelectedId = domainId
            risRemove = siteAreaManager.RemoveGroup(New GroupIdentificator(domainId))
          
            LoadArchive()
            'Gestione dei valori di ritorno
            If Not risRemove Then
                CreateJsMessage("Impossible to delete this Group because already associated to other sitearea")
            End If
                       
        End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim oGroupValue As New GroupValue
        siteAreaManager = New SiteAreaManager
       
        If (IsNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            With (oGroupValue)
                .GroupName = groupName.Text
                .GroupDescription = groupDescription.Text
                .GroupTarget = groupTarget.Text
            End With
         
            If SelectedId = 0 Then
                siteAreaManager.CreateGroup(oGroupValue)
                LoadArchive()
                ActivePanelGrid()
            Else
                oGroupValue.Key.Id = SelectedId
                siteAreaManager.UpdateGroup(oGroupValue)
                LoadArchive()
                ActivePanelGrid()
            End If
        Else
            'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
    End Sub
    
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
       
    End Sub
    
    'popola i campi di testo con le informazioni contenute nel GroupValue
    Sub LoadDett(ByVal oGroupValue As GroupValue)
        If Not oGroupValue Is Nothing Then
            groupName.Text = oGroupValue.GroupName
            groupDescription.Text = oGroupValue.GroupDescription
            groupTarget.Text = oGroupValue.GroupTarget
                                        
        Else
            groupName.Text = Nothing
            groupDescription.Text = Nothing
            groupTarget.Text = Nothing
               
        End If
    End Sub
    
    'controlla che i campi di testo non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (groupName.Text = String.Empty) Then strName = "Group Name"
               
        If (groupDescription.Text = String.Empty) Then strName = "Group Description"
        
        If (groupTarget.Text = String.Empty) Then strName = "Group Target"
        
        If ((groupName.Text <> String.Empty) And (groupDescription.Text <> String.Empty) And (groupTarget.Text <> String.Empty)) Then
            _return = True
        End If
        
        Return _return
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, mPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:Panel id="pnlGrid" runat="server" visible="false"> 
<div style="margin-bottom:10px"><asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></div>
<asp:Label CssClass="title" runat="server" id="sectionTit">Group List</asp:Label>
<asp:gridview id="gdw_groupList" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No item available">
              <Columns >
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="ID">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Group Name">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "GroupName")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Group Description">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "GroupDescription")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%">
                    <ItemTemplate>
                       <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                       <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClick="RemoveRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <table  style=" margin-top:10px" class="form" width="99%">
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGroupName&Help=cms_HelpGroupName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGroupName", "Name")%></td>
        <td><HP3:Text runat ="server" id="groupName" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGroupDescription&Help=cms_HelpGroupDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGroupDescription", "Description")%></td>
        <td><HP3:Text runat ="server" id="groupDescription" TypeControl ="TextBox" style="width:200px" MaxLength="50"/></td>
    </tr>
    <tr>
       <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGroupTarget&Help=cms_HelpGroupTarget&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGroupTarget", "Target")%></td>
        <td><HP3:Text runat ="server" id="groupTarget" TypeControl ="TextBox" style="width:30px" MaxLength="4"/></td>
    </tr>
 </table>
</asp:Panel>

