<%@ Control Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagPrefix="UC" Namespace="RadioButtonGroupName" Assembly="RadioButtonGroupName" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<script runat="server">
    Private utility As New Core.Utility.WebUtility
    Private QuestManager As New QuestionnaireManager
    
    Private _strDomain As String
    Private _language As String
    Private strJS As String
    Private _isNew As Boolean = False
   
    Public Property QuestId() As Int32
        Get
            Return ViewState("questId")
        End Get
        Set(ByVal value As Int32)
            ViewState("questId") = value
        End Set
    End Property
    
    Public Property QuestionId() As Int32
        Get
            Return ViewState("questionId")
        End Get
        Set(ByVal value As Int32)
            ViewState("questionId") = value
        End Set
    End Property
    
    Public Property AnswerId() As Int32
        Get
            Return ViewState("answerId")
        End Get
        Set(ByVal value As Int32)
            ViewState("answerId") = value
        End Set
    End Property
    
    Public Property isNew() As Boolean
        Get
            Return ViewState("_isNew")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_isNew") = value
        End Set
    End Property
    
    Public Property isQuestion() As Boolean
        Get
            Return ViewState("_isQuestion")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_isQuestion") = value
        End Set
    End Property
          
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not (Me.Page.IsPostBack) Then
            LoadQuest()
            ActiveQuestGrid()
        End If
    End Sub
   
    ''recupera la lista dei siti
    Sub LoadQuest()
        Dim QuestSearcher As New QuestionnaireSearcher
        Dim QuestColl As New QuestionnaireCollection
        Dim so As New QuestionnaireSearcher
        
        QuestManager.Cache = False
        so.KeyLanguage.Id = lSelector.Language
       
        Dim site As Integer = 0
        site = Request("S")
        
        If (site > 0) Then
            QuestColl = QuestManager.ReadSiteRelation(so, New SiteAreaIdentificator(site))
        End If
       
        gdw_Questionnaire.DataSource = QuestColl
        gdw_Questionnaire.DataBind()
    End Sub
    
    'inserisce una nuovo questionario
    Sub NewQuest(ByVal sender As Object, ByVal e As EventArgs)
        'btnUpdate.Text = "Save"
        
        If (Request("S") <> Nothing) Then
            Dim site As New SiteAreaValue
            Dim coll As New SiteAreaCollection
            site = Me.BusinessSiteAreaManager.Read(Request("S"))
            coll.Add(site)
            
            oSite.GenericCollection = coll
        End If
     
        
        QuestId = 0
        questDescription.Text = Nothing
        'questFullText.Value = Nothing
        questFullText.Content = Nothing
        questMinGrade.Text = Nothing
        questOrder.Text = Nothing
        questStatus.Checked = True
        
        'sito
        oSite.IsNew = True
        oSite.LoadControl()
        
        'sitearea
        Aree.IsNew = True
        Aree.LoadControl()
        
        'setta la lingua per un nuovo content
        lSelector.IsNew = True
        lSelector.LoadDefaultValue()
                
        
        ActiveQuestDett()
        ActiveNewQuestionButt()
    End Sub
    
    Sub SaveQuest(ByVal sender As Object, ByVal e As EventArgs)
        QuestId = 0
        SaveQuest()
        ActiveQuestGrid()
        LoadQuest()
    End Sub
    
    'modifica le property legate al questionario
    Sub SaveQuest()
        Dim QuestValue As New QuestionnaireValue
        
        If (QuestId = 0) Then
            QuestValue.KeyLanguage.Id = lSelector.Language
            QuestValue.Description = questDescription.Text
            'QuestValue.FullText = questFullText.Value
            QuestValue.FullText = questFullText.Content
            
            If (questMinGrade.Text = String.Empty) Then
                QuestValue.MinGrade = 0
            Else
                QuestValue.MinGrade = questMinGrade.Text
            End If
            
            QuestValue.DateInsert = DateInsert.Value
            QuestValue.DateStart = DateStart.Value
            QuestValue.DateEnd = DateEnd.Value
            
            If (questStatus.Checked) Then
                QuestValue.Status = QuestionnaireValue.ActivationStatus.Active
            Else
                QuestValue.Status = QuestionnaireValue.ActivationStatus.NotActive
            End If
            
            If (questOrder.Text <> String.Empty) Then
                QuestValue.Order = questOrder.Text
            End If
            
            QuestValue = QuestManager.Create(QuestValue)
                        
        Else
            QuestValue.Key.Id = QuestId
            QuestValue.KeyLanguage.Id = lSelector.Language
            QuestValue.Description = questDescription.Text
            'QuestValue.FullText = questFullText.Value
            QuestValue.FullText = questFullText.Content
            
            If (questMinGrade.Text = String.Empty) Then
                QuestValue.MinGrade = 0
            Else
                QuestValue.MinGrade = questMinGrade.Text
            End If
                
            QuestValue.DateInsert = DateInsert.Value
            QuestValue.DateStart = DateStart.Value
            QuestValue.DateEnd = DateEnd.Value
            
            If (questStatus.Checked) Then
                QuestValue.Status = QuestionnaireValue.ActivationStatus.Active
            Else
                QuestValue.Status = QuestionnaireValue.ActivationStatus.NotActive
            End If
            
            If (questOrder.Text <> String.Empty) Then
                QuestValue.Order = questOrder.Text
            End If
            
            QuestValue = QuestManager.Update(QuestValue)
        End If
        'gestione sites
        'SiteRelationManagement(QuestValue)
        
        'gestione siteareas
        SiteAreaRelationManagement(QuestValue)
    End Sub
    
    'Sub SiteRelationManagement(ByVal vo As QuestionnaireValue)
    '    If Not (vo Is Nothing) Then
    '        Response.Write("PROVA1")
    '        Dim bool As Boolean = QuestManager.RemoveSiteRelation(vo.Key)
    '        If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
    '            Dim siteColl As New Object
    '            Dim siteArea As New SiteAreaValue
    '            siteColl = oSite.GetSelection

    '            For Each siteArea In siteColl
    '                Response.Write("PROVA2")
    '                Dim B As Boolean = QuestManager.CreateSiteRelation(vo.Key, siteArea.Key)
    '                Response.Write("PROVA3" & B)
    '            Next
    '        End If
    '    End If
    'End Sub
    
    Sub SiteAreaRelationManagement(ByVal vo As QuestionnaireValue)
        If Not (vo Is Nothing) Then

            Dim bool As Boolean = QuestManager.RemoveSiteRelation(vo.Key)
            
            If Not Aree.GetSelection Is Nothing AndAlso Aree.GetSelection.Count > 0 Then
                Dim siteColl As New Object
                Dim siteArea As New SiteAreaValue
                siteColl = Aree.GetSelection

                For Each siteArea In siteColl
                    QuestManager.CreateSiteRelation(vo.Key, siteArea.Key)
                Next
            End If
            
            If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
                Dim siteColl As New Object
                Dim siteArea As New SiteAreaValue
                siteColl = oSite.GetSelection
           
                For Each siteArea In siteColl
                   
                    Dim B As Boolean = QuestManager.CreateSiteRelation(vo.Key, siteArea.Key)
                    
                Next
            End If
        End If
    End Sub
    
    'cancellazione logica di un questionario
    Sub DeleteQuest(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If sender.CommandName = "DeleteQuest" Then
            Dim questId As Integer = sender.CommandArgument
            QuestManager.Delete(New QuestionnaireIdentificator(questId))
        End If
        
        LoadQuest()
    End Sub
    
    'modifica dei campi associati al questionario
    Sub EditQuest(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If sender.CommandName = "SelectQuest" Then
            Dim qId As Integer = sender.CommandArgument
            QuestId = qId
            
            QuestManager.Cache = False
            Dim QuestValue As QuestionnaireValue = QuestManager.Read(New QuestionnaireIdentificator(QuestId))
       
            If Not QuestValue Is Nothing Then
                'gestione siti
                oSite.GenericCollection = GetSite(QuestValue.Key.Id)
                oSite.IsNew = False
                oSite.LoadControl()
                
                'gestione siti
                Aree.GenericCollection = GetSiteArea(QuestValue.Key.Id)
                Aree.IsNew = False
                Aree.LoadControl()
                
                lSelector.LoadLanguageValue(QuestValue.KeyLanguage.Id)
                
                questDescription.Text = QuestValue.Description
                'questFullText.Value = QuestValue.FullText
                questFullText.Content = QuestValue.FullText
                questMinGrade.Text = QuestValue.MinGrade
                questOrder.Text = QuestValue.Order
                
                If (QuestValue.Status = QuestionnaireValue.ActivationStatus.Active) Then
                    questStatus.Checked = True
                Else
                    questStatus.Checked = False
                End If
                
                If QuestValue.DateInsert.HasValue Then
                    DateInsert.Value = QuestValue.DateInsert.Value
                Else
                    DateInsert.Clear()
                End If
                
                If QuestValue.DateStart.HasValue Then
                    DateStart.Value = QuestValue.DateStart.Value
                Else
                    DateStart.Clear()
                End If
                
                If QuestValue.DateEnd.HasValue Then
                    DateEnd.Value = QuestValue.DateEnd.Value
                Else
                    DateEnd.Clear()
                End If
                
                ActiveQuestDett()
            End If
        End If
    End Sub
    
    Sub ContentRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "ContentRel") Then
            Dim qId As Integer = sender.CommandArgument
                
            Dim webRequest As New WebRequestValue
            Dim objQs As New ObjQueryString
            webRequest.KeycontentType = New ContentTypeIdentificator("CMS", "ContQuest")
            webRequest.customParam.append("Q", qId)
            webRequest.customParam.append(objQs)
            Response.Redirect(Me.BusinessMasterPageManager.FormatRequest(webRequest))
        End If
    End Sub
    
    Sub LoadQuestArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActiveQuestGrid()
        LoadQuest()
    End Sub
    
    'gestisce il bind delle domande associate ad un questionario
    Sub LoadQuestionList()
        Dim QManager As New QuestionnaireManager
        
        QManager.Cache = False
        Dim QuestionColl As QuestionCollection = QManager.ReadQuestions(New QuestionnaireIdentificator(QuestId()))
        
        grw_questionList.DataSource = QuestionColl
        grw_questionList.DataBind()
        pnlAnswerList.Visible = False
        pnlQuestionList.Visible = True
    End Sub
    
    'gestisce il bind delle risposte associate alle domande di un questionario
    Sub LoadAnswerList()
        Dim QManager As New QuestionManager
        
        QManager.Cache = False
        Dim AnsColl As AnswerCollection = QManager.ReadAnswers(New QuestionIdentificator(QuestionId()))
        
        grw_answerList.DataSource = AnsColl
        grw_answerList.DataBind()
        
        pnlAnswerList.Visible = True
        pnlQuestionList.Visible = False
    End Sub
    
    'gestisce il salvataggio del questionario
    Sub SaveQuestionnaire(ByVal sender As Object, ByVal e As EventArgs)
        If (sender.commandName = "Step1") Then
            SaveQuest()
            BindQuestions()
            ActiveQeAPanel()
        End If
        
        If (sender.CommandName = "Step2") Then
            
        End If
    End Sub
   
    'recupera tutte le domande relazionate ad un questionario
    Sub BindQuestions()
        Dim qId As Integer = QuestId
        Dim questionColl As QuestionCollection
        
        If (QuestId <> 0) Then
            QuestManager.Cache = False
            questionColl = QuestManager.ReadQuestions(New QuestionnaireIdentificator(qId))
            
            rpQuestion.DataSource = questionColl
            rpQuestion.DataBind()
            If Not (questionColl Is Nothing) AndAlso (questionColl.Count > 1) Then
                rpQuestion.Visible = True
            Else
                noItems.Text = "There aren't questions related to the questionnaire!"
            End If
            
        End If
    End Sub
    
    'usato nel binding del repeater di livello 1
    Function GetAnswers(ByVal question As QuestionValue) As AnswerCollection
        Dim QuestManager As New QuestionManager
        Dim answers As AnswerCollection
             
        QuestManager.Cache = False
        answers = QuestManager.ReadAnswers(question.Key)
        Return answers
    End Function
    
    '---------------------------Gestione Questions/Answers---------------------------------------------
    'gestisce l'edit dei campi associati ad un item del questionario (domanda o risposta)
    Sub EditItemQuest(ByVal sender As Object, ByVal e As EventArgs)
        Dim QuestionManager As New QuestionManager
        QuestionManager.Cache = False
        
        'itero le doamande
        For Each question As RepeaterItem In rpQuestion.Items
            Dim questionRItem As GroupRadioButton = question.FindControl("radioQuestionItem")
            
            If (questionRItem.Checked) Then
                Dim value As Integer = CType(questionRItem.Text, Integer)
                              
                Dim QuestionValue As QuestionValue = QuestionManager.Read(New QuestionIdentificator(value))
                isNew() = False
                typeAnswer.Enabled = False
                
                LoadQuestion(QuestionValue)
                ActiveQuestionPanel()
            End If
          
            'itero le risposte           
            Dim rpAnswers As Repeater = question.FindControl("rpAnswers")
            For Each answer As RepeaterItem In rpAnswers.Items
                
                Dim answerRItem As GroupRadioButton = answer.FindControl("radioAnswerItem")
                If (answerRItem.Checked) Then
                    Dim value As String = answerRItem.Text
                                      
                    Dim AnswerValue As AnswerValue = QuestionManager.Read(New AnswerIdentificator(value))
                    isNew() = False
                    LoadAnswer(AnswerValue)
                    ActiveAnswerPanel()
                End If
            Next
        Next
    End Sub
    
    'gestisce l'edit dei campi associati ad un item del questionario (domanda o risposta)
    Sub DeleteItemQuest(ByVal sender As Object, ByVal e As EventArgs)
        Dim QuestionManager As New QuestionManager
        
        'itero le domande
        For Each question As RepeaterItem In rpQuestion.Items
            Dim questionRItem As GroupRadioButton = question.FindControl("radioQuestionItem")
            
            If (questionRItem.Checked) Then
                Dim value As String = questionRItem.Text
                QuestionManager.Delete(New QuestionIdentificator(value))
            End If
          
            'itero le risposte           
            Dim rpAnswers As Repeater = question.FindControl("rpAnswers")
            For Each answer As RepeaterItem In rpAnswers.Items
                
                Dim answerRItem As GroupRadioButton = answer.FindControl("radioAnswerItem")
                If (answerRItem.Checked) Then
                                        
                    Dim value As String = answerRItem.Text
                    QuestionManager.Delete(New AnswerIdentificator(value))
                End If
            Next
        Next
        
        BindQuestions()
        ActiveQeAPanel()
    End Sub
    
    'gestisce la visualizzazione dei dati della domanda nel form
    Sub LoadQuestion(ByVal QValue As QuestionValue)
        If Not (QValue Is Nothing) Then
            QuestionId() = QValue.Key.Id
            questionDescription.Text = QValue.Description
            questionBond.Text = QValue.Bond
            typeAnswer.SelectedIndex = typeAnswer.Items.IndexOf(typeAnswer.Items.FindByValue(QValue.AnswerType))
            questionOrder.Text = QValue.Order
            
            Select Case QValue.Type
                Case QuestionValue.QuestionType.Compulsory
                    questionCompulsory.Checked = True
                Case 0
                    questionCompulsory.Checked = False
            End Select
        Else
            questionDescription.Text = Nothing
            questionBond.Text = Nothing
            typeAnswer.SelectedIndex = 0
            questionOrder.Text = Nothing
            questionCompulsory.Checked = False
        End If
    End Sub
    
    'gestisce la visualizzazione dei dati della risposta nel form
    Sub LoadAnswer(ByVal AValue As AnswerValue)
        If Not (AValue Is Nothing) Then
            AnswerId() = AValue.Key.Id
            QuestionId() = AValue.KeyQuestion.Id
            
            answerDescription.Text = AValue.Description
            answerOrder.Text = AValue.Order
            answerGrade.Text = AValue.Grade
            
            'questionOrder.Text = AValue.Order
            If (AValue.IsCorrect) Then
                answerCorrect.Checked = True
            Else
                answerCorrect.Checked = False
            End If
            
            dwlType.SelectedIndex = dwlType.Items.IndexOf(dwlType.Items.FindByValue(AValue.Type))
                   
        Else
            answerDescription.Text = Nothing
            answerOrder.Text = Nothing
            answerGrade.Text = Nothing
            'questionOrder.Text = Nothing
            answerCorrect.Checked = False
           
            dwlType.SelectedValue = AnswerValue.TypeAnswer.Standard
        End If
    End Sub
    
    'gestisce la creazione a la'update della domanda
    Sub SaveQuestion(ByVal sender As Object, ByVal e As EventArgs)
        Dim questionManager As New QuestionManager
        Dim QValue As New QuestionValue
                
        QValue.Description = questionDescription.Text
        If (questionBond.Text = String.Empty) Then
            QValue.Bond = 0
        Else
            QValue.Bond = CType(questionBond.Text, Integer)
        End If
       
        If (questionOrder.Text = String.Empty) Then
            QValue.Order = 0
        Else
            QValue.Order = CType(questionOrder.Text, Integer)
        End If
          
        If (questionCompulsory.Checked) Then
            QValue.Type = QuestionValue.QuestionType.Compulsory
        Else
            QValue.Type = 0
        End If
        
        Select Case typeAnswer.SelectedValue
            Case QuestionValue.Answer.SingleResponse
                QValue.AnswerType = QuestionValue.Answer.SingleResponse
                
            Case QuestionValue.Answer.MultipleChoice
                QValue.AnswerType = QuestionValue.Answer.MultipleChoice
                
            Case QuestionValue.Answer.OpenendedAnswer
                QValue.AnswerType = QuestionValue.Answer.OpenendedAnswer
        End Select
        
        'gestisce l'update e la creazione
        If (QuestionId = 0) Then
            questionManager.Create(QValue)
            
            Dim QQValue As New QuestionnaireQuestionValue
            QQValue.KeyQuestion.Id = QValue.Key.Id
            QQValue.KeyQuestionnaire.Id = QuestId()
            QuestManager.Create(QQValue)
        Else
            QValue.Key.Id = QuestionId
            questionManager.Update(QValue)
        End If
        
        If (isNew()) Then
            LoadQuestion(Nothing)
            QuestionId = 0
            LoadQuestionList()
        Else
           
            BindQuestions()
            ActiveQeAPanel()
        End If
    End Sub
    
    'gestisce la creazione a la'update della domanda
    Sub SaveAnswer(ByVal sender As Object, ByVal e As EventArgs)
        Dim questionManager As New QuestionManager
        Dim AValue As New AnswerValue
        
        AValue.Description = answerDescription.Text
        If (answerGrade.Text = String.Empty) Then
            AValue.Grade = 0
        Else
            AValue.Grade = answerGrade.Text
        End If
        
        If (answerOrder.Text = String.Empty) Then
            AValue.Order = 0
        Else
            AValue.Order = answerOrder.Text
        End If
                
        If (answerCorrect.Checked) Then
            AValue.IsCorrect = True
        Else
            AValue.IsCorrect = False
        End If
       
        AValue.Type = dwlType.SelectedValue
                
        'gestisce l'update e la creazione
        If (AnswerId = 0) Then
            If (CheckAnswer(AValue.IsCorrect)) Then
                AValue.KeyQuestion.Id = QuestionId()
                questionManager.Create(AValue)
            Else
                CreateJsMessage("A correct answer for this question has already been inserted. Please try again.")
                Exit Sub
            End If
        Else
            If (CheckAnswerId(AValue.IsCorrect, AnswerId)) Then
                AValue.Key.Id = AnswerId
                AValue.KeyQuestion.Id = QuestionId
                questionManager.Update(AValue)
            Else
                CreateJsMessage("A correct answer for this question has already been inserted. Please try again.")
                Exit Sub
            End If
        End If
       
        
        If (isNew()) Then
            LoadAnswer(Nothing)
            LoadAnswerList()
        Else
            BindQuestions()
            ActiveQeAPanel()
        End If
    End Sub
    
    'gestisce la creazione di una nuova domanda
    Sub AddQuestion(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddQuestion") Then
            Dim qId As Integer = sender.CommandArgument
            LoadQuestion(Nothing)
            ActiveQuestionPanel()
            
            QuestId = qId
            typeAnswer.Enabled = True
            isNew() = True
            isQuestion() = True
            QuestionId = 0
            
            saveQButt_Q.Text = "Save"
            backButt_Q.Text = "Go Back"
            LoadQuestionList()
        End If
    End Sub
    
    'gestisce la creazione di nuove risposte
    Sub AddAnswer(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddAnswer") Then
            Dim qId As Integer = sender.CommandArgument
            LoadAnswer(Nothing)
            ActiveAnswerPanel()
            
            QuestionId = qId
            AnswerId = 0
            isNew() = True
            isQuestion() = False
            
            backButt_A.Text = "Go Back"
            saveButton_A.Text = "Save"
            
            LoadAnswerList()
        End If
    End Sub
    
    'gestisce
    Sub GoBackToQuest(ByVal sender As Object, ByVal e As EventArgs)
        If (isNew() And isQuestion()) Then
            pnlQuestion.Visible = False
            ActiveQuestGrid()
            LoadQuest()
        ElseIf (isNew() And (Not isQuestion())) Then
            isQuestion() = True
            QuestionId = 0
            pnlAnswer.Visible = False
            ActiveQuestionPanel()
            LoadQuestionList()
        Else
            BindQuestions()
            ActiveQeAPanel()
        End If
    End Sub
    '----------------------------------End-----------------------------------------------------
   
    
    'Return True, se la domanda � del tipo MultipleChoise 
    'Se la domanda � del tipo Single Response 
    '                                       ->Return True, se esiste solo una risposta corretta
    '                                       ->Return False, altrimenti
    Function CheckAnswer(ByVal isCorrect As Boolean) As Boolean
        Dim QManager As New QuestionManager
        
        QManager.Cache = False
        Dim AnsColl As AnswerCollection = QManager.ReadAnswers(New QuestionIdentificator(QuestionId()))
        Dim questionValue As QuestionValue = QManager.Read(New QuestionIdentificator(QuestionId()))
        
        'se la domanda � del tipo a risposta singola
        If (questionValue.AnswerType = Quest.Quest.ObjectValues.QuestionValue.Answer.SingleResponse) Then
            'se � la prima risposta inserita
            If (AnsColl Is Nothing) Then Return True
            'se la risposta da inserire ha come valore 'isCorrect = True'
            If (isCorrect) Then
                For Each answer As AnswerValue In AnsColl
                    If (answer.IsCorrect) Then
                        Return False
                    End If
                Next
            End If
        End If
        
        Return True
    End Function
    
    
    Function CheckAnswerId(ByVal isCorrect As Boolean, ByVal answerId As Integer) As Boolean
        Dim QManager As New QuestionManager
        
        QManager.Cache = False
        Dim AnsColl As AnswerCollection = QManager.ReadAnswers(New QuestionIdentificator(QuestionId()))
        Dim questionValue As QuestionValue = QManager.Read(New QuestionIdentificator(QuestionId()))
        
        'se la domanda � del tipo a risposta singola
        If (questionValue.AnswerType = Quest.Quest.ObjectValues.QuestionValue.Answer.SingleResponse) Then
            'se � la prima risposta inserita
            'If (AnsColl Is Nothing) Then Return True
            'se la risposta da inserire ha come valore 'isCorrect = True'
            If (isCorrect) Then
                For Each answer As AnswerValue In AnsColl
                    If (answer.IsCorrect) And (answer.Key.Id = answerId) Then
                        Return True
                    End If
                    If (answer.IsCorrect) And Not (answer.Key.Id = answerId) Then
                        Return False
                    End If
                Next
            End If
        End If
        
        Return True
    End Function
    
    Function GetAnswerType(ByVal question As QuestionValue) As String
        Select Case question.AnswerType
            Case QuestionValue.Answer.SingleResponse
                Return "Single Response"
                
            Case QuestionValue.Answer.MultipleChoice
                Return "Multiple Choice"
                
            Case QuestionValue.Answer.OpenendedAnswer
                Return "Open-ended Answer"
        End Select
        Return ""
    End Function
    
    'recupera tutti i siti associate al questionario
    Function GetSite(ByVal id As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
                       
        Return QuestManager.ReadSiteRelation(New QuestionnaireIdentificator(id))
    End Function
    
    'recupera tutti le sitearea associate al questionario
    Function GetSiteArea(ByVal id As Integer) As SiteAreaCollection
        If id = 0 Then Return Nothing
                       
        Return QuestManager.ReadSiteAreaRelation(New QuestionnaireIdentificator(id))
    End Function
    
    
    Function IsCorrectAnswer(ByVal answer As AnswerValue) As String
        If (answer.IsCorrect) Then
            Dim imgCA As String = "<img id=""imgCorrect"" src=""/HP3Office/HP3Image/Ico/approved.gif"" title=""Correct Answer"" alt=""""/>"
            imgCA = imgCA + "[Grade:" & answer.Grade & "]"
            
            Return imgCA
        End If
        Return ""
    End Function
    
    'gestisce la visibilit� dell'immagine 
    Function isVisible(ByVal question As QuestionValue) As Boolean
        If (question.AnswerType = QuestionValue.Answer.OpenendedAnswer) Then
            Return False
        End If
        
        Return True
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveQuestGrid()
        pnlQuestGrid.Visible = True
        pnlQuestDett.Visible = False
        pnlQeA.Visible = False
        pnlAnswer.Visible = False
        pnlQuestionList.Visible = False
        pnlAnswerList.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveQuestDett()
        pnlQuestGrid.Visible = False
        pnlQeA.Visible = False
        pnlAnswer.Visible = False
        pnlQuestionList.Visible = False
        pnlAnswerList.Visible = False
        ButtStep1.Visible = True
        pnlQuestDett.Visible = True
        btnSave.Visible = False
    End Sub
       
    Sub ActiveQeAPanel()
        pnlQuestDett.Visible = False
        pnlQuestGrid.Visible = False
        pnlAnswer.Visible = False
        pnlQuestion.Visible = False
        pnlQuestionList.Visible = False
        pnlQeA.Visible = True
    End Sub
    
    Sub ActiveQuestionPanel()
        btnSave.Visible = False
        'btnAddQ.Visible = False
        ButtStep1.Visible = True
        pnlQuestDett.Visible = False
        pnlQuestGrid.Visible = False
        pnlQeA.Visible = False
        pnlAnswer.Visible = False
        pnlQuestionList.Visible = False
        pnlQuestion.Visible = True
    End Sub
    
    Sub ActiveAnswerPanel()
        pnlQuestDett.Visible = False
        pnlQuestGrid.Visible = False
        pnlQeA.Visible = False
        pnlQuestionList.Visible = False
        pnlQuestion.Visible = False
        pnlAnswer.Visible = True
    End Sub
    
    Sub ActiveNewQuestionButt()
        btnSave.Visible = True
        'btnAddQ.Visible = True
        ButtStep1.Visible = False
        'pnlQuestionList.Visible = True
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
   
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    Sub SearchQuest(ByVal sender As Object, ByVal e As EventArgs)
        Dim so As New QuestionnaireSearcher
        Dim QuestColl As New QuestionnaireCollection
        
        If (srcId.Text <> "") Then so.Key.Id = srcId.Text
        If (srcTitle.Text <> "") Then so.Description = srcTitle.Text
        If (srcDateStart.Value.HasValue) Then so.DateStart = srcDateStart.Value
        If (srcDateEnd.Value.HasValue) Then so.DateEnd = srcDateEnd.Value
        
        so.KeyLanguage.Id = srcLanguage.Language
       
        If (Request("S")) Then
            QuestManager.Cache = False
            QuestColl = QuestManager.ReadSiteRelation(so, New SiteAreaIdentificator(Request("S")))
        End If
       
        gdw_Questionnaire.DataSource = QuestColl
        gdw_Questionnaire.DataBind()
    End Sub
    
    Sub ChangeStatus(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "Change") Then
            Dim qId As String = sender.CommandArgument
            
            Dim _status As Integer
            QuestManager.Cache = False
            
            Dim quest As QuestionnaireValue = QuestManager.Read(New QuestionnaireIdentificator(CType(qId, Integer)))
           
            If Not (quest Is Nothing) Then
                Select Case quest.Status
                    Case QuestionnaireValue.ActivationStatus.Active
                        _status = QuestionnaireValue.ActivationStatus.NotActive
                    Case QuestionnaireValue.ActivationStatus.NotActive
                        _status = QuestionnaireValue.ActivationStatus.Active
                        
                End Select
             
                QuestManager.Update(New QuestionnaireIdentificator(CType(qId, Integer)), "Status", _status)
            End If
           
            LoadQuest()
        End If
    End Sub
    
    Protected Sub gridListQuest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Sub BindWithSearch(ByVal sender)
        Dim so As New QuestionnaireSearcher
        Dim QuestColl As New QuestionnaireCollection
        
        If (srcId.Text <> "") Then so.Key.Id = srcId.Text
        If (srcTitle.Text <> "") Then so.Description = srcTitle.Text
        If (srcDateStart.Value.HasValue) Then so.DateStart = srcDateStart.Value
        If (srcDateEnd.Value.HasValue) Then so.DateEnd = srcDateEnd.Value
        
        so.KeyLanguage.Id = srcLanguage.Language
       
        If (Request("S")) Then
            QuestManager.Cache = False
            QuestColl = QuestManager.ReadSiteRelation(so, New SiteAreaIdentificator(Request("S")))
        End If
       
        gdw_Questionnaire.DataSource = QuestColl
        gdw_Questionnaire.DataBind()
    End Sub
    
        
    Function GetVisibility(ByVal quest As QuestionnaireValue, ByVal type As Integer) As Boolean
        If (quest.Status = type) Then
            Return True
        End If
      
        Return False
    End Function
  </script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:Panel id="pnlQuestGrid" runat="server" visible="false"> 

 <div>
   <asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewQuest"  CssClass="button"  style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
     <table class="topContentSearcher">
         <tr>
              <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
              <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Questionnaire Searcher</span></td>
         </tr>
       </table>
       <fieldset class="_Search">
       <table class="form">
          <tr>
             <td style="width:50px"><strong>ID</strong></td>      
             <td><strong>Title</strong></td>
             <td><strong>Language</strong></td>
          </tr>
          <tr>
             <td style="width:50px"><HP3:Text runat="server" ID="srcId" TypeControl="TextBox" Style="width:50px"/></td>
             <td><HP3:Text runat="server" ID="srcTitle" TypeControl="TextBox" Style="width:400px"/></td>  
             <td><HP3:contentLanguage ID="srcLanguage" runat="server" Typecontrol="StandardControl" /></td> 
         </tr>
         <tr>
             <td><strong>Start Date</strong></td>      
             <td><strong>End Date</strong></td>
         </tr>
         <tr>
             <td style="width:200px"><HP3:Date ID="srcDateStart" runat="server" EnableViewState="false" ShowDate="false" TypeControl="JsCalendar" /></td>
             <td style="width:200px"><HP3:Date ID="srcDateEnd" runat="server" EnableViewState="false" ShowDate="false" TypeControl="JsCalendar" /></td>  
         </tr>
      </table>
      <table class="form">   
          <tr>
              <td><asp:button runat="server" CssClass="button" ID="btnSearch"  OnClick="SearchQuest" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
          </tr>
      </table>
      </fieldset>
      <hr />
  </asp:Panel>
 
 <asp:Label CssClass="title" runat="server" id="sectionTit">Questionnaires List</asp:Label>
 <asp:gridview id="gdw_Questionnaire" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                CssClass="tbl1"
                HeaderStyle-CssClass="header"
                AlternatingRowStyle-BackColor="#E9EEF4"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                OnPageIndexChanging="gridListQuest_PageIndexChanging"
                PagerStyle-CssClass="Pager"             
                AllowSorting="true"
                PageSize ="20"
                emptydatatext="No items available">
              <Columns >
                <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="60%" HeaderText="Title">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateField>
                <%-- <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Status">
                        <ItemTemplate> <%#IIf(DataBinder.Eval(Container.DataItem, "Status") = QuestionnaireValue.ActivationStatus.Active, "<img  src='/hp3Office/HP3Image/Ico/content_active.gif' alt='Active'  />", "<img  src='/hp3Office/HP3Image/Ico/content_noactive.gif' alt='Not Active'/>")%></ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Status">
                        <ItemTemplate>
                         <%--<%#IIf(DataBinder.Eval(Container.DataItem, "Status") = QuestionnaireValue.ActivationStatus.Active, "hhhh<asp:ImageButton id='lnkStatus' runat='server' ToolTip ='Active' ImageUrl='" & Domain() & "/HP3Image/ico/content_active.gif' OnClick='AddQuestion' CommandName ='AddQuestion' CommandArgument ='" & DataBinder.Eval(Container.DataItem, "Key.Id") & "'/>", "  no<asp:ImageButton id='lnkStatusa' runat='server' ToolTip ='Not Active' ImageUrl='/hp3Office/HP3Image/Ico/content_noactive.gif' OnClick='AddQuestion' CommandName ='AddQuestion'/>")%>--%>
                         <asp:ImageButton id="lnkStatus" runat="server" ToolTip ="Active" ImageUrl="~/hp3Office/HP3Image/ico/content_active.gif"  Visible='<%#GetVisibility(Container.DataItem, QuestionnaireValue.ActivationStatus.Active) %>'  OnClick="ChangeStatus" CommandName ="Change" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/> 
                         <asp:ImageButton id="lnkStatusNotAc" runat="server" ToolTip ="Not Active" ImageUrl="~/hp3Office/HP3Image/ico/content_noactive.gif"  Visible='<%#GetVisibility(Container.DataItem, QuestionnaireValue.ActivationStatus.NotActive) %>' OnClick="ChangeStatus" CommandName ="Change" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' /> 
                         </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%">
                       <ItemTemplate>
                            <asp:ImageButton id="lnkAddQ" runat="server" ToolTip ="Add questions" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif" OnClick="AddQuestion" CommandName ="AddQuestion" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton id="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditQuest" CommandName ="SelectQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkContentRel" runat="server" ToolTip ="Content Relation" ImageUrl="~/hp3Office/HP3Image/Ico/content_content.gif"  OnClick="ContentRelation" CommandName ="ContentRel" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <img style="cursor:pointer" alt="Preview questionnaire" src="/HP3Office/HP3Image/Ico/content_preview.gif" onclick="window.open('<%=Domain()%>/Popups/popQuestionnaire.aspx?QuestId=<%#Container.DataItem.Key.Id%>','','width=800,height=600,scrollbars=yes,status=yes')"/>
                            <asp:ImageButton id="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="DeleteQuest" CommandName ="DeleteQuest" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                       </ItemTemplate>
                </asp:TemplateField>
              </Columns>
 </asp:gridview>
 </asp:Panel>

<asp:Panel id="pnlQuestDett" runat="server" visible="false">
 <div>
    <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadQuestArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="btnSave" runat="server" Text="Save" OnClick="SaveQuest" CssClass="button"  Visible="false" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 <hr />
 </div>
 <table  style="margin-top:10px" class="form" width="99%">
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpDescrQuest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormTitle", "Title")%> </td>
        <td><HP3:Text runat ="server" id="questDescription" TypeControl ="TextBox" style="width:600px"/></td>
    </tr>
    <tr runat="server" id="tr_lSelector">
            <td style="width:25%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLang&Help=cms_HelpLang&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img   src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLang", "Language")%></td>
            <td><HP3:contentLanguage ID="lSelector" runat="server"  Typecontrol="StandardControl" /></td>
    </tr>
    <tr runat="server" id="tr_oSite">
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
        <td><HP3:ctlSite runat="server" ID="oSite" TypeControl="GenericRelation" SiteType="Site" /></td>
    </tr>
    <tr runat="server" id="tr_Aree">
          <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
          <td><HP3:ctlSite runat="server" ID="Aree" TypeControl="GenericRelation" SiteType="area"/></td>
       </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateInsert&Help=cms_HelpDateInsert&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateInsert", "Date Insert")%></td>
         <td><HP3:Date runat="server" ID="DateInsert" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateStart&Help=cms_HelpDateStart&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateStart", "Date Start")%></td>
         <td><HP3:Date runat="server" ID="DateStart" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateEnd&Help=cms_HelpDateEnd&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateEnd", "Date End")%></td>
        <td><HP3:Date runat="server" ID="DateEnd" TypeControl="JsCalendar" ShowTime="true"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFulltextWeb&Help=cms_HelpFTextQuest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFulltextWeb", "FullText")%> </td>
        <td>
<%--            <FCKeditorV2:FCKeditor id="questFullText" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>         <telerik:RadEditor ID="questFullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                 <Content></Content>
                 <Snippets>
                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                 </Snippets>
                 <Languages>
                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                 </Languages>

                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                 
                 
                 <Links>
                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                   </telerik:EditorLink>
                 </Links>
                 <Links>
                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                 </Links>        
                                     
            </telerik:RadEditor>
        </td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormMinGradeQuest&Help=cms_HelpMinGradeQuest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormMinGradeQuest", "MinGrade")%> </td>
        <td><HP3:Text runat ="server" id="questMinGrade" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%> </td>
        <td><HP3:Text runat ="server" id="questOrder" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormActive&Help=cms_HelpActive&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormActive", "Active")%></td>
        <td><asp:checkbox ID="questStatus" runat="server" /></td>
    </tr>
    <tr>
       <td></td>
       <td style="text-align:right"><asp:Button id ="ButtStep1" runat="server" Text="Next" OnClick="SaveQuestionnaire" CssClass="button"  CommandName="Step1" /></td>
    </tr>
 </table>
</asp:Panel>

<asp:Panel id="pnlQeA" runat="server" visible="false">
    <div>
        <asp:Button id ="archButt" runat="server" Text="Archive" OnClick="LoadQuestArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="editButt" runat="server" Text="Edit" OnClick="EditItemQuest" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Ico/content_edit.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:Button id ="delButton" runat="server" Text="Delete" OnClick="DeleteItemQuest" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/delete.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr/>
    </div>
    <asp:literal ID="noItems" runat="server"></asp:literal>
    <asp:Repeater id="rpQuestion" runat="server">
	            <HeaderTemplate><table width="100%"></HeaderTemplate>
	            <ItemTemplate>
	              <tr>
	                <td style="border:0;"><div style="margin-top:15px"> <UC:GroupRadioButton Id="radioQuestionItem" runat="server" GroupName='radioAnswer' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/> <strong><%#DataBinder.Eval(Container.DataItem, "Order")%>) <%#DataBinder.Eval(Container.DataItem, "Description")%></strong><%--</li>--%></div></td>
	                <td style="color:#336699;border:0">[<%#GetAnswerType(Container.DataItem)%>]</td>
    	          </tr>
    	          <div class="clearer">
    	             <asp:Repeater id="rpAnswers" runat="server"  DataSource='<%#GetAnswers(Container.DataItem)%>'>
	                        <HeaderTemplate></HeaderTemplate>
	                        <ItemTemplate>
	                                <%--<input type="radio"  name="RadioButt" id="Radio"  value='<%#DataBinder.Eval(Container.DataItem, "KeyQuestion.Id") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")& "|" & DataBinder.Eval(Container.DataItem, "Grade")& "|" & DataBinder.Eval(Container.DataItem, "IsCorrect") %>' runat="server" visible="<%#IsSingleType(Container.DataItem)%>"/> 
	                                <input type="checkbox" name="ChkButton" id="Chk"  value='<%#DataBinder.Eval(Container.DataItem, "KeyQuestion.Id") & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")& "|" & DataBinder.Eval(Container.DataItem, "Grade")& "|" & DataBinder.Eval(Container.DataItem, "IsCorrect") %>' runat="server"  visible="<%#IsMultipleType(Container.DataItem)%>"/>--%>
                                   <tr>
                                    <td style="padding-left:40px;border:0"> <UC:GroupRadioButton Id="radioAnswerItem" runat="server" GroupName='radioAnswer' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/> <%#DataBinder.Eval(Container.DataItem, "Description")%> </td>
                                    <td style="color:#336699;border:0"><%#IsCorrectAnswer(Container.DataItem)%></td>
                                   </tr>
	                        <div class="clearer">
                            </ItemTemplate>
	                       <FooterTemplate><div class="clearer"></div></FooterTemplate>
                   </asp:Repeater> 
	   	     </ItemTemplate>
	      <FooterTemplate></table></FooterTemplate>
      </asp:Repeater> 
</asp:Panel>

<asp:Panel id="pnlQuestion" runat="server" Visible="false">
 <div>
    <asp:Button id ="backButt_Q" runat="server" Text="Archive" OnClick="GoBackToQuest" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="saveQButt_Q" runat="server" Text="Update" OnClick="SaveQuestion" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <table  style="margin-top:10px" class="form" width="99%">
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpDescrQuestion&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
        <td><HP3:Text runat ="server" id="questionDescription" TypeControl ="TextArea" style="width:500px"/></td>
    </tr>
    <tr>
        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAnswerType&Help=cms_HelpAnswerTypeQuest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAnswerType", "Answer Type")%> </td>
        <td>
            <asp:RadioButtonList id="typeAnswer" runat="server">
                <asp:ListItem Text="Single Response" Value="1"></asp:ListItem>
                <asp:ListItem Text="Multiple Choice" Value="2"></asp:ListItem>
                <asp:ListItem Text="Open-ended Answer" Value="3"></asp:ListItem>
            </asp:RadioButtonList>
       </td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBond&Help=cms_HelpBondQuest&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBond", "Bond")%> </td>
        <td><HP3:Text runat ="server" id="questionBond" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOrderQuestion&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%> </td>
        <td><HP3:Text runat ="server" id="questionOrder" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCompulsory&Help=cms_HelpCompulsory&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCompulsory", "Compulsory")%> </td>
        <td><asp:CheckBox id="questionCompulsory" runat="server"/></td>
    </tr>
  </table>
</asp:Panel>

<asp:Panel id="pnlQuestionList" runat="server" Visible="false">
<hr />
<%--<asp:Button id ="btnAddQ" runat="server" Text="Add Question" OnClick="AddQuestion" CssClass="button"  Visible="false" style="margin-top:10px;margin-bottom:5px;"/>--%>
<table  style="margin-top:10px" class="form" width="99%">
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormQuestionList&Help=cms_HelpQuestionList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormQuestionList", "Question List")%> </td>
            <td  style="width:85%">
              <asp:GridView id="grw_questionList"
                    Width="100%" 
                    runat="server" 
                    autogeneratecolumns="false" 
                    GridLines="none" 
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"
                    PagerStyle-HorizontalAlign="Right"  
                    PagerStyle-CssClass="Pager"
                    Emptydatatext="No items available">
                 <Columns>
                    <asp:BoundField  HeaderText="Order"  datafield="Order"/>  
                    <asp:BoundField  HeaderText="Decription"  datafield="Description"/>  
                    <asp:BoundField  HeaderText="Answer Type"  datafield="AnswerType"/> 
                    <asp:TemplateField >        
                      <ItemTemplate>            
                            <asp:imagebutton ID="lnkAddAnswer" runat="server"  ToolTip="Add Answers"  OnClick="AddAnswer" ImageUrl="~/hp3Office/HP3Image/Ico/content_add.gif"  CommandName="AddAnswer" CommandArgument="<%#Container.DataItem.Key.Id%>" Visible="<%#isVisible(Container.DataItem) %>"/>
                      </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
              </asp:GridView>
        </td>
    </tr> 
</table>
</asp:Panel>

<asp:Panel id="pnlAnswer" runat="server" Visible="false">
 <div>
    <asp:Button id ="backButt_A" runat="server" Text="Archive" OnClick="GoBackToQuest" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:Button id ="saveButton_A" runat="server" Text="Update" OnClick="SaveAnswer" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
 </div>
 <table  style="margin-top:10px" class="form" width="99%">
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpDescrAnswer&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%> </td>
        <td><HP3:Text runat ="server" id="answerDescription" TypeControl ="TextArea" style="width:500px"/></td>
    </tr>
    <tr>
        <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%> </td>
        <td>
            <asp:DropDownList id="dwlType" runat="server" >
                <asp:ListItem Text="Standard" Value="1"></asp:ListItem>
                <asp:ListItem Text="Other" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGrade&Help=cms_HelpGradeAnswer&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGrade", "Grade")%> </td>
        <td><HP3:Text runat ="server" id="answerGrade" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpOrderAnswer&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%> </td>
        <td><HP3:Text runat ="server" id="answerOrder" TypeControl ="TextBox" style="width:200px"/></td>
    </tr>
    <tr>
        <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCorrectAnswer&Help=cms_HelpCorrectAnswer&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCorrectAnswer", "Correct Answer")%> </td>
        <td><asp:CheckBox id="answerCorrect" runat="server" Checked="false" /></td>
    </tr>
  </table>
</asp:Panel>

<asp:Panel id="pnlAnswerList" runat="server" Visible="false">
<hr />
<table  style="margin-top:10px" class="form" width="99%">
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAnswerList&Help=cms_HelpAnswerList&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAnswerList", "Answer List")%> </td>
            <td  style="width:85%">
              <asp:GridView id="grw_answerList"
                    Width="100%" 
                    runat="server" 
                    autogeneratecolumns="false" 
                    GridLines="none" 
                    AlternatingRowStyle-BackColor="#e9eef4"
                    HeaderStyle-CssClass="header"
                    PagerSettings-Position="TopAndBottom"
                    PagerStyle-HorizontalAlign="Right"  
                    PagerStyle-CssClass="Pager"
                    Emptydatatext="No items available">
                 <Columns>
                    <asp:BoundField  HeaderText="Order"  datafield="Order"/>  
                    <asp:BoundField  HeaderText="Decription"  datafield="Description"/> 
                    <asp:BoundField  HeaderText="Type"  datafield="Type"/> 
                    <asp:BoundField  HeaderText="Grade"  datafield="Grade"/> 
                    <asp:BoundField  HeaderText="Right answer"  datafield="IsCorrect"/> 
                </Columns>
              </asp:GridView>
        </td>
    </tr> 
</table>
</asp:Panel>