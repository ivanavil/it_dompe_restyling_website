<%@ Control Language="VB" ClassName="ctlSiteAreaManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlRoles" Src ="~/hp3Office/HP3Parts/ctlRolesList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlCultures" Src ="~/hp3Office/HP3Parts/ctlCulturesList.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlGroups" Src ="~/hp3Office/HP3Parts/ctlGroupsList.ascx" %>

<script runat="server">

    Private oCManager As SiteAreaManager
    Private olanguageManager As LanguageManager
    Private oSearcher As SiteAreaSearcher
    Private oCollection As SiteAreaCollection
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
   
    Private _typecontrol As ControlType = ControlType.View
    Private _sortType As SiteAreaGenericComparer.SortType
    Private _sortOrder As SiteAreaGenericComparer.SortOrder
    
    Private strJS As String
    Private strName As String
    Private utility As New WebUtility
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property IsNew() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NewItem") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        'Site.LoadControl()
        'Area.LoadControl()
        GetTypeSiteAreaValue("Select Type", 1)
    End Sub

    Function LoadValue() As SiteAreaValue
        If SelectedId <> 0 Then
            oCManager = New SiteAreaManager
            oCManager.Cache = False
            Return oCManager.Read(SelectedId)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella logicamente la sitearea selezionata
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemDelete() As Boolean
        Dim ret As Boolean = False
        If SelectedId <> 0 Then
            oCManager = New SiteAreaManager
            Dim key As New SiteAreaIdentificator
            key.Id = SelectedId
            ret = oCManager.Delete(key)
        End If
        Return ret
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente una sitearea (questa funzionalit� � da implementare)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ItemRemove() As Boolean
        'Dim ret As Boolean = False
        'If SelectedId <> 0 Then
        '    oCManager = New SiteAreaManager
        '    Dim key As New SiteAreaIdentificator
        '    key.Id = SelectedId
        '    ret = oCManager.remove(key)
        '    If ret Then
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Site)
        '        oAdvManager.RemoveAllBannerSiteArea(keyBanner, SiteAreaCollection.SiteAreaType.Area)
        '        oAdvManager.RemoveAllBannerContentType(keyBanner)
        '        oAdvManager.RemoveAllBannerBox(keyBanner)
        '    End If
        'End If
        Return True
    End Function
        
    ''' <summary>
    ''' Recupera i ruoli collegati alla sitearea
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSiteAreaRolesValue(ByVal id As Int32) As RoleCollection
        If id = 0 Then Return Nothing
        oCManager = New SiteAreaManager
        Dim key As New SiteAreaIdentificator
        key.Id = id
        oCManager.Cache = False
        Return oCManager.ReadRoles(key)
    End Function
    
    'Recupera i gruppi collegati alla sitearea
    Public Function GetSiteAreaGroupsValue(ByVal id As Int32) As GroupSiteCollection
        If id = 0 Then Return Nothing
        Dim key As New SiteAreaIdentificator
        key.Id = id
        oCManager.Cache = False
                
        Return oCManager.ReadGroupSiteRelation(key)
    End Function
    
    'Recupera le culture collegati alla sitearea
    Public Function GetSiteAreaCulturesValue(ByVal id As Int32) As CultureCollection
        If id = 0 Then Return Nothing
        
        oCManager = New SiteAreaManager
        oCManager.Cache = False
        
        Return oCManager.ReadCultureSiteRelation(New SiteAreaIdentificator(id))
    End Function
    
    'Recupera i language collegati alla sitearea
    Public Function GetSiteAreaLanguagesValue(ByVal id As Int32) As LanguageCollection
        If id = 0 Then Return Nothing
       
        oCManager = New SiteAreaManager
        oCManager.Cache = False
        
        Return oCManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(id))
    End Function
    
    
    ''' <summary>
    ''' Recupera le aree associate alla sitearea
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRelatedSiteAreaValue(ByVal id As Int32) As SiteAreaCollection
        If id = 0 Then Return Nothing
        oCManager = New SiteAreaManager
        Dim keyFather As New SiteAreaIdentificator
        keyFather.Id = id
        oCManager.Cache = False
        Return oCManager.ReadRelations(keyFather)
    End Function
    
    ''' <summary>
    ''' Recupera i type delle siteareaa
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetTypeSiteAreaValue(ByVal itemZero As String, ByVal list As Integer)
        oCManager = New SiteAreaManager
        Dim sValue As SiteTypeValue
        Dim sCollection As SiteTypeCollection
        Dim oListItem As ListItem
        If list = 1 Then ddlType.Items.Clear()
        If list = 2 Then ddlTypeEdit.Items.Clear()
        sCollection = oCManager.Read()
        
        If Not sCollection Is Nothing Then
            For Each sValue In sCollection
                oListItem = New ListItem(sValue.Name, sValue.Key.Id)
                If list = 1 Then ddlType.Items.Add(oListItem)
                If list = 2 Then ddlTypeEdit.Items.Add(oListItem)
            Next
            If itemZero <> "" And list = 1 Then ddlType.Items.Insert(0, New ListItem(itemZero, 0))
            If itemZero <> "" And list = 2 Then ddlTypeEdit.Items.Insert(0, New ListItem(itemZero, 0))
            
            sortListItems(ddlTypeEdit)
        End If
    End Sub
        
    Private Sub sortListItems(ByVal list As Object)
        'create a variables
        Dim li As New ListItem
        Dim sl As SortedList = New SortedList
        
        'loop throigh each item in the List and
        'move each item over to the SortedList
        For Each li In list.Items
            sl.Add(li.Text, li.Value)     
        Next
        'move sorted items back to list again
        list.DataSource = sl
        list.DataValueField = "Value"
        list.DataTextField = "Key"
        list.DataBind()
    End Sub
    
    Sub LoadFormDett(ByVal oValue As SiteAreaValue)
        GetTypeSiteAreaValue("", 2)
        
        If Not oValue Is Nothing Then
            With oValue
                lblDescrizione.InnerText = " - " & .Name
                lblId.InnerText = "ID: " & .Key.Id
                keyDomain.Text = .Key.Domain
                keyName.Text = .Key.Name
                Name.Text = .Name
                Label.Text = .Label
                ddlTypeEdit.SelectedIndex = ddlTypeEdit.Items.IndexOf(ddlTypeEdit.Items.FindByValue(.IdType))
                ctlRoles.GenericCollection = GetSiteAreaRolesValue(.Key.Id)
                ctlRoles.LoadControl()
                ctlGroups.GenericCollection = GetSiteAreaGroupsValue(.Key.Id)
                ctlGroups.LoadControl()
                ctlCultures.GenericCollection = GetSiteAreaCulturesValue(.Key.Id)
                ctlCultures.LoadControl()
                ctlLanguage.GenericCollection = GetSiteAreaLanguagesValue(.Key.Id)
                ctlLanguage.LoadControl()
                Delete.Checked = .Delete
                
                If (ddlTypeEdit.SelectedItem.Text = "Site") Then
                    ActivateSiteElements()
                Else
                    EnableSiteElements()
                End If
                             
            End With
        Else
            ctlRoles.LoadControl()
            ctlGroups.LoadControl()
            ctlCultures.LoadControl()
            ctlLanguage.LoadControl()
            lblDescrizione.InnerText = "New SiteArea"
            lblId.InnerText = ""
            keyDomain.Text = Nothing
            keyName.Text = Nothing
            Name.Text = Nothing
            Label.Text = Nothing
            Delete.Checked = False
            
            If (ddlTypeEdit.SelectedItem.Text = "Site") Then
                ActivateSiteElements()
            Else
                EnableSiteElements()
            End If
        End If
    End Sub
    
    Sub SaveValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New SiteAreaValue
        
        If (IsNotNull()) Then
            'se i campi di testo non sono nulli 
            'procede regolarmente con le operazioni 
            With (oValue)
                If SelectedId > 0 Then .Key.Id = SelectedId
                .Key.Domain = keyDomain.Text
                .Key.Name = keyName.Text
                .Name = Name.Text
                .Label = Label.Text
                If ddlTypeEdit.SelectedValue <> "" Then .IdType = ddlTypeEdit.SelectedValue
                .Delete = Delete.Checked
            End With

            oCManager = New SiteAreaManager
            If IsNew Then
                oValue = oCManager.Create(oValue)
                ShowPanel(FindControl("pnlGrid").ID)
            Else
                oValue = oCManager.Update(oValue)
                ShowPanel(FindControl("pnlGrid").ID)
            End If
       
            oCManager.RemoveAllSiteAreaRoles(oValue.Key)
            If Not ctlRoles.GetSelection Is Nothing AndAlso ctlRoles.GetSelection.Count > 0 Then
                Dim Rc As New Object
                Rc = ctlRoles.GetSelection
                
                oCManager.CreateRole(oValue.Key, Rc)
            End If
            
            'salvo gli accoppiamenti Site/Group Site/Culture Site/Language
            'solo se il tipo specificato � 'Site'
            If (ddlTypeEdit.SelectedItem.Text = "Site") Then
                oCManager.RemoveAllGroupSiteRelation(New SiteAreaIdentificator(oValue.Key.Id))
                If Not ctlGroups.GetSelection Is Nothing AndAlso ctlGroups.GetSelection.Count > 0 Then
                    Dim Rc As New Object
                    Dim gValue As New GroupValue
                    Dim GSRValue As New GroupSiteRelationValue
                       
                    GSRValue.SiteAreaId = New SiteAreaIdentificator(oValue.Key.Id)
            
                    Rc = ctlGroups.GetSelection
                    Dim gcoll As GroupSiteCollection = CType(Rc, GroupSiteCollection)
                    If Not gcoll Is Nothing Then
                        If (gcoll.Count > 0) Then
                            For Each gValue In gcoll
                                GSRValue.GroupId = New GroupIdentificator(gValue.Key.Id)
                                oCManager.CreateGroupSiteRelation(GSRValue)
                            Next
                        End If
            
                    End If
                End If
        
                oCManager.RemoveAllCultureSiteRelation(New SiteAreaIdentificator(oValue.Key.Id))
                If Not ctlCultures.GetSelection Is Nothing AndAlso ctlCultures.GetSelection.Count > 0 Then
                    Dim Rc As New Object
                    Dim cultureValue As New CultureValue
                    Dim CSRValue As New CultureSiteRelationValue
           
                    CSRValue.SiteAreaId = New SiteAreaIdentificator(oValue.Key.Id)
          
                    Rc = ctlCultures.GetSelection
                    Dim cultureColl As CultureCollection = CType(Rc, CultureCollection)
                    If Not cultureColl Is Nothing Then
                        If (cultureColl.Count > 0) Then
                            For Each cultureValue In cultureColl
                          
                                CSRValue.CultureId = New CultureIdentificator(cultureValue.Key.Id)
                                oCManager.CreateClutureSiteRelation(CSRValue)
                            Next
                        End If
            
                    End If
                End If
        
                oCManager.RemoveAllLanguageSiteRelation(New SiteAreaIdentificator(oValue.Key.Id))
                If Not ctlLanguage.GetSelection Is Nothing AndAlso ctlLanguage.GetSelection.Count > 0 Then
                    Dim Rc As New Object
                    Dim olanguageValue As New LanguageValue
                    Dim LSRValue As New LanguageSiteRelationValue
           
                    LSRValue.SiteAreaId = New SiteAreaIdentificator(oValue.Key.Id)
          
                    Rc = ctlLanguage.GetSelection
                    Dim olanguageColl As LanguageCollection = CType(Rc, LanguageCollection)
                    If Not olanguageColl Is Nothing Then
                        If (olanguageColl.Count > 0) Then
                            For Each olanguageValue In olanguageColl
                          
                                LSRValue.LanguageId = New LanguageIdentificator(olanguageValue.Key.Id)
                                oCManager.CreateLanguageSiteRelation(LSRValue)
                            Next
                        End If
            
                    End If
                End If
            End If
        Else
            '    'stampa un messaggio di avviso
            CreateJsMessage("'" & strName & "'" & " cannot be empty!")
        End If
        
        BindWithSearch(gridList)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
               
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    'BindWithSearch(gridList)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        If Not Page.IsPostBack Then
            
            BindDomains()
            ShowRightPanel()
        End If
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As SiteAreaSearcher = Nothing)
        oCManager = New SiteAreaManager
        oCollection = New SiteAreaCollection
        
        If Searcher Is Nothing Then
            Searcher = New SiteAreaSearcher
        End If
        oCManager.Cache = False
        oCollection = oCManager.Read(Searcher)
        If Not oCollection Is Nothing Then
            oCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oCollection
        objGrid.DataBind()
    End Sub
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSiteAreaValue As SiteAreaValue
        
        oSearcher = New SiteAreaSearcher
        If txtId.Text <> "" Then oSearcher.Key = New SiteAreaIdentificator(txtId.Text)
        
        If txtNameArea.Text <> "" Then oSearcher.Key.Name = txtNameArea.Text
        If dwlDomain.SelectedItem.Value <> "-1" Then oSearcher.Key.Domain = dwlDomain.SelectedItem.Value
        
        'oSiteAreaValue = GetSiteSelection(Site)
        'If Not oSiteAreaValue Is Nothing Then
        '    oSearcher.KeyFather.Id = oSiteAreaValue.Key.Id
        'End If
        
        'oSiteAreaValue = GetSiteSelection(Area)
        'If Not oSiteAreaValue Is Nothing Then
        '    oSearcher.KeyFather.Id = oSiteAreaValue.Key.Id
        'End If
        
        If ddlType.SelectedValue <> "" Then oSearcher.IdType = ddlType.SelectedValue
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> SiteAreaGenericComparer.SortType.ById Then
                    SortType = SiteAreaGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Name"
                If SortType <> SiteAreaGenericComparer.SortType.ByName Then
                    SortType = SiteAreaGenericComparer.SortType.ByName
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Key"
                If SortType <> SiteAreaGenericComparer.SortType.ByKey Then
                    SortType = SiteAreaGenericComparer.SortType.ByKey
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Label"
                If SortType <> SiteAreaGenericComparer.SortType.ByLabel Then
                    SortType = SiteAreaGenericComparer.SortType.ByLabel
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As SiteAreaGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = SiteAreaGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As SiteAreaGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As SiteAreaGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = SiteAreaGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As SiteAreaGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        'Dim ltl As Literal
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        'ltl = e.Row.FindControl("litBanner")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing Then 'AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                IsNew = False
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedId = id
                ItemDelete()
                BindWithSearch(gridList)
            Case "CopyItem"
                TypeControl = ControlType.Edit
                SelectedId = id
                IsNew = True
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.Edit
        SelectedId = 0
        IsNew = True
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedId = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    Sub ActivateSiteElements()
        divSite.Visible = True
    End Sub
    
    Sub EnableSiteElements()
        divSite.Visible = False
    End Sub
    
    Sub SelectChanged(ByVal Sender As Object, ByVal e As EventArgs)
        If ddlTypeEdit.SelectedItem.Text = "Site" Then
            ActivateSiteElements()
        ElseIf ddlTypeEdit.SelectedItem.Text = "Area" Then
            EnableSiteElements()
        End If
    End Sub
    
    'controlla che i campi di testo non siano nulli
    Function IsNotNull() As Boolean
        Dim _return As Boolean = False
        
        If (keyDomain.Text = String.Empty) Then strName = "Identificator Domain"
          
        If (keyName.Text = String.Empty) Then strName = "Identificator Name"
        
        If (Name.Text = String.Empty) Then strName = "Name"
        
        If (Label.Text = String.Empty) Then strName = "Label"
        
        If (ddlTypeEdit.SelectedItem.Text = String.Empty) Then strName = "Type"
                       
        If ((keyDomain.Text <> String.Empty) And (keyName.Text <> String.Empty) And (Name.Text <> String.Empty) And (Label.Text <> String.Empty) And (ddlTypeEdit.SelectedItem.Text <> String.Empty)) Then
        
            _return = True
        End If
        
        Return _return
    End Function
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollection = New SiteAreaCollection
        oSearcher = New SiteAreaSearcher
        oCManager = New SiteAreaManager
        
        oCollection = oCManager.Read(oSearcher)
        oCollection = oCollection.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollection, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />
<asp:Panel id="pnlGrid" runat="server">
    <asp:Button ID="Button1" CssClass="button" runat="server" Text="New" onclick="NewItem" style=" margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">SiteArea Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Domain</strong></td>
                    <td><strong>Name</strong></td>
                    <td><strong>Type</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><asp:DropDownList id="dwlDomain" runat="server"/></td>  
                    <td><HP3:Text runat ="server" id="txtNameArea" TypeControl ="TextBox" style="width:250px" /></td>  
                    <td><asp:DropDownList id="ddlType" DataTextField="Name" DataValueField="Key.Id" runat="server"></asp:DropDownList></td>  
                </tr> 
                <tr>
                    <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                </tr>
            </table>
            
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"             
                    AllowSorting="true"
                    OnPageIndexChanging="gridList_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridList_Sorting"
                    emptydatatext="No item available"                
                    OnRowDataBound="gridList_RowDataBound"
                    >
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="18%" HeaderText="Identificator" SortExpression="Key">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%></ItemTemplate>
                    </asp:TemplateField>  
                    <asp:BoundField HeaderStyle-Width="40%" DataField ="Name" HeaderText ="Description" SortExpression="Name" />
                    <asp:BoundField HeaderStyle-Width="30%" DataField ="Label" HeaderText ="Label" SortExpression="Label" />
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                            <asp:ImageButton ID="lnkCopy" runat="server" ToolTip ="Copy item" ImageUrl="~/hp3Office/HP3Image/ico/content_copy.gif" OnClick="OnItemSelected" CommandName ="CopyItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" runat="server" Text="Archive" onclick="GoList" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button runat="server" ID="btnSave" onclick="SaveValue" Text="Save" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>  
      
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%" >
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorDomain&Help=cms_HelpIdentificatorDomain&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorDomain", "Identificator Domain")%></td>
            <td><HP3:Text runat ="server" id="keyDomain" TypeControl ="TextBox" style="width:90px" MaxLength="5"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIdentificatorName&Help=cms_HelpIdentificatorName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormIdentificatorName", "Identificator Name")%></td>
            <td><HP3:Text runat ="server" id="keyName" TypeControl ="TextBox" style="width:90px" MaxLength="10"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpSiteType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%></td>
            <td><asp:DropDownList id="ddlTypeEdit" DataTextField="Name" DataValueField="Key.Id" runat="server" autopostback="true" OnSelectedIndexChanged="SelectChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpSiteDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" id="Name" TypeControl ="TextBox" style="width:300px" MaxLength="120"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLabel&Help=cms_HelpSiteLabel&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLabel", "Label")%></td>
            <td><HP3:Text runat ="server" id="Label" TypeControl ="TextBox" style="width:300px" MaxLength="120"/></td>
        </tr>
        <tr>
            <td><a href="#" onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormListRoles&Help=cms_HelpListRoles&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormListRoles", "List Roles")%></td>
            <td><HP3:ctlRoles runat ="server" id="ctlRoles" typecontrol="GenericRelation" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDelete&Help=cms_HelpDelete&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDelete", "Delete")%></td>
            <td><asp:checkbox ID="Delete" runat="server" /></td>
        </tr>
        </table>
      
      <div id="divSite" visible="false" runat="server" style="width:100%">
          <table class="form" style="width:100%">
            <tr>
                <td style="width:250px"></td>
                <td></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGroups&Help=cms_HelpGroups&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGroups", "Groups")%></td>
                <td><HP3:ctlGroups runat ="server" id="ctlGroups" typecontrol="GenericRelation"/></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormCultures&Help=cms_HelpCultures&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormCultures", "Cultures")%></td>
                <td><HP3:ctlCultures runat ="server" id="ctlCultures" typecontrol="GenericRelation" /></td>
            </tr>
            <tr>
                <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguages&Help=cms_HelpLanguages&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguages", "Languages")%></td>
                <td><HP3:contentLanguage runat ="server" id="ctlLanguage" typecontrol="GenericRelation"/></td>
            </tr>
        </table>
    </div> 
</asp:Panel>

 
 

   
        
   




