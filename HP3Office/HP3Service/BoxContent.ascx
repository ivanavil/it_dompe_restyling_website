<%@ Control Language="VB" ClassName="BoxContent" %>
<%@ Implements Interface="System.Web.UI.ICallbackEventHandler" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.Logs"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Register TagName="ctlText" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlText.ascx"%>
<%@ Register TagName="ctlLanguage" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register TagName="ctlSite" tagprefix="HP3" Src="~/hp3Office/HP3Parts/ctlSite.ascx"%>

<script runat="server">
    Private iTheme As ThemeIdentificator
    Private SiteFolder As String
    Private webRequest As New WebRequestValue

    Private _webUtility As WebUtility
    Private mPageManager As New MasterPageManager
    Private objQs As New ObjQueryString
    
    Private oLManager As New LanguageManager()
    Private _boxTypeCollection As BoxTypeCollection = Nothing
    Private oGenericUtlity As New GenericUtility
    
    Private oCollectionSiteArea As SiteAreaCollection

    Private siteAreaSearcher As New SiteAreaSearcher
    Private siteAreaManager As New SiteAreaManager
    Private utility As New WebUtility
    ' il datasource dei contents
    Private _dataSource As ContentCollection = Nothing
    ' il datasource dei contents relazionati al box
    Private _dataSourceRelations As ContentCollection
    
    ' il datasource dei contents relazionati al box
    Private _dataSourceActiveRelations As ContentBoxCollection
    
    Dim oCBManager As New ContentManager
    Dim oCBSearcher As New ContentBoxSearcher
    Dim oCBCollection As New ContentBoxCollection
    Dim oCBValue As New ContentBoxValue
    
    ' la lista degli id che hanno una relazione con il box
    Private _related As List(Of Integer) = Nothing
    ' la lista delle modifiche apportate alle relazioni
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing
    
    ' le variabili istanza delle properties pubbliche
    'Private _allowEdit As Boolean = True
    Private _useInPopup As Boolean = False
    Private _cache As PersistentCache

    Private _contentTypeCollection As ContentTypeCollection = Nothing
    
    ' l'id del box corrente
    Private _keyBox As BoxIdentificator
    ' l'istanza del Box corrente
    Private _boxValue As BoxValue

    ' l'id del content corrente
    Private _keyContent As ContentIdentificator
    ' l'istanza del Content corrente
    Private _contentValue As ContentValue

    Private strJs As String
    Private _idRel As Integer
    
    Public Enum ControlType
        List = 0
        Edit = 1
    End Enum
    
    Private _typecontrol As ControlType = ControlType.List
    
    Private strDomain As String
 

    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property

    Public Property UseInPopup() As Boolean
        Get
            Return _useInPopup
        End Get
        Set(ByVal value As Boolean)
            _useInPopup = value
        End Set
    End Property
    
    Public Property IdRel() As Integer
        Get
            Return ViewState("_idRel")
        End Get
        Set(ByVal value As Integer)
            ViewState("_idRel") = value
        End Set
    End Property
    
    Public Property Operation() As String
        Get
            Return ViewState("operation")
        End Get
        Set(ByVal value As String)
            ViewState("operation") = value
        End Set
    End Property
       
    Private Overloads ReadOnly Property Cache() As PersistentCache
        Get
            If _cache Is Nothing Then
                _cache = New PersistentCache
            End If
            Return _cache
        End Get
    End Property
    
    Public Property KeyBox() As BoxIdentificator
        Get
            If _keyBox Is Nothing Then
                webRequest.customParam.LoadQueryString()
                _keyBox = New BoxIdentificator(Integer.Parse(webRequest.customParam.item("KeyBox")))
            End If
            Return _keyBox
        End Get
        Set(ByVal value As BoxIdentificator)
            _keyBox = value
        End Set
    End Property
    
    Private Sub readBoxValue()
        Dim _boxManager As New BoxManager
        _boxValue = _boxManager.Read(KeyBox)
    End Sub

    Public Property BoxValue() As BoxValue
        Get
            If (_boxValue Is Nothing OrElse _boxValue.IdBox = 0) AndAlso Not KeyBox Is Nothing Then
                readBoxValue()
            End If
            Return _boxValue
        End Get
        Set(ByVal value As BoxValue)
            _boxValue = value
        End Set
    End Property

    Public Property KeyContent() As ContentIdentificator
        Get
            If _keyContent Is Nothing Then
                _keyContent = ViewState("KeyContent")
            End If
            Return _keyContent
        End Get
        Set(ByVal value As ContentIdentificator)
            _keyContent = value
            ViewState.Add("KeyContent", _keyContent)
        End Set
    End Property
    
    Private Function readContentValue() As ContentValue
        Dim _contentManager As New ContentManager
        Dim contentCollection As New ContentCollection
        
        Dim so As New ContentSearcher
        so.key = KeyContent
        so.Active = SelectOperation.All
        so.Display = SelectOperation.All
        so.Delete = SelectOperation.All
        contentCollection = _contentManager.Read(so)

        If Not contentCollection Is Nothing AndAlso contentCollection.Any Then
            Return contentCollection(0)
        End If
        Return Nothing
    End Function

    Public Property ContentValue() As ContentValue
        Get
            If (_contentValue Is Nothing OrElse _contentValue.Key.Id = 0) AndAlso Not KeyContent Is Nothing Then
                _contentValue = readContentValue()
            End If
            Return _contentValue
        End Get
        Set(ByVal value As ContentValue)
            _contentValue = value
        End Set
    End Property

 
    Private ReadOnly Property KeyChanges() As String
        Get
            Dim _key As String
            _key = ViewState("RelatedChangesKey")
            If _key Is Nothing Then
                ' Genera una chiave random per assicurarsi che lo stato
                ' delle modifiche cambi tra una richiesta e l'altra
                _key = SimpleHash.GenerateUUID(32)
                ViewState("RelatedChangesKey") = _key
            End If
            Return _key
        End Get
    End Property

 
    
    Public Property ShowRelatedSearch() As Boolean
        Get
            Return pnlRelatedSearch.Visible
        End Get
        Set(ByVal value As Boolean)
            pnlRelatedSearch.Visible = value
        End Set
    End Property
    
    Public ReadOnly Property DataValueFilter() As String
        Get
            Return txtDataValueFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataTextFilter() As String
        Get
            Return txtDataTextFilter.Text
        End Get
    End Property

    Public ReadOnly Property DataGlobalKeyFilter() As String
        Get
            Return txtGlobalKeyFilter.Text
        End Get
    End Property
    
    Public ReadOnly Property DataPrimaryKeyFilter() As String
        Get
            Return txtPrimaryKeyFilter.Text
        End Get
    End Property
    
    Public Sub BindGrid()
        BindGrid(False)
    End Sub

    Public Sub BindGrid(ByVal clearCache As Boolean)
        ' Legge il datasource completo 
        getDataSource(clearCache)
        
        ' Effettua il bind della griglia dei contents
        gridContents.DataSource = _dataSource
        gridContents.DataBind()
    End Sub
    
    Public Sub BindGridRelation()
        BindGridRelation(False)
    End Sub
    
    Public Sub BindGridRelation(ByVal clearCache As Boolean)
        oCBManager = New ContentManager
        oCBSearcher = New ContentBoxSearcher
        oCBCollection = New ContentBoxCollection
        oCBValue = New ContentBoxValue
         
        If txtContentId.Text <> "" Then
            oCBSearcher.KeyContent.Id = txtContentId.Text
        End If
        
        Dim oSiteAreaValue As SiteAreaValue
        oSiteAreaValue = GetSiteSelection(ctlSiteAreaRel)
        If Not oSiteAreaValue Is Nothing Then
            oCBSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
        End If
        
        'oCBSearcher.Key.Id = KeyBox.Id
        _dataSourceActiveRelations = oCBManager.ReadContentBoxRelation(oCBSearcher)
                
        ' Effettua il bind della griglia dei contents relation
        gridRelations.DataSource = _dataSourceActiveRelations
        gridRelations.DataBind()
    End Sub
    
    Public Sub BindGridRelations()
        BindGridRelations(False)
    End Sub

    Public Sub BindGridRelations(ByVal clearCache As Boolean)
        'legge le relazioni 
        Dim coll As ContentBoxCollection
        coll = getContentBoxRelation()
      
        
        ' Effettua il bind della griglia delle relazioni
        gridRelations.DataSource = coll '_dataSourceRelations
        gridRelations.DataBind()
    End Sub

    Function getContentBoxRelation() As ContentBoxCollection
        Dim _oContentManager As New ContentManager()
        _oContentManager.Cache = False
       
      
        Return _oContentManager.ReadContentBoxRelation(KeyBox)
     
        
          
    End Function
    
    Function GetKey(ByVal content_id As Int32, ByVal language_id As Int16) As String
        Return content_id & "_" & language_id
    End Function

    Sub BackToBoxManager(ByVal s As Object, ByVal e As EventArgs)
        'If Not AllowEdit Then Return
        Dim wr As New WebRequestValue
        Dim ctm As New ContentTypeManager()
        Dim mPage As New MasterPageManager()
        Dim objQs As New ObjQueryString
        wr.KeycontentType = New ContentTypeIdentificator("CMS", "boxmanager")
        wr.customParam.append(objQs)
        Response.Redirect(mPage.FormatRequest(wr))
    End Sub

    Sub BackToList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.List
        ShowRightPanel()
    End Sub

    Protected Sub gridContents_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridContents.DataBound
        If gridContents.FooterRow Is Nothing Then Return
        For cellNum As Integer = gridContents.Columns.Count - 1 To 1 Step -1
            Try
                gridContents.FooterRow.Cells.RemoveAt(cellNum)
            Catch ex As Exception
            End Try
        Next
        gridContents.FooterRow.Cells(0).ColumnSpan = gridContents.Columns.Count
        gridContents.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        If Not _dataSource Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSource, Object).Count
            Dim startIndex As Integer = gridContents.PageSize * gridContents.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            msg.Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", s, startIndex + 1, startIndex + gridContents.Rows.Count, gridViewTotalCount, s)
        Else
            msg.Text = "No items found."
        End If
    End Sub

    Protected Sub gridContents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridContents.PageIndexChanging
        gridContents.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    
    
    Protected Sub gridRelations_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridRelations.DataBound
        If gridRelations.FooterRow Is Nothing Then Return
        For cellNum As Integer = gridRelations.Columns.Count - 1 To 1 Step -1
            Try
                gridRelations.FooterRow.Cells.RemoveAt(cellNum)
            Catch ex As Exception
            End Try
        Next
        gridRelations.FooterRow.Cells(0).ColumnSpan = gridRelations.Columns.Count
        gridRelations.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Center
        If Not _dataSourceActiveRelations Is Nothing Then
            Dim gridViewTotalCount As Integer = CType(_dataSourceActiveRelations, Object).Count
            Dim startIndex As Integer = gridRelations.PageSize * gridRelations.PageIndex
            Dim s As String = IIf(gridViewTotalCount > 1, "s", "")
            msgRelation.Text = String.Format("Showing item{0} {1} to {2} of {3} item{4}", s, startIndex + 1, startIndex + gridRelations.Rows.Count, gridViewTotalCount, s)
        Else
            msgRelation.Text = ""
        End If
    End Sub
    Protected Sub gridRelations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridRelations.PageIndexChanging
        gridRelations.PageIndex = e.NewPageIndex
        BindGridRelation()
    End Sub
    
    Protected Sub btnSearchRelated_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchRelated.Click
        filterDataSource()
    End Sub

    Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSearch.Click
        txtDataTextFilter.Text = ""
        txtDataValueFilter.Text = ""
        txtGlobalKeyFilter.Text = ""
        txtPrimaryKeyFilter.Text = ""
        ctlSiteC.SetSelectedIndex(0)
        filterDataSource()
    End Sub

    
    Protected Sub btnSearchFromRelation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchFromRelation.Click
        filterDataSourceRelation()
    End Sub
    
    
    Public ReadOnly Property getgridContents() As GridView
        Get
            Return gridContents
        End Get
    End Property
    

    Private Sub LoadBoxSummary()
        If KeyBox.Id > 0 Then
            lblBoxId.Text = BoxValue.IdBox
            lblBoxName.Text = BoxValue.Name
            lblBoxDescription.Text = BoxValue.Description
            lblBoxType.Text = BoxValue.BoxType.Description
            lblBoxActive.Text = IIf(BoxValue.Active, "yes", "no")
        End If
    End Sub

    Private Sub LoadContentSummary()
        If Not KeyContent Is Nothing AndAlso KeyContent.Id > 0 Then
            lblContentId.Text = ContentValue.Key.Id
 
            If ContentValue.DatePublish.HasValue Then
                lblContentDatePublish.Text = ContentValue.DatePublish
            Else
                lblContentDatePublish.Text = ""
            End If
 
            imgContentLanguage.ImageUrl = "~/hp3Office/HP3Image/Language/ico_flag_" & oLManager.GetCodeLanguage(ContentValue.Key.IdLanguage) & "_small.gif"
            imgContentLanguage.Visible = True
            lblContentTitle.Text = ContentValue.Title
        Else
            imgContentLanguage.Visible = False
        End If
    End Sub
    
    Protected Sub dwlDomainContentEdit_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        relContentType.SubDomainContentType = dwlDomainContentEdit.SelectedValue
        relContentType.LoadControl()
    End Sub
    Protected Sub dwlDomainEdit_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ctlSiteArea.SubDomainArea = dwlDomainEdit.SelectedValue
        ctlSiteArea.LoadControl()
    End Sub
    Protected Sub dwlDomain_IndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ctlSiteAreaRel.SubDomainArea = dwlDomain.SelectedValue
        ctlSiteAreaRel.LoadControl()
    End Sub
    
    'popola la dwl dei Domain nel Searcher
    Sub BindDomains()
        oCollectionSiteArea = New SiteAreaCollection
        siteAreaSearcher = New SiteAreaSearcher
        siteAreaManager = New SiteAreaManager

        oCollectionSiteArea = siteAreaManager.Read(siteAreaSearcher)
        oCollectionSiteArea = oCollectionSiteArea.DistinctBy("Key.Domain")
        
        utility.LoadListControl(dwlDomain, oCollectionSiteArea, "Key.Domain", "Key.Domain")
        dwlDomain.Items.Insert(0, New ListItem("--Select--", -1))
        utility.LoadListControl(dwlDomainEdit, oCollectionSiteArea, "Key.Domain", "Key.Domain")
        dwlDomainEdit.Items.Insert(0, New ListItem("--Select--", -1))
        utility.LoadListControl(dwlDomainContentEdit, oCollectionSiteArea, "Key.Domain", "Key.Domain")
        dwlDomainContentEdit.Items.Insert(0, New ListItem("--Select--", -1))
        
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        strDomain = WebUtility.MyPage(Me).DomainInfo
        If Not Page.IsCallback Then
            Dim callbackRef As String = Page.ClientScript.GetCallbackEventReference(Me, _
                "getCbParams()", _
                "callbackReorderItems", _
                Nothing, _
                "callbackErrorReorderItems", _
                True)
        End If
       
        If Not Page.IsPostBack Then
            LoadBoxSummary()
            BindDomains()
        End If
        
        BindGridRelations(False)
      
        ShowRightPanel()
        
        
    End Sub
    
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
        Response.Clear()
        'SimpleLogger.Log("Callback params: " & eventArgument)
        Dim params() As String = eventArgument.Split("&")
        If params(0).Split("=")(1) = "reorderItems" Then
            Dim items As String = params(1).Split("=")(1)
            reorderItems(items)
        End If
        Response.End()
    End Sub
    
    Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
        
        Return "OK"
    End Function
    
    Private Sub reorderItems(ByVal items As String)
        
        If Not items Is Nothing Then
            Dim arrayId() = items.Split(",")
            If Not arrayId Is Nothing AndAlso arrayId.Length > 0 Then
                'SimpleLogger.Log("reordering items: " & items & " - count = " & arrayId.Length)
                
                Dim cm As New ContentManager
                Dim contentId As ContentIdentificator
                Dim contentBox As ContentBoxValue
                Dim order As Integer = 0
                Dim strId As String
                For Each strId In arrayId
                    'SimpleLogger.Log("+ reordering item: " & strId)
                    
                    contentId = New ContentIdentificator()
                    contentId.PrimaryKey = strId
                    contentBox = cm.ReadContentBoxValue(KeyBox, contentId)
                    If Not contentBox Is Nothing Then
                        order = order + 1
                        If contentBox.Order <> order Then
                            'SimpleLogger.Log("| changing order from: " & contentBox.Order & " To: " & order.ToString)
                            contentBox.Order = order
                            If cm.UpdateContentBox(contentBox) Then
                                'SimpleLogger.Log("= done")
                            End If
                        End If
                    End If
                Next
            End If
        End If
    End Sub
    
    Private Sub ShowRightPanel()
        Select Case TypeControl
            Case ControlType.Edit
                pnlContentSummary.Visible = True
                pnlActiveRelations.Visible = False
                pnlSearchRelation.Visible = False
                pnlRelatedSearch.Visible = False
                pnlGridContents.Visible = False
                pnlRelationEdit.Visible = True
                btBackToList.Visible = True
            Case ControlType.List
                ContentType.LoadControl()
                pnlContentSummary.Visible = False
                pnlRelationEdit.Visible = False
                btBackToList.Visible = False
                If KeyBox.Id = 0 And Not UseInPopup Then
                    pnlContentBox.Visible = False
                Else
                    pnlContentBox.Visible = True
                    pnlActiveRelations.Visible = True
                    pnlSearchRelation.Visible = False 'ANU
                    pnlRelatedSearch.Visible = True
                    pnlGridContents.Visible = True
                    If Not Page.IsPostBack Then
                        'LoadControl()
                        ctlSiteC.LoadControl()
                        ctlSiteAreaRel.LoadControl()
                        relContentType.LoadControl()
                        ctlSiteArea.LoadControl()
                    End If
                End If
        End Select
    End Sub
    
    Private ReadOnly Property BoxTypes() As BoxTypeCollection
        Get
            If _boxTypeCollection Is Nothing Then
                Dim _boxManager As New BoxManager
                _boxTypeCollection = _boxManager.ReadBoxType(0)
            End If
            Return _boxTypeCollection
        End Get
    End Property

    Private Function getDataSource() As ContentCollection
        Return getDataSource(False)
    End Function
    
    Private Function getDataSource(ByVal clearCache As Boolean) As ContentCollection
        If _dataSource Is Nothing Then
            Dim _ContentSearcher As New ContentSearcher()
            Dim ctc As ContentTypeCollection = ContentType.GetSelection()
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                _ContentSearcher.KeyContentType.Id = ctc.Item(0).Key.Id
            End If
            If Not DataValueFilter Is Nothing AndAlso DataValueFilter <> "" Then
                _ContentSearcher.key.Id = DataValueFilter
            End If
            If Not DataTextFilter Is Nothing AndAlso DataTextFilter <> "" Then
                _ContentSearcher.SearchString = DataTextFilter
            End If
            If Not DataGlobalKeyFilter Is Nothing AndAlso DataGlobalKeyFilter <> "" Then
                _ContentSearcher.key.IdGlobal = DataGlobalKeyFilter
            End If
            If Not DataPrimaryKeyFilter Is Nothing AndAlso DataPrimaryKeyFilter <> "" Then
                _ContentSearcher.key.PrimaryKey = DataPrimaryKeyFilter
            End If
            If lSelectorLanguage.Language > 0 Then
                _ContentSearcher.key.IdLanguage = lSelectorLanguage.Language
            End If
            Dim _oContentBoxValue As New ContentBoxValue()
            Dim oSiteAreaValue As SiteAreaValue
            oSiteAreaValue = GetSiteSelection(ctlSiteC)
            If Not oSiteAreaValue Is Nothing Then
                _ContentSearcher.KeySiteArea = oSiteAreaValue.Key
            End If
            
            
            Dim _oContentManager As New ContentManager()
            _oContentManager.Cache = Not clearCache
            _dataSource = _oContentManager.Read(_ContentSearcher) '.DistinctBy("Key.Id")
            
            ' TODO: filtrare anche la griglia delle relazioni in base ai parametri impostati nel contentSearcher? (in questo caso commentare la riga di sotto)
            _ContentSearcher = New ContentSearcher()
            _dataSourceRelations = _oContentManager.ReadContentBox(KeyBox, _ContentSearcher) '.DistinctBy("Key.Id")
        End If
        Return _dataSource
    End Function

    Private Function getDataSourceRelations() As ContentCollection
        Return getDataSourceRelations(False)
    End Function
    
    Private Function getDataSourceRelations(ByVal clearCache As Boolean) As ContentCollection
        If _dataSource Is Nothing Then
            Dim _contentSearcher As New ContentSearcher()
            '_contentSearcher.KeySite.Id =  Integer.Parse(webRequest.customParam.item("KeyBox"))
            Dim _oContentManager As New ContentManager()
            _oContentManager.Cache = Not clearCache
            
            _dataSourceRelations = _oContentManager.ReadContentBox(KeyBox, _contentSearcher) '.DistinctBy("Key.Id")
        End If
        Return _dataSourceRelations
    End Function

    'Private Function readSelection(ByVal keyBox As BoxIdentificator) As System.Collections.Generic.List(Of Integer)
    '    If Not Request("selItem") Is Nothing AndAlso Request("selItem") <> "" Then
    '        _related = New List(Of Integer)
    '        _related.Add(Request("selItem"))
    '        'RelatedChanges.Clear()
    '        'RelatedChanges.Add(Request("selItem"), True)
    '        Return _related
    '    Else
    '        Return Nothing
    '    End If
        
    'End Function

    Private Function readRelated(ByVal keyBox As Healthware.HP3.Core.Box.ObjectValues.BoxIdentificator) As System.Collections.Generic.List(Of Integer)
        
        If _related Is Nothing AndAlso Not keyBox Is Nothing Then
            Dim _oContentRelatedCollection As ContentCollection = Nothing
            Dim _oBoxManager As New BoxManager
            'TODO: _oContentRelatedCollection = _oBoxManager.ReadBoxContent(keyBox)
            If Not _oContentRelatedCollection Is Nothing Then
                _related = New List(Of Integer)
                Dim _content As ContentValue
                For Each _content In _oContentRelatedCollection
                    _related.Add(_content.Key.PrimaryKey)
                Next
            End If
        End If
        Return _related
    End Function
    
    Private Overloads Sub LoadControl()
        Dim _oKeyBox As BoxIdentificator = KeyBox
        Dim objCMSUtility As New GenericUtility

        readRelated(_oKeyBox)
        
        'languageSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        'relContentType.LoadControl()       
        'ctlSiteArea.LoadControl()

        
        BindGrid()
        BindGridRelations()
        pnlContentBox.Visible = True

    End Sub
    
    Public Sub filterDataSource()
        LoadControl()
    End Sub
    
    Private Overloads Sub LoadControlRelation()
                
        BindGridRelation()
        pnlContentBox.Visible = True
    End Sub
    
    Public Sub filterDataSourceRelation()
        LoadControlRelation()
    End Sub
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Protected Sub addRelation(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
      
        Dim key As Integer = sender.commandArgument.ToString
        Dim oContentManager As New ContentManager()
        Dim objCMSUtility As New GenericUtility()
        Operation = "Create"
        IdRel = 0
        
        Dim maxOrder As Integer = 0
        Try
            maxOrder = oContentManager.ReadContentBoxMaxOrder(KeyBox, New ContentSearcher())
        Catch ex As Exception
            maxOrder = 0
        End Try
        maxOrder = maxOrder + 1
        ' 1149
        KeyContent = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(key)
        LoadContentSummary()

        relContentType.SetSelectedIndex(0)
        ctlSiteArea.SetSelectedIndex(0)
        txtOrder.Text = maxOrder.ToString()
        txtLink.Text = "http://"
        ddTarget.SelectedIndex = 0
        txtTarget.Text = ""
        'languageSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
        txtTitle.Text = ""
        txtLaunch.Text = ""
        
        TypeControl = ControlType.Edit
        ShowRightPanel()
        
    End Sub

    ''' <summary>
    ''' Lettura delle descrizioni dei themi dato l'id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFatherTheme(ByVal id As Int32) As String
        Dim ov As Object = oGenericUtlity.GetThemeValue(id)
        
        If ov Is Nothing Then
            Return Nothing
        Else
            Return ov.Description
        End If
    End Function
    
 
    Protected Sub EditRelation(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
      
 
        
        
        Dim comargument As String = sender.commandArgument.ToString
        Dim keys As String() = comargument.Split("|")
        
         
        
        Dim key As Integer = keys(0)
        Dim rel As Integer = keys(1)
        'chiave della relazione in edit
        IdRel = rel
        
        Dim _oContentManager As New ContentManager()
        Operation = "Update"
        _oContentManager.Cache = False
        '1149
        KeyContent = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(key)
 
        LoadContentSummary()
 
        relContentType.LoadControl()
        Dim _oContentBoxValue As ContentBoxValue = _oContentManager.ReadContentBoxRelation(New ContentBoxIdentificator(IdRel))
        If Not _oContentBoxValue Is Nothing Then
            With _oContentBoxValue
                relContentType.SetSelectedValue(.KeyContentType.Id)
                ctlSiteArea.SetSelectedValue(.KeySiteArea.Id)
             
                txtIdFather.Text = .KeyTheme.Id
                Father.Text = ReadFatherTheme(.KeyTheme.Id)

                
                txtOrder.Text = .Order
                txtLink.Text = .Link
                If Not ddTarget.Items.FindByValue(.Target) Is Nothing Then
                    ddTarget.SelectedValue = .Target
                    CType(txtTarget.GetControl, TextBox).Style.Item("visibility") = "hidden"
                Else
                    ddTarget.SelectedValue = "(custom)"
                    txtTarget.Text = .Target
                    CType(txtTarget.GetControl, TextBox).Style.Item("visibility") = "visible"
                End If
                'languageSelector.DefaultValue = .KeyLanguage.Id
                lSelector.LoadLanguageValue(.KeyLanguage.Id)
                txtTitle.Text = .Title
                txtLaunch.Text = .Launch
            End With
        End If
        
        TypeControl = ControlType.Edit
        ShowRightPanel()
    End Sub

    Protected Sub UpdateRelation(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim oSiteAreaValue As SiteAreaValue
        
        Dim _oContentBoxValue As New ContentBoxValue()
        With _oContentBoxValue
            .KeyContent = KeyContent
            .IdBox = KeyBox.Id
            If txtIdFather.Text <> "" Then
                .KeyTheme = New ThemeIdentificator(CType(txtIdFather.Text, Integer))
            Else
                .KeyTheme = New ThemeIdentificator()
            End If
            
            Dim ctc As ContentTypeCollection = relContentType.GetSelection()
            If Not ctc Is Nothing AndAlso ctc.Count > 0 Then
                .KeyContentType.Id = ctc.Item(0).Key.Id
            End If

            oSiteAreaValue = GetSiteSelection(ctlSiteArea)
            If Not oSiteAreaValue Is Nothing Then
                .KeySiteArea = oSiteAreaValue.Key
            End If
            If txtOrder.Text = "" Then txtOrder.Text = "0"
            .Order = txtOrder.Text
            .Link = txtLink.Text
            If ddTarget.SelectedValue = "(custom)" Then
                .Target = txtTarget.Text
            Else
                .Target = ddTarget.SelectedValue
            End If
            '.KeyLanguage = New LanguageIdentificator(languageSelector.Language)
            .KeyLanguage.Id = lSelector.Language
            .Title = txtTitle.Text
            .Launch = txtLaunch.Text
        End With
        
        Dim _oContentManager As New ContentManager()
        'If Not _oContentManager.ReadContentBoxValue(KeyBox, KeyContent) Is Nothing Then
        
        If (Operation = "Update") Then
            'Response.Write("idRel" & IdRel)
            '_oContentManager.ReadContentBox()
            _oContentBoxValue.Key.Id = IdRel
            '_oContentManager.UpdateContentBox(_oContentBoxValue)
            If _oContentManager.UpdateContentBox(_oContentBoxValue) Then
                'strJs = "alert('Update succeeded!');"
            Else
                strJs = "alert('Error during update!');"
            End If
        Else
            If _oContentManager.CreateContentBox(_oContentBoxValue) Then
                'strJs = "alert('Save succeeded!');"
            Else
                strJs = "alert('Error during save!');"
            End If
        End If
        
        TypeControl = ControlType.List
        BindGrid(True)
        BindGridRelations(True)
        ShowRightPanel()
    End Sub
    
    Protected Sub removeRelation(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim keysRel As String = sender.commandArgument.ToString
        Dim keys As String() = keysRel.Split("|")
        
        Dim keyCont As Integer = keys(0)
        Dim keyRel As Integer = keys(1)
        
        Dim _oContentManager As New ContentManager()
        Dim _oContentBoxValue As New ContentBoxValue
        With _oContentBoxValue
            .Key.Id = keyRel
            '1149
            .KeyContent = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(keyCont)
            .IdBox = KeyBox.Id
            .KeyLanguage = New LanguageIdentificator(.KeyContent.IdLanguage)
        End With
        _oContentManager.RemoveContentBox(_oContentBoxValue)
        
        TypeControl = ControlType.List
        BindGrid(True)
        BindGridRelations(True)
        ShowRightPanel()
    End Sub
    
    Protected Sub gridRelations_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowIndex >= 0 Then
            e.Row.ID = "td_content_" & DataBinder.Eval(e.Row.DataItem, "Key.Id") & "_" & DataBinder.Eval(e.Row.DataItem, "KeyContent.Id") & "_" & DataBinder.Eval(e.Row.DataItem, "ContentLanguage") & "_" & DataBinder.Eval(e.Row.DataItem, "KeyContent.PrimaryKey")
            Dim img As Image = e.Row.Cells(0).FindControl("dragHandler")
            If Not img Is Nothing Then
                img.Attributes.Add("onmousedown", "makeDraggable(document.getElementById('" & e.Row.ClientID & "'));")
            End If
        End If
    End Sub
    
</script>
<script type="text/javascript" language="javascript" src="/HP3Office/Js/CallBackObject.js"></script>
<script type="text/javascript">

    // *** BEGIN DRAG & DROP CODE ***
//    var opacity = parseFloat(0.8);
    
    document.onmousemove = mouseMove;
    document.onmouseup   = mouseUp;
    
    var dragObject  = null;
    var mouseOffset = null;
    var targetRow   = null;
    var rowsTop     = null;
    var rowsId      = null;
    var inCallback = false;

    function getMouseOffset(target, e) {
	    e = e || window.event;

	    var docPos = getPosition(target);
	    var mousePos = mouseCoords(e);
	    return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
    }

    function getPosition(e) {
	    var left = 0;
	    var top  = 0;

	    while (e.offsetParent) {
		    left += e.offsetLeft;
		    top  += e.offsetTop;
		    e     = e.offsetParent;
	    }

	    left += e.offsetLeft;
	    top  += e.offsetTop;

	    return {x:left, y:top};
    }
    
    function mouseMove(e) {
	    e            = e || window.event;
	    var mousePos = mouseCoords(e);

	    if (dragObject) {
	        if (!rowsTop) init();
	        if (!targetRow) makeTargetRow(dragObject);
            targetRow.style.display = 'block';

            // controlla se spostare la riga target
            var table = getTable();
            var targetIndex = table.rows.length-1; //dragObject.rowIndex;
            var mouseY = 0;
            
            for (var i=1; i<=table.rows.length; i++) {
            
                mouseY = parseInt(parseInt(getPosition(dragObject).y)); 
                if (i == 1 && (mouseY <= rowsTop[i])) {
                    targetIndex = i;
                    break;
                } else if (i == table.rows.length && (mouseY >= rowsTop[i])) {
                    targetIndex = i;
                    break;
                } else if (rowsTop[i-1] <= mouseY && mouseY <= rowsTop[i]) {
                    targetIndex = i;
                    break;
                }
            }
            table.moveRow(targetRow.rowIndex, targetIndex);
	        
	        var tablePos = getPosition(getTable());
	    
		    dragObject.style.position = 'absolute';
		    dragObject.style.top      = mousePos.y - mouseOffset.y;
		    dragObject.style.left     = mousePos.x - mouseOffset.x;
		    
		    return false;
	    }
    }
    
    function mouseUp(e) {
        if (dragObject != null) {
            if (targetRow != null) {
                var table = getTable();
                table.removeChild(dragObject);
                table.appendChild(dragObject);

                var targetRowIndex = targetRow.rowIndex;
                table.moveRow(targetRow.rowIndex, table.rows.length - 1);
                table.moveRow(dragObject.rowIndex, targetRowIndex);
                targetRow.style.display = 'none';
		        dragObject.style.position = '';

                alternateRowStyle();
                makeIdArray();
                reorderItems();
            }
		    //dragObject.cells[4].style.display = 'block';
            dragObject.onmousedown = null;
	        dragObject = null;
	    }
    }

    function mouseCoords(e) {
	    if (e.pageX || e.pageY) {
		    return { x:e.pageX, y:e.pageY };
	    }
	    return {
		    x:e.clientX + document.body.scrollLeft - document.body.clientLeft,
		    y:e.clientY + document.body.scrollTop  - document.body.clientTop
	    };
    }
    
    function makeDraggable(item) {
	    if (!item || inCallback) return;
	    item.onmousedown = function(e) {
		    dragObject  = this;
		    dragObject.style.border = 'solid 1px #eeeeee';
		    //dragObject.cells[4].style.display = 'none';
		    mouseOffset = getMouseOffset(this, e);
		    return false;
	    }
    }
    
    function getTable() {
        var table = document.getElementById('<%=gridRelations.clientId %>');
        var tbody = table.getElementsByTagName('tbody').item(0);
        return tbody;
    }
    
    function init() {
        var table = getTable();
        rowsTop = new Array(table.rows.length);
        
        for (var i=1; i<table.rows.length; i++) {
            rowsTop[i] = getPosition(table.rows[i]).y;
        }
        makeIdArray();
    }
    
    function makeIdArray() {
        var table = getTable();
        rowsId = new Array(table.rows.length - 2);
        var j = 0;
        for (var i=1; i<table.rows.length; i++) {
            if (table.rows[i].pk != 'target_row') {
                rowsId[j++] = Trim(table.rows[i].cells[3].innerHTML);
            }
        }
    }
    
    function makeTargetRow() {
        var table = getTable();
        targetRow = document.createElement('tr');
        targetRow.style.border = 'solid 1px #eeeeee';
        targetRow.pk = 'target_row';
        var td = document.createElement('td');
        td.colSpan = getTable().parentElement.rows[0].cells.length;
        var div = document.createElement('div');
        div.style.width = '100%';
        div.style.height = '17px';
        div.style.margin = '0px';
        div.style.border = 'dotted 1px #ff0000';
        
        td.appendChild(div);
        targetRow.appendChild(td);
        table.appendChild(targetRow);
    }
    
    function alternateRowStyle() {
        var table = getTable();
        var color = '#ffffff';
        for (var i = 0; i < table.rows.length; i++) {
            if (i % 2 == 0) {
                var color = '#eeefef';
            } else {
                var color = '#ffffff';
            }
            table.rows[i].style.backgroundColor = color;
        }
        targetRow.style.backgroundColor = '#ffffff';
    }
    
    function getCbParams() {
        var params = "action=reorderItems"
        params += "&items=";
        for (var i=0; i<rowsId.length; i++) {
            params += rowsId[i] + ((i<rowsId.length-1)?',':'');
        }
        return params;
    }
    
    function reorderItems() {
        inCallback = true;
        WebForm_DoCallback('<%=Me.clientId() %>',getCbParams(),callbackReorderItems,null,callbackErrorReorderItems,true)
    }
    
    function callbackReorderItems(result, context) {
        inCallback = false;
    }
    
    function callbackErrorReorderItems(result, context) {
        inCallback = false;
    }
    
    // *** END DRAG & DROP CODE ***
    
    
    function enableCustomTarget(select) {
        var obj = document.getElementById('<%=txtTarget.clientId() %>');
        if (!obj) return;
        
        if (select.value == '(custom)') {
            obj.style.visibility = 'visible';
        } else {
            obj.style.visibility = 'hidden';
        }
    }

    <%=strJs %>
    
    
    function ClearTheme() {
        document.getElementById('<%=txtIdFather.clientId() %>').value = '';
        document.getElementById('<%=Father.clientId() %>').value = '';
    }
</script>
<hr />
<asp:Panel ID="pnlHeader" runat="server">
    <asp:Button ID="btBackToList" Text="Archive" CssClass="button" runat="server" OnClick="BackToList" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    <asp:Button ID="btBackToBoxManager" Text="Box Manager" CssClass="button" runat="server" OnClick="BackToBoxManager" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/box_manager.gif');background-repeat:no-repeat;background-position:1px 1px;" />
    
</asp:Panel>
<hr />
<asp:Panel ID="pnlContentBox" runat="server">
    <asp:Panel ID="BoxSummary" Width="100%" runat="server">
 

        <div class="title">Box Selected</div>
            <br />
 
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:30%">Name</th>
                <th>Description</th>
                <th style="width:20%">Type</th>
                <th style="width:5%">Active</th>
                
            </tr>
            <tr>
                <td><asp:Label ID="lblBoxId" runat="server" /></td>
                <td><asp:Label ID="lblBoxName" runat="server" /></td>
                <td><asp:Label ID="lblBoxDescription" runat="server" /></td>
                <td><asp:Label ID="lblBoxType" runat="server" /></td>
                <td><asp:Label ID="lblBoxActive" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    
    <asp:Panel ID="pnlContentSummary" Width="100%" runat="server">
        <div class="title">Content Selected</div>
     <br />
        <table class="tbl1">
            <tr>
                <th  style="width:5%">Id</th>
                <th style="width:20%">Date Publish</th>
                <th style="width:5%"></th>
                <th style="width:70%">Title</th>
                
            </tr>
            <tr>
                <td><asp:Label ID="lblContentId" runat="server" /></td>
                <td><asp:Label ID="lblContentDatePublish" runat="server" /></td>
                <td><asp:Image ID="imgContentLanguage" runat="server" /></td>
                <td><asp:Label ID="lblContentTitle" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
    <!--INIZIO modifiche luca per ricerca tra relazioni-->
    <asp:Panel ID="pnlSearchRelation" cssclass="boxSearcher" Width="100%" Visible="false" runat="server"  >
        <table class="topContentSearcher">
        <tr>
        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Relation Searcher</span></td>
        </tr>
        </table>
        <asp:Table ID="tableSearchFromReltion" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>ContentId</asp:TableCell>
                <asp:TableCell>Domain</asp:TableCell>
                <asp:TableCell>SiteArea</asp:TableCell>   
            </asp:TableRow>
            <asp:TableRow >
                <asp:TableCell><HP3:ctlText ID="txtContentId" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><asp:DropDownList id="dwlDomain" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="dwlDomain_IndexChanged"/></asp:TableCell>
                <asp:TableCell><HP3:ctlSite runat="server" ID="ctlSiteAreaRel" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchFromReltion2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchFromRelation" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <!--FINE modifiche luca per ricerca tra relazioni-->
    <br />
    <asp:Panel ID="pnlActiveRelations" Width="100%" Visible="true" runat="server">
 
    <asp:Label runat="server" ID="msgRelation"/>
        <div class="title">Active Relations</div>
            <br />
             
            <asp:GridView ID="gridRelations"
                AutoGenerateColumns="false"  AllowSorting="false"
                ShowHeader="true" HeaderStyle-CssClass="header"
                ShowFooter="false"
                RowStyle-BackColor="#ffffff" AlternatingRowStyle-BackColor="#eeefef"
                GridLines="None" width="100%"
                OnRowDataBound="gridRelations_RowDataBound"
                runat="server" >
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="1%" ItemStyle-Width="1%">
                        <ItemTemplate>
                            <asp:Image ID="dragHandler" ImageUrl="~/hp3Office/HP3Image/Ico/dragHandler.gif" ToolTip="Drag this item to change order" style="cursor:move;" runat="server" /> 
                        </ItemTemplate>
                    </asp:TemplateField>
                   <%-- <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                   <ItemTemplate>
                        <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                    </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <a title="Id"><asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContent.Id")%>' /></a>
                            <a title="GlobalKey"><asp:Literal ID="lblGlobalK" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContent.IdGlobal")%>' /></a>
                            <a title="PrimaryKey"><asp:Literal ID="lblPk" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KeyContent.PrimaryKey")%>' /></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "ContentLanguage"))%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
<%--  TODO:         Non � possibile visualizzare la colonna order per il momento perch� il bind della gridVire
                    � stato effettuato usando una ContentCollection e non una ContentBoxCollection (se c'�...)    
                    <asp:TemplateField HeaderText="#" AccessibleHeaderText="Order" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                        <ItemTemplate>
                            <asp:Literal ID="lblOrder" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Order")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
--%>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ContentTitle")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="5%">
                        <ItemTemplate>
                            <asp:ImageButton ID="btn_update" ToolTip ="Edit relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" onClick="EditRelation" CommandArgument ='<%#Container.Dataitem.KeyContent.PrimaryKey & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>' CommandName ="EditRelation" />
                            <asp:ImageButton ID="btn_del" ToolTip="Remove relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/content_delete.gif" OnClientClick="return confirm('Are you sure to remove current relation?')" onClick="removeRelation" CommandArgument ='<%#Container.Dataitem.KeyContent.PrimaryKey & "|" & DataBinder.Eval(Container.DataItem, "Key.Id")%>' CommandName ="Remove" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                <br />
                    No relations found. 
                </EmptyDataTemplate>
            </asp:GridView> 
        </asp:Panel>
    <hr />

    <asp:Panel ID="pnlRelatedSearch" cssclass="boxSearcher" Width="100%" Visible="true" runat="server">
    <table class="topContentSearcher">
    <tr>
    <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
    <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
    </tr>
    </table>
        <asp:Table ID="tableSearchParameters" CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell>Id</asp:TableCell>
                <asp:TableCell>GlobalKey</asp:TableCell>
                <asp:TableCell>PrimaryKey</asp:TableCell>       
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="txtDataValueFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtGlobalKeyFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
                <asp:TableCell><HP3:ctlText ID="txtPrimaryKeyFilter" runat="server" TypeControl="NumericText" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Title</asp:TableCell>
                <asp:TableCell>Content Type</asp:TableCell>
                <asp:TableCell>Language</asp:TableCell>
                <asp:TableCell>SiteArea</asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell><HP3:ctlText ID="txtDataTextFilter" runat="server" typecontrol="TextBox" /></asp:TableCell>
                <asp:TableCell><HP3:ctlContentType ID="ContentType" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></asp:TableCell>
                <asp:TableCell><HP3:ctlLanguage ID="lSelectorLanguage" runat="server"  Typecontrol="WithSelectControl" /></asp:TableCell> 
                <asp:TableCell><HP3:ctlSite runat="server" ID="ctlSiteC" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></asp:TableCell>
            </asp:TableRow>
         </asp:Table>
         <asp:Table ID="tableSearchParameters2"  CssClass="form" runat="server">
            <asp:TableRow>
                <asp:TableCell><asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button" Text="Search" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /> </asp:TableCell>
                <asp:TableCell><asp:Button ID="btnClearSearch" Enabled="true" CssClass="button" Text="Reset" runat="server" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_reset.gif');background-repeat:no-repeat;background-position:1px 1px;" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <hr />
    <asp:Panel ID="pnlGridContents" Width="100%" Visible="true" runat="server">
    <asp:Label runat="server" ID="msg"/>
        <asp:GridView ID="gridContents" 
            AutoGenerateColumns="false"
            AllowPaging="true" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right" PagerStyle-CssClass="Pager2" PageSize="20"
            
            AllowSorting="false"
            ShowHeader="true" HeaderStyle-CssClass="header"
            ShowFooter="true" FooterStyle-CssClass="gridFooter"
            
            GridLines="None" AlternatingRowStyle-BackColor="#eeefef" width="100%"
            runat="server">
            
            <Columns>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <a title="Id"><asp:Literal ID="lblId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'  /></a>
                        <a title="GlobalKey"><asp:Literal ID="lblIdGlobal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.IdGlobal")%>' /></a>
                        <a title="PrimaryKey"><asp:Literal ID="lblPrimaryKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%>' /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img alt="Language: <%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="2%">
                    <ItemTemplate>
                        <asp:ImageButton ID="btn_add" ToolTip="Add content relation" runat="server" ImageUrl="~/hp3Office/HP3Image/Ico/insert.gif" onClick="addRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    
    <asp:Panel id="pnlRelationEdit" runat="server">
        <table class="form" style="width:100%">
            <tr>
                <td>
                    <asp:button ID="btnSave" runat="server" CssClass="button" Text="Save" onclick="UpdateRelation" style="padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
                </td>
            </tr>
            <tr>
                <td>Domain</td>
                <td><asp:DropDownList id="dwlDomainContentEdit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dwlDomainContentEdit_IndexChanged" /></td>
            </tr>
             <tr>
                <td  style="width:20%" valign="top">Content Type</td>
                <td><HP3:ctlContentType ID="relContentType" TypeControl="combo" ItemZeroMessage="Select ContentType" runat="server" /></td>
            </tr>
            <tr>
                <td>Domain</td>
                <td><asp:DropDownList id="dwlDomainEdit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dwlDomainEdit_IndexChanged" /></td>
            </tr>
            <tr>
                <td style="width:20%" valign="top">Site Area</td>
                <td><HP3:ctlSite runat="server" ID="ctlSiteArea" TypeControl="combo" SiteType="Area" ItemZeroMessage="Select Area"/></td>
            </tr>
            <tr>
                <td style="width:20%" valign="top">Theme</td>
                <td>                
                    <table width="100%">
                        <tr>
                            <td width="50px"><asp:TextBox ID="txtIdFather" runat="server" style="display:none"/><HP3:ctlText runat ="server" ID="Father"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                            <td>
                                <input type="button" class="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popThemes.aspx?SelItem=' + document.getElementById('<%=txtIdFather.clientid%>').value + '&ctlHidden=<%=txtIdFather.clientid%>&ctlVisible=<%=Father.clientid%>','','width=780,height=480,scrollbars=yes')" />
                                <input type="button" class="button" value="clear" onclick="ClearTheme()" />
                            </td>        
                        </tr>
                    </table>    
                </td>
            </tr>            
            <tr>
                <td style="width:20%" valign ="top">Order</td>
                <td><HP3:ctlText runat="server" ID="txtOrder" TypeControl="TextBox" MaxLength="5" style="width:40px" /></td>
            </tr>
            <tr>
                <td>Link</td>
                <td><HP3:ctlText runat="server" ID="txtLink" TypeControl="TextBox" style="width:400px" /></td>
            </tr>
            <tr>
                <td>Target</td>
                <td>
                <table>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddTarget"  OnChange="enableCustomTarget(this);" runat="server">
                        <asp:ListItem Text="_blank" Value="_blank" Selected="True" />
                        <asp:ListItem Text="_parent" Value="_parent" />
                        <asp:ListItem Text="_search" Value="_search" />
                        <asp:ListItem Text="_self" Value="_self" />
                        <asp:ListItem Text="_top" Value="_top" />
                        <asp:ListItem Text="(none)" Value="" />
                        <asp:ListItem Text="(custom)" Value="(custom)" />
                        </asp:DropDownList>
                    </td>
                    <td><HP3:ctlText runat="server" ID="txtTarget" TypeControl="TextBox" style="visibility: hidden; width:200px" /></td>
                </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>Language</td>
                <td><HP3:ctlLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
            </tr>
            <tr>
                <td>Title</td>
                <td><HP3:ctlText runat="server" ID="txtTitle" TypeControl="TextBox" style="width:400px" /></td>
            </tr>
            <tr>
                <td>Launch</td>
                <td><HP3:ctlText runat="server" ID="txtLaunch" TypeControl="TextArea" style="width:400px" /></td>
            </tr>
            
        </table>
        </asp:Panel>
</asp:Panel>