<%@ Control Language="VB" ClassName="ctlBoxManager" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="server">
    Private _strDomain As String
    Private _language As String
    
    Private linkLabel As String = Nothing
    Private strJS As String
    
    Public Property SelectedId() As Integer
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Integer)
            ViewState("selectedId") = value
        End Set
    End Property
           
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
         
    Sub Page_load()
        Language() = Me.BusinessMasterPageManager.GetLang.Id()
        Domain() = WebUtility.MyPage(Me).DomainInfo
    
        If Not (Page.IsPostBack) Then
            ctlSiteArea.LoadControl()
            txtSite.LoadControl()
            LoadArchive()
        End If
    End Sub
    
    Sub LoadArchive()
        Dim dlGroup As Integer = ReadContextGroup("HP3", "directlink")
        
        If (dlGroup <> 0) Then
            Dim cxtSearcher As New ContextSearcher
            cxtSearcher.KeyContextGroup.Id = dlGroup
            
            Me.BusinessContextManager.Cache = False
            Dim coll As ContextCollection = Me.BusinessContextManager.Read(cxtSearcher)
            coll.Sort(ContextGenericComparer.SortType.ById, ContextGenericComparer.SortOrder.DESC)
            gridList.DataSource = coll
            gridList.DataBind()
        End If
        
        ActiveGridPanel()
    End Sub
    
    Function ReadContextGroup(ByVal domain As String, ByVal name As String) As Integer
        Dim cxtGroupIdent As New ContextGroupIdentificator(domain, name)
        Dim cxtGroupValue As ContextGroupValue = Me.BusinessContextManager.ReadGroup(cxtGroupIdent)
        If Not (cxtGroupValue Is Nothing) Then
            Return cxtGroupValue.Key.Id
        End If
        
        Return 0
    End Function
           
    
    Sub SaveItem(ByVal sender As Object, ByVal ev As EventArgs)
        Dim cxtValue As New ContextValue
        cxtValue.Key.KeyName = dlFolder.Text
        cxtValue.Description = dlDescription.Text
        cxtValue.Text = dlLink.Text
        cxtValue.Key.Language = Me.BusinessMasterPageManager.GetLang
        
        Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(ctlSiteArea)
        If Not oSiteAreaValue Is Nothing Then
            cxtValue.KeySiteArea.Id = oSiteAreaValue.Key.Id
        End If
        
        If Not (IsEmpty(cxtValue)) Then
            If (SelectedId = 0) Then
                cxtValue = Me.BusinessContextManager.Create(cxtValue)
                If (cxtValue.Key.Id <> 0) Then
                    Dim cxtGIndent As New ContextGroupIdentificator(ReadContextGroup("HP3", "directlink"))
                    Me.BusinessContextManager.CreateGroupContextRelation(cxtGIndent, cxtValue.Key)
                End If
          
            Else
                cxtValue.Key.Id = SelectedId
                Me.BusinessContextManager.Update(cxtValue)
            End If
            LoadArchive()
            
        Else
            CreateJsMessage("The fields can not be empty!")
        End If
    End Sub
    
    Sub EditItem()
        If (SelectedId <> 0) Then
            
            Me.BusinessContextManager.Cache = False
            Dim cxtValue As ContextValue = Me.BusinessContextManager.Read(New ContextIdentificator(SelectedId))
            LoadDett(cxtValue)
        End If
    End Sub
    
    Sub RemoveItem()
        If (SelectedId <> 0) Then
            Dim cxtGIndent As New ContextGroupIdentificator(ReadContextGroup("HP3", "directlink"))
            Dim bool As Boolean = Me.BusinessContextManager.RemoveGroupContextRelation(cxtGIndent, New ContextIdentificator(SelectedId))
            
            If (bool) Then
                Me.BusinessContextManager.Delete(New ContextIdentificator(SelectedId))
            End If
        End If
    End Sub
    
    Sub LoadDett(ByVal value As ContextValue)
        If Not (value Is Nothing) Then
            dlFolder.Text = value.Key.KeyName
            dlDescription.Text = value.Description
            dlLink.Text = value.Text
            ctlSiteArea.SetSelectedValue(value.KeySiteArea.Id)
        Else
            dlFolder.Text = Nothing
            dlDescription.Text = Nothing
            dlLink.Text = Nothing
            ctlSiteArea.SetSelectedIndex(0)
        End If
    End Sub
    
    Function IsEmpty(ByVal cxtValue As ContextValue) As Boolean
        If ((cxtValue.Key.KeyName <> String.Empty) And (cxtValue.Text <> String.Empty) And (cxtValue.KeySiteArea.Id <> 0)) Then
            Return False
        End If
    
        Return True
    End Function
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        
        Return Nothing
    End Function
    
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
        

    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSearcher As New ContextSearcher
        If txtId.Text <> "" Then oSearcher.Key.Id = txtId.Text
        If txtDescr.Text <> "" Then oSearcher.Description = txtDescr.Text

        Dim oSiteAreaValue As SiteAreaValue = GetSiteSelection(txtSite)
        If Not oSiteAreaValue Is Nothing Then
            oSearcher.KeySiteArea = oSiteAreaValue.Key
        End If
        
        Dim dlGroup As Integer = ReadContextGroup("HP3", "directlink")
        If (dlGroup <> 0) Then
            oSearcher.KeyContextGroup.Id = dlGroup
        End If
        
        BindGrid(objGrid, oSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContextSearcher = Nothing)
        Dim coll As New ContextCollection
        
        Me.BusinessContextManager.Cache = False
        coll = Me.BusinessContextManager.Read(Searcher)
   
        coll.Sort(ContextGenericComparer.SortType.ById, ContextGenericComparer.SortOrder.DESC)
        objGrid.DataSource = coll
        objGrid.DataBind()
    End Sub
    
    Sub OnItemSelected(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim id As Integer = sender.CommandArgument
        
        Select Case sender.CommandName
            Case "SelectItem"
                SelectedId = id
                EditItem()
                ActiveDettPanel()
            Case "DeleteItem"
                SelectedId = id
                RemoveItem()
                LoadArchive()
        End Select
    End Sub
    
    Sub NewItem(ByVal s As Object, ByVal e As EventArgs)
        SelectedId = 0
        LoadDett(Nothing)
        ActiveDettPanel()
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        LoadArchive()
    End Sub
     
    Sub DynamicCreation(ByVal sender As Object, ByVal ev As EventArgs)
        'newLink.Disable()
        ActiveDynamicPanel()
    End Sub
    
    Sub CreateLink(ByVal sender As Object, ByVal ev As EventArgs)
        Dim siteLink As String = String.Empty

        Dim siteId As Integer = Request("S")
        If (oldLink.Text <> String.Empty) And (Request("S") <> Nothing) Then
            Dim siteV As SiteValue = Nothing
            siteV = Me.BusinessSiteManager.Read(New SiteIdentificator(siteId))
           
            If Not (siteV Is Nothing) Then
                Dim randGen As New Random(DateTime.Now.Millisecond)
                Dim int As Integer = randGen.Next(5, 8)
                Dim redFolder As String = SimpleHash.GenerateUUID(int)
                         
                txtSiteId.Text = siteId.ToString
                newLink.Text = siteV.Domain & "/" & redFolder
                shortLink.Visible = True
                btnCreateL.Visible = False
                newLink.Disable()
            End If
        Else
            CreateJsMessage("Select a Site or Insert a long URL!")
        End If
    End Sub
    
    Sub SaveDirectLink(ByVal sender As Object, ByVal ev As EventArgs)
        Dim cxtValue As New ContextValue
      
        If (newLink.Text <> String.Empty) Then
            Dim redLink As String() = newLink.Text.Split("/")
           
            If Not (Exist(txtSiteId.Text, redLink(1))) Then
                cxtValue.Key.KeyName = redLink(1)
                cxtValue.Description = "Dynamic Link: " & " " & redLink(0) & "/" & redLink(1)
                cxtValue.Text = oldLink.Text
                cxtValue.Key.Language = Me.BusinessMasterPageManager.GetLang
                If (txtSiteId.Text <> String.Empty) Then cxtValue.KeySiteArea.Id = CType(txtSiteId.Text, Integer)
       
                cxtValue = Me.BusinessContextManager.Create(cxtValue)
                oldLink.Text = Nothing
                newLink.Text = Nothing
                shortLink.Visible = False
                btnCreateL.Visible = True
                If (cxtValue.Key.Id <> 0) Then
                    Dim cxtGIndent As New ContextGroupIdentificator(ReadContextGroup("HP3", "directlink"))
                    Me.BusinessContextManager.CreateGroupContextRelation(cxtGIndent, cxtValue.Key)
                End If
            Else
                CreateJsMessage("This folder already exist for this site domain!Please, generate a new redirec link.")
            End If
        End If
        
        LoadArchive()
    End Sub
    
    Function getRedLink(ByVal ctxV As ContextValue) As String
        Dim site As SiteValue = Me.BusinessSiteManager.Read(ctxV.KeySiteArea.Id)
        
        If Not (site Is Nothing) Then
            'linkLabel = site.Domain & "/" & ctxV.Key.KeyName
            Return "http://" & site.Domain & "/" & ctxV.Key.KeyName
        End If
        
        Return Nothing
    End Function
    
    Function Exist(ByVal idSite As String, ByVal newfolder As String) As Boolean
        Dim cxtSearcher As New ContextSearcher()
        cxtSearcher.KeySiteArea.Id = CType(idSite, Integer)  'identifica il Sito
        cxtSearcher.Key.KeyName = newfolder         'identifica la folder
       
        Me.BusinessContextManager.Cache = False
        Dim cxtColl As ContextCollection = Me.BusinessContextManager.Read(cxtSearcher)

        If Not (cxtColl Is Nothing) AndAlso (cxtColl.Count > 0) Then
            Return True
        End If
        
        Return False
    End Function
        
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveGridPanel()
        pnlGrid.Visible = True
        pnlDett.Visible = False
        pnlDynamic.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActiveDettPanel()
        pnlDett.Visible = True
        pnlGrid.Visible = False
        pnlDynamic.Visible = False
    End Sub
    
    Sub ActiveDynamicPanel()
        pnlDett.Visible = False
        pnlGrid.Visible = False
        pnlDynamic.Visible = True
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.BusinessMasterPageManager.GetLang.Id, description)
    End Function
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
</script>

<script type ="text/javascript" >
   <%=strJS%>
</script>

<asp:Panel id="pnlGrid" runat="server">
<hr />
<asp:button cssclass="button" ID="btnNew" runat="server" Text="New" onclick="NewItem" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>  
<asp:button cssclass="button" ID="btnDynamic" runat="server" Text="Short Link Generation" onclick="DynamicCreation" style="margin-bottom:10px;padding-left:5px;"/>  
    
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Descrition</strong></td>
                    <td><strong>Site</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                    <td><HP3:Text runat="server" ID="txtDescr" TypeControl ="TextBox" style="width:200px"/></td>  
                    <td><HP3:ctlSite runat="server" ID="txtSite" TypeControl="combo" SiteType="Site" ItemZeroMessage="Select Site"/></td>
                </tr>
                <tr>
                    <td><asp:button cssclass="button" runat="server" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
           </table>
        </fieldset>
        <hr />
    </asp:Panel>
    
    <asp:gridview ID="gridList" 
        runat="server"
        AutoGenerateColumns="false" 
        GridLines="None" 
        CssClass="tbl1"
        HeaderStyle-CssClass="header"
        AlternatingRowStyle-BackColor="#E9EEF4"
        AllowPaging="true"
        PagerSettings-Position="TopAndBottom"  
        PagerStyle-HorizontalAlign="Right"
        PagerStyle-CssClass="Pager"             
        AllowSorting="true"
        OnPageIndexChanging="gridList_PageIndexChanging"
        PageSize ="20"
        emptydatatext="No item available">
            <Columns >
                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Folder">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.KeyName")%></ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Short Link">
                    <ItemTemplate><%--<a href='<%#getRedLink(Container.DataItem)%>'  target="_blank" title=""></a>--%><%#getRedLink(Container.DataItem)%></ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Redirect Link">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Text")%></ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderStyle-Width="5%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/HP3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/HP3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server" Visible="false">
    <div>
        <asp:button cssclass="button" ID="btnList" runat="server" Text="Archive"  onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
        <asp:button cssclass="button" runat="server" ID="btnSave"  Text="Save"  onclick="SaveItem" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>   
    <span id="lblId" class="title" runat ="server"/>
    <span id="lblDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFolder&Help=cms_HelpFolderName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFolder", "Folder Name")%></td>
            <td><HP3:Text runat ="server" ID="dlFolder" TypeControl ="TextBox"  MaxLength="30" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="dlDescription" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRedLink&Help=cms_HelpRedLink&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRedLink", "Redirect Link")%></td>
            <td><HP3:Text runat ="server" ID="dlLink" TypeControl ="TextArea" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormArea&Help=cms_HelpArea&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormArea", "Area")%></td>
            <td><HP3:ctlSite runat="server" ID="ctlSiteArea" TypeControl="combo" SiteType="Site" ItemZeroMessage="Select Site"/></td>
        </tr>
     </table>
</asp:Panel>

<asp:Panel id="pnlDynamic" runat="server" Visible="false">
    <hr/>
     <table  class="form" style="width:100%">
        <tr>
             <td><HP3:Text runat ="server" id="txtSiteId" TypeControl="TextBox" visible="false"/></td>
        </tr>
        <tr>
            <td style="width:100px" ><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRedLink&Help=cms_HelpRedLink&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRedLink", "Long URL")%></td>
            <td><HP3:Text runat ="server" ID="oldLink" TypeControl ="TextBox"  style="float:left;width:300px"/>&nbsp;&nbsp;
            <asp:button cssclass="button"  ID="btnCreateL" runat="server" Text="Generate Link"  onclick="CreateLink" style="margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/import_export.PNG');background-repeat:no-repeat;background-position:1px 1px;" /></td>
        </tr>
      </table>
     
     <div id="shortLink" runat="server"  visible="false">
        <table  class="form" style="width:84%">
           <tr>
            <td style="width:100px"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormShortLink&Help=cms_HelpShortLink&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_ShortLink", "Short URL")%></td>
            <td><HP3:Text runat ="server" ID="newLink" TypeControl ="TextBox"  style="float:left;width:300px"/>&nbsp;&nbsp;
            <asp:button cssclass="button"  ID="btnConfirm" runat="server" Text="Confirm"  onclick="SaveDirectLink" style="margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;" /></td>
        </tr>
        </table>
     </div>
</asp:Panel>