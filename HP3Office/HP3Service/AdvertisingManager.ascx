<%@ Control Language="VB" ClassName="BannerManager" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues.SiteAreaCollection" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlBox" Src ="~/hp3Office/HP3Parts/ctlBox.ascx"%>
<%@ Register tagprefix="HP3" TagName ="ctlContentType" Src ="~/hp3Office/HP3Parts/ctlContentType.ascx"%>
<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx" %>

<script runat="server">
    Private oAdvManager As AdvertisingManager
    Private oAdvSearcher As BannerSearcher
    Private oAdvCollection As BannerCollection
    Private oGenericUtlity As New GenericUtility
    
    Private _sortType As BannerGenericComparer.SortType
    Private _sortOrder As BannerGenericComparer.SortOrder
    
    Private objCMSUtility As New GenericUtility
    Private dateUtility As New StringUtility
    
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    
    Private strDomain As String
    Private _strDomain As String
    Private _language As String
    
    Public Enum ControlType
        Edit = 0
        Selection
        View
    End Enum
    
    Private _typecontrol As ControlType = ControlType.View
    
    Public Property SelectedIdBanner() As Int32
        Get
            Return ViewState("idbanner")
        End Get
        Set(ByVal value As Int32)
            ViewState("idbanner") = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub

    Sub BindFilerObject()
        Site.LoadControl()
        Area.LoadControl()
        ContentType.LoadControl()
        Box.LoadControl()
    End Sub

    Function LoadBannerValue() As BannerValue
        If SelectedIdBanner <> 0 Then
            oAdvManager = New AdvertisingManager
            oAdvManager.Cache = False
            Return oAdvManager.Read(SelectedIdBanner)
        End If
        Return Nothing
    End Function
    
    ''' <summary>
    ''' Cancella fisicamente il banner selezionato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
   
    ''AMa #2310 17/11/2011  
    'aggiunta del controllo se ci sono site aree e siti associati al banner
    Function DeleteBanner() As Boolean
        Dim ret As Boolean = False
        If SelectedIdBanner <> 0 Then
            oAdvManager = New AdvertisingManager
            Dim keyBanner As New BannerIdentificator
            keyBanner.Id = SelectedIdBanner
            ret = oAdvManager.Remove(keyBanner)
            If ret Then
                'If Not SiteAreaCollection.SiteAreaType.Site Is Nothing AndAlso SiteAreaCollection.SiteAreaTypeThen Then
                Dim areas As SiteAreaCollection = oAdvManager.ReadBannerSiteArea(keyBanner)
                If Not areas Is Nothing AndAlso areas.Count > 0 Then
                    areas = areas.FilterBySiteAreaType(SiteAreaType.Area)
                    
                    If Not areas Is Nothing AndAlso areas.Count > 0 Then
                        oAdvManager.RemoveAllBannerSiteArea(New BannerIdentificator(SelectedIdBanner), SiteAreaType.Area)
                    End If
                    
                    Dim sites As SiteAreaCollection = areas.FilterBySiteAreaType(SiteAreaType.Site)
                    If Not sites Is Nothing AndAlso sites.Count > 0 Then
                        oAdvManager.RemoveAllBannerSiteArea(New BannerIdentificator(SelectedIdBanner), SiteAreaType.Site)
                    End If
                End If
              
                oAdvManager.RemoveAllBannerContentType(keyBanner)
                oAdvManager.RemoveAllBannerBox(keyBanner)
            End If
        End If
        Return ret
    End Function
    
    ''' <summary>
    ''' Recupera le aree associate al banner
    ''' </summary>
    ''' <param name="idBanner"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBannerSiteAreaValue(ByVal idBanner As Int32) As SiteAreaCollection
        If idBanner = 0 Then Return Nothing
        oAdvManager = New AdvertisingManager
        Dim keyBanner As New BannerIdentificator
        keyBanner.Id = idBanner
        oAdvManager.Cache = False
        Return oAdvManager.ReadBannerSiteArea(keyBanner)
    End Function
    
    ''' <summary>
    ''' Recupera i ContentType associati al banner
    ''' </summary>
    ''' <param name="idBanner"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBannerContentTypeValue(ByVal idBanner As Int32) As ContentTypeCollection
        If idBanner = 0 Then Return Nothing
        oAdvManager = New AdvertisingManager
        Dim keyBanner As New BannerIdentificator
        keyBanner.Id = idBanner
        oAdvManager.Cache = False
        Return oAdvManager.ReadBannerContentType(keyBanner)
    End Function
    
    ''' <summary>
    ''' Recupera i Box associati al banner
    ''' </summary>
    ''' <param name="idBanner"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBannerBoxValue(ByVal idBanner As Int32) As BoxCollection
        If idBanner = 0 Then Return Nothing
        oAdvManager = New AdvertisingManager
        Dim keyBanner As New BannerIdentificator
        keyBanner.Id = idBanner
        oAdvManager.Cache = False
        Return oAdvManager.ReadBannerBox(keyBanner)
    End Function
    
   
    'AMa #2310 17/11/2011 
    'aggiunta del controllo se ci sono site aree e siti associati al banner
    
    Sub LoadFormDett(ByVal oBannerValue As BannerValue)
        If Not oBannerValue Is Nothing Then
            With oBannerValue
                ' Caricamento BOXC
             
                BannerDescrizione.InnerText = " - " & .Description
                BannerId.InnerText = "Banner ID: " & .KeyBanner.Id
                
                'Associa al controllo di tipo Site le aree e i siti correlati al banner
              
                Dim siteCollBan As New SiteAreaCollection
                oSite.Clear()
                siteCollBan = GetBannerSiteAreaValue(.KeyBanner.Id)
               
                If Not siteCollBan Is Nothing AndAlso siteCollBan.Count > 0 Then
                    oSite.GenericCollection = siteCollBan.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Site)
                    oSite.LoadControl()
                End If
               
                Dim areaCollBanner As New SiteAreaCollection
                
                oAree.Clear()
               
                areaCollBanner = GetBannerSiteAreaValue(.KeyBanner.Id)
   
                If Not areaCollBanner Is Nothing AndAlso areaCollBanner.Count > 0 Then
                    oAree.GenericCollection = areaCollBanner.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Area)
                    oAree.LoadControl()
                   
                End If
                
                oContentType.GenericCollection = GetBannerContentTypeValue(.KeyBanner.Id)
                oContentType.LoadControl()
                oBox.GenericCollection = GetBannerBoxValue(.KeyBanner.Id)
                oBox.LoadControl()
                'Carica i controlli
                
               
                If .DateStart.HasValue Then
                    DateStart.Value = .DateStart.Value
                
                End If
               
                   
                If .DateEnd.HasValue Then
                    DateEnd.Value = .DateEnd.Value
               
                End If
                
                
                
                
                Description.Text = .Description
                Link.Text = .Link
                Alt.Text = .Alt
                'lSelector.DefaultValue = .KeyLanguage.Id
                lSelector.LoadLanguageValue(.KeyLanguage.Id)
                Name.Text = .Name
                txtType.Text = .Type
                chkPrelogin.Checked = .Prelogin
                Source.Text = .Source
                TImpression.Text = .TotalImpression
                CImpression.Text = .CountImpression
                TCTR.Text = .TotalCTR
                CCTR.Text = .CountCTR

                'anteprima banner
               
                anteprimaBanner.InnerHtml = oBannerValue.GetBanner(False)
                
                If anteprimaBanner.InnerHtml <> "" Then
                    preview.Visible = True
                End If
             
               
            End With
        Else
            oSite.LoadControl()
            oAree.LoadControl()
            oContentType.LoadControl()
            oBox.LoadControl()
            BannerDescrizione.InnerText = "New Banner"
            BannerId.InnerText = ""
            'lSelector.DefaultValue = objCMSUtility.ReadLanguage(WebUtility.MyPage(Me).idCurrentLang)
            DateStart.Clear()
            DateEnd.Clear()
            Description.Text = Nothing
            Link.Text = Nothing
            Alt.Text = Nothing
            Name.Text = Nothing
            txtType.Text = Nothing
            chkPrelogin.Checked = False
            Source.Text = Nothing
            TImpression.Text = Nothing
            CImpression.Text = Nothing
            TCTR.Text = Nothing
            CCTR.Text = Nothing
            anteprimaBanner.InnerHtml = ""
        End If
    End Sub
    
    'AMa #2310 17/11/2011  
    'Aggiunta del controllo se ci sono site aree e siti associati al banner
    
    Sub SaveBannerValue(ByVal sender As Object, ByVal e As EventArgs)
        Dim oBannerValue As New BannerValue
        With (oBannerValue)
            If SelectedIdBanner > 0 Then .KeyBanner.Id = SelectedIdBanner
            .Alt = Alt.Text
            .DateStart = DateStart.Value
            .DateEnd = DateEnd.Value
            .Description = Description.Text
            .Link = Link.Text
            .KeyLanguage.Id = lSelector.Language
            .Name = Name.Text
            If txtType.Text <> String.Empty Then .Type = Int32.Parse(txtType.Text)
            .Prelogin = chkPrelogin.Checked
            .Source = Source.Text
            If TImpression.Text <> String.Empty Then .TotalImpression = Int32.Parse(TImpression.Text)
            If TCTR.Text <> String.Empty Then .TotalCTR = Int32.Parse(TCTR.Text)
            '.CountImpression = Int32.Parse(CImpression.Text)
            '.CountCTR = Int32.Parse(CCTR.Text)
        End With
        If uploadBanner.PostedFile.ContentLength > 0 Then
            oBannerValue.Source = uploadBanner.FileName
            uploadBanner.PostedFile.SaveAs(SystemUtility.CurrentPhysicalApplicationPath() & "\" & ConfigurationsManager.BannerPath & "\" & uploadBanner.FileName)
        End If

        oAdvManager = New AdvertisingManager
        If SelectedIdBanner <= 0 Then
            oBannerValue = oAdvManager.Create(oBannerValue)
        Else
            oBannerValue = oAdvManager.Update(oBannerValue)
        End If

        oAdvManager.Cache = False
        
        
        Dim sites As SiteAreaCollection = oAdvManager.ReadBannerSiteArea(oBannerValue.KeyBanner)
        If Not sites Is Nothing AndAlso sites.Count > 0 Then
            oAdvManager.RemoveAllBannerSiteArea(oBannerValue.KeyBanner, SiteAreaCollection.SiteAreaType.Site)
        End If
        
        If Not oSite.GetSelection Is Nothing AndAlso oSite.GetSelection.Count > 0 Then
            Dim Sac As New Object
            
            Sac = oSite.GetSelection
            
            Dim salvi As Boolean = oAdvManager.Create(oBannerValue.KeyBanner, Sac)
           
        End If
        
        
        
        
        Dim arees As SiteAreaCollection = oAdvManager.ReadBannerSiteArea(oBannerValue.KeyBanner)
        If Not arees Is Nothing AndAlso arees.Count > 0 Then
            oAdvManager.RemoveAllBannerSiteArea(oBannerValue.KeyBanner, SiteAreaCollection.SiteAreaType.Area)
        End If
        
        If Not oAree.GetSelection Is Nothing AndAlso oAree.GetSelection.Count > 0 Then
            Dim Sc As New Object
            Sc = oAree.GetSelection
            oAdvManager.Create(oBannerValue.KeyBanner, Sc)
        End If
        
        If Not oContentType.GetSelection Is Nothing AndAlso oContentType.GetSelection.Count > 0 Then
            Dim Ctc As New Object
            Ctc = oContentType.GetSelection
            oAdvManager.RemoveAllBannerContentType(oBannerValue.KeyBanner)
            oAdvManager.Create(oBannerValue.KeyBanner, Ctc)
        End If
        
        If Not oBox.GetSelection Is Nothing AndAlso oBox.GetSelection.Count > 0 Then
            Dim Bc As New Object
            Bc = oBox.GetSelection
            oAdvManager.RemoveAllBannerBox(oBannerValue.KeyBanner)
            oAdvManager.Create(oBannerValue.KeyBanner, Bc)
        End If
        
        BindWithSearch(gridListBanner)
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Select Case TypeControl
            Case ControlType.Edit
                pnl = FindControl("pnlDett")
                LoadFormDett(LoadBannerValue)
            Case ControlType.Selection, ControlType.View
                pnl = FindControl("pnlGrid")
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListBanner)
                    BindFilerObject()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
       
        oContentType.LoadControl()
     
    End Sub
    
    Sub Page_load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
        
       
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As BannerSearcher = Nothing)
        oAdvManager = New AdvertisingManager
        oAdvCollection = New BannerCollection
        
        If Searcher Is Nothing Then
            Searcher = New BannerSearcher
        End If
        oAdvManager.Cache = False
        oAdvCollection = oAdvManager.Read(Searcher)
        If Not oAdvCollection Is Nothing Then
            oAdvCollection.Sort(SortType, SortOrder)
        End If
        objGrid.DataSource = oAdvCollection
        objGrid.DataBind()
    End Sub
    
    Function GetSiteSelection(ByVal olistItem As ctlSite) As SiteAreaValue
        Dim oSiteAreaCollection As Object = olistItem.GetSelection
        If Not oSiteAreaCollection Is Nothing AndAlso oSiteAreaCollection.count > 0 Then
            Return oSiteAreaCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Function GetContentTypeSelection(ByVal olistItem As ctlContentType) As ContentTypeValue
        Dim oContentTypeCollection As Object = olistItem.GetSelection
        If Not oContentTypeCollection Is Nothing AndAlso oContentTypeCollection.count > 0 Then
            Return oContentTypeCollection.item(0)
        End If
        Return Nothing
    End Function
    
    Function GetBoxSelection(ByVal olistItem As ctlBox) As BoxValue
        Dim oBoxCollection As Object = olistItem.GetSelection
        If Not oBoxCollection Is Nothing AndAlso oBoxCollection.count > 0 Then
            Return oBoxCollection.item(0)
        End If
        Return Nothing
    End Function
        
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSiteAreaValue As SiteAreaValue
        Dim oContentTypeValue As ContentTypeValue
        Dim oBoxValue As BoxValue
        
        oAdvSearcher = New BannerSearcher
        If txtId.Text <> "" Then oAdvSearcher.KeyBanner = New BannerIdentificator(txtId.Text)
        
        oSiteAreaValue = GetSiteSelection(Site)
        If Not oSiteAreaValue Is Nothing Then
            oAdvSearcher.KeySite.Id = oSiteAreaValue.Key.Id
        End If
        
        oSiteAreaValue = GetSiteSelection(Area)
        If Not oSiteAreaValue Is Nothing Then
            oAdvSearcher.KeySiteArea.Id = oSiteAreaValue.Key.Id
        End If
        
        oContentTypeValue = GetContentTypeSelection(ContentType)
        If Not oContentTypeValue Is Nothing Then
            Response.Write(oContentTypeValue.Key.Id)
            oAdvSearcher.KeyContentType.Id = oContentTypeValue.Key.Id
        End If
        
        oBoxValue = GetBoxSelection(Box)
        If Not oBoxValue Is Nothing Then
            oAdvSearcher.KeyBox.Id = oBoxValue.IdBox
        End If
                
        BindGrid(objGrid, oAdvSearcher)
    End Sub
    
    Sub SearchBanner(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListBanner)
    End Sub
    
    Protected Sub gridListBanner_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridListBanner_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
      
        gridListBanner.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Id"
                If SortType <> BannerGenericComparer.SortType.ById Then
                    SortType = BannerGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Name"
                If SortType <> BannerGenericComparer.SortType.ByName Then
                    SortType = BannerGenericComparer.SortType.ByName
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "DateStart"
                If SortType <> BannerGenericComparer.SortType.ByDateStart Then
                    SortType = BannerGenericComparer.SortType.ByDateStart
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "DateEnd"
                If SortType <> BannerGenericComparer.SortType.ByDateEnd Then
                    SortType = BannerGenericComparer.SortType.ByDateEnd
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
        BindWithSearch(sender)
    End Sub
    
    Private Property SortType() As BannerGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = BannerGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As BannerGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As BannerGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = BannerGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As BannerGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    Protected Sub gridListBanner_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litBanner")
        lnkSelect = e.Row.FindControl("lnkSelect")
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.Selection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                Case ControlType.View
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.Edit, ControlType.View
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim id As Int32 = s.commandargument
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.Edit
                SelectedIdBanner = id
                ViewState("CheckedItem") = Nothing
                txtHidden.Text = ""
                ShowRightPanel()
            Case "DeleteItem"
                SelectedIdBanner = id
                DeleteBanner()
                BindWithSearch(gridListBanner)
        End Select
    End Sub
    Sub NewBanner(ByVal s As Object, ByVal e As EventArgs)
        
        TypeControl = ControlType.Edit
        SelectedIdBanner = 0
        txtHidden.Text = ""
        ShowRightPanel()
        preview.Visible = False
        DateStart.Value = Date.Now.ToShortDateString
        DateEnd.Value = Date.Now.ToShortDateString
    End Sub
    
    Sub GoList(ByVal s As Object, ByVal e As EventArgs)
        TypeControl = ControlType.View
        SelectedIdBanner = 0
        txtHidden.Text = ""
        ShowRightPanel()
    End Sub
    
    'restituisce la data nel corretto formato in base al codice della lingua di navigazioen del sito
    Function getCorrectedFormatDate(ByVal DatePublish As DateTime) As String
        masterPageManager = New MasterPageManager
        Return dateUtility.FormatDateFromLanguage(DatePublish, masterPageManager.GetLang.Code)
    End Function
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    Public ReadOnly Property ReadSiteCollection() As Object
        Get
            Return oSite.GetSelection
        End Get
    End Property
    
</script>
<asp:textbox id="txtHidden" runat="server" style="display:none"/>
<hr />

<asp:Panel id="pnlGrid" runat="server">
    <asp:button ID="btnNew" CssClass="button" runat="server" Text="New" onclick="NewBanner" style="margin-bottom:10px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        
    <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Banner Campaign Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Site</strong></td>
                    <td><strong>Area</strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>
                   <td><HP3:ctlSite runat="server" ID="Site" TypeControl ="combo" SiteType="Site" ItemZeroMessage="Select Site"/></td>  
                    <td><HP3:ctlSite runat="server" ID="Area" TypeControl ="combo" SiteType="Area" ItemZeroMessage="Select Area"/></td>  
                    <td></td>  
                    <td></td>  
                    <td></td>
                </tr>
            </table>
    
            <table class="form">
                <tr>
                    <td><strong>ContentType</strong></td>    
                    <td><strong>Box</strong></td>
                    <td><strong></strong></td>
                </tr>
                <tr>
                    <td><HP3:ctlContentType runat="server" ID="ContentType" TypeControl ="combo" ItemZeroMessage="Select ContentType"/></td>
                    <td><HP3:ctlBox runat="server" ID="Box" TypeControl ="combo" ItemZeroMessage="Select Box"/></td>  
                </tr>
                <tr>
                    <td><asp:button CssClass="button" runat="server" ID="btnSearch" onclick="SearchBanner" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
             </table>
           </fieldset>
           <hr />
        </asp:Panel>
        
    <asp:gridview ID="gridListBanner" 
                    runat="server"
                    AutoGenerateColumns="false" 
                    GridLines="None" 
                    CssClass="tbl1"
                    HeaderStyle-CssClass="header"
                    AlternatingRowStyle-BackColor="#E9EEF4"
                    AllowPaging="true"
                    PagerSettings-Position="TopAndBottom"  
                    PagerStyle-HorizontalAlign="Right"
                    PagerStyle-CssClass="Pager"                     
                    AllowSorting="true"
                    OnPageIndexChanging="gridListBanner_PageIndexChanging"
                    PageSize ="20"
                    OnSorting="gridListBanner_Sorting"
                    emptydatatext="No Banner available"                
                    OnRowDataBound="gridListBanner_RowDataBound"
                    >
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "KeyBanner.Id")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="18%" DataField ="Name" HeaderText ="Name" SortExpression="Name" />
                    <asp:BoundField HeaderStyle-Width="25%" DataField ="Description" HeaderText ="Description" />
                     <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Date Start" SortExpression="DateStart">
                            <ItemTemplate><%#getCorrectedFormatDate(DataBinder.Eval(Container.DataItem, "DateStart"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Date End" SortExpression="DateEnd">
                            <ItemTemplate><%#getCorrectedFormatDate(DataBinder.Eval(Container.DataItem, "DateEnd"))%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Link">
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Link")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit sitem" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeyBanner.Id")%>'/>
                            <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "KeyBanner.Id")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
    </asp:gridview>
</asp:Panel>

<asp:Panel id="pnlDett" runat="server">
    <div>
        <asp:button ID="btnList" CssClass="button" runat="server" Text="Archive" onclick="GoList" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
        <asp:button CssClass="button" runat="server" ID="btnSave" onclick="SaveBannerValue" Text="Save" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    </div>
    <span id="BannerId" class="title" runat ="server"/>
    <span id="BannerDescrizione" class="title" runat ="server"/>
    <hr />
    <table class="form" style="width:100%">
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLanguage&Help=cms_HelpBannerLanguage&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLanguage", "Language")%></td>
            <td><HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBox&Help=cms_HelpBox&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBox", "Box")%></td>
            <td><HP3:ctlBox runat="server" ID="oBox" TypeControl="GenericRelation"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSite&Help=cms_HelpSite&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSite", "Site")%></td>
            <td><HP3:ctlSite runat="server" ID="oSite" TypeControl="GenericRelation" SiteType="Site" /></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAree&Help=cms_HelpAree&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAree", "Aree")%></td>
            <td><HP3:ctlSite runat="server" ID="oAree" TypeControl="GenericRelation" SiteType="Area"/></td>
        </tr>
        <tr>
            <td valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
            <td><HP3:ctlContentType runat="server" ID="oContentType" TypeControl="GenericRelation"/></td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateStart&Help=cms_HelpDateStart&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateStart", "Date Start")%></td>
            <td><HP3:Date runat="server" ID="DateStart" TypeControl="JsCalendar"  /></td>
        </tr>
        <tr>
            <td  valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDateEnd&Help=cms_HelpDateEnd&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDateEnd", "Date End")%></td>
            <td><HP3:Date runat="server" ID="DateEnd" TypeControl="JsCalendar"  /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpBannerName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
            <td><HP3:Text runat ="server" ID="Name" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td  style="width:20%" valign ="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBannerDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><HP3:Text runat ="server" ID="Description" TypeControl ="TextArea" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerPreLogin&Help=cms_HelpBannerPreLogin&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerPreLogin", "Pre Login")%></td>
            <td><asp:checkbox ID="chkPrelogin" runat="server" /></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpBannerLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
            <td><HP3:Text runat ="server" ID="Link" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAlt&Help=cms_HelpAlt&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormAlt", "Alt")%></td>
            <td><HP3:Text runat ="server" ID="Alt" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpBannerType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%></td>
            <td><HP3:Text runat ="server" ID="txtType" TypeControl ="NumericText" style="width:90px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormSource&Help=cms_HelpBannerSource&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormSource", "Source")%></td>
            <td><HP3:Text runat ="server" ID="Source" TypeControl ="TextBox" style="width:300px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerTotImpression&Help=cms_HelpBannerTotImpression&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerTotImpression", "Total Impression")%></td>
            <td><HP3:Text runat ="server" ID="TImpression" TypeControl ="NumericText" style="width:90px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerCountImpression&Help=cms_HelpBannerCountImpression&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerCountImpression", "Count Impression")%></td>
            <td><HP3:Text runat ="server" ID="CImpression" TypeControl ="NumericText" style="width:90px" isReadOnly="true"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerTotalCTR&Help=cms_HelpBannerTotalCTR&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerTotalCTR", "Total CTR")%></td>
            <td><HP3:Text runat ="server" ID="TCTR" TypeControl ="NumericText" style="width:90px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerCountCTR&Help=cms_HelpBannerCountCTR&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerCountCTR", "Count CTR")%></td>
            <td><HP3:Text runat ="server" ID="CCTR" TypeControl="NumericText" style="width:90px" isReadOnly="true"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormUploadBanner&Help=cms_HelpUploadBanner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUploadBanner", "Upload Banner")%></td>
            <td><asp:FileUpload  ID="uploadBanner" style="width:300px" runat="server" /></td>
        </tr>
        <tr>
            <td runat="server" id="preview" visible="false"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormBannerPreview&Help=cms_HelpBannerPreview&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormBannerPreview", "Preview")%></td>
            <td></td>
        </tr>
    </table>
    <span id="anteprimaBanner"  runat="server"></span>
</asp:Panel>
