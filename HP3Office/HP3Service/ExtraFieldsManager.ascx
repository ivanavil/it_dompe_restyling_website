<%@ Control Language="VB" ClassName="myExtraField" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private _strDomain As String
    Private _language As String
    
    Private strJS As String
    
    Private oGenericUtlity As New GenericUtility
    
    Public Property SelectedId() As Int32
        Get
            Return ViewState("selectedId")
        End Get
        Set(ByVal value As Int32)
            ViewState("selectedId") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
        
    Sub Page_load()
        Domain() = WebUtility.MyPage(Me).DomainInfo
        Language() = Me.PageObjectGetLang.Id
        
        If Not (Page.IsPostBack) Then
            BindCombo()
            ActivePanelGrid()
            LoadArchive()
        End If
    End Sub
    
    Sub BindCombo()
        Dim oCTmpManager As New ContentTemplateManager
        Dim oListItem As ListItem
        
        ddlContentsTemplate.Items.Clear()
        Dim templateColl As ContentTemplateCollection = oCTmpManager.Read(New ContentTemplateSearcher())
        
        If (Not templateColl Is Nothing) AndAlso (templateColl.Count > 0) Then
            For Each templValue As ContentTemplateValue In templateColl
                oListItem = New ListItem(templValue.Description, templValue.Key.Id)
                ddlContentsTemplate.Items.Add(oListItem)
            Next
            
            ddlContentsTemplate.Items.Insert(0, New ListItem("Select Content Template", 0))
        End If
    End Sub
    
    Protected Sub gdw_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        LoadArchive()
    End Sub
   
    'recupera la lista dei mail Template 
    Sub LoadArchive()
        Dim so As New ExtraFieldsSearcher
                
        Me.BusinessExtraFieldsManager.Cache = False
        Dim col As ExtraFieldsCollection = Me.BusinessExtraFieldsManager.Read(so)
         
        If Not col Is Nothing AndAlso col.Count > 0 Then
            gw_ExtraField.DataSource = col
            gw_ExtraField.DataBind()
        End If
    End Sub
    
    'modifica un singolo record
    Sub EditRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim value As New ExtraFieldsValue
        Dim idItem As Integer
       
        If sender.CommandName = "SelectItem" Then
            idItem = Integer.Parse(sender.CommandArgument)
            SelectedId = idItem
            
            Me.BusinessExtraFieldsManager.Cache = False
            value = Me.BusinessExtraFieldsManager.Read(New ExtraFieldsIdentificator(idItem))
       
            If Not value Is Nothing Then
                LoadDett(value)
                ActivePanelDett()
            End If
        End If
    End Sub
    
    'cancella un singolo record
    Sub RemoveRow(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim idItem As Integer
        Dim ris As Boolean
        
        If sender.CommandName = "DeleteItem" Then
            idItem = Integer.Parse(sender.CommandArgument)
            SelectedId = idItem
            ris = Me.BusinessExtraFieldsManager.Delete(New ExtraFieldsIdentificator(idItem))
                                  
            LoadArchive()
            ActivePanelGrid()
            
            'Gestione dei valori di ritorno
            If Not ris Then CreateJsMessage("Impossible to delete this Extra Field")
        End If
    End Sub
    
    Sub LoadArchive(ByVal sender As Object, ByVal e As EventArgs)
        ActivePanelGrid()
        LoadArchive()
    End Sub
   
    'modifica le propiet� di un record
    Sub UpdateRow(ByVal sender As Object, ByVal e As EventArgs)
        Dim Value As New ExtraFieldsValue
        With Value
            .Name = ExtraFieldName.Text
            .Description = ExtraFieldDescription.Text
            
            If (ExtraFieldLength.Text = Nothing) Then
                .Length = 0
            Else
                .Length = ExtraFieldLength.Text
            End If
           
            If (ExtraFieldDecimal.Text = Nothing) Then
                .DecimalNumber = 0
            Else
                .DecimalNumber = ExtraFieldDecimal.Text
            End If
            
            If (ExtraFieldOrder.Text = Nothing) Then
                .Order = 0
            Else
                .Order = ExtraFieldOrder.Text
            End If
            
            If (txtHiddenContentType.Text = Nothing) Then
                .KeyContentType.Id = 0
            Else
                .KeyContentType.Id = txtHiddenContentType.Text
            End If
            
            If (txtIdFather.Text = Nothing) Then
                .KeyTheme.Id = 0
            Else
                .KeyTheme.Id = txtIdFather.Text
            End If
            
            .Indexable = chkIndexable.Checked
            .KeyContentTemplate.Id = ddlContentsTemplate.SelectedItem.Value
            .Type = dwlExFType.SelectedItem.Value
            .Template = dwlExtFieldTemplate.SelectedItem.Value
        End With
         
        If SelectedId = 0 Then
            Me.BusinessExtraFieldsManager.Create(Value)
            CreateJsMessage("Saved successful!")
            
            LoadArchive()
            ActivePanelGrid()
        Else
            Value.Key.Id = SelectedId
            Me.BusinessExtraFieldsManager.Update(Value)
            CreateJsMessage("Updated successful!")
            
            LoadArchive()
            ActivePanelGrid()
        End If
    End Sub
    
    'inserisce una nuova riga 
    Sub NewRow(ByVal sender As Object, ByVal e As EventArgs)
        btnUpdate.Text = "Save"
        SelectedId = 0
        
        ActivePanelDett()
        LoadDett(Nothing)
    End Sub
    
    'popola i campi di testo con le informazioni contenute nel mailTemplateValue
    Sub LoadDett(ByVal Value As ExtraFieldsValue)
        If Not Value Is Nothing Then
            ExtraFieldName.Text = Value.Name
            ExtraFieldDescription.Text = Value.Description
            ExtraFieldLength.Text = Value.Length
            ExtraFieldDecimal.Text = Value.DecimalNumber
            ExtraFieldOrder.Text = Value.Order
            
            txtHiddenContentType.Text = Value.KeyContentType.Id
            txtContentType.Text = getContentTypeById(Value.KeyContentType.Id)
            
            txtIdFather.Text = Value.KeyTheme.Id
            Father.Text = getThemeById(Value.KeyTheme.Id)
            
            ddlContentsTemplate.SelectedIndex = ddlContentsTemplate.Items.IndexOf(ddlContentsTemplate.Items.FindByValue(Value.KeyContentTemplate.Id))
            dwlExFType.SelectedIndex = dwlExFType.Items.IndexOf(dwlExFType.Items.FindByValue(Value.Type))
            
            dwlExtFieldTemplate.SelectedIndex = dwlExtFieldTemplate.Items.IndexOf(dwlExtFieldTemplate.Items.FindByValue(Value.Template))
            dwlExtFieldTemplate.Enabled = False
            chkIndexable.Checked = Value.Indexable
            
            Select Case Value.Template
                Case ExtraFieldsValue.ExtraFieldsTemplate.ContentTemplate
                    contTemplate.Visible = True
                Case ExtraFieldsValue.ExtraFieldsTemplate.UserTemplate
                    contTemplate.Visible = False
            End Select
            
        Else
            chkIndexable.Checked = False
            ExtraFieldName.Text = Nothing
            ExtraFieldDescription.Text = Nothing
            ddlContentsTemplate.SelectedIndex = 0
            dwlExFType.SelectedIndex = 0
            dwlExtFieldTemplate.SelectedIndex = 0
            ExtraFieldLength.Text = Nothing
            ExtraFieldDecimal.Text = Nothing
            ExtraFieldOrder.Text = Nothing
            txtHiddenContentType.Text = Nothing
            txtIdFather.Text = Nothing
            txtContentType.Text = Nothing
            Father.Text = Nothing
        End If
    End Sub
    
    Function getThemeById(ByVal id As Integer) As String
        If (id = 0) Then Return ""
        
        Return oGenericUtlity.GetThemeValue(id).Description
    End Function

    Function getContentTypeById(ByVal id As Integer) As String
        If (id = 0) Then Return ""
        
        Return oGenericUtlity.GetContentType(id).Description
    End Function
    
    'attiva il pannello per la visualizzazione dell'archivio
    'disattiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelGrid()
        pnlGrid.Visible = True
        pnlDett.Visible = False
    End Sub
    
    'disattiva il pannello per la visualizzazione dell'archivio
    'attiva il pannello che visualizza i dettagli di un elemento
    Sub ActivePanelDett()
        pnlDett.Visible = True
        pnlGrid.Visible = False
    End Sub
    
    Sub ResetField(ByVal sender As Object, ByVal e As EventArgs)
        If (sender.CommandName.Equals("Theme")) Then
            txtIdFather.Text = Nothing
            Father.Text = Nothing
        Else
            txtContentType.Text = Nothing
            txtHiddenContentType.Text = Nothing
        End If
    End Sub
    
    Sub SelectTemplate(ByVal sender As Object, ByVal ev As EventArgs)
        Select Case dwlExtFieldTemplate.SelectedValue
            Case 1
                contTemplate.Visible = False
            Case 2
                contTemplate.Visible = True
        End Select
    End Sub
    
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        strJS = "alert(""" & strMessage & """)"
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return Me.BusinessDictionaryManager.Read(labelName, Me.PageObjectGetLang.Id, description)
    End Function
</script>


<script type ="text/javascript" >
   <%=strJS%>
</script>

<hr/>
<asp:UpdatePanel ID="upPanel" runat="server">
     <ContentTemplate>
            <asp:Panel id="pnlGrid" runat="server" visible="true"> 
            <div style="margin-bottom:10px"><asp:Button id ="btnNew" runat="server" Text="New" OnClick="NewRow"  CssClass="button"  style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></div>
            <asp:Label CssClass="title" runat="server" id="sectionTit">Extra Field List</asp:Label>
            <asp:gridview id="gw_ExtraField" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            OnPageIndexChanging="gdw_PageIndexChanging"
                            PagerSettings-Position="TopAndBottom"
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="20"
                            emptydatatext="No item available">
                          <Columns >
                           <asp:TemplateField HeaderStyle-Width="5%" HeaderText="ID">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                           </asp:TemplateField>  
                           <asp:TemplateField HeaderStyle-Width="70%" HeaderText="Field Name">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Name")%></ItemTemplate>
                           </asp:TemplateField>  
                           <asp:TemplateField HeaderStyle-Width="7%">
                                <ItemTemplate>
                                   <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  OnClick="EditRow" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                   <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')"  OnClick="RemoveRow" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                          </Columns>
             </asp:gridview>
            </asp:Panel>
   </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
     <ContentTemplate>
            <asp:Panel id="pnlDett" runat="server" visible="false">
             <div>
                <asp:Button id ="btnArchive" runat="server" Text="Archive" OnClick="LoadArchive" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
                <asp:Button id ="btnUpdate" runat="server" Text="Update" OnClick="UpdateRow" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
             </div>
             <table class="form" style="width:100%" >
               <tr>
                    <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormName&Help=cms_HelpExtraFieldName&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormName", "Name")%></td>
                    <td><HP3:Text runat ="server" id="ExtraFieldName" TypeControl ="TextBox" style="width:600px" MaxLength="50"/></td>
                </tr>
                <tr>
                    <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpExtraFieldDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                    <td><HP3:Text runat ="server" id="ExtraFieldDescription" TypeControl ="TextArea" style="width:600px"/></td>
                </tr>
                <tr>
                    <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtFTemplate&Help=cms_HelpExtraFieldTemplate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormExtFTemplate", "ExtraField Template")%></td>
                    <td>
                        <asp:DropDownList id="dwlExtFieldTemplate" runat="server" OnSelectedIndexChanged="SelectTemplate"  AutoPostBack="true">
                            <asp:ListItem Text="User Template" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Content Template" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                     <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormType&Help=cms_HelpExtraFieldType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormType", "Type")%></td>
                     <td>
                        <asp:DropDownList id="dwlExFType" DataTextField="Description" DataValueField="Description" runat="server" >
                            <asp:ListItem Text="String" Value="0"> </asp:ListItem>
                            <asp:ListItem Text="Integer" Value="1"> </asp:ListItem>
                            <asp:ListItem Text="Decimal" Value="2"> </asp:ListItem>
                            <asp:ListItem Text="Boolean" Value="3"> </asp:ListItem>
                            <asp:ListItem Text="MultiField" Value="4"> </asp:ListItem>
                            <asp:ListItem Text="FullText" Value="5"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                     <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraFieldLength&Help=cms_HelpExtraFieldLength&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormExtraFieldLength", "Type")%></td>
                     <td><HP3:Text runat ="server" id="ExtraFieldLength"  TypeControl="NumericText" style="width:150px"/></td>
                </tr>
                <tr>
                     <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormExtraFieldDecimal&Help=cms_HelpExtraFieldDecimal&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormExtraFieldDecimal", "Decimal")%></td>
                     <td><HP3:Text runat ="server" id="ExtraFieldDecimal"  TypeControl="NumericText" style="width:150px"/></td>
                </tr>
                <tr>
                     <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormOrder&Help=cms_HelpExtraFieldOrder&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormOrder", "Order")%></td>
                     <td><HP3:Text runat ="server" id="ExtraFieldOrder"  TypeControl="NumericText" style="width:150px"/></td>
                </tr>
                <tr runat="server" id="index">
                    <td valign="top"><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormIndexable&Help=cms_HelpIndexable&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%= getLabel("cms_FormIndexable", "Indexable")%></td>
                    <td><asp:checkbox ID="chkIndexable" runat="server"/></td>
                </tr>
                
             </table>
             
             <div id="contTemplate" runat="server" visible="false">
                 <table class="form" style="width:77%">
                     <tr>
                       <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentTemplate&Help=cms_HelpContentTemplate&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentTemplate", "Content Template")%></td>
                       <td><asp:DropDownList id="ddlContentsTemplate" DataTextField="Description" DataValueField="Description" runat="server" /></td>
                    </tr>
                    <tr>
                       <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormContentType&Help=cms_HelpContentType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormContentType", "Content Type")%></td>
                           <td>
                             <table>
                                <tr>
                                  <td><asp:TextBox ID="txtHiddenContentType" runat="server" style="display:none"/><HP3:Text runat ="server" ID="txtContentType"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                                  <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popContentType.aspx?SelItem=' + document.getElementById('<%=txtHiddenContentType.clientid%>').value + '&ctlHidden=<%=txtHiddenContentType.clientid%>&ctlVisible=<%=txtContentType.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                                  <td><asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="ResetField" CommandName="ContentType" CssClass="button"/></td>  
                                </tr>
                             </table>        
                          </td>
                     </tr>
                     <tr>
                        <td><a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=	cms_FormThemes&Help=cms_HelpThemes&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormThemes", "Theme")%></td>
                           <td>
                              <table>
                                   <tr>
                                     <td><asp:TextBox ID="txtIdFather" runat="server" style="display:none"/><HP3:Text runat ="server" ID="Father"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/></td>
                                     <td><input type="button" class="button" value="..." onclick="window.open('<%=Domain%>/Popups/popThemes.aspx?SelItem=' + document.getElementById('<%=txtIdFather.clientid%>').value + '&ctlHidden=<%=txtIdFather.clientid%>&ctlVisible=<%=Father.clientid%>','','width=780,height=480,scrollbars=yes')" /></td>        
                                     <td><asp:Button ID="Button1" runat="server" Text="Reset" OnClick="ResetField"  CommandName="Theme" CssClass="button"/></td>  
                                   </tr>
                              </table>        
                           </td>
                     </tr>
                 </table>
              </div>
            </asp:Panel>
   </ContentTemplate>
  <%-- <Triggers>
    <asp:AsyncPostBackTrigger ControlID="dwlTicketType" EventName="SelectedIndexChanged" />
  </Triggers>--%>
</asp:UpdatePanel>
