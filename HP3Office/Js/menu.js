﻿var _menuTimeout

function showMenu(menuNum) {
	clearTimeout(_menuTimeout);
	hideAllMenus(menuNum)
	var menu = document.getElementById('ulMenuOf' + menuNum);
	var themeLiv1 = document.getElementById('aThemeLev1Ind' + menuNum);
	if (menu.style.display == 'none') {
		menu.style.display = '';			
		if (themeLiv1.className != 'aThemeLev1Current') 
			themeLiv1.className = 'aThemeLev1Sel';		
	}
	//hideSelects();
}

function hideMenu(menuNum) {
	_menuTimeout = setTimeout('hideMenuNow("' + menuNum + '")', 2000);
}

function hideMenuNow(menuNum) {
	document.getElementById('ulMenuOf' + menuNum).style.display = 'none';
	var obj = document.getElementById('aThemeLev1Ind' + menuNum);
	
	if (obj.className != 'aThemeLev1Current') 
		obj.className = 'aThemeLev1Unsel';
		
	showSelects();
}

function hideAllMenus(exceptNum) {
	for (i=0; ; i++) {	
		if (i==exceptNum)
			continue;
		var menu = document.getElementById('ulMenuOf' + i);
		if (menu != null) {
			menu.style.display = 'none';		
			var obj = document.getElementById('aThemeLev1Ind' + i)
			if (obj.className != 'aThemeLev1Current') {
				obj.className = 'aThemeLev1Unsel';
			}
		} else
			return;
	}
}

function hideSelects() {
	var els = document.getElementsByTagName("select");
	for (i=0; i<els.length; i++) {
		els[i].style.display = 'none';
	}
}
 
 
function showSelects() {
	var els = document.getElementsByTagName("select");
	for (i=0; i<els.length; i++) {
		els[i].style.display = '';
	}
}
 