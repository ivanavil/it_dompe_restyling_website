﻿// JScript File

 var MouseOverColor = '#234eee'
    var MouseOverTextColor ='#fff'
    var MouseClickTextColor = '#000'
    var MouseClickColor = 'yellow'
    
    var SelDiv        
    var bgcolor,fcolor
    
    function DisplayDiv(idDiv,ancor)
        {                   
            var oDiv = document.getElementById (idDiv)
            
            if (oDiv != null)             
                    if (oDiv.style.display=='none')
                        {
                            oDiv.style.display=''
                            ancor.innerText = '- '
                        }
                    else
                        {
                            oDiv.style.display='none'
                            ancor.innerText = '+ '
                        }
        }
        
    function ChangeColor(objDiv)
        {   
           if (SelDiv != objDiv) 
            {
                var ancor = objDiv.getElementsByTagName('a')        
            
                bgcolor=objDiv.style.backgroundColor                       
                fcolor = ancor[0].style.color       
              
                objDiv.style.backgroundColor=MouseOverColor
                ancor[0].style.color  = MouseOverTextColor          
                ancor[1].style.color  = MouseOverTextColor      
                objDiv.style.cursor="hand"  
            }
        }
        
     function RestoreColor(objDiv)
        {
           if (SelDiv != objDiv) 
           {
               var ancor = objDiv.getElementsByTagName('a')
                
               objDiv.style.backgroundColor=bgcolor
               ancor[0].style.color  = fcolor          
               ancor[1].style.color  = fcolor          
           }
        }
        
  