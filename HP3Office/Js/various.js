function zoom(filename) {
	window.open("popups/zoom.aspx?filename=" + filename, "_blank", "resizable=yes, width=200, height=200, status=no, menubar=no");
}

function safeSubmit(buttonId) {
	if (window.event && window.event.keyCode == 13) {
		document.getElementById(buttonId).click();
		return false;
	} else
		return true;
}

function hideSelects() 
    {
	    var els = document.getElementsByTagName("select");
	    for (i=0; i<els.length; i++) 
	        {
		        els[i].style.display = 'none';		        
	        }
	}
	
function showSelects() 
    {
	var els = document.getElementsByTagName("select");
	for (i=0; i<els.length; i++) 
	{
		els[i].style.display = '';
	}
    }	    