<%@ Page Language="vb" ClassName="myMasterPage" ValidateRequest="false" EnableEventValidation="false" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %> 
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>

<%@ Register TagName ="Languages" tagprefix="HP3" Src ="~/hp3Office/HP3Common/ListLanguages.ascx" %>
<%@ Register TagName ="NavigationBar" tagprefix="HP3" Src ="~/hp3Office/HP3Common/NavigationBar.ascx"%>
<%@ Register TagName ="ThemesSite" tagprefix="HP3" Src ="~/HP3Office/HP3Common/ThemesSite.ascx"%>

<%@ Register TagName ="Menu" tagprefix="HP3" Src ="~/HP3Office/HP3Common/commonThemesMenu.ascx"%>
<%@ Register TagName ="SiteareaInfo" tagprefix="HP3" Src ="~/HP3Office/HP3Common/SiteAreaInfo.ascx"%>
<%@ Register TagName ="ctlToolbar" tagprefix="HP3" Src ="~/hp3Office/HP3Parts/Toolbar.ascx"%>
<%@ Register TagName ="ctlLastService" tagprefix="HP3" Src ="~/hp3Office/HP3Common/LastViewedService.ascx"%>

<script language="vb" runat="server">
    Public webRequest As WebRequestValue = New WebRequestValue()
    Public idCurrentLang As Integer = Me.BusinessMasterPageManager.GetLang.Id
   
    Public CurrentSite As SiteValue()
   
    Public ThemesSite As String
    Public idConsolleContentType As String
    Public idThemes As String
    Public idConsolleSiteArea As String
    Private listShareProperty As New Hashtable
    Private systemUtility As New SystemUtility()
    Private _isnew As String
    Private objQs As New ObjQueryString
    Public Tab As String = ""
    
    Dim ListLink As Hashtable
    Dim count As Integer = 0
    'MaintainScrollPositionOnPostback="true"
#Region "Variabili Membro"
    Private _currentLang As New LanguageIdentificator
    Private _siteValue As SiteValue
    Private _selectMenu As Boolean = True
    
#End Region
    
#Region "ProprietÓ"
          
    Public Property CurrentLang() As LanguageIdentificator
        Get
            Return _currentLang
        End Get
        Set(ByVal value As LanguageIdentificator)
            _currentLang = value
        End Set
    End Property
    
    Public Property SiteValue() As SiteValue
        Get
            Return _siteValue
        End Get
        
        Set(ByVal value As SiteValue)
            _siteValue = value
        End Set
    End Property
    
    Public ReadOnly Property SiteFolder()
        Get
            Return Me.ObjectSiteFolder
        End Get
    End Property
    
    Public ReadOnly Property isNew()
        Get
            Return _isnew <> "" AndAlso _isnew <> "0"
        End Get
    End Property
    
    Public ReadOnly Property DomainInfo()
        Get
            Dim strDomain = "http://" & Me.ObjectSiteDomain.Domain & "/" & Me.ObjectSiteFolder
            Return strDomain
        End Get
    End Property
#End Region
    '***********
    Public ReadOnly Property GetToolbar() As ToolBar
        Get
            Return oToolbar
        End Get
    End Property

    Sub traceMe()
        Me.BusinessMasterPageManager.Trace()
        Me.BusinessMasterPageManager.TraceExtend()
    End Sub
    
    ''' <summary>
    ''' Imposta la toolbar
    ''' </summary>
    ''' <remarks></remarks>
    Sub SetToolbar()
        GetToolbar.AddForVisibilty("ShowControl", False)
    End Sub
    
    Sub Page_Init()
        ' Web.HttpContext.Current.Handler = CType(Activator.CreateInstance(Me.GetType()), Web.IHttpHandler)
    End Sub
    
    Sub LoadSharedProperty()
        With listShareProperty
            .Add("CurrentLang", CurrentLang)
            .Add("idCurrentLang", idCurrentLang)
            .Add("isNew", isNew)
            .Add("SiteFolder", SiteFolder)
            .Add("ThemesSite", ThemesSite)
            .Add("idConsolleContentType", idConsolleContentType)
            .Add("idConsolleSiteArea", idConsolleSiteArea)
            .Add("idThemes", idThemes)
            .Add("DomainInfo", DomainInfo)
            .Add("SiteValue", SiteValue)
        End With
        GenericDataContainer.Add("PublicObjects", listShareProperty, True)
    End Sub
    
    Sub ReadQS()
        'ThemesSite = webRequest.customParam.item("Site_Id")
        ThemesSite = objQs.Site_Id
        If ThemesSite = "" Then ThemesSite = -1
              
        idThemes = objQs.Theme_id
        If idThemes = "" Then idThemes = "0"
               
        idConsolleContentType = objQs.ContentType_Id
        If idConsolleContentType = "" Then idConsolleContentType = "0"
        
        idConsolleSiteArea = objQs.SiteArea_Id
        If idConsolleSiteArea = "" Then idConsolleSiteArea = "0"
        
        _isnew = objQs.IsNew
    End Sub
    
    'Formatta i link in base a contentType e siteArea
    Function getLink(ByVal keyContentType As ContentTypeIdentificator, ByVal keySiteArea As SiteAreaIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(keyContentType, keySiteArea, True)
    End Function
    
    Sub RemoveSharedObject()
        ' GenericDataContainer.Remove("LastViewService")
        ' GenericDataContainer.Remove("PublicObjects")
    End Sub
    
    '***LOGOUT
    Sub DoLogout(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Me.BusinessAccessService.Logout() Then
            RemoveSharedObject()
            
            '***Resetto il menu
            Dim oCookie As New HttpCookie("Tab")
            oCookie.Value = ""
            Response.Cookies.Add(oCookie)
            '******************
            Response.Redirect("/")
        End If
    End Sub
    
    Function getVersion() As String
        Dim ris As String = "N.D."
        Dim assVersion As String = Me.systemUtility.GetVersionAssembly()
        Dim dbVersion As String = Me.systemUtility.GetVersionDatabase()
        ris = assVersion
 
         
        If assVersion.Trim <> dbVersion.Trim Then
            ris = ris + " <br/><em> Warning misaligned database version </em>"
        End If
            
        
        Return ris
        
    End Function
    
    Sub Page_Load()
       
        phScriptMan.Controls.Add(LoadControl("/HP3Office/HP3Common/ScriptManager.ascx"))
        Try
           
            If Me.BusinessAccessService.IsAuthenticated And Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("HP3", "officeAcc") Then
                setTab()
                ReadQS()
                Tabber.Visible = True
                ThemesMenu.Visible = True
                btn_logOut.Visible = True
                leftMenu.Visible = True
                user_labelMsg.Text = Me.BusinessAccessService.GetTicket().User.Surname & " " & Me.BusinessAccessService.GetTicket().User.Name
                navBar.Visible = True
                If ThemesSite <> -1 Then
                    'Dim oSiteSearcher As New SiteSearcher
                    'oSiteSearcher.Key.Id = 1
                    ThemesMenu.Visible = True
                    SiteSelect.SiteId = ThemesSite
                    ThemesMenu.SiteId = ThemesSite
                    ThemesMenu.idTheme = Me.BusinessSiteManager.Read(Int16.Parse(ThemesSite)).mainTheme
                    oToolbar.Visible = True
                Else
                    ThemesMenu.Visible = False
                End If

                If Not Me.BusinessContentTypeManager.LoadPathControlConsolle(Me.ObjectSiteFolder, Me.BusinessMasterPageManager.GetContentType) Is Nothing Then
                    mainControll.Controls.Add(LoadControl(Me.BusinessContentTypeManager.LoadPathControlConsolle(Me.ObjectSiteFolder, Me.BusinessMasterPageManager.GetContentType)))
                End If
                'imposta i defautl per la toolbar
                LoadSharedProperty()
                SetToolbar()
            Else
                Tabber.Visible = False
                btn_logOut.Visible = False
                navBar.Visible = False
                leftMenu.Visible = False
           
                mainControll.Controls.Add(LoadControl(Me.BusinessContentTypeManager.LoadPathControlConsolle(Me.ObjectSiteFolder, New ContentTypeIdentificator("HP3", "Login"))))
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
        traceMe()
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CMS", "CMS") Then
            tabCMS.Visible = True
        End If
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CRM", "CRM") Then
            tabCRM.Visible = True
        End If
        
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CRM", "CRMPW") Then
            tabCRMPW.Visible = True
        End If
        
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("ADMIN", "ADMIN") Then
            tabADMIN.Visible = True
        End If
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("USER", "USER") Then
            tabUser.Visible = True
        End If
        
    End Sub
    
    Function ReadTheme(ByVal Url As String) As Int32
        Dim objQs As New ObjQueryString(Url)
        Return objQs.Theme_id
    End Function
    
    Sub setTab()
        If Not Request.Cookies("Tab") Is Nothing Then
            Tab = Request.Cookies("Tab").Value
           
            If Tab <> "" AndAlso Tab <> "CRMPW" Then
                Dim targ = CType(Me.FindControl("Tab_" & Tab), ImageButton)
                If Not targ Is Nothing Then
                    targ.ImageUrl = "/Hp3Office/Hp3image/button/Tab_" & Tab.ToString() & "_On" & ".gif"
                End If
            Else
                
                Dim targPW = CType(Me.FindControl("Tab_" & Tab), ImageButton)
                If Not targPW Is Nothing Then
                    targPW.ImageUrl = "/Hp3Office/Hp3image/button/Tab_" & Tab.ToString() & "_on" & ".bmp"
                End If
            End If
                
        End If
    End Sub
    
    Sub Page_Prerender()
        If Not Page.IsPostBack Then oToolbar.Loadcontrol()
    End Sub
    
    Sub SelItem(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim label As String = s.commandargument
        
        Dim oCookie As New HttpCookie("Tab")
        oCookie.Value = label
       
        Response.Cookies.Add(oCookie)
        Response.Redirect(Request.Url.ToString)
    End Sub

    </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>HP3 Office</title>
	<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
	
    <link type="text/css" href="/hp3Office/_css/calendar-blue.css" rel="stylesheet"/>
    <%--<link href="/hp3Office/_css/piwikcrmstyle.css" type="text/css" rel="stylesheet" />	--%>
    
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/Menu.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/Various.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/string.js"></script>
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/Site.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/ctlOptionExtra.js"></script>
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/ctlText.js"></script>
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/ctlGeo.js"></script>
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/ctlThemes.js"></script>
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/ShowControl.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/calendar.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/calendar-setup.js"></script>
	<script type="text/javascript" language="javascript" src="/hp3Office/Js/lang/calendar-en.js"></script>		
    
    <script type="text/javascript" language="javascript" src="/hp3Office/Js/OverlayWindow.js"></script>
    

    <META http-equiv="Page-Enter" content="blendTrans(Duration=0.1)">
    <META http-equiv="Page-Exit" content="blendTrans(Duration=0.1)">
  
</head>

<body>
<form id="form1" runat = "server" enctype="multipart/form-data">	
<asp:PlaceHolder ID="phScriptMan" runat="server" />

<div class="dHeader"><div class="topHp3Logo" style="cursor:pointer" onclick="document.location='/';"><div class="topHealthwareLogo"/></div></div>
	<div class="dNavBar"><div class="left"></div>
	    <div class="center">
	        <asp:PlaceHolder runat="server" ID="Tabber">
	            <div class="user_label"><asp:Label runat="server" ID="user_labelMsg"/></div>
	            <div class="tabMenuSpacer">
	                    <div id="tabCMS" visible="false" class="tabMenu" runat="server">
	                        <asp:ImageButton runat="server" ID="tab_cms" ImageUrl="~/HP3Office/HP3image/button/Tab_CMS.gif" onclick="SelItem" commandArgument="CMS" />
	                    </div>
	                    <div id="tabCRM" visible="false" class="tabMenu" runat="server">
	                        <asp:imagebutton runat="server" ID="tab_crm" ImageUrl="~/HP3Office/HP3image/button/Tab_CRM.gif" onclick="SelItem" commandArgument="CRM" />
	                    </div>
                         <div id="tabCRMPW" visible="false" class="tabMenu" runat="server">
	                        <asp:imagebutton runat="server" ID="tab_crmpw" ImageUrl="~/HP3Office/HP3image/button/TAB_CRMPW.bmp" onclick="SelItem" commandArgument="CRMPW" />
	                    </div>
	                    <span id="tabADMIN" visible="false" runat="server">
	                        <div class="tabMenu">
	                            <asp:imagebutton runat="server" ID="tab_admin" ImageUrl="~/HP3Office/HP3image/button/Tab_ADMIN.gif" onclick="SelItem" commandArgument="ADMIN" />
	                        </div>
	                        </span>
	                    <span id="tabUser" visible="false" runat="server">
	                        <div class="tabMenu">
	                            <asp:imagebutton runat="server" ID="tab_user" ImageUrl="~/HP3Office/HP3image/button/Tab_USER.gif" onclick="SelItem" commandArgument="USER" />
	                        </div>
	                    </span>
	                <div class="btn_Logout" >
	                    <asp:ImageButton runat="server" ID="btn_logOut" ImageUrl="~/hp3Office/_slice/button/btn_LogOutIco.gif" AlternateText="Logout" onClick="doLogout" />
	                </div>
	           </div>
	       </asp:PlaceHolder>
	        
	    </div>
	    <div class="right"></div>
	</div>		
	
	<div class="innerMenu">
	   <div>
	        <HP3:NavigationBar runat="server" id="navBar"  />
	    </div>
	</div>
	
	
	<div class="dMain">
	
		<div class="dColumn">
		    <asp:PlaceHolder runat="server" ID="leftMenu">
		    <HP3:ThemesSite runat="server" ID="SiteSelect" />
            <HP3:Menu runat="server" id="ThemesMenu" />
		    </asp:PlaceHolder>
		    <div style="clear:both"></div>
	     </div>
	    <div class="dContent" id="dPage" runat="server">
		     <div class="pad10">  
                <div class="topLastViewed"><HP3:ctlLastService id="LastService" runat="server" /></div>
                <HP3:ctlToolbar ID="oToolbar" runat ="server" Visible="false" />
                <asp:PlaceHolder runat="server" ID="mainControll"/>
		    </div>
	    </div>
	</div>
	
	<div class="dFooter">
		<div class="left"></div>		
		<div class="center"><b>HP3 Office</b>
        
                <div style="float:right; color:Gray" >Assembly Version: <%= getVersion()%></div>
        </div>
        <%--<%= Me.systemUtility.GetVersionDatabase()%>-<b>HP3 Office</b>-<%= Me.systemUtility.GetVersionAssembly()%>--%>
		<div class="right"></div>	
	</div>
    <style type="text/css">

#tableversion td {border: 0; color:Gray; }


</style>
</form>	
</body>
</html>




