<%@ Control Language="VB" ClassName="ctlOption_Extra" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script runat="server">
    Private _optManager As ContentOptionService
    Private _optSearcher As ContentOptionSearcher
    Private _optCollection As ContentOptionCollection
    
    Private _cManager As ContentManager
    Private _cSearcher As ContentOptionExtraSearcher
    Private _cCollection As ContentOptionExtraCollection
    
    Private _idRelated As Int32
    Private _idLingua As Int32
    Private StrJs As String
      
    Public Sub LoadControl()
        drpOptions.Attributes.Add("onchange", "DisplayControls(this);")
        lstMain.Attributes.Add("onchange", "DisplayButton(this,'" & btnRemove.ClientID & "');ReadAddInfo('" & txtHidden.ClientID & "','" & lstMain.ClientID & "','" & lblDefault.ClientID & "','" & lblExtSite.ClientID & "','" & lblUrl.ClientID & "')")
        
        If Not Page.IsPostBack Then
            BindComboOptions()
            BindData()
        End If
    End Sub
    
    
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare l'area
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value                        
        End Set
    End Property
        
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
    
    ''' <summary>
    ''' Verifica l'esistenza del parametro value nella collezione di opzioni
    ''' </summary>
    ''' <param name="Value"></param>
    ''' <param name="Item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ListContainsOptions(ByVal List As ContentOptionExtraCollection, ByVal Value As Int32, ByRef Item As ContentOptionExtraValue) As Boolean
        Dim outVal As Boolean = False
        
        If Not List Is Nothing Then
            For Each Item In List
                If Item.KeyOptionExtra.Id = Value Then
                    outVal = True
                    Exit For
                End If
            Next
        End If
        
        Return outVal
    End Function
    
 
    ''' <summary>
    ''' Recupera l'opzione dalla chiave primaria
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetContentOption(ByVal id As Int32) As ContentOptionValue
        Try
            _optManager = New ContentOptionService
                   
            _optSearcher = New ContentOptionSearcher()
            _optSearcher.Key = New ContentOptionIdentificator(id)
            
            _optCollection = _optManager.Read(_optSearcher)
      
            Dim _cValue As ContentOptionValue
            If Not _optCollection Is Nothing AndAlso _optCollection.Count = 1 Then
                _cValue = _optCollection(0)
            End If
        
            Return _cValue
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    
    ''' <summary>
    ''' Setta la collezione di opzioni.
    ''' La recupera leggendo dalla listbox i dati gi� caricati    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property OptionsExtra() As ContentOptionExtraCollection
        Get
            Dim cItem As ContentOptionExtraValue
            Dim arrExlude As New ArrayList
            
            If Not ViewState("ListOptions") Is Nothing Then
                                        
                For Each cItem In ViewState("ListOptions")
                    If lstMain.Items.FindByValue(cItem.KeyOptionExtra.Id) Is Nothing Then
                        arrExlude.Add(cItem)                        
                    End If
                Next                                   
                
                For Each cItem In arrExlude
                    CType(ViewState("ListOptions"), ContentOptionExtraCollection).Remove(cItem)
                Next
                
                Return ViewState("ListOptions")
            End If
        End Get
        
        Set(ByVal value As ContentOptionExtraCollection)
            ViewState("ListOptions") = value
        End Set
    End Property
     
    Function AddItem(ByVal cItem As ContentOptionExtraValue) As ContentOptionExtraValue
       
        Try
            Dim _collItem As ContentOptionExtraValue
            Dim cCollection As ContentOptionExtraCollection
        
            'Recupero le opzioni caricate
            If OptionsExtra Is Nothing Then
                cCollection = New ContentOptionExtraCollection
            Else
                cCollection = OptionsExtra
            End If
        
            If Not ListContainsOptions(cCollection, cItem.KeyOptionExtra.Id, _collItem) Then
                cCollection.Add(cItem)
                OptionsExtra = cCollection
                
                txtHidden.Text += cItem.KeyOptionExtra.Id & "|" & cItem.ExternalSiteUrl & "|" & IIf(cItem.IsExternalSite, 1, 0) & "|" & IIf(cItem.IsDefault, 1, 0) & "#"
            Else
                StrJs = "alert('Element is already present in the list')"
                cItem = Nothing
            End If
            
        Catch ex As Exception
            cItem = Nothing
            Throw ex
        End Try
        
        Return cItem
    End Function
    

    Sub BindList(ByVal srcColl As ContentOptionExtraCollection, Optional ByVal bAddItem As Boolean = True)
        lstMain.Items.Clear()
        Dim optionValue As ContentOptionValue
        
        If Not srcColl Is Nothing Then
            Dim _cValue As ContentOptionExtraValue
            Dim item As ListItem
            
            For Each _cValue In srcColl               
                item = New ListItem
                
                optionValue = GetContentOption(_cValue.KeyOptionExtra.Id)
                If Not optionValue Is Nothing Then
                    item.Text = optionValue.Label
                    item.Value = optionValue.Key.Id
                
                    lstMain.Items.Add(item)
                    
                    If bAddItem Then AddItem(_cValue)
                End If
            Next
        End If
    End Sub
    
 
    ''' <summary>
    ''' Carica le opzioni associate al content
    ''' </summary>
    ''' <remarks></remarks>
    Sub BindData()
        Try
            If idRelated = 0 Then Exit Sub
            
            _cManager = New ContentManager
            _cManager.Cache = False
                        
            _cSearcher = New ContentOptionExtraSearcher(New ContentIdentificator(idRelated, 0))
            _cCollection = _cManager.ReadContentOptionExtra(_cSearcher)
            
            BindList(_cCollection.DistinctBy("KeyOptionExtra.Id"))
                        
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub
    
 
    Sub BindComboOptions()
        _optManager = New ContentOptionService
        _optManager.Cache = False
        
        _optSearcher = New ContentOptionSearcher
        
        _optCollection = _optManager.Read(_optSearcher)
      
        Dim _cValue As ContentOptionValue
        Dim item As ListItem
        
        If Not _optCollection Is Nothing Then
                       
            For Each _cValue In _optCollection
                item = New ListItem
                item.Text = _cValue.Label
                item.Value = _cValue.Key.Id
                
                drpOptions.Items.Add(item)
            Next
            
            drpOptions.Items.Insert(0, New ListItem("Select option", 0))
            
        End If
        
       
       
    End Sub
    
  
    Sub AddOption(ByVal s As Object, ByVal e As EventArgs)
        Dim cOptionValue As New ContentOptionExtraValue
        
        If Not drpOptions.SelectedItem Is Nothing Then
            'If (readOptionExtra() And Not (chkIdDefault.Checked)) Or (Not (readOptionExtra()) And (chkIdDefault.Checked)) Then
            
            Dim id As Int32 = drpOptions.SelectedItem.Value
      
            cOptionValue.KeyContent.Id = idRelated
            cOptionValue.KeyOptionExtra.Id = id
            cOptionValue.IsDefault = chkIdDefault.Checked
            cOptionValue.IsExternalSite = chkExtUrl.Checked
            cOptionValue.ExternalSiteUrl = txtUrl.Text
        
            Dim cOption As ContentOptionExtraValue = AddItem(cOptionValue)
            Dim optionValue As ContentOptionValue
        
            If Not cOption Is Nothing Then
                optionValue = GetContentOption(cOption.KeyOptionExtra.Id)
                lstMain.Items.Add(New ListItem(optionValue.Label, cOption.KeyOptionExtra.Id))
                
                chkIdDefault.Checked = False
                chkExtUrl.Checked = False
                txtUrl.Text = ""
            End If
               
            lstMain.SelectedIndex = -1
        End If
    End Sub
    
    Sub RemoveOption(ByVal s As Object, ByVal e As EventArgs)
        If Not lstMain.SelectedItem Is Nothing Then
            ClearHidden(lstMain.SelectedItem.Value)
            lstMain.Items.RemoveAt(lstMain.SelectedIndex)
            
            BindList(OptionsExtra, False)
            chkIdDefault.Checked = False
            chkExtUrl.Checked = False
            txtUrl.Text = ""
        End If
    End Sub
    
    Sub ClearHidden(ByVal id As Int32)
        Dim strOut As String
        Dim strHidden As String = txtHidden.Text
        
        If strHidden <> "" Then
            Dim arr() As String = strHidden.Split("#")
            Dim arr2() As String
            Dim strItem As String
        
            For Each strItem In arr
                If strItem <> "" Then
                    arr2 = strItem.Split("|")
                    If arr2(0) <> id Then
                        strOut += strItem & "#"
                    End If
                End If
            Next
        End If
        txtHidden.Text = strOut
    End Sub
    
    Sub Disable()
        lstMain.Enabled = False
        drpOptions.Enabled = False
        chkIdDefault.Enabled = False
        chkExtUrl.Enabled = False
        btnAdd.Enabled = False
        lblDefault.Enabled = False
        lblExtSite.Enabled = False
        lblUrl.Enabled = False
        btnRemove.Enabled = False
    End Sub
    
    Sub Enabled()
        lstMain.Enabled = True
        drpOptions.Enabled = True
        chkIdDefault.Enabled = True
        chkExtUrl.Enabled = True
        btnAdd.Enabled = True
        lblDefault.Enabled = True
        lblExtSite.Enabled = True
        lblUrl.Enabled = True
        btnRemove.Enabled = True
    End Sub
</script>


<asp:textbox ID="txtHidden" runat="server" style="display:none"/>
<table >
    <tr>
        <td colspan="2"><asp:ListBox id="lstMain" runat="server" style="width:200px"/></td>
        <td colspan="2" valign="top"><asp:dropdownlist ID="drpOptions" runat="server" style=" padding-top:0px"/></td>
    </tr>

</table>


<table>
    <tr id="ctlDiv">
        <td colspan="3"><asp:checkbox runat="server" id="chkIdDefault" Text ="Default"/></td>
        <td colspan="3"><asp:checkbox runat="server" id="chkExtUrl" Text ="Url esterno"/> <asp:TextBox ID="txtUrl" runat="server" /></td>
        <td colspan="3"><asp:button ID="btnAdd" runat="server" onClick="AddOption" Text="Add" CssClass="button"/></td>
    
    </tr>
 </table>
 <table>
    <tr >
        <td colspan="4"><asp:Label ID="lblDefault" runat="server" /></td>
        <td colspan="4"><asp:Label ID="lblExtSite" runat="server" /></td>
        <td colspan="4"><asp:Label id="lblUrl" runat="server" /></td>
        <td colspan="4"><asp:Button id="btnRemove"  runat="server" text="Remove" onclick="RemoveOption" CssClass="button"/> </td>
    </tr>
</table>


<%--<table width="100%" >
    <tr>
        <td  width="30%" rowspan="3"  valign="middle"><asp:ListBox id="lstMain" runat="server" style="width:200px"/></td> 
        <td><asp:Label ID="lblDefault" runat="server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="lblExtSite" runat="server" /></td>
    </tr>
    <tr>
        <td><div id="lblUrl" runat="server" /></td>
    </tr>
</table>

<asp:dropdownlist ID="drpOptions" runat="Server" />
<span id="ctlDiv">
    <asp:checkbox runat="server" id="chkIdDefault" Text ="Default"/>
    <asp:checkbox runat="server" id="chkExtUrl" Text ="Url esterno"/>
    <asp:TextBox ID="txtUrl" runat="server" />
    <asp:button ID="btnAdd" runat="server" onClick="AddOption" Text="Add" CssClass="button"/>    
</span>
<asp:Button type="button" id="btnRemove"  runat="server" text="Remove" onclick="RemoveOption" CssClass="button"/>--%>

<script type="text/javascript">
    DisplayControls(document.getElementById ('<%=drpOptions.ClientID%>'))
    DisplayButton(document.getElementById ('<%=lstMain.ClientID%>'),'<%=btnRemove.clientid%>')
    <%=StrJs%>
</script>