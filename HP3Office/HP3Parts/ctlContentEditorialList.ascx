<%@ Control Language="VB" ClassName="ctlContentEditorialList" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">   
    Public Enum ControlType        
        GenericRelation
        
    End Enum
    
#Region "Membri"    
    Private _typecontrol As ControlType = ControlType.GenericRelation
    Private strDomain As String
    Private _idRelated As Int32
    Private _GenericCollection As ContentEditorialCollection
    Private _enable As Boolean = True
#End Region
           
#Region "Properties"
        
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                BindData(ListBox1)
        End Select
    End Sub    
       
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
    Public Property Enable() As Boolean
        Get
            Return _enable
        End Get
        Set(ByVal value As Boolean)
            _enable = value
        End Set
    End Property
#End Region
    
    'Legge la descrizione del ContentEditorial dato l'id
    Function ReadEditorialName(ByVal idEditorial As Int32) As ContentEditorialValue
        Dim EditorialManger As New ContentEditorialManager
        Dim EditorialValue As ContentEditorialValue
        Dim EditorialSearcher As New ContentEditorialSearcher
        
        EditorialSearcher.Id = idEditorial
        EditorialValue = EditorialManger.Read(EditorialSearcher)(0)
       
        Return EditorialValue
    End Function
       
  
    Public ReadOnly Property GetSelection() As ContentEditorialCollection
        Get
            Dim EditorialCollection As New ContentEditorialCollection
            Dim EditorialValue As ContentEditorialValue
            
            Select Case TypeControl
                Case ControlType.GenericRelation
                    Dim item As ListItem
                           
                    Dim strOut As String = ""
                    LoadItems()
		
                    For Each item In ListBox1.Items
                        EditorialValue = ReadEditorialName(item.Value)
            
                        If Not EditorialValue Is Nothing Then
                            EditorialCollection.Add(EditorialValue)
                        End If
            
                    Next
		
                    Return EditorialCollection
            End Select
        End Get
    End Property
       	           
    
    Sub BindData(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim editorialCollection As ContentEditorialCollection = ReadSource() 'GenericCollection
                
                LoadList(editorialCollection, List)
        End Select
    End Sub
    
    Function ReadSource() As ContentEditorialCollection
        Dim _editorialCollection As New ContentEditorialCollection
           
        Select Case TypeControl
            Case ControlType.GenericRelation
                _editorialCollection = ReadContentEditorialRelation()
                 
        End Select
        
        Return _editorialCollection
    End Function
    
    Public Function ReadContentEditorialRelation() As ContentEditorialCollection
        Dim _contentManager As New ContentManager
        Dim contentEditorialColl As New ContentEditorialCollection
        Dim editorialEditorial As New ContentIdentificator()
        
        editorialEditorial.Id = idRelated
       
        If idRelated = 0 Then Return Nothing
        
        _contentManager.Cache = False
        contentEditorialColl = _contentManager.ReadContentEditorial(editorialEditorial)
               
        Return contentEditorialColl
    End Function
    
    Sub LoadList(ByVal EditorialCollection As Object, ByVal List As ListControl)
        Dim editorialValue As ContentEditorialValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        List.Items.Clear()
        If Not EditorialCollection Is Nothing Then
            For Each editorialValue In EditorialCollection
                oListItem = New ListItem(editorialValue.Description, editorialValue.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
               
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                pnl = FindControl("pnlGeneric")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Disable()
        ListBox1.Enabled = False
    End Sub
    
    Sub Enabled()
        ListBox1.Enabled = True
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
    
    Public Sub clear()
        ListBox1.Items.Clear()
        txtHidden.Text = ""
    End Sub
    
</script>	

<asp:Panel ID="pnlGeneric" runat="server">
  	<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
            <td valign="top">
                <asp:ListBox id="ListBox1"   runat="server" width="200px"></asp:ListBox>
            </td>
            <td>
                 <%If Enable() Then%>
                     <input type="button" class="button" id="btnAree" value ='Insert Editorial' onClick="window.open('<%=strDomain%>/Popups/popEditorials.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')" /><br /><br />
                     <input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')" />
                 <%Else%>
                     <asp:Button ID="btInsert"  CssClass="button" Text="Insert" Enabled="false" runat="server" /><br /><br />
                     <asp:Button ID="btDelete"  CssClass="button" Text="Delete" Enabled="false" runat="server" />
                 <%End If%>
               
            </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
