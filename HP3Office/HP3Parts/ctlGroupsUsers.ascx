<%@ Control Language="VB" ClassName="ctlGroupsUsers" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@Import Namespace ="System.Web.UI.WebControls" %>

<script runat="server"> 
    Dim userManager As New UserManager
    Dim newUser As UserValue
    Dim userColl As New UserCollection
    Dim userSearcher As New UserSearcher
    
    Sub Page_Load()
        If Not Page.IsPostBack Then userColl = userManager.Read(userSearcher)
    End Sub
    
    Sub Node_Populate(ByVal sender As Object, ByVal e As TreeNodeEventArgs)
        If (e.Node.ChildNodes.Count = 0) Then
            Select Case e.Node.Depth
                Case 0
                    FillGroups(e.Node)
                Case 1
                    FillDescription(e.Node)
            End Select
        End If
    End Sub
    
    Sub FillGroups(ByVal node As TreeNode)
        Dim i As Integer
        Dim newNode As TreeNode
        Dim item As New UserValue
        For Each item In userColl
            Response.Write(item.Surname)
            newNode = New TreeNode(item.Surname)
            newNode.PopulateOnDemand = True
            newNode.SelectAction = TreeNodeSelectAction.Expand
            node.ChildNodes.Add(newNode)
        Next
    End Sub
    
    Sub FillDescription(ByVal node As TreeNode)
        Dim i As Integer
        Dim newNode As TreeNode
             
        For i = 0 To 5
            newNode = New TreeNode("Livello2")
            newNode.PopulateOnDemand = True
            newNode.SelectAction = TreeNodeSelectAction.None
            node.ChildNodes.Add(newNode)
        Next
    End Sub
    
    'Sub Node_Populate(ByVal sender As Object, ByVal e As TreeNodeEventArgs)
    '    Response.Write("In")
    '    If (e.Node.ChildNodes.Count = 0) Then
    '        Select Case e.Node.Depth
    '            Case 0
    '                FillGroups(e.Node)
    '            Case 1
    '                FillDescription(e.Node)
    '        End Select
    '    End If
    'End Sub
    
    'Sub FillGroups(ByVal node As TreeNode)
    '    Dim userGroupManager As New UserGroupManager
    '    Dim userGoupSearcher As New UserGroupSearcher
    '    Dim userGroupColl As New UserGroupCollection
    '    Dim elem As New UserGroupValue
    '    Dim newNode As TreeNode
        
    '    userGroupColl = userGroupManager.Read(userGoupSearcher)
    '    Response.Write("dim" & userGroupColl.Count)
    '    For Each elem In userGroupColl
    '        newNode = New TreeNode(elem.Description)
    '        newNode.PopulateOnDemand = True
    '        newNode.SelectAction = TreeNodeSelectAction.Expand
    '        node.ChildNodes.Add(newNode)
    '    Next
    'End Sub
    
    'Sub FillDescription(ByVal node As TreeNode)
    '    Dim userGroupManager As New UserGroupManager
    '    Dim userGoupSearcher As New UserGroupSearcher
    '    Dim userGroupColl As New UserGroupCollection
    '    Dim elem As New UserGroupValue
    '    Dim newNode As TreeNode
    '    userGroupColl = userGroupManager.Read(userGoupSearcher)
       
    '    For Each elem In userGroupColl
    '        newNode = New TreeNode(elem.Description)
    '        newNode.PopulateOnDemand = True
    '        newNode.SelectAction = TreeNodeSelectAction.None
    '        node.ChildNodes.Add(newNode)
    '    Next
    'End Sub
</script>
    
<asp:TreeView runat="server" ExpandImageUrl="~/hp3Office/HP3Image/Tree/plus.gif" CollapseImageUrl="~/hp3Office/HP3Image/Tree/minus.gif" OnTreeNodePopulate="Node_Populate" id="tvwauthors">
    <Nodes>
        <asp:TreeNode Text="Groups" PopulateOnDemand="true" Value="0" />
    </Nodes>
</asp:TreeView>