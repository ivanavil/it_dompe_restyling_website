<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ctlContextTree.ascx.vb" 
    Inherits="hp3Office_HP3Parts_ctlContextTree" ClassName="ctlContextTree" %>
<%@ Register TagName="ContentRelationEdit" TagPrefix="HP3" Src="~/hp3Office/HP3Common/ContentRelationEdit.ascx"  %>
<script type="text/javascript">
    function hideShowPanel(img, panelId) {
        
        var panel = document.getElementById(panelId);
        if (panel) {
            if (panel.style.display == "none") {
                img.src = "/HP3Office/HP3Image/Tree/minus.gif";
                panel.style.display = "block";
            } else {
                img.src = "/HP3Office/HP3Image/Tree/plus.gif";
                panel.style.display = "none";
            }
        }
    }
</script>
<%--<asp:Panel ID="pnlTree" runat="server">
</asp:Panel>--%>
<asp:Table ID="tblContexts" Width="100%" GridLines="Both" runat="server">
<%--    
    <asp:TableHeaderRow Visible="true">
    </asp:TableHeaderRow>
--%>    
    <asp:TableFooterRow Visible="true">
        <asp:TableCell ColumnSpan="2"></asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>
