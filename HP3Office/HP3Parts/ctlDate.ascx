<%@ Control Language="VB" ClassName="ctlDate" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>

<script runat="server">
    Dim strDomain As String
    Dim utility As New StringUtility
    Dim masterPageManager As MasterPageManager
    
    Dim _style As String
    
    Public Enum ControlType
        PopupCalendar = 1
        JsCalendar
        Combo
    End Enum
    
    
    Private _typecontrol As ControlType
    Private _showTime As Boolean = False
    Private _showDate As Boolean = True
    Private _culture As String = ""
    Private _enable As Boolean = True
    
    ''' <summary>
    ''' Abilita o meno la visualizzazione della casella di testo per l'inserimento dell' ora
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ShowTime() As Boolean
        Get
            Return _showTime
        End Get
        Set(ByVal value As Boolean)
            _showTime = value
        End Set
    End Property
    
    Public Property Culture() As String
        Get
            masterPageManager = New MasterPageManager
            If (masterPageManager.GetLang.Code = "IT") Then
                Return "%d/%m/%Y"
            Else
                Return "%m/%d/%Y"
            End If
        End Get
        Set(ByVal value As String)
            _culture = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property ShowDate() As Boolean
        Get
            Return _showDate
        End Get
        Set(ByVal value As Boolean)
            _showDate = value
        End Set
    End Property
       
    Public Property Enable() As Boolean
        Get
            Return _enable
        End Get
        Set(ByVal value As Boolean)
            _enable = value
        End Set
    End Property
    
    Public Title As String = ""
    
    Public Property Value() As Nullable(Of DateTime)
        Get
            Dim outDate As Nullable(Of DateTime) = Nothing
            Dim culture As System.Globalization.CultureInfo
            
            Select Case TypeControl
                Case ControlType.PopupCalendar, ControlType.JsCalendar
                    'se la lingua di navigazione del sito � l'inglese statunitense 
                    'allora viene impostato il formato utilizzato negli US per l'ora e la data 
                    masterPageManager = New MasterPageManager
                    If (masterPageManager.GetLang.Code = "EN") Then
                        culture = New System.Globalization.CultureInfo("en-US")
                        
                        If txtCalendar.Text <> "" And Not txtTime.Visible Then
                            'outDate = DateTime.Parse(txtCalendar.Text)
                            outDate = DateTime.Parse(txtCalendar.Text, culture)
                        End If
                        If txtCalendar.Text <> "" And txtTime.Text <> "" Then
                            Try
                                'outDate = DateTime.Parse(txtCalendar.Text & " " & txtTime.Text)
                                outDate = DateTime.Parse(txtCalendar.Text & " " & txtTime.Text, culture)
                            Catch
                                Return Nothing
                            End Try
                        End If
                    Else 'in tutti gli atri casi usiamo il formato italiano
                        culture = New System.Globalization.CultureInfo("it-IT")
                        
                        If txtCalendar.Text <> "" And Not txtTime.Visible Then
                            outDate = DateTime.Parse(txtCalendar.Text, culture)
                        End If
                        If txtCalendar.Text <> "" And txtTime.Text <> "" Then
                            Try
                                outDate = DateTime.Parse(txtCalendar.Text & " " & txtTime.Text, culture)
                            Catch
                                Return Nothing
                            End Try
                        End If
                    End If
              
                Case ControlType.Combo
                    Try
                        outDate = New DateTime(drpAnno.SelectedItem.Text, drpMese.SelectedItem.Text, drpGiorno.SelectedItem.Text)
                    Catch ex As Exception
                        Throw ex
                    End Try
            End Select
           
            Return outDate
        End Get
        
        Set(ByVal value As Nullable(Of DateTime))
            Dim formatDate As String = ""
            Dim formatTime As String = ""
            
            Select Case TypeControl
                Case ControlType.PopupCalendar, ControlType.JsCalendar
                    'se la lingua di navigazione del sito � l'inglese statunitense 
                    'allora viene impostato il formato utilizzato negli US per l'ora e la data 
                    masterPageManager = New MasterPageManager
                                        
                    If (masterPageManager.GetLang.Code = "EN") Then
                        formatTime = "hh:mm"
                        txtCalendar.Text = utility.FormatDateToEnglish(value.Value)
                        If ShowTime Then txtTime.Text = utility.FormatDateTimeToEnglish(value.Value, formatTime)
                    Else
                        'in tutti gli atri casi usiamo il formato italiano
                        formatTime = "HH:mm"
                        txtCalendar.Text = utility.FormatDateToItalian(value.Value)
                        If ShowTime Then txtTime.Text = utility.FormatDateTimeToItalian(value.Value, formatTime)
                    End If
                   
                Case ControlType.Combo
                    Try
                        drpAnno.SelectedIndex = drpAnno.Items.IndexOf(drpAnno.Items.FindByText(value.Value.Year))
                        drpMese.SelectedIndex = drpMese.Items.IndexOf(drpMese.Items.FindByText(value.Value.Month))
                        drpGiorno.SelectedIndex = drpGiorno.Items.IndexOf(drpGiorno.Items.FindByText(value.Value.Day))
                    Catch ex As Exception
                        drpAnno.SelectedIndex = 0
                        drpMese.SelectedIndex = 0
                        drpGiorno.SelectedIndex = 0
                    End Try
            End Select
            
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub Page_Init()
        ShowPanel()
        ShowRightPanel()
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.PopupCalendar, ControlType.JsCalendar
                pnl = FindControl("pnlCalendar")
                
                If ShowTime Then
                    txtTime.Visible = True
                    txtTime.Text = DateTime.Now.ToShortTimeString
                End If
               
                If (ShowDate) Then
                    CreateDate()
                End If
               
            Case ControlType.Combo
                pnl = FindControl("pnlCombo")
                If Not Page.IsPostBack Then
                    LoadComboDate()
                End If
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
        
       
    End Sub
    
    Sub Page_load()
        'txtTime.Attributes.Add("onkeypress", "return (window.event.keyCode >= 48 && window.event.keyCode <=58)")
        'txtTime.Attributes.Add("onkeydown", "return GestChange(this)")
        txtTime.Attributes.Add("onpaste", "return false")
        txtTime.Attributes.Add("onblur", "isDataValid(this)")
        'ShowRightPanel()
        
        strDomain = WebUtility.MyPage(Me).DomainInfo
        txtCalendar.Attributes.Add("onkeypress", "return false")
        txtCalendar.Attributes.Add("onpaste", "return false")
    End Sub
    
        
    Sub LoadComboDate()
        Dim i As Int16
        Dim Year As Int16 = DateTime.Now.Year
        
        For i = 1 To 31
            drpGiorno.Items.Add(i)
        Next
        
        For i = 1 To 12
            drpMese.Items.Add(i)
        Next
        
        For i = 0 To 20
            drpAnno.Items.Add(Year - i)
        Next
    End Sub
    
    Public Property Style() As String
        Get
            Return _style
        End Get
        Set(ByVal value As String)
            Dim arr() As String = value.Split(";")
            Dim arr1() As String
            
            For Each str As String In arr
                If str <> "" Then
                    arr1 = str.Split(".")
                    Select Case TypeControl
                        Case ControlType.PopupCalendar, ControlType.JsCalendar
                            txtCalendar.Style.Add(arr1(0), arr1(1))
                    End Select
                End If
            Next
        End Set
    End Property
    
    Sub CreateDate()
        Me.Value = Date.Now
    End Sub
    
    Public Sub Clear()
        Select Case TypeControl
            Case ControlType.PopupCalendar, ControlType.JsCalendar
                txtCalendar.Text = ""
                txtTime.Text = ""
            Case ControlType.Combo
                
                drpAnno.SelectedIndex = 0
                drpMese.SelectedIndex = 0
                drpGiorno.SelectedIndex = 0
        End Select
    End Sub
    
    Public Sub Disable()
		_enable = false
        txtCalendar.Enabled = False
        txtTime.Enabled = False
        btnCalendar.Enabled = False
        'btnCalendarClear.Disabled = True
    End Sub
    
    Public Sub Enabled()
        txtCalendar.Enabled = True
        txtTime.Enabled = True
        btnCalendar.Enabled = True
        'btnCalendarClear.Disabled = True
    End Sub
</script>

<asp:panel ID="pnlCalendar" runat="server" >
    <asp:TextBox ID="txtCalendar" runat="server" Columns ="10" ToolTip ="Dat"/>
    <asp:textbox ID ="txtTime" runat="server" MaxLength="8"  Columns="6"  Visible="false" ToolTip ="Time"/>
    <%--onclick="window.open('<%=strDomain%>/Popups/Calendar.aspx?title=<%=Title%>&field_name=<%=txtCalendar.clientId%>','',' width=200,height=150,status=0')"--%>
    <asp:Button ID="btnCalendar" runat="server" Text="..." CssClass="button" />
    <%If Enable() Then%>
        <input  id="btnCalendarClear" value="Clear" type="button"  onclick="<%=txtCalendar.clientid%>.value=''" class="button"  />
   <%Else%>
        <input  id="btCal" value="Clear"  disabled="disabled" type="button"  class="button"  />
   <%End If%>
   </asp:panel>
   
<asp:panel ID="pnlCombo" runat="server">
    <ASP:dropdownlist ID="drpGiorno" runat="server"/>
    <ASP:dropdownlist ID="drpMese" runat="server"/>
     <ASP:dropdownlist ID="drpAnno" runat="server"/>
</asp:panel>
<script type="text/javascript">

//  function isDataValid(src)
//    {
//        var arr = src.value.split('.')
//        var bValid= ((parseInt(arr[0]) >= 0) && (parseInt(arr[0]) <= 23)) && ((parseInt(arr[1]) >= 0) && (parseInt(arr[1]) <= 59))
//        if (! bValid) src.focus();
//    }
    
  function GestChange(src)
     {            
//        if (window.event.keyCode == 9)
//            {
//                if (src.value.length < 4 )                
//                    return false;                    
//                else
//                    return true;
//            }
            
        if ((window.event.keyCode >= 0 && window.event.keyCode <=256) || (window.event.keyCode == 8) || (window.event.keyCode == 9)  || (window.event.keyCode == 46))
          {
//                if ((src.value.length == 3) && (window.event.keyCode == 8))
//                    {                        
//                        src.value=src.value.substring(1,src.value.lenght - 1);
//                        return false;
//                    }
                    
                if (src.value.length == 2)
                    switch (window.event.keyCode)
                        {    
                            //case 8:
                            //break;                        
                            //case 46:
                            //    break;
                            default:
                            src.value += '.'
                                //break;
                       }
            }
        else
           return false;             
        }
    
  Calendar.setup(
    {
      inputField  : "<%=txtCalendar.clientid %>",
      ifFormat    : "<%=Culture()%>",
      button      : "<%=btnCalendar.clientid %>"
    }
  );
</script>
