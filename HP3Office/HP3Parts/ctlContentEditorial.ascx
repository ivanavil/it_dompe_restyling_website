<%@ Control Language="VB" ClassName="ctlContentEditorial" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="server">
    Private strDomain As String
    Private _idLingua As Int16
    Private _idRelated As Int32
    Private nuovo As Boolean
    Private hsLink As Hashtable
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
             
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare il content editorial
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
    Public Property IsNew() As Boolean
        Get
            Return nuovo
        End Get
        Set(ByVal Value As Boolean)
            nuovo = Value
        End Set
    End Property
    
    Sub Page_Load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        hsLink = GenericDataContainer.Item("SiteLinks")
        
      
    End Sub
    
    Function GetThemeValue(ByVal Description As String) As ThemeCollection
        Dim oThemeSearcher As New ThemeSearcher
        Dim oThemeManager As New ThemeManager
        oThemeManager.Cache = False
        
        oThemeSearcher.Description = Description
        Return oThemeManager.Read(oThemeSearcher)
        
    End Function
    
    Function ReadKey(ByVal Key As String, ByVal isNew As Boolean) As String
        Dim oThemeCollection As ThemeCollection = GetThemeValue(Key)
        
        If Not oThemeCollection Is Nothing AndAlso oThemeCollection.Count = 1 Then
            Key = oThemeCollection.Item(0).Key.Id
            
            If Not hsLink Is Nothing AndAlso hsLink.ContainsKey(key) Then
                       
                Dim oWebOut As WebRequestValue = hsLink(Key)
                Dim TypeControlParam As String = "0"
                Dim mPageManager As New MasterPageManager
            
                Dim objQs As New ObjQueryString(mPageManager.FormatRequest(oWebOut))
            
                If isNew Then
                    objQs.ContentId = 0
                Else
                    objQs.ContentId = IIf(txtContentEditorialId.Text = "", 0, txtContentEditorialId.Text)
                End If
            
                oWebOut.customParam.append("TypeControl", TypeControlParam)
                oWebOut.customParam.append(objQs)
            
            
           
                Return mPageManager.FormatRequest(oWebOut)
            Else
                Return Request.Url.ToString
            End If
        Else
            Return Request.Url.ToString
        End If
    End Function
    
    ''' <summary>
    ''' Restituisce la collezione di ContentEditorialValue relative alla selezione
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetSelection() As ContentEditorialCollection
        Get
            Dim oContentEditorialValue As ContentEditorialValue
            Dim oContentEditorialCollection As New ContentEditorialCollection
            Dim oGenericUtility As New GenericUtility
            
            Dim id As Int32 = IIf(txtContentEditorialId.Text = "", 0, txtContentEditorialId.Text)
            oContentEditorialValue = oGenericUtility.GetContentEditorial(id)
            
            If Not oContentEditorialValue Is Nothing Then
                oContentEditorialCollection.Add(oContentEditorialValue)
            End If
            
            Return oContentEditorialCollection         
        End Get
    End Property
    
    ''' <summary>
    ''' Caricamento dei dati nel controllo sulla base del content 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Sub LoadControl()
       
        Dim oContentManager As New ContentManager
        oContentManager.Cache = False
        Dim oContentEditorialCollection As ContentEditorialCollection = oContentManager.ReadContentEditorial(New ContentIdentificator(idRelated, idLanguage), 0)
       
        If Not oContentEditorialCollection Is Nothing AndAlso oContentEditorialCollection.Count > 0 Then
            txtContentEditorialDesc.Text = oContentEditorialCollection.Item(0).Description
            txtContentEditorialId.Text = oContentEditorialCollection.Item(0).Id
        End If
    End Sub
    
    Sub GoToEdit(ByVal s As Object, ByVal e As EventArgs)        
        Response.Redirect(ReadKey("Content Editorial", False))
    End Sub
    
    Sub GoToNew(ByVal s As Object, ByVal e As EventArgs)
        Response.Redirect(ReadKey("Content Editorial", True))
    End Sub
    
    Sub RemoveAcc(ByVal s As Object, ByVal e As EventArgs)
        txtContentEditorialDesc.Text = ""
        txtContentEditorialId.Text = ""
    End Sub
</script>
<table width="100%">
    <tr>
        <td width="50px">
            <asp:TextBox ID="txtContentEditorialId" runat="server" style="display:none"/>
            <HP3:Text runat ="server" ID="txtContentEditorialDesc"  TypeControl="TextBox"  style="width:300px" isReadOnly="true"/>    
        </td>
        <td>
            <input type="button" value="..." onclick="window.open('<%=strDomain%>/Popups/popContentEditorial.aspx?SelItem=' + document.getElementById('<%=txtContentEditorialId.clientid%>').value + '&ctlHidden=<%=txtContentEditorialId.clientid%>&ctlVisible=<%=txtContentEditorialDesc.clientid%>','','width=780,height=480,scrollbars=yes')"  class="button"/>
            <asp:button runat="server" text="Remove" onclick="RemoveAcc"  CssClass="button"/>
            <asp:button runat="server" text="Edit" onclick="GoToEdit" CssClass="button" />
            <asp:button runat="server" text="New" onclick="GoToNew" CssClass="button" />
        </td>        
    </tr>
</table>        