<%@ Control Language="VB" ClassName="ctlText" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">
    Dim _style As String
    
    Public Enum ControlType
        TextBox = 1
        TextArea = 2
        NumericText = 3
        TextEmail = 4
        PathSelector
    End Enum
    
    Private _typecontrol As ControlType
    Private _readonly As Boolean
    Private strDomain As String
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
      
    Public ReadOnly Property GetControl() As Object
        Get
            Select Case TypeControl
                Case ControlType.TextBox, ControlType.NumericText, ControlType.TextEmail
                    Return txtSingle
                Case ControlType.TextArea
                    Return txtMulti
                Case ControlType.PathSelector
                    Return txtFolder
            End Select
            Return Nothing
        End Get
    End Property
    
    Public Property Style() As String
        Get
            Return _style
        End Get
        Set(ByVal value As String)
            Dim arr() As String = value.Split(";")
            Dim arr1() As String
            
            
            For Each str As String In arr
                If str <> "" Then
                    arr1 = str.Split(":")
               
                    Select Case TypeControl
                        Case ControlType.TextBox, ControlType.NumericText, ControlType.TextEmail
                            txtSingle.Style.Add(arr1(0), arr1(1))
                        Case ControlType.TextArea
                            txtMulti.Style.Add(arr1(0), arr1(1))
                        Case ControlType.PathSelector
                            txtFolder.Style.Add(arr1(0), arr1(1))
                    End Select
                End If
            Next
        End Set
    End Property
     
    Public ReadOnly Property Attributes() As System.Web.UI.AttributeCollection
        Get
            Select Case TypeControl
                Case ControlType.TextBox, ControlType.NumericText, ControlType.TextEmail
                    Return txtSingle.Attributes
                Case ControlType.TextArea
                    Return txtMulti.Attributes
                Case ControlType.PathSelector
                    Return txtFolder.Attributes
            End Select
        End Get
        
    End Property
        
    Public Property Text() As String
        Get
            Select Case TypeControl
                Case ControlType.TextBox, ControlType.NumericText, ControlType.TextEmail
                    Return txtSingle.Text
                Case ControlType.TextArea
                    Return txtMulti.Text
                Case ControlType.PathSelector
                    Return txtFolder.Text
            End Select
        End Get
        
        Set(ByVal value As String)
            Select Case TypeControl
                Case ControlType.TextBox, ControlType.NumericText, ControlType.TextEmail
                    txtSingle.Text = value
                Case ControlType.TextArea
                    txtMulti.Text = value
                Case ControlType.PathSelector
                    txtFolder.Text = value
            End Select
        End Set
    End Property
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel
        strDomain = WebUtility.MyPage(Me).DomainInfo
        
        Select Case TypeControl
            Case ControlType.TextArea
                pnl = FindControl("pnlTextArea")
            Case ControlType.TextBox
                pnl = FindControl("pnlTextBox")
            Case ControlType.NumericText
                pnl = FindControl("pnlTextBox")
                txtSingle.Attributes.Add("onkeypress", "return text_Change()")
                txtSingle.Attributes.Add("onpaste", "return false")
            Case ControlType.TextEmail
                pnl = FindControl("pnlTextBox")
                txtSingle.Attributes.Add("onblur", "checkMail(this)")
            Case ControlType.PathSelector
                pnl = FindControl("pnlUpload")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
    
    Public Property MaxLength() As Int16
        Get
            Select Case TypeControl
                Case ControlType.TextArea
                    Return txtMulti.MaxLength
                Case ControlType.NumericText, ControlType.TextBox, ControlType.TextEmail
                    Return txtSingle.MaxLength
            End Select
        End Get
        Set(ByVal value As Int16)
            Select Case TypeControl
                Case ControlType.TextArea
                    txtMulti.MaxLength = value
                Case ControlType.NumericText, ControlType.TextBox, ControlType.TextEmail
                    txtSingle.MaxLength = value
            End Select
        End Set
    End Property
    
    Public Overrides ReadOnly Property ClientID() As String
        Get
            Select Case TypeControl
                Case ControlType.PathSelector
                    Return txtFolder.ClientID
                Case ControlType.TextArea
                    Return txtMulti.ClientID
                Case ControlType.NumericText, ControlType.TextBox, ControlType.TextEmail
                    Return txtSingle.ClientID
            End Select
        End Get
    End Property
    
    Public Property isReadOnly() As Boolean
        Get
            Return _readonly
        End Get
        Set(ByVal value As Boolean)
            _readonly = value
            Select Case TypeControl
                Case ControlType.PathSelector
                    txtFolder.ReadOnly = _readonly
                Case ControlType.TextArea
                    txtMulti.ReadOnly = _readonly
                Case ControlType.NumericText, ControlType.TextBox, ControlType.TextEmail
                    txtSingle.ReadOnly = _readonly
            End Select
        End Set
    End Property
    
    Sub Disable()
        txtSingle.Enabled = False
        txtMulti.Enabled = False
    End Sub
    
    Sub Enable()
        txtSingle.Enabled = True
        txtMulti.Enabled = True
    End Sub
</script>

<asp:panel ID ="pnlTextBox" runat="server">
    <asp:TextBox ID="txtSingle" runat="server"/>
</asp:panel>
<asp:panel ID ="pnlTextArea" runat="server">
    <asp:TextBox ID="txtMulti" runat="server" TextMode ="MultiLine" Rows="5" />
</asp:panel>
<asp:Panel ID="pnlUpload" runat="server">
    <asp:TextBox ID="txtFolder" runat="server" />
    <input type="button" value="Browse" onclick="window.open('<%=strDomain%>/Popups/FileSystemNavigator.aspx?ctl=<%=txtFolder.clientid%>','','width=500,height=500,scrollbars=yes')" class="button" />
</asp:Panel>
<script type ="text/javascript" >
    function text_Change()
        {
            return (window.event.keyCode >= 48 && window.event.keyCode <=57)                
        }
        
</script>