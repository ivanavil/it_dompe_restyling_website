<%@ Control Language="VB" ClassName="ctlNlist" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">   
    Public Enum ControlType        
        GenericRelation
    End Enum
    
#Region "Membri"    
    Private _innerArea As String
    Private _enabled As Boolean
    Private nuovo As Boolean = False
    Private _typecontrol As ControlType = ControlType.GenericRelation
    Private strDomain As String
    Private _GenericCollection As NewsletterListCollection
#End Region
           
#Region "Properties"
        
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                                                             
                BindData(ListBox1)
                           
        End Select
    End Sub    
       
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
                  
             
#End Region
    Function ReadLabel(ByVal id As Integer) As NewsletterListValue
        Dim Manager As New NewsletterManager
        Dim vo As NewsletterListValue
                
        vo = Manager.Read(id)
       
        Return vo
    End Function
       
  
    Public ReadOnly Property GetSelection() As NewsletterListCollection
        Get
            Dim pCollection As New NewsletterListCollection
            Dim pValue As NewsletterListValue
            
            Select Case TypeControl
                Case ControlType.GenericRelation
                    Dim item As ListItem
                           
                    Dim strOut As String = ""
                    LoadItems()
		
                    For Each item In ListBox1.Items
                        pValue = ReadLabel(item.Value)
            
                        If Not pValue Is Nothing Then
                            pCollection.Add(pValue)
                        End If
            
                    Next
		
                    Return pCollection
            End Select
           
        End Get
        
    End Property
       	           
    
    Sub BindData(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim _pCollection As NewsletterListCollection = GenericCollection
               
                LoadList(_pCollection, List)
        End Select
    End Sub
    
    Sub LoadList(ByVal pCollection As Object, ByVal List As ListControl)
        Dim _pValue As NewsletterListValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        List.Items.Clear()
        
        If Not pCollection Is Nothing Then
            For Each _pValue In pCollection
                oListItem = New ListItem(_pValue.Description, _pValue.Key)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
               
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                pnl = FindControl("pnlGeneric")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
    
    Public Sub clear()
        ListBox1.Items.Clear()
        txtHidden.Text = ""
    End Sub
</script>	

<asp:Panel ID="pnlGeneric" runat="server">
    
	<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td valign="top">
                    <asp:ListBox id="ListBox1"   runat="server" width="200px"></asp:ListBox>
                </td>
                <td>
					<input type="button" class="button" id="btnAree" value ='Insert NList' onClick="window.open('<%=strDomain%>/Popups/popNLists.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')"/>
                    <br />
					
					<input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>
                 
                </td>
            </tr>
        </tbody>
    </table>

</asp:Panel>
