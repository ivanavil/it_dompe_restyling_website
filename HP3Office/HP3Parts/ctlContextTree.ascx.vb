Imports System.Collections.Generic
Imports Healthware.HP3.Core.Base
Imports Healthware.HP3.Core.Base.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Site
Imports Healthware.HP3.Core.Site.ObjectValues
Imports Healthware.HP3.Core.Web
Imports Healthware.HP3.Core.Web.ObjectValues
Imports Healthware.HP3.Core.Utility

'Public Delegate Function getContextDataSourceHandler() As Object
'Public Delegate Function getContextRelatedDataSourceHandler(ByVal keyContent As ContentIdentificator) As List(Of Integer)

Partial Class hp3Office_HP3Parts_ctlContextTree
    Inherits System.Web.UI.UserControl

    'Public getContextDataSourceHandler1 As getContextDataSourceHandler
    'Public getContextRelatedDataSourceHandler1 As getContextRelatedDataSourceHandler

    Private objQs As New ObjQueryString
    Private webRequest As New WebRequestValue

    Private _keyContent As ContentIdentificator = Nothing

    Private _cache As PersistentCache

    Private _relationEdit As hp3Office_HP3Common_ContentRelationEdit

    Private nodes As Dictionary(Of String, ContextNode) = Nothing

    Private _dataSource As ContextCollection
    Private _related As List(Of Integer) = Nothing
    Private _changes As Dictionary(Of Integer, Boolean) = Nothing

    Public Property RelationEdit() As hp3Office_HP3Common_ContentRelationEdit
        Get
            Return _relationEdit
        End Get
        Set(ByVal value As hp3Office_HP3Common_ContentRelationEdit)
            _relationEdit = value
        End Set
    End Property

    Public Function saveRelatedStatus() As Boolean

        If RelationEdit Is Nothing Then Return False

        If nodes.Count = 0 Then Return False

        Dim isChanges As Boolean = False

        Dim check As CheckBox
        Dim radio As RadioButton
        Dim checked As Boolean = False
        Dim idRelation As Integer
        ' legge ogni elemento dell'albero
        For Each node As ContextNode In nodes.Values
            ' azzera la variabile idRelation
            idRelation = -1
            ' se ci troviamo in multiselect abbiamo a che fare con dei checkbox, altrimenti con dei radiobutton
            If RelationEdit.AllowEdit Then
                ' estrae il checkbox
                check = CType(node.row.Cells(0).Controls(1), CheckBox)
                If Not check Is Nothing AndAlso check.Text <> "" Then
                    ' la propriet� text del checkbox contiene l'id della relazione
                    idRelation = Integer.Parse(check.Text)
                    ' verifica se la relazione � stata impostata
                    checked = check.Checked
                End If
            Else
                ' estrae il radiobutton
                radio = CType(node.row.Cells(0).Controls(1), RadioButton)
                If Not radio Is Nothing AndAlso radio.Text <> "" Then
                    ' la propriet� text del radiobutton contiene l'id della relazione
                    idRelation = Integer.Parse(radio.Text)
                    ' verifica se la relazione � stata impostata
                    checked = radio.Checked
                End If
            End If
            ' verifica se idrelation � stata valorizzata
            If idRelation > -1 Then
                ' verifica se lo stato della relazione attuale � cambiato rispetto a quello iniziale
                If (RelationEdit.checkRelated(idRelation, True) AndAlso Not checked) OrElse (Not RelationEdit.checkRelated(idRelation, True) AndAlso checked) Then
                    ' aggiorna l'hashmap dei cambiamenti
                    If RelationEdit.RelatedChanges.ContainsKey(idRelation) Then
                        RelationEdit.RelatedChanges.Item(idRelation) = checked
                    Else
                        RelationEdit.RelatedChanges.Add(idRelation, checked)
                    End If
                    ' imposta il flag
                    isChanges = True
                ElseIf RelationEdit.checkRelated(idRelation, True) <> RelationEdit.checkRelated(idRelation, False) Then
                    ' aggiorna l'hashmap dei cambiamenti
                    RelationEdit.RelatedChanges.Remove(idRelation)
                    ' imposta il flag
                    isChanges = True
                End If
            End If
        Next

        Return ischanges

    End Function

    Public Overloads Sub DataBind()

        If RelationEdit Is Nothing Then Return

        renderTree()

    End Sub

    Private Sub renderTree()

        If RelationEdit Is Nothing OrElse RelationEdit.getDataSourceHandler1 Is Nothing Then Return

        ClearNodes()
        If nodes Is Nothing Then nodes = New Dictionary(Of String, ContextNode)

        _dataSource = RelationEdit.getDataSourceHandler1()
        If Not _dataSource Is Nothing Then
            Dim context As ContextValue
            For Each context In _dataSource
                AddNode(context)
            Next
        End If

    End Sub

    Private Sub ClearNodes()

        If Not nodes Is Nothing Then
            nodes.Clear()
        End If
        tblContexts.Rows.Clear()
        Dim row As New TableRow()
        Dim cell As New TableHeaderCell()
        cell.Text = "Context Id"
        cell.Width = New Unit(30, UnitType.Percentage)
        row.Cells.Add(cell)
        cell = New TableHeaderCell()
        cell.Text = "Context Description"
        row.Cells.Add(cell)
        tblContexts.Rows.Add(row)

    End Sub

    Private Sub AddNode(ByVal context As ContextValue)

        Dim parentTable As Table = Nothing
        Dim level As Integer
        parentTable = tblContexts
        level = 0
        If context.KeyFather.Id > 0 Then
            If nodes.ContainsKey(context.KeyFather.Id & "_" & context.Key.Language.Id) Then
                Dim parentContextNode As ContextNode = nodes.Item(context.KeyFather.Id & "_" & context.Key.Language.Id)
                If Not parentContextNode Is Nothing Then
                    level = parentContextNode.level + 1

                    If parentContextNode.subTable Is Nothing Then
                        parentContextNode.subTable = New Table()
                        parentContextNode.subTable.GridLines = GridLines.Both
                        parentContextNode.subTable.Width = tblContexts.Width
                    End If
                    'parentContextNode.subTable.ID = "subPanel" & context.KeyFather.Id & "_" & context.Key.Language.Id
                    'parentContextNode.subTable.Style.Add("margin-left", "16px")
                    'parentContextNode.subTable.Style.Add("display", "none")

                    Dim subTableRow As New TableRow
                    subTableRow.ID = "subTableRow" & context.KeyFather.Id & "_" & context.Key.Language.Id
                    subTableRow.Style.Add("display", "none")
                    Dim cell2 As New TableCell
                    cell2.ColumnSpan = 2
                    cell2.Controls.Add(parentContextNode.subTable)
                    subTableRow.Cells.Add(cell2)
                    parentContextNode.table.Rows.AddAt(parentContextNode.table.Rows.GetRowIndex(parentContextNode.row) + 1, subTableRow)
                    parentTable = parentContextNode.subTable
                    parentContextNode.img.ImageUrl = "~/HP3Office/HP3Image/Tree/plus.gif"
                    parentContextNode.img.Attributes.Add("onClick", "hideShowPanel(this, '" & subTableRow.ClientID & "')")
                End If
            End If
        End If

        Dim row As New TableRow()
        Dim cell As New TableCell
        cell.Width = tblContexts.Rows(0).Cells(0).Width
        Dim img As New Image()
        img.ImageUrl = "~/HP3Office/HP3Image/Tree/filler.gif"
        img.Style.Add("width", (16 * (level + 1)) & "px")
        img.Style.Add("height", "16px")
        cell.Controls.Add(img)
        If RelationEdit.AllowEdit Then
            Dim check As New CheckBox()
            check.Checked = RelationEdit.checkRelated(context.Key.Id)
            check.Text = context.Key.Id
            check.Attributes.Add("onClick", "enableApplyButton();")
            cell.Controls.Add(check)
        Else
            Dim radio As New RadioButton()
            radio.Text = context.Key.Id
            radio.Checked = RelationEdit.checkRelated(context.Key.Id)
            radio.Attributes.Add("onClick", "enableApplyButton();")
            cell.Controls.Add(radio)
        End If
        row.Cells.Add(cell)
        cell = New TableCell
        Dim label As New Label()
        label.Text = context.Description
        cell.Controls.Add(label)
        row.Cells.Add(cell)
        parentTable.Rows.Add(row)
        Dim contextNode As New ContextNode(context.Key.Id & "_" & context.Key.Language.Id, level, row, img, parentTable)
        If Not nodes.ContainsKey(contextNode.key) Then
            'Response.Write("Errore sulla key: " & contextNode.key & "<br>")
            'Else
            nodes.Add(contextNode.key, contextNode)
        End If
    End Sub

    Private Class ContextNode

        Protected Friend key As String
        Protected Friend level As Integer
        Protected Friend row As TableRow
        Protected Friend img As Image
        Protected Friend subTable As Table
        Protected Friend table As Table

        Protected Friend Sub New(ByVal key As String, ByVal level As Integer, ByVal row As TableRow, ByVal img As Image, ByVal table As Table)
            Me.key = key
            Me.level = level
            Me.row = row
            Me.img = img
            Me.table = table
        End Sub
    End Class
End Class
