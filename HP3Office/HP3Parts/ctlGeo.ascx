<%@ Control Language="VB" ClassName="ctlLocalization" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="ctlDate" Src ="~/hp3Office/HP3Parts/ctlDate.ascx"  %>


<script runat="server">
    Public Enum ControlType
        AccContentGeo = 1
        Geo
        GeoType
        GeoList
        GeoEdit
        GeoSelection
    End Enum
    
    Private _idLingua As Int32
    Private _idRelated As Int32
    Private _userGeoId As Integer
       
    Private _oContentManager As ContentManager
    Private _oContentSearcher As ContentSearcher
    
    Private _oGeoManager As GeoManager
    Private _oGeoSearcher As GeoSearcher
    Private _oGeoCollection As GeoCollection
    Private _oCMSUtility As New GenericUtility
    
    Private strJS As String
    Private strJS2 As String
    Private _sortType As GeoGenericComparer.SortType = GeoGenericComparer.SortType.ById
    Private _sortOrder As GeoGenericComparer.SortOrder = GeoGenericComparer.SortOrder.DESC
    
    Private _mySortType As GeoGenericComparer.SortType = GeoGenericComparer.SortType.ByDescription
    
    Private objUtility As New GenericUtility
    Private _typecontrol As ControlType = ControlType.GeoList
    
    Private dictionaryManager As New DictionaryManager
    Private masterPageManager As New MasterPageManager
    Private _strDomain As String
    Private _language As String
    
    Public Property TypeControl() As ControlType
        Get
            If ViewState("TypeControl") Is Nothing Then
                ViewState("TypeControl") = _typecontrol
            End If
           
            Return ViewState("TypeControl")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("TypeControl") = value
        End Set
    End Property
    
    Public Property NewItem() As Boolean
        Get
            Return ViewState("NewItem")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NewItem") = value
        End Set
    End Property
    
    Public Property SelectedIdGeo() As Int32
        Get
            Return ViewState("SelectedIdContentEditorial")
        End Get
        Set(ByVal value As Int32)
            ViewState("SelectedIdContentEditorial") = value
        End Set
    End Property
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    ''' <summary>
    ''' Carica i controlli innestati nel controllo principale
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadSubControl()
        Select Case TypeControl
            Case ControlType.AccContentGeo
                'Carica se stesso con un type diverso
                Dim objGeoVis As Object = Me.Page.LoadControl("~/hp3Office/HP3Parts/ctlGeo.ascx")
                
                objGeoVis.TypeControl = ControlType.Geo
                objGeoVis.loadcontrol()
                GeoVisualization.Controls.Add(objGeoVis)
                            
            Case ControlType.GeoEdit
                'Carica se stesso con un type diverso
                Dim objGeoVis As Object = Me.Page.LoadControl("~/hp3Office/HP3Parts/ctlGeo.ascx")
                
                objGeoVis.TypeControl = ControlType.Geo
                objGeoVis.loadcontrol()
                GeoSelect.Controls.Add(objGeoVis)
        End Select
    End Sub
    
    ''' <summary>
    ''' Metodo public necessario per il caricamento del controllo stesso
    ''' In base al parametro TypeControl effettua le sue scelte
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.AccContentGeo
                If Not Page.IsPostBack Then
                    BindRelatedGeo()
                End If
              
            Case ControlType.Geo
                BindAllGeo()
                                        
            Case ControlType.GeoList
                If Not Page.IsPostBack Then
                    BindWithSearch(gridListGeo)
                   
                End If
            Case ControlType.GeoEdit
                tdGeoType.Visible = False
                
                LoadFormDett(LoadGEOValue)
                BindComboType(drbListType)
                
        End Select
        
        LoadSubControl()
    End Sub
    
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
    
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare l'area
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
    Public Property UserGeoId() As Integer
        Get
            Return _userGeoId
        End Get
        Set(ByVal value As Integer)
            _userGeoId = value
        End Set
    End Property
       

    Sub LoadFatherControl(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim GeoM As New GeoManager
        Dim geoType As New GeoSearcher
        Dim geoColl As New GeoCollection
        
        
        geoType.geoTypeDescription = dwlgeoType.SelectedItem.Text
        geoColl = GeoM.Read(geoType)
        
        Dim oWebUtil As New WebUtility
       
        
        'Combo dei themi per la selezione del padre
        drpThemeFather.Items.Clear()
        oWebUtil.LoadListControl(drpThemeFather, geoColl, "Description", "Key.Id")
        drpThemeFather.Items.Insert(0, New ListItem("---Select Father---", 0))
    End Sub
    
    Sub Page_load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo
        
        ShowRightPanel()
        If Not Page.IsPostBack Then
            BindComboType(dwlgeoType)
        End If
        
        
        ' LoadFatherControl()
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel
        
        Select Case TypeControl
            Case ControlType.AccContentGeo
                pnl = FindControl("pnlContentGeo")
            Case ControlType.Geo
                pnl = FindControl("pnlGeo")
            Case ControlType.GeoType
                pnl = FindControl("pnlGeoType")
            Case ControlType.GeoList
                pnl = FindControl("pnlGeoList")
                ' LoadControl()
            Case ControlType.GeoEdit
                pnl = FindControl("pnlGeoEdit")
                LoadControl()
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
       
    ''' <summary>
    '''Carica le location associate al content passato in input (parametro idRelated)
    ''' </summary>
    ''' <remarks></remarks>
    Sub BindRelatedGeo()
        Dim cPK As Int32 = objUtility.ContentPrimaryKey(idRelated, idLanguage)
        If cPK <> 0 Then
            _oContentManager = New ContentManager
            _oContentSearcher = New ContentSearcher
                
            _oContentSearcher.key.PrimaryKey = cPK
            _oGeoCollection = _oContentManager.ReadGeoRelated(_oContentSearcher)
            
            For Each geoItem As GeoValue In _oGeoCollection
                lstMain.Items.Add(New ListItem(geoItem.Description & " (" & geoItem.GeoType.Description & ")", geoItem.Key.Id))
                txtHiddenList.Text = "#" & geoItem.Key.Id & "#"
            Next
        End If
    End Sub
    
    'gestisce la visualizzazione del geo nel caso degli utenti
    Sub BindGeo()
        Dim _userGeoId As Integer = UserGeoId
        Dim _ogeoValue As New GeoValue
        If UserGeoId <> 0 Then
            _oGeoManager = New GeoManager
            _ogeoValue = _oGeoManager.Read(New GeoIdentificator(UserGeoId))
                  
            ClearList()
            If Not (_ogeoValue Is Nothing) Then
                lstMain.Items.Add(New ListItem(_ogeoValue.Description & " (" & _ogeoValue.GeoType.Description & ")", _ogeoValue.Key.Id))
                txtHiddenList.Text = "#" & _ogeoValue.Key.Id & "#"
            End If
        Else
            ClearList()
        End If
    End Sub
    
    Sub ClearList()
        lstMain.Items.Clear()
        txtHiddenList.Text = ""
    End Sub
          
    ''' <summary>
    ''' Caricamento ricorsivo per il caricamento del tree dei GeoValue
    ''' </summary>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function AddGetoToTree(ByVal item As GeoValue) As StringBuilder
        Dim outDiv As New StringBuilder
        Try
            Dim oGeoCollection As GeoCollection = ReadSons(item.Key.Id)
           
            Dim nCount As Int16
            Dim oGeoItem As GeoValue
            Dim bHasSons As Boolean
            
            If oGeoCollection Is Nothing Then
                nCount = 0
                bHasSons = False
            Else
                         
                oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
                nCount = oGeoCollection.Count
                bHasSons = True
            End If
           
            '*************************divMain******************************
            outDiv.Append("<Div style='margin:10px' id='divMain'>" & vbCrLf)
            
            '*************************divLink***************************
            
            outDiv.Append("<Div id='divLink' onclick='DisplayDiv(""divSons_" & item.Key.Id & """,document.getElementById(""linkPlus_" & item.Key.Id & """));SelItem(this,""" & item.Key.Id & """,""" & item.Description.Replace("'", Chr(60)) & """)' onmouseout='RestoreColor(this)' onmouseover='ChangeColor(this)' >" & vbCrLf)
            If nCount > 0 Then
                outDiv.Append("<a id='linkPlus_" & item.Key.Id & "'>+&nbsp;</a>")
            Else
                outDiv.Append("<a id='linkPlus_" & item.Key.Id & "'>&nbsp;&nbsp;&nbsp;</a>")
            End If
            outDiv.Append("<a id='linkDescr'>" & item.Description & "</a>" & vbCrLf)
            
            'divLink
            outDiv.Append("</Div>" & vbCrLf)
            '************************divLink********************************
            
            '***************************divSons******************************
            If bHasSons Then
                outDiv.Append("<Div id='divSons_" & item.Key.Id & "' style='display:none'>" & vbCrLf)
                                    
                For Each oGeoItem In oGeoCollection
                    outDiv.Append(AddGetoToTree(oGeoItem))
                Next
            
                outDiv.Append("</Div>" & vbCrLf)
            End If
            '***************************divSons******************************
            
            outDiv.Append("</Div>" & vbCrLf)
            '************************divMain*******************************    
           
        Catch ex As Exception
            Throw ex
        End Try
        
        Return outDiv
    End Function
    
    ''' <summary>
    '''Restituisce una GeoCollection data la Key del GeoValue 
    ''' contenente tutti i figli del nodo dato in input
    ''' </summary>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSons(ByVal item As Int32) As GeoCollection
        Try
            _oGeoManager = New GeoManager
            _oGeoManager.Cache = False
            _oGeoSearcher = New GeoSearcher
            _oGeoSearcher.SonsOf = item
            _oGeoCollection = _oGeoManager.Read(_oGeoSearcher)
        
            Return _oGeoCollection
        Catch ex As Exception
            Throw ex
        End Try
        
    End Function
           
    ''' <summary>
    ''' Leggo un GeoValue dato l'id
    ''' </summary>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadGeo(ByVal item As Integer) As GeoValue
        Try
            _oGeoManager = New GeoManager
            _oGeoManager.Cache = False 'necessario
            
            Dim oGeoValue As GeoValue = _oGeoManager.Read(New GeoIdentificator(item))
            Return oGeoValue
        Catch ex As Exception
            Throw ex
        End Try
        
    End Function
    
    ''' <summary>
    '''Caricamento del tree di GeoValue, per default usa il paramentro geoParent = -1
    '''In tal modo recupera tutti i nodi che non hanno padre
    ''' </summary>
    ''' <param name="geoParent"></param>
    ''' <remarks></remarks>
    '''    
    Sub BindAllGeo(Optional ByVal geoParent As Int16 = -1)
        Try
            
            Dim oGeoCollection As GeoCollection = ReadSons(geoParent)
            If Not _oGeoCollection Is Nothing Then
                _oGeoCollection.Sort(_mySortType, GeoGenericComparer.SortOrder.ASC)
                For Each geoItem As GeoValue In _oGeoCollection
                    pnlGeoTree.Controls.Add(New LiteralControl(AddGetoToTree(geoItem).ToString))
                Next
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub
    
    ''' <summary>
    '''  Ritorna la lista di GeoValue in base al parametro TypeControl
    '''ControlType.AccContentGeo:Restituisce tutti i geoValue prensenti nella lista
    '''ControlType.Geo:Restituisce il nodo selezionato nel tree
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GeoList() As Object
        Get
            Dim outColl As New GeoCollection
            Dim str As String
            Dim geoValue As GeoValue
            
            Select Case TypeControl
                Case ControlType.AccContentGeo
                    Dim item As ListItem
                    For Each item In lstMain.Items
                        geoValue = ReadGeo(Int32.Parse(item.Value))
                        outColl.Add(geoValue)
                    Next
                                             
                Case ControlType.Geo
                    str = txtHidden.Text
                    If str <> "" Then
                        geoValue = ReadGeo(Int32.Parse(str))
                        outColl.Add(geoValue)
                    Else
                        outColl = Nothing
                    End If
            End Select
            
            Return outColl
        End Get
    End Property
    
    ''' <summary>
    ''' Aggiunge l'item selezionato nel tree alla lista
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub AddItem(ByVal s As Object, ByVal e As EventArgs)
               
        _oGeoCollection = CType(GeoVisualization.Controls(0), Object).GeoList
        CType(GeoVisualization.Controls(0), Object).GeoListResetSelection()
        Dim oGeoValue As GeoValue
        If Not _oGeoCollection Is Nothing Then
            oGeoValue = _oGeoCollection(0)
            
            If txtHiddenList.Text.IndexOf("#" & oGeoValue.Key.Id & "#") = -1 Then
                txtHiddenList.Text += "#" & oGeoValue.Key.Id & "#"
                lstMain.Items.Add(New ListItem(oGeoValue.Description & " (" & oGeoValue.GeoType.Description & ")", oGeoValue.Key.Id))
            Else
                strJS = "alert('Element is already present in the list ')"
            End If
        Else
            strJS = "alert('Select an element from the list ')"
        End If
    End Sub
    
    ''' <summary>
    ''' Resetta la selezione effettuata nel tree
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GeoListResetSelection()
        txtHidden.Text = ""
    End Sub
    
    ''' <summary>
    ''' Rimuove l'elemento selezionato nella lista
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub RemoveListItem(ByVal s As Object, ByVal e As EventArgs)
        If Not lstMain.SelectedItem Is Nothing Then
            txtHiddenList.Text = txtHiddenList.Text.Replace("#" & lstMain.SelectedItem.Value & "#", "")
            lstMain.Items.RemoveAt(lstMain.SelectedIndex)
        End If
    End Sub
    
    ''' <summary>
    ''' Esegue il bind della griglia
    ''' data la griglia stessa e l'eventuale searcher    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <param name="Searcher"></param>
    ''' <remarks></remarks>
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As GeoSearcher = Nothing)
        _oGeoManager = New GeoManager
        _oGeoManager.Cache = False
        
        If Searcher Is Nothing Then
            Searcher = New GeoSearcher
        End If
        
        _oGeoCollection = _oGeoManager.Read(Searcher)
              
        If Not _oGeoCollection Is Nothing Then
            _oGeoCollection.Sort(SortType, SortOrder)
        End If
        
        objGrid.DataSource = _oGeoCollection
        objGrid.DataBind()
                
    End Sub
    
          
    ''' <summary>
    ''' Caricamento della griglia 
    ''' Verifica la presenza di criteri di ricerca, ed eventualmente
    ''' li passa al manager    
    ''' </summary>
    ''' <param name="objGrid"></param>
    ''' <remarks></remarks>
    Sub BindWithSearch(ByVal objGrid As GridView)
                
        _oGeoManager = New GeoManager
               
        ReadSelectedItems()
        BindGrid(objGrid, _oGeoSearcher)
    End Sub
    
    Sub SearchThemes(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridListGeo)
    End Sub
    
    ''' <summary>
    ''' Costruisce la struttura necessaria per il mantenimento
    ''' dello stato dei radio button    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadSelectedItems()
        Dim oRow As GridViewRow
        Dim ctlChk As RadioButton
        Dim ctlLit As New Literal
        Dim arr As ArrayList = CheckedItems
        
        For Each oRow In gridListGeo.Rows
            ctlChk = oRow.FindControl("chkSel")
            ctlLit = oRow.FindControl("litTheme")
            
            If Not ctlChk Is Nothing AndAlso Not ctlLit Is Nothing Then
                If ctlChk.Checked Then
                    arr.Clear()
                    arr.Add(ctlLit.Text)
                End If
            End If
        Next
        
        CheckedItems = arr
    End Sub
    
    ''' <summary>
    ''' Recupero lo stato dei radio button dal viewstate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CheckedItems() As ArrayList
        Get
            Dim arr As New ArrayList
            If ViewState("CheckedItem") Is Nothing Then
                Return arr
            Else
                Return ViewState("CheckedItem")
            End If
        End Get
        Set(ByVal value As ArrayList)
            ViewState("CheckedItem") = value
        End Set
    End Property
    
    Private Property SortType() As GeoGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = GeoGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As GeoGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Private Property SortOrder() As GeoGenericComparer.SortOrder
        Get
            If Not ViewState("SortOrder") Is Nothing Then
                _sortOrder = ViewState("SortOrder")
            Else
                _sortOrder = GeoGenericComparer.SortOrder.DESC ' SortOrder di Default                
            End If
            Return _sortOrder
        End Get
        Set(ByVal value As GeoGenericComparer.SortOrder)
            _sortOrder = value
            ViewState("SortOrder") = _sortOrder
        End Set
    End Property
    
    ''' <summary>
    ''' Caricamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ctl As RadioButton
        Dim ltl As Literal
        Dim lnkSelect As ImageButton
        Dim lnkCopy As ImageButton
        
        ctl = e.Row.FindControl("chkSel")
        ltl = e.Row.FindControl("litTheme")
        lnkSelect = e.Row.FindControl("lnkSelect")
        lnkCopy = e.Row.FindControl("lnkCopy")
        
        Dim arr As ArrayList = CheckedItems
        
        If Not ctl Is Nothing AndAlso Not ltl Is Nothing Then
            Select Case TypeControl
                Case ControlType.GeoSelection
                    ctl.Visible = True
                    ctl.Attributes.Add("onclick", "GestClick(this)")
                    lnkSelect.Visible = False
                   
                    If arr.Contains(ltl.Text) Then
                        ctl.Checked = True
                        txtHidden.Text = ctl.ClientID
                    End If
                Case ControlType.GeoList
                    lnkSelect.Visible = True
                    ctl.Visible = False
                Case ControlType.GeoEdit
                    ctl.Visible = False
                    lnkSelect.Visible = False
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Paginazione della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridPageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub

    ''' <summary>
    ''' Ordinamento della griglia
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gridSorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridListGeo.PageIndex = 0
       
        Select Case e.SortExpression
            Case "Id"
                If SortType <> GeoGenericComparer.SortType.ById Then
                    SortType = GeoGenericComparer.SortType.ById
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Description"
                If SortType <> GeoGenericComparer.SortType.ByDescription Then
                    SortType = GeoGenericComparer.SortType.ByDescription
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "GeoParentId"
                If SortType <> GeoGenericComparer.SortType.ByFather Then
                    SortType = GeoGenericComparer.SortType.ByFather
                Else
                    SortOrder = SortOrder * -1
                End If
            Case "Type"
                If SortType <> GeoGenericComparer.SortType.ByType Then
                    SortType = GeoGenericComparer.SortType.ByType
                Else
                    SortOrder = SortOrder * -1
                End If
        End Select
         
        BindWithSearch(sender)
    End Sub
    
    
    ''' <summary>
    ''' Ripulisce la form dai vecchi valori
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearForm()
        Dim ctl As Object
        For Each ctl In pnlGeoEdit.Controls
            If TypeOf (ctl) Is TextBox Or TypeOf (ctl) Is ctlText Then
                ctl.text = ""
            ElseIf TypeOf (ctl) Is ctlDate Then
                ctl.clear()
            End If
        Next
               
        ResetHeader()
    End Sub
    
    ''' <summary>
    ''' Gestione del click sul bottone Select nella griglia
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ClearForm()
        Dim id As Int32 = s.commandargument
        btnSave.Text = "Update"
                
        SelectedIdGeo = id
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
             
        Select Case s.commandname
            Case "SelectItem"
                TypeControl = ControlType.GeoEdit
                
                NewItem = False
                
                ShowRightPanel()
            Case "DeleteItem"
                Dim oGeoManager As New GeoManager
                Dim oGeoIdentificator As New GeoIdentificator
                oGeoIdentificator.Id = SelectedIdGeo
                oGeoManager.Remove(oGeoIdentificator)
                
                BindWithSearch(gridListGeo)
        End Select
        
        
    End Sub
    
    ''' <summary>
    ''' Ritorno alla lista dalla form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub goBack(ByVal s As Object, ByVal e As EventArgs)
               
        TypeControl = ControlType.GeoList
        SelectedIdGeo = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
        
        BindWithSearch(gridListGeo)
        
        ShowRightPanel()
    End Sub
    
    ''' <summary>
    ''' Save/Update del GeoValue
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub Update(ByVal s As Object, ByVal e As EventArgs)
        Dim oGeoManager As New GeoManager
        Dim oGeoValue As GeoValue = ReadFormValues()
        
       
        If NewItem Then
            oGeoValue = oGeoManager.Create(oGeoValue)
        Else
            oGeoValue = oGeoManager.Update(oGeoValue)
        End If
        
        If oGeoValue Is Nothing Then
            strJS = "alert('Error during saving')"
        Else
            strJS = "alert('Saved successfully')"
            goBack(Nothing, Nothing)
        End If
    End Sub
    
    ''' <summary>
    ''' Lettura dei dati dalla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadFormValues() As GeoValue
        Dim oGeoValue As New GeoValue
        
        With oGeoValue
            .Description = Description.Text
            .Abbreviation = Abbrev.Text
            
            If txtFatherHidden.Text <> "" Then
                .GeoParentId = txtFatherHidden.Text
            End If
            
            If txtHideGeoType.Text <> "" Then
                .GeoType.Id = txtHideGeoType.Text
            End If
            
            .Key.Id = SelectedIdGeo
        End With
              
        
        Return oGeoValue
    End Function
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Carica la form di Inserimento/Modifica    
    ''' </summary>
    ''' <param name="oCTValue"></param>
    ''' <remarks></remarks>
    Sub LoadFormDett(ByVal oCTValue As GeoValue)
               
        If Not oCTValue Is Nothing Then
            With oCTValue
                              
                GeoDescrizione.InnerText = oCTValue.Description
                GeoId.InnerText = "GEO ID: " & oCTValue.Key.Id
                
                If .GeoParentId <> 0 Then
                    txtFatherHidden.Text = .GeoParentId
                    Father.Text = _oCMSUtility.GetGeoValue(txtFatherHidden.Text).Description
                End If
                
                txtHideGeoType.Text = .GeoType.Id
                GeoType.Text = .GeoType.Description
                
                Description.Text = .Description
                Abbrev.Text = .Abbreviation
            End With
            
         
        End If
    End Sub
    
   
    
    ''' <summary>
    ''' Usata nella modifica
    ''' Ritorna il GeoValue relativo alla selezione nella griglia    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LoadGEOValue() As GeoValue
        If SelectedIdGeo <> 0 Then
            Return _oCMSUtility.GetGeoValue(SelectedIdGeo)
        End If
    End Function
    
    ''' <summary>
    ''' Aggiunge il geo selezionata nella form di Edit
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub AddItemFatherText(ByVal s As Object, ByVal e As EventArgs)
               
        _oGeoCollection = CType(GeoSelect.Controls(0), Object).GeoList
        CType(GeoSelect.Controls(0), Object).GeoListResetSelection()
        Dim oGeoValue As GeoValue
        If Not _oGeoCollection Is Nothing Then
            oGeoValue = _oGeoCollection(0)
            
            txtFatherHidden.Text = oGeoValue.Key.Id
            Father.Text = oGeoValue.Description & " (" & oGeoValue.GeoType.Description & ")"
            ' strJS2 = "ShowPanel()"
            
        Else
            strJS = "alert('Select an element from the list')"
        End If
    End Sub
    
    Sub RemoveItemFatherText(ByVal s As Object, ByVal e As EventArgs)
        txtFatherHidden.Text = Nothing
        Father.Text = Nothing
    End Sub
    
    
    Sub Gest_SaveType(ByVal s As Object, ByVal e As EventArgs)
        If txtTypeGeo.Text <> "" Then
            _oGeoManager = New GeoManager
            Dim oGeoTypeValue As New GeoTypeValue
            oGeoTypeValue.Description = txtTypeGeo.Text
            _oGeoManager.Create(oGeoTypeValue)
            
            BindComboType(drbListType, oGeoTypeValue.Id)
            tdGeoType.Visible = True
            txtTypeGeo.Text = ""
        End If
    End Sub
    
    Sub ShowForm(ByVal s As Object, ByVal e As EventArgs)
        tdGeoType.Visible = True
    End Sub
    
    ''' <summary>
    ''' Chiamata dalla ClearForm, per il momenti ripulisce gli h2 con le info sul tema da editare
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResetHeader()
        GeoId.InnerText = ""
        GeoDescrizione.InnerText = "New Geo"
    End Sub
    
    ''' <summary>
    ''' Riempie il combo dei tipi
    ''' </summary>
    ''' <param name="newId"></param>
    ''' <remarks></remarks>
    Sub BindComboType(ByVal dwlObject As WebControl, Optional ByVal newId As Int32 = 0)
       
        'drbListType.Items.Clear()
        _oGeoManager = New GeoManager
        Dim id As New GeoIdentificator()
        id.Id = 0
        
        Dim oWebUtility As New WebUtility
        Dim geoTypeColl As GeoTypeCollection = _oGeoManager.ReadType()
        
        oWebUtility.LoadListControl(dwlObject, _oGeoManager.ReadType(), "Description", "Id")
        dwlgeoType.Items.Insert(0, New ListItem("---Select Type---", -1))
        
        If newId <> 0 Then
            For Each Item As ListItem In drbListType.Items
                If Item.Value = newId Then
                    drbListType.SelectedIndex = drbListType.Items.IndexOf(Item)
                    Exit For
                End If
            Next
        End If
    End Sub
    
    ''' <summary>
    ''' Creazione nuovo Geo
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub NewContentEditorial(ByVal s As Object, ByVal e As EventArgs)
        btnSave.Text = "Save"
        ClearForm()
        
        TypeControl = ControlType.GeoEdit
        SelectedIdGeo = 0
        ViewState("CheckedItem") = Nothing
        txtHidden.Text = ""
                     
        NewItem = True
                     
        ShowRightPanel()
    End Sub
    
    Sub SearchGeo(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearcher(gridListGeo)
    End Sub
    
    Sub BindWithSearcher(ByVal objGrid As GridView)
                
        _oGeoSearcher = New GeoSearcher
        If txtId.Text <> "" Then _oGeoSearcher.Id = CType(txtId.Text, Integer)
        If txtDescription.Text <> "" Then _oGeoSearcher.geoDescription = txtDescription.Text
        
        
        If Not drpThemeFather.SelectedItem Is Nothing AndAlso drpThemeFather.SelectedIndex <> 0 Then
            _oGeoSearcher.SonsOf = drpThemeFather.SelectedItem.Value
        Else
            If (dwlgeoType.SelectedValue <> -1) Then _oGeoSearcher.geoTypeId = dwlgeoType.SelectedItem.Value
        End If
        
        BindGrid(objGrid, _oGeoSearcher)
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
    'disattiva il controllo ctlGeo
    Sub Disable()
        lstMain.Enabled = False
        txtLocation.Enabled = False
        btAdd.Enabled = False
        removebutton.Enabled = False
    End Sub
    
    'attiva il controllo ctlGeo
    Sub Enable()
        lstMain.Enabled = True
        txtLocation.Enabled = True
        btAdd.Enabled = True
        removebutton.Enabled = True
    End Sub
</script>

<script type ="text/javascript" >
<%=strJS%>
    //Gestisce la selezione degli Item nel Tree
    function SelItem(objDiv,id,Desc)
        {               
           
           var obj = document.getElementById ('<%=TxtHidden.clientid%>');           
           obj.value = id
         
           var ancor 
           if (SelDiv != null)
            {                
              ancor = SelDiv.getElementsByTagName('a')  
              SelDiv.style.backgroundColor=bgcolor;
              ancor[0].style.color  = fcolor;          
              ancor[1].style.color  = fcolor;  
            }
           
           ancor = objDiv.getElementsByTagName('a')  
           objDiv.style.backgroundColor=MouseClickColor;
         
            ancor[0].style.color  = MouseClickTextColor;          
            ancor[1].style.color  = MouseClickTextColor;  
           
           SelDiv = objDiv;
        }
    function GestClick(obj)
        {         
            var selRadio
            txtVal = document.getElementById ('<%=txtHidden.clientid%>') 
           
            if (txtVal.value != '') 
                {
                    selRadio=document.getElementById (txtVal.value);
                    if (selRadio!= null) selRadio.checked = false;
                }
                
            txtVal.value= obj.id                
        }
        
    
  </script>
  
<asp:panel ID ="pnlContentGeo" runat="server">
    <asp:textbox ID="txtHiddenList" runat="server" style="display:none"/>
    <table>
        <tr>
            <td rowspan="2">
                <asp:ListBox id="lstMain" runat="server" style="width:300px" />
                <asp:TextBox id="txtLocation" runat="server"  Visible="false" style="width:200px"></asp:TextBox>
            </td>
            <td>
               <asp:button id="btAdd" Text="add" style="width:100px" runat="server" OnClick="AddItem" CssClass ="button"/>
            </td>
        </tr>   
        <tr>
            <td >
                <asp:button runat="server"
                    text ="Remove" 
                    style="width:100px" 
                    onclick="RemoveListItem"   
                    id="removebutton"   
                    CssClass ="button"              
                 />
            </td>
        </tr>     
    </table>
    <br/>      
    <asp:PlaceHolder ID="GeoVisualization" runat="server" />
</asp:panel>

<asp:panel ID ="pnlGeo" runat="server">
    <%--Gestione Visualizzazione GeoValue --%>
    <asp:textbox ID="txtHidden" runat="server" style="display:none"/>
    <asp:panel ID="pnlGeoTree" runat="server" style="overflow:auto;height:300px;border:1px solid #000;width:300px"/>     
</asp:panel>

<asp:panel ID ="pnlGeoType" runat="server">
    <%--Gestione Visualizzazione GeoTypeValue--%>
</asp:panel>

<asp:panel ID ="pnlGeoList" runat="server">
    <table class="form">
        <tr id="rowToolbar" runat="server">
            <td  colspan="4">
                <asp:button CssClass ="button" ID="btnNew" runat="server" Text="New" onclick="NewContentEditorial"  style="margin-bottom:10px;padding-left:15px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/new_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
            </td>
        </tr>
    </table>
    
   <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
        <table class="topContentSearcher">
            <tr>
                <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Geo Searcher</span></td>
            </tr>
        </table>
        <fieldset class="_Search">
            <table class="form">
                <tr>
                    <td><strong>ID</strong></td>    
                    <td><strong>Description</strong></td>
                    <td><strong>Type</strong></td>
                    <td><strong>Father</strong></td>
                </tr>
                <tr>
                    <td><HP3:Text runat="server" ID="txtId" TypeControl ="NumericText" style="width:50px"/></td>  
                    <td><HP3:Text runat="server" ID="txtDescription" TypeControl ="textbox" style="width:300px"/></td>
                    <td><asp:DropDownList id="dwlgeoType" runat= "server" AutoPostBack="true" OnSelectedIndexChanged="LoadFatherControl"/></td>
                    <td><asp:dropdownlist ID="drpThemeFather" runat="server" /></td>
                </tr>
                <tr>
                    <td><asp:button runat="server" ID="btnSearch" onclick="SearchGeo" Text="Search" cssclass="button" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td>
                </tr>
            </table>
        </fieldset>
        <hr />
    </asp:Panel> 
    
    <%--LISTA GEO--%>
    <asp:gridview ID="gridListGeo" 
                runat="server"
                AutoGenerateColumns="false" 
                GridLines="None" 
                style="width:100%"
                AllowPaging="true"                
                AllowSorting="true"                
                PageSize ="20"               
                emptydatatext="No items available"                                                          
                OnRowDataBound="gridRowDataBound" 
                OnPageIndexChanging="gridPageIndexChanging" 
                AlternatingRowStyle-BackColor="#e9eef4"
                HeaderStyle-CssClass="header"
                PagerSettings-Position="TopAndBottom"  PagerStyle-HorizontalAlign="Right"  PagerStyle-CssClass="Pager"
                OnSorting="gridSorting">
              <Columns >
                 <asp:TemplateField>
                    <ItemTemplate>
                        <asp:radiobutton ID="chkSel" runat="server" />
                        <asp:literal ID="litTheme" runat="server" visible ="false" text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                    </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateField>               
                <asp:BoundField HeaderStyle-Width="48%" DataField ="Description" HeaderText ="Description" SortExpression="Description"/>                           
                 <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Type" SortExpression="Type">
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "GeoType.Description")%></ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Father" SortExpression="GeoParentId">
                        <ItemTemplate><%#IIf(DataBinder.Eval(Container.DataItem, "GeoParentId") <> 0, _oCMSUtility.GetGeoValue(DataBinder.Eval(Container.DataItem, "GeoParentId")).Description, "No Father")%></ItemTemplate>
                </asp:TemplateField>              
                <asp:TemplateField HeaderStyle-Width="10%">
                    <ItemTemplate>
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>
                        <asp:ImageButton ID="lnkDelete" runat="server" ToolTip ="Delete item" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Attenzione! Sicuro di voler cancellare?')" OnClick="OnItemSelected" CommandName ="DeleteItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>'/>                        
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
    </asp:gridview>
</asp:panel>
<%--FINE LISTA Geo--%>

<asp:panel ID ="pnlGeoEdit" runat="server">
    <br />
    <asp:button ID="btnBack" runat="server" CssClass="button" Text="Archive" onclick="goBack" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/archive_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <asp:button ID="btnSave" runat="server" CssClass="button" Text="Save" onclick="Update" style="padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
    <hr />
    <span id="GeoId" class="title" runat ="server"/> - <span id="GeoDescrizione" class="title" runat ="server"/>
    
    <table style="width:100%" class="form">
       <tr>
            <td>
               <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormFather&Help=cms_HelpGeoDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormFather", "Description")%>
            </td>
            <td>
                <HP3:Text runat="server" ID="Description" TypeControl="TextBox" style="width:300px" />
            </td>
        </tr>
        <tr>
            <td>
               <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormAbbrev&Help=cms_HelpGeoAbbrev&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormrAbbrev", "Abbreviation")%>
            </td>
            <td>
                <HP3:Text runat="server" ID="Abbrev" TypeControl="TextBox" style="width:300px" />
            </td>
        </tr>
        <tr>
            <td>
                <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGeoType&Help=cms_HelpGeoType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGeoType", "GEO Type")%>
            </td>
            <td >
                <asp:Textbox  id="txtHideGeoType" runat="server" style="display:none"/>   
                <table style="width:100%" >
                    <tr>
                        <td style="width:150px">
                            <HP3:Text runat ="server" ID="GeoType" TypeControl ="TextBox"  style="width:150px" />       
                        </td>
                        <td style="text-align:left">
                            <asp:Button ID="NewType" runat="server" CssClass="button" text="Select" OnClick="ShowForm" />
                        </td>
                    </tr>
               </table>
       <asp:panel id="tdGeoType" runat="server" visible ="false">   
        <table>
            <tr>
                <td>
                    <asp:dropdownlist ID="drbListType" runat ="server" style="width:150px"/> 
                </td>
                <td>
                    <input type="button" class="button" value="Select" onclick="SelectTypeEdit()" />
                </td>
                <td>
                      <input type="button" class ="button" value="New Type" style="width:100px" onclick="document.getElementById('divNewType').style.display='';document.getElementById('<%=txtTypeGeo.clientid%>').focus();document.getElementById('<%=txtTypeGeo.clientid%>').value=''"/><br/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="divNewType" style="display:none">
                        <asp:textbox runat ="server" ID="txtTypeGeo" TypeControl ="TextBox"  style="width:150px" /> <asp:button ID="saveType" runat="server"  CssClass="button" Text ="Save Type" OnClick="Gest_SaveType"/>
                    </div>   
                </td>
            </tr>
         </table>
       </asp:panel>           
    </td>
    </tr>
    <tr>
        <td valign ="top">
          <a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormGeoFather&Help=cms_HelpGeoFather&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormGeoFather", "Father")%>
        </td>
            <td>
                <table width="100%">    
                    <tr>
                        <td style="width:300px">
                            <asp:textbox ID="txtFatherHidden" runat="Server" style="display:none"/>
                            <HP3:Text runat="server" ID="Father" TypeControl="TextBox" style="width:300px" />
                        </td>
                        <td style="width:25px">
                            <input type="button"  class ="button" id="btnSelect" value="Select" onclick="ShowPanel()" /> 
                        </td>
                        <td>
                           <asp:button ID="btnRemoveSelect" runat="server" CssClass="button" text="Remove" OnClick="RemoveItemFatherText" />                
                        </td>
                    </tr>
                </table>
                
                <div id="divTree" style="display:none">
                    <table>
                        <tr>
                            <td><asp:PlaceHolder ID="GeoSelect" runat="server" /></td>
                            <td><asp:button ID="btnAddSelect" runat="server" CssClass="button" text="ADD" OnClick="AddItemFatherText" style="display:none"/></td>
                        </tr>   
                    </table>
                </div>
            </td>
    </tr>
</table>
</asp:panel>

<script type="text/javascript">
    function ShowPanel()
        {
         

            var pnl = document.getElementById ('divTree')
            var btn = document.getElementById ('<%=btnAddSelect.clientid%>')
          
            if ((pnl != null) && (btn != null))
                if (pnl.style.display == 'none')
                    {
                        pnl.style.display='';
                        btn.style.display='';
                    }
                else
                    {
                        pnl.style.display='none';
                        btn.style.display='none';
                    }
        }
        
        function SelectTypeEdit()
        {
       
            var txtDesc,txtId,cmb,oPnl
            cmb=document.getElementById ('<%=drbListType.clientid%>')
            txtId=document.getElementById ('<%=txtHideGeoType.clientid%>')
            txtDesc=document.getElementById ('<%=GeoType.clientid%>')
         
            oPnl =document.getElementById ('<%=tdGeoType.clientid%>')
            txtDesc.value = cmb.options[cmb.selectedIndex].text;
            txtId.value = cmb.value;
            
            document.getElementById('divNewType').style.display='none'
            oPnl.style.display='none'
        }
        <%=strJS2%>
</script>