<%@ Control Language="VB" ClassName="ctlExtraField" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>


<script runat="server">
    Private _typecontrol As ControlType = ControlType.BooleanType
        
    Public Enum ControlType
        StringType = 0
        IntegerType = 1
        DecimalType = 2
        BooleanType = 3
        MultiFieldType = 4
        FullTextType = 5
    End Enum
           
    Public Property TypeControl() As ControlType
        Get
            Return ViewState("_typecontrol")
        End Get
        
        Set(ByVal value As ControlType)
            ViewState("_typecontrol") = value
            ShowRightPanel()
        End Set
    End Property
          
    Public ReadOnly Property GetControl() As Object
        Get
            Select Case TypeControl
                Case ControlType.StringType, ControlType.DecimalType, ControlType.IntegerType, ControlType.MultiFieldType
                    Return fieldText
                Case ControlType.BooleanType
                    Return fieldCk
                Case ControlType.FullTextType
                    Return fieldFullText
            End Select
            Return Nothing
        End Get
    End Property
        
    Public Property Indexable() As Boolean
        Get
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    Return ckIndexable.Checked
                Case ControlType.FullTextType
                    Return ckIndexableFull.Checked
            End Select
            Return Nothing
        End Get
        Set(ByVal value As Boolean)
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    ckIndexable.Checked = value
                Case ControlType.FullTextType
                    ckIndexableFull.Checked = value
            End Select
        End Set
    End Property
    
    Public Property Text() As String
        Get
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    Return fieldText.Text
                Case ControlType.FullTextType
                    Return fieldFullText.Content
            End Select
            Return Nothing
        End Get
        Set(ByVal value As String)
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    fieldText.Text = value
                Case ControlType.FullTextType
                    fieldFullText.Content = value
            End Select
        End Set
    End Property
    
    Public Property MaxLength() As Integer
        Get
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    Return fieldText.MaxLength
            End Select
        End Get
        Set(ByVal value As Integer)
            Select Case TypeControl
                Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                    fieldText.MaxLength = value
            End Select
        End Set
    End Property
           
    Public Property Checked() As Boolean
        Get
            Select Case TypeControl
                Case ControlType.BooleanType
                    Return fieldCk.Checked
            End Select
        End Get
        Set(ByVal value As Boolean)
            Select Case TypeControl
                Case ControlType.BooleanType
                    fieldCk.Checked = value
            End Select
        End Set
    End Property
    
    Sub Page_load()
        If Page.IsPostBack Then
            ShowRightPanel()
        End If
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
            
    Sub ShowRightPanel()
        Select Case TypeControl
            Case ControlType.StringType, ControlType.IntegerType, ControlType.DecimalType, ControlType.MultiFieldType
                ShowPanel("pnlTextBox")
            Case ControlType.BooleanType
                ShowPanel("pnlCk")
            Case ControlType.FullTextType
                ShowPanel("pnlFullText")
        End Select
    End Sub
          
    Sub Disable()
        fieldText.Enabled = False
        fieldCk.Enabled = False
        fieldFullText.Enabled = False
    End Sub
    
    Sub Enable()
        pnlTextBox.Enabled = True
        fieldCk.Enabled = True
        fieldFullText.Enabled = True
    End Sub
</script>

<asp:panel ID="pnlTextBox" runat="server" visible="false">
 <table>
     <tr>
         <td><asp:TextBox id="fieldText" width="300px" runat="server"/> </td>
         <td><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /> Indexable<asp:CheckBox id="ckIndexable"  runat="server"/> </td>
     </tr>
 </table>
</asp:panel>

<asp:panel id="pnlCk" runat="server" visible="false">
    <asp:CheckBox id="fieldCk"  runat="server"/>
</asp:panel>

<asp:panel id="pnlFullText" runat="server" visible="false">
    <table>
         <tr>
            <td> <telerik:RadEditor onClientLoad="onClientLoad" ID="fieldFullText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" ><Content></Content><Snippets><telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet></Snippets><Languages><telerik:SpellCheckerLanguage Code="en-US" Title="English" /><telerik:SpellCheckerLanguage Code="fr-FR" Title="French" /><telerik:SpellCheckerLanguage Code="de-DE" Title="German" /><telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" /></Languages><ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" /><FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" /><TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" /><Links><telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" /></Links></telerik:RadEditor></td>
            <td><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /> Indexable<asp:CheckBox id="ckIndexableFull"  runat="server"/></td>

        </tr>
     </table>
</asp:panel>

