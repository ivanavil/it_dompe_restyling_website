<%@ Control Language="VB" ClassName="ctlContentType" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<script runat="server">   
    Public Enum ControlType
        Combo = 1
        GenericRelation
    End Enum
    
#Region "Membri"
    Private curSite As Int16
    Private _idRelated As Int32
    Private _innerArea As String   
    Private _enabled As Boolean
    Private nuovo As Boolean = False
    Private _typecontrol As ControlType = ControlType.GenericRelation
    Private _idLingua As Int32
    Private strDomain As String    
    Private _ItemZeroMessage As String
    Private _GenericCollection As ContentTypeCollection
    Private _onlyAssociatedContents As Boolean
    Private _subDomainContentType As String
#End Region
           
#Region "Properties"
    Public Property ItemZeroMessage() As String
        Get
            Return _ItemZeroMessage
        End Get
        Set(ByVal value As String)
            _ItemZeroMessage = value
        End Set
    End Property
    
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Property OnlyAssociatedContents() As Boolean
        Get
            Return _onlyAssociatedContents
        End Get
        Set(ByVal value As Boolean)
            _onlyAssociatedContents = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                curSite = WebUtility.MyPage(Me).ThemesSite
                
                'If Not Page.IsPostBack Then
                binddata(ListBox1)
                'End If
                
                'Case ControlType.ContentsConnection
                '    strDomain = WebUtility.MyPage(Me).DomainInfo
                '    curSite = WebUtility.MyPage(Me).ThemesSite
        
                '    If Not IsNew Then
           
                '        If Not Page.IsPostBack Then
                '            binddata(ListBox1)
                '        End If
            
                '        TraslateButton()
                '    Else
                '        LoadDefault()
                '    End If
            Case ControlType.Combo
                If Not Page.IsPostBack Or (Not SubDomainContentType Is Nothing And SubDomainContentType <> "") Then
                    binddata(drpSite)
                End If
        End Select
    End Sub
    
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
       
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare l'area
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
   
    ''' <summary>
    ''' Area Slezionata
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IdContentType() As Integer
        Get
            Try
                Return ListBox1.SelectedValue
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property
   
    Public Property IsNew() As Boolean
        Get
            Return nuovo
        End Get
        Set(ByVal Value As Boolean)
            nuovo = Value
        End Set
    End Property
        
    Public WriteOnly Property SetContentsType() As String
        Set(ByVal value As String)
            txtHidden.Text = value
           
            LoadItems()
        End Set
    End Property
    
    ''' <summary>
    ''' content for domain
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SubDomainContentType() As String
        Get
            Return _subDomainContentType
        End Get
        Set(ByVal value As String)
            _subDomainContentType = value
        End Set
    End Property
#End Region
    
    ''' <summary>
    ''' Legge la descrizione del ContentType in base all'id
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadLabel(ByVal idContentType As Int32) As ContentTypeValue
        Dim sManger As New ContentTypeManager
        Dim sValue As ContentTypeValue
        
        sValue = sManger.Read(New ContentTypeIdentificator(idContentType))
       
        Return sValue
    End Function
       
    Public Sub SetSelectedIndex(ByVal index As Integer)
        If TypeControl = ControlType.Combo Then
            drpSite.SelectedIndex = index
        Else
            Return
        End If
    End Sub
    
    Public Sub SetSelectedValue(ByVal value As String)
        If TypeControl = ControlType.Combo Then
            drpSite.SelectedValue = value
        Else
            Return
        End If
    End Sub

   
    ''AMa #2310 17/11/2011  
    'Aggiunta di un HashTable per evitare il doppio salvataggio di ContentType associati al banner 
    Public ReadOnly Property GetSelection() As ContentTypeCollection
        Get
            Dim sCollection As New ContentTypeCollection
            Dim sValue As ContentTypeValue
            
            Select Case TypeControl
                Case ControlType.GenericRelation
                    Dim item As ListItem
                   
                    LoadItems()
                    Dim hash As New Hashtable()
                    For Each item In ListBox1.Items
                        sValue = ReadLabel(item.Value)
                        If Not hash.ContainsKey(item.Value) Then
                            If Not sValue Is Nothing Then
                                sCollection.Add(sValue)
                                hash.Add(item.Value, "")
                            End If
                        End If
                    Next
		
                    Return sCollection
                Case ControlType.Combo
                    If Not drpSite.SelectedItem Is Nothing AndAlso drpSite.SelectedIndex <> 0 Then
                        sValue = ReadLabel(drpSite.SelectedItem.Value)
                        sCollection.Add(sValue)
                    End If
                    
                    Return sCollection
            End Select
            Return Nothing
        End Get
        
    End Property
      	        	        
    
    Sub LoadDefault()
                
    End Sub
    
    Function ReadSource() As ContentTypeCollection
        Dim _sCollection As ContentTypeCollection = Nothing
       
        Select Case TypeControl
            Case ControlType.Combo
                Dim oCTypeManager As New ContentTypeManager
                oCTypeManager.Cache = False
                Dim oCTypeSearcher As New ContentTypeSearcher
                oCTypeSearcher.OnlyAssociatedContents = OnlyAssociatedContents
                
                If Not SubDomainContentType Is Nothing And SubDomainContentType <> "" Then
                    oCTypeSearcher.Key.Domain = SubDomainContentType
                End If
                
                _sCollection = oCTypeManager.Read(oCTypeSearcher)
        End Select
        Return _sCollection
    End Function
    
    Sub binddata(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim _sCollection As ContentTypeCollection = GenericCollection
                LoadList(_sCollection, List)
            
            Case ControlType.Combo
                Dim _sSiteCollection As ContentTypeCollection = ReadSource()
                LoadList(_sSiteCollection, List)
        End Select
    End Sub
    
    Sub LoadList(ByVal sCollection As Object, ByVal List As ListControl)
        Dim _sValue As ContentTypeValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        List.Items.Clear()
        If Not sCollection Is Nothing Then
            For Each _sValue In sCollection
                oListItem = New ListItem(_sValue.Description, _sValue.Key.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
        
        If ItemZeroMessage <> "" Then
            List.Items.Insert(0, New ListItem(ItemZeroMessage, 0))
        End If
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                pnl = FindControl("pnlAccContent")
            Case ControlType.Combo
                pnl = FindControl("pnlCompo")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
</script>	

<asp:Panel ID="pnlAccContent" runat="server">
    
	<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td valign="top">
                    <asp:ListBox id="ListBox1"   runat="server" width="200px"></asp:ListBox>
                </td>
                <td>
					<input type="button" class="button" id="btnAree" value ='Insert ContentType' onClick="window.open('<%=strDomain%>/Popups/popCType.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')"/>
                    <br />
					
					<input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>
                 
                </td>
            </tr>
        </tbody>
    </table>

</asp:Panel>
<asp:Panel ID="pnlCompo" runat="server">
    <asp:DropDownList ID="drpSite" runat="server" />
</asp:Panel>
<%--<script type ="text/javascript" >
        <%if Typecontrol = ControlType.ContentsConnection then%>
		LoadItems(document.getElementById ('<%=ListBox1.clientid%>'),document.getElementById('<%=txtHidden.clientid%>'))					
		<%end if %>
</script>--%>