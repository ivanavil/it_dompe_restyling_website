<%@ Control Language="VB" ClassName="ContentsDetail" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<%@ Register tagprefix="HP3" TagName ="Date" Src ="~/hp3Office/HP3Parts/ctlDate.ascx" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<script runat="server">
    Private objQs As New ObjQueryString
    Private _idcontent As Int32
    Private idLanguage As Int16
    Private _idContentType As Int32
    Private _contentManager As ContentManager
    Private _contentSearcher As ContentSearcher
    Private _contentValue As ContentValue    
    Private _contentCollection As ContentCollection
    Public isNew As Boolean
    
    Public Property idContent() As Int32
        Get
            Return _idcontent
        End Get
        Set(ByVal value As Int32)
            _idcontent = value
            
            If _idcontent <> 0 Then
                isNew = False
            Else
                isNew = True
            End If                        
        End Set
    End Property
        
    ''' <summary>
    ''' Leggo le info sulla lingua corrente
    ''' La recupero dal qs, ma se non lo trovo lo leggo dal cookie    
    ''' </summary>
    ''' <remarks></remarks>
    Sub ReadLanguage()
        Dim webRequest As New WebRequestValue
        Dim strLanguage As String = objQs.Language_id
        
        If strLanguage = "" Then
            idLanguage = 0
        Else
            idLanguage = Int32.Parse(strLanguage)
        End If
        
        If idLanguage = 0 Then idLanguage = WebUtility.MyPage(Me).idCurrentLang
    End Sub
    
    Sub Page_Load()
        ReadLanguage()
        _idContentType = WebUtility.MyPage(Me).idConsolleContentType
        
        If idContent <> 0 Then
            _contentManager = New ContentManager
            _contentManager.Cache = False
            
            _contentSearcher = New ContentSearcher
                           
            _contentValue = _contentManager.Read(New ContentIdentificator(idContent, idLanguage))
           
            Title.InnerHtml = "Content ID = " & idContent
            
            If Not _contentValue Is Nothing Then
                LoadControl()
            Else
                isNew = True
            End If
        End If
        
        Site.IsNew = isNew
        
        'nuovo content
        lSelector.IsNew = True
        lSelector.LoadDefaultValue()
        'lSelector.DefaultValue = idLanguage
        
    End Sub
    
    Sub LoadControl()
        With _contentValue
            If .DateCreate.HasValue Then
                DateCreate.Value = .DateCreate.Value
            End If
                         
            If .DateExpiry.HasValue Then
                DateExpiry.Value = .DateExpiry.Value
            End If
            
            If .DateInsert.HasValue Then
                DateInsert.Value = .DateInsert.Value
            End If
           
            If .DateUpdate.HasValue Then
                DateUpdate.Value = .DateUpdate.Value
            End If
                        
            If .DatePublish.HasValue Then
                DatePublish.Value = .DatePublish.Value
            End If
                        
            Titleid.Text = .Title
            Launch.Text = .Launch
            
            Site.idRelated = idContent
            Aree.idRelated = idContent
            
            'edit content
            lSelector.LoadLanguageValue(.Key.IdLanguage)
        End With
    End Sub
    
    ''' <summary>
    ''' Restituisce il contentvalue relativo alla form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ReadContentValue() As Object
        Get
            Dim cValue As New ContentValue
            cValue.DateExpiry = DateExpiry.Value
            cValue.DateInsert = DateInsert.Value
            cValue.DatePublish = DatePublish.Value
            cValue.DateUpdate = DateUpdate.Value
            cValue.DateCreate = DateCreate.Value
            cValue.Title = Titleid.Text
            cValue.Launch = Launch.Text
            cValue.ContentType.Key.Id = _idContentType
            cValue.Active = True
            cValue.Display = True
            
            cValue.Key = New ContentIdentificator(idContent, idLanguage)
                      
            Return cValue
                        
        End Get
    End Property
                
    Public ReadOnly Property ReadSiteCollection() As Object
        Get
            Return Site.GetSelection
        End Get
    End Property
            
    Public ReadOnly Property ReadAreaCollection() As Object
        Get
            Return Aree.GetSelection()
        End Get
    End Property
</script>
<h2 id="Title" runat="server" />
<table style="width:100%; " >
<tr>
<td style="width:40%" valign ="top">
    Content language
</td>
<td>
<HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="StandardControl" />
</td>
</tr>
<tr>
<td  valign ="top">Date Create</td>
<td>
<HP3:Date runat="server" ID="DateCreate" Title="Date Create" TypeControl="JsCalendar"  />
</td>
</tr>
<tr>
<td valign ="top">Date Expiry</td>
<td>
<HP3:Date runat="server" ID="DateExpiry" TypeControl="JsCalendar"/>
</td>
</tr>
<tr>
<td valign ="top">Date Insert</td>
<td>
<HP3:Date runat="server" ID="DateInsert" TypeControl="JsCalendar"/>
</td>
</tr>
<tr>
<td valign ="top">Date Publish</td>
<td>
<HP3:Date runat="server" ID="DatePublish" TypeControl="JsCalendar"/>
</td>
</tr>
<tr>
<td valign ="top">Date Update</td>
<td>
<HP3:Date runat="server" ID="DateUpdate" TypeControl="JsCalendar"/>
</td>
</tr>
<tr>
<td valign ="top">Title</td>
<td>
<HP3:Text runat="server" ID="Titleid" TypeControl="TextBox" Style = "width:300px"/>
</td>
</tr>
<tr>
<td valign ="top">Launch</td>
<td>
<HP3:Text runat="server" ID="Launch" TypeControl="textArea" Style = "width:300px"/>
</td>
</tr>
<tr>
<td valign ="top">Site</td>
<td>
<HP3:ctlSite runat="server" ID="Site" TypeControl="ContentsConnection" SiteType="Site"/>
</td>
</tr>
<tr>
<td valign ="top">Aree</td>
<td>
<HP3:ctlSite runat="server" ID="Aree" TypeControl="ContentsConnection" SiteType="area"/>
</td>
</tr>

</table>