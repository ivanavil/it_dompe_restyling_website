<%@ Control Language="VB" ClassName="ctlServiceOptionExtra" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">   
    Public Enum ControlType        
        GenericRelation
    End Enum
    
#Region "Membri"    
    Private _typecontrol As ControlType = ControlType.GenericRelation
    Private _GenericCollection As ContentOptionCollection
    Private strDomain As String
#End Region
           
#Region "Properties"
        
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                                                             
                BindData(ListBox1)
                           
        End Select
    End Sub    
       
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
#End Region
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
        
    
    'Legge la descrizione del dato l'id
    Function ReadOptionName(ByVal idOption As Int32) As ContentOptionValue
        Dim optionManger As New ContentOptionService
        Dim cOptionValue As ContentOptionValue
        
        cOptionValue = optionManger.Read(New ContentOptionIdentificator(idOption))
       
        Return cOptionValue
    End Function
       
  
    Public ReadOnly Property GetSelection() As ContentOptionCollection
        Get
            Dim opCollection As New ContentOptionCollection
            Dim contentOptionValue As ContentOptionValue
            
            Select Case TypeControl
                Case ControlType.GenericRelation
                    Dim item As ListItem
                           
                    Dim strOut As String = ""
                    LoadItems()
		
                    For Each item In ListBox1.Items
                        contentOptionValue = ReadOptionName(item.Value)
            
                        If Not contentOptionValue Is Nothing Then
                            opCollection.Add(contentOptionValue)
                        End If
            
                    Next
		
                    Return opCollection
            End Select
        End Get
    End Property
       	           
    Sub BindData(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim ocontentOptionCollection As ContentOptionCollection = GenericCollection
               
                LoadList(ocontentOptionCollection, List)
        End Select
    End Sub
    
    Sub LoadList(ByVal ocontentOptionCollection As Object, ByVal List As ListControl)
        Dim contentOptionValue As ContentOptionValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        List.Items.Clear()
        
        If Not ocontentOptionCollection Is Nothing Then
            For Each contentOptionValue In ocontentOptionCollection
                oListItem = New ListItem(contentOptionValue.Label, contentOptionValue.Key.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
               
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                pnl = FindControl("pnlGeneric")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    'Sub Page_Init()
    '    ShowPanel()
    'End Sub
       
    Public Sub clear()
        ListBox1.Items.Clear()
        txtHidden.Text = ""
    End Sub
    
    Sub SelectItem(ByVal sender As Object, ByVal e As EventArgs)
        'Response.Write(ListBox1.Items.Count)
        'Response.Write(ListBox1.SelectedItem.Text)
        plVisible.Visible = True
    End Sub
    
</script>	

<asp:Panel id="pnlGeneric" runat="server">
  	<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
            <td valign="top">
                <asp:ListBox id="ListBox1" AutoPostBack="true" OnSelectedIndexChanged="SelectItem"  runat="server" width="200px"></asp:ListBox>
            </td>
            <td>
			    <input type="button" class="button" id="btnAree" value ='Add Option' onClick="window.open('<%=strDomain%>/Popups/popOptionExtra.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=250,height=500,scrollbars=yes')"/>
		   <br />
		    </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>  

    <asp:Panel id="plVisible" runat="server"  Visible="false">        
            <asp:Label id="txtVisible" Text = "Visible :" runat="server"></asp:Label>
			<asp:dropdownlist id="visible" runat="server">
                <asp:ListItem Selected ="False" Text="Service" Value="1" />
                <asp:ListItem  Selected ="False" Text="Content" Value="2" />
                <asp:ListItem  Selected ="True" Text="Always" Value="3" />
            </asp:dropdownlist><br />
            
        <input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>    
    </asp:Panel>    
