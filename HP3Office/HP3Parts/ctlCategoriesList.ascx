<%@ Control Language="VB" ClassName="ctlCategoriesList" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Product"%>

<script runat="server">   
    Private _innerArea As String
    Private _enabled As Boolean
     
    Private _GenericCollection As ContextBusinessCollection

    Private strDomain As String
    Private Language As String
    Private Business As String
    
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Sub LoadControl()
        binddata(ListBox1)
    End Sub
    
    Public ReadOnly Property GetSelection() As ContextBusinessCollection
        Get
            Dim ctxBCollection As New ContextBusinessCollection
            Dim ctxBValue As ContextBusinessValue
            Dim item As ListItem
                  
            Dim strOut As String = ""
            LoadItems()
            
            For Each item In ListBox1.Items
                ctxBValue = ReadContextBusinessLabel(item.Value)
            
                If Not ctxBValue Is Nothing Then
                    ctxBCollection.Add(ctxBValue)
                End If
            Next
		           
            Return ctxBCollection
        End Get
    End Property
      
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Language = Request("L")
        Business = Request("B")
    End Sub
    
    
    'Legge la descrizione del context business dato l'id
    Function ReadContextBusinessLabel(ByVal idCtx As Int32) As ContextBusinessValue
        Dim ctxBManager As New ContextBusinessManager
        Dim ctxBColl As ContextBusinessCollection
        
        Dim CtxBSearcher As New ContextBusinessSearcher()
        CtxBSearcher.KeyContextBusiness.Id = idCtx
        CtxBSearcher.KeyContextBusiness.IdBusiness = Business
        CtxBSearcher.KeyContextBusiness.IdLanguage = Language
        
        ctxBColl = ctxBManager.Read(CtxBSearcher)
       
        If Not (ctxBColl Is Nothing) And (ctxBColl.Count > 0) Then
            Return ctxBColl(0)
        End If
       
        Return Nothing
    End Function
    
    Sub binddata(ByVal List As ListControl)
        Dim _ctxBCollection As New ContextBusinessCollection
        _ctxBCollection = GenericCollection
                     
        LoadList(_ctxBCollection, List)
    End Sub
   
    Sub LoadList(ByVal cBCollection As ContextBusinessCollection, ByVal List As ListControl)
        Dim _ctxBValue As ContextBusinessValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        
        List.Items.Clear()
        txtHidden.Text = ""
       
        If Not cBCollection Is Nothing Then
            For Each _ctxBValue In cBCollection
                oListItem = New ListItem(_ctxBValue.Label, _ctxBValue.KeyContextBusiness.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
                
            Next
        End If
                       
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
        strIn = strIn.Substring(0, strIn.Length - 1)
      
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
    End Sub
            
    Public Sub clear()
        ListBox1.Items.Clear()
        txtHidden.Text = ""
    End Sub
</script>	

<asp:Panel ID="pnlGeneric" runat="server">
	<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top">
             <asp:ListBox id="ListBox1" runat="server" width="300px" Height="100px"></asp:ListBox>
          </td>
          <td>
            <input type="button" class="button" id="btnAree" value ='Insert Category' onclick="window.open('<%=strDomain%>/Popups/popCategories.aspx?BusinessId=<%=Business%>&LanguageId=<%=Language%>&HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')"/>
            <br />
		    <input type="button" class="button" id="btnDel" value ='Delete' onclick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>
         </td>
       </tr>
    </table>
</asp:Panel>
