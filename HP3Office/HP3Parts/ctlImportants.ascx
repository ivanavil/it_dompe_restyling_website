<%@ Control Language="VB" ClassName="ctlImportants" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script runat="server">
    Private _importants As ContentValue.ImportantsEnum
    
    Sub LoadOption(Optional ByVal Value As Int32 = -1)
        Dim item As ListItem
        
        ctlOptions.Items.Clear()
        
        Dim oImportants As ContentValue.ImportantsEnum
        For Each oImportants In [Enum].GetValues(GetType(ContentValue.ImportantsEnum))
            item = New ListItem
            item.Text = oImportants.ToString
            item.Value = oImportants
                
            If Value <> -1 AndAlso Value = oImportants Then
                item.Selected = True
            End If
                
            ctlOptions.Items.Add(item)
        Next
    End Sub
    
    Sub Page_Load()
        If Not Page.IsPostBack Then
            LoadOption()
        End If
    End Sub
    
    Public Property Importants() As ContentValue.ImportantsEnum
        Get
            If Not ctlOptions.SelectedItem Is Nothing Then
                Return ctlOptions.SelectedValue
            End If
            
        End Get
        Set(ByVal value As ContentValue.ImportantsEnum)
            _importants = value
            LoadOption(value)
            
        End Set
    End Property
    
    Sub Disable()
        ctlOptions.Enabled = False
    End Sub
    
    Sub Enable()
        ctlOptions.Enabled = True
    End Sub
</script>
<asp:radiobuttonlist runat="server" ID="ctlOptions" RepeatDirection ="horizontal"/>