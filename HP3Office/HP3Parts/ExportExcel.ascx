﻿<%@Control Language="VB" Debug="True"%>
<%@Import NameSpace="System.Data"%>
<%@Import NameSpace="System.Data.SqlClient"%>
<%@Import NameSpace="System.IO"%>
<%@Import NameSpace="System.Text"%>

<script runat="server">
    Dim _strContentType As String
	Dim _strFileName as String
	Dim _contentEncoding as System.Text.Encoding
	
    Public Sub init()
        Dim tw As New System.IO.StringWriter()
        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
        Response.ContentType = ContentType
        Response.ContentEncoding = ContentEncoding
        phReport.Controls.Add(Source)
        Source.EnableViewState = False
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        Source.RenderControl(hw)
        Response.Write(tw.ToString())
        Response.End()
    End Sub
    
    Public Property ContentEncoding()
        Get
            Return _contentEncoding
        End Get
        Set(ByVal value)
            _contentEncoding = Value
        End Set
    End Property
	
    Public Property Source()
        Get
            Return Session("source")
        End Get
        Set(ByVal value)
            Session("source") = value
        End Set
    End Property
	
    Public Property ContentType()
        Get
            Return _strContentType
        End Get
        Set(ByVal value)
            _strContentType = Value
        End Set
    End Property
	
    Public Property FileName()
        Get
            Return _strFileName
        End Get
        Set(ByVal value)
            _strFileName = Value
        End Set
    End Property

</script>

<asp:placeholder id="phReport" runat="server" />




