<%@ Control Language="VB" ClassName="ctlDemo1" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>

<script runat="server">   
    Public Enum ControlType
        ContentsConnection = 1
        Combo
        GenericRelation
    End Enum
    
#Region "Membri"
    Private curSite As Int16
    Private _SiteType As SiteAreaCollection.SiteAreaType
    Private _idRelated As Int32
    Private _innerArea As String   
    Private _enabled As Boolean
    Private nuovo As Boolean = False
    Private _typecontrol As ControlType = ControlType.ContentsConnection
    Private _idLingua As Int32
    Private strDomain As String    
    Private _ItemZeroMessage As String
    Private _GenericCollection As SiteAreaCollection
    Private objQs As New ObjQueryString
    Private _enable As Boolean = True
    Private _subDomainArea As String

    Private _sortOrder As SiteAreaGenericComparer.SortOrder = SiteAreaGenericComparer.SortOrder.ASC
    Private _sortType As SiteAreaGenericComparer.SortType = SiteAreaGenericComparer.SortType.ByName
#End Region
           
#Region "Properties"
    Public Property ItemZeroMessage() As String
        Get
            Return _ItemZeroMessage
        End Get
        Set(ByVal value As String)
            _ItemZeroMessage = value
        End Set
    End Property
    
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Property Domain() As String
        Get
            Return ViewState("domain")
        End Get
        Set(ByVal value As String)
            ViewState("domain") = value
        End Set
    End Property
    
    Public Property Enable() As Boolean
        Get
            Return _enable
        End Get
        Set(ByVal value As Boolean)
            _enable = value
        End Set
    End Property
    
    Public Property SubDomainArea() As String
        Get
            Return _subDomainArea
        End Get
        Set(ByVal value As String)
            _subDomainArea = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                Domain = strDomain
                curSite = WebUtility.MyPage(Me).ThemesSite
                
                'If Not Page.IsPostBack Then
                binddata(lbSite)
                'End If
                
            Case ControlType.ContentsConnection
                strDomain = WebUtility.MyPage(Me).DomainInfo
                Domain = strDomain
                curSite = WebUtility.MyPage(Me).ThemesSite
               
                If Not IsNew Then
                    If Not Page.IsPostBack Then
                        binddata(lbSite)
                    End If
            
                    TraslateButton()
                Else
                    LoadDefault()
                End If
            Case ControlType.Combo
                If Not Page.IsPostBack Or (Not SubDomainArea Is Nothing And SubDomainArea <> "") Then
                    binddata(drpSite)
                End If
        End Select
    End Sub
    
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
       
    
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare l'area
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
    ''' <summary>
    ''' Tipo di site area da prelevare
    ''' Sito o Area    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SiteType() As SiteAreaCollection.SiteAreaType
        Get
            Return _SiteType
        End Get
        Set(ByVal value As SiteAreaCollection.SiteAreaType)
            _SiteType = value
        End Set
    End Property
    
    Public ReadOnly Property SiteTypeName() As String
        Get
            Return IIf(SiteType = SiteAreaCollection.SiteAreaType.Site, "Site", "Area")
        End Get
        
    End Property
   
    ''' <summary>
    ''' Area Slezionata
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Area() As Integer
        Get
            Return lbSite.SelectedValue
        End Get
    End Property
   
    Public Property IsNew() As Boolean
        Get
            Return nuovo
        End Get
        Set(ByVal Value As Boolean)
            nuovo = Value
        End Set
    End Property
        
    Public WriteOnly Property SetAree() As String
        Set(ByVal value As String)
            txtHidden.Text = value
           
            LoadItems()
        End Set
    End Property
#End Region
    
    ''' <summary>
    ''' Legge la descrizione dell' area dato l'id
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSiteLabel(ByVal idSite As Int32) As SiteAreaValue
        Dim sManger As New SiteAreaManager
        Dim sValue As SiteAreaValue
        
        sValue = sManger.Read(New SiteAreaIdentificator(idSite))
       
        Return sValue
    End Function
       
  
    Public Sub SetSelectedIndex(ByVal index As Integer)
        If TypeControl = ControlType.Combo Then
            Try
                drpSite.SelectedIndex = index
            Catch ex As Exception
            End Try
        End If
    End Sub
    
    Public Sub SetSelectedValue(ByVal value As String)
        If TypeControl = ControlType.Combo Then
            try 
                drpSite.SelectedValue = value
            Catch ex As Exception
            End Try
        End If
    End Sub
    
    Public ReadOnly Property GetSelection() As SiteAreaCollection
        Get
            Dim sCollection As New SiteAreaCollection
            Dim sValue As SiteAreaValue
            
            Select Case TypeControl
                Case ControlType.ContentsConnection, ControlType.GenericRelation
                    Dim item As ListItem
                                               
                    Dim strOut As String
                    LoadItems()
		
                    For Each item In lbSite.Items
                        sValue = ReadSiteLabel(item.Value)
            
                        If Not sValue Is Nothing Then
                            sCollection.Add(sValue)
                        End If
            
                    Next
		
                    Return sCollection
                Case ControlType.Combo
                    If Not drpSite.SelectedItem Is Nothing AndAlso drpSite.SelectedIndex <> 0 Then
                        sValue = ReadSiteLabel(drpSite.SelectedItem.Value)
                        sCollection.Add(sValue)
                    End If
                    
                    Return sCollection
            End Select
        End Get
        
    End Property
      	        	        
    
    Sub LoadDefault()
        
        Select Case TypeControl
            Case ControlType.ContentsConnection
                If Not Page.IsPostBack And SiteType = SiteAreaCollection.SiteAreaType.Site Then
                    Dim oSiteValue As SiteAreaValue = ReadSiteLabel(curSite)
                    
                    If Not oSiteValue Is Nothing Then
                        Dim strSiteName As String = oSiteValue.Name  'ReadSiteLabel(curSite).Name
                        lbSite.Items.Insert(0, New ListItem(strSiteName, curSite))
                        txtHidden.Text = "chk_" & strSiteName & "_" & curSite & "#"
                    End If
                ElseIf Not Page.IsPostBack And SiteType = SiteAreaCollection.SiteAreaType.Area Then
                    Dim nSiteArea As Int16 = objQs.SiteArea_Id
                    If nSiteArea <> 0 Then
                        Dim oSiteValue As SiteAreaValue = ReadSiteLabel(nSiteArea)
                        If Not oSiteValue Is Nothing Then
                            Dim strSiteName As String = oSiteValue.Name 'ReadSiteLabel(curSite).Name
                            lbSite.Items.Insert(0, New ListItem(strSiteName, nSiteArea))
                            txtHidden.Text = "chk_" & strSiteName & "_" & nSiteArea & "#"
                        End If
                    End If
                End If
        End Select
        
    End Sub
    
    Sub TraslateButton(Optional ByVal obj As Object = Nothing)
        'dim ctl as object
        'dim src as object
	
        '	if obj is nothing then
        '		src = me.controls
        '	else
        '		src = obj
        '	end if
		
        '	for each ctl in src
        '		if typeOf(ctl) is button then
        '			ctl.text=multilingue.getdictionary(ctl.text,ctl.text)
        '		elseif typeOf(ctl) is Panel
        '			TraslateButton(ctl.controls)
        '		end if
        '	next
    End Sub
	
    Function ReadContentRelation() As SiteAreaCollection
        Dim _cManager As New ContentManager
        Dim _sCollection As SiteAreaCollection
        
        _sCollection = _cManager.ReadContentSiteArea(New ContentIdentificator(idRelated, 0))
                
        If SiteType <> 0 Then
            _sCollection = _sCollection.FilterBySiteAreaType(SiteType)
        End If
        
        If Not _sCollection Is Nothing Then Return _sCollection.DistinctBy("Key.Id")
    End Function
    
    Function ReadSource() As SiteAreaCollection
        Dim _sCollection As SiteAreaCollection
        
        
        Select Case TypeControl
            Case ControlType.ContentsConnection
                _sCollection = ReadContentRelation()
            Case ControlType.Combo
                              
                Dim oSiteManager As New SiteAreaManager
                oSiteManager.Cache = False
                Dim oSiteSearcher As New SiteAreaSearcher
               
                oSiteSearcher.Status = 1
                oSiteSearcher.IdType = SiteType
                
                If Not SubDomainArea Is Nothing And SubDomainArea <> "" Then
                    oSiteSearcher.Key.Domain = SubDomainArea
                End If
                
                _sCollection = oSiteManager.Read(oSiteSearcher)
        End Select
        
        Return _sCollection
    End Function
    
    Sub binddata(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim _sCollection As SiteAreaCollection = GenericCollection
                LoadList(_sCollection, List)
            Case ControlType.ContentsConnection
                Dim _sCollection As SiteAreaCollection = ReadSource()
                LoadList(_sCollection, List)
            Case ControlType.Combo
                Dim _sSiteCollection As SiteAreaCollection = ReadSource()
                
                If Not (_sSiteCollection Is Nothing) Then _sSiteCollection.Sort(_sortType, _sortOrder)
                LoadList(_sSiteCollection, List)
        End Select
    End Sub
    
    Sub LoadList(ByVal sCollection As Object, ByVal List As ListControl)
        Dim _sValue As SiteAreaValue
        Dim strOut As String
        Dim oListItem As ListItem
        List.Items.Clear()
        If Not sCollection Is Nothing Then
            For Each _sValue In sCollection
                oListItem = New ListItem(_sValue.Name, _sValue.Key.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
        
        If ItemZeroMessage <> "" Then
            List.Items.Insert(0, New ListItem(ItemZeroMessage, 0))
        End If
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        lbSite.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            lbSite.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.ContentsConnection, ControlType.GenericRelation
                pnl = FindControl("pnlAccContent")
            Case ControlType.Combo
                pnl = FindControl("pnlCompo")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Public Sub Clear()
        Select Case TypeControl
            Case ControlType.Combo
                drpSite.Items.Clear()
        End Select
    End Sub
    
    'disabilita gli elementi del controllo
    Sub Disable()
        lbSite.Enabled = False
        drpSite.Enabled = False
    End Sub
    
    'abilita gli elementi del controllo
    Sub Enabled()
        lbSite.Enabled = True
        drpSite.Enabled = True
    End Sub
    
    Sub Page_Init()
        ShowPanel()
    End Sub
        
    Sub Page_load()
        strDomain = WebUtility.MyPage(Me).DomainInfo
        Domain = strDomain
        
        ShowRightPanel()
    End Sub
</script>	

<asp:Panel ID="pnlAccContent" runat="server" Enabled="true">
    <asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>           
                <td valign="top"><asp:ListBox id="lbSite" runat="server"  width="200px"></asp:ListBox></td>
                <td>
                    <%If Enable() Then%>
                        <input type="button" class="button"  id="btnAree" value ='Insert <%=SiteTypeName()%>' onClick="window.open('<%=Domain%>/Popups/popSite.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=lbSite.clientid%>&SiteAreaType=<%=SiteType%>&sites=' + Getid('<%=lbSite.clientid%>'),'','width=600,height=500,scrollbars=yes')" /><br /><br />
                        <input type="button" class="button"   id="btnDel" value ='Delete'  onClick="DelListItem('<%=lbSite.clientid%>','<%=txtHidden.clientid%>')"/>
                   <%Else%>
                       <asp:Button ID="btInsert"  CssClass="button" Text="Insert" Enabled="false" runat="server" /><br /><br />
                       <asp:Button ID="btDelete"  CssClass="button" Text="Delete" Enabled="false" runat="server" />
                   <%End If%>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>

<asp:Panel ID="pnlCompo" runat="server">
    <asp:DropDownList ID="drpSite"  runat="server"/>
</asp:Panel>

<script type ="text/javascript" >
        <%if Typecontrol = ControlType.ContentsConnection then%>
		LoadItems(document.getElementById ('<%=lbSite.clientid%>'),document.getElementById('<%=txtHidden.clientid%>'))					
		<%end if %>
</script>