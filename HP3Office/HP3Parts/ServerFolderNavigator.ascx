<%@ Control Language="VB" ClassName="ServerFolderNavigator" %>
<%@ Import Namespace ="System.io" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">
    Private _initFolder As String
    Private _typecontrol As ControlType
    
    Public Enum ControlType
        NavigateFolder = 0
        NavigateFile
        NavigateBoth
    End Enum
    
    Private ReadOnly Property ActOnDirectory() As Boolean
        Get
            Return TypeControl.Equals(ControlType.NavigateBoth) OrElse TypeControl.Equals(ControlType.NavigateFolder)
        End Get        
    End Property
    
    Private ReadOnly Property ActOnFile() As Boolean
        Get
            Return TypeControl.Equals(ControlType.NavigateBoth) OrElse TypeControl.Equals(ControlType.NavigateFile)
        End Get
    End Property
    
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property StartFolder() As String
        Get
            If _initFolder = "" Then _initFolder = "c:\"
            
            Return _initFolder
        End Get
        Set(ByVal value As String)
            _initFolder = value
        End Set
    End Property
    
    Public Sub LoadControl()
        LoadFolder(StartFolder, pnlMain)
    End Sub
    
    Sub LoadFolder(ByVal Folder As String, ByVal pnlSrc As Panel)
        Dim oFolder As New DirectoryInfo(Folder)
        Dim oSubFolder As DirectoryInfo
        Dim oFiles As FileInfo           
        
        If Not oFolder Is Nothing Then
            If ActOnDirectory Then
                For Each oSubFolder In oFolder.GetDirectories
                    pnlSrc.Controls.Add(LoadNode(oSubFolder))
                Next
            End If
            
            If ActOnFile Then
                For Each oFiles In oFolder.GetFiles
                    pnlSrc.Controls.Add(LoadNode(oFiles))
                Next
            End If
        End If
    End Sub
    
    Function LoadNode(ByVal objSource As DirectoryInfo) As Panel
        Dim key As String = SimpleHash.ComputeMD5(objSource.FullName)
        
        Dim mainPnl As New Panel
        Dim innerPnl As New Panel
        Dim sonsPnl As New Panel
        
        Dim signLink As New LinkButton
        Dim descLink As New LinkButton
        Dim imageLink As New ImageButton
        
        mainPnl.Style.Add("margin", "5px")
        
        signLink.ID = "sign_" & key
        descLink.ID = "desc_" & key
        imageLink.ID = "image_" & key
        mainPnl.ID = "mainPnl_" & key
        innerPnl.ID = "innerPnl_" & key
        sonsPnl.ID = "innerPlc_" & key
        
        AddHandler descLink.Click, AddressOf GestClick
        AddHandler signLink.Click, AddressOf GestClick
        AddHandler imageLink.Click, AddressOf GestClick
        
        signLink.Text = "+"
        descLink.Text = objSource.Name
        
        innerPnl.Controls.Add(signLink)
        innerPnl.Controls.Add(descLink)
        innerPnl.Controls.Add(imageLink)
        
        mainPnl.Controls.Add(innerPnl)
        mainPnl.Controls.Add(sonsPnl)
        
        Return mainPnl
    End Function
    
    Function LoadNode(ByVal objSource As FileInfo) As Panel
        Dim key As String = SimpleHash.ComputeMD5(objSource.FullName)
        
        Dim mainPnl As New Panel
        Dim innerPnl As New Panel
        Dim sonsPnl As New Panel
               
        Dim descLink As New LinkButton
        Dim imageLink As New ImageButton
        
        mainPnl.Style.Add("margin", "5px")
                
        descLink.ID = "desc_" & key
        imageLink.ID = "image_" & key
        mainPnl.ID = "mainPnl_" & key
        innerPnl.ID = "innerPnl_" & key
        sonsPnl.ID = "innerPlc_" & key
        
        AddHandler descLink.Click, AddressOf GestClick        
        AddHandler imageLink.Click, AddressOf GestClick
                
        descLink.Text = objSource.Name
                
        innerPnl.Controls.Add(descLink)
        innerPnl.Controls.Add(imageLink)
        
        mainPnl.Controls.Add(innerPnl)
        mainPnl.Controls.Add(sonsPnl)
        
        Return mainPnl
    End Function
    
    Sub GestClick(ByVal s As Object, ByVal e As EventArgs)
                
    End Sub
    
    Sub GestClick(ByVal s As Object, ByVal e As ImageClickEventArgs)
                
    End Sub
</script>
<asp:Panel ID="pnlMain" runat="server" />
