<%@ Control Language="VB" ClassName="ToolBar" %>
<%@ Import Namespace ="Healthware.HP3.Core.Utility.CMS.Forms.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="server">
    Public Event ButtonClick As EventHandler
    Public accService As New AccessService()
    
    Private oToolbarCollection As New ToolbarCollection
    Private oToolbarValue As ToolbarValue
    Private _hsVisibility As New Hashtable
    Private strDomain As String
    Private idThemes As Int32
    Private hList As Hashtable
    
    ''' <summary>
    ''' Carica i parametri pubblici
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadListParam()
        If GenericDataContainer.Exists("PublicObjects") Then
            hList = GenericDataContainer.Item("PublicObjects")
        End If
    End Sub
    
    Sub LoadParam()
        LoadListParam()
                               
        If hList Is Nothing Then           
            idThemes = WebUtility.MyPage(Me).idThemes
        Else            
            idThemes = hList("idThemes")
        End If
        
        strDomain = WebUtility.MyPage(Me).DomainInfo
    End Sub
    
    ''' <summary>
    ''' Setta la visibilit� dei bottoni nella toolbar
    ''' deve essere sempre chiamato prima della LoadControl
    ''' Se il bottone non viene trovato viene considerato visibile    
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <param name="Visibility"></param>
    ''' <remarks></remarks>
    Public Sub AddForVisibilty(ByVal Key As String, ByVal Visibility As Boolean)
        
        If _hsVisibility.ContainsKey(Key) Then
            _hsVisibility(Key) = Visibility
        Else
            _hsVisibility.Add(Key, Visibility)
        End If
        
    End Sub
    
    ''' <summary>
    ''' Legge la visibilit� dei bottoni
    ''' Se il bottone non viene trovato viene considerato visibile     
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadButtonVisibility(ByVal Key As String) As Boolean
        If _hsVisibility.ContainsKey(Key) Then
            Return _hsVisibility(Key)
        Else
            Return True
        End If
    End Function
    
    ''' <summary>
    ''' Caricamento della toolbar
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Loadcontrol()        
        'Per il momento creo io i bottoni e li stampo a video
        'in futuro v� aggiunta la gestione legata al DB
        
        'Bottone per la gestione dei campi da visualizzare nelle form
        oToolbarValue = New ToolbarValue
        oToolbarValue.ButtonType = ToolbarValue.TypeButton.Button
        oToolbarValue.Name = "ShowControl"
        oToolbarValue.Tooltip = "Show/Hide controls"
        oToolbarValue.EventDescription = "ShowControl"
        oToolbarValue.Key = "ShowControl"
        oToolbarValue.ClientEvent = "window.open('" & strDomain & "/Popups/popShowControl.aspx?idService=" & idThemes & "','','scrollbars=yes,width=640px,height=480px');return false"
        '---------------------------------------------------------------------
        
        oToolbarCollection.Add(oToolbarValue)
        
        rpToolbar.DataSource = oToolbarCollection
        rpToolbar.DataBind()
       
    End Sub
    
    Sub GestClick(ByVal s As Object, ByVal e As EventArgs)
        'RaiseEvent ButtonClick(s, e)
    End Sub
    
    Sub GestClick(ByVal s As Object, ByVal e As ImageClickEventArgs)
        ' RaiseEvent ButtonClick(s, e)
    End Sub
    
    Function ReadUrlImage(ByVal ImageName As String) As String
        'Da modificare con il path dove reperire le immagini
        Return ImageName
    End Function
    
    Sub Page_Load()
        If Not accService.IsAuthenticated Then Exit Sub
        LoadParam()
    End Sub
</script>

<asp:Repeater id="rpToolbar" runat="server" EnableViewState="false" >
<HeaderTemplate >
    <table class="form">
        <tr>
</HeaderTemplate>
<ItemTemplate >
    <td>
        <asp:button         
                    OnClientClick="<%#Container.dataitem.ClientEvent%>"
                    id ="ctlButton"            
                    Text ="<%#Container.dataitem.Name%>" 
                    visible = "<%#ReadButtonVisibility(Container.dataitem.Key) AND Container.dataitem.ButtonType=ToolbarValue.TypeButton.Button%>" 
                    runat="server" 
                    onclick="GestClick" 
                    ToolTip="<%#Container.dataitem.Tooltip%>" 
                    commandname="<%#Container.dataitem.EventDescription%>"
                     CssClass="button"/>
        <asp:imagebutton 
                    OnClientClick="<%#Container.dataitem.ClientEvent%>"
                    id ="ctlImage"
                    ImageUrl ="<%#ReadUrlImage(Container.dataitem.ImageName)%>"
                    visible = "<%#ReadButtonVisibility(Container.dataitem.Key) AND Container.dataitem.ButtonType=ToolbarValue.TypeButton.Image%>" 
                    runat="server" 
                    onclick="GestClick" 
                    ToolTip="<%#Container.dataitem.Tooltip%>" 
                    commandname="<%#Container.dataitem.EventDescription%>"
                    CssClass="button" />
    </td>
</ItemTemplate>
<FooterTemplate>
    </tr>
        </table>
</FooterTemplate>
</asp:Repeater>
