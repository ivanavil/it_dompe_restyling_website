<%@ Control Language="VB" ClassName="ctlMediaList" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>

<script runat="server">
    <Serializable()> _
    Private Structure DownloadForSaving
        Public oDownloadTypeValue As DownloadTypeValue
        Public FileNametoCopy As String
        Public RealFileNametoCopy As String
    End Structure
    
    <Serializable()> _
    Private Structure ExistingItem
        Public oDownIdentificator As DownloadTypeValueIdentificator
        Public FileName As String
    End Structure
    
    Private _idLingua As Int32
    Private _idRelated As Int32
    Private strJS As String
    Private objUtility As New GenericUtility
    Private _downloadManager As DownloadManager
    Private _downloadSearcher As DownloadSearcher
    Private _downloadValue As DownloadValue
    Private _downloadCollection As DownloadCollection
    Private arrDownload As New ArrayList
    Private arrOverwritableFile As New ArrayList
       
    Public Property idLanguage() As Int32
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Int32)
            _idLingua = value
        End Set
    End Property
    
    
    ''' <summary>
    ''' Riferimento dell' oggetto con cui accoppiare l'area
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property idRelated() As Int32
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Int32)
            _idRelated = value
        End Set
    End Property
    
    Public Sub LoadControl()
                
        If Not Page.IsPostBack AndAlso idRelated <> 0 Then
            BindGrid()
        End If
    End Sub
    
    
    ''' <summary>
    ''' Caricamento dei download associati
    ''' </summary>
    ''' <remarks></remarks>
    Sub BindGrid()
        _downloadManager = New DownloadManager
        _downloadManager.Cache = False
                
        _downloadSearcher = New DownloadSearcher()
        _downloadSearcher.key.Id = idRelated
        _downloadSearcher.key.IdLanguage = idLanguage
        _downloadSearcher.IdContentSubType = 10
        _downloadSearcher.Display = SelectOperation.All
        _downloadSearcher.active = SelectOperation.All
        _downloadSearcher.Delete = SelectOperation.All
        _downloadCollection = _downloadManager.Read(_downloadSearcher)
        
        'Carico content
        _downloadValue = _downloadCollection.Item(0)
        ViewState("DownValue") = _downloadValue
        
        If Not _downloadCollection Is Nothing Then
            objGrid.DataSource = _downloadValue.DownloadTypeCollection.DistinctBy("Key.Id")
            objGrid.DataBind()
        End If
    End Sub
    
    Function ExistsFile(ByVal oType As DownloadType) As DownloadTypeValueIdentificator
        Dim oDownLoadValue As DownloadValue = ReadContentValue()
        Dim oDownType As DownloadTypeValue
        
        If Not oDownLoadValue Is Nothing Then
            For Each oDownType In oDownLoadValue.DownloadTypeCollection
                If oDownType.DownloadType.Key.Id = oType.Key.Id Then
                    Return oDownType.Key
                End If
            Next
        End If
    End Function
    
    ''' <summary>
    ''' Restituisce l'istanza DownloadValue
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ReadContentValue() As DownloadValue
        Get
            Return ViewState("DownValue")
        End Get
    End Property
    
    
    Function GetDownload(ByVal id As Int32) As DownloadValue
        _downloadManager = New DownloadManager
        _downloadManager.Cache = False
        
        Dim outDownValue As DownloadValue = _downloadManager.Read(New ContentIdentificator(id))
        Return outDownValue
    End Function
    
    Sub SelectItem(ByVal s As Object, ByVal e As EventArgs)
       
        If Not lstMain.SelectedItem Is Nothing Then
            Dim oDownLoadvalue As DownloadTypeValue = GetItemSelected(lstMain.SelectedItem.Text)
            'If Not oDownLoadvalue Is Nothing Then
            'isPrivate.Text = IIf(oDownLoadvalue.Visibility = DownloadManager.DownloadBehavior.isPrivate, "Private", "Public")
            'isDefault.Text = oDownLoadvalue.isDefault
            'txtWeight.Text = oDownLoadvalue.Weight & " byte"
            'End If
        End If
             
    End Sub
    
    Sub TakeDownload(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Try
            Dim oDownManager As New DownloadManager
            Dim oDownloadValue As DownloadValue = oDownManager.Read(New ContentIdentificator(s.commandargument.ToString.Split(";")(1)), New DownloadTypeIdentificator(s.commandargument.ToString.Split(";")(0)))
        
            If Not oDownloadValue Is Nothing Then
                DownloadManager.TakeDownload(oDownloadValue, oDownloadValue.Key.PrimaryKey)
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
            strJS = "alert('Error during downloading')"
        End Try
    End Sub
    
    Function GetItemSelected(ByVal SelItem As String) As DownloadTypeValue
        Dim arr As ArrayList = DownloadInList
        If arr.Count > 0 Then
            For Each item As DownloadForSaving In arr
                If item.RealFileNametoCopy = SelItem Then
                    Return item.oDownloadTypeValue
                End If
            Next
        End If
    End Function
    
    Public Property ExisistingFile() As ArrayList
        Get
            Dim arr As ArrayList
            If ViewState("ExisistingFile") Is Nothing Then
                arr = New ArrayList
            Else
                arr = ViewState("ExisistingFile")
            End If
            
            Return arr
        End Get
        Set(ByVal value As ArrayList)
            ViewState("ExisistingFile") = value
        End Set
    End Property
    
    Public Property DownloadInList() As ArrayList
        Get
            Dim arr As ArrayList
            If ViewState("DownloadInList") Is Nothing Then
                arr = New ArrayList
            Else
                arr = ViewState("DownloadInList")
            End If
            
            Return arr
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DownloadInList") = value
        End Set
    End Property
    
    Function CheckForAcceptFile(ByRef oType As DownloadType) As Boolean
        
        If FileUpload1.FileName = "" Then
            strJS = "alert('No file selected .')"
            Return True
        End If
        
              
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Dim arr As ArrayList = DownloadInList
        For Each item As DownloadForSaving In arr
            If item.RealFileNametoCopy = FileUpload1.FileName Then
                strJS = "alert('File already have been selected.')"
                Return True
            End If
        Next
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                      
        If FileUpload1.PostedFile.ContentLength > ConfigurationsManager.MaxUploadFileSize Then
            strJS = "alert('Max Size File Excedeed: " & ConfigurationsManager.MaxUploadFileSize \ 1000 & " Kbyte.')"
            Return True
        End If
        
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
        _downloadManager = New DownloadManager
        oType = _downloadManager.Read(FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf(".") + 1))
        
        If oType Is Nothing Then
            strJS = "alert('File format not allowed!')"
            Return True
        End If
        '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
        Dim oDownTypeValueIdentificator As DownloadTypeValueIdentificator = ExistsFile(oType)
        Dim oDownExistingItem As ExistingItem
        
        If Not oDownTypeValueIdentificator Is Nothing Then
            arrOverwritableFile = ExisistingFile
            
            'strJS = "alert('Attenzione, un file con la stessa estensione � gi� presente nella lista.Il vecchio file verr� rimosso.')"
            oDownExistingItem.oDownIdentificator = oDownTypeValueIdentificator
            oDownExistingItem.FileName = FileUpload1.FileName
            arrOverwritableFile.Add(oDownExistingItem)
            
            ExisistingFile = arrOverwritableFile
        End If
        
        Return False
    End Function
    
    Sub AddResource(ByVal s As Object, ByVal e As EventArgs)
        Dim oType As DownloadType
        If CheckForAcceptFile(oType) Then Return
        
        arrDownload = DownloadInList
        Dim oDownToSave As DownloadForSaving
        Dim Path As String = SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.DownloadPath & "\tmpDownloadPath"
        Dim FileName As String = DateTime.Now.Ticks
        _downloadManager = New DownloadManager
        
        Try
            'Salvo il file in una cartella temporanea
            If FileUpload1.PostedFile.ContentLength > 0 Then
                If Not IO.File.Exists(Path) Then
                    IO.Directory.CreateDirectory(Path)
                End If
            
                Path = Path & "\" & FileName
                oDownToSave.RealFileNametoCopy = FileUpload1.FileName
                
                FileUpload1.PostedFile.SaveAs(Path)
            End If
            
            Dim oDownTypeValue As New DownloadTypeValue
            Dim ioFile As New IO.FileInfo(Path)
                       
            If Not oType Is Nothing Then
                With oDownTypeValue
                    .Weight = ioFile.Length
                    '.isDefault = chkDefault.Checked
                    '.Visibility = IIf(chkPrivate.Checked, DownloadManager.DownloadBehavior.isPrivate, DownloadManager.DownloadBehavior.isPublic)
                    .DownloadType = oType
                End With
                
                oDownToSave.oDownloadTypeValue = oDownTypeValue
                oDownToSave.FileNametoCopy = Path
                                
                arrDownload.Add(oDownToSave)
                DownloadInList = arrDownload
                                
                lstMain.Items.Add(New ListItem(oDownToSave.RealFileNametoCopy))
            End If
                        
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
        
    End Sub
    
    Function SetKeyForDownload(ByVal val1 As String, ByVal val2 As String) As String
        Return val1 & ";" & val2
    End Function
    
    Sub RemoveStoredItems(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Try
            Dim id As Int32 = s.commandargument
            _downloadManager = New DownloadManager
            Dim bRet As Boolean = _downloadManager.Remove(New DownloadTypeValueIdentificator(id), True)
        
            If bRet Then
                BindGrid()
            Else
                strJS = "alert('Error during file removing')"
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
            strJS = "alert('Error during saving')"
        End Try
    End Sub
    
    Sub RemoveResource(ByVal s As Object, ByVal e As EventArgs)
        If Not lstMain.SelectedItem Is Nothing Then
            Dim oDownToSave As DownloadForSaving
            Dim oDownExisting As ExistingItem
            Dim arrLists As ArrayList = DownloadInList
            Dim bFound As Boolean = False
            
            For Each oDownToSave In arrLists
                If lstMain.SelectedItem.Text = oDownToSave.RealFileNametoCopy Then
                    Try
                        IO.File.Delete(oDownToSave.FileNametoCopy)
                        bFound = True
                        
                        Exit For
                    Catch ex As Exception
                        Response.Write(ex.ToString)
                    End Try
                End If
            Next
            
            If bFound Then
                arrLists.Remove(oDownToSave)
                DownloadInList = arrLists
            End If
            
            arrLists = ExisistingFile
            bFound = False
            For Each oDownExisting In arrLists
                If oDownExisting.FileName = lstMain.SelectedItem.Text Then
                    bFound = True
                    
                    Exit For
                End If
            Next
            
            If bFound Then
                arrLists.Remove(oDownExisting)
                ExisistingFile = arrLists
            End If
            
            lstMain.Items.RemoveAt(lstMain.SelectedIndex)
        Else
            strJS = "alert('Select a file to continue')"
        End If
    End Sub
       
</script>
    
<asp:GridView id="objGrid"
        Width="100%" 
        runat="server" 
        autogeneratecolumns="false" 
        GridLines="none" 
        AlternatingRowStyle-BackColor="#e9eef4"
        HeaderStyle-CssClass="header"
        PagerSettings-Position="TopAndBottom"
        PagerStyle-HorizontalAlign="Right"  
        PagerStyle-CssClass="Pager"
         >
    <Columns>   
        <asp:TemplateField ItemStyle-HorizontalAlign="Left">        
            <ItemTemplate>
            <img src="/HP3ImageLibrary/icon/gif.gif" alt="" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField  HeaderText="Weight">  
        <ItemTemplate>
            <%#CType(Container.DataItem.Weight / 1024, Integer)%> KB
         </ItemTemplate>
         </asp:TemplateField>
         <asp:BoundField  HeaderText="Filename"  datafield="Filename"/>  
        <asp:BoundField  HeaderText="Default"  datafield="isDefault"/> 
        <asp:TemplateField>
        <ItemTemplate>
            <asp:Literal ID="litVisibility" runat="server" text='<%#iif(Container.dataitem.Visibility,"Private","Public")%>'/>
        </ItemTemplate>
        </asp:TemplateField>       
        <asp:TemplateField >        
            <ItemTemplate>            
                <asp:imagebutton ID="lnkRemove" runat="server" onClick="RemoveStoredItems"  ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif" OnClientClick="return confirm('Confirm Delete?')" commandargument="<%#Container.dataitem.Key.Id%>"/>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

<table class="form"  style="margin-top:10px">
<tr>
    <td valign="bottom" style="width:100px">Upload Files</td>
    <td>
       <table class="form">
            <tr>
                <td valign="top" >
                    <asp:listbox ID="lstMain" runat ="server" style="width:400px" AutoPostBack ="true" OnSelectedIndexChanged="SelectItem"/>
                </td>
<%--                <td valign ="top">
                    <strong>Weight:&nbsp;</strong>            
                    <asp:Label ID="txtWeight" runat="server" />           
                </td>
             </tr>
             <tr>
                <td valign ="top">
                    <strong>Visibility:&nbsp;</strong>
                    <asp:Label ID="isPrivate" runat="server" />
                </td>
             </tr>
             <tr>
                <td valign ="top">
                    <strong>Default:&nbsp;</strong>
                    <asp:Label ID="isDefault" runat="server" />
                </td>
--%>             </tr>
    </table>

    <asp:FileUpload ID="FileUpload1" runat="server" />
    <%--<asp:checkbox ID="chkDefault" runat="server" Text="Default"/>
    <asp:checkbox ID="chkPrivate" runat="server" Text="Private"/>--%>
    <asp:Button cssclass="button" ID="btnAdd" runat="server" onClick="AddResource" Text ="Add" />
    <asp:Button cssclass="button" ID="btnRemove" runat="server" onClick="RemoveResource" Text ="Remove" />
   
    </td>
    </tr>
 </table>   

<script type ="text/javascript" >
<%=strJs%>
</script>