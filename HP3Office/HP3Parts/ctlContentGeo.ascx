<%@ Control Language="VB" ClassName="ctlContentGeo" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@Import Namespace ="System.Web.UI.WebControls" %>



<script runat="server">   
    Public Enum ControlType        
        GenericRelation
    End Enum
    
#Region "Membri"    
    Private _innerArea As String
    Private _enabled As Boolean
    Private nuovo As Boolean = False
    Private _typecontrol As ControlType = ControlType.GenericRelation
    Private strDomain As String
    Private _GenericCollection As GeoCollection
    Private _idLingua As Integer = 0
    Private _idRelated As Integer = 0
  
#End Region
           
#Region "Properties"
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public Sub LoadControl()
        Select Case TypeControl
            
            Case ControlType.GenericRelation
                BindData(ListBox1)
        End Select
    End Sub    
       
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property idRelated() As Integer
        Get
            Return _idRelated
        End Get
        Set(ByVal value As Integer)
            _idRelated = value
        End Set
    End Property
    
    Public Property idLanguage() As Integer
        Get
            Return _idLingua
        End Get
        Set(ByVal value As Integer)
            _idLingua = value
        End Set
    End Property
#End Region
    
    Function ReadGeo(ByVal idgeo As Int32) As GeoValue
        Dim geoManager As New geoManager
        Dim geoValue As geoValue
        
        geoValue = geoManager.Read(New geoIdentificator(idgeo))
       
        Return geoValue
    End Function
       
  
    Public ReadOnly Property GetSelection() As geoCollection
        Get
            Dim geoCollection As New geoCollection
            Dim geoValue As geoValue
            
            Dim item As ListItem
           
            Dim strOut As String = ""
          
            LoadItems()
            '1230	 
            Dim hash As New Hashtable()
          
            If Not (ListBox1.Items Is Nothing) AndAlso (ListBox1.Items.Count > 0) Then
 
                For Each item In ListBox1.Items
                    If Not hash.ContainsKey(item.Value) Then
                        geoValue = ReadGeo(item.Value)
                        hash.Add(item.Value, "")
                        If Not geoValue Is Nothing Then
                            geoCollection.Add(geoValue)
                
                        End If
                    End If
                    
                Next
            End If
            
            Return geoCollection
        End Get
    End Property
       	           
    Sub BindData(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim geoColl As GeoCollection = GenericCollection
                LoadList(geoColl, List)
        End Select
    End Sub
    
    Public Sub LoadGeo()
        Select TypeControl
            Case ControlType.GenericRelation
                BindRelatedGeo()
        End Select
    End Sub
    
    
    Sub LoadList(ByVal GeoCollection As Object, ByVal List As ListControl)
        Dim geoValue As GeoValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
      
        List.Items.Clear()

        If Not GeoCollection Is Nothing Then
            For Each geoValue In GeoCollection
                oListItem = New ListItem(geoValue.Description, geoValue.Key.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If

        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
         
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
        
    End Sub
    
    Sub BindRelatedGeo()
        Dim _oContentManager As New ContentManager
        Dim _oContentSearcher As ContentSearcher
        Dim _oGeoCollection As GeoCollection
        Dim cPK As Integer = 0
        
             
        If (idRelated <> 0) And (idLanguage <> 0) Then
            
            'Response.Write("popo a " & idRelated & " " & idLanguage)
            ' 1149
            Dim KeyContent As ContentIdentificator = HP3Office.CMS.Utility.ContentHelper.GetContentIdentificator(idRelated, idLanguage)
            cPK = KeyContent.PrimaryKey
        End If
       
        'Response.Write("popo a " & idRelated & " " & idLanguage & "cPK " & cPK)
        
        If cPK <> 0 Then
                       
            ListBox1.Items.Clear()
            
            _oContentManager = New ContentManager
            _oContentSearcher = New ContentSearcher
                
            _oContentSearcher.key.PrimaryKey = cPK
            ' M# 1230
            _oContentSearcher.Active = SelectOperation.All
            _oContentSearcher.Display = SelectOperation.All
            _oContentManager.Cache = False
            
            _oGeoCollection = _oContentManager.ReadGeoRelated(_oContentSearcher)
          
            
            ' Response.Write("count " & _oGeoCollection.Count & "cPK " & cPK)
            If Not (_oGeoCollection Is Nothing) AndAlso (_oGeoCollection.Count > 0) Then
                For Each geoItem As GeoValue In _oGeoCollection
                  
                    ListBox1.Items.Add(New ListItem(geoItem.Description & " (" & geoItem.GeoType.Description & ")", geoItem.Key.Id))
                    'txtHidden.Text = "#" & geoItem.Key.Id & "#"
           
                    txtHidden.Text += "chk_" & geoItem.Description & "_" & geoItem.Key.Id & "#"
                   
                Next
            End If
            
            
        End If
    End Sub
    
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                pnl = FindControl("pnlGeneric")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    
    Sub Page_Init()
        ShowPanel()
    End Sub
    
    Sub Page_load()
        ShowRightPanel()
    End Sub
    
    Public Sub clear()
        ListBox1.Items.Clear()
        txtHidden.Text = ""
    End Sub
    
    'disabilita gli elementi del controllo
    Sub Disable()
        pnlGeneric.Enabled = False
     
    End Sub
    
    'abilita gli elementi del controllo
    Sub Enabled()
        pnlGeneric.Enabled = True
    End Sub
</script>	

<asp:Panel ID="pnlGeneric" runat="server">
    <asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none" />
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td valign="top">
                    <asp:ListBox id="ListBox1"   runat="server"  Height="100px" width="200px"></asp:ListBox>
                </td>
                <td>
					<input type="button" class="button" id="btnInsert" value ='Insert Geo' onClick="window.open('<%=strDomain%>/Popups/popContentGeo.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&geos=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')"/>
                    <br />
					<input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
