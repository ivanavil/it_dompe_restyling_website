<%@ Control Language="VB" ClassName="LanguageSelector" Inherits="Healthware.HP3.PresentationLayer.Share.ControlShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<script runat="server">
    Public Enum ControlType
        GeneriContent = 0
        GenericRelation
        StandardControl
        AlternativeControl
        WithSelectControl
    End Enum
       
    'Private mPageManager As New MasterPageManager()
    'Private accService As New AccessService()
    Private webRequest As WebRequestValue = New WebRequestValue()
    Private objQs As New ObjQueryString
    Private utility As New WebUtility
   
    Private _GenericCollection As LanguageCollection
   
    Private _defaultValue As Int16
    Private _typecontrol As ControlType
    Private _outVal As Int16
    Private strDomain As String
    Private _isNew As Boolean = False
    Private _isStandard As Boolean
    
        
    Public ReadOnly Property Language() As Int16
        Get
            If Not dwlLanguage.SelectedItem Is Nothing Then Return dwlLanguage.SelectedValue
        End Get
    End Property
    
    Public Property TypeControl() As ControlType
        Get
            Return _typecontrol
        End Get
        Set(ByVal value As ControlType)
            _typecontrol = value
        End Set
    End Property
    
    Public Property IsNew() As Boolean
        Get
            Return _isNew
        End Get
        Set(ByVal value As Boolean)
            _isNew = value
        End Set
    End Property
      
    Public Property Enabled() As Boolean
        Get
            Return dwlLanguage.Enabled
        End Get
        Set(ByVal value As Boolean)
            dwlLanguage.Enabled = value
        End Set
    End Property
    
    'controlla la visibilit� del pannelllo che contiene il radiobutton per scegliere la lingua
    Public Property ControlRBpanel() As Boolean
        Get
            Return Nothing
        End Get
        Set(ByVal value As Boolean)
            rbpanel.Visible = value
        End Set
    End Property
    
    'controlla la visibilit� del pannelllo che contiene il ctrLanguage per scegliere la lingua
    Public Property ControlTXpanel() As Boolean
        Get
            Return Nothing
        End Get
        Set(ByVal value As Boolean)
            txppanel.Visible = value
        End Set
    End Property
    
       
    Sub Page_Init()
        Select Case TypeControl
            Case ControlType.StandardControl
                BindCombo()
            Case ControlType.AlternativeControl
                BindAllLanguage(False)
            Case ControlType.WithSelectControl
                BindAllLanguage(True)
        End Select
    End Sub
      
    'popola la dwl delle lingue 
    'recuperando solo quelle assiciate al sito
    Sub BindCombo()
        Dim siteAreaManger As New SiteAreaManager
        Dim siteAreaId As Integer = objQs.Site_Id
               
        If (siteAreaId <> 0) Then
            Dim oLanguageValue As LanguageValue
            Dim oLanguageColl As LanguageCollection = siteAreaManger.ReadLanguageSiteRelation(New SiteAreaIdentificator(siteAreaId))
            
            dwlLanguage.Items.Clear()
            For Each oLanguageValue In oLanguageColl
                dwlLanguage.Items.Add(New ListItem(oLanguageValue.Description, oLanguageValue.Key.Id))
            Next
            'dwlLanguage.Items.Insert(0, New ListItem("Select language", -1))
        End If
    End Sub
     
    'popola la dwl delle lingue 
    'recuperando tutte le lingue disponibili
    Sub BindAllLanguage(ByVal withSelect As Boolean)
        Dim languageSearcher As New LanguageSearcher
        Dim languageManager As New LanguageManager
        
        Dim oLanguageColl As LanguageCollection = languageManager.Read(languageSearcher)
        Dim oLanguageValue As LanguageValue
        dwlLanguage.Items.Clear()
        For Each oLanguageValue In oLanguageColl
            dwlLanguage.Items.Add(New ListItem(oLanguageValue.Description, oLanguageValue.Key.Id))
        Next
        If withSelect Then dwlLanguage.Items.Insert(0, New ListItem("Select language", -1))
    End Sub
    
    'metodo utilizzato nel caso in cui TypeControl=StandardControl 
    'recupera come valore di default della dwl la lingua del sito selezionato
    'nel caso in cui IsNew = True
    Sub LoadDefaultValue()
        Dim sitesearcher As New SiteSearcher
        Dim siteManager As New SiteManager
        Dim siteAreaId As Integer = objQs.Site_Id
        
        If (siteAreaId <> 0) Then
            If (IsNew) Then
                sitesearcher.KeySiteArea = New SiteAreaIdentificator(siteAreaId)
                Dim siteValue As SiteValue = siteManager.Read(sitesearcher)(0)
                
                dwlLanguage.SelectedIndex = dwlLanguage.Items.IndexOf(dwlLanguage.Items.FindByValue(siteValue.DefLanguage.Id))
            End If
        End If
    End Sub
    
    'setta la dwl sulla lingua associata al languageId 
    Sub LoadLanguageValue(ByVal languageId As Integer)
        dwlLanguage.SelectedIndex = dwlLanguage.Items.IndexOf(dwlLanguage.Items.FindByValue(languageId))
    End Sub
    
   
    'carica la dwl con tutte le lingue disponibili per un certo content
    'Restituisce True se la dwl contiene almeno un elemento 
    'False, altrimenti
    Function LoadContentLanguage(ByVal contentId As Integer) As Boolean
        Dim exist_language As New LanguageCollection
        Dim out_language As New LanguageCollection
        Dim siteAreaId As Integer = objQs.Site_Id
        
        Dim contentSearcher As New ContentSearcher
        'recupero tutti i content che hanno lo stesso contentId
        contentSearcher.key.Id = contentId
        
        BusinessContentManager.Cache = False
        Dim contentColl As ContentCollection = Me.BusinessContentManager.Read(contentSearcher)
            
        Dim oLanguageColl As New LanguageCollection
        'recupero le lingue associate al sito selezionato
        If (siteAreaId <> 0) Then
            BusinessSiteAreaManager.Cache = False
            oLanguageColl = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(siteAreaId))
        End If
               
        'recupera tutte le lingue gi� usate per creare un content
        'exixt_language conter� tutte le lingue per le quali esiste un content
        'con IdContent = contentId (valore in input al metodo)
        If Not (contentColl Is Nothing) And Not (oLanguageColl Is Nothing) Then
            For Each content As ContentValue In contentColl
                For Each language As LanguageValue In oLanguageColl
                    If (content.Key.IdLanguage = language.Key.Id) Then
                        exist_language.Add(language)
                    End If
                Next
            Next
        End If
        
                
        'Restituisce l'insieme delle lingue a disposizione per creare un nuovo content
        'ciclo le lingue associate al sito selezionato
        'valuto se la lingua corrente non � gi� stata utilizzata,solo in quel caso la recupero
        If Not (exist_language Is Nothing) And Not (oLanguageColl Is Nothing) Then
            For Each elem As LanguageValue In oLanguageColl
                If Not (exist_language.Contains(elem)) Then
                    out_language.Add(elem)
                End If
            Next
        End If
        
        'carica la dwl con le lingue rimaste a disposizione per un certo content
        If Not (out_language Is Nothing) Then
            dwlLanguage.Items.Clear()
            For Each oLanguageValue As LanguageValue In out_language
                dwlLanguage.Items.Add(New ListItem(oLanguageValue.Description, oLanguageValue.Key.Id))
            Next
            'dwlLanguage.Items.Insert(0, New ListItem("Select language", -1))
        End If
        
        If (dwlLanguage.Items.Count = 0) Then Return False
        
        Return True
    End Function
           
    'Inizio nuova gestione scelta del language mediante 
    Public Sub LoadControl()
        Select Case TypeControl
            Case ControlType.GenericRelation
                ActiveRigthPanel()
                ShowRightPanel()
                BindData(ListBox1)
        End Select
    End Sub
    
    Sub BindData(ByVal List As ListControl)
        Select Case TypeControl
            Case ControlType.GenericRelation
                Dim olanguageCollection As LanguageCollection = GenericCollection
               
                LoadList(olanguageCollection, List)
        End Select
    End Sub
    
    'Legge la descrizione del language dato l'id
    Function ReadLanguageDescription(ByVal idLanguage As Int32) As LanguageValue
        Dim languageManger As New LanguageManager
        Dim languageValue As LanguageValue
        
        languageValue = languageManger.Read(New LanguageIdentificator(idLanguage))
       
        Return languageValue
    End Function
    
    Sub LoadList(ByVal olanguageCollection As Object, ByVal List As ListControl)
        Dim olanguageValue As LanguageValue
        Dim strOut As String = ""
        Dim oListItem As ListItem
        List.Items.Clear()
        If Not olanguageCollection Is Nothing Then
            For Each olanguageValue In olanguageCollection
                oListItem = New ListItem(olanguageValue.Description, olanguageValue.Key.Id)
                List.Items.Add(oListItem)
                strOut += "chk_" & oListItem.Text & "_" & oListItem.Value & "#"
            Next
        End If
               
        txtHidden.Text = strOut
    End Sub
    
    Sub LoadItems()
        Dim strIn As String = txtHidden.Text
        ListBox1.Items.Clear()
		
        If strIn = "" Then Return
				
        strIn = strIn.Substring(0, strIn.Length - 1)
        Dim arr() As String = strIn.Split("#")
        Dim arr2() As String
        Dim str As String
        Dim Item As ListItem
		
        For Each str In arr
            arr2 = str.Split("_")
            
            Item = New ListItem(arr2(1), arr2(2))
            ListBox1.Items.Add(Item)
        Next
	
    End Sub
    
    Sub ShowRightPanel()
        Dim pnl As Panel = Nothing
        
        Select Case TypeControl
            Case ControlType.GenericRelation
                strDomain = WebUtility.MyPage(Me).DomainInfo
                pnl = FindControl("txppanel")
        End Select
        
        If Not pnl Is Nothing Then
            ShowPanel(pnl.ID)
        End If
    End Sub
    
    Sub ShowPanel(Optional ByVal pnlName As String = "")
        For Each pnl As Control In Me.Controls
            If TypeOf (pnl) Is Panel Then
                If pnl.ID = pnlName Then
                    pnl.Visible = True
                Else
                    pnl.Visible = False
                End If
            End If
        Next
    End Sub
    
    Sub ActiveRigthPanel()
        txppanel.Visible = True
        rbpanel.Visible = False
    End Sub
    
    Public Property GenericCollection() As Object
        Get
            Return _GenericCollection
        End Get
        Set(ByVal value As Object)
            _GenericCollection = value
        End Set
    End Property
    
    Public ReadOnly Property GetSelection() As LanguageCollection
        Get
            Dim languageCollection As New LanguageCollection
            Dim languageValue As LanguageValue
            
            Select Case TypeControl
                Case ControlType.GenericRelation
                    Dim item As ListItem
                           
                    Dim strOut As String = ""
                    LoadItems()
		
                    For Each item In ListBox1.Items
                        languageValue = ReadLanguageDescription(item.Value)
            
                        If Not languageValue Is Nothing Then
                            languageCollection.Add(languageValue)
                        End If
            
                    Next
		
                    Return languageCollection
            End Select
        End Get
    End Property
</script>

<asp:Panel id ="rbpanel"   runat = "server" visible="true">
    <asp:dropdownlist id="dwlLanguage"  runat="server"/>
</asp:Panel>

<asp:Panel id="txppanel" runat="server" Visible="false">
<asp:textbox id="txtHidden" runat="Server"  width="500px" style="display:none"/>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td valign="top">
                    <asp:ListBox id="ListBox1"   runat="server" width="200px"></asp:ListBox>
                </td>
                <td>
					<input type="button" class="button" id="btnAree" value ='Insert Language' onClick="window.open('<%=strDomain%>/Popups/popLanguages.aspx?HiddenId=<%=txtHidden.clientid%>&ListId=<%=ListBox1.clientid%>&sites=' + Getid('<%=ListBox1.clientid%>'),'','width=600,height=500,scrollbars=yes')"/>
					<br />
					<input type="button" class="button" id="btnDel" value ='Delete' onClick="DelListItem('<%=ListBox1.clientid%>','<%=txtHidden.clientid%>')"/>
               </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>