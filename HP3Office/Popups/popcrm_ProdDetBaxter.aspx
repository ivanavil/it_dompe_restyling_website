<%@ Page Language="VB"  EnableEventValidation="true" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack" %>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>

<script runat="Server">
    Private webTracKSearcher As New WebTrackSearcher()
    Private webTrackManager As New WebTrackManager()
    Private gdw_Visible As Boolean = False
    Private prodKey As String
    Private siteId As String
    
    Private userHCPManager As New UserHCPManager
    Private _totContents As Integer = 0
    Private _totTime As String = ""
    
    'si � deciso di fissare a max 50 le sessioni utente restituite
    Const maxRows As Integer = 50
    
    Property TotContents()
        Get
            _totContents = ViewState("TotContents")
            Return _totContents
        End Get
        Set(ByVal value)
            _totContents = value
            ViewState("TotContents") = _totContents
        End Set
    End Property
    
    Property TotTime() As String
        Get
            _totTime = ViewState("TotTime")
            Return _totTime
        End Get
        Set(ByVal value As String)
            _totTime = value
            ViewState("TotTime") = _totTime
        End Set
    End Property

    Sub Page_Load()
        If Not Page.IsPostBack Then
            siteId = 0
            If Not Request("SelItem") Is Nothing Then
                'prodKey = Request("SelItem")
                Dim appId As String = Request("SelItem")
                Dim arr As Array
                arr = appId.Split(" - ")
                appId = arr(arr.Length - 1)
                prodKey = appId
                If Not Request("S") Is Nothing AndAlso Request("S") <> "" Then
                    siteId = Request("S")
                Else
                    siteId = 0
                End If
            End If
            Dim TNodesManager As New TraceNodesManager
            Dim TNodesSearcher As New TraceNodesSearcher
            Dim TNodesColl As New TraceNodesCollection
            Dim Course As New TraceNodesCollection
            Dim TotalColl As New TraceNodesCollection
            Dim TNodesVal As New TraceNodeValue
            Dim CatalogManager As New CatalogManager
            Dim catalogSearcher As New CatalogSearcher
            catalogSearcher.IdContentSubCategory = CatalogValue.CatalogSubTypes.Lesson
            catalogSearcher.IdContentSubType = ContentValue.Subtypes.Course
        
            If (prodKey <> 0) Then
                catalogSearcher.key.PrimaryKey = prodKey
            End If
               
            CatalogManager.Cache = False
            If siteId <> "" Then
                catalogSearcher.KeySite.Id = CInt(siteId)
            End If
            ' catalogSearcher.key.IdLanguage = srcLanguage.Language
            'catalogSearcher.Active = SelectOperation.All
            'catalogSearcher.Display = SelectOperation.All
            Dim catColl As CatalogCollection = CatalogManager.ReadCatalogRelation(catalogSearcher)
        
            Dim List As New ListItem
            Dim collList As New ListItemCollection
            For Each elem As CatalogValue In catColl
                List = New ListItem
                TNodesSearcher.KeyCourse.PrimaryKey = elem.Key.PrimaryKey
                TNodesSearcher.KeyEvent = 4
                TNodesColl = TNodesManager.Read(TNodesSearcher)
                TNodesColl.Sort(TraceNodeGenericComparer.SortType.ByDate, TraceNodeGenericComparer.SortOrder.DESC)
                Course = TNodesColl.DistinctBy("IdSession")
                List.Value = Course.Count
                List.Text = elem.Title.Trim & " - " & elem.Key.PrimaryKey
                TotalColl.AddRange(Course)
                collList.Add(List)
                'Response.Write(elem.Key.PrimaryKey & " " & elem.Title & "<br/>")
            Next

            GridView1.DataSource = collList
            GridView1.DataBind()
        End If
    End Sub
   
    
    Function getDuration(ByVal sessionValue As WebTrackUserSessionValue) As String
        Dim startDate As DateTime = sessionValue.StartDateSession
        Dim endDate As DateTime = sessionValue.EndDateSession
        
        Return New TimeSpan(0, 0, DateDiff(DateInterval.Second, startDate, endDate)).ToString
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToShortTimeString()
    End Function
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As ImageClickEventArgs)
        'leggo i paragrafi per la lezione selezionata
        ' Response.Write(s.commandargument)
        'Response.End()
        Dim appId As String = s.commandargument
        Dim arr As Array
        arr = appId.Split(" - ")
        appId = arr(arr.Length - 1)
        
        'leggo i paragrafi
        Dim MapNodesSearcher As New NodeRelationSearcher
        Dim nodesRelColl As NodeRelationCollection
        Dim CatalogManager As New CatalogManager
            
        CatalogManager.Cache = False
        
        MapNodesSearcher.KeyLesson.PrimaryKey = appId
        'MapNodesSearcher.IsStartRelation = True
        MapNodesSearcher.TypeRelation = NodeRelationSearcher.TypeRel.StartRelation
                
        nodesRelColl = CatalogManager.Read(MapNodesSearcher)
        Dim TNodesManager As New TraceNodesManager
        Dim TNodesSearcher As New TraceNodesSearcher
        Dim TNodesColl As New TraceNodesCollection
        Dim Course As New TraceNodesCollection
        Dim List As New ListItem
        Dim collList As New ListItemCollection
        Dim TotalColl As New TraceNodesCollection
        Dim capValue As ContentValue
        For Each elem As NodeRelationValue In nodesRelColl
            List = New ListItem
            TNodesSearcher.KeyCourse.PrimaryKey = elem.KeyEndNode.PrimaryKey
            TNodesSearcher.KeyEvent = 4
            TNodesColl = TNodesManager.Read(TNodesSearcher)
            TNodesColl.Sort(TraceNodeGenericComparer.SortType.ByDate, TraceNodeGenericComparer.SortOrder.DESC)
            Course = TNodesColl.DistinctBy("IdSession")
            List.Value = Course.Count
            capValue = Me.BusinessContentManager.Read(New ContentIdentificator(elem.KeyEndNode.PrimaryKey))

            List.Text = capValue.Title.Trim & " - " & elem.KeyEndNode.PrimaryKey
            TotalColl.AddRange(Course)
            collList.Add(List)

        Next
        
           
        GridViewParagraph.DataSource = collList
        GridViewParagraph.DataBind()
        
    End Sub
    
    
  </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Product Details</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
    <form id="formHistory" runat="server" >
     <div class="title" style="padding-left:10px">Product Detail Details</div>
     <hr />
     
      
    <div id="divTraceHistory">
        <asp:GridView ID="GridViewParagraph" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Paragraph - Key" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "text")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "value")%></div></ItemTemplate>
            </asp:TemplateField>
            
          <%-- <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Paragraph" ImageUrl="/HP3Office/HP3Image/Ico/user_detail_inspector.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "text")%>'/>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
        <br />
        <asp:GridView ID="GridView1" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="30"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Lesson - Key" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "text")%></div></ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Visit" HeaderStyle-Width="8%">
                <ItemTemplate><div style="text-align:center"><%#DataBinder.Eval(Container.DataItem, "value")%></div></ItemTemplate>
            </asp:TemplateField>
            
           <asp:TemplateField HeaderText=" " HeaderStyle-Width="5%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Paragraph" ImageUrl="/HP3Office/HP3Image/Ico/insert.gif" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "text")%>'/>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
        
    </div> 
</form>     
    </body>
</html>

