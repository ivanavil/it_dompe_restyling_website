<%@Page Language="Vb" Debug="True"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script runat ="server">
    Private idNewsletter As Int32
    'Manager delle Newsletter
    Private _oNewsletterManager As NewsletterManager
    Private _oNewsletterSearcher As NewsletterSearcher
    'Object value della Newsletter
    Private _oNewsletterValue As NewsletterValue
    Private _oNewsletterCollection As NewsletterCollection
    'Template associato alla newsletter
    Private _oTemplateValue As TemplateNewsletterValue
    Private strJs As String
    
    Sub Page_Load()
        Try
            If Not Request("idNewsletter") Is Nothing AndAlso IsNumeric(Request("idNewsletter")) Then
                'Recupero il NewsletterValue dall' id
                _oNewsletterManager = New NewsletterManager
                _oNewsletterSearcher = New NewsletterSearcher
                _oNewsletterSearcher.Key.KeyNewsletter = Request("idNewsletter")
                _oNewsletterCollection = _oNewsletterManager.Read(_oNewsletterSearcher)
                _oNewsletterValue = _oNewsletterCollection(0)
                
                'Ho recuperato l'oggetto Newletter
                If Not _oNewsletterValue Is Nothing Then
                    Divpreview.InnerHtml = _oNewsletterValue.FullTextHtml
                Else
                    strJs = "alert('Impossible to load the page.\nAn error occurred during newsletter retrieval.');this.close();"
                End If
                                
            Else
                strJs = "alert('Impossible to load the page.\nThe input parameters are not corrected.');this.close();"
            End If
        Catch ex As Exception
            strJs = "alert('Impossible to load the page'.\nAn error occurred during page loading);this.close();"
        End Try
                
    End Sub
    
    ''' <summary>
    ''' Leggo dato l'idTemplate l'objectvalue associato
    ''' </summary>
    ''' <param name="idTemplate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadTemplateInfoById(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateNewsletterValue
        Dim _oNewsletterManager As New NewsletterManager
        
        Return _oNewsletterManager.Read(idTemplate)
    End Function
</script>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Newsletter Preview</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link href="../_css/popup.grp.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
    
    <script type ="text/javascript" >
        <%=strJs%>
    </script>
 </head>
 <body>
 <div runat="server" id="Divpreview" style ="overflow:auto;height:600px;width:800px" />  
 </body>
 </html>