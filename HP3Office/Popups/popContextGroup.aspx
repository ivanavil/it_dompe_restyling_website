<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private utility As New WebUtility
    
    Private ocontextGroupSearcher As ContextGroupSearcher
    Private _sortType As ContextGroupGenericComparer.SortType
                  
    Private Property SortType() As ContextGroupGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContextGroupGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContextGroupGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Description & "_" & Request("sites")
        End If
        
        Dim contextGroupColl As ContextGroupCollection = Read()
       
        gridList.DataSource = contextGroupColl
        gridList.DataBind()
        
    End Sub
        
    Function Read() As ContextGroupCollection
        Dim cgCollection As ContextGroupCollection
        Dim cgSearcher As New ContextGroupSearcher
             
        cgCollection = Me.BusinessContextManager.ReadGroup(cgSearcher)
        Return cgCollection
    End Function
    
    
    'Legge la descrizione del gruppo dato l'id
    Function ReadLabel(ByVal idGroup As Int32) As ContextGroupValue
        Dim cgValue As ContextGroupValue
        
        cgValue = Me.BusinessContextManager.ReadGroup(New ContextGroupIdentificator(idGroup))
        Return cgValue
    End Function
       	
    Function setVisibility(ByVal id As Int32) As String
        Dim sCollection = Read()
        If sCollection Is Nothing OrElse sCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
	sub btnSave_OnClick(s as object,e as eventargs)
		dim el as Object
        Dim strOut As String = ""
		
		for each el in Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
		next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else            
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca di ruoli
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        ocontextGroupSearcher = New ContextGroupSearcher()
     
        If txtId.Value <> "" Then ocontextGroupSearcher.Key = New ContextGroupIdentificator(txtId.Value)
        If txtDesciption.Value <> "" Then ocontextGroupSearcher.Description = "%" & txtDesciption.Value & "%"
        
        BindGrid(objGrid, ocontextGroupSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContextGroupSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New ContextGroupSearcher
        End If
        
        Dim _oCxtGroupColl As ContextGroupCollection = Me.BusinessContextManager.ReadGroup(Searcher)
        
        If Not (_oCxtGroupColl Is Nothing) Then
            _oCxtGroupColl.Sort(SortType, ContextGroupGenericComparer.SortOrder.ASC)
        End If
        
        objGrid.DataSource = _oCxtGroupColl
        objGrid.DataBind()
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Description"
                SortType = ContextGroupGenericComparer.SortType.ByDescription
            Case "Id"
                SortType = ContextGroupGenericComparer.SortType.ById
        End Select
        
        BindWithSearch(sender)
    End Sub
  
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Context Group List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="ctxGroupForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Box Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:200px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            OnSorting="gridList_Sorting"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.Id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.Id)%>  name = 'chk_<%#Container.DataItem.Description.replace("'", "`")%>_<%#Container.DataItem.Key.Id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.Id)%>  name = 'chk_<%#Container.DataItem.Description.replace("'", "`")%>_<%#Container.DataItem.Key.Id%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Domain" >
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Domain")%>-<%#DataBinder.Eval(Container.DataItem, "Key.Name")%> </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Description" SortExpression="Description">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField> 
                         </Columns>
            </asp:gridview>
        </div> 
       
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>