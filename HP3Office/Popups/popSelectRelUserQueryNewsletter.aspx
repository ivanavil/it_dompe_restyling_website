<%@ Page Language="VB" debug="true"%>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<script runat="server">
    Dim arr As Hashtable
    Dim StrJs As String
    Dim counter As Int32 = 0
    Dim counterXLS As Int32 = 0
    Dim idNL As Int32
    
    Sub cambiapagina(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        FindCheked(sender.id)
        
        sender.CurrentPageIndex = e.NewPageIndex
                
        BindQuery()    

    End Sub
    
    Sub BindQuery()
      
        dgQuery.DataSource = GetQueries()
        dgQuery.DataBind()
    End Sub
    
    Function GetQueries(Optional ByVal id As Int32 = 0) As NewletterQueriesCollection
        Dim oNewsManager As New NewsletterManager
        Dim oNewsSearcher As New NewsletterQueriesSearcher
       
        Dim typeQuery As Integer = Request("TypeQ")
        If typeQuery <> 0 Then oNewsSearcher.Type = typeQuery
        
        If id <> 0 Then oNewsSearcher.Key = New NewsletterQueriesIdentificator(id)
        
        Return oNewsManager.Read(oNewsSearcher)
    End Function
    
    Sub Page_Load()
        pnlShow.Visible = False
        pnlEdit.Visible = False
        divToolbar.Visible = False
        ' divExport.Visible = False
        
        btnMostra.Attributes.Add("onClick", "return checkBtn()")
        btnSalva.Attributes.Add("onClick", "return checkBtn()")
        
       
        Select Case Request("type").ToUpper
            Case "SHOW"
                litTitle.Text = "Select query"
                pnlShow.Visible = True
                divToolbar.Visible = True
                If Not Page.IsPostBack Then
           
                    ViewState("FieldOrder_dgQuery") = "news_query_id"
                    ViewState("OrderType_dgQuery") = "DESC"
            
                    BindQuery()
                End If
            Case "EDIT"
                litTitle.Text = "Edit query"
                If Not Request("id") Then
                    divToolbar.Visible = True
                    pnlEdit.Visible = True
                    litMess.Text = "Edit Query"
                    If Not Page.IsPostBack Then
                        Dim oNC As NewletterQueriesCollection = GetQueries(Request("id"))
                        Dim oNV As NewletterQueriesValue = oNC(0)
                        
                        If Not oNV Is Nothing Then
                            txtQuery.Text = oNV.Query
                            txtCommento.Text = oNV.Comment
                            txtDescrizione.Text = oNV.Description
                        End If
                    End If
                End If
            Case "NEW"
                litTitle.Text = "New query"
                divToolbar.Visible = True
                pnlEdit.Visible = True
                litMess.Text = "New Query"
          
        End Select
        
    End Sub
    
    Function ReadNewsletterSend() As NewsletterSendingValue
        Return Session("NewsletterSend")
    End Function
    
    Sub FindCheked(ByVal id As String)
        Dim obj As DataGridItem
        Dim ctl, chk As Object
       		
        If ViewState(id) Is Nothing Then
            arr = New Hashtable
        Else
            arr = ViewState(id)
        End If
		
        For Each obj In dgQuery.Items
		
            chk = obj.FindControl("chkSel")
                       
            If Not chk Is Nothing Then
               
                If (obj.Cells(1).Text <> String.Empty) Then
                    If Not arr.ContainsKey(Int32.Parse(obj.Cells(1).Text)) AndAlso CType(chk, RadioButton).Checked Then
                        arr.Clear()
                        arr.Add(Int32.Parse(obj.Cells(1).Text), Int32.Parse(obj.Cells(1).Text))
                    End If
                End If
            End If
        Next
		
        ViewState(id) = arr
    End Sub
    
    Sub ReloadChk(ByVal s As Object, ByVal e As DataGridItemEventArgs)
       
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
           
            Dim strRow As String
            strRow = "<a href='#'  onclick='gestAddedRow(" & dgQuery.DataKeys(e.Item.ItemIndex).id() & ")'>" & _
                     "<img id='ancor_" & dgQuery.DataKeys(e.Item.ItemIndex).id() & "' src='../HP3Image/ico/ico_15x_plus.gif' title='Visualizza Commento'/></a>"
            strRow += "<tr style='display:none' id ='trQ_" & dgQuery.DataKeys(e.Item.ItemIndex).id() & "'><td colspan='8'>" & IIf(e.Item.Cells(5).Text.Trim = "&nbsp;", "Nessun commento", e.Item.Cells(5).Text) & "</td></tr>"
            
            e.Item.Cells(8).Text = strRow
            
            If Not ViewState(s.id) Is Nothing Then
			
                If ViewState(s.id).containskey(dgQuery.DataKeys(e.Item.ItemIndex)) Then
                    Dim ctl As Object
					
                    ctl = e.Item.FindControl("chkSel")
                    If Not ctl Is Nothing Then
                        ctl.checked = True
                        StrJs = "<script>" & vbCrLf
                      
                        StrJs += "gestClick(document.getElementById('" & ctl.clientid & "'),'" & s.id & "')" & vbCrLf
                        StrJs += "</s" & "cript>"
                    End If
                End If
            End If
        End If
    End Sub
    
    Sub ordinamento(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
        
        FindCheked(sender.id)
        
        ViewState("FieldOrder_" & sender.id) = e.SortExpression
    
        If ViewState("OrderType_" & sender.id) Is Nothing OrElse ViewState("OrderType_" & sender.id) = "DESC" Then
            ViewState.Add("OrderType_" & sender.id, "ASC")
        Else
            ViewState("OrderType_" & sender.id) = "DESC"
        End If
       
        
        BindQuery()              
    End Sub

    Function CheckCampoQueryForSelect(ByVal txtQuery As String) As Boolean
        Dim str As String = txtQuery
        Dim arr() As String
        Dim arr2() As String
        Dim strItem, strItem2 As String
		
        str = str.Trim
        arr = str.Split(" ")
		
        For Each strItem2 In arr
            arr2 = strItem2.Split(vbCrLf)
            For Each strItem In arr2
                If (strItem.Trim.ToLower = "insert") Or (strItem.Trim.ToLower = "update") Or (strItem.Trim.ToLower = "delete") Or (strItem.Trim.ToLower = "truncate") Or (strItem.Trim.ToLower = "drop") Or (strItem.Trim.ToLower = "alter") Then
                    Return True
                End If
            Next
        Next
		
        Return False
    End Function
    
    Function CheckQueryValidate(ByVal sqlQuery As String) As Boolean
        Dim Err As String = String.Empty
        Dim oNewsManager As New NewsletterManager
        
        Dim outVal As Boolean = oNewsManager.TestContactQuery(sqlQuery, Err)
	
        If outVal Then
            'Response.Write(Err)
            Dim msg As String = "Sorry, the Query does not select the column name or the column with Alias : " & Err
            CreaAlertScript(msg)
        End If
       
        Return outVal
        
        Return False
    End Function
    
    Sub CreaAlertScript(ByVal msg As String)
        StrJs = "<script>"
        StrJs += "alert(""" & msg & """);"
        StrJs += "</s" & "cript>"
    End Sub
    
    Sub showPreview(ByVal listUser As UserCollection, ByVal id As String, ByVal descrizione As String)
        Try
            pnlSql.Visible = False
            If (id <> "") OrElse (descrizione <> "") Then
                pnlPreviewData.Visible = True
                If id <> "" Then lblId.Text = id
                If descrizione <> "" Then lblDescr.Text = descrizione
            End If
          
        Catch ex As Exception

        End Try
    End Sub
    
    Sub showPreview(ByVal Query As String, ByVal id As String, ByVal descrizione As String)
        Try
            lblSql.Text = Query
            pnlSql.Visible = True
        
            If (id <> "") OrElse (descrizione <> "") Then
                pnlPreviewData.Visible = True
                If id <> "" Then lblId.Text = id
                If descrizione <> "" Then lblDescr.Text = descrizione
            End If
		
            Dim oNewsletterManager As New NewsletterManager
            oNewsletterManager.Cache = False
            
         
            Dim colContactUSer As Dictionary(Of String, ContactValue) = oNewsletterManager.ExecuteContactQuery(Query)
          
            gridTest.DataSource = colContactUSer.Values
            gridTest.DataBind()
                      
        Catch ex As Exception
        End Try
    End Sub
        
    Sub ShowPreview(ByVal s As Object, ByVal e As ImageClickEventArgs)
        Dim str As String = s.commandname
        
        Dim arr() As String = str.Split("_")
        showPreview(s.commandargument, arr(0), arr(1))
    End Sub
    
    
    Function setCommandName(ByVal par1 As String, ByVal par2 As String) As String
        Dim str As String = par1 & "_" & par2
                
        Return str
    End Function
    
    Function creaUrl(ByVal id As String) As String
        Dim str As String = Request.Url.ToString
        Dim arr() As String = str.Split("?")
        
        Return arr(0) & "?idNL=" & Request("idNL") & "&Type=EDIT&id=" & id & "&TypeQ=" & Request("TypeQ")
    End Function
    
    Function creaUrlParam(ByVal param As String) As String
        Dim str As String = Request.Url.ToString
        Dim arr() As String = str.Split("?")
        
        Return arr(0) & "?idNL=" & Request("idNL") & "&Type=" & param & "&TypeQ=" & Request("TypeQ")
    End Function
    
    Sub ProvaQuery(ByVal s As Object, ByVal e As EventArgs)
        If CheckCampoQueryForSelect(txtQuery.Text) Then
            CreaAlertScript("Only Select Queries")
            Return
        End If
		
        If CheckQueryValidate(txtQuery.Text) Then
            Return
        End If
        
        showPreview(txtQuery.Text, Request("id"), txtDescrizione.Text)
        StrJs = "<Script>"
        StrJs += "window.location='#ListaUtenti'"
        StrJs += "</" & "Script>"
    End Sub
    
    Sub SalvaQuery(ByVal s As Object, ByVal e As EventArgs)
        Dim Err As String
        Dim oNewsletterManger As New NewsletterManager
        Dim accService As New AccessService()
        
        If CheckCampoQueryForSelect(txtQuery.Text) Then
            CreaAlertScript("Only Select Queries")
            Return
        End If
		
        If CheckQueryValidate(txtQuery.Text) Then
            Return
        End If
        
        Dim oNewsQueryValue As New NewletterQueriesValue
        With oNewsQueryValue
            .Description = txtDescrizione.Text
            .Query = txtQuery.Text
            .Comment = txtCommento.Text
            .Type = Request("TypeQ")
            .UserId = New UserIdentificator(accService.GetTicket.User.Key.Id)
            .Key = New NewsletterQueriesIdentificator(Request("id"))
        End With
      
        If Not Request("id") Is Nothing Then            
            If Not oNewsletterManger.Update(oNewsQueryValue) Is Nothing Then
                CreaReloadScript()
            Else
                CreaAlertScript(Err.Replace("'", ""))
            End If
        Else
            If Not oNewsletterManger.Create(oNewsQueryValue) Is Nothing Then
                CreaReloadScript()
            Else
                CreaAlertScript(Err.Replace("'", ""))
            End If
        End If
    End Sub
    
    Sub CreaReloadScript()
        StrJs = "<script>"
        StrJs += "window.location='" + creaUrlParam("SHOW") + "'"
        StrJs += "</s" & "cript>"		        
    End Sub
    
    Function Couting(ByRef counter As Int16) As Int32
               
        counter += 1
        Return counter
    End Function
    
   

</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><asp:literal id="litTitle" runat="server" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	<style>
    .hide
        {display:none}
</style>
</head>

<body style="text-align:left">
	<form id="Form1" runat="server">
	<asp:textbox id="txtHidden2" runat="server" width="100%" style="display:none"/>

	<div class="dToolbar" runat="server"  style=" margin-top:10px; margin-left:5px;margin-bottom:5px" id="divToolbar">		
		<input type="button" class="button" value="New Query" onclick="location.href='<%=creaUrlParam("NEW")%>'; this.style.display='none'" />
		<input type="button" class="button" value="Queries List" onclick="location.href='<%=creaUrlParam("SHOW")%>'; this.style.display='none'" />
	</div>

	<asp:panel runat="server" ID="pnlEdit" Visible ="false" style="padding-left:10px;width:98%"> 
	    <div >
            <strong><asp:Literal ID ="litMess" runat="server" /></strong>
           <table  style="width:98%; border:1px solid #ffffff ">
				<tr style="background-color:#F7B539; color:#ffffff">
					<td style="color:#ffffff; font-weight:bold">Descriprion : </td> 
				</tr>
				<tr>
					<td><asp:TextBox ID="txtDescrizione" runat="server" Width="550px" Columns="99"/></td> 
				</tr>
				<tr style="background-color:#F7B539">
					<td style="color:#ffffff; font-weight:bold">Query :</td>
				</tr>
				<tr >
					<td><asp:TextBox ID="txtQuery" runat="server" Columns="99" rows ="10"  Width="550px" TextMode="MultiLine" /></td> 
				</tr>
				<tr style="background-color:#F7B539; color:#ffffff">
					<td style="color:#ffffff; font-weight:bold">Comment :</td>
				</tr>
				<tr>
					<td><asp:TextBox ID="txtCommento" runat="server" Columns="99" rows ="10" Width="550px" TextMode="MultiLine" /></td> 
				</tr>
				<tr>
					<td align ="right" >
						<asp:button id="btnMostra" onclick="ProvaQuery" runat="Server" Text="Test Query" cssclass="button"/>
						<asp:button id="btnSalva" onclick="SalvaQuery" runat="Server" Text="Save" cssclass="button"/>
					</td> 
				</tr>
            </table>
        </div>
	</asp:panel>
	
	<asp:panel runat="server" ID="pnlShow" Visible ="false" style="margin-left:5px">
		<br/>
		<asp:DataGrid ID="dgQuery" runat ="server"
			AutoGenerateColumns="False" 
			OnPageIndexChanged="cambiapagina" 			
			AllowPaging="True" 
			PagerStyle-Mode="NumericPages"
			PagerStyle-HorizontalAlign="Center"
			PagerStyle-Position="Top"		
			HorizontalAlign="Center" 
			PageSize="5"
			PagerStyle-CssClass="Pager"
			HeaderStyle-CssClass="header"
			DataKeyField="Key"
			CssClass="tbl1"
			onItemDataBound="ReloadChk"                   
			AllowSorting ="true"
			OnSortCommand="ordinamento">
			<Columns>  
				<asp:TemplateColumn ItemStyle-Width="20px">
					<ItemTemplate>
						<asp:radiobutton runat="server" id="chkSel" onclick="gestClick(this,'dgQuery')" GroupName='<%#container.dataitem.Key.id%>' />
						<asp:literal id="litVal" runat="Server" text='<%#container.dataitem.Key.id%>' visible="false" />
					</ItemTemplate>
				</asp:TemplateColumn>
	      
				 <asp:TemplateColumn HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Description" >
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Date Create"  >
                    <ItemTemplate ><%#DataBinder.Eval(Container.DataItem, "DataInsert", "{0:MM/dd/yyyy}")%></ItemTemplate>
                </asp:TemplateColumn>
                												
				<asp:BoundColumn HeaderStyle-CssClass="hide" ItemStyle-CssClass ="hide" DataField ="Query"></asp:BoundColumn>
				<asp:BoundColumn HeaderStyle-CssClass="hide" ItemStyle-CssClass ="hide" DataField ="Comment"></asp:BoundColumn>
	            
	            <asp:TemplateColumn ItemStyle-Width="10px">
					<ItemTemplate>
						<asp:ImageButton CommandName ='<%#setCommandName(container.dataitem.key.id,container.dataitem.Description)%>' commandargument ='<%#container.dataitem.query%>' onClick="showPreview" ID="ImageButton1" runat="server"  ImageUrl ="../HP3Image/Ico/content_preview.gif" ToolTip ="Test query"/>
					</ItemTemplate>
				</asp:TemplateColumn>
	            
				<asp:TemplateColumn ItemStyle-Width="10px">
					<ItemTemplate>
						<a href='<%#creaUrl(container.dataitem.key.id)%>' >	
							<img src="../HP3Image/Ico/content_edit.gif" title="Edit Query" />		                        
						</a>
					</ItemTemplate>
				</asp:TemplateColumn>
	            
				<asp:TemplateColumn ItemStyle-Width="10px">
					<ItemTemplate>
	                    
					</ItemTemplate>
				</asp:TemplateColumn>
	            
			</Columns>
		</asp:DataGrid>
        
        <br/>
        <div style="margin-left:5px">
            <input runat="server" id="btnSelect" type ="button" value="Select" class="button" onclick="SelectItem()"/>
        </div>
        <br/>                                                                
                        
	</asp:panel>
          
          <div style="width:98%;padding-left:10px">
          <a name ="ListaUtenti"></a>
           
          <asp:Panel ID="pnlPreviewData" visible="false" runat="server">
			<fieldset class="fsStdGroup">
				<h3>Query</h3>
				<div class="subbox">					
					<table class="tbl1">
						<colgroup class="cgHeavyLabel" />
						<colgroup />
						<tr>
							<td>Id</td>
							<td><asp:label ID="lblId" runat="server" /></td>
						</tr>				
						<tr>
							<td>Description</td>
							<td><asp:label ID="lblDescr" runat="server" /></td>
						</tr>				
					</table>					
				</div>
			</fieldset>
          </asp:Panel>
          
        <asp:Panel id="pnlRecipients" visible="false" runat="server">
			<fieldset class="fsStdGroup">
				<h3>Recipients list</h3>
				<div class="subbox">
					<asp:label ID="lblRecipients" runat="server" />
				</div>
			</fieldset>
        </asp:Panel>

        <asp:Panel id="pnlSql" visible="false" runat="server" >
			<fieldset class="fsStdGroup">
				<%--<h3>Query</h3>--%>
				<div class="subbox">
					<asp:label ID="lblSql" runat="server" />
				</div>
			</fieldset>
        </asp:Panel>

       
         <asp:Panel ID ="pnlError" runat="server" CssClass="dErrorMsg" Visible ="False" />
         <asp:gridview ID="gridTest" 
                            runat="server"
                            HorizontalAlign="Center"                                                 
                            HeaderStyle-CssClass="header"
                            FooterStyle-CssClass ="gridFooter"
                            CssClass="tbl1"
                            AutoGenerateColumns ="false"
                            Width="100%" 
                            >
                          <Columns >
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="User Id" >
                                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "User.Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Related User Id">
                                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "RelatedUser.Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Related User Name" >
                                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "RelatedUser.Name")%></ItemTemplate>
                            </asp:TemplateField>  
                              <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Related User Surname" >
                                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "RelatedUser.Surname")%></ItemTemplate>
                            </asp:TemplateField> 
                          
                         </Columns>
                       
            </asp:gridview>
      
        
        
            <div style="text-align:center;padding-top:5px" >
                <input type="button"  visible ="false" runat="server" id="btnSendNewsletter" value="Send Newsletter" class="button" onclick ="window.open('popSendNewsletter.aspx','','modal=yes,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=600,height=60');window.close()"/>
            </div>
            </div>
	</form>
</body>
</html>
<script >

function checkBtn()
    {
        var txt =document.getElementById ('<%=txtQuery.clientid%>')
        if (txt != null)
            {
                if (txt.value == '' )
                    {
                    alert('Insert query, please')
                    return false
                    }
                else    
                    return true
            }
    }
function SelectItem()
    {
    
        var idChk
	    var curId 
	    var i
	    var arrtd
	        
        var txt = document.getElementById ('<%=txtHidden2.clientid%>')
       
        if (txt != null)
            {
                if (txt.value=='')
                    alert('Select query, please')
                else
                    {
                        
                        idChk = document.getElementById (txt.value).name.split('$')
                        
                        curId = idChk[idChk.length-1]
                        
                        var tbl = document.getElementById ('<%=dgQuery.clientid%>')
                        
                        if (tbl != null)
                            {
                            
                                var arr = tbl.getElementsByTagName('tr') 
                                if (arr.length > 0)
                                    {	   
                                                               
                                        for (i=1;i<arr.length-1;i++)	                            
                                            {        	  
                                                if (arr[i].id.indexOf('trQ_')==-1)                              
                                                    {
                                                    arrtd=arr[i].getElementsByTagName('td') 
            	                                
            	                                if (navigator.appName=="Netscape")
                                                {
                                                    valore=arrtd[1].textContent
                                                    valore4=arrtd[4].textContent
                                                }
                                                else
                                                {
                                                    valore=arrtd[1].innerText
                                                    valore4=arrtd[4].innerText
                                                }

                                                    if (curId == valore )
                                                        {

                                                            window.opener.setRelQuery(valore4) 
                                                            this.close()
                                                        }
    	                                            }
                                            }	                        
                                    }
                            }	      
                    }
            }
    }
    
    function gestAddedRow(id)
        {
             var tbl = document.getElementById ('<%=dgQuery.clientid%>')
             var ancor = document.getElementById ('ancor_' + id)
             
             if (tbl != null)
                {
                    var arr = tbl.getElementsByTagName('tr') 
	                if (arr.length > 0)
	                    {	                               
	                        for (i=0;i<arr.length;i++)	 
	                          {
	                            
	                            if (arr[i].id == 'trQ_' + id)
	                             {	                                                             
                                       if (arr[i].style.display=='')
                                           { 
                                            arr[i].style.display = 'none'
                                            ancor.src ='../HP3Image/Ico/ico_15x_plus.gif'
                                           }
                                       else
                                            {
                                             arr[i].style.display = ''
                                             ancor.src ='../HP3Image/Ico/ico_15x_minus.gif'
                                            }
                                      
                                      break;
                                                                         
                                 }
	                          }
	                    }                                                      
                }
        }
        
        function gestClick(obj,src)
	    {   	   	                 	     	                                	       
    	    var txtH = '<%=txtHidden2.clientid%>'      	        
                      	           	            	    
	        var oText = document.getElementById (txtH)
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.id))
	            {	          
	                ochk = document.getElementById (oText.value);
	                if (ochk != null) ochk.checked = false;
	            }
	         	       
	        oText.value = obj.id	  	        
	       
	    }
	    	
</script>
<%=StrJs%>