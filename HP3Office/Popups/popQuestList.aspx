<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oQuestSearcher As QuestionnaireSearcher
    Dim QuestManager As New QuestionnaireManager
    
    Private utility As New WebUtility
    Const maxRow As Integer = 1
        
    Sub Page_Load()
       
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Description & "_" & Request("sites")
        End If
        
        Dim questColl As QuestionnaireCollection = Read()
        
        'Se il numero di ruoli � >= maxRow allora il risultato viene associato alla GridView 
        'altrimenti al repeater
        If Not (questColl Is Nothing) Then
            If (questColl.Count >= maxRow) Then
                gridList.DataSource = questColl
                gridList.DataBind()
                
            Else
                repQuest.DataSource = questColl
                repQuest.DataBind()
            End If
        End If
        
    End Sub
        
    Function Read() As QuestionnaireCollection
       
        Dim qCollection As QuestionnaireCollection
        oQuestSearcher = New QuestionnaireSearcher()
        
        qCollection = QuestManager.Read(oQuestSearcher)
        Return qCollection
    End Function
    
    Function ReadLabel(ByVal idQuest As Int32) As QuestionnaireValue
        Dim qValue As QuestionnaireValue = QuestManager.Read(New QuestionnaireIdentificator(idQuest))
        
        Return qValue
    End Function
    
   	
    Function setVisibility(ByVal id As Int32) As String
        Dim qCollection = Read()
        If qCollection Is Nothing OrElse qCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oQuestSearcher = New QuestionnaireSearcher()
        If txtId.Value <> "" Then oQuestSearcher.Key.Id = txtId.Value
        If txtDesciption.Value <> "" Then oQuestSearcher.Description = txtDesciption.Value
                                
        BindGrid(objGrid, oQuestSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As QuestionnaireSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New QuestionnaireSearcher
        End If
        
        Dim _oQuestColl As QuestionnaireCollection = QuestManager.Read(Searcher)
        If Not (_oQuestColl Is Nothing) Then
                   
            If (_oQuestColl.Count >= maxRow) Then
                ActiveGridView()
                
                objGrid.DataSource = _oQuestColl
                objGrid.DataBind()

            Else
                ActiveRepeater()
                
                repQuest.DataSource = _oQuestColl
                repQuest.DataBind()
                
            End If
        Else
            ActiveGridView()
            
            objGrid.DataSource = _oQuestColl
            objGrid.DataBind()
        End If
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
         
    'rende visibile il GridView
    Sub ActiveGridView()
        gridList.Visible = True
        repQuest.Visible = False
    End Sub
    
    'rende visibile il Repeater
    Sub ActiveRepeater()
        gridList.Visible = False
        repQuest.Visible = True
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Questionnaires List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="questionnairesForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Questionnaire Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:300px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                           >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Name" SortExpression="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
         
        <div id="divRepeater" class="title" style="margin-top:10px">    
            <asp:Repeater id="repQuest" runat="Server">
                <HeaderTemplate></HeaderTemplate>
                <itemtemplate>							
                    <div class="dControl" >
                        <span  <%#fDisable(container.DataItem.Key.id)%> >
                        <%If Not bSingleSel Then%>
                        <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.Description = "", "[Descrizione Assente]", Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%Else%>
                        <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.Key.id%>' type="radio"/>&nbsp;<%#iif(Container.DataItem.Description="","[Descrizione Assente]",Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%end if%>
                        </span>
                    </div>
                </itemtemplate>
            </asp:Repeater>
        </div>
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>