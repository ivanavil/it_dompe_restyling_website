<%@ Page Language="VB"  EnableEventValidation="true" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack" %>

<script runat="Server">
    Private webTracKSearcher As New WebTrackSearcher()
    Private webTrackManager As New WebTrackManager()
    Private gdw_Visible As Boolean = False
    Private userId As String
    
    Private userHCPManager As New UserHCPManager
    Private _totContents As Integer = 0
    Private _totTime As String = ""
    
    'si � deciso di fissare a max 50 le sessioni utente restituite
    Const maxRows As Integer = 50
    
    Property TotContents()
        Get
            _totContents = ViewState("TotContents")
            Return _totContents
        End Get
        Set(ByVal value)
            _totContents = value
            ViewState("TotContents") = _totContents
        End Set
    End Property
    
    Property TotTime() As String
        Get
            _totTime = ViewState("TotTime")
            Return _totTime
        End Get
        Set(ByVal value As String)
            _totTime = value
            ViewState("TotTime") = _totTime
        End Set
    End Property

    Sub Page_Load()
        
        
        If Not Request("SelItem") Is Nothing Then
            userId = Request("SelItem")
        End If
        
        Dim usValue As UserValue = ReadUser(userId)
              
        If Not (usValue Is Nothing) Then
            loadUserDetail(usValue)
                    
            If (usValue.UserType = Healthware.HP3.Core.User.ObjectValues.UserValue.UsersType.HCP) Then
                divHcp.Visible = True
            End If
       
            If Not (Page.IsPostBack) Then
                TotContents() = ContentViewed()
            End If
            tbViewedContents.Text = TotContents()

            ReadUserSession(userId)
            VisitInfo(userId)
        End If
    End Sub
    
    'recupera lo userValue in base all'id dell'utente
    Function ReadUser(ByVal userId As Integer) As UserValue
        Return Me.BusinessUserManager.Read(New UserIdentificator(userId))
    End Function
    
    'dato l'id utente recupera tutte le sessioni di un utente
    Sub ReadUserSession(ByVal userId As Integer)

        Dim webTrackSearcher As New WebTrackSearcher
        
        webTrackSearcher.IdUser = userId
        webTrackSearcher.MaxRows = maxRows
        Dim userSessionColl As WebTrackUserSessionCollection = webTrackManager.ReadTrackUserSession(webTrackSearcher)
        
        gdw_sessionList.DataSource = userSessionColl
        gdw_sessionList.DataBind()
       
        Dim totTime As String = ""
        If Not (userSessionColl Is Nothing) AndAlso (userSessionColl.Count > 0) Then
            Dim totPageV As Integer = 0
            Dim totDuration As Long = 0
            
            For Each userSessionValue As WebTrackUserSessionValue In userSessionColl
                totPageV = totPageV + userSessionValue.PageView
                totDuration = totDuration + DateDiff(DateInterval.Second, userSessionValue.StartDateSession, userSessionValue.EndDateSession)
            Next
           
            tbTotalTime.Text = New TimeSpan(0, 0, totDuration).ToString
            ' = totTime
            tbTotPagesViewed.Text = totPageV
        End If

    End Sub
            
    'popola il gridView che visulalizza le informazioni relative al tracciamento 
    'delle azioni dell'utente in una sessione 
    Function OrderDetails(ByVal trackUserSessionValue As WebTrackUserSessionValue) As WebTrackCollection
        Dim webTrackSearcher As New WebTrackSearcher()
        Dim webTrackManager As New WebTrackManager
        
        webTrackSearcher.IdSessionNum = trackUserSessionValue.SessionNum
        Dim webTrackColl As WebTrackCollection = webTrackManager.ReadTrack(webTracKSearcher)
                  
        Return webTrackColl
    End Function
    
    Function ContentViewed() As Integer
        Dim idUser As Integer = Request("SelItem")
        If idUser <= 0 Then Return 0
        Return webTrackManager.ReadTrackUserTotalContents(idUser)
    End Function
    
   Sub VisitInfo(ByVal userId As Integer)
        Dim webTrackManager As New WebTrackManager
        Dim webTrackSearcher As New WebTrackSearcher
        
        webTrackSearcher.IdUser = userId
        Dim webTrackUserColl As WebTrackUserCollection = webTrackManager.ReadUser(webTrackSearcher)
        
        If Not (webTrackUserColl Is Nothing) AndAlso (webTrackUserColl.Count > 0) Then
            Dim webTrackUserValue As WebTrackUserValue = webTrackUserColl(0)
            tbLastVisit.Text = GetDateFormat(webTrackUserValue.DateLastVisit)
            tbTotalVisits.Text = webTrackUserValue.TotVisits
        End If
    End Sub
    
    'rende visibili le informazioni dell'utente
    Sub loadUserDetail(ByVal uValue As UserValue)
        Dim userEmail As String = getFirstElem(uValue.Email)
        Dim userAddress As String = getFirstElem(uValue.Address)
        Dim userTel As String = getFirstElem(uValue.Telephone)
        Dim userCellular As String = getFirstElem(uValue.Cellular)
        Dim userFax As String = getFirstElem(uValue.Fax)
        
        If (uValue.Name <> String.Empty) Then tbName.Text = uValue.Name
        If (uValue.Surname <> String.Empty) Then tbSurname.Text = uValue.Surname
        If (uValue.Gender <> String.Empty) Then tbGender.Text = uValue.Gender
          
        If (uValue.Birthday.HasValue) Then tbDataBirth.Text = uValue.Birthday.Value
        If (userEmail <> String.Empty) Then tbEmail.Text = userEmail
        If (uValue.DegreeDate.HasValue) Then tbDataDegree.Text = uValue.DegreeDate.Value
          
        If (uValue.CF <> String.Empty) Then tbCF.Text = uValue.CF
        If (userTel <> String.Empty) Then tbTel.Text = userTel
        If (userCellular <> String.Empty) Then tbCell.Text = userCellular
          
        If (userFax <> String.Empty) Then tbFax.Text = userFax
        If (userAddress <> String.Empty) Then tbAddress.Text = userAddress
        If (uValue.Geo.Key.Id <> 0) Then tbCountry.Text = getGeoDescription(uValue.Geo.Key.Id)
          
        If (uValue.Province <> String.Empty) Then tbProvince.Text = uValue.Province
        If (uValue.City <> String.Empty) Then tbCity.Text = uValue.City
        If (uValue.Cap <> String.Empty) Then tbCap.Text = uValue.Cap
             
        If (uValue.RegistrationDate.HasValue) Then tbRegistrationDate.Text = GetDateFormat(uValue.RegistrationDate.Value)
        If (uValue.StatusDataRichiesta.HasValue) Then tbStatusRegistationDate.Text = GetDateFormat(uValue.StatusDataRichiesta.Value)
        If (uValue.StatusDataAzione.HasValue) Then tbStatusActionDate.Text = GetDateFormat(uValue.StatusDataAzione.Value)
        Dim siteVelue As SiteAreaValue = getSiteLabel(uValue.RegistrationSite.Id)
        
        If (uValue.RegistrationSite.Id <> 0) And Not (siteVelue Is Nothing) Then tbSiteFrom.Text = siteVelue.Name
               
        Select Case uValue.Status
            Case 1
                tbStatus.Text = "Active"
            Case 2
                tbStatus.Text = "Pending"
            Case 3
                tbStatus.Text = "No active"
            Case 4
                tbStatus.Text = "Disabled"
            Case 5
                tbStatus.Text = "Deleted"
        End Select
    End Sub
    
    'rende visibili le informazioni professionali dell'utente Hcp
    Sub loadUserHCPDetail(ByVal userHcpValue As UserHCPValue)
        Dim hcpEmail As String = getFirstElem(userHcpValue.HCPEmail)
        Dim hcpAddress As String = getFirstElem(userHcpValue.HCPAddress)
        Dim hcpTel As String = getFirstElem(userHcpValue.HCPTelephone)
        Dim hcpCellular As String = getFirstElem(userHcpValue.HCPCellular)
        Dim hcpFax As String = getFirstElem(userHcpValue.HCPFax)
        
        If (userHcpValue.HCPRegistration <> String.Empty) Then tbHcpRegistration.Text = userHcpValue.HCPRegistration
        If (userHcpValue.HCPWard <> String.Empty) Then tbHcpWard.Text = userHcpValue.HCPWard
        If (userHcpValue.HCPStructure <> String.Empty) Then tbHcpStructure.Text = userHcpValue.HCPStructure
        
        If (hcpEmail <> String.Empty) Then tbHcpEmail.Text = hcpEmail
        If (hcpAddress <> String.Empty) Then tbHcpAddress.Text = hcpAddress
        If (userHcpValue.HCPCap <> String.Empty) Then tbHcpCap.Text = userHcpValue.HCPCap
        
        If (userHcpValue.HCPGeo.Key.Id <> 0) Then tbHcpCountry.Text = getGeoDescription(userHcpValue.HCPGeo.Key.Id)
        If (userHcpValue.HCPProvince <> String.Empty) Then tbHcpProvince.Text = userHcpValue.HCPProvince
        If (userHcpValue.HCPCity <> String.Empty) Then tbHcpCity.Text = userHcpValue.HCPCity
        
        If (hcpTel <> String.Empty) Then tbHcpTel.Text = hcpTel
        If (hcpCellular <> String.Empty) Then tbHcpCell.Text = hcpCellular
        If (hcpFax <> String.Empty) Then tbHcpFax.Text = hcpFax
    End Sub
    
    'attiva il pannello con i dettagli dell'utente HCP
    Sub viewProfessionalDetail(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        divProfetionalDetail_1.Visible = True
        divProfetionalDetail_2.Visible = True
        
        Dim userHCPSearcher As New UserHCPSearcher
        Dim userHcpValue As New UserHCPValue
        
        userHCPSearcher.Key.Id = userId
        userHcpValue = userHCPManager.Read(userHCPSearcher)(0)
       
        If Not userHcpValue Is Nothing Then
            loadUserHCPDetail(userHcpValue)
        End If
        btn_plus.Visible = False
        btn_minus.Visible = True
    End Sub
    
     
    Sub hideProfessionalDetail(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        btn_plus.Visible = True
        btn_minus.Visible = False
        
        divProfetionalDetail_1.Visible = False
        divProfetionalDetail_2.Visible = False
    End Sub
    
    'visualizza i dettagli della navigazione dell'utente
    Sub ViewTraceDeatil(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        gdw_Visible = True
    End Sub
       
    'restituisce il nome della siteArea in base all'id
    Function getSiteLabel(ByVal idSite As Int32) As SiteAreaValue
        Return Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(idSite))
    End Function
    
    'restituisce il primo elemento  di un oggetto MultiFieldType
    Function getFirstElem(ByRef mutiField As Healthware.HP3.Core.Base.ObjectValues.MultiFieldType) As String
        Return mutiField.Item(0)
    End Function
    
    'restituisce la description in base ala geoId
    Function getGeoDescription(ByVal geoId As Int32) As String
        Dim geoValue As GeoValue = Me.BusinessGeoManager.Read(New GeoIdentificator(geoId))
        
        If Not (geoValue Is Nothing) Then Return geoValue.Description
        Return "-"
    End Function
  
    Function getVisibility() As Boolean
        Return gdw_Visible
    End Function
    
    Protected Sub gdw_sessionList_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        Dim fatherGV As GridView = DirectCast(sender, GridView)
        Dim rowIndex As Integer = e.CommandArgument
        Select Case e.CommandName
            Case "Open"
                Dim childGV As GridView = DirectCast(fatherGV.Rows(rowIndex).FindControl("gdw_traceDetail"), GridView)
                If Not childGV Is Nothing Then
                    childGV.Visible = True
                    
                End If
                
            Case "Close"
                Dim childGV As GridView = DirectCast(fatherGV.Rows(rowIndex).FindControl("gdw_traceDetail"), GridView)
                If Not childGV Is Nothing Then
                    childGV.Visible = False
                End If
        End Select
    End Sub
    
    Function getDuration(ByVal sessionValue As WebTrackUserSessionValue) As String
        Dim startDate As DateTime = sessionValue.StartDateSession
        Dim endDate As DateTime = sessionValue.EndDateSession
        
        Return New TimeSpan(0, 0, DateDiff(DateInterval.Second, startDate, endDate)).ToString
    End Function
    
    Function GetDateFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToString("dd/MM/yyyy")
    End Function
    
    Function GetTimeFormat(ByVal value As String) As String
        If value Is Nothing Or value = String.Empty Then Return String.Empty
        Return DateTime.Parse(value).ToShortTimeString()
    End Function
    
    Protected Sub gridSessionUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindGrid(sender)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView)
        If Not Request("SelItem") Is Nothing Then
            userId = Request("SelItem")
        End If
       
        'webTrackManager.Cache = False
        Dim webTrackSearcher As New WebTrackSearcher
        
        webTrackSearcher.IdUser = userId
        webTrackSearcher.MaxRows = maxRows
        Dim userSessionColl As WebTrackUserSessionCollection = webTrackManager.ReadTrackUserSession(webTrackSearcher)
     
        gdw_sessionList.DataSource = userSessionColl
        gdw_sessionList.DataBind()
    End Sub
  </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>User Details</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
    <form id="formHistory" runat="server" >
     <div class="title" style="padding-left:10px">User Details</div><br />
     <hr />
     
        <div id="divUser" style="float:left; width:50%;padding-left:10px;">
           <%--name--%>
	       <div class="divFormLabel">
                <asp:Label id="lbName"  EnableViewState="false" AssociatedControlID="tbName" Text="Name: " runat="server" />
           </div>
           <div class="divFormObject">
                   <asp:Literal id="tbName" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
           
            <%--surname--%>
           <div class="divFormLabel">
                <asp:Label id="lbSurname"  EnableViewState="false" AssociatedControlID="tbSurname" Text="SurName: " runat="server" />
           </div>
           <div class="divFormObject">
                  <asp:Literal id="tbSurname" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
            <%--gender--%>
           <div class="divFormLabel">
                <asp:Label id="lbGender" EnableViewState="false" AssociatedControlID="tbGender" Text="Gender: " runat="server" />
           </div>
           <div class="divFormObject">
                 <asp:Literal id="tbGender" Text="-" EnableViewState="false" runat="server"></asp:Literal>
          </div>     
            <div class="divFormClearer"></div>
            
            <%--Date of Birth--%>
           <div class="divFormLabel">
                <asp:Label id="lbDataBirth" EnableViewState="false" AssociatedControlID="tbDataBirth" Text="Date of Birth: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbDataBirth" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
           <%--Email--%>
           <div class="divFormLabel">
                <asp:Label id="lbEmail" EnableViewState="false" AssociatedControlID="tbEmail" Text="Email: " runat="server" />
           </div>
           <div class="divFormObject">
                <asp:Literal id="tbEmail" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
           <%--Date of Degree--%>
           <div class="divFormLabel">
                <asp:Label id="lbDataDegree" EnableViewState="false" AssociatedControlID="tbDataDegree" Text="Date of Degree: " runat="server" />
           </div>
           <div class="divFormObject">
                <asp:Literal id="tbDataDegree" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
              
             <%--CF--%>
           <div class="divFormLabel">
                <asp:Label id="lbCF" EnableViewState="false" AssociatedControlID="tbCF" Text="CF: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbCF" Text="-"  EnableViewState="false" runat="server"></asp:Literal> 
           </div>     
           <div class="divFormClearer"></div>
            
           <%--TEL--%>
           <div class="divFormLabel">
                <asp:Label id="lbTel" EnableViewState="false" AssociatedControlID="tbTel" Text="Phone Number: " runat="server" />
           </div>
           <div class="divFormObject">
                <asp:Literal id="tbTel" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
            <%--CELL--%>
           <div class="divFormLabel">
                <asp:Label id="lbCell" EnableViewState="false" AssociatedControlID="tbCell" Text="Cell Number: " runat="server" />
           </div>
           <div class="divFormObject">
                 <asp:Literal id="tbCell" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
             <%--Fax--%>
           <div class="divFormLabel">
                <asp:Label id="lbFax" EnableViewState="false" AssociatedControlID="tbFax" Text="Fax Number: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbFax" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
            <%--Address--%>
           <div class="divFormLabel">
                <asp:Label id="lbAddress" EnableViewState="false" AssociatedControlID="tbAddress" Text="Address: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbAddress" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
            <%--Country--%>
           <div class="divFormLabel">
                <asp:Label id="lbCountry" EnableViewState="false" AssociatedControlID="tbCountry" Text="Country: " runat="server" />
           </div>
           <div class="divFormObject" >
                  <asp:Literal id="tbCountry" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
             <%--Province--%> 
           <div class="divFormLabel">
                <asp:Label id="lbProvince" EnableViewState="false" AssociatedControlID="tbProvince" Text="Country/ State / Province: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbProvince" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
            
             <%--City--%>
           <div class="divFormLabel">
                <asp:Label id="lbCity" EnableViewState="false" AssociatedControlID="tbCity" Text="City: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbCity" Text="-" EnableViewState="false" runat="server"></asp:Literal>   
           </div>     
           <div class="divFormClearer"></div>
            
             <%--Cap--%>
           <div class="divFormLabel">
                <asp:Label id="lbCap" EnableViewState="false" AssociatedControlID="tbCap" Text="ZIP Postal Code: " runat="server" />
           </div>
           <div class="divFormObject">
                    <asp:Literal id="tbCap" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>     
           <div class="divFormClearer"></div>
           
           <%--Newsletter--%>
           <%--<div class="divFormLabel">
                <asp:Label id="lbNewsletter" EnableViewState="false" AssociatedControlID="tbNewsletter" Text="Receives Newsletter: " runat="server" />
           </div>
           <div class="divFormObject">
                 <asp:Literal id="tbNewsletter" Text="-" EnableViewState="false" runat="server"></asp:Literal>
           </div>--%>
        </div>
           
        <div style="float:right;width:40%;">
               <%--Site From--%>
               <div class="divFormLabel">
                    <asp:Label id="lbSiteFrom" EnableViewState="false" AssociatedControlID="tbSiteFrom" Text="Site From: " runat="server" />
               </div>
               <div class="divFormObject">
                     <asp:Literal id="tbSiteFrom" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div class="divNewFormClearer"></div>
               
                <%--Registration Date--%>
               <div class="divFormLabel">
                    <asp:Label id="lbRegistrationDate" EnableViewState="false" AssociatedControlID="tbRegistrationDate" Text="Registration Date: " runat="server" />
               </div>
               <div class="divFormObject">
                     <asp:Literal id="tbRegistrationDate" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div class="divNewFormClearer"></div>
               
                            
               <%--Status--%>
               <div class="divFormLabel">
                    <asp:Label id="lbStatus" EnableViewState="false" AssociatedControlID="tbStatus" Text="Status: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbStatus" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div class="divNewFormClearer"></div>
               
                <%--Status Registration Date--%>
               <div class="divFormLabel">
                    <asp:Label id="lbStatusRegistationDate" EnableViewState="false" AssociatedControlID="tbStatusRegistationDate" Text="Status Registation Date: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbStatusRegistationDate" Text="-" EnableViewState="false" runat="server"></asp:Literal>    
               </div>     
               <div class="divNewFormClearer"></div>
               
                <%--Status Action Date--%>
               <div class="divFormLabel">
                    <asp:Label id="lbStatusActionDate" EnableViewState="false" AssociatedControlID="tbStatusActionDate" Text="Status Action Date: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbStatusActionDate" Text="-" EnableViewState="false" runat="server"></asp:Literal>  
               </div>     
               <div class="divNewFormClearer" style="margin-top:110px;"></div>
               
                              
                <%--LastVisit--%>
               <div class="divFormLabel">
                    <asp:Label id="lbLastVisit" EnableViewState="false" AssociatedControlID="tbLastVisit" Text="LastVisit: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbLastVisit" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div style=" margin-bottom:5px"></div>
               
                <%--Viewed contents--%>
               <div class="divFormLabel">
                    <asp:Label id="lbViewedContents" EnableViewState="false" AssociatedControlID="tbViewedContents" Text="Viewed Contents: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbViewedContents" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div style="margin-bottom:5px"></div>
               
                <%--Totals pages viewed--%>
               <div class="divFormLabel">
                    <asp:Label id="lbTotPagesViewed" EnableViewState="false" AssociatedControlID="tbTotPagesViewed" Text="Total Pages Viewed: " runat="server" />
               </div>
               <div class="divFormObject">
                      <asp:Literal id="tbTotPagesViewed" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div style="margin-bottom:5px"></div>
               
                <%--Total Visits--%>
               <div class="divFormLabel">
                    <asp:Label id="lbTotalVisits" EnableViewState="false" AssociatedControlID="tbTotalVisits" Text="Total Visits: " runat="server" />
               </div>
               <div class="divFormObject">
                       <asp:Literal id="tbTotalVisits" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div style="margin-bottom:5px"></div>
               
               <%--Total Time--%>
               <div class="divFormLabel">
                    <asp:Label id="lbTotalTime" EnableViewState="false" AssociatedControlID="tbTotalTime" Text="Total Time: " runat="server" />
               </div>
               <div class="divFormObject">
                        <asp:Literal id="tbTotalTime" Text="-" EnableViewState="false" runat="server"></asp:Literal>
               </div>     
               <div style="margin-bottom:5px"></div>
        </div>
        <div class="divFormClearer"></div>
        
        <div id="divHcp" visible="false" style="margin-left:600px; margin-bottom:10px" runat="server">
                <%--<form id="fBt" style="margin-left:600px"  runat="server">--%>
                      <asp:Label ID ="lbProfDetail" Text ="Professional Details" runat="server" CssClass="title"/> <asp:ImageButton ID="btn_plus" ToolTip="Professional Detail" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="viewProfessionalDetail" />
                      <asp:ImageButton ID="btn_minus" ToolTip="Professional Detail" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/meno.png" onClick="hideProfessionalDetail" Visible="false" />
               <%-- </form>--%>
         </div>                        
        <div id="divProfetionalDetail_1" visible="false" runat="server" style="float:left;width:50%;padding-left:10px">
            <%--HCP Registration--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpRegistration" EnableViewState="false" AssociatedControlID="tbHcpRegistration" Text="Medical Registration Number: " runat="server" />
            </div>
            <div class="divFormObject">
                  <asp:Literal id="tbHcpRegistration" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
            
             <%--HCP Structure--%>
            <div class="divFormLabel" style="width:170px">
                <asp:Label id="lbHcpStructure" EnableViewState="false" AssociatedControlID="tbHcpStructure" Text="Name of Affiliated Structure: " runat="server" />
            </div>
            <div class="divFormObject">
                    <asp:Literal id="tbHcpStructure" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
           
            <%--HCP Ward--%>
           <div class="divFormLabel">
                <asp:Label id="lbWard" EnableViewState="false" AssociatedControlID="tbHcpWard" Text="Ward: " runat="server" />
            </div>
            <div class="divFormObject">
                     <asp:Literal id="tbHcpWard" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
        		
            <%--HCP Email--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpEmail" EnableViewState="false" AssociatedControlID="tbHcpEmail" Text="Email: " runat="server" />
            </div>
            <div class="divFormObject">
                    <asp:Literal id="tbHcpEmail" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
           
            <%--HCP Address--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpAddress" EnableViewState="false" AssociatedControlID="tbHcpAddress" Text="Address: " runat="server" />
            </div>
            <div class="divFormObject">
                      <asp:Literal id="tbHcpAddress" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
            
             <%--HCP CAP--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpCap" EnableViewState="false" AssociatedControlID="tbHcpCap" Text="ZIP Postal Code: " runat="server" />
            </div>
            <div class="divFormObject">
                    <asp:Literal id="tbHcpCap" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>     
            <div class="divFormClearer"></div>
       </div> 
        
       <div id="divProfetionalDetail_2" visible="false" runat="server" style="float:right;width:40%;">
           <%--HCP Country--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpCountry" AssociatedControlID="tbHcpCountry" Text="Country: " runat="server" />
            </div>
            <div class="divFormObject">
                     <asp:Literal id="tbHcpCountry" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div> 
            <div class="divNewFormClearer"></div>    
                         
             <%--HCP Province--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpProvince" AssociatedControlID="tbHcpProvince" Text="County / State / Province: " runat="server" />
            </div>
            <div class="divFormObject">
                       <asp:Literal id="tbHcpProvince" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>  
            <div class="divNewFormClearer"></div>   
                           
            <%--HCP City--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpCity" EnableViewState="false" AssociatedControlID="tbHcpCity" Text="City: " runat="server" />
            </div>
            <div class="divFormObject">
                   <asp:Literal id="tbHcpCity" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div> 
            <div class="divNewFormClearer"></div>    
                           
            <%--TEL--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpTel" EnableViewState="false" AssociatedControlID="tbHcpTel" Text="Phone Number: " runat="server" />
            </div>
            <div class="divFormObject">
                   <asp:Literal id="tbHcpTel" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>
            <div class="divNewFormClearer"></div>     
                            
            <%--CELL--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpCell" EnableViewState="false" AssociatedControlID="tbHcpCell" Text="Cell Number: " runat="server" />
            </div>
            <div class="divFormObject">
                    <asp:Literal  id="tbHcpCell" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>
            <div class="divNewFormClearer"></div>     
                            
            <%--Fax--%>
            <div class="divFormLabel">
                <asp:Label id="lbHcpFax" EnableViewState="false" AssociatedControlID="tbHcpFax" Text="Fax Number: " runat="server" />
            </div>
            <div class="divFormObject">
                      <asp:Literal id="tbHcpFax" Text="-" EnableViewState="false" runat="server"></asp:Literal>
            </div>
            <div class="divNewFormClearer"></div>     
      </div>
              
    <div id="divTraceHistory">
        <asp:gridview id="gdw_sessionList"  
                runat="server"
                AutoGenerateColumns="false" 
                OnRowCommand ="gdw_sessionList_RowCommand"
                CssClass="tbl1"
                AllowPaging="true"
                PagerSettings-Position="TopAndBottom"  
                PagerStyle-HorizontalAlign="Right"
                PagerStyle-CssClass="Pager"
                AlternatingRowStyle-BackColor="#E9EEF4"             
                AllowSorting="true"
                PageSize ="20"
                HeaderStyle-BorderWidth="0"
                emptydatatext="No item available"
                OnPageIndexChanging="gridSessionUsers_PageIndexChanging">
              <Columns>
               <asp:ButtonField ButtonType="Image" ControlStyle-BackColor="#EAEAEA" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" CommandName="Open" Text="Click here to view more details" />
               <asp:ButtonField ButtonType="Image" ImageUrl="~/HP3Office/HP3Image/Ico/meno.png" CommandName="Close" Text="Click here to close this panel"  />                 
                <asp:TemplateField> 
                         <HeaderTemplate>
                           <table >
                                <!-- this row has the parent grid -->
                                <tr>
                                    
                                    <td  style="width:200px;border:0;text-align:left;">Start</td>
                                    <td style="width:180px;border:0;text-align:left;">Finish</td>
                                    <td style="width:170px;border:0;text-align:left;">Page View</td>
                                    <td style="width:100px;border:0;text-align:left;">Duration</td>
                                </tr>
                            </table>
                         </HeaderTemplate>
                         <ItemTemplate>
                             <table  style="border-color:Maroon">
                                <!-- this row has the parent grid style="border:0;width:100%"-->
                                <tr>
                                   <td style="width:200px;border:0;text-align:left">
                                        <asp:Label ID="lbStart" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDateSession")%>'></asp:Label>
                                    </td>
                                    <td style="width:180px;border:0;text-align:left">
                                        <asp:Label ID="lbFinish" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDateSession")%>'></asp:Label>
                                    </td>
                                    <td style="width:170px;border:0;text-align:left">
                                        <asp:Label ID="lbPageView" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PageView")%>'></asp:Label>
                                    </td>
                                    <td style="width:100px;border:0;text-align:left">
                                        <asp:Label ID="lbDuration" runat="server" Text='<%#getDuration(Container.DataItem) %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="border:0">
                                         <table style="width:100%">
                                           <tr>
                                                <td style="border:0">
                                                   <asp:GridView id="gdw_traceDetail" 
                                                       runat="server" 
                                                       AutoGenerateColumns="False"
                                                       Visible="False"
                                                       RowStyle-BackColor="#FFFFFF"
                                                       AlternatingRowStyle-BackColor="#E9EEF4"
                                                       DataSource='<%#OrderDetails(Container.DataItem)%>'
                                                       emptydatatext="No item available"
                                                       GridLines="None">
                                                       <Columns>
                                                           <asp:TemplateField HeaderText="Hour" HeaderStyle-Width="5%">
                                                                <ItemTemplate><%#GetTimeFormat(DataBinder.Eval(Container.DataItem, "DateInsert"))%></ItemTemplate>
                                                           </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Site" HeaderStyle-Width="25%">
                                                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "SiteDescription")%></ItemTemplate>
                                                           </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Area" HeaderStyle-Width="25%">
                                                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "SiteAreaDescription")%></ItemTemplate>
                                                           </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Event" HeaderStyle-Width="25%">
                                                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "EventDescription")%></ItemTemplate>
                                                           </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Content" HeaderStyle-Width="40%">
                                                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ContentTitle")%></ItemTemplate>
                                                           </asp:TemplateField>
                                                       </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                         </ItemTemplate>
                </asp:TemplateField>
              </Columns>
        </asp:gridview>
    </div> 
</form>     
    </body>
</html>

