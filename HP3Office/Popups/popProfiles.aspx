<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oProfileSearcher As ProfileSearcher
    Private _sortType As ProfileGenericComparer.SortType
    
    Private utility As New WebUtility
    Const maxRow As Integer = 8
  
    Private Property SortType() As ProfileGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ProfileGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ProfileGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Sub Page_Load()
       
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Description & "_" & Request("sites")
        End If
        
        Dim profilesColl As ProfileCollection = Read()
        
        'Se il numero di ruoli � >= maxRow allora il risultato viene associato alla GridView 
        'altrimenti al repeater
        If Not (profilesColl Is Nothing) Then
            If (profilesColl.Count >= maxRow) Then
                gridList.DataSource = profilesColl
                gridList.DataBind()
                
            Else
                repProfile.DataSource = profilesColl
                repProfile.DataBind()
            End If
        End If
        
        'If Not (Page.IsPostBack) Then
        '    utility.LoadListControl(dwlCountry, profilesColl, "Country", "Key.Id")
        '    dwlCountry.Items.Insert(0, New ListItem("--Select Country", -1))
        'End If
    End Sub
        
    Function Read() As ProfileCollection
        Dim pCollection As ProfileCollection
        oProfileSearcher = New ProfileSearcher()
        
        pCollection = Me.BusinessProfilingManager.Read(oProfileSearcher)
        Return pCollection
    End Function
    
    ''' <summary>
    ''' Legge la descrizione del profilo dato l'id
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadLabel(ByVal idProfile As Int32) As ProfileValue
        Dim pValue As ProfileValue = Me.BusinessProfilingManager.Read(New ProfileIdentificator(idProfile))
        
        Return pValue
    End Function
    
   	
    Function setVisibility(ByVal id As Int32) As String
        Dim sCollection = Read()
        If sCollection Is Nothing OrElse sCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
	sub btnSave_OnClick(s as object,e as eventargs)
		dim el as Object
        Dim strOut As String = ""
		
		for each el in Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
		next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else            
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
	function fDisable(id as string) as string
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "        
        End If
        Return ""
    End Function
	
	function fChecked(id as string) as string
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oProfileSearcher = New ProfileSearcher()
        If txtId.Value <> "" Then oProfileSearcher.Key.Id = txtId.Value
        If txtDesciption.Value <> "" Then oProfileSearcher.Description = txtDesciption.Value
                                
        BindGrid(objGrid, oProfileSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ProfileSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New ProfileSearcher
        End If
        
        Dim _oProfileColl As ProfileCollection = Me.BusinessProfilingManager.Read(Searcher)
        If Not (_oProfileColl Is Nothing) Then
            _oProfileColl.Sort(SortType, RoleGenericComparer.SortOrder.ASC)
            
            If (_oProfileColl.Count >= maxRow) Then
                ActiveGridView()
                
                objGrid.DataSource = _oProfileColl
                objGrid.DataBind()

            Else
                ActiveRepeater()
                
                repProfile.DataSource = _oProfileColl
                repProfile.DataBind()
                
            End If
        Else
            ActiveGridView()
            
            objGrid.DataSource = _oProfileColl
            objGrid.DataBind()
        End If
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
   
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Description"
                SortType = ProfileGenericComparer.SortType.ByDescription
            Case "Id"
                SortType = ProfileGenericComparer.SortType.ById
        End Select
        
        BindWithSearch(sender)
    End Sub
    
    'rende visibile il GridView
    Sub ActiveGridView()
        gridList.Visible = True
        repProfile.Visible = False
    End Sub
    
    'rende visibile il Repeater
    Sub ActiveRepeater()
        gridList.Visible = False
        repProfile.Visible = True
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Profiles List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="profilesForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Profiles Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:300px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            OnSorting="gridList_Sorting"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Name" SortExpression="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
         
        <div id="divRepeater" class="title" style="margin-top:10px">    
            <asp:Repeater id="repProfile" runat="Server">
                <HeaderTemplate></HeaderTemplate>
                <itemtemplate>							
                    <div class="dControl" >
                        <span  <%#fDisable(container.DataItem.Key.id)%> >
                        <%If Not bSingleSel Then%>
                        <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.Description = "", "[Descrizione Assente]", Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%Else%>
                        <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.Key.id%>' type="radio"/>&nbsp;<%#iif(Container.DataItem.Description="","[Descrizione Assente]",Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%end if%>
                        </span>
                    </div>
                </itemtemplate>
            </asp:Repeater>
        </div>
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>