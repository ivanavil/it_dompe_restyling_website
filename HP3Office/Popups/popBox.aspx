<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Box"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private utility As New WebUtility
    
    Private oBoxSearcher As BoxSearcher
    Private _sortType As BoxGenericComparer.SortType
                  
    Private Property SortType() As BoxGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = BoxGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As BoxGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Description & "_" & Request("sites")
        End If
        
        Dim boxColl As BoxCollection = Read()
       
        gridList.DataSource = boxColl
        gridList.DataBind()
        
        If Not (Page.IsPostBack) Then
            Dim boxTypeColl As BoxTypeCollection = Me.BusinessBoxManager.ReadBoxType(0)
            
            utility.LoadListControl(dwlBoxType, boxTypeColl, "Description", "Id")
            dwlBoxType.Items.Insert(0, New ListItem("--Select Type--", -1))
        End If
    End Sub
        
    Function Read() As BoxCollection
        Dim sManger As New BoxManager
        Dim sCollection As BoxCollection
        Dim sSearcher As New BoxSearcher
             
        sCollection = sManger.Read(sSearcher)
        Return sCollection
    End Function
    
    ''' <summary>
    ''' Legge la descrizione del box dato l'id
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadLabel(ByVal idBox As Int32) As BoxValue
        Dim sManger As New BoxManager
        Dim sValue As BoxValue
        
        sValue = sManger.Read(New BoxIdentificator(idBox))
        Return sValue
    End Function
       	
    Function setVisibility(ByVal id As Int32) As String
        Dim sCollection = Read()
        If sCollection Is Nothing OrElse sCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
	sub btnSave_OnClick(s as object,e as eventargs)
		dim el as Object
        Dim strOut As String = ""
		
		for each el in Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
		next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else            
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca di ruoli
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oBoxSearcher = New BoxSearcher()
        If txtId.Value <> "" Then oBoxSearcher.Id = txtId.Value
        If txtDesciption.Value <> "" Then oBoxSearcher.Description = txtDesciption.Value
        If dwlBoxType.SelectedItem.Value <> -1 Then oBoxSearcher.BoxType.Id = dwlBoxType.SelectedItem.Value
        
        BindGrid(objGrid, oBoxSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As BoxSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New BoxSearcher
        End If
        
        Dim _oBoxColl As BoxCollection = Me.BusinessBoxManager.Read(Searcher)
        
        If Not (_oBoxColl Is Nothing) Then
            _oBoxColl.Sort(SortType, BoxGenericComparer.SortOrder.ASC)
        End If
        
        objGrid.DataSource = _oBoxColl
        objGrid.DataBind()
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Name"
                SortType = BoxGenericComparer.SortType.ByName
            Case "Id"
                SortType = BoxGenericComparer.SortType.ById
        End Select
        
        BindWithSearch(sender)
    End Sub
  
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Boxs List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="boxForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Box Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Name</strong></td>
                            <td><strong>Type</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:200px"/></td>  
                            <td><asp:DropDownList id="dwlBoxType"  runat="server" /></td>
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            OnSorting="gridList_Sorting"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.IdBox)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.IdBox)%>  name = 'chk_<%#Container.DataItem.Name.replace("'", "`")%>_<%#Container.DataItem.IdBox%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.IdBox)%>  name = 'chk_<%#Container.DataItem.Name.replace("'", "`")%>_<%#Container.DataItem.IdBox%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "IdBox")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Name" SortExpression="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Name")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
       
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>