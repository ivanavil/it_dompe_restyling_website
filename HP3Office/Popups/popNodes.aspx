<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oCatSearcher As CatalogSearcher
    Dim CatalogManager As New CatalogManager
    
    Private utility As New WebUtility
    Const maxRow As Integer = 1
           
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Title & "_" & Request("sites")
        End If
        
        Dim nodeColl As NodeCollection = Read()
        
        gridList.DataSource = nodeColl
        gridList.DataBind()
    End Sub
        
    Function Read() As NodeCollection
        Dim nodeColl As NodeCollection
        
        Dim catSearcher As New CatalogSearcher
        catSearcher.Active = SelectOperation.All
        catSearcher.Display = SelectOperation.All
  
        nodeColl = CatalogManager.ReadNodes(catSearcher)
        Return nodeColl
    End Function
    
    Function ReadLabel(ByVal pk As Integer) As NodeValue
        Dim catSearcher As New CatalogSearcher
        catSearcher.Active = SelectOperation.All
        catSearcher.Display = SelectOperation.All
        catSearcher.key.PrimaryKey = pk
        Dim nodeColl As NodeCollection = CatalogManager.ReadNodes(catSearcher)
        
        If Not (nodeColl Is Nothing) AndAlso (nodeColl.Count > 0) Then
            Return nodeColl(0)
        End If
        
        Return Nothing
    End Function
    
   	
    Function setVisibility(ByVal id As Int32) As String
        Dim nCollection = Read()
        If nCollection Is Nothing OrElse nCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oCatSearcher = New CatalogSearcher()
        
        If txtId.Value <> "" Then oCatSearcher.key.Id = txtId.Value
        If txtPK.Value <> "" Then oCatSearcher.key.PrimaryKey = txtPK.Value
        If txtDesciption.Value <> "" Then oCatSearcher.SearchString = txtDesciption.Value
                                
        BindGrid(objGrid, oCatSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As CatalogSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New CatalogSearcher
        End If
        
        Searcher.Active = SelectOperation.All
        Searcher.Display = SelectOperation.All
        Dim _oNodeColl As NodeCollection = CatalogManager.ReadNodes(Searcher)
        If Not (_oNodeColl Is Nothing) Then
            ActiveGridView()
            
            objGrid.DataSource = _oNodeColl
            objGrid.DataBind()
        End If
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
         
    'rende visibile il GridView
    Sub ActiveGridView()
        gridList.Visible = True
      
    End Sub
    
    'rende visibile il Repeater
    Sub ActiveRepeater()
        gridList.Visible = False
       
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Nodes List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="nodesForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Nodes Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>PK</strong></td>
                            <td><strong>ID</strong></td>    
                            <td><strong>Title</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtPK" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:300px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                           >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.PrimaryKey)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.PrimaryKey)%>  name = 'chk_<%#Container.DataItem.Title.Replace("'", "`")%>_<%#Container.DataItem.Key.PrimaryKey%>' type="checkbox"/>
                                        <%Else%>
                                            <input <%#fChecked(container.DataItem.Key.PrimaryKey)%>  name = 'chk_<%#Container.DataItem.Title.Replace("'", "`")%>_<%#Container.DataItem.Key.PrimaryKey%>' onclick="gestClick('chk_<%#Container.DataItem.Title.Replace("'", "`")%>_<%#Container.DataItem.Key.PrimaryKey%>')" type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="PrimaryKey">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.PrimaryKey")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Title">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Title")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
                 
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	       	var oText1 = document.getElementById(obj)
	        var ochk
	             
             if (oText.value!='') 
             {  
                ochk = document.getElementById (oText.value)
                if (ochk!=null)
                {
                    ochk.checked = false;
                }
            }
            oText.value= oText1.name;
            oText1.checked = true;
	    }
</script>