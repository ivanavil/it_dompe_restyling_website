<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName="ctlContentType" src="~/hp3Office/HP3Service/ctlContentTypeManager.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public DomainInfo As String
    
    Private StrJs As String
    Sub SelItem(ByVal s As Object, ByVal e As EventArgs)
        
        Dim ctcol As Object = ctlContentType.GetSelection
        Dim ov As ContentTypeValue
        If Not ctcol Is Nothing AndAlso ctcol.count > 0 Then
            ov = ctcol.item(0)
            
            If Not ov Is Nothing AndAlso Not Request("ctlHidden") Is Nothing AndAlso Not Request("ctlVisible") Is Nothing Then
                StrJs = "window.opener.SetValue('" & Request("ctlHidden") & "','" & Request("ctlVisible") & "'," & ov.Key.Id & ",'" & Server.HtmlDecode(ov.description).Replace("'", "`") & "');"
                StrJs += "window.close();"
            End If
                      
        Else
            StrJs = "alert('Select an item to continue.')"
        End If
    End Sub
    
    Sub Page_Load()
        If Not Request("SelItem") Is Nothing AndAlso Not Page.IsPostBack Then            
            ctlContentType.SetDefaultChecked = Request("SelItem")
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ContentsType List</title>    	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />	
		
	<script type="text/javascript" >
	    <%=StrJs%>
	</script>
</head>
<body>
    <form id="contentTypeForm" runat="server">    
        <div style="text-align:left">
            <HP3:ctlContentType runat="server" ID="ctlContentType" TypeControl="Selection" />
        </div> 
        <div style="text-align:left; margin-top:15px"> 
            <asp:button ID ="btnSel" runat="server" onclick="SelItem"  CssClass="button" Text="Select Item"/>
        </div>
    
    </form>
</body>
</html>
