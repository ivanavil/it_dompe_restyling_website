<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName="ctlThemes" src="~/hp3Office/HP3Service/ThemesManager.ascx"%>
<script runat="server">
    Public DomainInfo As String
    Dim StrJs As String = ""
    Sub SelItem(ByVal s As Object, ByVal e As EventArgs)
        Dim themecol As Object = ctlThemes.GetSelection
        Dim ov As ThemeValue
        If Not themecol Is Nothing AndAlso themecol.count > 0 Then
            ov = themecol.item(0)
            If Not ov Is Nothing AndAlso Not Request("ctlHidden") Is Nothing AndAlso Not Request("ctlVisible") Is Nothing Then
                StrJs = "<script>window.opener.SetValue('" & Request("ctlHidden") & "','" & Request("ctlVisible") & "'," & ov.Key.Id & ",'" & Server.HtmlDecode(ov.Description).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
            End If
        Else
            StrJs = "<script>alert('Select an item to continue.');</s" + "cript>"
        End If
        
        If StrJs <> "" Then ClientScript.RegisterClientScriptBlock(Me.GetType, "js", StrJs)
    End Sub
    
    Sub Page_Load()
        If Not Request("SelItem") Is Nothing AndAlso Not Page.IsPostBack Then
            ctlThemes.SetDefaultChecked = Request("SelItem")
        End If
    End Sub
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Themes List</title>    	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="../_css/consolle.css" />		
</head>
<body>
    <form id="themesForm" runat="server">    
        <div style="text-align:left">
            <HP3:ctlThemes runat="server" ID="ctlThemes" TypeControl="Selection" />
        </div>
       <div style="text-align:left;margin-top:15px">
            <asp:button ID ="btnSel" runat="server" onclick="SelItem" CssClass="button" Text="Select Item"/>
        </div>

    </form>
</body>
</html>
