<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oEditorialSearcher As ContentEditorialSearcher
    Private editorialManger As New ContentEditorialManager
        
    Private _sortType As ContentEditorialGenericComparer.SortType
    Private utility As New WebUtility
           
    Private Property SortType() As ContentEditorialGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = ContentEditorialGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As ContentEditorialGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
                
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Description & "_" & Request("sites")
        End If
        
        Dim contentsColl As ContentEditorialCollection = ReadContentEditorial()
        
        gridList.DataSource = contentsColl
        gridList.DataBind()
        
        If Not (Page.IsPostBack) Then BindComboType()
    End Sub
    
    Function ReadContentEditorial() As ContentEditorialCollection
        Dim editorialCollection As ContentEditorialCollection
        oEditorialSearcher = New ContentEditorialSearcher
             
        editorialCollection = editorialManger.Read(oEditorialSearcher)
        Return editorialCollection
    End Function
    
   
    ' Legge la descrizione del Content Editorial dato l'id
    Function ReadLabel(ByVal idEditorial As Int32) As ContentEditorialValue
        oEditorialSearcher = New ContentEditorialSearcher
               
        oEditorialSearcher.Id = idEditorial
        Dim editorialValue As ContentEditorialValue = editorialManger.Read(oEditorialSearcher)(0)
        
        Return editorialValue
    End Function
   
    Function setVisibility(ByVal id As Int32) As String
        Dim eCollection = ReadContentEditorial()
        If eCollection Is Nothing OrElse eCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal Sender As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
	function fDisable(id as string) as string
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "        
        End If
        Return ""
    End Function
	
	function fChecked(id as string) as string
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca 
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    'popola la dwl con tutti i tipi di editori
    Sub BindComboType()
        Dim idEditorial As Integer = 0
        Dim editorialTypeColl As ContentEditorialTypeCollection = editorialManger.ReadType(idEditorial)
        
        utility.LoadListControl(dwlEditorialType, editorialTypeColl, "Description", "Id")
        dwlEditorialType.Items.Insert(0, New ListItem("--Select Type--", -1))
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oEditorialSearcher = New ContentEditorialSearcher()
        If txtId.Value <> "" Then oEditorialSearcher.Id = txtId.Value
        If txtDescription.Value <> "" Then oEditorialSearcher.SearchString = txtDescription.Value
        If dwlEditorialType.SelectedItem.Value <> -1 Then oEditorialSearcher.EditorialType = dwlEditorialType.SelectedItem.Value
        
        BindGrid(objGrid, oEditorialSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As ContentEditorialSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New ContentEditorialSearcher
        End If
        
        Dim _oEditorialColl As ContentEditorialCollection = editorialManger.Read(Searcher)
        
        If Not (_oEditorialColl Is Nothing) Then
            _oEditorialColl.Sort(SortType, RoleGenericComparer.SortOrder.ASC)
        End If
        
        objGrid.DataSource = _oEditorialColl
        objGrid.DataBind()
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Name"
                SortType = ContentEditorialGenericComparer.SortType.ByDescription
            Case "Id"
                SortType = ContentEditorialGenericComparer.SortType.ById
        End Select
        
        BindWithSearch(sender)
    End Sub
    
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Editorials List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="editorialForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Editorial Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                            <td><strong>Editorial Type</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDescription" type="text" runat="server"  style="width:250px"/></td>  
                            <td><asp:DropDownList id="dwlEditorialType"  runat="server"></asp:DropDownList></td>
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            OnSorting="gridList_Sorting"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.id)%>  name = 'chk_<%#Container.DataItem.Description.replace("'", "`")%>_<%#Container.DataItem.id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.id)%>  name = 'chk_<%#Container.DataItem.Description.replace("'", "`")%>_<%#Container.DataItem.id%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description" SortExpression="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Type">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "DescriptionType")%></ItemTemplate>
                            </asp:TemplateField> 
                         </Columns>
            </asp:gridview>
        </div> 
         
        <div id="divRepeater" class="title" style="margin-top:10px">    
            <asp:Repeater id="repEditorial" runat="Server">
                <HeaderTemplate></HeaderTemplate>
                <itemtemplate>							
                    <div class="dControl" >
                        <span  <%#fDisable(container.DataItem.id)%> >
                        <%If Not bSingleSel Then%>
                            <input <%#fChecked(container.DataItem.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.Description = "", "[Descrizione Assente]", Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.DescriptionType%>]
                        <%Else%>
                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.id%>' type="radio"/>&nbsp;<%#iif(Container.DataItem.Description="","[Descrizione Assente]",Container.DataItem.Description)%>&nbsp;[<%#Container.DataItem.DescriptionType%>]
                        <%end if%>
                        </span>
                    </div>
                </itemtemplate>
            </asp:Repeater>
        </div>
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>