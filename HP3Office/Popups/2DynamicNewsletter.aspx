<%@ Page Language="VB" debug="true"  validaterequest="false"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Box" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility" %>
<%@ Import NameSpace="System.io" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>
<%@ Register tagPrefix="FCKeditorV2" namespace="FredCK.FCKeditorV2" assembly="FredCK.FCKeditorV2" %>
<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register TagName="ctlContentType" TagPrefix="HP3" Src="~/hp3Office/HP3Parts/ctlContentType.ascx"%>

<script runat="Server">
    <Serializable()> _
    Class ItemChecked
        Public id As Int32
        Public optId As Int32
        Public desc As String
    End Class
    
    Dim idStep As Int16 = 1
    Dim idTml, idNl As String
    Dim idLanguage As Int16
    Dim arr As Hashtable
    Dim strJs As String
    Dim StrIn As String
    Dim strPAth As String
    Dim bAction As Boolean
    Dim strDomain As String
    Dim bookmark As Boolean
    Dim _siteId As String
    
    Private _strDomain As String
    Private _language As String
    Private masterPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    Private oLManager As New LanguageManager
    Private oBoxManger As New BoxManager
    Private webutility As New WebUtility
    Private _isNew As Boolean = True
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
     
    Private Property isNew() As Boolean
        Get
            Return ViewState("isNew")
        End Get
        Set(ByVal value As Boolean)
            ViewState("isNew") = value
        End Set
    End Property
   
       
    ''' <summary>
    ''' Salvataggio dell' HTML nel controllo chiamante
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub SaveFullText(ByVal s As Object, ByVal e As EventArgs)
        Dim strAll As String
        'modificare nome file
        Dim strFile As String = GetFullPathTmpFile(idNl & "_" & Request("id_tm") & ".Txt")
        
        If Not File.Exists(strFile) Then
            strJs = "<script>" & vbCrLf
            strJs += "alert('Error during saving, Impossobile to recover the file.')" & vbCrLf
            strJs += "</s" & "cript>"
            Exit Sub
        End If

        Dim objFile As New FileStream(strFile, FileMode.Open)
        Dim objReader As New StreamReader(objFile)
        strAll = objReader.ReadToEnd()

        objReader.Close()
        objFile.Close()

        'Aggiornamento campi della newsletter
        If InsertFullText(strAll, idNl) Then
            bAction = True
            File.Delete(strFile)
            
            'strJs = "<script>" '& vbCrLf
            'strJs += "alert('Saved successfully');" '& vbCrLf
            'Dim ctl As String = Request("ctlSrc")
            'strJs += "window.opener.SetValue('" & ctl & "','" & "pippo" & "');"
            strJs += "window.opener.location.reload(true);" & vbCrLf
            strJs += "window.close();"
            'strJs += "</s" & "cript>"
        Else
            'strJs = "<script>" & vbCrLf
            strJs += "alert('Error during saving');" & vbCrLf
            'strJs += "</s" & "cript>"
        End If
        
    End Sub
    
    Function InsertFullText(ByVal strAll As String, ByVal idNL As Int32) As Boolean
        Dim oNewsletterManager As New NewsletterManager
        Dim oNewletterValue As NewsletterValue
       
        Try           
            oNewletterValue = oNewsletterManager.Read(New NewsletterIdentificator(Request("idNL"), Request("idLingua")))
            oNewletterValue.FullTextHtml = strAll
            oNewletterValue.RefTemplate = idTml
                                
            Dim myNLValue As NewsletterValue = oNewsletterManager.Update(oNewletterValue)
            Return True
        Catch er As Exception
            Response.Write(er.ToString)
            Return False
        End Try
    End Function
    
    Function GetFullPathTmpFile(ByVal FileName As String) As String
        Return PhisicalTempPath & "\" & FileName
    End Function
    
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
   
        Dim mystringBuilder As New StringBuilder()
        Dim stringWriter As New StringWriter(mystringBuilder)
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        
        MyBase.Render(htmlWriter)
        
        Dim html As String = mystringBuilder.ToString()
        
        If idStep = 3 Then
            'Modificare il nome del file usare funzione di hashing
            'Il path utilizzare una costante della classe configuration
            Dim strFile As String = GetFullPathTmpFile(idNl & "_" & Request("id_tm") & ".Txt")
  
            If Request("id_tm") <> "" And idNl <> "" And Not bAction Then
                If File.Exists(strFile) Then File.Delete(strFile)
            
                Dim objFile As New FileStream(strFile, FileMode.Create)
                Dim objWriter As New StreamWriter(objFile)
                objWriter.Write(EditRender(html))

                objWriter.Close()
                objFile.Close()
            End If
        End If
        
        writer.Write(html)
    End Sub
    
   
    Function EditRender(ByVal strSource As String) As String
        Dim pos As Integer = strSource.IndexOf("<!--Inizio NL-->")
        Dim pos2 As Integer

        If Pos <> -1 Then
            pos += 16
            pos2 = strSource.IndexOf("<!--Fine NL-->", pos)
            If Pos2 <> -1 Then
                strSource = strSource.SubString(pos, pos2 - pos)
            End If
        End If
        
        Return strSource
    End Function
    
    ReadOnly Property isTemplateSelected()
        Get
            Return False
            'If Not ViewState("itemSel") Is Nothing Then
            '    Return True
            'Else
            '    Return False
            'End If
        End Get
    End Property
    
    Function ReadTemplateInfoById(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateNewsletterValue
        Dim oNewsLetterManager As New NewsletterManager
        Return oNewsLetterManager.Read(idTemplate)
    End Function
    
    Function ReadNewsletterbyid(ByVal idNl As NewsletterIdentificator) As NewsletterValue
        Dim oNewsLetterManager As New NewsletterManager
        Dim oNewsLetterSearcher As New NewsletterSearcher
        oNewsLetterSearcher.Key.PrimaryKey = idNl.KeyNewsletter
        Dim oCol As NewsletterCollection = oNewsLetterManager.Read(oNewsLetterSearcher)
        
        If Not oCol Is Nothing Then
            Return oCol.Item(0)
        End If
    End Function
    
    Public ReadOnly Property DomainInfo()
        Get
            Dim SiteManager As New SiteManager
            Dim SiteValue As SiteValue = SiteManager.getDomainInfo()
            Dim arr() As String = SiteValue.Folder.Split("/")
            ReDim Preserve arr(arr.GetUpperBound(0) - 1)
            Dim strDomain = "http://" & SiteValue.Domain & "/" & Join(arr, "/")
            
            Return strDomain
        End Get
    End Property
    
    Function ReadContentIdentificator(ByVal keyNewsletter As NewsletterIdentificator) As NewsletterIdentificator
        Dim cm As New ContentManager
        Dim ci As ContentIdentificator = cm.getContentIdentificator(keyNewsletter.Id, keyNewsletter.IdLanguage)
        keyNewsletter.PrimaryKey = ci.PrimaryKey
        
        Return keyNewsletter
    End Function
    
     
    Sub Page_Load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo()
        
        Dim strTesto As String
        Dim oNewsletterValue As NewsletterValue
        Dim oTemplateValue As TemplateNewsletterValue
        
        strDomain = DomainInfo
        
        If Not Request("Step") Is Nothing Then idStep = Request("Step")
        If Not Request("id_tm") Is Nothing Then idTml = Request("id_tm")
        If Not Request("idNL") Is Nothing Then
           
            'hiddenNLId.Text = Request("idNL")
            idNl = Request("idNL")
        End If
        
        If Not Request("idLingua") Is Nothing Then idLanguage = Request("idLingua")
        
        idNl = ReadContentIdentificator(New NewsletterIdentificator(idNl, idLanguage)).PrimaryKey
        hiddenNLId.Text = idNl
        
        If idTml <> 0 Then
            oTemplateValue = ReadTemplateInfoById(New TemplateNesletterIdentificator(idTml))
            If Not oTemplateValue Is Nothing Then
                strPAth = oTemplateValue.Path
                
            End If
        End If
       
        If Not Page.IsPostBack Then
            openPanel(idStep)
            schCT.LoadControl()
            BindSite()
            
            If Not Request("idNL") Is Nothing OrElse Request("idNL") <> 0 Then
                oNewsletterValue = ReadNewsletterbyid(New NewsletterIdentificator(idNl))
                
                If Not oNewsletterValue Is Nothing Then
                   
                    strTesto = "The newsletter <strong>" & oNewsletterValue.Title & " [" & oNewsletterValue.Key.PrimaryKey & "]</strong> is currently associated to the  <strong>"
                    
                    If oNewsletterValue.RefTemplate <> 0 Then
                        oTemplateValue = ReadTemplateInfoById(New TemplateNesletterIdentificator(Int32.Parse(oNewsletterValue.RefTemplate)))
                        
                        If Not oTemplateValue Is Nothing Then
                           
                            strTesto += oTemplateValue.Description & " [" & oTemplateValue.Key.Id & "]</strong>"
                            
                            Dim oItem = New ItemChecked
                            oItem.id = oTemplateValue.Key.Id
                            oItem.optId = oTemplateValue.Key.Id
                            oItem.desc = oTemplateValue.Path
					                
                            ViewState("itemSel") = oItem
                            
                            lblDesc.Text = strTesto
                            pnlDesc.Visible = True
                        End If
                    End If
                End If
 
            End If
        End If
        
        ActionOnStep()
        SetupButton()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
     
    Sub SelectStep(ByVal id As Int16)
        If id = 0 Then Exit Sub
        
        Dim oItem = New ItemChecked
        oItem.id = Int32.Parse(id)
        oItem.optId = Int32.Parse(id)
        oItem.desc = ""
					       
        ViewState("itemSel") = oItem
    End Sub
    
    Sub SetupButton()
        btnPrec.Attributes.Add("onClick", "return OnClickAttributes()")
        btnPrec.Visible = setVis(idStep, False)
        btnPrec.CommandArgument = idStep - 1
        'btnPrecTop.Attributes.Add("onClick", "return OnClickAttributes()")
        'btnPrecTop.Visible = btnPrec.Visible
        'btnPrecTop.CommandArgument = idStep - 1
        
        btnSucc.Attributes.Add("onClick", "return OnClickAttributes()")
        btnSucc.Visible = setVis(idStep, True)
        btnSucc.CommandArgument = idStep + 1
        'btnSuccTop.Attributes.Add("onClick", "return OnClickAttributes()")
        'btnSuccTop.Visible = btnSucc.Visible
        'btnSuccTop.CommandArgument = idStep + 1
    End Sub
    
    Sub ActionOnStep()
        Select Case idStep
            Case 1
                'litMsg.Text = "Scelta Template"
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep1&Help=cms_HelpCreateNewsletterStep1&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep1", "Step 1 - Choose template") & "</strong>"
                If Not Page.IsPostBack Then
                    SelectStep(idTml)
                    ViewState("ordinamento") = "nl_tm_id"
                    ViewState("OrderType") = "DESC"
                   
                    BindData()
                End If
            Case 2
           
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep2&Help=cms_HelpCreateNewsletterStep2&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep2", "Step 2 - Box association") & "</strong>"
                If Not Page.IsPostBack Then
                    BindDataBox()
                End If
                
            Case 3
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep3&Help=cms_HelpCreateNewsletterStep3&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep3", "Step 3 - Preview") & "</strong>"
                If strPAth <> "" AndAlso TemplateExist(strPAth) Then
                    Dim obj As Object = LoadControl(VirtualTemplatePath & "/" & strPAth)
                    obj.idNl = idNl
                    obj.idLanguage = idLanguage
                    plcAnteprima.Controls.Add(obj)
                    btnUpdateFullTex.Visible = True
                    'btnUpdateFullTexTop.Visible = True
                Else
                    plcAnteprima.Controls.Add(New LiteralControl("<h2>The Template " & strPAth & " not exist into " & ConfigurationsManager.NewsletterTemplatePath & ".<br/>Impossible to continue.</h2>"))
                    bAction = True
                End If
        End Select
    End Sub
    
    Function TemplateExist(ByVal strPath As String) As Boolean        
        Return File.Exists(SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterTemplatePath & "\" & strPath)
    End Function
    
    Function ReadBoxByTemplate(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateBoxCollection
        Dim oNewsletterManager As New NewsletterManager
        oNewsletterManager.Cache = False
        
        Dim idBox As BoxIdentificator
        
        Return oNewsletterManager.ReadBoxTemplate(idTemplate, idBox)
    End Function
    
    Function GetBoxCollection(ByVal src As TemplateBoxCollection) As BoxCollection
        Dim outCol As New BoxCollection
        Dim oBoxVal As BoxValue
        Dim oBoxManager As New BoxManager
        
        If Not src Is Nothing AndAlso src.Count > 0 Then
            For Each oTBV As TemplateBoxValue In src
                oBoxVal = oBoxManager.Read(oTBV.IdBox)
                If Not oBoxVal Is Nothing Then
                    outCol.Add(oBoxVal)
                End If
            Next
        End If
         
        Return outCol
    End Function
    
    Sub BindDataBox()
        Dim oTemplateBox As TemplateBoxCollection = ReadBoxByTemplate(New TemplateNesletterIdentificator(idTml))
        If Not oTemplateBox Is Nothing Then
            rpBox.DataSource = GetBoxCollection(oTemplateBox)
            rpBox.DataBind()
                    
            '    'If rpBox.Items.Count = 0 Then
            '    '    WriteMsg("No box associated to the template" & idTml, "dWarningMsg", lblMsg2)
            'End If
        End If
    End Sub
    
    Sub openPanel(ByVal nStep As Int16)
        If nStep = 0 Then Return
        
        Dim ctl As Control
        ctl = FindControl("pnl_" & nStep)
        If Not ctl Is Nothing Then
            ctl.Visible = True
        End If
    End Sub
    
    Sub WriteMsg(ByVal Testo As String, ByVal Classe As String, Optional ByVal ctl As Panel = Nothing)
        If ctl Is Nothing Then ctl = lblMsg2
        ctl.CssClass = Classe
        
        ctl.Controls.Clear()
        ctl.Controls.Add(New LiteralControl(Testo))
    End Sub
    
    Sub cambiapagina(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        FindCheked()
        dgElenco.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub
    
    Sub cambiapaginaBox(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        'rpBox.CurrentPageIndex = e.NewPageIndex
        'BindDataBox()
    End Sub
    
    Function GetTemplates() As TemplateNesletterCollection
        Dim oNewsletterManager As New NewsletterManager
        oNewsletterManager.Cache = False
        Return oNewsletterManager.Read()
    End Function
    
    Sub BindData()
        Dim TemplateCollection As TemplateNesletterCollection = GetTemplates()
        If TemplateCollection Is Nothing OrElse TemplateCollection.Count = 0 Then
            dgElenco.Visible = False
            
            WriteMsg("No box found", "dWarningMsg", lblMsg2)
        Else
            dgElenco.Visible = True
            
            WriteMsg("", "", lblMsg2)
            
            dgElenco.DataKeyField = "Key"
            dgElenco.DataSource = TemplateCollection
            dgElenco.DataBind()
        End If
    End Sub
    
    Sub ordinamento(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
        FindCheked()
        ViewState("ordinamento") = e.SortExpression
    
        If ViewState("OrderType") Is Nothing OrElse ViewState("OrderType") = "DESC" Then
            ViewState.Add("OrderType", "ASC")
        Else
            ViewState("OrderType") = "DESC"
        End If

        dgElenco.CurrentPageIndex = 0
        BindData()
    End Sub
    
    Sub FindCheked()
        Dim obj As DataGridItem
        Dim ctl, chk As Object
        Dim oItem As ItemChecked
		  		
        For Each obj In dgElenco.Items
            chk = obj.FindControl("chkSel")
            ctl = obj.FindControl("litVal")
            If Not chk Is Nothing AndAlso Not ctl Is Nothing Then
				                
                If CType(chk, CheckBox).Checked Then
                    oItem = New ItemChecked
                    oItem.id = Int32.Parse(ctl.text)
                    oItem.optId = Int32.Parse(ctl.text)
                    oItem.desc = obj.Cells(2).Text
					                
                    ViewState("itemSel") = oItem
                    Exit For
                End If
            End If
        Next
    End Sub
    
    Sub ReloadChk(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If Not ViewState("itemSel") Is Nothing Then
			   
                If ViewState("itemSel").id = dgElenco.DataKeys(e.Item.ItemIndex).Id Then
                    Dim ctl As Object
                    
                    ctl = e.Item.FindControl("chkSel")
                    If Not ctl Is Nothing Then
                        ctl.checked = True
                        'strJs = "<script>" & vbCrLf
                        'strJs += "gestClick(document.getElementById('" & ctl.clientid & "'))" & vbCrLf
                        'strJs += "</s" & "cript>"
                    End If
                End If
            End If
        End If
    End Sub
    
    Function getTotStep() As Int16
        Dim ctl As Control
        Dim curId As Int16 = 0
        Dim Id As Int16 = 0
        
        For Each ctl In Form1.Controls
            If TypeOf (ctl) Is Panel AndAlso ctl.ID.ToString.Substring(0, 3) = "pnl" Then
                Id = ctl.ID.ToString.Substring(4)
                If Id > curId Then curId = Id
                
            End If
        Next
       
        Return Id
    End Function
    
    'True  - Ultimo
    'False - Primo
    Function setVisButton(ByVal nStep As Int16, ByVal buttonType As Boolean) As String
        Dim strRet As String
        
        If (nStep = 1 AndAlso Not buttonType) OrElse (nStep = getTotStep() AndAlso buttonType) Then
            strRet = "style='display:none'"
        End If
        
        Return strRet
    End Function
    
    'True  - Ultimo
    'False - Primo
    Function setVis(ByVal nStep As Int16, ByVal buttonType As Boolean) As Boolean
        If (nStep = 1 AndAlso Not buttonType) OrElse (nStep = getTotStep() AndAlso buttonType) Then
            Return False
        End If
        
        Return True
    End Function
    
    Function EditQs(ByVal srcQs As String, ByVal Key As String, ByVal Valore As String) As String
        Dim strqs As String = srcQs
        Dim sKey As String
        Dim bExist As Boolean
        
        If Key = "" Then Return strqs
        bExist = IIf(Request(Key) Is Nothing, False, True)
        
        If bExist Then
            Dim str As String
            Dim i As Int16
            Dim arr() As String = strqs.Split("&")
            Dim arr2() As String
            
            For i = 0 To arr.Length - 1
                str = arr(i)
                arr2 = str.Split("=")
                If Key <> "" AndAlso arr2(0) = Key Then
                    arr2(1) = Valore
                    
                    arr(i) = Join(arr2, "=")
                    Exit For
                End If
            Next
          
            strqs = Join(arr, "&")
            Return strqs
        Else
            
            Return strqs & "&" & Key & "=" & Valore
        End If
    End Function
    
    Function getUrlWQ() As String
        Dim strUrl As String = Request.Url.ToString
        Dim arr() As String = strUrl.Split("?")
        
        Return arr(0)
    End Function
    
    Sub ActionOnCursor(ByVal s As Object, ByVal e As EventArgs)
        Dim Key, Valore As String
                
        SelectActionOnStep(Key, Valore)
        Response.Redirect(getUrlWQ() & "?" & EditQs(EditQs(Request.QueryString.ToString, "Step", s.commandargument), Key, Valore))
        
    End Sub
    
    Sub SelectActionOnStep(ByRef Key As String, ByRef Valore As String)
        Select Case idStep
            Case 1
                FindCheked()
                Key = "id_tm"
                Valore = getIdSelected()
        End Select
    End Sub
    
    Function getIdSelected() As Int16
        If Not ViewState("itemSel") Is Nothing Then
                
            Return ViewState("itemSel").id
        End If
    End Function
          
    
    'Sub SalvaContent(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim bResult As Boolean = False
               
    '    If txtHidden_CK1.Text <> "" Then
    '        bResult = True
    '        Dim arr() As String = txtHidden_CK1.Text.Split("�")
    '        For Each str As String In arr
    '            If IsNumeric(str) Then
                    
    '                bResult = bResult And EditContent(New BoxIdentificator(CType(sender.CommandArgument(), Integer)), New NewsletterIdentificator(idNl), New ContentIdentificator(Int32.Parse(str)))
    '            End If
    '        Next
    '    End If
        
    '    If bResult Then
    '        Response.Redirect(Request.Url.ToString)
    '    Else
    '        WriteMsg("Error during saving.", "dErrorMsg")
    '    End If
    'End Sub
           
    Function getList_ID(ByVal val As String)
        Dim arr() As String = val.Split("_")
        Return arr(0)
    End Function
    
    Function UpdateBOX_CN(ByVal oBoxNCValue As BoxContentNewsletterValue) As Boolean
        Try
            Dim oBoxManager As New BoxManager
            oBoxNCValue = oBoxManager.Update(oBoxNCValue)
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    Function RemoveItem(ByVal key As BoxContentNewsletterIdentificator) As Boolean
        Dim obox As New BoxManager
        
        Try
            obox.Remove(key)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    
    Sub Move(ByVal key As BoxContentNewsletterIdentificator, ByVal Url As String, ByVal moltiplicatore As Int16)
        Dim oBoxNCValue As BoxContentNewsletterValue
        Dim oBoxNCPrecValue As BoxContentNewsletterValue
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        
        Dim order As Int16
        'Recupero l'elemento selezionato per lo spostamento 
        oBoxNCSearcher.Key.Id = key.Id
        oBoxNCValue = LoadBoxContentNewsletter(oBoxNCSearcher)
        '------------------------------------
        
        'Recupero l'elemento precedente
        oBoxNCSearcher = New BoxContentNewsletterSearcher
        oBoxNCSearcher.KeyBox.Id = oBoxNCValue.KeyBox.Id
        oBoxNCSearcher.KeyNewsletter = oBoxNCValue.KeyNewsletter
        oBoxNCSearcher.Order = oBoxNCValue.Order - moltiplicatore '- 1
        
        oBoxNCPrecValue = LoadBoxContentNewsletter(oBoxNCSearcher)
        '-----------------------------
        If Not oBoxNCValue Is Nothing Then
            order = oBoxNCValue.Order
            
            If order = 1 AndAlso moltiplicatore = 1 Then Return
            
            order = order - moltiplicatore '- 1
            oBoxNCValue.Order = order
            
            If Not oBoxNCPrecValue Is Nothing Then
                If UpdateBOX_CN(oBoxNCValue) Then
                    order = oBoxNCPrecValue.Order
                    order = order + moltiplicatore '+1
                    oBoxNCPrecValue.Order = order
                    
                    If UpdateBOX_CN(oBoxNCPrecValue) Then
                        SummeryReletion()
                    Else
                        WriteMsg("Error during this operation", "dErrorMsg")
                    End If
                Else
                    WriteMsg("Error during this operation", "dErrorMsg")
                End If
            End If
        End If
    End Sub
     
    Sub ReorderIitems(ByVal sender As Object, ByVal e As EventArgs)
        Dim key As BoxContentNewsletterIdentificator
        Dim list As ListBox = Me.Page.FindControl("listRel")
        'Dim list As New ListBox
        If sender.CommandName = "Up" Or sender.CommandName = "Down" Then
            If Not sender Is Nothing Then
                If list.SelectedItem Is Nothing Then
                    strJs = "alert('Select an element to continue.')"
                    Return
                End If
            End If
                
            Dim strUrl As String = Request.Url.ToString
            key = New BoxContentNewsletterIdentificator()
            key.Id = getList_ID(list.SelectedItem.Value)
        
            Select Case sender.CommandName
                Case "Up"
                    Move(key, strUrl, 1)
                                   
                Case "Down"
                    Move(key, strUrl, -1)
            End Select
        End If
    End Sub
    
    Sub RemoveFile(ByVal id As BoxContentNewsletterIdentificator)
        Dim oFolder As New IO.DirectoryInfo(PhisicalPath)
        Dim oFile As FileInfo
        
        For Each oFile In oFolder.GetFiles(id.Id & ".*")
            On Error Resume Next
            oFile.Delete()
        Next
    End Sub
     
    ReadOnly Property VirtualTemplatePath() As String
        Get
            Return "/" & ConfigurationsManager.NewsletterTemplatePath
        End Get
    End Property
    
    ReadOnly Property PhisicalPath() As String
        Get
            Return SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterImagePath
        End Get
    End Property
    
    ReadOnly Property VirtualPath() As String
        Get
            Return "\" & ConfigurationsManager.NewsletterImagePath
        End Get
    End Property
    
    ReadOnly Property PhisicalTempPath() As String
        Get
            Dim Path As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterTempPath
            If Not IO.Directory.Exists(Path) Then
                IO.Directory.CreateDirectory(Path)
            End If
            
            Return Path
        End Get
    End Property
    
    Sub ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        Dim ctl As Control = e.Item.FindControl("btnCancella")
        If Not ctl Is Nothing Then
            CType(ctl, Button).Attributes.Add("onclick", "return confirm('Are you sure you want to erase the item?')")
        End If
        
        Dim id As String
        
        ctl = e.Item.FindControl("litVal")
        If Not ctl Is Nothing Then
            id = CType(ctl, Literal).Text
        End If
        
        ctl = e.Item.FindControl("rdTypeItem")
        If Not ctl Is Nothing Then
            CType(ctl, RadioButtonList).Attributes.Add("onclick", "ChangeSel(this," & id & ")")
            CType(ctl, RadioButtonList).SelectedIndex = CType(ctl, RadioButtonList).Items.IndexOf(CType(ctl, RadioButtonList).Items.FindByValue(0))
        End If
        
        ctl = e.Item.FindControl("listRel")
        If Not ctl Is Nothing Then
            CType(ctl, ListBox).Attributes.Add("onDblClick", "gestEdit(this," & id & ")")
        End If
        
        Dim contentBox As ListBox = e.Item.FindControl("listRel")
        If Not contentBox Is Nothing Then
            Dim boxIdentificator As New BoxIdentificator
            boxIdentificator.Id = id
            LoadContentBox(contentBox, boxIdentificator, New NewsletterIdentificator(idNl))
        End If
    End Sub
    
    Function BindTypeItem()
        Dim oH As New Hashtable
        
        oH.Add(0, "Content")
        oH.Add(1, "Banner")
        oH.Add(2, "Testo")
        
        Return oH
    End Function
    
    Sub LoadContentBox(ByVal oList As ListControl, ByVal keyBox As BoxIdentificator, ByVal keyNewsletter As NewsletterIdentificator)
        Dim oBoxManager As New BoxManager
        oBoxManager.Cache = False
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        Dim Key As String
        Dim Valore As String
        
        Try
            oBoxNCSearcher.KeyBox = keyBox
            oBoxNCSearcher.KeyNewsletter = keyNewsletter
            
            Dim oBoxCNCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
            If Not oBoxCNCollection Is Nothing Then
                For Each oBoxNCValue As BoxContentNewsletterValue In oBoxCNCollection
                    Valore = "" : Key = ""
                    Key = oBoxNCValue.Key.Id & "_" & oBoxNCValue.BoxContentType
                                       
                    Select Case oBoxNCValue.BoxContentType
                        Case BoxContentNewsletterValue.ContentBoxType.Text
                            If oBoxNCValue.FreeText.Length >= 7 Then
                                Valore = "[Testo] " & oBoxNCValue.FreeText.Substring(0, 7) & "..."
                            Else
                                Valore = "[Testo] " & oBoxNCValue.FreeText
                            End If
                        Case BoxContentNewsletterValue.ContentBoxType.Banner
                            Valore = "[Immagine] " & oBoxNCValue.Key.Id & ".*"
                        Case BoxContentNewsletterValue.ContentBoxType.Content
                            Dim oUtility As New GenericUtility
                            Dim cv As ContentValue = oUtility.GetContentValue(oBoxNCValue.KeyContent.PrimaryKey)
                            'Dim cv As ContentValue = oUtility.GetContentValue( oBoxNCValue.KeyContent.PrimaryKey, idLanguage)
                            If Not cv Is Nothing Then
                                Valore = cv.Title
                            End If
                    End Select
                    oList.Items.Add(New ListItem(Valore, Key))
                Next
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub

    Function LoadBoxContentNewsletter(ByVal oBoxNCSearcher As BoxContentNewsletterSearcher) As BoxContentNewsletterValue
        Dim oBoxManager As New BoxManager
        oBoxManager.Cache = False
        Dim oBoxNCCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
        If Not oBoxNCCollection Is Nothing AndAlso oBoxNCCollection.Count > 0 Then
            Return oBoxNCCollection.Item(0)
        End If
    End Function
    
    Function SetBookmark(ByVal Path As String) As String
        bookmark = TemplateExist(Path)
    End Function
           
    'Function EditContent(ByVal keyBox As BoxIdentificator, ByVal keyNewletter As NewsletterIdentificator, ByVal KeyContent As ContentIdentificator) As Boolean
    '    Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
    '    Dim oBoxNcCollection As BoxContentNewsletterCollection
    '    Dim oBoxNCValue As BoxContentNewsletterValue
        
    '    Dim order As Int32
    '    Try
    '        'Recupero dell' order
    '        oBoxNCSearcher.KeyBox = keyBox
    '        oBoxNCSearcher.KeyNewsletter = keyNewletter
    '        oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
            
    '        If Not oBoxNcCollection Is Nothing Then
    '            order = oBoxNcCollection.Count + 1
    '        Else
    '            order += 1
    '        End If
    '        '---------------------------
            
    '        oBoxNCValue = New BoxContentNewsletterValue
    '        oBoxNCValue.KeyBox = keyBox
    '        oBoxNCValue.KeyNewsletter = keyNewletter
    '        oBoxNCValue.Order = order
    '        oBoxNCValue.KeyContent = KeyContent
    '        oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Content
    '        'oBoxManger.Create(oBoxNCValue)
                      
    '        Return True
    '    Catch ex As Exception
    '        Response.Write(ex.ToString)
    '        Return False
    '    End Try
    'End Function
    
    Sub AddContent(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddContent") Then
            Dim arguments As String() = sender.CommandArgument().Split("|")
           
            isNew() = True
            hiddenBoxId.Text = arguments(0)
           
            Dim boxName As String = arguments(1)
            lblBoxName.Text = boxName
            BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Content)
                    
        End If
        
        rpBox.Visible = False
        RelSummary.Visible = True
        pnlContent.Visible = True
        BoxSelected.Visible = True
        pnlsummeryrelation.Visible = False
    End Sub
    
    Sub AddFreeText(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddFreeText") Then
            Dim arguments As String() = sender.CommandArgument().Split("|")
           
            isNew() = True
            hiddenBoxId.Text = arguments(0)
            Dim boxName As String = arguments(1)
           
            lblBoxName.Text = boxName
            BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Text)
        End If
        
        rpBox.Visible = False
        RelSummary.Visible = True
        pnlFreeText.Visible = True
        BoxSelected.Visible = True
        pnlsummeryrelation.Visible = False
    End Sub
           
    Sub BindBoxRelation(ByVal type As Integer)
        Dim BoxManager As New BoxManager
        Dim boxCNSearcher As New BoxContentNewsletterSearcher
        
        BoxManager.Cache = False
        boxCNSearcher.BoxContentType = type
        boxCNSearcher.KeyBox.Id = hiddenBoxId.Text
        boxCNSearcher.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
        Dim boxCNColl As BoxContentNewsletterCollection = BoxManager.Read(boxCNSearcher)
        
        gdw_boxRelation.DataSource() = boxCNColl
        gdw_boxRelation.DataBind()
    End Sub
    
    Sub AddBanner(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "AddBanner") Then
            Dim arguments As String() = sender.CommandArgument().Split("|")
            isNew() = True
            hiddenBoxId.Text = arguments(0)
          
            Dim boxName As String = arguments(1)
            lblBoxName.Text = boxName
            BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Banner)
        End If
        
        rpBox.Visible = False
        RelSummary.Visible = True
        BoxSelected.Visible = True
        pnlBanner.Visible = True
        pnlsummeryrelation.Visible = False
    End Sub
     
    Sub SummeryReletion()
        Dim BoxManager As New BoxManager
        Dim boxCNSearcher As New BoxContentNewsletterSearcher
        
        BoxManager.Cache = False
        boxCNSearcher.KeyBox.Id = hiddenBoxId.Text
        boxCNSearcher.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
        Dim boxCNColl As BoxContentNewsletterCollection = BoxManager.Read(boxCNSearcher)
       
        listRel.Items.Clear()
        If Not (boxCNColl Is Nothing) AndAlso (boxCNColl.Count > 0) Then
            For Each elem As BoxContentNewsletterValue In boxCNColl
                Dim text As String = ""
                Select Case elem.BoxContentType
                    Case BoxContentNewsletterValue.ContentBoxType.Banner
                        text = "[Banner]" & getPropertyValue(elem)
                    Case BoxContentNewsletterValue.ContentBoxType.Content
                        text = "[Content]" & getPropertyValue(elem)
                    Case BoxContentNewsletterValue.ContentBoxType.Text
                        text = "[Text]" & getPropertyValue(elem)
                End Select
                    
                Dim listItem As ListItem = New ListItem(text, elem.Key.Id)
                listRel.Items.Add(listItem)
            Next
        End If
        
        pnlsummeryrelation.Visible = True
    End Sub
    
    Sub SummeryReletion(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName = "SummeryReletion") Then
            hiddenBoxId.Text = sender.CommandArgument()
           
            Dim BoxManager As New BoxManager
            Dim boxCNSearcher As New BoxContentNewsletterSearcher
        
            BoxManager.Cache = False
            boxCNSearcher.KeyBox.Id = hiddenBoxId.Text
            boxCNSearcher.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
            Dim boxCNColl As BoxContentNewsletterCollection = BoxManager.Read(boxCNSearcher)
           
            listRel.Items.Clear()
            If Not (boxCNColl Is Nothing) AndAlso (boxCNColl.Count > 0) Then
                For Each elem As BoxContentNewsletterValue In boxCNColl
                    Dim text As String = ""
                    Select Case elem.BoxContentType
                        Case BoxContentNewsletterValue.ContentBoxType.Banner
                            text = "[Banner]" & getPropertyValue(elem)
                        Case BoxContentNewsletterValue.ContentBoxType.Content
                            text = "[Content]" & getPropertyValue(elem)
                        Case BoxContentNewsletterValue.ContentBoxType.Text
                            text = "[Text]" & getPropertyValue(elem)
                    End Select
                    
                    Dim listItem As ListItem = New ListItem(text, elem.Key.Id)
                    listRel.Items.Add(listItem)
                Next
            End If
        End If
        
        pnlsummeryrelation.Visible = True
    End Sub
    
    Sub DeleteRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName() = "DeleteRelation") Then
            Dim arguments As String() = sender.CommandArgument().Split("|")
            Dim idRel As Integer = arguments(0)
            Dim typeRel As Integer = arguments(1)
                        
            oBoxManger.Remove(New BoxContentNewsletterIdentificator(idRel))
            
            Select Case typeRel
                Case BoxContentNewsletterValue.ContentBoxType.Text
                    pnlFreeText.Visible = False
                    BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Text)
                    
                Case BoxContentNewsletterValue.ContentBoxType.Banner
                    pnlBanner.Visible = False
                    BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Banner)
                    
                Case BoxContentNewsletterValue.ContentBoxType.Content
                    pnlContent.Visible = True
                    BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Content)
            End Select
        End If
    End Sub
    
    Sub EditRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        If (sender.CommandName() = "EditRelation") Then
            Dim arguments As String() = sender.CommandArgument().Split("|")
            
            Dim idRel As Integer = arguments(0)
            Dim typeRel As Integer = arguments(1)
            
            isNew() = False
            
            Dim boxCNSearcher As New BoxContentNewsletterSearcher
            boxCNSearcher.Key.Id = idRel
            oBoxManger.Cache = False
            Dim boxCNColl As BoxContentNewsletterCollection = oBoxManger.Read(boxCNSearcher)
            
            If Not (boxCNColl Is Nothing) AndAlso (boxCNColl.Count > 0) Then
                Dim boxCNValue As BoxContentNewsletterValue = boxCNColl(0)
                hiddenBoxId.Text = boxCNValue.KeyBox.Id
                hiddenNLId.Text = boxCNValue.KeyNewsletter.Id
                hiddenKeyId.Text = idRel
                
                Select Case typeRel
                    Case BoxContentNewsletterValue.ContentBoxType.Text
                        pnlFreeText.Visible = True
                        txtFreeText.Value = boxCNValue.FreeText
                        txtLink.Text = boxCNValue.Link
                       
                    Case BoxContentNewsletterValue.ContentBoxType.Banner
                        txtbannerdesc.Text = boxCNValue.Description
                        txtbannerlink.Text = boxCNValue.Link
                        pnlBanner.Visible = True
                        'Dim Path As String = PhisicalPath
                        'Dim objDir As New DirectoryInfo(Path)
                        
                    Case BoxContentNewsletterValue.ContentBoxType.Content
                        txtcontLink.Text = boxCNValue.Link
                        dwlSite.SelectedIndex = dwlSite.Items.IndexOf(dwlSite.Items.FindByValue(boxCNValue.KeySiteArea.Id))
                        
                        RelSummary.Visible = False
                        pnlEditCt.Visible = True
                        pnlContent.Visible = False
                End Select
            End If
        End If
    End Sub
    
    Sub SaveFtRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim oBoxNCValue As New BoxContentNewsletterValue
               
        oBoxNCValue.KeyBox.Id = hiddenBoxId.Text
        oBoxNCValue.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
        oBoxNCValue.FreeText = txtFreeText.Value
        oBoxNCValue.Link = txtLink.Text
        oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Text
               
        If (isNew()) Then
            oBoxNCValue = oBoxManger.Create(oBoxNCValue)
        Else
            oBoxNCValue.Key.Id = CType(hiddenKeyId.Text, Integer)
            oBoxNCValue = oBoxManger.Update(oBoxNCValue)
            pnlFreeText.Visible = False
        End If
                
        BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Text)
        ClearForm()
    End Sub
    
    Sub SaveBanner(ByVal sender As Object, ByVal e As EventArgs)
        Dim Path As String = PhisicalPath
        Dim outID As Int32
              
        If (inputFile.PostedFile.ContentLength > 0) Then
            Dim strFileName As String = inputFile.PostedFile.FileName
            strFileName = strFileName.Substring(strFileName.LastIndexOf("\") + 1)
            strFileName = strFileName.Substring(strFileName.LastIndexOf(".") + 1)
            Select Case strFileName.ToUpper
                Case "JPG", "GIF", "SWF"
                    'Controllo della dimensione
                    If inputFile.PostedFile.ContentLength > ConfigurationsManager.MaxUploadFileSize Then
                        strJs = "alert('The file selected exceed the concurred maximum dimension of 1Mb')"
                    Else
                        Dim objDirInfo As New DirectoryInfo(Path)
                        outID = SaveBrRelation()
                        If (outID > 0) Then
                            DelFile(Path, outID)
                            inputFile.PostedFile.SaveAs(Path & outID & "." & strFileName)
                            strJs = "alert('File saved successfully.')"
                        Else
                            strJs = "alert('Error during file saving')"
                        End If
                    End If
                    
                Case Else
                    strJs = "alert('The file selected must have extension GIF, jpg or swf');"
                    '    ElseIf EditImageBox(New BoxIdentificator(Request("Box")), New NewsletterIdentificator(Request("idNL")), txtbannerlink.Text, txtbannerdesc.Text, outID, IIf(Request("idItem") Is Nothing, Nothing, New BoxContentNewsletterIdentificator(Request("idItem")))) Then
                    '        strJs = "alert('The file have been updated correctly');window.opener.location.reload();window.close()"
                    '    End If
            End Select

        Else
            strJs = "alert('It is necessary to select an image with extension gif/jpg or a file with extension swf');"
        End If
    End Sub
    
    Sub DelFile(ByVal strPathName As String, ByVal strFileName As String)
        
        Try
            Dim objDirInfo As New DirectoryInfo(strPathName)
            Dim objFiles() As FileInfo = objDirInfo.GetFiles(strFileName & ".*")
            Dim objFile As FileInfo
            
            For Each objFile In objFiles
                objFile.Delete()
            Next
        Catch ex As Exception
        End Try
    End Sub
    
    Function SaveBrRelation() As Integer
        Dim oBoxNCValue As New BoxContentNewsletterValue
               
        oBoxNCValue.KeyBox.Id = hiddenBoxId.Text
        oBoxNCValue.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
        'oBoxNCValue.Order = order
        oBoxNCValue.Description = txtbannerdesc.Text
        oBoxNCValue.Link = txtbannerlink.Text
        
        oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Banner
        
       
        If (isNew()) Then
            oBoxNCValue = oBoxManger.Create(oBoxNCValue)
        Else
            oBoxNCValue.Key.Id = CType(hiddenKeyId.Text, Integer)
            oBoxNCValue = oBoxManger.Update(oBoxNCValue)
            pnlBanner.Visible = False
        End If
                
        BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Banner)
        ClearForm()
        
        If Not (oBoxNCValue Is Nothing) Then Return oBoxNCValue.Key.Id
        
        Return 0
    End Function
    
    Sub SaveCtRelation(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim contentPK As Integer = sender.CommandArgument()
        Dim oBoxNCValue As New BoxContentNewsletterValue
               
        oBoxNCValue.KeyBox.Id = hiddenBoxId.Text
        oBoxNCValue.KeyNewsletter = New NewsletterIdentificator(CType(hiddenNLId.Text, Integer))
        oBoxNCValue.KeyContent.PrimaryKey = contentPK
        oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Content
        
        If (isNew()) Then
            oBoxNCValue = oBoxManger.Create(oBoxNCValue)
        End If
                
        BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Content)
        pnlContent.Visible = True
    End Sub
    
    Sub updateCTRelation(ByVal sender As Object, ByVal e As EventArgs)
        Dim boxCNValue As New BoxContentNewsletterValue
        
        Dim boxCNIdent As New BoxContentNewsletterIdentificator()
        boxCNIdent.Id = hiddenKeyId.Text
        oBoxManger.Update(boxCNIdent, "Link", txtcontLink.Text)
        oBoxManger.Update(boxCNIdent, "KeySiteArea.Id", dwlSite.SelectedItem.Value)
        
        BindBoxRelation(BoxContentNewsletterValue.ContentBoxType.Content)
        pnlEditCt.Visible = False
        RelSummary.Visible = True
        pnlContent.Visible = True
    End Sub
    
    Function getPropertyValue(ByVal boxCNValue As BoxContentNewsletterValue) As String
        Select Case boxCNValue.BoxContentType
            Case BoxContentNewsletterValue.ContentBoxType.Content
                
                Return getContentTitle(boxCNValue.KeyContent.PrimaryKey)
            Case BoxContentNewsletterValue.ContentBoxType.Text
                Dim freeText As String = ""
                
                If Not (boxCNValue.FreeText Is Nothing) And (boxCNValue.FreeText.Length > 20) Then
                    freeText = boxCNValue.FreeText.Substring(0, 20) & "..."
                    Return freeText
                Else
                    Return boxCNValue.FreeText
                End If
            Case BoxContentNewsletterValue.ContentBoxType.Banner
                Return GetFileName(boxCNValue.Key.Id) 'GetBanner(190, txtbannerlink.Text)visualizza il banner come un'immagine
        End Select
        
        Return ""
    End Function
    
    Function GetFileName(ByVal id As String) As String
        Dim Path As String = PhisicalPath
        Dim objDir As New DirectoryInfo(Path)
        Dim objFile() As FileInfo
        
        objFile = objDir.GetFiles(id & ".*")
        If objFile.Length = 0 Then
            Return ""
        Else
            Return objFile(0).Name
        End If
    End Function
    
    Function GetBanner(ByVal idNl As Int32, ByVal link As String) As String
        Dim strOut As String = ""
        Dim Path As String = PhisicalPath
        Dim Path2 As String = VirtualPath
        
        Dim objDir As New DirectoryInfo(Path)
        Dim objFile() As FileInfo
        Dim FileExt As String
        Dim nWidth, nHeight As Integer
        nWidth = 100
        nHeight = 100
       
        ''Cerco l' esistenza del file
        objFile = objDir.GetFiles(idNl & ".*")
        If objFile.Length = 0 Then Return ""
        ''Imposto il percorso
        Path += objFile(0).Name
        Path2 += objFile(0).Name
        
        FileExt = objFile(0).Extension.ToString.ToUpper.Substring(1)


        Select Case FileExt
            Case "SWF"
                strOut = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' width='[width]' height='[height]'>" & _
                  "<param name=movie value='[Path]?url_banner=[URL]'>" & _
                  "<param name=quality value=best>" & _
                  "<embed src='[Path]?url_banner=[URL]' quality=best pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' width='[width]' height='[height]'></embed>" & _
                 "</object>"

                strOut = Replace(Replace(Replace(Replace(strOut, "[width]", nWidth), "[height]", nHeight), "[Path]", Path2), "[URL]", link)

            Case "JPG", "GIF"
                strOut = "<a href= '[URL]' target='_blank'> <img src = '[Path]' alt = 'Anteprima' width = '[WIDTH]' height = '[HEIGHT]'></a>"
                strOut = Replace(Replace(Replace(Replace(strOut, "[Path]", Path2), "[WIDTH]", nWidth), "[HEIGHT]", nHeight), "[URL]", link)

        End Select
	
        objFile = Nothing

        Return strOut
    End Function
    
    Function getContentTitle(ByVal PK As Integer) As String
        Dim contentManager As New ContentManager
        
        Dim contentValue As ContentValue = contentManager.Read(New ContentIdentificator(PK))
        If Not (contentValue Is Nothing) Then
            Return contentValue.Title
        End If
        
        Return ""
    End Function
  
    Function getTypeRel(ByVal type As Integer) As String
        Select Case type
            Case BoxContentNewsletterValue.ContentBoxType.Content
                Return "[Content]"
            Case BoxContentNewsletterValue.ContentBoxType.Text
                Return "[Text]"
            Case BoxContentNewsletterValue.ContentBoxType.Banner
                Return "[Banner]"
        End Select
       
        Return "[]"
    End Function
    
      
    Sub SearchContents(ByVal sender As Object, ByVal e As EventArgs)
        BindWithSearch(gridContents)
    End Sub
    
    Sub BindWithSearch(ByVal obj As GridView)
        Dim contentManager As New ContentManager
        Dim contentSearcher As New ContentSearcher
        
        If (schId.Text <> String.Empty) Then contentSearcher.key.Id = schId.Text
        If (schTitle.Text <> String.Empty) Then contentSearcher.SearchString = schTitle.Text
        Dim ctColl As ContentTypeCollection = schCT.GetSelection()
        
        'se viene scelto il contentType dalla dwnList
        If Not ctColl Is Nothing AndAlso ctColl.Count > 0 Then
            contentSearcher.KeyContentType.Id = ctColl.Item(0).Key.Id
        End If
        
        Dim contentColl As ContentCollection = contentManager.Read(contentSearcher)
        obj.DataSource = contentColl
        obj.DataBind()
    End Sub
    
    Sub IndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        BindWithSearch(sender)
    End Sub
    
    Sub BindSite()
        Dim siteManager As New SiteAreaManager
        Dim siteSearcher As New SiteAreaSearcher
            
        siteSearcher.IdType = 1
        Dim siteColl As SiteAreaCollection = siteManager.Read(siteSearcher)
           
        webutility.LoadListControl(dwlSite, siteColl, "Label", "Key.Id")
        dwlSite.Items.Insert(0, New ListItem("--Select Site--", -1))
    End Sub
    
    Sub ClearForm(ByVal sender As Object, ByVal e As EventArgs)
        ClearForm()
    End Sub
    
    Sub ClearForm()
        txtFreeText.Value = ""
        txtLink.Text = ""
    End Sub
    
    Sub BacktoList(ByVal sender As Object, ByVal e As EventArgs)
        BoxSelected.Visible = False
        RelSummary.Visible = False
        pnlEditCt.Visible = False
        pnlFreeText.Visible = False
        pnlBanner.Visible = False
        pnlContent.Visible = False
        rpBox.Visible = True
    End Sub
    
    Sub GoBack(ByVal sender As Object, ByVal e As EventArgs)
        pnlEditCt.Visible = False
        RelSummary.Visible = True
        pnlContent.Visible = True
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Guided creation of the newsletter body</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link href="../_css/popup.grp.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
    
    <script type="text/javascript">
    var msg
            
    function checkRadioCheck()
        {
            var arr = document.getElementsByTagName('input') 
            var oId
            for (i=0;i<arr.length;i++)
                {
                    oId = arr[i].id
                    if (oId.indexOf('chkSel') != -1)
                        if (arr[i].checked) return true;
                }
                
                return false;
        }
        
    function gestClick(obj)
	    {
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.id))
	            {
	                ochk = document.getElementById (oText.value)
	                ochk.checked = false;
	            }
	            
	        oText.value = obj.id
	    }	   
	    
	    function checkClick()
	        {
	            switch (<%=idStep%>)
	                {
	                    case 1:	                    
    	                    if ((<%=iif(isTemplateSelected,1,0)%>==1)||checkRadioCheck())
    	                        return true;
    	                    else
    	                        msg='No template selected';
    	                        return false;
	                        break;    	                    	                    
	                }
	            return true;
	        }
	    
	    function OnClickAttributes()
	        {
	            if (! checkClick())	   	                     	                
                   { 
                    alert(msg);
                    return false
                    }
	        }

        function setVis_All(Valore,key)
            {
                var arr = document.getElementsByTagName ("span")
                for(i=0;i<arr.length;i++)
                    {
                        if (arr[i].id.indexOf('spn_' + key) != -1 )
                            {
                                arr[i].style.display=Valore
                            }
                    }
            }    
        
        function setVis_Single(id,key,Valore)
            {
                var oDiv =document.getElementById ('spn_' + key + '_' + id)
                if (oDiv != null)
                    {
                        oDiv.style.display=Valore
                    }
            }                        
        function ChangeSel(obj,id)
            {
                setVis_All('none',id);
                setVis_Single(getRadioItemSel(obj),id,'')   
            }	        
            
        function getRadioItemSel(obj)
            {
         
                var Rad = obj.getElementsByTagName('input')
                
                 for(i=0;i<Rad.length;i++)
                    {
                        if (Rad[i].checked)
                            {
                                return Rad[i].value
                            }                            
                    }
            }
                
        function getTextVal(strText)
        {
            var oTxt = document.getElementById (strText)
            return oTxt.value
        }    
        
        function SaveContentSelection(strTxtDest2)
            {
                var arr = strTxtDest2.split('_')
	            var id = arr[arr.length -1];
	            
	            var oDiv = document.getElementById ('divAcc_' + id + '_0');
                if (oDiv != null)
                    {	                            
                        var arr = oDiv.getElementsByTagName ('input')
                        for (i=0;i<arr.length;i++)
                            {	                                    
                                if (arr[i].id.indexOf('submit') != -1)
                                    {	                                            
                                        arr[i].click();
                                        return                                       
                                    }
                            }
                    }	           
            }
            
        function addRow(ContentsKey,strValId,strValDesc,strTxtDest2,strTxtDest1,strTxtDest3)
	    {	        	   
	        var arr = strTxtDest2.split('_')
	        var id = arr[arr.length -1];
	      if (!ExistsId(strValId,'txtHidden_CK1'))
	        {	              	        
                document.getElementById ('<%=txtHidden_Desc1.clientid%>').value+= strValId + '�'	                          
                document.getElementById ('<%=txtHidden_CK1.clientid%>').value += ContentsKey + '�' 
                document.getElementById ('<%=txtContents1.clientid%>').value += strValDesc + '�'
            }
       }
	 </script> 
</head>
<body>



<form id="Form1" runat="server">
	<asp:textbox id="txtHidden" runat="server" width="100%" style="display:none"/>
	<asp:textbox  runat="server" id='txtHidden_Desc1' style="display:none"/>        
    <asp:textbox runat="server" id='txtHidden_CK1' style="display:none"/>
    <asp:textbox  runat="server" id='txtContents1' style="display:none"/>

	<asp:Panel ID="lblMsg2" runat="server" ForeColor ="red" />
	   <div style="text-align:left; margin-top:10px; margin-left:2px">
    	   <asp:literal runat="server" ID="litMsg" />
       </div> 	    
	    <%--<div style="text-align:center">
	        <asp:button cssclass="button" runat="server" text="<< Step Precedente" onClick="ActionOnCursor" id="btnPrecTop" />
            <asp:button cssclass="button" runat="server" text="Step Successivo >>" onClick="ActionOnCursor" id="btnSuccTop" />
            <asp:button cssclass="button" runat="server" text="Conferma" onClick="SaveFulltext" id="btnUpdateFullTexTop" visible="false" />
	    </div><br/>--%>
	    
	    <asp:Panel id="pnl_1" runat="server" visible="false">
	        <asp:Panel  ID="pnlDesc" visible="false" runat="server">
				<asp:Label ID="lblDesc" runat="server"/>
		     </asp:Panel>
		    
             <fieldset class="fsStdGroup">
                 <div class="subbox">                
                    <asp:datagrid ID="dgElenco" 
                        runat ="server"                                                                         
                        AutoGenerateColumns="False" 
                        OnPageIndexChanged="cambiapagina"                         
                        PagerStyle-Mode="NumericPages"	                    
                        HorizontalAlign="Center" 
	                    OnSortCommand="ordinamento"
                    	onItemDataBound="ReloadChk"	                   
	                    HeaderStyle-CssClass="header"
                        CssClass="tbl1"> 
                        <Columns>       
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <%#SetBookmark(Container.DataItem.path)%>                                   
                                </ItemTemplate>
                                </asp:TemplateColumn>     
                                <asp:TemplateColumn>
                                    <ItemTemplate >
                                        <asp:radiobutton runat="server" name="radio" id="chkSel" onClick="gestClick(this)"/>
					                    <asp:literal id="litVal" runat="Server" text='<%#container.dataitem.Key.Id%>' visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>   
                                <asp:TemplateColumn>
                                    <ItemTemplate >                                        
                                        <asp:literal ID="Literal1" Text ="<%#container.dataitem.Key.Id%>" runat="server" />                                       
                                    </ItemTemplate>
                                    </asp:TemplateColumn>                                                                                                                                                  
                                <asp:BoundColumn DataField="Path" HeaderText="Path" SortExpression="Path"></asp:BoundColumn>                                
                                <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description"></asp:BoundColumn>
                                <asp:TemplateColumn> 
                                    <ItemTemplate>                                                                                         
                                        <img id="Img1" runat="server" visible ="<%#not bookmark%>" src="../HP3Image/Ico/content_delete.gif" alt="Template not available"/>                                            
                                        <%--<a runat="server" visible ="<%#bookmark%>" onclick="window.open('<%=strDomain%>/Popups/anteprima_newsletter.aspx?idNL=<%#idNl%>&idTemplate=<%#container.dataitem.Key.Id%>','','width=800,height=600,scrollbars=yes,status=no')" href='#'>                        --%>
                                        <img id="Img2" runat="server" visible ="<%#bookmark%>"  src="../HP3Image/Ico/ico_16x_preview.gif" alt="Template available"/>                                           
                                        <%--</a>--%> 
                                   </ItemTemplate> 
                                </asp:TemplateColumn>
                            </Columns>
                    </asp:datagrid>
                </div>
            </fieldset>
        </asp:Panel>
        
        <!--Insieme dei box-->
	    <asp:Panel id="pnl_2" runat="server" visible="false">	        	        
	       <fieldset class="fsStdGroup">
                <div class="subbox" >           
                    <asp:gridview id="rpBox" 
                        runat="server"
                        AutoGenerateColumns="false" 
                        GridLines="None" 
                        CssClass="tbl1"
                        HeaderStyle-CssClass="header"
                        AlternatingRowStyle-BackColor="#E9EEF4"
                        PagerSettings-Position="TopAndBottom"  
                        AllowSorting="true"
                        emptydatatext="No item available">
                      <Columns >
                        <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Box">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Name")%></ItemTemplate>
                        </asp:TemplateField> 
                       <asp:TemplateField HeaderStyle-Width="10%">
                            <ItemTemplate>
                             <asp:ImageButton ID="lnkSummery" runat="server" ToolTip ="Summery" ImageUrl="~/hp3Office/HP3Image/ico/content_edit.gif"  OnClick="SummeryReletion" CommandName ="SummeryReletion"  CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdBox")%>'/>
                             <asp:ImageButton ID="lnkAssContent" runat="server" ToolTip ="Add Content" ImageUrl="~/hp3Office/HP3Image/ico/content_add.gif"  OnClick="AddContent" CommandName ="AddContent"  CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdBox") &"|"& DataBinder.Eval(Container.DataItem, "Name") %>'/>
                             <asp:ImageButton ID="lnkAssFreeText" runat="server" ToolTip ="Add FreeText" ImageUrl='~/hp3Office/HP3Image/ico/text_add.gif'  OnClick="AddFreeText" CommandName ="AddFreeText" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdBox") &"|"& DataBinder.Eval(Container.DataItem, "Name") %>' />
                             <asp:ImageButton ID="lnkAssBanner" runat="server" ToolTip ="Add Banner" ImageUrl="~/hp3Office/HP3Image/ico/banner_add.gif"   OnClick = "AddBanner" CommandName ="AddBanner" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdBox") &"|"& DataBinder.Eval(Container.DataItem, "Name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                      </Columns>
                    </asp:gridview>
                 </div>                    
            </fieldset>  
          
        <%--Tutte le Associazioni con il box --%>
        <fieldset class="fsStdGroup">  
        <div class="subbox" id="pnlsummeryrelation" runat="server" visible="false">
            <hr />
            <div style="text-align:left"><strong>Relations</strong></div>
            <table cellpadding ="0" cellspacing ="0" width="100%" style="border:0px; background-color:White" >
               <tr style="border:0px; background-color:White">
                    <td style="border:0px; background-color:White;width:90%"><asp:Listbox width ="100%" id="listRel" runat="Server"/></td>
                    <td style="border:0px; background-color:White" valign ="center">                                        
                        <asp:button ID="Buttup"  OnClick="ReorderIitems" commandname ="Up" runat="server" style="width:25px" text="&uarr;" cssclass="button" tooltip="Moves higher the selected item"/>
                        <br />
                        <asp:button ID="Butdown"   OnClick="ReorderIitems" commandname ="Down" runat="server" style="width:25px" text="&darr;" cssclass="button" tooltip="Moves lower the selected item" />
                    </td>                                
                </tr>     
             </table>      
         </div>
         </fieldset> 
            
        <%--Box selezionato--%> 
        <fieldset class="fsStdGroup">
         <div class="subbox" id="BoxSelected" runat="server" visible="false"> 
            <div style="text-align:left; margin-bottom:10px "><asp:Button id="btn_BacktoList" text="New Relation" CssClass="button" runat="server" OnClick="BacktoList" /></div>    
            <div style="text-align:left"><strong>Box Selected</strong></div>
            <%--<HP3:Text runat ="server" id="hiddenBoxSelected" TypeControl ="TextBox"  Visible="false" />--%>
            <table class="tbl1">
                <tr>
                    <th style="width:80%">Name</th>
                    <%--<th style="width:10%">&nbsp;</th>--%>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblBoxName" runat="server" />
                    </td>
                    <%--<td>
                         <asp:ImageButton ID="_lnkAssContent" runat="server" ToolTip ="Add Content" ImageUrl="~/hp3Office/HP3Image/ico/content_delete.gif"  OnClick="AddContent" CommandName ="AddContent"  CommandArgument='<%=hiddenBoxId.Text  & "|" & lblBoxName.Text%>'/>
                        <asp:ImageButton ID="_lnkAssFreeText" runat="server" ToolTip ="Add FreeText" ImageUrl='~/hp3Office/HP3Image/ico/content_delete.gif'  OnClick="AddFreeText" CommandName ="AddFreeText"  CommandArgument='<%=hiddenBoxId.Text %>'/>
                         <asp:ImageButton ID="_lnkAssBanner" runat="server" ToolTip ="Add Banner" ImageUrl="~/hp3Office/HP3Image/ico/Forum.gif"   OnClick = "AddBanner" CommandName ="AddBanner" CommandArgument='<%=hiddenBoxId.Text  & "|" & lblBoxName.Text%>' />
                    </td>--%>
                </tr>
            </table>
           <hr />
        </div>                    
       </fieldset>  
       
         <%--Associazioni Box/Testo--%> 
        <fieldset class="fsStdGroup">  
        <div class="subbox" id="RelSummary" runat="server" visible="false">
            <div style="text-align:left"><strong>Relations active</strong></div>
                  <asp:gridview id="gdw_boxRelation" 
                        runat="server"
                        AutoGenerateColumns="false" 
                        GridLines="None" 
                        CssClass="tbl1"
                        HeaderStyle-CssClass="header"
                        AlternatingRowStyle-BackColor="#E9EEF4"
                        AllowSorting="true"
                        emptydatatext="No item available">
                      <Columns>
                        <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Type">
                                <ItemTemplate><%#getTypeRel(DataBinder.Eval(Container.DataItem, "BoxContentType"))%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="40%">
                                <ItemTemplate><%#getPropertyValue(Container.DataItem)%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Link">
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Link")%></ItemTemplate>
                        </asp:TemplateField> 
                       <asp:TemplateField HeaderStyle-Width="7%">
                            <ItemTemplate>
                              <asp:ImageButton ID="lnkEditRela" runat="server" ToolTip ="Edit Relation" ImageUrl="~/hp3Office/HP3Image/ico/content_edit.gif"  OnClick="EditRelation" CommandName ="EditRelation"  CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Key.Id")&"|"& DataBinder.Eval(Container.DataItem, "BoxContentType")  %>'/>
                              <asp:ImageButton ID="lnkDeleteRela" runat="server" ToolTip ="Delete Relation" ImageUrl='~/hp3Office/HP3Image/ico/content_delete.gif'  OnClick="DeleteRelation"  OnClientClick="return confirm('Confirm Delete?')" CommandName ="DeleteRelation" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Key.Id")&"|"& DataBinder.Eval(Container.DataItem, "BoxContentType") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                      </Columns>
                    </asp:gridview>
                    <hr />
                </div>
          </fieldset>  
        
        <div id="pnlEditCt" visible="false" runat="server">
             <%--<div class="title" style="margin-bottom:20px">Edit Content</div>--%>
             <table class="form" width="99%">
                 <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormNewsletterText&Help=cms_HelpNewsletterText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterLink", "Link")%></td>
                    <td><HP3:Text runat ="server" id="txtcontLink" TypeControl ="TextBox"  Style="width:190px"/></td>
                 </tr>
                 <tr>
                    <td><%--<a href="#"  onclick="window.open('<%=Domain()%>/Popups/popHelp.aspx?Label=cms_FormRelationType&Help=cms_HelpRelationType&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormRelationType", "Site")%>--%></td>
                    <td><asp:DropDownList id="dwlSite" runat="server"/></td>
                 </tr>
             </table>
             <asp:Button id ="btn_SaveRelation" runat="server" Text="Update" OnClick="UpdateCtRelation" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
             <asp:Button id ="btn_Goback" runat="server" Text="Go Back" OnClick="GoBack" CssClass="button" style="margin-top:10px;margin-bottom:5px"/>
             <hr />
         </div>
        
         <!--Gestione Free Text-->
         <div runat="server" id="pnlFreeText" visible ="false" > 
         <div style="text-align:left"><strong>Insert text:</strong></div>           
            <table class="form">
                <HP3:Text runat ="server" id="hiddenKeyId" TypeControl ="TextBox"  Visible="false" />
                <HP3:Text runat ="server" id="hiddenBoxId" TypeControl ="TextBox"  Visible="false" />
                <HP3:Text runat ="server" id="hiddenNLId" TypeControl ="TextBox"  Visible="false" />
                <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormNewsletterText&Help=cms_HelpNewsletterText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterText", "Text")%></td>
                    <td><FCKeditorV2:FCKeditor id="txtFreeText" Height="300px" width="580px" BasePath="/hp3office/js/FCKeditor/" runat="server" /></td>
                </tr>
                <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormNewsletterText&Help=cms_HelpNewsletterText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterLink", "Link")%></td>
                    <td><HP3:Text runat ="server" id="txtLink" TypeControl ="TextBox" style="width:300px" MaxLength="200" /></td>
                </tr>
            </table>
            
            <div style="margin-top:10px">
                <asp:button ID="btnSalvaBanner" runat="server" text="Save" cssclass="button" tooltip="Save"  OnClick="SaveFtRelation" />
           </div>
        </div>
        
      <!--Gestione Banner-->
        <div runat="server" id="pnlBanner" visible ="false" > 
        <div style="text-align:left"><strong>Insert banner:</strong></div>           
           <table class="form">
                 <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBannerDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
                    <td><asp:Textbox id ="txtbannerdesc" runat="Server" width="340px"/></td>
                </tr>
                <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpBannerLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
                    <td><asp:Textbox id ="txtbannerlink" runat="Server" width="340px"/></td>
                </tr>
                <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormUploadBanner&Help=cms_HelpUploadBanner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUploadBanner", "Upload Banner")%> <asp:label ID="lblFileName" runat ="server" /></td>
                    <td><input id="inputFile"  type="file" size="37" runat="server"  style="width:350px"/></td>
                </tr>
            </table>
            <div style="margin-top:10px">
                <asp:button id ="btnSalvaImg" runat="server" text="Save" cssclass="button" tooltip="Save" onClick="SaveBanner" />
            </div>
         </div>
	  	  
	   <!--Gestione Content-->
        <div runat="server" id="pnlContent" visible ="false"> 
        <div style="text-align:left"><strong>Insert News :</strong></div>
            <div class="boxSearcher">
                <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Content Searcher</span></td>
                    </tr>
                </table>
                <table class="form">
                    <tr>
                        <td>Id</td>
                        <td>Title</td>
                        <td>Content Type</td>
                    </tr>
                    <tr>
                        <td><HP3:Text ID="schId" runat="server" TypeControl="NumericText" Style="width:50px"/></td>
                        <td><HP3:Text ID="schTitle" runat="server" typecontrol="TextBox"  Style="width:300px"/></td>
                        <td><HP3:ctlContentType ID="schCT" TypeControl="combo" OnlyAssociatedContents="true" ItemZeroMessage="Select ContentType" runat="server" /></td>
                    </tr>
                </table>
                <div  style="text-align:left; margin-top:5px; margin-left:2px">
                   <asp:Button ID="btnSearchRelated" Enabled="true" CssClass="button"  OnClick="SearchContents" Text="Search" runat="server" style="padding-left:16px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;" />
                </div>
            <hr />
            </div>
                          
            <asp:GridView ID="gridContents" 
                AutoGenerateColumns="false"
                AllowPaging="true" 
                PagerSettings-Position="TopAndBottom" 
                PagerStyle-HorizontalAlign="Right" 
                PagerStyle-CssClass="Pager" 
                PageSize="20"
                AllowSorting="false"
                ShowHeader="true" 
                HeaderStyle-CssClass="header"
                ShowFooter="false" 
                FooterStyle-CssClass="gridFooter"
                GridLines="None" 
                AlternatingRowStyle-BackColor="#eeefef" 
                OnPageIndexChanging="IndexChanging"
                width="100%"
                runat="server">
                <Columns>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                       <ItemTemplate>
                            <img alt="Type: <%#Container.DataItem.IdContentSubType%>" src="/HP3Office/HP3Image/Ico/type_<%#Container.DataItem.IdContentSubType%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Key.Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=" " HeaderStyle-Width="2%" ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" src="/hp3Office/HP3Image/Language/ico_flag_<%#oLManager.GetCodeLanguage(DataBinder.Eval(Container.DataItem, "Key.IdLanguage"))%>_small.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="Title">
                        <ItemTemplate>
                            <asp:Literal ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-Width="2%">
                        <ItemTemplate>
                             <asp:ImageButton ID="btn_add" ToolTip="Add relation" runat="server" ImageUrl="~/HP3Office/HP3Image/Ico/insert.gif" onClick="SaveCtRelation" CommandArgument ='<%#Container.Dataitem.Key.PrimaryKey%>' CommandName ="Add" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
        </asp:GridView>
        </div>
        </asp:Panel> 
        
	    <asp:Panel id="pnl_3" runat="server" visible="false">
	        <!--Inizio NL-->
	              <asp:placeholder ID="plcAnteprima" runat ="server" />
	        <!--Fine NL-->
	    </asp:Panel>
	    
	    <div style ="text-align:right;margin-top:15px;margin-right:5px">
	        <asp:button cssclass="button" runat="server" text="<< Previous Step" onClick="ActionOnCursor" id="btnPrec" />
            <asp:button cssclass="button" runat="server" text="Next Step >>" onClick="ActionOnCursor" id="btnSucc" />
            <asp:button cssclass="button" runat="server" text="Save" onClick="SaveFulltext" id="btnUpdateFullTex" visible="false" />
        </div>
    </form>	    
</body >
</html> 

 <script type ="text/javascript" >
   <%=strJS%>
</script>