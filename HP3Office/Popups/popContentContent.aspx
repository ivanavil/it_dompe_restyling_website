<%@ Page Language="VB" debug="true" ValidateRequest ="false"%>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import NameSpace="Healthware.HP3"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>

<%@ Register TagPrefix="HP3" TagName="ContentSearch" src="~/HP3Office/HP3Common/SearchEngine.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public DomainInfo As String
    Public accService As New AccessService()
    Public idCurrentLang As Integer = accService.GetTicket.Travel.KeyLang.Id
    Public webRequest As WebRequestValue = New WebRequestValue()
    Public ThemesSite As String
    Public idConsolleContentType As String
    Public idConsolleSiteArea As String = "0"
    Public idThemes As String
    
    Private _isnew As String
    Private objQs As New ObjQueryString
    Private oGenericUtility As New GenericUtility
    Private StrJs As String
    
    Dim strField1, strField2, strField3, strText As String
    
    Public ReadOnly Property SiteFolder()
        Get
            Return webRequest.folder
        End Get
    End Property
      
    Sub ReadQS()
        ThemesSite = objQs.Site_Id
        If ThemesSite = "" Then ThemesSite = 0 '-1
        
        idThemes = objQs.Theme_id
        If idThemes = "" Then idThemes = "0"
        
        idConsolleContentType = objQs.ContentType_Id
        If idConsolleContentType = "" Then idConsolleContentType = "0"
        
        _isnew = objQs.IsNew
        
        strField1 = Request("txtDestField1")
        strField2 = Request("txtDestField2")
        strField3 = Request("txtDestField3")
        
        If Not Request("txtHidden") Is Nothing AndAlso Request("txtHidden") <> "" Then
            strText = Request("txtHidden")         
        End If
    End Sub

    Protected Sub btnSel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim _Collection As New ContentCollection
        Dim _Value As New ContentValue
        _Collection = ctlSearch.GridContentRowsSelected()
        If Not _Collection Is Nothing Then
            For Each _Value In _Collection                
                StrJs += "window.opener.addRow('" & _Value.Key.PrimaryKey & "','" & _Value.Key.Id & "','" & _Value.Title.Replace(vbCrLf, "").replace("'","\'") & "','" & strField2.replace("'","\'") & "','" & strField1 & "','" & strField3 & "','" & _Value.Title.Replace(vbCrLf, "").replace("'","\'") & "');" & vbCrLf                
            Next
            'StrJs += "window.opener.SaveContentSelection('" & strField2.replace("'","\'") & "')" & vbCrLf
            StrJs += "this.close()"
        Else
            StrJs = "alert('Select row please!')"
        End If       
    End Sub
    
    Sub Page_Load()
        ReadQS()
    End Sub

    Public ReadOnly Property TextHidden() As TextBox
        Get
            Return txtHidden
        End Get
    End Property
</script>

<script type="text/javascript" >
    function GestClick(obj) {
        var inputs = document.getElementsByTagName('input');
        for (var i=0; i<inputs.length; i++) {
            var input = inputs.item(i);
            if (input != obj && input.type == 'radio' && input.value == 'optRelation') {
                input.checked = false;
            }
        }
    }
    
    <%=strJs%>
</script>


<script language="javascript">
function select_check(){
alert('check');
return false;
}



</script>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Content searcher</title>
	    
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	    
	    <link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	    <link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
	    
	    <script type="text/javascript" language="javascript" src="../Js/calendar.js"></script>
	    <script type="text/javascript" language="javascript" src="../Js/calendar-setup.js"></script>
	    <script type="text/javascript" language="javascript" src="../Js/lang/calendar-en.js"></script>
	    <script type="text/javascript" >
	        <%=StrJs%>
	    </script>
    </head>
    
    <body>
        <form id="form1" runat="server">
            <asp:textbox id="txtHidden" runat="server" style="display:none"/>
            <div style="text-align:left; margin: 10px;">
                <HP3:ContentSearch runat="server" ID="ctlSearch" AllowSelect="True" UseInPopup="true" AllowEdit="false" />
                <hr />
                <asp:button cssclass ="button" ID ="btnSel" runat="server" Text="Select Item" OnClick="btnSel_Click" />
                <!--<input type="button" text="Select item" onclick="select_check()"/>-->
            </div>
        </form>
    </body>
</html>
