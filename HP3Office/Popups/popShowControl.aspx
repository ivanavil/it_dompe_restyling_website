<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Forms.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Forms"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Private idthemes As Int32
    Private strJS As String
    Private oGenericUtility As New GenericUtility
    Private outJsString As String
    
    Private KeyUser As UserIdentificator
    Private KeyTheme As ThemeIdentificator
    Private ListKeys As New Hashtable
    
    Sub Page_Load()
        Dim accessService As New AccessService()
        Dim idUser As Int32 = accessService.GetTicket.User.Key.Id
        
        KeyUser = New UserIdentificator(idUser)
        
        If Not Request("idService") Is Nothing Then
            idthemes = Request("idService")
            KeyTheme = New ThemeIdentificator(idthemes)
        End If
        
        If Not Page.IsPostBack Then
            LoadControlRelated()
        End If
    End Sub
    
    ''' <summary>
    ''' Leggo i controlli associati al serivizio        
    ''' </summary>
    ''' <remarks></remarks>
    Sub LoadControlRelated()
        Dim item As ListItem        

        'Recupero tutti i controlli associati al servizio
        Dim oControlsCollection As ControllCollection = oGenericUtility.ReadControls(New UserIdentificator(0), KeyTheme)
       
        If Not oControlsCollection Is Nothing Then
            For Each oControlValue As ControlValue In oControlsCollection                
                item = New ListItem
                item.Text = IIf(oControlValue.Description = "", oControlValue.Name, oControlValue.Description)
                item.Value = oControlValue.Key.Id
                    
                item.Selected = oGenericUtility.GetControlVisibility(KeyUser, KeyTheme, oControlValue.Name)
                
                ListKeys.Add(oControlValue.Key.Id.ToString, oControlValue.Name)
                chkControl.Items.Add(item)
            Next
        End If
        
        ViewState("Keys") = ListKeys
    End Sub
    
    Function ReadKey(ByVal idControl As Int32) As String
        Dim hsList As Hashtable = ViewState("Keys")
        If Not hsList Is Nothing AndAlso hsList.ContainsKey(idControl.ToString) Then
            Return hsList(idControl.ToString)
        End If
    End Function
    
    Sub SaveSelection(ByVal s As Object, ByVal e As EventArgs)
        'rimuvo tutti i controlli associati all'utente e al servizio corrente
        Dim oFormManager As New FormManager
        Dim bRet As Boolean = oFormManager.RemoveAllControl(KeyTheme, KeyUser)
        
        If bRet Then
            Dim oUserControlThemeValue As UserControlThemeValue
            'aggiungo i controlli relativi alla selezione
            For Each item As ListItem In chkControl.Items
                outJsString += ReadKey(item.Value) & "|" & item.Selected & "#"
                If item.Selected Then
                    oUserControlThemeValue = New UserControlThemeValue
                    oUserControlThemeValue.KeyUser = KeyUser
                    oUserControlThemeValue.KeyTheme = KeyTheme
                    oUserControlThemeValue.KeyControl = New ControlIdentificator(item.Value)
                    
                    bRet = oFormManager.CreateUserThemeControl(oUserControlThemeValue)
                End If
            Next
        End If
        
        If bRet Then
            If outJsString <> "" Then outJsString = outJsString.Substring(0, outJsString.Length - 1)
            
            CacheManager.RemoveAllCache()
            strJS = "window.opener.ReadControlVisibility('" & outJsString & "');this.close()"
        End If
    End Sub
    
    Function ReadServiceName() As String
        If idthemes <> 0 Then
            Dim oThemeValue As ThemeValue = oGenericUtility.GetThemeValue(idthemes)
            If Not oThemeValue Is Nothing Then
                Return oThemeValue.Description
            End If
        End If
    End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Show Controls</title>
    <link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
    	<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
    <script type ="text/javascript" >
        <%=strJS%>
    </script>
</head>
<body style="text-align:left">
    <form id="form1" runat="server">
        <div style ="font-weight:bold;padding:5px;border-bottom:1px solid #000;margin-bottom:3px">
            The list indicates the control list which can be visualised in the 
            <%=ReadServiceName()%> service.<br/>
            Select control to visualise it, deselect control to hide it. <br/>
        </div>
    <div>
        <asp:checkboxlist id="chkControl" runat="server" RepeatColumns ="2"  cssclass="form"/>
        <br />
        <asp:button cssclass="button" ID="btnSave" Text="Save" onclick="SaveSelection" runat="server"/>
    </div>
 </form>
    
    
</body>
</html>
