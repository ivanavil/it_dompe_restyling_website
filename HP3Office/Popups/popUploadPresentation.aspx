<%@ Page Language="VB" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Aspose.Slides" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="System.Drawing.Imaging" %>

<script runat="server">
    'Private sourcepath As String = SystemUtility.CurrentPhysicalApplicationPath & "\" & ConfigurationsManager.DownloadPath & "\" & "200442.ppt"
    'Private finalpath As String = ConfigurationsManager.DownloadPath & "\" & "PPT"
      
    Private imagePath As String = ""
    Private pk As String = ""
   
    Sub Page_Load()
        If Request("pkContent") <> "" Then
            pk = Request("pkContent")
            imagePath = ConfigurationsManager.DownloadPath & "/PPT/" & pk
        End If
    End Sub
    
    'creazione e update della presentazione per un certo corso
    Sub UploadPresentation(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not targetFile.HasFile Or targetFile.PostedFile.ContentLength < 0 Then
            Err.Text = "<div class='litErroressage'>The file could be empty .Please, upload a file!</div>"
            Exit Sub
        End If
        
        If targetFile.FileName.IndexOf(".ppt") <> -1 Then
            If (imagePath <> "") Then
                'crea una nuova cartella per la presentazione
                           
                If (rbSlide.Checked) Then
                    CreateFolder(False)
                    CreatePresentation(targetFile.FileName)
                    ConvertToSlidesShow(targetFile.FileName)
                    Err.Text = "<div class='litErroressage'>Upload completed.</div>"
                End If
                
                If (rbPdf.Checked) Then
                    CreateFolder(True)
                    CreatePresentation(targetFile.FileName)
                    ConvertToPdf(targetFile.FileName)
                    Err.Text = "<div class='litErroressage'>Upload completed.</div>"
                End If
            End If
        Else
            Err.Text = "<div class='litErroressage'>Invalid file (" & targetFile.FileName & ") check for extension and try again (*.ppt only!).</div>"
        End If
    End Sub
    
    Sub CreateFolder(ByVal isPdf As Boolean)
        Try
            If Not (Directory.Exists(SystemUtility.CurrentMapPath("/" & imagePath))) Then
                Directory.CreateDirectory(SystemUtility.CurrentMapPath("/" & imagePath))
            Else
                If Not (isPdf) Then
                    Dim files As String() = Directory.GetFiles(SystemUtility.CurrentMapPath("/" & imagePath))
                
                    For Each elem As String In files
                        If elem.IndexOf(".jpg") <> -1 Then
                            My.Computer.FileSystem.DeleteFile(elem)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
    Sub CreatePresentation(ByVal filename As String)
        Try
            Dim file As String = pk & ".ppt"
            'save the file to the server
            targetFile.PostedFile.SaveAs(SystemUtility.CurrentMapPath("/" & imagePath & "/" & file))
           
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
    'it converts and saves the presentation to .pdf file
    Sub ConvertToSlidesShow(ByVal filename As String)
        Dim smallImage As System.Drawing.Image = Nothing
        Dim bigImage As System.Drawing.Image = Nothing
        Dim slide As Slide = Nothing
        Dim file As String = pk & ".ppt"
        
        Dim sourcepath As String = SystemUtility.CurrentMapPath("/" & imagePath & "/" & file)
        Try
            Dim presentation As New Presentation(sourcepath)
            Dim oSlides As Slides = presentation.Slides
                       
            For i As Integer = 1 To oSlides.Count - 1
                slide = presentation.GetSlideByPosition(i)
                bigImage = slide.GetThumbnail(New Drawing.Size(500, 400))
                smallImage = slide.GetThumbnail(New Drawing.Size(200, 150))
                                               
                bigImage.Save(SystemUtility.CurrentMapPath("/" & imagePath & "/" & i & "_b.jpg"), ImageFormat.Jpeg)
                smallImage.Save(SystemUtility.CurrentMapPath("/" & imagePath & "/" & i & ".jpg"), ImageFormat.Jpeg)
            Next
                      
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
    'it converts and saves the presentation to .pdf file
    Sub ConvertToPdf(ByVal filename As String)
        Dim file As String = pk
        Dim sourcepath As String = SystemUtility.CurrentMapPath("/" & imagePath & "/" & file & ".ppt")
        
        Try
            Dim presentation As New Presentation(sourcepath)
            Dim path As String = SystemUtility.CurrentMapPath("/" & imagePath & "/" & file & ".pdf")
            presentation.SaveToPdf(path)
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
	        <title>Upload Presentation</title>
	        <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        </head>
        
        <body>
            <form id="userForm" runat="server">
               <div class="innerMenu">&nbsp;<b>Upload Presentation</b></div>
               <div style="width:250px;padding-left:145px;">
                   <asp:Label runat="server" id="Err" />
                   <asp:FileUpload runat="server" id="targetFile"/><small>*.ppt</small><br /><br />
                   
                   <div>Convert to:</div>
                   <asp:RadioButton runat="server" id="rbSlide" GroupName="convertChk" Text="Slides show" TextAlign="Right" Checked="true" /><br />
                   <asp:RadioButton runat="server" id="rbPdf" GroupName="convertChk" Text="*.pdf" TextAlign="Right" /><br /><br />
                   <asp:Button runat="server" ID="btn_go"  Text="Upload" OnClick="UploadPresentation"  CssClass="button"/><br />
               </div>
                 
               <div  style="text-align:center;margin-top:50px"> 
                <asp:Button runat="server" Text="Close" CssClass="button" OnClientClick="window.close();false"  ID="btnClose"/>
               </div>
            </form>
        </body>
</html>