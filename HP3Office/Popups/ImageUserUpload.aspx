<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.User" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public idUser As Integer
    Public imageFile As String
    
   
    Sub page_init()
        If Request("idUser") <> "" Then
            idUser = Integer.Parse(Request("idUser"))
        End If
       
        imageFile = GetCover(idUser)
    End Sub
    
    Sub Page_Load()
        If Not Page.IsPostBack Then
            If Not imageFile Is Nothing Then
                btnAdd.Text = "Add Image"
               
                Dim objImg As System.Drawing.Bitmap = System.Drawing.Image.FromFile(Server.MapPath("/HP3Image/user/" & imageFile))
                Dim ratio As Int32
                
                If objImg.Width > 230 Then
                    ratio = (objImg.Width / objImg.Height)
                    tmb.Width = 230
                    tmb.Height = (230 / ratio)
                End If
               
                tmb.ImageUrl = "/HP3Image/user/" & imageFile
                If Request.QueryString.ToString.Split("&").Length > 2 Then tmb.ImageUrl = "/HP3Image/user/" & imageFile & "?" & Date.Now
                tmbInfo.Text = "File name: <b>" & imageFile & "</b> - Original size: W = <b>" & objImg.Width & "px</b> H = <b>" & objImg.Height & "px</b> - "
                                
                tmbInfo.Text = tmbInfo.Text & "<b>Generic Cover</b>"
                           
                btnDel.Visible = True
                objImg.Dispose()
            Else
                btnDel.Visible = False
                tmb.ImageUrl = "/HP3Office/HP3Image/ico/NoCover.gif"
                tmb.Width = 128
                tmb.Height = 128
                tmbInfo.Text = "No cover image available for this user."
            End If
        End If
    End Sub
    
    Function GetCover(ByVal idUser As Integer) As String
        Dim oUManager As New UserManager
       
        Return oUManager.GetCover(New UserIdentificator(idUser), 0)
    End Function
    
    Sub UploadCover(ByVal sender As Object, ByVal e As System.EventArgs)
        If TargetFile.PostedFile.ContentLength > 0 Then
            'delFiles()
            If TargetFile.FileName.IndexOf(".gif") <> -1 Or TargetFile.FileName.IndexOf(".jpg") <> -1 Then
                Dim f_name As String = ""
                
                f_name = idUser.ToString & "_gen." & TargetFile.FileName.Split(".")(1)
                
                TargetFile.SaveAs(Server.MapPath("/HP3Image/user/" & f_name))
            
                selectFile.Visible = False
              
                Response.Redirect("/HP3office/popups/ImageUserUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
            Else
                Err.Text = "<div class='litErroressage'>Invalid file (" & TargetFile.FileName & ") check for extension and try again (gif and jpg image only!).</div>"
            End If
        End If
    End Sub
    
    Sub selFile(ByVal sender As Object, ByVal e As System.EventArgs) '
        sender.visible = False
        mainPnl.Visible = False
        selectFile.Visible = True
        btnDel.Visible = False
        btnBack.Visible = True
    End Sub
    
    Sub delFile(ByVal sender As Object, ByVal e As System.EventArgs)
        delFiles()
        Response.Redirect("/HP3office/popups/ImageUserUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
    End Sub
    
    Sub delFiles()
        Try
            If Not imageFile Is Nothing Then
                
                If imageFile.Length() > 8 Then
                    Dim files As String() = System.IO.Directory.GetFiles(Server.MapPath("/HP3Image/user/"), imageFile.Split(".")(0) & "*")
                    Dim file As String
                    Dim fso = New FileIO.FileSystem()
                    
                    For Each file In files
                        fso.DeleteFile(file)
                    Next
                End If
            End If
                    
        Catch ex As Exception
        End Try
    End Sub
    
    Sub back(ByVal sender As Object, ByVal e As System.EventArgs)
        btnAdd.Visible = True
        mainPnl.Visible = True
        selectFile.Visible = False
        btnDel.Visible = True
        btnBack.Visible = False
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
 <title>HP3 User Image Managment</title>
</head>

<link type="text/css" rel="stylesheet" href="/HP3Office/_css/consolle.css" />
<body>
    <form id="formIMG" runat="server">
        <div>
            <div class="innerMenu">&nbsp;<b>User Image</b></div>
            <table width="100%" class="formIMG" >
                <asp:PlaceHolder runat="server" ID="mainPnl">
                    <tr>
                        <td align="center"><asp:Label runat="server" ID="tmbInfo"/></td>
                    </tr>
                    <tr align="center">
                        <td style="vertical-align:middle; text-align:center" height="205">
                            <asp:Image runat="server" ID="tmb" BorderColor="black" BorderWidth="1px" BorderStyle="Solid"  />
                        </td>
                    </tr>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder runat="server" ID="selectFile" Visible="false">
                    <tr height="280" align="center">
                        <td>
                            <div style="width:250px;padding-left:145px;">
                                <asp:Label runat="server" ID="Err" />
                                File :
                                    <asp:FileUpload runat="server" ID="TargetFile"/>
                                <small>*.gif or *.jpg only</small>
                                <br /><br />
                                    <asp:Button runat="server" ID="btn_go"  Text="Upload" OnClick="UploadCover"  CssClass="button"/>
                                <br />
                            </div>
                        </td>
                    </tr>
                </asp:PlaceHolder>   
                
                <tr align="center"><td colspan="2" align="center">
                <div align="center">
                    <asp:Button runat="server" Text="New Image" CssClass="button" OnClick="selFile" ID="btnAdd"/>
                    <asp:Button runat="server" Text="Delete Image" CssClass="button" OnClick="delFile"  OnClientClick="if (!confirm('Delete this cover?')) return false;"  ID="btnDel"/>
                    <asp:Button runat="server" Text="Back" CssClass="button" onClick="back"  ID="btnBack" Visible="false"/>
                    <asp:Button runat="server" Text="Close" CssClass="button" OnClientClick="window.close();false"  ID="btnClose"/>
                </div>
                </td></tr>
            </table>
        </div>
    </form>
</body>
</html>


