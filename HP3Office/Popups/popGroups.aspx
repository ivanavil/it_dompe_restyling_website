<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oGroupSearcher As GroupSearcher
               
    Sub Page_Load()
       
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).GroupName & "_" & Request("sites")
        End If
        
        Dim groupsColl As GroupSiteCollection = ReadGroup()
        
        gridList.DataSource = groupsColl
        gridList.DataBind()
              
    End Sub
    
    Function ReadGroup() As GroupSiteCollection
        Dim oGroupSearcher As New GroupSearcher
             
        Dim gCollection As GroupSiteCollection = Me.BusinessSiteAreaManager.ReadGroup(oGroupSearcher)
        Return gCollection
    End Function

    ' Legge la descrizione del gruppo dato l'id
    Function ReadLabel(ByVal idGroup As Int32) As GroupValue
        Dim groupValue As GroupValue
        
        groupValue = Me.BusinessSiteAreaManager.ReadGroup(New GroupIdentificator(idGroup))
        Return groupValue
    End Function

    Function setVisibility(ByVal id As Int32) As String
        Dim gCollection = ReadGroup()
        
        If gCollection Is Nothing OrElse gCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal Sender As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oGroupSearcher = New GroupSearcher()
        If txtId.Value <> "" Then oGroupSearcher.Key.Id = txtId.Value
        If txtDescription.Value <> "" Then oGroupSearcher.GroupDecsription = txtDescription.Value
        If txtName.Value <> "" Then oGroupSearcher.GroupName = txtName.Value
        
        BindGrid(objGrid, oGroupSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As GroupSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New GroupSearcher
        End If
        
        Dim _oGroupColl As GroupSiteCollection = Me.BusinessSiteAreaManager.ReadGroup(Searcher)
              
        objGrid.DataSource = _oGroupColl
        objGrid.DataBind()
       
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
      
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Groups List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="groupsForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Group Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>  
                            <td><strong>Name</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtName" type="text" runat="server"  style="width:150px"/></td>  
                            <td><input id="txtDescription" type="text" runat="server"  style="width:300px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            OnPageIndexChanging="gridList_PageIndexChanging"
                           
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.GroupDescription.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.GroupDescription.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "GroupName")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "GroupDescription")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
                         <EmptyDataTemplate>
                              No item available.
                         </EmptyDataTemplate>
            </asp:gridview>
        </div> 
              
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>