<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" ValidateRequest="false"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>


<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>
<%@ Register tagprefix="HP3" TagName ="contentLanguage" Src ="~/hp3Office/HP3Parts/LanguageSelector.ascx"%>

<script runat="server">
    Private labelHelp As String
    Private Language As String
    Private labelName As String
    Private _isEdit As Boolean
    
    Private Property SelectedId()
        Get
            Return ViewState("_selectId")
        End Get
        Set(ByVal value)
            ViewState("_selectId") = value
        End Set
    End Property
    
    Private Property IsEdit() As String
        Get
            Return _isEdit
        End Get
        Set(ByVal value As String)
            _isEdit = value
        End Set
    End Property
    
    Sub Page_Load()
        Me.BusinessDictionaryManager.Cache = False
        If Me.BusinessMasterPageManager.GetTicket.Roles.HasRole("CMS", "ADMIN") Then edit_btn.Visible = True
        If Not (Request("Label") Is Nothing) And (Not Request("Language") Is Nothing) Then
            labelName = Request("Label")
            Language = Request("Language")
            
            lbName.Text = Me.BusinessDictionaryManager.Read(labelName, CType(Language, Integer))
        End If
        
        If (Not Request("Help") Is Nothing) And (Not Request("Language") Is Nothing) Then
            labelHelp = Request("Help")
            Language = Request("Language")
            
            ReadDictionary(labelHelp, Language)
        End If
    End Sub
    
    'legge la definizione dell'help dal dictionary
    Sub ReadDictionary(ByVal label As String, ByVal language As String)
        Dim description As String = Me.BusinessDictionaryManager.Read(label, CType(language, Integer))
        lbDescription.Text = description
    End Sub
    
    Function getLabelHelp() As String
        Return labelHelp
    End Function
    
    Sub EditDictionary(ByVal sender As Object, ByVal e As EventArgs)
        pnlDett.Visible = True
        IsEdit() = "True"
        
        Me.BusinessDictionaryManager.Cache = False
        Dim dictionaryValue As DictionaryValue = Me.BusinessDictionaryManager.ReadDictionary(labelHelp, CType(Language, Integer))
              
        LoadFormDett(dictionaryValue)
        divHelp.Visible = False
    End Sub
    
    Sub SaveDictionary(ByVal sender As Object, ByVal e As EventArgs)
        Dim oValue As New DictionaryValue
        
        With (oValue)
            If SelectedId > 0 Then .Key.Id = SelectedId
            .KeyLanguage.Id = lSelector.Language
            .Key.Domain = keyDomain.Text
            .Key.Name = keyName.Text
            .Label = Label.Text
            '.Description = Description.Value
            .Description = Description.Content
            .Active = Active.Checked
        End With
        
        Me.BusinessDictionaryManager.Update(oValue)
        
        pnlDett.Visible = False
        divHelp.Visible = True
    End Sub
    
    Sub GoBack(ByVal sender As Object, ByVal e As EventArgs)
        pnlDett.Visible = False
        divHelp.Visible = True
    End Sub
    
    Sub LoadFormDett(ByVal oValue As DictionaryValue)
        If Not oValue Is Nothing Then
            With oValue
                'setto l'id
                SelectedId = oValue.Key.Id
               
                lSelector.LoadLanguageValue(.KeyLanguage.Id)
                keyDomain.Text = .Key.Domain
                keyName.Text = .Key.Name
                Label.Text = .Label
                'Description.Value = .Description
                Description.Content = .Description
                Active.Checked = .Active
            End With
        End If
    End Sub
   </script>

<script type="text/javascript">
    
    function resizeW() {
    
    var myProp = '<%=IsEdit()%>';
    var myFalse = 'False';
    
        if(myProp == 'True'){
           window.resizeTo(900,600)
          } else {
           window.resizeTo(600,400)
          }
    }
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Help</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

  
   <%--<body onload="window.resizeTo(900,900);">--%>
   <body onload="resizeW()">

       <form id="form" runat="server">  
            <div  id="divHelp" class="Help" runat="server">
                <h1>HP3 Help</h1>
                <div></div>
                <asp:LinkButton id="edit_btn" Text="Edit" OnClick="EditDictionary" runat="server" Visible="false" />
                <img src="/hp3Office/HP3Image/Ico/help.gif" alt='<%=getLabelHelp() %>'  hspace="5" border="0" align="absmiddle"/><strong><asp:Label id="lbName" runat="server"/></strong><br /><br />
                <asp:Label id="lbDescription" runat="server"  CssClass="LHelp-" /><br />
            </div>
            
            <asp:Panel id="pnlDett" runat="server" Visible="false">
                <div>
                    <asp:button runat="server" ID="btnBack"  Text="Back"  OnClick="GoBack" CssClass="button" style="margin-top:10px;margin-bottom:5px;margin-left:4px;"/>
                    <asp:button runat="server" ID="btnSave"  Text="Save"  OnClick="SaveDictionary" CssClass="button" style="margin-top:10px;margin-bottom:5px;padding-left:15px;background-image:url('/HP3Office/HP3Image/Button/save_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/>
                </div>
                
                <table class="form" style="width:99%">
                    <tr><td style="width:25%" valign="top"></td><td></td></tr>
                    <tr>
                        <td  valign ="top">Language</td>
                       <td><HP3:contentLanguage ID ="lSelector" runat="server"  Typecontrol="AlternativeControl" Enabled="false"/></td>
                    </tr>
                    <tr>
                        <td> Domain</td>
                        <td><asp:TextBox id="keyDomain" runat="server" style="width:80px" MaxLength="5" Enabled="false"/></td>
                    </tr>
                    <tr>
                        <td>Identificator Name</td>
                        <td><asp:TextBox id="keyName" runat="server" style="width:80px" MaxLength="10"  Enabled="false"/></td>
                    </tr>
                    <tr>
                        <td>Label</td>
                        <td><asp:TextBox id="Label" runat="server" style="width:600px"  Enabled="false"/></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td> 
<%--                            <FCKeditorV2:FCKeditor id="Description" Height="300px" width="600px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                        <telerik:RadEditor ID="Description" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 
                                 
                                 <Links>
                                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                                   </telerik:EditorLink>
                                 </Links>
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td><asp:checkbox ID="Active" runat="server" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>
        </form>
    </body>
</html>

