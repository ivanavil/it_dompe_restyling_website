<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script runat="Server">
           
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim SiteAreaType As Int16 = 0
    Dim nNumberSiteType As Int16
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private _sortType As SiteAreaGenericComparer.SortType
    
    Private Property SortType() As SiteAreaGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = SiteAreaGenericComparer.SortType.ById ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As SiteAreaGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    
    ''' <summary>
    ''' Legge i siti associati ad dato tipo
    ''' Il paramentro nType � opzionale se non passato restituisce tutti i siti    
    ''' </summary>
    ''' <param name="nType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSite(optional nType as int16=0) As SiteAreaCollection
        Dim sManger As New SiteAreaManager
        Dim sCollection As SiteAreaCollection
        Dim sSearcher As New SiteAreaSearcher
             
        sSearcher.Status=-1
        if nType <>  0 then sSearcher.IdType =nType
       
        sCollection = sManger.Read(sSearcher)
       
        Return sCollection
    End Function
    
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
                
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        If Not Request("SiteAreaType") Is Nothing Then
            SiteAreaType = Request("SiteAreaType")
        End If
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
       
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadSiteLabel(Request("sites")).Name & "_" & Request("sites")
           
        End If
                
        Dim SiteTypeCollection As SiteTypeCollection = ReadType()
        
        If Not (SiteTypeCollection Is Nothing) Then
            For Each type As SiteTypeValue In SiteTypeCollection
                If (type.Key.Id = SiteAreaType) Then
                
                    gridList.DataSource = GetSubArea(SiteAreaType)
                    gridList.DataBind()
                End If
            Next
        End If
    End Sub
    
    ''' <summary>
    ''' Legge la descrizione dell' area dato l'id
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadSiteLabel(ByVal idSite As Int32) As SiteAreaValue
        Return Me.BusinessSiteAreaManager.Read(New SiteAreaIdentificator(idSite))
    End Function
    
    ''' <summary>
    ''' Legge i SiteType presenti nella tabella PP_Site_Type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
     Function ReadType() As SiteTypeCollection
        Dim sCollection As SiteTypeCollection
                              
        Dim id As New SiteTypeIdentificator(SiteAreaType)
       
        sCollection = Me.BusinessSiteAreaManager.Read(id)
        nNumberSiteType = sCollection.Count
        If nNumberSiteType = 1 Then
            nFirstId = sCollection.Item(0).Key.Id
           
        End If
        
        Return sCollection
    End Function
              
    Function GetSubArea(ByVal quale As Int32) As SiteAreaCollection
        Return ReadSite(quale)
    End Function
	
  	
	sub btnSave_OnClick(s as object,e as eventargs)
		dim el as Object
		dim strOut as string
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
       
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else            
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='true' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
	    
    'metodo che si occupa della ricerca di ruoli
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oSiteAreaSearcher As New SiteAreaSearcher()
        
        If txtId.Value <> "" Then oSiteAreaSearcher.Key.Id = txtId.Value
        If txtDesciption.Value <> "" Then oSiteAreaSearcher.Label = txtDesciption.Value
        oSiteAreaSearcher.IdType = SiteAreaType
        
        BindGrid(objGrid, oSiteAreaSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As SiteAreaSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New SiteAreaSearcher
        End If
        
        Dim _ositeColl As SiteAreaCollection = Me.BusinessSiteAreaManager.Read(Searcher)
              
        _ositeColl.Sort(SortType, SiteAreaGenericComparer.SortOrder.ASC)
        objGrid.DataSource = _ositeColl
        objGrid.DataBind()
       
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
    
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Name"
                SortType = SiteAreaGenericComparer.SortType.ByName
            Case "Id"
                SortType = SiteAreaGenericComparer.SortType.ById
        End Select
        
        BindWithSearch(sender)
    End Sub
       
</script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>


    <body>
	    <form id="siteForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Site Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtDesciption" type="text" runat="server"  style="width:300px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
          
	    
	        <div id="divGrid">
                <asp:gridview ID="gridList"  
                                runat="server"
                                AutoGenerateColumns="false" 
                                GridLines="None" 
                                CssClass="tbl1"
                                HeaderStyle-CssClass="header"
                                AlternatingRowStyle-BackColor="#E9EEF4"
                                AllowPaging="true"
                               PagerSettings-Position="Top"  
                                 PagerStyle-CssClass="Pager2"               
                                AllowSorting="true"
                                PageSize ="8"
                                emptydatatext="No item available"   
                                OnPageIndexChanging="gridList_PageIndexChanging" 
                                  OnSorting="gridList_Sorting"          
                                >
                              <Columns >
                                 <asp:TemplateField HeaderStyle-Width="2%">
                                        <ItemTemplate>
                                           <span  >
                                            <%If Not bSingleSel Then%>
                                                <input  <%#fDisable(container.DataItem.Key.id)%> <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Name.replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                            <%Else%>
                                                <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Name.replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                            <%end if%>
                                           </span> 
                                        </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Name" SortExpression="Name">
                                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Name")%></ItemTemplate>
                                </asp:TemplateField>  
                             </Columns>
                    </asp:gridview>
             </div>      
		    <div style="padding-left:7px;margin-top:15px">
                <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
            </div>
	</form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	  
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	        oText.value = obj.Name
	    }
</script>