<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Product.ObjectValues"%>
<%@import Namespace="Healthware.HP3.Core.Product" %>

<script runat="Server">
    'Assunzione:
    'le costanti sono settate con il valore dlla 'Label' presente nel DB 
    'dei seguenti ContextBusiness : Brand, ProductType, SurgeryType
    Const byBrand As String = "By Brand"
    Const byProductType As String = "By Product Type"
    Const bySurgeryType As String = "By Surgery Type"
    Const byWoundType As String = "By Wound Type"
    Const byWoundManagement As String = "By Wound management"
    
    Private arrMatch() As String
    Private bSingleSel As Boolean = False
    Private nFirstId As Int32
    
    Private ListId, HiddenId As String
    Private BusinessId, LanguageId As Integer
    Private FranchiseId As Integer
    Private idContextGroup As Integer
    
    Dim utility As New WebUtility
    
    Private oRoleSearcher As RoleSearcher
    Private _sortType As RoleGenericComparer.SortType
    
    Private ContextBusinessManager As New ContextBusinessManager
           
    Sub Page_Load()
        
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
       
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        If Not Request("BusinessId") Is Nothing Then
            BusinessId = Request("BusinessId")
        End If
        
        If Not Request("LanguageId") Is Nothing Then
            LanguageId = Request("LanguageId")
        End If
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Label & "_" & Request("sites")
        End If
       
        If Not (Page.IsPostBack) Then BindFranchises()
    End Sub
    
       
    'effettua il binding della dwl relativa ai franchise
    Sub BindFranchises()
        'legge i franchise filtrando per lingua, mercato 
        Dim businessFranchise As New ContextBusinessCollection
      
        Dim franciseColl As ContextCollection = ReadFranciseByGroup()
               
        If Not (franciseColl Is Nothing) AndAlso (franciseColl.Count > 0) Then
            businessFranchise = getBusinessFranchiseKilds(franciseColl)
              
            utility.LoadListControl(dwlFranchise, businessFranchise, "Label", "KeyContextBusiness.Id")
            dwlFranchise.Items.Insert(0, New ListItem("Select Franchise", -1))
        End If
    End Sub
    
    
    Function ReadLabel(ByVal idContext As Int32) As ContextBusinessValue
        Dim myCtxBsMng As New ContextBusinessManager
        Dim myCtxBsId As New ContextBusinessIdentificator
        myCtxBsId.Id = idContext
        Dim pValue As ContextBusinessValue = myCtxBsMng.Read(myCtxBsId)
        
        Return pValue
    End Function
    
    'recupera tutti i context (franchise) 
    Function ReadFranciseByGroup() As ContextCollection
        Dim contextColl As ContextCollection = Nothing
        Dim contextIdent As New ContextGroupIdentificator
        contextIdent.Name = "franchise"
        contextIdent.Domain = "CVT"
        
        contextIdent.Id = ReadGroupIdentificator("franchise", "CVT").Id
        
        BusinessContextManager.Cache = False
        contextColl = Me.BusinessContextManager.ReadGroupContextRelation(contextIdent)
            
        Return contextColl
    End Function
    
    'recupera un ContextGroupIdentificator dato Domain e Name
    Function ReadGroupIdentificator(ByVal name As String, ByVal domain As String) As ContextGroupIdentificator
        Dim contextIdent As New ContextGroupIdentificator
        contextIdent.Name = name
        contextIdent.Domain = domain
        
        Dim contextGroupValue As ContextGroupValue = Me.BusinessContextManager.ReadGroup(contextIdent)
        If Not (contextGroupValue Is Nothing) Then
            Return contextGroupValue.Key
        End If
        
        Return Nothing
    End Function
    
    'recupera le descrizione dei context filtrando per mercato e lingua
    Function getBusinessFranchiseKilds(ByRef franchiseKilds As ContextCollection) As ContextBusinessCollection
        Dim businessFranchiseKilds As New ContextBusinessCollection
                
        For Each franchiseChildren As ContextValue In franchiseKilds
            'recupero le descrizioni del context in base all lingua, mercato e idContext                        
            Dim contextBusinessSearcher As New ContextBusinessSearcher
            contextBusinessSearcher.KeyContextBusiness.Id = franchiseChildren.Key.Id
            contextBusinessSearcher.KeyContextBusiness.IdLanguage = LanguageId
            contextBusinessSearcher.KeyContextBusiness.IdBusiness = BusinessId
               
            Dim contextBusinessColl As ContextBusinessCollection = ContextBusinessManager.Read(contextBusinessSearcher)
                
            If Not (contextBusinessColl Is Nothing) AndAlso (contextBusinessColl.Count > 0) Then
                businessFranchiseKilds.Add(contextBusinessColl(0))
            End If
        Next
            
        Return businessFranchiseKilds
    End Function
    
    
    'usato nel binding del repeater di livello 1
    'permette di recuperare tutti i figli di un frinchise attraverso la chiave del padre (frinchise)
    'e l'id del contextGroup che in questo caso ha Domain = CVT e Name = searchertype
    Function GetFrinchiseKilds(ByVal franchise As Integer) As ContextBusinessCollection
        Dim idCB As Integer = franchise
       
        Dim contextSearcher As New ContextSearcher
        contextSearcher.KeyFather.Id = idCB
                    
        contextSearcher.KeyContextGroup = ReadGroupIdentificator("searchtype", "CVT")
        
        BusinessContextManager.Cache = False
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        Dim businessFranchiseKilds As New ContextBusinessCollection
        
        If Not (contextColl Is Nothing) AndAlso (contextColl.Count > 0) Then
            businessFranchiseKilds = getBusinessFranchiseKilds(contextColl)
        End If
        
        businessFranchiseKilds.Sort(ContextBusinessGenericComparer.SortType.ByOrder, ContextBusinessGenericComparer.SortOrder.ASC)
        Return businessFranchiseKilds
    End Function
    
    Function Read() As RoleCollection
        Dim sCollection As RoleCollection
        oRoleSearcher = New RoleSearcher
             
        sCollection = Me.BusinessRoleManager.Read(oRoleSearcher)
        Return sCollection
    End Function
    
    Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
        
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
                       
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
           
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
      
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
	function fDisable(id as string) as string
        If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        
        Return ""
    End Function
	
	function fChecked(id as string) as string
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della creazione dell'albero delle categorie dato un franchise
    Sub CreateTree(ByVal s As Object, ByVal e As EventArgs)
        Dim ctxBussColl As New ContextBusinessCollection
        
        btnSave.Visible = False
        If (dwlFranchise.SelectedItem.Value <> -1) Then
            FranchiseId = dwlFranchise.SelectedItem.Value
            ctxBussColl = GetFrinchiseKilds(FranchiseId)
                
            rp_franchise_level_1.DataSource = ctxBussColl
            rp_franchise_level_1.DataBind()
            btnSave.Visible = True
        End If
        
        rp_franchise_level_1.DataSource = ctxBussColl
        rp_franchise_level_1.DataBind()
    End Sub
    
    'usato nel binding del repeater di livello 3/livello 4
    'quando gi� addentrati nel secondo livello, permette di recuperare tutti i figli 
    'della categoria con contextId = contextBusinessValue.KeyContextBusiness.Id e contextGroupId
    Function GetCategoryByFather(ByVal contextBusinessValue As ContextBusinessValue, ByVal group As Integer) As ContextBusinessCollection
        Dim keyContextBuss As Integer = contextBusinessValue.KeyContextBusiness.Id
        
        Return GetCategory(keyContextBuss, New ContextGroupIdentificator(group))
    End Function
    
    'funzione di supporto
    'restituisce una BusinessContextCollection di figli di una categoria, dati contextGroup e la categoria padre
    Function GetCategory(ByVal franchise As Integer, ByVal ctxGroup As ContextGroupIdentificator) As ContextBusinessCollection
        Dim contextSearcher As New ContextSearcher
        
        contextSearcher.KeyFather.Id = franchise
        contextSearcher.KeyContextGroup = ctxGroup
        
        BusinessContextManager.Cache = False
        Dim contextColl As ContextCollection = Me.BusinessContextManager.Read(contextSearcher)
        Dim businessFranchiseKilds As New ContextBusinessCollection
        
        If Not (contextColl Is Nothing) AndAlso (contextColl.Count > 0) Then
            businessFranchiseKilds = getBusinessFranchiseKilds(contextColl)
        End If
        
        businessFranchiseKilds.Sort(ContextBusinessGenericComparer.SortType.ByOrder, ContextBusinessGenericComparer.SortOrder.ASC)
        
        Return businessFranchiseKilds
    End Function
    
    'usato nel binding del repeater di livello 2
    'permette di recuperare tutti i figli delle seguenti categorie : Brand, ProductType, SurgeryType
    Function GetCategoryByGroup(ByVal contextBusinessValue As ContextBusinessValue) As ContextBusinessCollection
        Dim ctxGroup As New ContextGroupIdentificator()
        Dim bussFrKilds As New ContextBusinessCollection
        
        Select Case contextBusinessValue.Description
            Case byBrand
                
                ctxGroup = ReadGroupIdentificator("brand", "CVT")
                bussFrKilds = GetCategory(FranchiseId, ctxGroup)
            Case byProductType
                
                ctxGroup = ReadGroupIdentificator("prodtype", "CVT")
                bussFrKilds = GetCategory(FranchiseId, ctxGroup)
            Case bySurgeryType
                
                ctxGroup = ReadGroupIdentificator("surgtype", "CVT")
                bussFrKilds = GetCategory(FranchiseId, ctxGroup)
                
            Case byWoundType
                
                ctxGroup = ReadGroupIdentificator("wounType", "CVT")
                bussFrKilds = GetCategory(FranchiseId, ctxGroup)
                
            Case byWoundManagement
                
                ctxGroup = ReadGroupIdentificator("woundManag", "CVT")
                bussFrKilds = GetCategory(FranchiseId, ctxGroup)
        End Select
            
        Return bussFrKilds
    End Function
    
    'setta l'id del contextGroup
    Function SetContextGroup(ByVal id As Integer) As String
        Dim ctxGroupColl As ContextGroupCollection = Me.BusinessContextManager.ReadGroupContextRelation(New ContextIdentificator(id))
        
        If (Not (ctxGroupColl Is Nothing)) And (ctxGroupColl.Count > 0) Then
            idContextGroup = ctxGroupColl(0).Key.Id
        End If
                
        Return Nothing
    End Function
    
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Choose Category</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="rolesForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Choose Franchise</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>Franchise</strong></td>    
                        </tr>
                        <tr>
                            <td><asp:DropDownList id="dwlFranchise" runat="server" OnSelectedIndexChanged="CreateTree" AutoPostBack="true"/></td>
                        </tr> 
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
            
            
            <asp:Repeater id="rp_franchise_level_1" runat="server">
	               <HeaderTemplate><ul></HeaderTemplate>
	                            <ItemTemplate>
	                                <span  <%#fDisable(container.DataItem.KeyContextBusiness.Id)%> >
	                                <div><li  class="popCatTree"><input <%#fChecked(container.DataItem.KeyContextBusiness.Id)%>  name = 'chk_<%#Container.DataItem.Label%>_<%#Container.DataItem.KeyContextBusiness.Id%>' type="checkbox"/><%#DataBinder.Eval(Container.DataItem,"Label")%></li></div>    
	                                      <asp:Repeater id="rp_franchise_level_2" runat="server"   DataSource='<%#GetCategoryByGroup(Container.DataItem)%>' >
	                                        <HeaderTemplate><ul></HeaderTemplate>
	                                            <ItemTemplate>
	                                            <span  <%#fDisable(container.DataItem.KeyContextBusiness.Id)%> >
	                                                 <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                <div><li class="popCatTree"><input <%#fChecked(container.DataItem.KeyContextBusiness.Id)%>  name = 'chk_<%#Container.DataItem.Label%>_<%#Container.DataItem.KeyContextBusiness.Id%>' type="checkbox"/><%#DataBinder.Eval(Container.DataItem,"Label")%></li></div>
	                                                    <asp:Repeater id="rp_franchise_level_3" runat="server"   DataSource='<%#GetCategoryByFather(Container.DataItem,idContextGroup)%>' >
	                                                        <HeaderTemplate><ul></HeaderTemplate>
	                                                        
	                                                            <ItemTemplate>
	                                                            <span  <%#fDisable(container.DataItem.KeyContextBusiness.Id)%> >
	                                                                  <%#SetContextGroup(Container.DataItem.KeyContextBusiness.Id)%>
	                                                                  <div><li class="popCatTree"><input <%#fChecked(container.DataItem.KeyContextBusiness.Id)%>  name = 'chk_<%#Container.DataItem.Label%>_<%#Container.DataItem.KeyContextBusiness.Id%>' type="checkbox" /><%#DataBinder.Eval(Container.DataItem,"Label")%></li></div>
	                                                                        <asp:Repeater id="rp_franchise_level_4" runat="server"   DataSource='<%#GetCategoryByFather(Container.DataItem,idContextGroup)%>' >
	                                                                            <HeaderTemplate><ul></HeaderTemplate>
	                                                                                <ItemTemplate>
	                                                                                  <div><li class="popCatTree"><input <%#fChecked(container.DataItem.KeyContextBusiness.Id)%>  name = 'chk_<%#Container.DataItem.Description%>_<%#Container.DataItem.KeyContextBusiness.Id%>' type="checkbox"/><%#DataBinder.Eval(Container.DataItem,"Label")%></li></div>
	                                                                                   <%-- --%>
	                                                                                  </ItemTemplate>
	                                                                            <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                                                            </asp:Repeater>  
	                                                                    <%-- --%>
	                                                               </span> 
	                                                            </ItemTemplate>
	                                                        <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                                        </asp:Repeater>   
                                                        </span>
	                                               <%-- --%>
	                                            </ItemTemplate>
	                                          <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
                                        </asp:Repeater>  
                                        </span>
	                        </ItemTemplate>
	                 <FooterTemplate></ul><div class="clearer"></div></FooterTemplate>
            </asp:Repeater>
	            
                
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item"  Visible ="false" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>