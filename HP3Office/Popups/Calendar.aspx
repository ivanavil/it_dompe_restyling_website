<%@ Page Language="VB" debug="true" validateRequest="false" %>

<Script runat="Server">	
	sub calCourseDate_OnSelectionChanged(sender As Object, e As EventArgs) 
		Dim bmScript as String 
		
		bmScript = "<script language='JavaScript'>" & vbCrlf
		bmScript += "window.opener.document.getElementById('" & Request("field_name") & "').value='" 
		bmScript += calCourseDate.SelectedDate.ToShortDateString() & "';" & vbCrlf
		
		bmScript += "self.close();" & VbCrLf
		bmScript += "</s" & "cript>" 
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "js", bmScript)
	End Sub 		
	
	Sub Page_Load()
		If Request("title") Is Nothing Then litTitle.text = Request("title") 
	End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><asp:Literal id="litTitle" Text="Calendar" runat="server"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<style type="text/css" media="all">
		html, body { margin:0; padding:0; }
		html, body, td, th { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; }
	</style>		
</head>
<body style="background-color:#FFF ">
	<form id="Form2" runat = "server">		
		<asp:Calendar id="calCourseDate" 
			OnSelectionChanged="calCourseDate_OnSelectionChanged" 
			TodayDayStyle-BackColor="#ACAAE7"
			DayHeaderStyle-BackColor="#8C8C86"
			DayHeaderStyle-ForeColor="White"
			OtherMonthDayStyle-ForeColor="LightGray"
			NextPrevStyle-ForeColor="black"
			TitleStyle-BackColor="#E2E2E2"
			TitleStyle-ForeColor="black"
			TitleStyle-Font-Bold="True"
			TitleStyle-Font-Size="15px"
			SelectedDayStyle-BackColor="Navy"
			SelectedDayStyle-Font-Bold="True"
			runat="server"/>
	</form>
</body>
</html>
