<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<%@ Import Namespace="Healthware.HP3.Core.User" %>

<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public idContent As Integer
    Public idLang As Integer
    Public codeLang As String
    Public imageFiles As List(Of String) ' 1221
    Public MaxCoverForContent As Integer = 3

    ' 1221
    Function GetCover(ByVal idContent As Integer, ByVal idLang As Integer) As List(Of String)
        Dim oCManager As New ContentManager
        
        Dim coll As New List(Of String)
       
        Dim i As Integer = 0
        
        For i = 0 To MaxCoverForContent - 1
            Dim vo As New ImageQualityValue
            vo.ImageQuality = ImageQualityValue.EnumImageQuality.Medium
            Dim sImage As String = oCManager.GetCover(New ContentIdentificator(idContent, idLang), 0, 0, vo, i)
            If Not String.IsNullOrEmpty(sImage) Then coll.Add(sImage) Else coll.Add("NONE")
         
        Next
  
           
        Return coll
         
    End Function
    
    Sub page_init()
        If Request("idContent") <> "" Then
            idContent = Integer.Parse(Request("idContent"))
        End If
        If Request("idLang") <> "" Then
            idLang = Integer.Parse(Request("idLang"))
        End If
        codeLang = Request("codeLang")
        'Preleva l'immagine
        imageFiles = GetCover(idContent, idLang)
    
    End Sub
    
 
    
    
    Sub Page_Load()
        Dim accService As New AccessService
        
        If accService.IsAuthenticated Then
            divIMG.Visible = True
            Dim objImg As System.Drawing.Bitmap
            btnDel0.Visible = False
            tmb0.ImageUrl = "/HP3Office/HP3Image/ico/NoCover.gif"
   
            tmbInfo0.Text = "No cover image available for this content."
        
            btnDel2.Visible = False
            tmb2.ImageUrl = "/HP3Office/HP3Image/ico/NoCover.gif"
       
            tmbInfo2.Text = "No cover #3"
        
            btnDel1.Visible = False
            tmb1.ImageUrl = "/HP3Office/HP3Image/ico/NoCover.gif"
 
            tmbInfo1.Text = "No cover #2"
        
        
            If Not Page.IsPostBack Then
                If Not imageFiles Is Nothing AndAlso imageFiles.Count > 0 Then
                    Dim ratio As Int32
                    If imageFiles.Count > 0 AndAlso imageFiles(0) <> "NONE" Then
                        btnAdd0.Text = "Add"
                        objImg = System.Drawing.Image.FromFile(Server.MapPath("/HP3Image/cover/" & imageFiles(0)))
                   
                        If objImg.Width > 230 Then
                            ratio = (objImg.Width / objImg.Height)
                            tmb0.Width = 230
                            tmb0.Height = (230 / ratio)
                        End If
               
                        tmb0.ImageUrl = "/HP3Image/cover/" & imageFiles(0)
                        If Request.QueryString.ToString.Split("&").Length > 2 Then tmb0.ImageUrl = "/HP3Image/cover/" & imageFiles(0) & "?" & Date.Now
                        tmbInfo0.Text = "File: <b>" & imageFiles(0) & "</b> - W = <b>" & objImg.Width & "px</b> H = <b>" & objImg.Height & "px</b> - "
                
                        If imageFiles(0).IndexOf(codeLang & ".") <> -1 Then
                            tmbInfo0.Text = tmbInfo0.Text & "lang: <b>" & codeLang & "</b>"
                        Else
                            tmbInfo0.Text = tmbInfo0.Text & "<b>Generic Cover</b>"
                        End If
                
                        btnDel0.Visible = True
                        If Not objImg Is Nothing Then objImg.Dispose()
                        objImg = Nothing
                    End If
                
                    If imageFiles.Count > 1 AndAlso imageFiles(1) <> "NONE" Then
 
                        btnAdd1.Text = "Add"
                        objImg = System.Drawing.Image.FromFile(Server.MapPath("/HP3Image/cover/" & imageFiles(1)))
                
                        If objImg.Width > 230 Then
                            ratio = (objImg.Width / objImg.Height)
                            tmb1.Width = 230
                            tmb1.Height = (230 / ratio)
                        End If
               
                        tmb1.ImageUrl = "/HP3Image/cover/" & imageFiles(1)
                        If Request.QueryString.ToString.Split("&").Length > 2 Then tmb1.ImageUrl = "/HP3Image/cover/" & imageFiles(1) & "?" & Date.Now
                        tmbInfo1.Text = "File: <b>" & imageFiles(1) & "</b> - W = <b>" & objImg.Width & "px</b> H = <b>" & objImg.Height & "px</b> - "
                
                        If imageFiles(1).IndexOf(codeLang & ".") <> -1 Then
                            tmbInfo1.Text = tmbInfo1.Text & "lang: <b>" & codeLang & "</b>"
                        Else
                            tmbInfo1.Text = tmbInfo1.Text & "<b>Generic Cover</b>"
                        End If
                
                        btnDel1.Visible = True
                        If Not objImg Is Nothing Then objImg.Dispose()
                        objImg = Nothing
        
                 
                    End If
                
                    If imageFiles.Count > 2 AndAlso imageFiles(2) <> "NONE" Then
 
                        btnAdd2.Text = "Add"
                        objImg = System.Drawing.Image.FromFile(Server.MapPath("/HP3Image/cover/" & imageFiles(2)))
                
                        If objImg.Width > 230 Then
                            ratio = (objImg.Width / objImg.Height)
                            tmb2.Width = 230
                            tmb2.Height = (230 / ratio)
                        End If
               
                        tmb2.ImageUrl = "/HP3Image/cover/" & imageFiles(2)
                        If Request.QueryString.ToString.Split("&").Length > 2 Then tmb2.ImageUrl = "/HP3Image/cover/" & imageFiles(2) & "?" & Date.Now
                        tmbInfo2.Text = "File: <b>" & imageFiles(2) & "</b> -  W = <b>" & objImg.Width & "px</b> H = <b>" & objImg.Height & "px</b> - "
                
                        If imageFiles(2).IndexOf(codeLang & ".") <> -1 Then
                            tmbInfo2.Text = tmbInfo2.Text & "lang: <b>" & codeLang & "</b>"
                        Else
                            tmbInfo2.Text = tmbInfo2.Text & "<b>Generic Cover</b>"
                        End If
                
                        btnDel2.Visible = True
                        If Not objImg Is Nothing Then objImg.Dispose()
                
 
                    End If
                    If Not objImg Is Nothing Then objImg.Dispose()
                    objImg = Nothing
        
 
                End If
    
            
            
            End If
            objImg = Nothing
      
        Else
            
            plLogin.Controls.Add(LoadControl(Me.BusinessContentTypeManager.LoadPathControlConsolle(Me.ObjectSiteFolder, New ContentTypeIdentificator("HP3", "Login"))))
        End If
    End Sub
    
    Sub UploadCover(ByVal sender As Object, ByVal e As System.EventArgs)
        If TargetFile.PostedFile.ContentLength > 0 Then
            'delFiles()
            If TargetFile.FileName.IndexOf(".gif") <> -1 Or TargetFile.FileName.IndexOf(".jpg") <> -1 Then
                
                Dim f_name As String = ""
                Dim index As Integer = 0
                Dim indexString As String = String.Empty
                If Not Integer.TryParse(txtIndex.Text, index) Then index = 0
 
                index = index - 1
                
                If index > 0 Then
                    indexString = "-" & (index).ToString
                End If
                
                If All.Checked Then f_name = idContent.ToString & indexString & "_gen." & TargetFile.FileName.Split(".")(1)
                If Lang.Checked Then f_name = idContent.ToString & indexString & "_" & codeLang & "." & TargetFile.FileName.Split(".")(1)
                TargetFile.SaveAs(Server.MapPath("/HP3Image/cover/" & f_name))
            
                selectFile.Visible = False
              
                Response.Redirect("/HP3office/popups/ImageCoverUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
            Else
                Err.Text = "<div class='litErroressage'>Invalid file (" & TargetFile.FileName & ") check for extension and try again (gif and jpg image only!).</div>"
            End If
        End If
    End Sub
    
    Sub selFile(ByVal sender As Object, ByVal e As System.EventArgs) '
        sender.visible = False
        mainPnl.Visible = False
        selectFile.Visible = True
        btnDel0.Visible = False
        btnDel1.Visible = False
        btnDel2.Visible = False
        btnBack.Visible = True
        txtIndex.text = sender.CommandArgument +1
    End Sub
    
    Sub delFile0(ByVal sender As Object, ByVal e As System.EventArgs)
        delFiles(0)
        Response.Redirect("/HP3office/popups/ImageCoverUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
    End Sub
    Sub delFile1(ByVal sender As Object, ByVal e As System.EventArgs)
        delFiles(1)
        Response.Redirect("/HP3office/popups/ImageCoverUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
    End Sub
    Sub delFile2(ByVal sender As Object, ByVal e As System.EventArgs)
        delFiles(2)
        Response.Redirect("/HP3office/popups/ImageCoverUpload.aspx?" & Request.QueryString.ToString & "&" & Date.Now)
    End Sub
    Sub delFiles(index As Integer)
        Try
            If Not imageFiles Is Nothing AndAlso imageFiles.Count > 0 AndAlso index < imageFiles.Count Then
               
 
                
                If imageFiles(index).Length() > 8 Then
                    Dim files As String() = System.IO.Directory.GetFiles(Server.MapPath("/HP3Image/cover/"), imageFiles(index).Split(".")(0) & "*")
                    Dim file As String
                    Dim fso = New FileIO.FileSystem()
                    For Each file In files
                        'Response.Write(file)
                        ' Response.Write("<br/>")
                        fso.DeleteFile(file)
                        'fso.DeleteFile(file)
                    Next
                    'Dim fso = New FileIO.FileSystem()
                    'fso.DeleteFile(Server.MapPath("/HP3Image/cover/") & imageFile)
                End If
            End If
                    
        Catch ex As Exception
        End Try
    End Sub
    
    Sub back(ByVal sender As Object, ByVal e As System.EventArgs)
        btnAdd0.Visible = True
        btnAdd1.Visible = True
        btnAdd2.Visible = True
        
        mainPnl.Visible = True
        selectFile.Visible = False
        btnDel0.Visible = True
        btnDel1.Visible = True
        btnDel2.Visible = True
        
        btnBack.Visible = False
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
 <title>HP3 Cover Image Management</title>
</head>

<link type="text/css" rel="stylesheet" href="/HP3Office/_css/consolle.css" />

<body>

    <form id="formIMG" runat="server">

    <asp:PlaceHolder runat="server" ID="plLogin"/>
    <div id="divIMG" runat="server" visible="false">
 
      
     <asp:PlaceHolder runat="server" ID="mainPnl">
       <div style="width:100%;clear:both;">
         
                    <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;" class="innerMenu">
                         DEFAULT COVER
                    </div>
                      <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;" class="innerMenu">
                         Alternative Cover 1
                    </div>
                      <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;" class="innerMenu">
                        Alternative Cover 2
                    </div>
                 
         </div>
        <div  style="width:100%;" class="formIMG" >
        <div style="width:100%;clear:both;">
         
                    <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;height:40px;" class="innerMenu">
                        <asp:Label runat="server" ID="tmbInfo0" />
                    </div>
                      <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;height:40px;" class="innerMenu">
                        <asp:Label runat="server" ID="tmbInfo1"/>
                    </div>
                      <div style="float:left;width:32%;font-size:9px;margin:0px;padding:2px;text-align:center;margin-left:5px;height:40px;" class="innerMenu">
                        <asp:Label runat="server" ID="tmbInfo2"/>
                    </div>
                 
         </div>
         
       
        <div style="width:100%;clear:both;">
            <div style="float:left;width:32%;padding:2px;text-align:center;margin-left:5px;">
            <asp:Image runat="server" ID="tmb0" BorderColor="black" BorderWidth="1px" BorderStyle="Solid"  />
             </div>
            <div style="float:left;width:32%;padding:2px;text-align:center;margin-left:5px;">
                <asp:Image runat="server" ID="tmb1" BorderColor="black" BorderWidth="1px" BorderStyle="Solid"  />
            </div>
             <div style="float:left;width:32%;padding:2px;text-align:center;margin-left:5px;">
                <asp:Image runat="server" ID="tmb2" BorderColor="black" BorderWidth="1px" BorderStyle="Solid"  />
            </div>
        </div>
         <div style="width:100%;clear:both;margin:20px;">&nbsp;</div>
        <div style="width:100%;clear:both;">
            <div style="float:left;width:32%;padding:0px;text-align:center;margin-left:5px;">
                <asp:Button runat="server" Text="New" CssClass="button" OnClick="selFile" ID="btnAdd0" CommandArgument ="0"/>
                <asp:Button runat="server" Text="Delete"  CssClass="button" OnClick="delFile0"  OnClientClick="if (!confirm('Delete this cover?')) return false;"  ID="btnDel0"/>
            </div>
           <div style="float:left;width:32%;padding:0px;text-align:center;margin-left:5px;">
                <asp:Button runat="server" Text="New" CssClass="button" OnClick="selFile" ID="btnAdd1"  CommandArgument ="1"/>
                <asp:Button runat="server" Text="Delete" CssClass="button" OnClick="delFile1"  OnClientClick="if (!confirm('Delete this cover?')) return false;"  ID="btnDel1"/>
             </div>
              <div style="float:left;width:32%;padding:0px;text-align:center;margin-left:5px;">
                <asp:Button runat="server" Text="New" CssClass="button" OnClick="selFile" ID="btnAdd2"  CommandArgument ="2"/>
                <asp:Button runat="server" Text="Delete" CssClass="button" OnClick="delFile2"  OnClientClick="if (!confirm('Delete this cover?')) return false;"  ID="btnDel2"/>
             </div>
        </div>
            
      </div>
       <div style="width:100%;clear:both;text-align:right;">
       <br />    <br />
                    <asp:Button runat="server" Text="Close" CssClass="button" OnClientClick="window.close();false"  ID="btnClose"/>
        </div>   
 

    </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="selectFile" Visible="false">
          <div class="innerMenu">&nbsp;<b>Cover Image</b></div>
      <table width="100%" class="formIMG" >
 
    <tr height="280" align="center">
    <td>
    <div style="width:250px;padding-left:145px;">
    Cover: #<asp:Label runat="server" ID="txtIndex"></asp:Label>
    <br />  <br />
    <asp:Label runat="server" ID="Err" />
    File :
    <asp:FileUpload runat="server" ID="TargetFile"/>
    <small>*.gif or *.jpg only</small>
    <br />
    <br />
    Cover Type:
    <br />
    <asp:RadioButton runat="server" ID="All" GroupName="langChk" Text="Generic cover" TextAlign="Right" Checked="true" /><br />
    <asp:RadioButton runat="server" ID="Lang" GroupName="langChk" Text="Language related cover" TextAlign="Right" />
    <br /><br />
    <asp:Button runat="server" ID="btn_go"  Text="Upload" OnClick="UploadCover"  CssClass="button"/>
     <asp:Button runat="server" Text="Back" CssClass="button" onClick="back"  ID="btnBack"  />

    <br />
    </div>
 
    </td>
    </tr>
         
    </table>
    </asp:PlaceHolder>   
    
 
    </div>
    </form>
</body>
</html>


