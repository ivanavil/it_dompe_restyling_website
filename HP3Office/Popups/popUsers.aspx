<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private oUserSearcher As UserSearcher
    Private _sortType As UserGenericComparer.SortType
    
    Private utility As New WebUtility
    Const maxRow As Integer = 8
  
    Private Property SortType() As UserGenericComparer.SortType
        Get
            If Not ViewState("SortType") Is Nothing Then
                _sortType = ViewState("SortType")
            Else
                _sortType = UserGenericComparer.SortType.BySurName ' SortType di Default                
            End If
            Return _sortType
        End Get
        Set(ByVal value As UserGenericComparer.SortType)
            _sortType = value
            ViewState("SortType") = _sortType
        End Set
    End Property
    
    
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadUser(Request("sites")).CompleteName & "_" & Request("sites")
        End If
        
        Dim userColl As UserCollection = Read()
        
        'Se il numero di utenti � >= maxRow allora il risultato viene associato alla GridView 
        'altrimenti al repeater
        If Not (UserColl Is Nothing) Then
            If (UserColl.Count >= maxRow) Then
                gridList.DataSource = userColl
                gridList.DataBind()
                
            Else
                repUser.DataSource = userColl
                repUser.DataBind()
            End If
        End If
    End Sub
        
    Function Read() As UserCollection
        Dim userCollection As UserCollection
        oUserSearcher = New UserSearcher()
        userCollection = Me.BusinessUserManager.Read(oUserSearcher)
        Return userCollection
    End Function
    
    
    ' legge un utente dato l'id
    Function ReadUser(ByVal idUser As Int32) As UserValue
        Dim userValue As UserValue = Me.BusinessUserManager.Read(New UserIdentificator(idUser))
        
        Return userValue
    End Function
    
   	
    Function setVisibility(ByVal id As Int32) As String
        Dim sCollection = Read()
        If sCollection Is Nothing OrElse sCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        oUserSearcher = New UserSearcher()
        If txtId.Value <> "" Then oUserSearcher.Key.Id = txtId.Value
        If txtName.Value <> "" Then oUserSearcher.Name = txtName.Value
        If txtSurname.Value <> "" Then oUserSearcher.Surname = txtSurname.Value
        
        BindGrid(objGrid, oUserSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New UserSearcher
        End If
        
        Dim _oUserColl As UserCollection = Me.BusinessUserManager.Read(Searcher)
        If Not (_oUserColl Is Nothing) Then
            _oUserColl.Sort(SortType, UserGenericComparer.SortOrder.ASC)
            
            If (_oUserColl.Count >= maxRow) Then
                ActiveGridView()
                
                objGrid.DataSource = _oUserColl
                objGrid.DataBind()

            Else
                ActiveRepeater()
                
                repUser.DataSource = _oUserColl
                repUser.DataBind()
                
            End If
        Else
            ActiveGridView()
            
            objGrid.DataSource = _oUserColl
            objGrid.DataBind()
        End If
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
   
    Protected Sub gridList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        gridList.PageIndex = 0
        
        Select Case e.SortExpression
            Case "Name"
                SortType = UserGenericComparer.SortType.ByUserName
            Case "Surname"
                SortType = UserGenericComparer.SortType.BySurName
            Case "Id"
                SortType = UserGenericComparer.SortType.ByKey
        End Select
        
        BindWithSearch(sender)
    End Sub
    
    'rende visibile il GridView
    Sub ActiveGridView()
        gridList.Visible = True
        repUser.Visible = False
    End Sub
    
    'rende visibile il Repeater
    Sub ActiveRepeater()
        gridList.Visible = False
        repUser.Visible = True
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Users List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="userForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">User Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Name</strong></td>
                            <td><strong>Surname</strong></td>
                        </tr>
                        <tr>
                            <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>
                            <td><input id="txtName" type="text" runat="server"  style="width:200px"/></td>  
                            <td><input id="txtSurname" type="text" runat="server"  style="width:200px"/></td>  
                        </tr> 
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            OnSorting="gridList_Sorting"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                       <span  <%#fDisable(container.DataItem.Key.id)%> >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.CompleteName.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>
                                        <%Else%>
                                            <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.CompleteName.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="radio" />
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" SortExpression="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Name" SortExpression="Name">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "CompleteName")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
         
        <div id="divRepeater" class="title" style="margin-top:10px">    
            <asp:Repeater id="repUser" runat="Server">
                <HeaderTemplate></HeaderTemplate>
                <itemtemplate>							
                    <div class="dControl" >
                        <span  <%#fDisable(container.DataItem.Key.id)%> >
                        <%If Not bSingleSel Then%>
                        <input <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.CompleteName%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.CompleteName = "", "[Descrizione Assente]", Container.DataItem.CompleteName)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%Else%>
                        <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.CompleteName%>_<%#Container.DataItem.Key.id%>' type="radio"/>&nbsp;<%#IIf(Container.DataItem.CompleteName = "", "[Descrizione Assente]", Container.DataItem.CompleteName)%>&nbsp;[<%#Container.DataItem.key.id%>]
                        <%end if%>
                        </span>
                    </div>
                </itemtemplate>
            </asp:Repeater>
        </div>
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj,label){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name)){   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>