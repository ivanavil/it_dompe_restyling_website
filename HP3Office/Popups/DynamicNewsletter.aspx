<%@ Page Language="VB" debug="true"  validaterequest="false"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Box" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility" %>
<%@ Import NameSpace="System.io" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

<%@ Register tagprefix="HP3" TagName ="ctlSite" Src ="~/hp3Office/HP3Parts/ctlSite.ascx"  %>

<script runat="Server">
    <Serializable()> _
    Class ItemChecked
        Public id As Int32
        Public optId As Int32
        Public desc As String
    End Class
    
    Dim idStep As Int16 = 1
    Dim idTml, idNl As String
    Dim idLanguage As Int16
    Dim arr As Hashtable
    Dim strJs As String
    Dim StrIn As String
    Dim strPAth As String
    Dim bAction As Boolean
    Dim strDomain As String
    Dim bookmark As Boolean
    Dim _link As String = "ciao"
    Dim _siteId As String
    
    Private _strDomain As String
    Private _language As String
    Private masterPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    Private webutility As New WebUtility
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Private Property Link() As String
        Get
            Return _link
        End Get
        Set(ByVal value As String)
            _link = value
        End Set
    End Property
    
    Private Property SiteId() As String
        Get
            Return _siteId
        End Get
        Set(ByVal value As String)
            _siteId = value
        End Set
    End Property
    
    ''' <summary>
    ''' Salvataggio dell' HTML nel controllo chiamante
    ''' </summary>
    ''' <param name="s"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub SaveFullText(ByVal s As Object, ByVal e As EventArgs)
        Dim strAll As String
        'modificare nome file
        Dim strFile As String = GetFullPathTmpFile(idNl & "_" & Request("id_tm") & ".Txt")
        
        If Not File.Exists(strFile) Then
            strJs = "<script>" & vbCrLf
            strJs += "alert('Error during saving, Impossobile to recover the file.')" & vbCrLf
            strJs += "</s" & "cript>"            
            Exit Sub
        End If

        Dim objFile As New FileStream(strFile, FileMode.Open)
        Dim objReader As New StreamReader(objFile)
        strAll = objReader.ReadToEnd()

        objReader.Close()
        objFile.Close()

        'Aggiornamento campi della newsletter
        If InsertFullText(strAll, idNl) Then
            bAction = True
            File.Delete(strFile)
            
            strJs = "<script>" & vbCrLf
            strJs += "alert('Saved successfully');" & vbCrLf
            strJs += "window.opener.location.reload(true);" & vbCrLf
            strJs += "window.close();"
            strJs += "</s" & "cript>"
        Else
            strJs = "<script>" & vbCrLf
            strJs += "alert('Error during saving');" & vbCrLf
            strJs += "</s" & "cript>"
        End If
        
    End Sub
    
    Function InsertFullText(ByVal strAll As String, ByVal idNL As Int32) As Boolean
        Dim oNewsletterManager As New NewsletterManager
        Dim oNewletterValue As NewsletterValue
       
        Try           
            oNewletterValue = oNewsletterManager.Read(New NewsletterIdentificator(Request("idNL"), Request("idLingua")))
           
            oNewletterValue.FullTextHtml = strAll
            oNewletterValue.RefTemplate = idTml
                                
            oNewsletterManager.Update(oNewletterValue)
                    
            Return True
            
        Catch er As Exception
            Response.Write(er.ToString)
            Return False
        End Try
    End Function
    
    Function GetFullPathTmpFile(ByVal FileName As String) As String
        Return PhisicalTempPath & "\" & FileName
    End Function
    
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
   
        Dim mystringBuilder As New StringBuilder()
        Dim stringWriter As New StringWriter(mystringBuilder)
        Dim htmlWriter As New HtmlTextWriter(stringWriter)
        
        MyBase.Render(htmlWriter)
        
        Dim html As String = mystringBuilder.ToString()
        
        If idStep = 3 Then
            'Modificare il nome del file usare funzione di hashing
            'Il path utilizzare una costante della classe configuration
            Dim strFile As String = GetFullPathTmpFile(idNl & "_" & Request("id_tm") & ".Txt")
  
            If Request("id_tm") <> "" And idNl <> "" And Not bAction Then
                If File.Exists(strFile) Then File.Delete(strFile)
            
                Dim objFile As New FileStream(strFile, FileMode.Create)
                Dim objWriter As New StreamWriter(objFile)
                objWriter.Write(EditRender(html))

                objWriter.Close()
                objFile.Close()
            End If
        End If
        
        writer.Write(html)
    End Sub
    
    
    Function EditRender(ByVal strSource As String) As String
        Dim pos As Integer = strSource.IndexOf("<!--Inizio NL-->")
        Dim pos2 As Integer

        If Pos <> -1 Then
            pos += 16
            pos2 = strSource.IndexOf("<!--Fine NL-->", pos)
            If Pos2 <> -1 Then
                strSource = strSource.SubString(pos, pos2 - pos)
            End If
        End If
        
        Return strSource
    End Function
    
    ReadOnly Property isTemplateSelected()
        Get
            Return False
            'If Not ViewState("itemSel") Is Nothing Then
            '    Return True
            'Else
            '    Return False
            'End If
        End Get
    End Property
    
    Function ReadTemplateInfoById(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateNewsletterValue
        Dim oNewsLetterManager As New NewsletterManager
        Return oNewsLetterManager.Read(idTemplate)
    End Function
    
    Function ReadNewsletterbyid(ByVal idNl As NewsletterIdentificator) As NewsletterValue
        Dim oNewsLetterManager As New NewsletterManager
        Dim oNewsLetterSearcher As New NewsletterSearcher
        oNewsLetterSearcher.Key.PrimaryKey = idNl.KeyNewsletter
        Dim oCol As NewsletterCollection = oNewsLetterManager.Read(oNewsLetterSearcher)
        
        If Not oCol Is Nothing Then
            Return oCol.Item(0)
        End If
     
    End Function
    
    Public ReadOnly Property DomainInfo()
        Get
            Dim SiteManager As New SiteManager
            Dim SiteValue As SiteValue = SiteManager.getDomainInfo()
            Dim arr() As String = SiteValue.Folder.Split("/")
            ReDim Preserve arr(arr.GetUpperBound(0) - 1)
            Dim strDomain = "http://" & SiteValue.Domain & "/" & Join(arr, "/")
            
            Return strDomain
        End Get
    End Property
    
    Function ReadContentIdentificator(ByVal keyNewsletter As NewsletterIdentificator) As NewsletterIdentificator
        Dim cm As New ContentManager
        Dim ci As ContentIdentificator = cm.getContentIdentificator(keyNewsletter.Id, keyNewsletter.IdLanguage)
        keyNewsletter.PrimaryKey = ci.PrimaryKey
        
        Return keyNewsletter
    End Function
    
     
    Sub Page_Load()
        Language() = masterPageManager.GetLang.Id
        Domain() = WebUtility.MyPage(Me).DomainInfo()
        
        Dim strTesto As String
        Dim oNewsletterValue As NewsletterValue
        Dim oTemplateValue As TemplateNewsletterValue
        
        strDomain = DomainInfo
        
        If Not Request("Step") Is Nothing Then idStep = Request("Step")
        If Not Request("id_tm") Is Nothing Then idTml = Request("id_tm")
        If Not Request("idNL") Is Nothing Then idNl = Request("idNL")
        If Not Request("idLingua") Is Nothing Then idLanguage = Request("idLingua")
        
        idNl = ReadContentIdentificator(New NewsletterIdentificator(idNl, idLanguage)).PrimaryKey
               
        If idTml <> 0 Then
            oTemplateValue = ReadTemplateInfoById(New TemplateNesletterIdentificator(idTml))
            If Not oTemplateValue Is Nothing Then
                strPAth = oTemplateValue.Path
                
            End If
        End If
       
        If Not Page.IsPostBack Then
            openPanel(idStep)
                       
            
            If Not Request("idNL") Is Nothing OrElse Request("idNL") <> 0 Then
                oNewsletterValue = ReadNewsletterbyid(New NewsletterIdentificator(idNl))
                
                If Not oNewsletterValue Is Nothing Then
                   
                    strTesto = "The newsletter <strong>" & oNewsletterValue.Title & " [" & oNewsletterValue.Key.PrimaryKey & "]</strong> is currently associated to the  <strong>"
                    
                    If oNewsletterValue.RefTemplate <> 0 Then
                        oTemplateValue = ReadTemplateInfoById(New TemplateNesletterIdentificator(Int32.Parse(oNewsletterValue.RefTemplate)))
                        
                        If Not oTemplateValue Is Nothing Then
                           
                            strTesto += oTemplateValue.Description & " [" & oTemplateValue.Key.Id & "]</strong>"
                            
                            Dim oItem = New ItemChecked
                            oItem.id = oTemplateValue.Key.Id
                            oItem.optId = oTemplateValue.Key.Id
                            oItem.desc = oTemplateValue.Path
					                
                            ViewState("itemSel") = oItem
                            
                            lblDesc.Text = strTesto
                            pnlDesc.Visible = True
                        End If
                    End If
                End If
 
            End If
        End If
        
        ActionOnStep()
        SetupButton()
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
    
     
    Sub SelectStep(ByVal id As Int16)
        If id = 0 Then Exit Sub
        
        Dim oItem = New ItemChecked
        oItem.id = Int32.Parse(id)
        oItem.optId = Int32.Parse(id)
        oItem.desc = ""
					       
        ViewState("itemSel") = oItem
                
    End Sub
    
    Sub SetupButton()
        btnPrec.Attributes.Add("onClick", "return OnClickAttributes()")
        btnPrec.Visible = setVis(idStep, False)
        btnPrec.CommandArgument = idStep - 1
        'btnPrecTop.Attributes.Add("onClick", "return OnClickAttributes()")
        'btnPrecTop.Visible = btnPrec.Visible
        'btnPrecTop.CommandArgument = idStep - 1
        
        btnSucc.Attributes.Add("onClick", "return OnClickAttributes()")
        btnSucc.Visible = setVis(idStep, True)
        btnSucc.CommandArgument = idStep + 1
        'btnSuccTop.Attributes.Add("onClick", "return OnClickAttributes()")
        'btnSuccTop.Visible = btnSucc.Visible
        'btnSuccTop.CommandArgument = idStep + 1
    End Sub
    
    Sub ActionOnStep()
        Select Case idStep
            Case 1
                'litMsg.Text = "Scelta Template"
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep1&Help=cms_HelpCreateNewsletterStep1&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep1", "Step 1 - Choose template") & "</strong>"
                If Not Page.IsPostBack Then
                    SelectStep(idTml)
                    ViewState("ordinamento") = "nl_tm_id"
                    ViewState("OrderType") = "DESC"
                   
                    BindData()
                End If
            Case 2
           
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep2&Help=cms_HelpCreateNewsletterStep2&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep2", "Step 2 - Box association") & "</strong>"
                If Not Page.IsPostBack Then
                    BindDataBox()
                End If
                
            Case 3
                litMsg.Text = "<a href=""#""  onclick=""window.open('" & Domain() & "/Popups/popHelp.aspx?Label=cms_FormCreateNewsletterStep3&Help=cms_HelpCreateNewsletterStep3&Language=" & Language() & "','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')"" >" & "<img  src='/hp3Office/HP3Image/Ico/help.gif' alt='help' />" & "</a>" & "<strong>" & getLabel("cms_FormCreateNewsletterStep3", "Step 3 - Preview") & "</strong>"
                If strPAth <> "" AndAlso TemplateExist(strPAth) Then
                    Dim obj As Object = LoadControl(VirtualTemplatePath & "/" & strPAth)
                    obj.idNl = idNl
                    obj.idLanguage = idLanguage
                    plcAnteprima.Controls.Add(obj)
                    btnUpdateFullTex.Visible = True
                    'btnUpdateFullTexTop.Visible = True
                Else
                    plcAnteprima.Controls.Add(New LiteralControl("<h2>The Template " & strPAth & " not exist into " & ConfigurationsManager.NewsletterTemplatePath & ".<br/>Impossible to continue.</h2>"))
                    bAction = True
                End If
        End Select
    End Sub
    
    Function TemplateExist(ByVal strPath As String) As Boolean        
        Return File.Exists(SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterTemplatePath & "\" & strPath)
    End Function
    
    Function ReadBoxByTemplate(ByVal idTemplate As TemplateNesletterIdentificator) As TemplateBoxCollection
        Dim oNewsletterManager As New NewsletterManager
        oNewsletterManager.Cache = False
        
        Dim idBox As BoxIdentificator
        
        Return oNewsletterManager.ReadBoxTemplate(idTemplate, idBox)
    End Function
    
    Function GetBoxCollection(ByVal src As TemplateBoxCollection) As BoxCollection
        Dim outCol As New BoxCollection
        Dim oBoxVal As BoxValue
        Dim oBoxManager As New BoxManager
        
        If Not src Is Nothing AndAlso src.Count > 0 Then
            For Each oTBV As TemplateBoxValue In src
                oBoxVal = oBoxManager.Read(oTBV.IdBox)
                If Not oBoxVal Is Nothing Then
                    outCol.Add(oBoxVal)
                End If
            Next
        End If
         
        Return outCol
    End Function
    
    Sub BindDataBox()
        Dim oTemplateBox As TemplateBoxCollection = ReadBoxByTemplate(New TemplateNesletterIdentificator(idTml))
        If Not oTemplateBox Is Nothing Then
            rpBox.DataSource = GetBoxCollection(oTemplateBox)
            rpBox.DataBind()
                    
            If rpBox.Items.Count = 0 Then
                WriteMsg("No box associated to the template" & idTml, "dWarningMsg", lblMsg2)
            End If
        End If
    End Sub
    
    Sub openPanel(ByVal nStep As Int16)
        If nStep = 0 Then Return
        
        Dim ctl As Control
        ctl = FindControl("pnl_" & nStep)
        If Not ctl Is Nothing Then
            ctl.Visible = True
        End If
    End Sub
    
    Sub WriteMsg(ByVal Testo As String, ByVal Classe As String, Optional ByVal ctl As Panel = Nothing)
        If ctl Is Nothing Then ctl = lblMsg2
        ctl.CssClass = Classe
        
        ctl.Controls.Clear()
        ctl.Controls.Add(New LiteralControl(Testo))
    End Sub
    
    Sub cambiapagina(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        FindCheked()
        dgElenco.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub
    
    Sub cambiapaginaBox(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        rpBox.CurrentPageIndex = e.NewPageIndex
        BindDataBox()
    End Sub
    
    Function GetTemplates() As TemplateNesletterCollection
        Dim oNewsletterManager As New NewsletterManager
        oNewsletterManager.Cache = False
        Return oNewsletterManager.Read()
    End Function
    
    Sub BindData()        
        Dim TemplateCollection As TemplateNesletterCollection = GetTemplates()
        If TemplateCollection Is Nothing OrElse TemplateCollection.Count = 0 Then
            dgElenco.Visible = False
            
            WriteMsg("No box found", "dWarningMsg", lblMsg2)
        Else
            dgElenco.Visible = True
            
            WriteMsg("", "", lblMsg2)
            
            dgElenco.DataKeyField = "Key"
            dgElenco.DataSource = TemplateCollection
            dgElenco.DataBind()
        End If
    End Sub
    
    Sub ordinamento(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
        FindCheked()
        ViewState("ordinamento") = e.SortExpression
    
        If ViewState("OrderType") Is Nothing OrElse ViewState("OrderType") = "DESC" Then
            ViewState.Add("OrderType", "ASC")
        Else
            ViewState("OrderType") = "DESC"
        End If

        dgElenco.CurrentPageIndex = 0
        BindData()
    End Sub
    
    Sub FindCheked()
        Dim obj As DataGridItem
        Dim ctl, chk As Object
        Dim oItem As ItemChecked
		  		
        For Each obj In dgElenco.Items
            chk = obj.FindControl("chkSel")
            ctl = obj.FindControl("litVal")
            If Not chk Is Nothing AndAlso Not ctl Is Nothing Then
				                
                If CType(chk, CheckBox).Checked Then
                    oItem = New ItemChecked
                    oItem.id = Int32.Parse(ctl.text)
                    oItem.optId = Int32.Parse(ctl.text)
                    oItem.desc = obj.Cells(2).Text
					                
                    ViewState("itemSel") = oItem
                    Exit For
                End If
            End If
        Next
    End Sub
    
    Sub ReloadChk(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If Not ViewState("itemSel") Is Nothing Then
			   
                If ViewState("itemSel").id = dgElenco.DataKeys(e.Item.ItemIndex).Id Then
                    Dim ctl As Object
                    
                    ctl = e.Item.FindControl("chkSel")
                    If Not ctl Is Nothing Then
                        ctl.checked = True
                        strJs = "<script>" & vbCrLf
                        strJs += "gestClick(document.getElementById('" & ctl.clientid & "'))" & vbCrLf
                        strJs += "</s" & "cript>"
                    End If
                End If
            End If
        End If
    End Sub
    
    Function getTotStep() As Int16
        Dim ctl As Control
        Dim curId As Int16 = 0
        Dim Id As Int16 = 0
        
        For Each ctl In Form1.Controls
            If TypeOf (ctl) Is Panel AndAlso ctl.ID.ToString.Substring(0, 3) = "pnl" Then
                Id = ctl.ID.ToString.Substring(4)
                If Id > curId Then curId = Id
                
            End If
        Next
       
        Return Id
    End Function
    
    'True  - Ultimo
    'False - Primo
    Function setVisButton(ByVal nStep As Int16, ByVal buttonType As Boolean) As String
        Dim strRet As String
        
        If (nStep = 1 AndAlso Not buttonType) OrElse (nStep = getTotStep AndAlso buttonType) Then
            strRet = "style='display:none'"
        End If                
        
        Return strRet
    End Function
    
    'True  - Ultimo
    'False - Primo
    Function setVis(ByVal nStep As Int16, ByVal buttonType As Boolean) As Boolean
        If (nStep = 1 AndAlso Not buttonType) OrElse (nStep = getTotStep() AndAlso buttonType) Then
            Return False
        End If
        
        Return True
    End Function
    
    Function EditQs(ByVal srcQs As String, ByVal Key As String, ByVal Valore As String) As String
        Dim strqs As String = srcQs
        Dim sKey As String
        Dim bExist As Boolean
        
        If Key = "" Then Return strqs
        bExist = IIf(Request(Key) Is Nothing, False, True)
        
        If bExist Then
            Dim str As String
            Dim i As Int16
            Dim arr() As String = strqs.Split("&")
            Dim arr2() As String
            
            For i = 0 To arr.Length - 1
                str = arr(i)
                arr2 = str.Split("=")
                If Key <> "" AndAlso arr2(0) = Key Then
                    arr2(1) = Valore
                    
                    arr(i) = Join(arr2, "=")
                    Exit For
                End If
            Next
          
            strqs = Join(arr, "&")
            Return strqs
        Else
            
            Return strqs & "&" & Key & "=" & Valore
        End If
    End Function
    
    Function getUrlWQ() As String
        Dim strUrl As String = Request.Url.ToString
        Dim arr() As String = strUrl.Split("?")
        
        Return arr(0)
    End Function
    
    Sub ActionOnCursor(ByVal s As Object, ByVal e As EventArgs)
        Dim Key, Valore As String
                
        SelectActionOnStep(Key, Valore)      
        Response.Redirect(getUrlWQ() & "?" & EditQs(EditQs(Request.QueryString.ToString, "Step", s.commandargument), Key, Valore))
        
    End Sub
    
    Sub SelectActionOnStep(ByRef Key As String, ByRef Valore As String)
        Select Case idStep
            Case 1   
                FindCheked()
                Key = "id_tm"
                Valore = getIdSelected()
        End Select
    End Sub
    
    Function getIdSelected() As Int16                     
        If Not ViewState("itemSel") Is Nothing Then
                
            Return ViewState("itemSel").id
        End If
    End Function
          
    
    Sub SalvaContent(ByVal sender As Object, ByVal e As EventArgs)
        Dim bResult As Boolean = False
               
        If txtHidden_CK1.Text <> "" Then
            bResult = True
            Dim arr() As String = txtHidden_CK1.Text.Split("�")
            For Each str As String In arr
                If IsNumeric(str) Then
                    
                    bResult = bResult And EditContent(New BoxIdentificator(CType(sender.CommandArgument(), Integer)), New NewsletterIdentificator(idNl), New ContentIdentificator(Int32.Parse(str)))
                End If
            Next
        End If
        
        If bResult Then
            Response.Redirect(Request.Url.ToString)
        Else
            WriteMsg("Error during saving.", "dErrorMsg")
        End If
    End Sub
           
    Function getList_ID(ByVal val As String)
        Dim arr() As String = val.Split("_")
        Return arr(0)
    End Function
    
    Function UpdateBOX_CN(ByVal oBoxNCValue As BoxContentNewsletterValue) As Boolean
        Try
            Dim oBoxManager As New BoxManager
            oBoxNCValue = oBoxManager.Update(oBoxNCValue)
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    Function RemoveItem(ByVal key As BoxContentNewsletterIdentificator) As Boolean
        Dim obox As New BoxManager
        
        Try
            obox.Remove(key)
            Return True
        Catch ex As Exception            
            Return False
        End Try
    End Function
    
    Sub Move(ByVal key As BoxContentNewsletterIdentificator, ByVal Url As String, ByVal moltiplicatore As Int16)
        Dim oBoxNCValue As BoxContentNewsletterValue
        Dim oBoxNCPrecValue As BoxContentNewsletterValue
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        
        Dim order As Int16
        'Recupero l'elemento selezionato per lo spostamento 
        oBoxNCSearcher.Key.Id = key.Id
        oBoxNCValue = LoadBoxContentNewsletter(oBoxNCSearcher)
        '------------------------------------
        
        'Recupero l'elemento precedente
        oBoxNCSearcher = New BoxContentNewsletterSearcher
        oBoxNCSearcher.KeyBox.Id = oBoxNCValue.KeyBox.Id
        oBoxNCSearcher.KeyNewsletter = oBoxNCValue.KeyNewsletter
        oBoxNCSearcher.Order = oBoxNCValue.Order - moltiplicatore '- 1
        
        oBoxNCPrecValue = LoadBoxContentNewsletter(oBoxNCSearcher)
        Response.Write(oBoxNCPrecValue Is Nothing)
        '-----------------------------
        
        If Not oBoxNCValue Is Nothing Then
            order = oBoxNCValue.Order
            If order = 1 AndAlso moltiplicatore = 1 Then Return
            
            order = order - moltiplicatore '- 1
            
            oBoxNCValue.Order = order
            If Not oBoxNCPrecValue Is Nothing Then
                If UpdateBOX_CN(oBoxNCValue) Then
                    order = oBoxNCPrecValue.Order
                   
                    order = order + moltiplicatore '+1
            
                    oBoxNCPrecValue.Order = order
                    If UpdateBOX_CN(oBoxNCPrecValue) Then
                        Response.Redirect(Url)
                    Else
                        WriteMsg("Error during this operation", "dErrorMsg")
                    End If
                Else
                    Response.Redirect(Url)
                End If
            End If
        End If
    End Sub
    
       
    Sub Action(ByVal s As Object, ByVal e As DataGridCommandEventArgs)
        Dim ret As Int16
        Dim lst As ListBox = e.Item.FindControl("lstContent")
        Dim key As BoxContentNewsletterIdentificator
       
        If e.CommandName = "Delete" Or e.CommandName = "Up" Or e.CommandName = "Down" Then
            If Not lst Is Nothing Then
                If lst.SelectedItem Is Nothing Then
                    strJs = "<script>" & vbCrLf
                    strJs += "alert('Select an element to continue.')" & vbCrLf
                    strJs += "</s" & "cript>"
                    Return
                End If
            End If
        
        
            Dim strUrl As String = Request.Url.ToString
            key = New BoxContentNewsletterIdentificator()
            key.Id = getList_ID(lst.SelectedItem.Value)
        
            Select Case e.CommandName
                Case "Delete"
               
                    If RemoveItem(key) Then
                        RemoveFile(key)
                        Response.Redirect(strUrl)
                    Else
                        WriteMsg("Erroe during connection cancellation", "dErrorMsg")
                    End If
                               
                Case "Up"
                    Move(key, strUrl, 1)
                    'ret = admin.Move(40, e.CommandArgument, getList_ID(lst.SelectedItem.Value), idNl)
                
                Case "Down"
                    'ret = admin.Move(41, e.CommandArgument, getList_ID(lst.SelectedItem.Value), idNl)
                    Move(key, strUrl, -1)
            End Select
        End If
    End Sub
    
    Sub RemoveFile(ByVal id As BoxContentNewsletterIdentificator)
        Dim oFolder As New IO.DirectoryInfo(PhisicalPath)
        Dim oFile As FileInfo
        
        For Each oFile In oFolder.GetFiles(id.Id & ".*")
            On Error Resume Next
            oFile.Delete()
        Next
    End Sub
     
    ReadOnly Property VirtualTemplatePath() As String
        Get
            Return "/" & ConfigurationsManager.NewsletterTemplatePath
        End Get
    End Property
    
    ReadOnly Property PhisicalPath() As String
        Get
            Return SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterImagePath
        End Get
    End Property
    
    ReadOnly Property PhisicalTempPath() As String
        Get
            Dim Path As String = SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterTempPath
            If Not IO.Directory.Exists(Path) Then
                IO.Directory.CreateDirectory(Path)
            End If
            
            Return Path
        End Get
    End Property
    
    Sub ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs)
        Dim ctl As Control = e.Item.FindControl("btnCancella")
        If Not ctl Is Nothing Then
            CType(ctl, Button).Attributes.Add("onclick", "return confirm('Are you sure you want to erase the item?')")
        End If
        
        Dim id As String
        
        ctl = e.Item.FindControl("litVal")
        If Not ctl Is Nothing Then
            id = CType(ctl, Literal).text
        End If
        
        ctl = e.Item.FindControl("rdTypeItem")
        If Not ctl Is Nothing Then
            CType(ctl, RadioButtonList).Attributes.Add("onclick", "ChangeSel(this," & id & ")")
            CType(ctl, RadioButtonList).SelectedIndex = CType(ctl, RadioButtonList).Items.IndexOf(CType(ctl, RadioButtonList).Items.FindByValue(0))
        End If
        
        ctl = e.Item.FindControl("lstContent")
        If Not ctl Is Nothing Then
            CType(ctl, ListBox).Attributes.Add("onDblClick", "gestEdit(this," & id & ")")
        End If
        
        Dim contentBox As ListBox = e.Item.FindControl("lstContent")
        If Not contentBox Is Nothing Then
            Dim boxIdentificator As New BoxIdentificator
            boxIdentificator.Id = id
            LoadContentBox(contentBox, boxIdentificator, New NewsletterIdentificator(idNl))
        End If
        
        Dim dwlSite As DropDownList = e.Item.FindControl("dwlSite")
        If Not dwlSite Is Nothing Then
            Dim siteManager As New SiteAreaManager
            Dim siteSearcher As New SiteAreaSearcher
            
            siteSearcher.IdType = 1
            Dim siteColl As SiteAreaCollection = siteManager.Read(siteSearcher)
           
            webutility.LoadListControl(dwlSite, siteColl, "Label", "Key.Id")
            dwlSite.Items.Insert(0, New ListItem("--Select Site--", -1))
           
        End If
    End Sub
    
    Function BindTypeItem()
        Dim oH As New Hashtable
        
        oH.Add(0, "Content")
        oH.Add(1, "Banner")
        oH.Add(2, "Testo")               
        
        Return oH
    End Function
    
    Sub LoadContentBox(ByVal oList As ListControl, ByVal keyBox As BoxIdentificator, ByVal keyNewsletter As NewsletterIdentificator)
        Dim oBoxManager As New BoxManager
        oBoxManager.Cache = False
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        Dim Key As String
        Dim Valore As String
        
        Try
            oBoxNCSearcher.KeyBox = keyBox
            oBoxNCSearcher.KeyNewsletter = keyNewsletter
            
            Dim oBoxCNCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
            If Not oBoxCNCollection Is Nothing Then
                For Each oBoxNCValue As BoxContentNewsletterValue In oBoxCNCollection
                    Valore = "" : Key = ""
                    Key = oBoxNCValue.Key.Id & "_" & oBoxNCValue.BoxContentType
                                       
                    Select Case oBoxNCValue.BoxContentType
                        Case BoxContentNewsletterValue.ContentBoxType.Text
                            If oBoxNCValue.FreeText.Length >= 7 Then
                                Valore = "[Testo] " & oBoxNCValue.FreeText.Substring(0, 7) & "..."
                            Else
                                Valore = "[Testo] " & oBoxNCValue.FreeText
                            End If
                        Case BoxContentNewsletterValue.ContentBoxType.Banner
                            Valore = "[Immagine] " & oBoxNCValue.Key.Id & ".*"
                        Case BoxContentNewsletterValue.ContentBoxType.Content
                            Dim oUtility As New GenericUtility
                            Dim cv As ContentValue = oUtility.GetContentValue(oBoxNCValue.KeyContent.PrimaryKey)
                            'Dim cv As ContentValue = oUtility.GetContentValue( oBoxNCValue.KeyContent.PrimaryKey, idLanguage)
                            If Not cv Is Nothing Then
                                Valore = cv.Title
                            End If
                    End Select
                    oList.Items.Add(New ListItem(Valore, Key))
                Next
            End If
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub

    Function LoadBoxContentNewsletter(ByVal oBoxNCSearcher As BoxContentNewsletterSearcher) As BoxContentNewsletterValue
        Dim oBoxManager As New BoxManager
        oBoxManager.Cache = False
        Dim oBoxNCCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
        If Not oBoxNCCollection Is Nothing AndAlso oBoxNCCollection.Count > 0 Then
            Return oBoxNCCollection.Item(0)
        End If
    End Function
    
    Function SetBookmark(ByVal Path As String) As String
        bookmark = TemplateExist(Path)
    End Function
           
    Function EditContent(ByVal keyBox As BoxIdentificator, ByVal keyNewletter As NewsletterIdentificator, ByVal KeyContent As ContentIdentificator) As Boolean
        Dim oBoxManger As New BoxManager
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        Dim oBoxNcCollection As BoxContentNewsletterCollection
        Dim oBoxNCValue As BoxContentNewsletterValue
        
        Dim order As Int32
        Try
            'Recupero dell' order
            oBoxNCSearcher.KeyBox = keyBox
            oBoxNCSearcher.KeyNewsletter = keyNewletter
            oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
            
            If Not oBoxNcCollection Is Nothing Then
                order = oBoxNcCollection.Count + 1
            Else
                order += 1
            End If
            '---------------------------
            
            oBoxNCValue = New BoxContentNewsletterValue
            oBoxNCValue.KeyBox = keyBox
            oBoxNCValue.KeyNewsletter = keyNewletter
            oBoxNCValue.Order = order
            oBoxNCValue.KeyContent = KeyContent
            oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Content
            'oBoxManger.Create(oBoxNCValue)
            
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Guided creation of the newsletter body</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link href="../_css/popup.grp.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
    
    <script type="text/javascript">
    
    var msg
            
    function checkRadioCheck()
        {
            var arr = document.getElementsByTagName('input') 
            var oId
            for (i=0;i<arr.length;i++)
                {
                    oId = arr[i].id
                    if (oId.indexOf('chkSel') != -1)
                        if (arr[i].checked) return true;
                }
                
                return false;
        }
        
    function gestClick(obj)
	    {
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.id))
	            {
	                ochk = document.getElementById (oText.value)
	                ochk.checked = false;
	            }
	            
	        oText.value = obj.id
	    }	   
	    
	    function checkClick()
	        {
	            switch (<%=idStep%>)
	                {
	                    case 1:	                    
    	                    if ((<%=iif(isTemplateSelected,1,0)%>==1)||checkRadioCheck())
    	                        return true;
    	                    else
    	                        msg='No template selected';
    	                        return false;
	                        break;    	                    	                    
	                }
	            return true;
	        }
	    
	    function OnClickAttributes()
	        {
	            if (! checkClick())	   	                     	                
                   { 
                    alert(msg);
                    return false
                    }
	        }

        function setVis_All(Valore,key)
            {
                var arr = document.getElementsByTagName ("span")
                for(i=0;i<arr.length;i++)
                    {
                        if (arr[i].id.indexOf('spn_' + key) != -1 )
                            {
                                arr[i].style.display=Valore
                            }
                    }
            }    
        
        function setVis_Single(id,key,Valore)
            {
                var oDiv =document.getElementById ('spn_' + key + '_' + id)
                if (oDiv != null)
                    {
                        oDiv.style.display=Valore
                    }
            }                        
        function ChangeSel(obj,id)
            {
                setVis_All('none',id);
                setVis_Single(getRadioItemSel(obj),id,'')   
            }	        
            
        function getRadioItemSel(obj)
            {
         
                var Rad = obj.getElementsByTagName('input')
                
                 for(i=0;i<Rad.length;i++)
                    {
                        if (Rad[i].checked)
                            {
                                return Rad[i].value
                            }                            
                    }
            }
                
        function getTextVal(strText)
        {
            var oTxt = document.getElementById (strText)
            return oTxt.value
        }    
        
        function SaveContentSelection(strTxtDest2)
            {
                var arr = strTxtDest2.split('_')
	            var id = arr[arr.length -1];
	            
	            var oDiv = document.getElementById ('divAcc_' + id + '_0');
                if (oDiv != null)
                    {	                            
                        var arr = oDiv.getElementsByTagName ('input')
                        for (i=0;i<arr.length;i++)
                            {	                                    
                                if (arr[i].id.indexOf('submit') != -1)
                                    {	                                            
                                        arr[i].click();
                                        return                                       
                                    }
                            }
                    }	           
            }
            
        function addRow(ContentsKey,strValId,strValDesc,strTxtDest2,strTxtDest1,strTxtDest3)
	    {	        	   
	        var arr = strTxtDest2.split('_')
	        var id = arr[arr.length -1];
	           
//	        var oTxtId = document.getElementById (strTxtDest1);
//	        var oTxtDesc = document.getElementById (strTxtDest2);
//	        var oTxtCK  = document.getElementById (strTxtDest3);
	        
	       
	        if (!ExistsId(strValId,'txtHidden_CK1'))
	        {	              	        
//                oTxtId.value=strValDesc;
//                oTxtDesc.value=strValId;
//                oTxtCK.value = ContentsKey;
//                alert(ContentsKey)
        
                document.getElementById ('<%=txtHidden_Desc1.clientid%>').value+= strValId + '�'	                          
                document.getElementById ('<%=txtHidden_CK1.clientid%>').value += ContentsKey + '�' 
                document.getElementById ('<%=txtContents1.clientid%>').value += strValDesc + '�'
            }
/*
            var oDiv = document.getElementById ('divAcc_' + id + '_0');
            if (oDiv != null)
                {	                            
                    var arr = oDiv.getElementsByTagName ('input')
                    for (i=0;i<arr.length;i++)
                        {	                                    
                            if (arr[i].id.indexOf('submit') != -1)
                                {	                                            
                                    arr[i].click();
                                    return
                                   // location.href ='<%=request.url.tostring%>'
                                }
                        }
                }	                
               // }
       */
	    }
	    
	    function AddItemToList()
	        {}
	        
	    function ExistsId(val,field)
		    {
		      var oTxtId1 = document.getElementById (field);		        		        
		      return (oTxtId1.value.indexOf(val + '�') != -1) // || (val == oTxtId2.value))  
		    }
		    		            
        
        function moveTo(id)    	    
            {
                location.href ="#"  + id;
            }
            
        function getListItemVal(str,pos)
            {
                var arr = str.split('_')
                return arr[pos]
            }
        function gestEdit (obj,idBox)
            {
                if (getListItemVal(obj.options[obj.selectedIndex].value,1) == 2)                
                    nType=1
                else
                    nType=0
                    
                if (getListItemVal(obj.options[obj.selectedIndex].value,1) != 1)
                window.open('<%=strDomain%>/Popups/ItemTemplate.aspx?Type=' + nType + '&idNl=<%=idNl%>&Box=' + idBox + '&idItem=' + getListItemVal(obj.options[obj.selectedIndex].value,0),'','width=400,height=400,scrollbars=no,status=yes')                 
            }  
	 </script> 
</head>

<body>



<form id="Form1" runat="server">
	<asp:textbox id="txtHidden" runat="server" width="100%" style="display:none"/>
	<asp:textbox  runat="server" id='txtHidden_Desc1' style="display:none"/>        
    <asp:textbox runat="server" id='txtHidden_CK1' style="display:none"/>
    <asp:textbox  runat="server" id='txtContents1' style="display:none"/>

	<asp:Panel ID="lblMsg2" runat="server" ForeColor ="red" />
	   <div style="text-align:left; margin-top:10px; margin-left:2px">
    	   <asp:literal runat="server" ID="litMsg" />
       </div> 	    
	    <%--<div style="text-align:center">
	        <asp:button cssclass="button" runat="server" text="<< Step Precedente" onClick="ActionOnCursor" id="btnPrecTop" />
            <asp:button cssclass="button" runat="server" text="Step Successivo >>" onClick="ActionOnCursor" id="btnSuccTop" />
            <asp:button cssclass="button" runat="server" text="Conferma" onClick="SaveFulltext" id="btnUpdateFullTexTop" visible="false" />
	    </div><br/>--%>
	    
	    <asp:Panel id="pnl_1" runat="server" visible="false">
	         
             <asp:Panel  ID="pnlDesc" visible="false" runat="server">
				<asp:Label ID="lblDesc" runat="server"/>
		    </asp:Panel>
		    
            <fieldset class="fsStdGroup">
                 <div class="subbox">                
                    <asp:datagrid ID="dgElenco" 
                        runat ="server"                                                                         
                        AutoGenerateColumns="False" 
                        OnPageIndexChanged="cambiapagina"                         
                        AllowPaging="True" 
                        PagerStyle-Mode="NumericPages"	                    
                        HorizontalAlign="Center" 
                        PageSize="10"   	                   
	                    OnSortCommand="ordinamento"
                    	onItemDataBound="ReloadChk"	                   
	                    HeaderStyle-CssClass="header"
                        PagerStyle-HorizontalAlign="Right"  
                        PagerStyle-CssClass="Pager"	
                        PagerStyle-Position="Top"  
                                     
	                    CssClass="tbl1"> 
	                      	        
                        <Columns>       
                            <asp:TemplateColumn >
                                <ItemTemplate >
                                    <%#SetBookmark(Container.DataItem.path)%>                                   
                                </ItemTemplate>
                                </asp:TemplateColumn>     
                                <asp:TemplateColumn>
                                    <ItemTemplate >
                                        <asp:radiobutton runat="server" name="radio" id="chkSel" onClick="gestClick(this)"/>
					                    <asp:literal id="litVal" runat="Server" text='<%#container.dataitem.Key.Id%>' visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>   
                                
                                 <asp:TemplateColumn>
                                    <ItemTemplate >                                        
                                        <asp:literal ID="Literal1" Text ="<%#container.dataitem.Key.Id%>" runat="server" />                                       
                                    </ItemTemplate>
                                    </asp:TemplateColumn>                                                                                                                                                  
                                <asp:BoundColumn DataField="Path" HeaderText="Path" SortExpression="Path"></asp:BoundColumn>                                
                                <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description"></asp:BoundColumn>
                                <asp:TemplateColumn > 
                                    <ItemTemplate >                                                                                         
                                        <img id="Img1" runat="server" visible ="<%#not bookmark%>" src="../HP3Image/Ico/content_delete.gif" alt="Template not available"/>                                            
                                                                                                                                                                                     
                                        <%--<a runat="server" visible ="<%#bookmark%>" onclick="window.open('<%=strDomain%>/Popups/anteprima_newsletter.aspx?idNL=<%#idNl%>&idTemplate=<%#container.dataitem.Key.Id%>','','width=800,height=600,scrollbars=yes,status=no')" href='#'>                        --%>
                                        <img id="Img2" runat="server" visible ="<%#bookmark%>"  src="../HP3Image/Ico/ico_16x_preview.gif" alt="Template available"/>                                           
                                        <%--</a>--%> 
                                         
                                    </ItemTemplate> 
                                </asp:TemplateColumn>
                            </Columns>
                    </asp:datagrid>
                </div>
            </fieldset>
        </asp:Panel>
	    
	    <asp:Panel id="pnl_2" runat="server" visible="false">	        	        
	       <fieldset class="fsStdGroup">
                <div class="subbox" >           
                <asp:datagrid ID="rpBox" runat ="server"                                                 
                        AutoGenerateColumns="False" 
                        OnPageIndexChanged="cambiapaginaBox" 
                        DataKeyField="idBox" 
                        AllowPaging="True" 
                        PagerStyle-Mode="NumericPages"	                    
                        HorizontalAlign="Center" 
                        PageSize="10"   	                    	                    	                   
	                    CssClass="tbl3"
	                    onItemCommand="Action"
	                    OnItemDataBound="ItemDataBound"
	                    HeaderStyle-CssClass="header"
                        PagerStyle-HorizontalAlign="Right"  
                        PagerStyle-CssClass="Pager"
                        PagerStyle-Position="Top" 
                        width="100%">    	    
                     <Columns>
                        <asp:BoundColumn  DataField="Name" HeaderText="Box" HeaderStyle-Width="50px" ItemStyle-Width ="50px"></asp:BoundColumn>                        
                        <asp:TemplateColumn HeaderStyle-Width="300px" ItemStyle-Height ="100px">
                        <ItemTemplate>
                        <asp:literal ID="litVal" runat="server" text='<%#container.dataitem.idBox%>' visible ="false"/>
                        <table  cellpadding="0" cellspacing="0" width="100%" class="form">
                            <tr>
                                <td><asp:radiobuttonlist  Width="100%"  ID="rdTypeItem" runat="server" datasource="<%#BindTypeItem%>" datavaluefield="key" datatextfield="value"/></td>
                                <td>
                                ���<a name= '<%#container.dataitem.idBox%>' />
                                <div id='divAcc_<%#container.dataitem.idBox%>_0' >
                                    <input type="text" style="display:none" id='txtHidden_Desc1_<%#container.dataitem.idBox%>'  style="width:100%"   />        
                                    <input type="text" style="display:none" id='txtHidden_CK1_<%#container.dataitem.idBox%>'  style="width:100%"  />
                                    <input type="text" style="display:none" id='txtContents1_<%#container.dataitem.idBox%>'  style="width:100%"   />
                                    <asp:button onclick ="SalvaContent" id ='submit' commandargument='<%#container.dataitem.idBox%>' runat="server" style="display:none" />                                
                                    
                                    <table cellpadding ="0" cellspacing ="0" width="100%" style="border:0px" >
                                        <tr>
                                            <td colspan ="2">
                                                <strong>Link</strong>&nbsp;
                                                <asp:textbox ToolTip ="Link to associate" ID="txtLink" runat="server" Width="75%"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan ="2">
                                                <strong>Site</strong>&nbsp;
                                                <asp:dropdownlist id="dwlSite" runat="server" />
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td width ="90%">
                                                    <asp:Listbox 
                                                    width ="100%" 
                                                    id="lstContent"
                                                    runat="Server"
                                                     />
                                            </td>
                                            <td valign ="center">                                        
                                                <asp:button ID="Button1" commandargument='<%#container.dataitem.idBox%>' commandname ="Up" runat="server" style="width:25px" text="&uarr;" cssclass="button" tooltip="Moves higher the selected item"/>
                                                <br />
                                                <asp:button ID="Button2" commandargument='<%#container.dataitem.idBox%>' commandname ="Down" runat="server" style="width:25px" text="&darr;" cssclass="button" tooltip="Moves lower the selected item" />
                                            </td>                                
                                        </tr>     
                                        <tr>
                                            <td colspan ="2" >
                                                <span id="spn_<%#container.dataitem.idBox%>_0">
                                                    <input type="button" value="Insert" class="button" title="Allows you to select a new content to be associated to the box" onClick="window.open('<%=strDomain%>/Popups/popContentContent.aspx?txtHidden=' + getTextVal('txtHidden_CK1_<%#container.dataitem.idBox%>') + '&txtDestField1=txtContents1_<%#container.dataitem.idBox%>&txtDestField2=txtHidden_Desc1_<%#container.dataitem.idBox%>&txtDestField3=txtHidden_CK1_<%#container.dataitem.idBox%>','','width=780,height=480,scrollbars=yes')"/>
                                                </span>
                                                <span id="spn_<%#container.dataitem.idBox%>_1" style="display:none">
                                                   <input type="button" value="Insert"  class="button" title="Allows you to select a new image/banner to be associated to the box" onClick="window.open('<%=strDomain%>/Popups/ItemTemplate.aspx?Link=<%=Link() %>&idNl=<%=idNl%>&box=<%#container.dataitem.idBox%>&Type=0','','width=600,height=350,scrollbars=no,status=yes')"/>
                                                </span>
                                                <span id="spn_<%#container.dataitem.idBox%>_2" style="display:none">
                                                    <input type="button" value="Insert" class="button" title="Allows you to select a new text to be associated to the box" onClick="window.open('<%=strDomain%>/Popups/ItemTemplate.aspx?idNl=<%=idNl%>&box=<%#container.dataitem.idBox%>&Type=1','','width=600,height=350,scrollbars=no,status=yes')"/>
                                                </span>
                                                <asp:button style="width:100px" id="btnCancella" Text="Cancel" commandname ="Delete" runat="server" cssclass="button" tooltip="Remove the selected element" commandargument='<%#container.dataitem.idBox%>'/>                                        
                                                                                         
                                            </td>
                                        </tr>                           
                                    </table>
                                </div>                            
                               </tr>
                            </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    </asp:datagrid>    
                </div>                    
            </fieldset>                    
	    </asp:Panel>

	    <asp:Panel id="pnl_3" runat="server" visible="false">
	        <!--Inizio NL-->
	              <asp:placeholder ID="plcAnteprima" runat ="server" />
	        <!--Fine NL-->
	        
	    </asp:Panel>
	    <div style ="text-align:right; margin-top:20px; margin-right:5px">
	        <asp:button cssclass="button" runat="server" text="<< Previous Step" onClick="ActionOnCursor" id="btnPrec" />
            <asp:button cssclass="button" runat="server" text="Next Step >>" onClick="ActionOnCursor" id="btnSucc" />
            <asp:button cssclass="button" runat="server" text="Save" onClick="SaveFulltext" id="btnUpdateFullTex" visible="false" />
        </div>
    </form>	    
</body >
</html> 
 <%=strJs %>