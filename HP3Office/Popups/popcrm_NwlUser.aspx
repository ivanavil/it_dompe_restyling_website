<%@ Page Language="VB"  EnableEventValidation="true" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack" %>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest"%>
<script runat="Server">
    Sub Page_Load()
        If Not Page.IsPostBack Then
            Dim id As Integer = 0
            Dim DetEvent As String = ""
            id = Request("SelItem")
            DetEvent = Request("DetailEvent")
            LoadUser(id, DetEvent)
        End If
    End Sub
    
    Sub LoadUser(ByVal idNwl As Integer, ByVal eventiId As Integer)
        Dim appId As String = idNwl
        Dim webTrackMan As New WebTrackManager
        Dim WebTrackNewsSearcher As New WebTrackNewsletterSearcher
        Dim WebTrackUserColl As New WebTrackUserCollection

        WebTrackNewsSearcher.NwId = appId
        WebTrackNewsSearcher.EventId = eventiId
        If eventiId = 1 Then
            WebTrackUserColl = webTrackMan.ReadNwUserSend(WebTrackNewsSearcher)
        Else
            WebTrackUserColl = webTrackMan.ReadNwUniqueUserSend(WebTrackNewsSearcher)
        End If
        GridUser.DataSource = WebTrackUserColl
        GridUser.DataBind()
    End Sub
    Sub btnExport_Click(ByVal s As Object, ByVal e As EventArgs)
        Dim webTrackMan As New WebTrackManager
        Dim WebTrackNewsSearcher As New WebTrackNewsletterSearcher
        Dim WebTrackUserColl As New WebTrackUserCollection

        WebTrackNewsSearcher.NwId = Request("SelItem")
        WebTrackNewsSearcher.EventId = Request("DetailEvent")
        If Request("DetailEvent") = 1 Then
            WebTrackUserColl = webTrackMan.ReadNwUserSend(WebTrackNewsSearcher)
        Else
            WebTrackUserColl = webTrackMan.ReadNwUniqueUserSend(WebTrackNewsSearcher)
        End If
        
        Response.AddHeader("content-disposition", "fileattachment;filename=HP3ImportContents-" + DateTime.Now.ToUniversalTime.ToString("yyyyMMddhhmmss") + ".xls")
        Response.ContentType = "application/ms-excel"
            
        Dim sw As String = String.Empty
        sw += "<table><tr><td>User ID</td><td>Name</td><td>Surname</td><td>E-mail</td></tr></table>"
        sw += "<p></p>"
        sw += "<table>"
           
               
        For Each elem As WebTrackUserValue In WebTrackUserColl
            sw = sw & "<tr><td>" & elem.UserValue.Key.Id & "</td><td>" & elem.UserValue.Name & "</td><td>" & elem.UserValue.Surname & "</td><td>" & elem.UserValue.Email.ToString & "</td></tr>"
        Next
        sw += "</table>"
        Response.Write(sw)
        Response.End()
        
        
    End Sub
    
    Protected Sub GridUser_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridUser.PageIndex = e.NewPageIndex
        LoadUser(Request("SelItem"), Request("DetailEvent"))
    End Sub
  </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Newsletter User Details</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>
    <body>
    <form id="formHistory" runat="server" >
         <div class="title" style="padding-left:10px">Newsletter User Details</div>
         <br />
         <hr />
         
        <div id="divTraceHistory">
        <asp:Button Text="Export" ID="btnExport" runat="server"  OnClick="btnExport_Click"/>
            <asp:GridView ID="GridUser" 
            AutoGenerateColumns="false"  
            CssClass="tbl1"
            AlternatingRowStyle-BackColor="#eeefef"
            RowStyle-BackColor="#fffffe"
            PagerSettings-Position="Top"  
            PagerStyle-HorizontalAlign="Right" 
            PagerStyle-CssClass="Pager"  
            OnPageIndexChanging="GridUser_PageIndexChanging"
            ShowHeader="true" 
            AllowPaging="true"  
            PageSize="10" 
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            emptydatatext="No content available"
            Width="60%"
            GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="Username " HeaderStyle-Width="9%">
                    <ItemTemplate>
                        <div style="text-align:left">
                            <%#DataBinder.Eval(Container.DataItem, "UserValue.Name")%> - <%#DataBinder.Eval(Container.DataItem, "UserValue.Surname")%> - <%#DataBinder.Eval(Container.DataItem, "UserValue.Key.Id")%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderText="Username " HeaderStyle-Width="9%">
                    <ItemTemplate>
                        <div style="text-align:left">
                            <%#DataBinder.Eval(Container.DataItem, "UserValue.Email")%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
        </div> 
    </form>     
    </body>
</html>

