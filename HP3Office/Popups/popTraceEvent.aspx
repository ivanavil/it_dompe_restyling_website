<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName="ctlTraceEventManager" src="~/hp3Office/HP3Service/TraceEventManager.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public DomainInfo As String
    
    Private StrJs As String
    Sub SelItem(ByVal s As Object, ByVal e As EventArgs)
        Dim ctcol As Object = ctlTraceEventManager.GetSelection
        Dim ov As TraceEventValue
        If Not ctcol Is Nothing AndAlso ctcol.count > 0 Then
            ov = ctcol.item(0)
            
            If Not ov Is Nothing AndAlso Not Request("ctlHidden") Is Nothing AndAlso Not Request("ctlVisible") Is Nothing Then
                StrJs = "window.opener.SetValue('" & Request("ctlHidden") & "','" & Request("ctlVisible") & "'," & ov.Key.Id & ",'" & Server.HtmlDecode(ov.Description).Replace("'", "`").Replace("?", "\?") & "')" & vbCrLf
                StrJs += "this.close()"
            End If
                      
        Else
            StrJs = "alert('Select an item to continue.')"
        End If
    End Sub
    
    Sub Page_Load()
        If Not Request("SelItem") Is Nothing AndAlso Not Page.IsPostBack Then            
            ctlTraceEventManager.SetDefaultChecked = Request("SelItem")
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Trave Event List</title>    	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />	
	
	<script type="text/javascript" >
	    <%=StrJs%>
	</script>
</head>
<body>
    <form id="TraceEventForm" runat="server">    
        <div style="text-align:left">
            <HP3:ctlTraceEventManager runat="server" ID="ctlTraceEventManager" TypeControl="Selection" />
        </div>
        <div style="text-align:left;margin-top:15px">
            <asp:button ID ="btnSel" runat="server" onclick="SelItem"  CssClass="button" Text="Select Item"/>
        </div>
    </form>
</body>
</html>
