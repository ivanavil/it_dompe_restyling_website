<%@ Page Language="VB" debug="true"  validaterequest="false"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Function ReadOptionExtra() As ContentOptionCollection
        Dim optionManger As New ContentOptionService
        Dim opCollection As New ContentOptionCollection
        Dim opSearcher As New ContentOptionSearcher
             
        opCollection = optionManger.Read(opSearcher)
        Return opCollection
    End Function
    
   
    ' Legge la descrizione del OptionLabel dato l'id
    Function ReadLabel(ByVal idOption As Int32) As ContentOptionValue
        Dim optionManger As New ContentOptionService
        Dim contentOptionValue As ContentOptionValue
        
        contentOptionValue = optionManger.Read(New ContentOptionIdentificator(idOption))
        Return contentOptionValue
    End Function
    
    
    Sub Page_Load()
       
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("sites") Is Nothing Then
            arrMatch = Request("sites").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("sites")).Label & "_" & Request("sites")
        End If
        
        repOptionExtra.DataSource = ReadOptionExtra()
        repOptionExtra.DataBind()
    End Sub
	
    Function setVisibility(ByVal id As Int32) As String
        Dim optionCollection = ReadOptionExtra()
        If optionCollection Is Nothing OrElse optionCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal Sender As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
		
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "`") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
	function fDisable(id as string) as string
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "        
        End If
        Return ""
    End Function
	
	function fChecked(id as string) as string
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
	end function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
<head>
	<title>Selezionare il box</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<style type="text/css" media="all">
	    a {text-decoration:none}
		html, body { margin:0; padding:0; }
		html, body, td, th { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; }
		.dGroup {
		margin:5px;
	}

	.dControl {	
		margin:5px;
		padding-left:15px;
	</style>	
</head>

<body>
	<!--<img src="../HP3Image/Logo/hp3logo.gif" />-->
	
	<form id="Form1" runat="server">
	    <asp:textbox id="txtHidden" runat="server" width="100%" style="display:none" />
				<asp:Repeater ID="repOptionExtra" runat="Server">
				<HeaderTemplate><div class="title">Option Extra List</div><hr /></HeaderTemplate>
					<itemtemplate>							
						<div class="dControl" >
							<span  <%#fDisable(container.DataItem.Key.id)%> >
							    <%If Not bSingleSel Then%>
								    <input <%#fChecked(container.DataItem.Key.id)%> name = 'chk_<%#Container.DataItem.Label.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.Label = "", "[Descrizione Assente]", Container.DataItem.Label)%>&nbsp;
								<%Else%>
								    <input onclick="gestClick(this)" <%#fChecked(container.DataItem.Key.id)%>  name = 'chk_<%#Container.DataItem.Label.Replace("'", "`")%>_<%#Container.DataItem.Key.id%>' type="checkbox"/>&nbsp;<%#IIf(Container.DataItem.Label = "", "[Descrizione Assente]", Container.DataItem.Label)%>&nbsp;
								<%end if%>
							</span>
						</div>
					</itemtemplate>
				</asp:Repeater>
					<hr />
			          
        <div  style="text-align:center">
   			<input type="button" class="button" onClick="window.close()" value="Annulla"/>
			<asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Salva" runat="server"/>
	    </div>
</form>
</body>
</html>

<script>

    var ItemsCount = 2
    if (ItemsCount == 1)
        {
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc)
	{
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+')  
			{
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else 
			{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	}
	
	function splitName(src)
	    {
	        if (src != '')
	            {
	                
	                var arr = src.split('_')
	                
	                return arr[2];
	            }	        
	    }
	    
	    
	function gestClick(obj,label)
	    {	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	        var ochk
	        if ((oText.value != '') && (oText.value != obj.Name))
	            {   	                	                
	                ochk = document.getElementById (oText.value)
	                if (ochk != null) ochk.checked = false;
	            }
	            
	        oText.value = obj.Name
	    }
</script>