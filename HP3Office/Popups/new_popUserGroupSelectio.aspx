<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat ="server">
    Private _oUserGroupManager As New UserGroupManager

    Sub Page_Load()
        If Not (Page.IsPostBack) Then
            ReadUserGroup()
        End If
    End Sub
    
    Sub ReadUserGroup()
        Dim userGroupCollection As UserGroupCollection
        Dim oUserGroupSearcher As New UserGroupSearcher()
        
        _oUserGroupManager.Cache = False
        userGroupCollection = _oUserGroupManager.Read(oUserGroupSearcher)
        gridList.DataSource() = userGroupCollection
        gridList.DataBind()
    End Sub
    
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        Dim oUserGroupSearcher = New UserGroupSearcher()
              
        BindGrid(objGrid, oUserGroupSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As UserGroupSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New UserGroupSearcher
        End If
        
        Dim _oUserGroupColl As UserGroupCollection = _oUserGroupManager.Read(Searcher)
        If Not (_oUserGroupColl Is Nothing) Then
            
            objGrid.DataSource = _oUserGroupColl
            objGrid.DataBind()
        End If
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Group User List</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

</head>
<body style="text-align:left">
    <form id="Form1" runat="server">
	    <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="TopAndBottom"  
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                            >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                  <%-- OnCheckedChanged="ViewUser()--%>
                                       <asp:RadioButton ID="my_chk" runat="server"   /> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" >
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Description" >
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField>  
                         </Columns>
            </asp:gridview>
        </div> 
    </form>
</body>
</html>

	