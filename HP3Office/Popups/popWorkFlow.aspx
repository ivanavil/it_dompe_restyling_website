<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing"%>
<%@ Import Namespace="Healthware.HP3.Core.ContentPublishing.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<%@ Register TagPrefix="HP3" TagName="ContentsWorkFlow"   Src="~/hp3Office/HP3Common/ContentsWorkFlow.ascx"%> 

<script runat="Server">
    Private workFlowManager As New WorkFlowManager
    Private stepSearcher As StepSearcher = Nothing
    Private stepUserSearcher As StepUserSearcher = Nothing
    Private StepActionSearcher As StepActionSearcher = Nothing
    Private utility As New WebUtility
    Private stepsNotConfigurate As Integer = 0
    
    
    
    Private _strDomain As String = ""
    Private _language As String = ""
    Private _strJS As String = ""
    Private _status As String = ""
    Private _user As Boolean = False
    Private _stepUserId As Integer = Nothing
    Private _noActions As Boolean = False
    Private _isApproved As Boolean = False
    Private _note As String = Nothing
    Private _selGridRow As GridViewRow
    Dim LangId, ContentId, ThemeId As String
    
          
    Private Property isUserLog() As Boolean
        Get
            Return _user
        End Get
        Set(ByVal value As Boolean)
            _user = value
        End Set
    End Property
    
    Private Property StepUserId() As Integer
        Get
            Return ViewState("stepUserId")
        End Get
        Set(ByVal value As Integer)
            ViewState("stepUserId") = value
        End Set
    End Property
   
    Private Property PrimaryKey() As Integer
        Get
            Return ViewState("primaryKey")
        End Get
        Set(ByVal value As Integer)
            ViewState("primaryKey") = value
        End Set
    End Property
    
    Private Property noActions() As Boolean
        Get
            Return ViewState("noActions")
        End Get
        Set(ByVal value As Boolean)
            ViewState("noActions") = value
        End Set
    End Property
    
    Private Property isApproved() As Boolean
        Get
            Return _isApproved
        End Get
        Set(ByVal value As Boolean)
            _isApproved = value
        End Set
    End Property
        
    Private Property Note() As String
        Get
            Return _note
        End Get
        Set(ByVal value As String)
            _note = value
        End Set
    End Property
    
    Enum UserStepStatus
        NotApproved = 0
        Approved = 1
        Null = 3
    End Enum
    
    Enum ContentWFStatus
        Approved = 1
        ToBeApproved = 2
        NotApproved = 3
    End Enum
    
    Sub page_load()
        If Not Request("HiddenId") Is Nothing Then
            ContentId = Request("HiddenId")
        End If
        
        If Not Request("LangId") Is Nothing Then
            LangId = Request("LangId")
        End If
        
        If Not Request("ThemeId") Is Nothing Then
            ThemeId = Request("ThemeId")
        End If
        
        If Not (Page.IsPostBack()) Then
            Dim contentValue As ContentValue = Me.BusinessContentManager.Read(New ContentIdentificator(ContentId, LangId))
            
            If Not (contentValue Is Nothing) Then
                PrimaryKey() = contentValue.Key.PrimaryKey
                contTitle.Text = contentValue.Title
                contId.Text = contentValue.Key.Id
                contPK.Text = contentValue.Key.PrimaryKey
                contGK.Text = contentValue.Key.IdGlobal
            End If
            
            
            LoadWorkFlowSteps()
        End If
    End Sub

    'recupera tutti gli steps relativi al workFlow
    'Assunzione: il WorkFlow � unico per tutti i content
    Sub LoadWorkFlowSteps()
        Dim workFlowId As Integer = ReadWorkFlow()
        stepSearcher = New StepSearcher
        
        If (workFlowId <> 0) Then stepSearcher.KeyWorkFlow.Id = workFlowId
        Dim stepColl As StepCollection = workFlowManager.Read(stepSearcher)
        
        gdw_stepList.DataSource = stepColl
        gdw_stepList.DataBind()
    End Sub
 
    'controlla se esiste un workflow associato al tema
    'se esiste restituisce il suo id
    Function ReadWorkFlow() As Integer
        Dim themeValue As ThemeValue = Me.BusinessThemeManager.Read(New ThemeIdentificator(ThemeId))
       
        If Not (themeValue Is Nothing) Then
            Return themeValue.KeyWorkFlow.Id
        End If
        
        Return 0
    End Function
          
    'si occupa dell'edit di uno step
    Sub EditRow(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "EditItem") Then
            
            Dim stepId As Integer = e.CommandArgument
            Dim stepIdent As New StepIdentificator()
            stepIdent.Id = stepId
            
            Dim stepValue As StepValue = workFlowManager.Read(stepIdent)
            If Not (stepValue Is Nothing) Then
                stepUserSearcher = New StepUserSearcher
                stepUserSearcher.KeyStep.Id = stepId
                stepUserSearcher.KeyUser.Id = Me.ObjectTicket.User.Key.Id
                
                Dim stUserColl As StepUserCollection = workFlowManager.Read(stepUserSearcher)
            
                If Not (stUserColl Is Nothing) AndAlso (stUserColl.Count > 0) Then
                    StepUserId() = stUserColl(0).Key.Id
                    Dim isApproved As Integer = getUserChoice(stUserColl(0))
                    
                    noActions() = False
                    If (isApproved = UserStepStatus.Null) Then noActions() = True
                    
                    For Each item As GridViewRow In gdw_stepList.Rows
                        Dim ddl As DropDownList = item.Cells(0).FindControl("dwlStepStatus")
                        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(isApproved))
                    Next
                End If
            End If
            
        End If
    End Sub
   
    'gestisce l'edit dello status dello step, visualizzando la dwl che gestisce l'update
    Sub GridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If e.CommandName = "EditItem" Then
            Dim argument As String = e.CommandArgument
            
            Dim arrayArgument As String() = argument.Split("|")
            Dim index As Integer = arrayArgument(1)
            Dim stepId As Integer = arrayArgument(0)
            
            Dim row As GridViewRow = gdw_stepList.Rows(index)

            Dim stepIdent As New StepIdentificator()
            stepIdent.Id = stepId
            
            Dim stepValue As StepValue = workFlowManager.Read(stepIdent)
            If Not (stepValue Is Nothing) Then
                stepUserSearcher = New StepUserSearcher
                stepUserSearcher.KeyStep.Id = stepId
                stepUserSearcher.KeyUser.Id = Me.ObjectTicket.User.Key.Id
                
                Dim stUserColl As StepUserCollection = workFlowManager.Read(stepUserSearcher)
            
                If Not (stUserColl Is Nothing) AndAlso (stUserColl.Count > 0) Then
                    StepUserId() = stUserColl(0).Key.Id
                    Dim isApproved As Integer = getUserChoice(stUserColl(0))
                    
                    noActions() = False
                    If (isApproved = UserStepStatus.Null) Then noActions() = True
                                     
                    Dim ddLs As DropDownList = row.Cells(0).FindControl("dwlStepStatus")
                    Dim txtNote As TextBox = row.Cells(0).FindControl("Note")
                    Dim litNote As Literal = row.Cells(0).FindControl("litNote")
                    Dim litStat As Literal = row.Cells(0).FindControl("litStatus")
                    Dim btnBack As ImageButton = row.Cells(0).FindControl("btnBack")
                    Dim btnSave As ImageButton = row.Cells(0).FindControl("btnSave")
                    
                    ddLs.SelectedIndex = ddLs.Items.IndexOf(ddLs.Items.FindByValue(isApproved))
                    
                    txtNote.Text = Note()
                    
                    ddLs.Visible = True
                    txtNote.Visible = True
                    litNote.Visible = True
                    litStat.Visible = True
                    btnBack.Visible = True
                    btnSave.Visible = True
                End If
            End If
        End If
        
        If e.CommandName = "UpStatus" Then
            Dim argument As String = e.CommandArgument
            
            Dim arrayArgument As String() = argument.Split("|")
            Dim index As Integer = arrayArgument(1)
            Dim stepId As Integer = arrayArgument(0)
            
            Dim row As GridViewRow = gdw_stepList.Rows(index)
            
            Dim ddLs As DropDownList = row.Cells(0).FindControl("dwlStepStatus")
            Dim txtNote As TextBox = row.Cells(0).FindControl("Note")
            
            UpdateStatus(ddLs, txtNote)
        End If
        
        If e.CommandName = "GoBack" Then
            Dim argument As String = e.CommandArgument
            
            Dim arrayArgument As String() = argument.Split("|")
            Dim index As Integer = arrayArgument(1)
            Dim stepId As Integer = arrayArgument(0)
           
            Dim row As GridViewRow = gdw_stepList.Rows(index)
            
            Dim ddLs As DropDownList = row.Cells(0).FindControl("dwlStepStatus")
            Dim txtNote As TextBox = row.Cells(0).FindControl("Note")
            Dim litNote As Literal = row.Cells(0).FindControl("litNote")
            Dim litStat As Literal = row.Cells(0).FindControl("litStatus")
            Dim btnBack As ImageButton = row.Cells(0).FindControl("btnBack")
            Dim btnSave As ImageButton = row.Cells(0).FindControl("btnSave")
                    
            ddLs.Visible = False
            txtNote.Visible = False
            litNote.Visible = False
            litStat.Visible = False
            btnBack.Visible = False
            btnSave.Visible = False
        End If
    End Sub

    'gestisce l'update dello stato dello step
    Sub UpdateStatus(ByVal dwlStatus As DropDownList, ByVal note As TextBox)
        Dim stepActionValue As New StepActionValue
        'Dim dwlStatus As DropDownList = CType(sender, DropDownList)
       
        'devo fare una create
        If (noActions()) Then
            If (dwlStatus.SelectedValue <> -1) Then
                stepActionValue.KeyStepUser.Id = StepUserId()
                stepActionValue.ContentPK = PrimaryKey()
                stepActionValue.DateAction = Date.Now
                stepActionValue.Note = note.Text
                stepActionValue.IsApproved = dwlStatus.SelectedValue
        
                workFlowManager.Create(stepActionValue)
                
                'recupera lo status del workflow del content
                Dim wfStatus As ContentWFStatus = CheckWFStatus()
                'setta lo status attuale del workflow del content
                setContentWFStatus(wfStatus)
                
                dwlStatus.Visible = False
            Else
                CreateJsMessage("Please, select a status.")
            End If
        Else 'devo fare un update
            If (dwlStatus.SelectedValue <> -1) Then
                stepActionValue.KeyStepUser.Id = StepUserId()
                stepActionValue.ContentPK = PrimaryKey()
                stepActionValue.DateAction = Date.Now
                stepActionValue.IsApproved = dwlStatus.SelectedValue
                stepActionValue.Note = note.Text
                
                workFlowManager.Update(stepActionValue)
                
                'recupera lo status del workflow del content
                Dim wfStatus As ContentWFStatus = CheckWFStatus()
                'setta lo status attuale del workflow del content
                setContentWFStatus(wfStatus)
                
                dwlStatus.Visible = False
            Else
                CreateJsMessage("Please, select a status.")
            End If
        End If
               
        GoBack()
    End Sub
 
    'riporta il controllo nell'edit del content e visualizza il workflow
    Sub GoBack(ByVal sender As Object, ByVal e As EventArgs)
        GoBack()
    End Sub
    
    Sub GoBack()
        LoadWorkFlowSteps()
    End Sub
           
    'setta il workflow status del content
    Function setContentWFStatus(ByVal wfStatus As Integer) As Boolean
        Dim contentident As New ContentIdentificator()
        'Dim contentId As Integer = Request("C")
        'Dim lang As Integer = Request("L")
        
        Return Me.BusinessContentManager.Update(New ContentIdentificator(contentId, LangId), "ApprovalStatus", wfStatus)
    End Function
        
    'restitusce lo stato del workflow del contente
    'Approved, se tutte gli step del workflow sono state approvate
    'Not Approved, se esiste almeno uno step del workflow che non � stato approvato
    'To be Aproved, se esiste almeno uno step del workflow � ancora in approvazione
    Function CheckWFStatus() As ContentWFStatus
        stepSearcher = New StepSearcher
        
        Dim stepColl As StepCollection = workFlowManager.Read(stepSearcher)
        workFlowManager.Cache = False
        
        For Each stepV As StepValue In stepColl
            Dim stepValue As StepValue = workFlowManager.CheckStepStatus(PrimaryKey(), stepV.Key.Id)
        
            If (stepValue.Status = Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.NotApproved) Then
                Return ContentWFStatus.NotApproved
            ElseIf (stepValue.Status = Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.ToApproved) Then
                Return ContentWFStatus.ToBeApproved
            End If
        Next
             
        Return ContentWFStatus.Approved
    End Function
      
    'restituisce tutti gli utenti che sono coinvolti nello Step
    Function GetUsers(ByVal stepV As StepValue) As String
        stepUserSearcher = New StepUserSearcher
                
        stepUserSearcher.KeyStep.Id = stepV.Key.Id
        Dim stUserColl As StepUserCollection = workFlowManager.Read(stepUserSearcher)
       
        Dim strUsers As String = ""
        
        isUserLog() = False
        If Not (stUserColl Is Nothing) AndAlso (stUserColl.Count > 0) Then
            For Each stepUser As StepUserValue In stUserColl
                Dim userValue As UserValue = Me.BusinessUserManager.Read(New UserIdentificator(stepUser.KeyUser.Id))
               
                'se l'utente loggato � nella lista di chi pu� svolgere azioni sullo step 
                If (stepUser.KeyUser.Id = Me.ObjectTicket.User.Key.Id) Then isUserLog() = True
                                   
                strUsers = strUsers + "<img id=""imgUser"" src=""/HP3Office/HP3Image/ico/content_user.gif"" alt=""""/>" + userValue.CompleteName + " "
                Dim choise As Integer = getUserChoice(stepUser)
                
                If (choise = UserStepStatus.Approved) Then
                    strUsers = strUsers + " " + "<img id=""imgWFStatus"" src=""/HP3Office/HP3Image/Ico/approved.gif"" title=""Approved"" alt=""""/>" + " "
                ElseIf (choise = UserStepStatus.NotApproved) Then
                    strUsers = strUsers + " " + "<img id=""imgWFStatus"" src=""/HP3Office/HP3Image/Ico/notapproved.gif"" title=""Not Approved"" alt=""""/>" + " "
                ElseIf (choise = UserStepStatus.Null) Then
                    strUsers = strUsers + " "
                End If
                
                If (Note() <> String.Empty) Then
                    strUsers = strUsers + "<img id=""imgUserNote"" src=""/HP3Office/HP3Image/Ico/contentwf_usernote.gif"" title=""" & Note() & """ alt=""""/>" + " "
                End If
            Next
        End If
       
        Return strUsers
    End Function
    
    'restituisce la decisione(approved/not approved) presa dall'utente sullo step
    Function getUserChoice(ByVal stepuser As StepUserValue) As Integer
        StepActionSearcher = New StepActionSearcher
        
        StepActionSearcher.ContentPK = PrimaryKey()
        StepActionSearcher.KeyStepUser.Id = stepuser.Key.Id
       
        workFlowManager.Cache = False
        Dim stepActionColl As StepActionCollection = workFlowManager.Read(StepActionSearcher)
              
        Note() = Nothing
        If Not (stepActionColl Is Nothing) AndAlso (stepActionColl.Count > 0) Then
            Note() = stepActionColl(0).Note
                            
            Select Case stepActionColl(0).IsApproved
                Case False
                    Return UserStepStatus.NotApproved
                Case True
                    Return UserStepStatus.Approved
            End Select
        End If
        
        Return UserStepStatus.Null
    End Function
    
    'restituisce l'utente coinvolto nello step
    Function GetUserStep(ByVal stepV As StepValue) As String
        stepUserSearcher = New StepUserSearcher
        
        stepUserSearcher.KeyStep.Id = stepV.Key.Id
        Dim stUserColl As StepUserCollection = workFlowManager.Read(stepUserSearcher)
        
        isUserLog() = False
        If Not (stUserColl Is Nothing) AndAlso (stUserColl.Count > 0) Then
            Dim userId As Integer = stUserColl(0).KeyUser.Id
                       
            Dim userValue As UserValue = Me.BusinessUserManager.Read(New UserIdentificator(userId))
       
            'se l'utente loggato � nella lista di chi pu� svolgere azioni sullo step 
            If (userId = Me.ObjectTicket.User.Key.Id) Then isUserLog() = True
            Return userValue.CompleteName
        End If
    
        Return ""
    End Function
    
     
    'restituisce lo status dello Step (non approvato, approvato, da approvare, non configurato)
    Function getStepStatus(ByVal stepV As StepValue) As String
        If (PrimaryKey() <> 0) Then
            workFlowManager.Cache = False
            Dim stepValue As StepValue = workFlowManager.CheckStepStatus(PrimaryKey(), stepV.Key.Id)
                                    
            If Not (stepValue Is Nothing) Then
                Select Case stepValue.Status
                    Case Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.Approved
                        isApproved() = True
                        Return "<img id=""imgWFStatus"" src=""/HP3Office/HP3Image/Ico/contentwf_approval.gif"" title=""Approved"" alt=""""/>"
                                        
                    Case Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.NotApproved
                        isApproved() = False
                        Return "<img id=""imgWFStatus"" src=""/HP3Office/HP3Image/Ico/contentwf_notapproval.gif"" title=""Not Approved"" alt=""""/>"
                    
                    Case Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.ToApproved
                        isApproved() = False
                        Return "<img id=""imgWFStatus"" src=""/HP3Office/HP3Image/Ico/contentwf_tobeapproval.gif"" title=""To be Approved"" alt=""""/>"
                        
                    Case Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.NotConfigurated
                        isApproved() = False
                        Return "Not Configurated"
                End Select
            End If
        End If
       
        Return ""
    End Function
           
    'gestisce la visibilit� del bottone di Edit sullo step.
    'True, se l'utente loggato � uno degli attori sullo step.
    'False, se l'utente loggato � uno degli attori sullo step oppure uno degli step precedenti non � stato approvato.
    Function getVisibility(ByVal stepV As StepValue) As Boolean
        Dim userLog As Integer = Me.ObjectTicket.User.Key.Id
         
        Dim prevStepStatus As Boolean = CheckPreviousStep(stepV)
        
        'se l'utente loggato � coinvolto negli step del workflow
        'e lo step precedente � stato approvato
        If (isUserLog()) And (prevStepStatus) And (Not isApproved()) Then
            Return True
        End If
        
        Return False
    End Function
    
    'controlla che tutti lo step precedente � stato approvato
    Function CheckPreviousStep(ByVal stepV As StepValue) As Boolean
        If stepV.Order <= 1 Then Return True
        
        Dim prevOrder As Integer = stepV.Order - 1
        
        stepSearcher = New StepSearcher
        stepSearcher.Order = prevOrder
        Dim stepColl As StepCollection = workFlowManager.Read(stepSearcher)
        
        If Not (stepColl Is Nothing) AndAlso (stepColl.Count > 0) Then
            Dim prevStep As StepValue = stepColl(0)
            
            workFlowManager.Cache = False
            Dim stepValue As StepValue = workFlowManager.CheckStepStatus(PrimaryKey(), prevStep.Key.Id)
                       
            If (stepValue.Status = Healthware.HP3.Core.ContentPublishing.ObjectValues.StepValue.StatusEnum.Approved) Then
                Return True
            Else
                Return False
            End If
        End If
    End Function
            
    'crea un alert con un messaggio di conferma
    Sub CreateJsMessage(ByVal strMessage As String)
        _strJS = "alert(""" & strMessage & """)"
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Content Approval WorkFlow</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="wfForm" runat="server">
	        <table class="form">
                <tr>
                   <td class="menuBar" valign="middle"><span class="title"><asp:Label id="contTitle" ToolTip ="Title" runat="server"/></span>&nbsp;|</td>
                   <td class="menuBar" valign="middle"><span class="title"><asp:Label id="contId" ToolTip ="Id" runat="server"/></span>&nbsp;|</td>
                   <td class="menuBar" valign="middle"><span class="title"><asp:Label ToolTip ="PrimaryKey" id="contPK" runat="server"/></span>&nbsp;|</td>
                   <td class="menuBar" valign="middle"><span class="title"><asp:Label ToolTip ="Global Key" id="contGK" runat="server"/></span>&nbsp;|</td>
                </tr>
            </table>
	        <div id="contentWF" runat="server" class="wfClass" style="margin-top:10px; margin-left:2px">
                <asp:gridview id="gdw_stepList" 
                  runat="server"
                  AutoGenerateColumns="false" 
                  GridLines="None" 
                  Width="100%"
                  AlternatingRowStyle-BackColor="#E9EEF4"
                  AllowPaging="true"
                  AllowSorting="true"
                  PageSize ="20"
                  OnRowCommand="GridView_RowCommand"
                  emptydatatext="No item available">
                  <Columns >
                     <asp:TemplateField HeaderStyle-Width="2%" HeaderStyle-BackColor="#CCD7E7">
                           <ItemTemplate><%#getStepStatus(Container.DataItem)%></ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Approval WorkFlow" HeaderStyle-BackColor="#CCD7E7">
                           <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Description")%>
                            <table class="form">
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><asp:Literal id="litStatus" Text="Status:" runat="server" Visible="false"/></td>
                                    <td>
                                       <asp:DropDownList id="dwlStepStatus" runat="server" Visible="false" >
                                            <asp:ListItem Text="--Select Status--" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Approved" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Not Approved" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                      </td>
                                 </tr>
                                 <tr>
                                    <td><asp:Literal id="litNote" Text="Note:" runat="server" Visible="false"/></td>
                                    <td><asp:TextBox id="Note" runat="server"  Rows="3"  width="195px" TextMode ="MultiLine" Visible="false"/></td>
                                 </tr>  
                                    
                              </table>
                              <div id="listBt" style="text-align:right">
                                   <asp:ImageButton ID="btnBack"  runat="server" ToolTip ="Go Back"  CommandName="GoBack" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "|" & Container.DataItemIndex %>' ImageUrl="~/hp3Office/HP3Image/Button/archive_content.gif" Visible="false" /> <asp:ImageButton ID="btnSave"   CommandName="UpStatus" runat="server" ToolTip ="Save" ImageUrl="~/hp3Office/HP3Image/Button/save_content.gif" Visible="false" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "|" & Container.DataItemIndex %>'/>
                              </div>
                             </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Width="35%">
                              <ItemTemplate><%#GetUsers(Container.DataItem)%></ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Width="2%">
                            <ItemTemplate>
                                  <asp:ImageButton ID="lnkSelect" runat="server" ToolTip ="Edit item" ImageUrl="~/hp3Office/HP3Image/Ico/content_edit.gif"  CommandName ="EditItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id") & "|" & Container.DataItemIndex %>'  Visible='<%#getVisibility(Container.DataItem)%>' />
                            </ItemTemplate>
                     </asp:TemplateField>
                  </Columns>
                  </asp:gridview>
              </div>
        </form>
    </body>
</html>

