<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat ="server">    
    Private _oUserGroupManager As New UserGroupManager
    Private _accService As New AccessService()
    Private arrUser As String()
    Private strJs As String
    
    'Dim objCrmGroup As New Group_Users()
    Dim curId As String
    Dim curNodeSel As String
    Private curItemSel As String
    Dim curFFId As String
    
    Property Nodes()
        Get
            Return ViewState("Nodes")
        End Get
        
        Set(ByVal value)
            Try
                           
                Dim arr As ArrayList
                
                If ViewState("Nodes") Is Nothing Then
                    arr = New ArrayList
                Else
                    arr = ViewState("Nodes")
                End If
             
                If Not arr.Contains(CStr(value)) Then
                    arr.Add(CStr(value))
                 
                    ViewState("Nodes") = arr
                End If
                
            Catch ex As Exception
                Response.Write(ex.ToString)
            End Try
        End Set
    End Property
    
    Sub LoadTree()
        
        If pMain.Controls.Count > 0 Then pMain.Controls.Clear()
        pMain.Controls.Add(getGruppi(curId))
        
    End Sub
    
    Function getGruppi(ByVal id As Int32, Optional ByVal bIsUser As Boolean = True, Optional ByVal curKey As String = "") As Panel
        Dim objPanel As New Panel
        Dim innerP As PlaceHolder
        
        Dim bExist As Boolean
        
        Dim ds As Data.DataSet
        Dim objLinkText As LinkButton
        Dim objLinkSign As LinkButton
        Dim objLit As Label
        Dim innerPanel As New Panel
        
        Dim objAnchor As HtmlAnchor
        Dim oRadio As HtmlInputRadioButton
        Dim testLabel As New Label
        Dim Key As String
     
        Dim colGroups As UserGroupCollection
        Dim colUser As UserCollection
        Dim oUserGroupValue As UserGroupValue
        Dim oUserValue As UserValue
        
        '*************************
        If bIsUser Then
            'Se l'id passato fa riferimento ad un utente...devo prelevare tutti  i gruppi a cui appartiene
            'objCrmGroup.GetGroups(id, 2)
            colGroups = ReadGroupsByUser(New UserIdentificator(id))
            'ds = objCrmGroup.DataSet                     
        Else
            'Altrimenti se l'id fa riferimento ad un gruppo, prelevo tutti i gruppi e tutti gli utente collegati
            'objCrmGroup.GetSons(id, curId, 2)
           
            colUser = ReadUsersByGroup(New UserGroupIdentificator(id))           
            colGroups = ReadGroupsByGroup(New UserGroupIdentificator(id))            
            'ds = objCrmGroup.DataSet                        
        End If
        '*************************
        
        objPanel.CssClass = "dGroup"
 
        'Dim objRow As Data.DataRow
        If Not colGroups Is Nothing Then
            
            If curKey = "" Then curKey = id
            objPanel.ID = "mainPanel_" & curKey
            
            'For Each objRow In ds.Tables(0).Rows
            For Each oUserGroupValue In colGroups
                
                'Se si tratta di un gruppo                   
                Key = curKey & "_" & "1_" & oUserGroupValue.Key.Id  'objRow("id")
                objPanel.Controls.Add(LoadGruppo(Key, oUserGroupValue.Description)) 'objRow("Descrizione")))
                
                bExist = ExistsNodo(Key)
                
                innerP = New PlaceHolder
                innerP.ID = "p_" & Key
                
                If bExist Then                    
                    innerP.Controls.Add(getGruppi(oUserGroupValue.Key.Id, False, Key))
                End If
                
                objPanel.Controls.Add(innerP)
            Next
        End If
        
        If Not colUser Is Nothing Then
            For Each oUserValue In colUser
                Key = curKey & "_" & "2_" & oUserValue.Key.Id
                objPanel.Controls.Add(LoadUtente(Key, oUserValue.Name & " " & oUserValue.Surname, oUserValue.Email.Item(0)))
                
                bExist = ExistsNodo(Key)
                
                innerP = New PlaceHolder
                innerP.ID = "p_" & Key
                
                If bExist Then
                    innerP.Controls.Add(getGruppi(oUserValue.Key.Id, False, Key))
                End If
                
                objPanel.Controls.Add(innerP)
            Next
        End If
        
        Return objPanel
    End Function
    
    Function ExistsNodo(ByVal id As String) As Boolean
        If Nodes Is Nothing Then Return False
            
        Return Nodes.Contains(id)
    End Function
    
    Function LoadGruppo(ByVal id As String, ByVal Valore As String) As Panel
        Dim objLinkText As LinkButton
        Dim objLinkSign As LinkButton
        Dim objLit As Label
        Dim objImg As ImageButton
        Dim fileName As String
        Dim mainPanel As New Panel
        Dim innerP As PlaceHolder
        Dim objAnchor As HtmlAnchor
        Dim oRadio As CheckBox
        Dim objLink As Image
        Try
            
            mainPanel.ID = "div_" & id
            mainPanel.CssClass = "dControl"
           
            'Ancora***********************
            objAnchor = New HtmlAnchor
            objAnchor.Name = "#" & id
            mainPanel.Controls.Add(objAnchor)
            '*****************************         
            
            objLinkText = New LinkButton
            objLinkSign = New LinkButton
                
            
            
            'Segno**************************                
            objImg = New ImageButton
                
            If ExistsNodo(id) Then
                fileName = "../HP3Image/ico/ico_15x_minus.gif"
            Else
                fileName = "../HP3Image/ico/ico_15x_plus.gif"
            End If
                
            objImg.ID = "Sign_" & id
            objImg.ImageUrl = fileName
            objImg.CssClass = "imgPlusMinus"
            objImg.CommandArgument = id
            objImg.CommandName = Valore
            
            objImg.Attributes.Add("onMouseOver", "selDiv(this,'" & id & "')")
            objImg.Attributes.Add("onMouseOut", "UnselDiv()")
                
            AddHandler objImg.Click, AddressOf GestImageClick
            mainPanel.Controls.Add(objImg)
            '*****************************
            
            
            
            'Immagine********************* 
            objImg = New ImageButton
            If ExistsNodo(id) Then
                fileName = "../HP3Image/ico/ico_16x_folder_open.gif"
            Else
                fileName = "../HP3Image/ico/ico_16x_folder_closed.gif"
            End If
            objImg.ImageUrl = fileName
            objImg.ID = "img_" & id
            objImg.CssClass = "image"
				
            objImg.Attributes.Add("onMouseOver", "selDiv(this,'" & id & "')")
            objImg.Attributes.Add("onMouseOut", "UnselDiv()")
            objImg.CommandArgument = id
            objImg.CommandName = Valore
            AddHandler objImg.Click, AddressOf GestImageClick
            mainPanel.Controls.Add(objImg)
            '*****************************
            
            'Radio button*****************
            If ExistsNodo(id) Then
                oRadio = New CheckBox
                oRadio.ID = "inCheck_" & id
                oRadio.Attributes.Add("onclick", "GroupRadioClick(this,'" & id & "')")
                mainPanel.Controls.Add(oRadio)
            End If
            '*****************************
            
            'Testo************************                
            objLinkText.Text = " " & Valore
            objLinkText.ID = "txtValore_" & id
            objLinkText.CssClass = "itemText"
            objLinkText.CommandArgument = id
            objLinkText.CommandName = Valore
          
            objLinkText.Attributes.Add("onMouseOver", "selDiv(this,'" & id & "')")
            objLinkText.Attributes.Add("onMouseOut", "UnselDiv()")
                
            If curNodeSel = id Then
                mainPanel.CssClass = "dControlSelected"
            End If
                
            AddHandler objLinkText.Click, AddressOf GestClick
            mainPanel.Controls.Add(objLinkText)
            '*****************************        
            
            'PlaceHolder           
            innerP = New PlaceHolder
            mainPanel.Controls.Add(innerP)
            '******************************
            Return mainPanel
            
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    Sub GestClick(ByVal s As Object, ByVal e As EventArgs)
        
        If ExistsNodo(s.commandargument) Then
            CType(Nodes, ArrayList).Remove(s.commandargument)
        Else
            Nodes = s.commandargument
        End If
        
        curNodeSel = s.commandargument
        curItemSel = s.commandname
        
        LoadTree()
    End Sub
    
    Sub GestImageClick(ByVal s As Object, ByVal e As ImageClickEventArgs)
        If ExistsNodo(s.commandargument) Then
            CType(Nodes, ArrayList).Remove(s.commandargument)
        Else
            Nodes = s.commandargument
        End If
        
        curNodeSel = s.commandargument
        curItemSel = s.commandname
        
        LoadTree()
    End Sub
    
    
    Function LoadUtente(ByVal id As String, ByVal Valore As String, Optional ByVal email As String = "") As Panel
        Dim objLinkText As HtmlAnchor
        Dim objHidden As TextBox
        Dim objImg As HtmlImage
        Dim fileName As String
        Dim mainPanel As New Panel
        Dim innerP As PlaceHolder
        Dim objAnchor As HtmlAnchor
        Dim oRadio As HtmlInputRadioButton
        Dim chk As CheckBox
        Dim objLink As Image
        Try
                      
            
            mainPanel.ID = "div_" & id
            mainPanel.CssClass = "dControl"
           
            'Ancora***********************
            objAnchor = New HtmlAnchor
            objAnchor.Name = "#" & id
            mainPanel.Controls.Add(objAnchor)
            '*****************************       
            
            objLinkText = New HtmlAnchor
            objImg = New HtmlImage
           
            fileName = "../HP3Image/ico/ico_16x_file.gif"
				
            Dim img As New Image
				
            img.ImageUrl = "../HP3Image/ico/spacer.gif"
            img.Width = Unit.Pixel(15)
            img.CssClass = "imgPlusMinus"
            mainPanel.Controls.Add(img)
            
            chk = New CheckBox
            chk.ID = "rd_" & id
            chk.Attributes.Add("onclick", "clickRadio(this,'" & id & "',true)")
            
            'If Contains(id) AndAlso Not chk.Checked Then                
            'strJs += "clickRadio(document.getElementById('rd_" & id & "'),'" & id & "',false);" & vbCrLf
            'End If
            
            mainPanel.Controls.Add(chk)
            
            objImg.Src = "../HP3Image/ico/ico_16x_new_folder.gif"
            objImg.ID = "img_" & id
            objImg.Attributes.Add("onMouseOver", "selDiv(this,'" & id & "')")
            objImg.Attributes.Add("onMouseOut", "UnselDiv()")
            objImg.Attributes.Add("onclick", "clickRadio(document.getElementById('rd_" & id & "'),'" & id & "',false)")
            'objImg.Class = "image"
            mainPanel.Controls.Add(objImg)
            '**********************************
                
            objImg.Src = fileName
            objImg.ID = "img_" & id
            'objImg.CssClass = "image"
            mainPanel.Controls.Add(objImg)
            '*********************************
                            
            'Testo************************                
            objLinkText.InnerText = " " & Valore
            objLinkText.ID = "txtValore_" & id
                      
            objLinkText.Attributes.Add("onMouseOver", "selDiv(this,'" & id & "')")
            objLinkText.Attributes.Add("onMouseOut", "UnselDiv()")
            objLinkText.Attributes.Add("onclick", "clickRadio(document.getElementById('rd_" & id & "'),'" & id & "',false)")
            objHidden = New TextBox
            objHidden.Text = email
            objHidden.CssClass = "Hide"
            objHidden.ID = "txtHidden_" & id
            
            If curNodeSel = id Then
                curNodeSel = curFFId
                mainPanel.CssClass = "dControlSelected"
            End If
                
                
            'AddHandler objLinkText.Click, AddressOf GestClick
            mainPanel.Controls.Add(objLinkText)
            mainPanel.Controls.Add(objHidden)
            '*****************************   
            
            'PlaceHolder           
            innerP = New PlaceHolder
            mainPanel.Controls.Add(innerP)
            '******************************
            Return mainPanel
            
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    Function Contains(ByVal valore As String) As Boolean
        Try
            Dim arr As String() = valore.Split("_")
            Dim bRet As Boolean = False
            
            If Not arrUser Is Nothing AndAlso arrUser.Length > 0 Then
                bRet = Array.IndexOf(arrUser, arr(4).Trim) > -1
            End If
        
            Return bRet
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Function
    
    Sub Page_Load()
       
        If Not Request("ListUser") Is Nothing AndAlso Request("ListUser") <> "" Then
            arrUser = Request("ListUser").Split(",")           
        End If
        
        _oUserGroupManager.Cache = False
        curId = _accService.GetTicket.User.Key.Id
        
        LoadTree()
    End Sub
    
    ''' <summary>
    ''' Legge i gruppi prensenti nel DB
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadGroups() As UserGroupCollection
        Dim oGroup As New UserGroupSearcher
        
        Return _oUserGroupManager.Read(oGroup)
    End Function
    
    ''' <summary>
    ''' Gruppi ai quali l'utente passato in input � correlato
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadGroupsByUser(ByVal id As UserIdentificator) As UserGroupCollection
        Return _oUserGroupManager.ReadUserRelated(id)
    End Function
    
    ''' <summary>
    ''' Restituisce gli utenti collegati ad un dato gruppo
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadUsersByGroup(ByVal id As UserGroupIdentificator) As UserCollection
        Return _oUserGroupManager.ReadUserRelated(id)
    End Function
    
    ''' <summary>
    ''' Restituisce tutti i gruppi figli di un determinato gruppo
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ReadGroupsByGroup(ByVal id As UserGroupIdentificator) As UserGroupCollection
        Return _oUserGroupManager.Read(id, UserGroupManager.ReadDirection.Sons)
    End Function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Seleziona Utenti</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="../_css/consolle.css" />	    	

    <style>
        .Hide
            {
                display:none;
            }
    </style>
	<script type="text/javascript" language="javascript">
		var curDiv
		var curDivSel        
		var ItemSel
	    
			function openDiv(id)
				{
		        
					obj = document.getElementById ('div_' + id)
					objAnchor = document.getElementById ('a_' + id)
					objMain =document.getElementById ('main_' + id) 
					if ((obj != null) && (objAnchor != null))
						{
							if (obj.style.display=='')
								{
									obj.style.display='none'
									objMain.style.background=  'white'
									objAnchor.src = '../_images/ico_15x_plus.gif'
								}
							else
								{
									obj.style.display=''
									objMain.style.background=  colorOpen
									if (curDiv != null) curDiv.style.background=  'white'
									curDiv = objMain
									objAnchor.src = '../_images/ico_15x_minus.gif'
								}
						}
				}
		    
		 	           
		     
			function selDiv(obj,id)
				{
		         	            
					if (id == '<%=iif(curNodeSel="",0,curNodeSel)%>') return
		                
		            
					if (curDivSel != null) curDivSel.style.color=  'black'       
		            
					curDivSel = obj
					//obj.style.color=colorSel
				}
		   
		   function UnselDiv()
			{
				if (curDivSel != null) curDivSel.style.color=  'black' 
			}   
		    
		   function Wait()
			{
				document.getElementById ('divMain').style.display ='none'
				document.getElementById ('divWait').style.display =''
			}
		    
			function Normal()
			{
				document.getElementById ('divMain').style.display =''
				document.getElementById ('divWait').style.display ='none'
			}
		    
			function Replace(src,match,alter)
				{
					var out = src
					var pos = out.indexOf(match)
		            
					while (pos != -1)
						{
						   out=out.substring(0,pos) + alter + out.substring(pos+match.length)  
						   pos = out.indexOf(match, pos)
						   
						   break;
						}
		                
						return out
				}
		    
	
		        
			function moveTo(id)
				{
		        
					location.href = '#' + id    
				}
		        
				moveTo('<%=iif(curNodeSel="",0,curNodeSel)%>')
		    	    
			function clickRadio(obj,id,bFromCheck)
				{	  
				    
				    var strEmail = getEmail(id)
			        var txtEmail = document.getElementById ('txtEmail')
					var countTot=0
					var Clicked =0
					if (! bFromCheck) obj.checked = !obj.checked;
		                
           			var txt = document.getElementById ('<%=txtHidden.clientid%>')
           			var oDiv = getParentDiv(id)
	           	    
           			var chk = document.getElementById ('inCheck_' + getParent(id))
					 var txtGroup = document.getElementById ('<%=txtHiddenGroup.clientid%>')  
	                 
           			if (!obj.checked)
           				{                   	                       	        
           					txt.value =Replace(txt.value ,'rd_' + id + '#','')   
           					chk.checked = false 
           					txtGroup.value = Replace(txtGroup.value , chk.id + '#','')     
           					txtEmail.value = Replace(txtEmail.value ,strEmail + ',' ,'')       
           				}
           			else
           			{
	           	        txtEmail.value = txtEmail.value + strEmail + ','
						if (txt != null)
							{	                       
	                                 
							 var arr =oDiv.getElementsByTagName('input') 
	                        
							 var i
	                         
							 for(i=0;i<arr.length;i++)
								{
									if ((arr[i].id.substring(0,3)=='rd_') && MatchLength(arr[i].id.substr(3),getParent (arr[i].id.substr(3))))
										{         
	                                                                  
											countTot+=1
											if (arr[i].checked)     
											   {    
													if (txt.value.indexOf(arr[i].id + '#') == -1)                           
													{
														txt.value = txt.value + arr[i].id + '#'
														Clicked+=1
													}
												}
										}                                
								}    
	                            
	                         
	                        
							if (countTot==Clicked)   
								{
									chk.checked = true
									txtGroup.value = txtGroup.value + chk.id + '#'
								}
							else
								{
									chk.checked = false
	                                
									txtGroup.value = Replace(txtGroup.value , chk.id + '#','')
								}
	                                                
							}	            	            
					}
				}
	        
			function getId(srcObj)
				{
					var id = srcObj.id
					var pos = id.indexOf("_")
	                
					return id.substr(pos+1)
				}
	            
			function LoadCheck(txt)
				{
					var txt = document.getElementById (txt)
					var arr = txt.value.split('#')
					var i
					var obj
	                
					for(i=0;i<arr.length;i++)
							{
							 obj = document.getElementById (arr[i])   
							 if (obj != null)
								{
									obj.checked = true;
								}
							}
	                
				}
	       function getEmail(id)
	        {
	            var txt = document.getElementById ('txtHidden_' + id)
	            if (txt != null)
	                {
	                    return txt.value
	                }
	        }
	        
		   function getParentDiv(id)
				{
					var arr = id.split('_')
					var i,n
					var out=''
					var outObj
	                
					if (arr.length <= 5)                    
					   outObj = document.getElementById ('mainPanel_' + arr[0])
					else
						{
							n = arr.length - 2;
							for (i=0;i<n;i++)
								{
									out += arr[i] + '_'
								}
	                        
							out = out.substring(0,out.length-1)
							outObj = document.getElementById ('mainPanel_' + out)
	                        
						}
	                                                                
	                 
					return outObj              
				}
		  function getParent(id)
			{
				var arr = id.split('_')
				var n = arr.length - 2
				var i,out
	            
				out = ''
				for (i=0;i<n;i++)
					{
						out += arr[i] + '_'
					}
				out = out.substr(0,out.length - 1)
				return out
			}
	              
		  function GroupRadioClick(srcObj,id)
			{
			            
			            var strEmail 
			            var txtEmail = document.getElementById ('txtEmail')
			        
						var arr = getParentDiv(id).getElementsByTagName('input') 
						var i
	                    
						 for (i=0;i<arr.length;i++)
							{
	                            
								if ((arr[i].id.substring(0,3)=='rd_') && (arr[i].id.substr(3,id.length)==id) && MatchLength(arr[i].id.substr(3),id) )
									{                                   
										arr[i].checked = srcObj.checked;
	                                   
										var txt = document.getElementById ('<%=txtHidden.clientid%>')
										if (arr[i].checked)
											{
												if (txt.value.indexOf(arr[i].id + '#') == -1) 
													{
													    txt.value = txt.value + arr[i].id + '#'
													    strEmail = getEmail(arr[i].id.replace('rd_',''))
													    txtEmail.value = txtEmail.value + strEmail + ','
													    
													}
													 
											}
	                                            
										else
										   {
										    strEmail = getEmail(arr[i].id.replace('rd_',''))
	                                        txtEmail.value = Replace(txtEmail.value ,strEmail + ',' ,'')   
											txt.value =  Replace(txt.value, arr[i].id + '#','')
										   }                                                                                                                                                                                       
									}
							}
						 var txtGroup = document.getElementById ('<%=txtHiddenGroup.clientid%>')
						if (srcObj.checked)
							txtGroup.value = txtGroup.value + 'inCheck_' + id + '#'
						else
							txtGroup.value = Replace(txtGroup.value,'inCheck_' + id + '#','')    
			}
	        
		 function MatchLength(val1 ,val2)
			{
	            
				var arr = val1.split('_')
				var arr1 = val2.split('_')
	           
				return (arr.length - 2 == arr1.length)
			}
	     
		 function checkListBox()
			{
				
				var txt
				
			            var txt	     
						//txt = document.getElementById ('<%=txtHidden.clientid%>')
						txt = document.getElementById ('<%=txtEmail.clientid%>')
						if (txt.value == '')
							return false
						else
							return true
				 
			}
	           
		 function SetUser()
			{           
					
					var txt																         
//					txt = document.getElementById ('<%=txtHidden.clientid%>')
//					var txtGroup = document.getElementById ('<%=txtHiddenGroup.clientid%>')
	                txt = document.getElementById ('<%=txtEmail.clientid%>')
					return txt.value 
					  
			}
			
	    function SetUserId()
			{           
					var outValue=''
					
					//Recupero la lista degli item selezionati														         
					var txt = document.getElementById ('<%=txtHidden.clientid%>').value
					
					//splitto per #
					var arr=txt.split('#')
					var arr2
					
                    for (i=0;i<arr.length-1;i++)
                        {
                            arr2=arr[i].split('_')
                            //il Sesto elemento della lista � l'id dell'utente
                            outValue+=arr2[5] + ','
                        }
	                
					return outValue 
					  
			}
			
			
	</script>
</head>
<body style="text-align:left">
    <form id="Form1" runat="server">
        <asp:textbox id="txtHiddenGroup" runat="server" style="display:none"/>
        <asp:textbox id="txtHidden" runat="server" style="display:none"/>
        <asp:textbox id="txtEmail" runat="server" style="display:none"/>
            <div class="dTabs">
                <asp:Panel id ="dGroupTab" cssclass = "dTabSelected" runat="server">
			        <div class="dTabLeft"></div>
                    <span style="font-weight:bold; font-size:16px;">Groups</span>
                    <div class="dTabRight"></div>
                </asp:Panel>
            </div>
            <div id="divMain" class="dMain">                
                <asp:placeHolder ID="pMain" runat ="server" />
            </div>
            <div class="dToolbar" style="margin-left:5px">
                <input type="button" class="button" onclick="if (checkListBox()) {window.opener.WriteSelectedUsers(SetUser(),SetUserId());window.close()} else alert('Selezionare un elemento')" value="Select" id="btnSel" runat="server"/>
                <input type="button" class="button" onclick="window.close()" value="Cancel"/>        
	        </div>
    </form>
</body>
</html>

<script type="text/javascript" >
<%=strJs%>
LoadCheck('<%=txtHiddenGroup.clientid%>')     
LoadCheck('<%=txtHidden.clientid%>') 
</script>
