﻿<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>

<script runat="server">
    'Oggetto recuperato dallo SharedObject
    Private oNewsletterSend As NewsletterSendingValue
    Private NewsletterList As ArrayList
    
    Private oNewsletterManager As New NewsletterManager
    Private oNewsletterSended As NewsletterSended
    Private strJs As String
    Private curListToSent As UserCollection
       
    'NUMERO DI MAIL DA INSERIRE PER OGNI RICHIESTA AL SERVER
    Private nMaxMailToProcess As Int16 = 50
        
    Sub Page_load()        
        oNewsletterSend = Session("NewsletterSend")
        NewsletterList = Session("NewsletterList")
                
        'Tento di recuperare la struttura dallo shared object
        If oNewsletterSend Is Nothing Then
            strJs = "alert('No data!');this.close()"
        Else
            BatchInsert()
        End If
    End Sub
    
    Sub GoBack()
        If Not Session("LastViewedUrl") Is Nothing Then
            strJs += vbCrLf & "window.opener.location.href='" & Session("LastViewedUrl") & "';this.close()"
        End If
    End Sub
    
    ReadOnly Property TotToSent() As Int16
        Get
            Try
                If curListToSent Is Nothing Then
                    Return 0
                End If

                If oNewsletterSend.IsTest Then
                    Return IIf(oNewsletterSend.MaxMailForTest > curListToSent.Count, curListToSent.Count, oNewsletterSend.MaxMailForTest)
                Else
                    Return oNewsletterSend.ListUser.Count
                End If
            Catch
                Return 0
            End Try
        End Get
    End Property
   
    ''' <summary>
    ''' Numero effettivo di mail da inviare.
    ''' Tiene conto se si tratta di un test e in tal caso restituisce la property MaxMailForTest
    ''' altrimeti in numero di utenti dell' oggetto curListToSent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MailToSent() As Int16
        Get
            Try
                If curListToSent Is Nothing Then
                    Return 0
                End If

                If oNewsletterSend.IsTest Then
                    Return IIf(oNewsletterSend.MaxMailForTest > TotMailToSent, TotMailToSent, oNewsletterSend.MaxMailForTest)
                Else
                    Return IIf(TotMailToSent > nMaxMailToProcess, nMaxMailToProcess, TotMailToSent)
                End If
            Catch
                Return 0
            End Try
        End Get
    End Property
    
    ''' <summary>
    ''' Numero totale di mail da inviare
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TotMailToSent() As Int16
        Get
            If curListToSent Is Nothing Then
                Return 0
            End If

            If oNewsletterSend.IsTest Then
                Return IIf(oNewsletterSend.MaxMailForTest > oNewsletterSend.ListUser.Count, curListToSent.Count, oNewsletterSend.MaxMailForTest - (oNewsletterSend.ListUser.Count - curListToSent.Count))
            Else
                Return curListToSent.Count
            End If
        End Get
    End Property
    
    ''' <summary>
    ''' Ciclo sugli utenti e li inserisco nella Scheduler mail
    ''' </summary>
    ''' <remarks></remarks>
    Sub BatchInsert()
        Dim oMail As MailAgentValue
        Dim oMailManager As New SystemUtility
        Dim Title As String
        Dim Email As String
        Dim counter As Int16 = 0
        Dim Body As String
        'Oggetto usato per il tracciamento delle newsletter
        Dim oNewsTrace As NewsletterTraceValue
        'Flag usato per stabilire se ricaricare o meno la pagina
        Dim bRefresh As Boolean = False
        
        Try
            Dim oUser As UserValue
       
            'Lista degli utenti a cui ancora bisogna inviare la newsletter
            curListToSent = oNewsletterSend.ListToSend
            Do While counter < MailToSent
                Try
                    'Imposto il flag che mi impone l'aggiornamento della pagina
                    bRefresh = True
                
                    'Recupero l'utente i-esimo
                    oUser = curListToSent(counter)
                    'Creo la mail
                    oMail = New MailAgentValue
                
                    Dim descTitle As String = ""
                    If oUser.Title <> "" AndAlso Not IsNumeric(oUser.Title) Then
                        descTitle = oUser.Title
                    Else
                        If IsNumeric(oUser.Title) AndAlso oUser.Title > 0 Then
                            Dim oContextManager As New ContextManager
                            Dim oValue As New ContextValue
                            Dim oIndetificator As New ContextIdentificator
                            oIndetificator.Id = oUser.Title
                            oValue = oContextManager.Read(oIndetificator)
                            If Not oValue Is Nothing AndAlso oValue.Description <> "" Then
                                descTitle = oValue.Description
                            End If
                        End If
                    End If
                    Title = descTitle
                
                    If Title = "" Then
                        If oUser.Gender <> String.Empty AndAlso oUser.Gender = "F" Then
                            Title = "Ms."
                        Else
                            Title = "Mr"
                        End If
                    End If
                    'If oUser.Gender <> String.Empty AndAlso oUser.Gender = "F" Then
                    '    Title = "Gent.ma Dott.ssa"
                    'Else
                    '    Title = "Gent.mo Dott."
                    'End If
                         
                    'oMail.MailFrom = New MailAddress(oNewsletterSend.Mittente)
                    'If oNewsletterSend.IsTest Then
                    '    Email = oNewsletterSend.Destinatari
                    'Else
                    '    Email = oUser.Email(0)
                    'End If
                    
                    'modifica di Davide
                    Dim senderName As String = IIf(Request("alias") Is Nothing, String.Empty, Request("alias"))
                    oMail.MailFrom = New MailAddress(oNewsletterSend.Mittente, senderName)
                    

                    If oNewsletterSend.IsTest Then
                        If oNewsletterSend.Destinatari.Length > 0 Then
                            If oNewsletterSend.Destinatari.Contains(",") Then
                                Dim vettore_BCc() As String = oNewsletterSend.Destinatari.Split(",")
                                
                                Email = vettore_BCc(0)
                                For y As Integer = 1 To vettore_BCc.Length - 1
                                    oMail.MailBcc.Add(New MailAddress(vettore_BCc(y)))
                                Next
                            Else
                                Email = oNewsletterSend.Destinatari
                            End If
                        End If
                    Else
                        Email = oUser.Email(0)
                    End If
                    
                                        
                    Body = oNewsletterSend.FullTextHtml
                
                    oMail.MailTo.Add(New MailAddress(Email))
                                
                    oMail.MailPriority = MailAgentValue.Priority.Hight
                
                    oMail.MailIsBodyHtml = True
                    oMail.MailSubject = oNewsletterSend.Subject
                    oMail.MailDateInsert = DateTime.Now
                
                    If oNewsletterSend.DataPosticipata.HasValue Then
                        'Setto la data di invio
                        oMail.MailDateSend = oNewsletterSend.DataPosticipata
                    End If
                
                    oMail.MailType = MailAgentValue.TypeMail.Newletter
                
                    'Formatto il body della newsletter
                    If Not (NewsletterList Is Nothing) AndAlso (NewsletterList.Count > 0) Then
                        oNewsletterSend.Number = CType(NewsletterList(0), Integer)
                    End If
                                       
                    Dim mailBody As String = NewsletterSendingValue.FormatUserText(Body, oUser.Name, oUser.Surname, _
                                                          oUser.Key.Id, oUser.Email(0), Title, _
                                                          oNewsletterSend.Number, oNewsletterSend.IsTest, _
                                                          oUser.Username, oUser.Password)
                
                    If Not oNewsletterSend.ListRelatedUser Is Nothing AndAlso oNewsletterSend.ListRelatedUser.Count > 0 Then
                        Dim relatedUser As ContactValue = Nothing
                        oNewsletterSend.ListRelatedUser.TryGetValue(oUser.Key.Id, relatedUser)
                        
                        If Not relatedUser Is Nothing Then
                            mailBody = NewsletterSendingValue.FormatRelatedUserText(mailBody, relatedUser.RelatedUser)
                        End If
                    End If
                   
                    mailBody = oNewsletterManager.EncryptLink(mailBody)
            
                    'If (Request("webservice").ToString.ToLower = "true") Then
                    '    'nel caso in cui venga fatto il tracciamento tramite webservice
                    '    'viene recuperato il dominio del webservice e fatto il replace del 
                    '    'dominio in tutti i link presenti nel body
                    '    Dim webServiceUrl As String = ConfigurationManager.AppSettings("GetUrlWebServiceNLTrace").ToString
                    '    If (webServiceUrl <> "") Then mailBody = ReplacLinkDomain(mailBody, webServiceUrl)
                    'End If
            
                    oMail.MailBody = mailBody
                    'oMail.MailAttach = oNewsletterSend.CurrentAttach
                
                    'Controllo la validità della mail
                    If Not Email Is DBNull.Value AndAlso Email <> "" Then
                        'Aggiorna i totali 
                        oNewsletterSend.TotSended += 1
                        oMailManager.SendMail(oMail)
                     
                        If Not oNewsletterSend.IsTest Then
                            'Aggiorno il totale di newsletter inviate
                            oNewsletterManager.Update(oNewsletterSend)
                            '------------------------------------------------------------
                        
                            'Traccio l'invio su HP3_Trace_NL
                            oNewsTrace = New NewsletterTraceValue
                            'oNewsTrace.EventId = New TraceEventIdentificator
                            oNewsTrace.NewsletterId = oNewsletterSend.KeyNewsletter
                            oNewsTrace.UserId = oUser.Key
                            oNewsletterManager.Create(oNewsTrace)
                  
                            'If Request("webservice").ToString.ToLower = "true" Then
                            '    'modifiche x il tracciamento tramite webservice
                            '    Dim webServiceUrl As String = ConfigurationManager.AppSettings("GetUrlWebServiceNLTrace").ToString
                            '    If (webServiceUrl <> "") Then
                            '        Dim referenceObj As New it.healthware.hp3office.NLTrace
                            '        referenceObj.Url = webServiceUrl & "nltrace.asmx"
                            '        Dim ident As New it.healthware.hp3office.UserIdentificator
                            '        ident.Id = oUser.Key.Id
                                    
                            '        Try
                            '            referenceObj.NewsletterTrace(ident, oNewsletterSend.KeyNewsletter.PrimaryKey)
                            '        Catch ex As Exception
                            '            'Session("NewsletterSend") = Nothing
                            '            'counter += 1
                            '        End Try
                                    
                            '    End If
                            '    'fine modifiche
                            'End If
                        End If
                    End If
                        
                    'MEMORIZZO L'INSERIMENTO AL FINE DI NON INSERIRLO NUOVAMENTE
                    oNewsletterSended = New NewsletterSended
                    oNewsletterSended.UserID = oUser.Key
                    oNewsletterSended.NewsletterID = oNewsletterSend.KeyNewsletter
            
                    oNewsletterManager.Create(oNewsletterSended)
                    'taccia l'eveto sended newsletter
                    TraceNewsletterSended(oUser.Key, oNewsletterSend.KeyNewsletter.PrimaryKey)
                                
                    counter += 1
                Catch ex As Exception
                    counter += 1
                End Try
            Loop
                                            
        If bRefresh Then
            'Aggiorno la pagina
            ltlRefresh.Text = "<meta http-equiv=""refresh"" content=""1; url="">"
            'Salvo l'oggeto 
            Session("NewsletterSend") = oNewsletterSend
        Else
            'Rimuove le newsletter inviate
            DeleteInserted()
            'Rimuovo l'oggeto dalla cache
            Session("NewsletterSend") = Nothing
                
            strJs = "alert('All eMails sent.');"
            GoBack()
        End If
            
        Catch ex As Exception
            DeleteInserted()
            Response.Clear()
            Response.Write(ex.ToString)
            Response.End()
            'Session("NewsletterSend") = Nothing
            
            'GoBack()
        Finally
            If Not oNewsletterSend.ListUser Is Nothing AndAlso Not curListToSent Is Nothing Then
                Dim val As String = Math.Floor((100 * (oNewsletterSend.ListUser.Count - TotMailToSent)) / oNewsletterSend.ListUser.Count) & "%"
                divProgress.Style("width") = val
                lblMessage.Text = "e-Mail sent:" & oNewsletterSend.ListUser.Count - curListToSent.Count & " / " & TotToSent & " (" & val & ")"
            End If
        End Try
    End Sub
       
    Sub DeleteInserted()
        oNewsletterManager.DeleteSended(oNewsletterSend.KeyNewsletter)
    End Sub
    
    'si occupa di tracciare l'evento 'Send' newsletter a un certo utente userId
    Sub TraceNewsletterSended(ByVal userIdent As UserIdentificator, ByVal newsletterK As Integer)
        Dim nwlsManager As New NewsletterManager
        Dim traceValue As New NewsletterTraceValue
        traceValue.NewsletterId = New NewsletterIdentificator(newsletterK)
        traceValue.EventId = NewsletterTraceValue.TraceOperation.Send
        traceValue.UserId = userIdent

        Dim nslTraceValue As NewsletterTraceValue = nwlsManager.Create(traceValue)
    End Sub
    
    Function ReplacLinkDomain(ByVal body As String, ByVal domain As String) As String
        Dim linkPattern As String = "<a\b[^>]*>(.*?)</a>"
        Dim imgPattern As String = "<img[^>]+>"

        Dim _matchCollection As MatchCollection
        _matchCollection = Regex.Matches(body, linkPattern)
        
        For Each _match As Match In _matchCollection
            Dim tag As String = _match.Value.ToString
            If (tag.Contains("http://")) Then
                Dim startPoit As Integer = tag.IndexOf("http://")
                Dim point As Integer = tag.IndexOf("/", 7)
                point = point + 3
                Dim endPoint As Integer = point - startPoit
                Dim data As String = tag.Substring(startPoit, endPoint)

                body = body.Replace(data, domain)
            End If
        Next
        
        Return body
    End Function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invio Newsletter</title>
    <link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />
	<link type="text/css" rel="stylesheet" href="../_css/consolle.css" />	             	
</head>
<body id="Main" runat="server" style="margin:10px">
    <form id="form1" runat="server">
        <asp:Literal id="ltlRefresh" Runat="Server"/>
        <div style ="border:1px solid #000;width:98%;padding:2px;text-align:left ">
            <div runat="server" id="divProgress" style="background-color:Blue;width:0%;text-align:center">
            &nbsp;
            </div>
        </div>
        <asp:Label ID ="lblMessage" runat="server" />        
    </form>
    <script type ="text/javascript" >	
    <%=strJs%>  
    </script>	
</body>
</html>
