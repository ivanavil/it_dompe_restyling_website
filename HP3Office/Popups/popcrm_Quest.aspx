<%@ Page Language="VB"  EnableEventValidation="true" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.CRM.WebTrack" %>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.LMS.Catalog"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import NameSpace="Healthware.HP3.Quest.Quest"%>
<script runat="Server">
    Sub Page_Load()
        If Not Page.IsPostBack Then
            Dim id As Integer = 0
            Dim userstr As String = ""
            id = Request("SelItem")
            userstr = Request("DetailUser")
            If userstr = "y" Then
                LoadUSer(id)
            ElseIf userstr = "n" Then
                LoadEventResult(id)
            Else
                Esegui(id)
            End If
        End If
    End Sub
    
    Sub LoadEventResult(ByVal idQuestionnaire As Integer)
        Dim appId As String = idQuestionnaire
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQstSearch As New WebTrackQSTSearcher
        Dim webTrackQstCollEvent As New WebTrackQSTCollection
        Dim webTrackQstCollResult As New WebTrackQSTCollection

        webTrackQstSearch.QuestionnaireId = appId
        webTrackQstCollEvent = webTrackMan.ReadQSTGroupByEvent(webTrackQstSearch)
        webTrackQstCollResult = webTrackMan.ReadQSTGroupByResult(webTrackQstSearch)

        GridDetailsEvent.DataSource = webTrackQstCollEvent
        GridDetailsEvent.DataBind()
        GridDetailsResult.DataSource = webTrackQstCollResult
        GridDetailsResult.DataBind()
        lblDetails.Visible = True
    End Sub
    
    Private Sub LoadUSer(ByVal idQuestionnaire As Integer, Optional ByVal eventid As Integer = 0, Optional ByVal resultid As Integer = 0)
        Dim userColl As New UserCollection
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQuestSearc As New WebTrackQSTSearcher

        webTrackQuestSearc.ResultId = resultid
        webTrackQuestSearc.EventId = eventid
        
        webTrackQuestSearc.QuestionnaireId = idQuestionnaire
        userColl = webTrackMan.ReadUserByQuestionnaire(webTrackQuestSearc)
        
        GridUser.DataSource = userColl
        GridUser.DataBind()
    End Sub
    
    Private Sub Esegui(ByVal id As Integer)
        If id > 0 Then
            Dim QuestMan As New QuestionnaireManager
            Dim questIdent As New QuestionnaireIdentificator
            Dim questColl As New QuestionCollection

            questIdent.Id = id
            questColl = QuestMan.ReadQuestions(questIdent)
            GridQuestions.DataSource = questColl
            GridQuestions.DataBind()
        End If
    End Sub
    
    Function GetCompilation(ByVal id As Integer)
        Dim res As String = ""
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQstSearch As New WebTrackQSTSearcher
        Dim webTrackQstColl As New WebTrackQSTCollection
        webTrackQstSearch.QuestionnaireId = Request("SelItem")
        webTrackQstSearch.UserId = id
        webTrackQstColl = webTrackMan.ReadQSTVisit(webTrackQstSearch)
        
        If Not webTrackQstColl Is Nothing AndAlso webTrackQstColl.Count > 0 Then
            res = webTrackQstColl.Count
        End If
        
        Return res
    End Function
    
    Sub OnItemSelected(ByVal s As Object, ByVal e As EventArgs)
        Dim userId As String = s.commandargument
        Dim questId As String = Request("SelItem")
        Dim res As String = ""
        Dim webTrackMan As New WebTrackManager
        Dim webTrackQstSearch As New WebTrackQSTSearcher
        Dim webTrackQstColl As New WebTrackQSTCollection
        webTrackQstSearch.QuestionnaireId = questId
        webTrackQstSearch.UserId = userId
        webTrackQstColl = webTrackMan.ReadQSTVisit(webTrackQstSearch)

        Dim WebTrackManager As New WebTrackManager
        Dim webTracSearch As New WebTrackQSTSearcher
        Dim coll As New AnswerCollection
        Dim i As Integer = 1
        lblquest.Text = "<tr class='header'><th scope='col' style='width:9%;'><strong>Question</strong></td><th scope='col' style='width:9%;'><strong>Answer</strong></td><th scope='col' style='width:1%;'><strong>Correct</strong></td></tr>"
        For Each elem As WebTrackQSTValue In webTrackQstColl
            webTracSearch.UserId = userId
            webTracSearch.TracequestionnaireId = elem.TraceId
            webTracSearch.QuestionnaireId = questId
            coll = WebTrackManager.ReadAnswerUserByQuestionnaire(webTracSearch)
            lblquest.Text = lblquest.Text & "<tr><td colspan='3' style='text-align:center'><strong>Compilation " & i & "</strong></td></tr>"
            If Not coll Is Nothing AndAlso coll.Count > 0 Then
                For Each elem2 As AnswerValue In coll
                    lblquest.Text = lblquest.Text & "<tr><td>" & GetQuestion(elem2.KeyQuestion.Id) & "</td><td>" & elem2.Description & "</td><td>" & elem2.IsCorrect & "</td></tr>"
                Next
            End If
            i = i + 1
        Next
    End Sub
    
    Function GetQuestion(ByVal id As Integer) As String
        Dim res As String = ""
        Dim QuestMan As New QuestionManager
        Dim questIdent As New QuestionIdentificator
        Dim questS As New QuestionSearcher
        Dim questVal As New QuestionValue
        Dim questcoll As New QuestionCollection
        
        questIdent.Id = id
        questS.Key.Id = id
        questVal = QuestMan.Read(questIdent)
        questcoll = QuestMan.Read(questS)
        
        If Not questVal Is Nothing AndAlso questVal.Description = "" Then
            res = questVal.Description
        End If
        If Not questcoll Is Nothing AndAlso questcoll.Count > 0 Then
            res = questcoll(0).Description
        End If
        Return res
    End Function
    Function MappingResultId(ByVal resultId As Integer) As String
        Dim res As String = ""
        Select Case resultId
            Case 1
                res = "Passed"
            Case 2
                res = "Flunk"
        End Select
        Return res
    End Function
    Function MappingEventId(ByVal eventId As Integer) As String
        Dim res As String = ""
        Select Case eventId
            Case 1
                res = "Start"
            Case 2
                res = "Complete"
            Case 3
                res = "Certificate"
        End Select
        Return res
    End Function
    
    Sub SelectEvent(ByVal s As Object, ByVal e As EventArgs)
        Dim eventId As String = s.commandargument
        LoadUSer(Request("SelItem"), eventId, 0)
    End Sub
    Sub SelectResult(ByVal s As Object, ByVal e As EventArgs)
        Dim resultId As String = s.commandargument
        LoadUSer(Request("SelItem"), 0, resultId)
    End Sub
  </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Questionnaire Details</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>
    <body>
    <form id="formHistory" runat="server" >
         <div class="title" style="padding-left:10px">Questionnaire Details</div>
         <br />
         <hr />
         
        <div id="divTraceHistory">
            <asp:GridView ID="GridUser" 
            AutoGenerateColumns="false"  
            CssClass="tbl1"
            AlternatingRowStyle-BackColor="#eeefef"
            RowStyle-BackColor="#fffffe"
            PagerSettings-Position="Top"  
            PagerStyle-HorizontalAlign="Right" 
            PagerStyle-CssClass="Pager"  
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            emptydatatext="No content available"
            Width="100%"
            GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="Username " HeaderStyle-Width="9%">
                    <ItemTemplate>
                        <div style="text-align:center">
                            <%#DataBinder.Eval(Container.DataItem, "Name")%> - <%#DataBinder.Eval(Container.DataItem, "Surname")%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Compilation " HeaderStyle-Width="1%">
                    <ItemTemplate>
                        <div style="text-align:center">
                              <asp:LinkButton runat="server" id="lnkSel" OnClick="OnItemSelected" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' Text='<%#GetCompilation(DataBinder.Eval(Container.DataItem, "Key.id"))%>' ></asp:LinkButton>           
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
    <br />
            <table class="tbl1" cellspacing="0" border="0" id="ctl00_GridQuest" style="width:100%;border-collapse:collapse;">
            <asp:Label runat="server" ID="lblquest"></asp:Label>
            </table>
    
            <asp:GridView ID="GridQuestions" 
            AutoGenerateColumns="false"  
            CssClass="tbl1"
            AlternatingRowStyle-BackColor="#eeefef"
            RowStyle-BackColor="#fffffe"
            PagerSettings-Position="Top"  
            PagerStyle-HorizontalAlign="Right" 
            PagerStyle-CssClass="Pager"  
            ShowHeader="true"  
            HeaderStyle-CssClass="header"
            ShowFooter="false"  
            FooterStyle-CssClass="gridFooter"
            runat="server"
            emptydatatext="No content available"
            Width="100%"
            GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="Question" HeaderStyle-Width="9%">
                    <ItemTemplate>
                        <div style="text-align:center">
                            <%#DataBinder.Eval(Container.DataItem, "Description")%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
            
    <strong>
        <asp:Label ID="lblDetails" runat="server" Text="Event and Result Details" Visible="false"></asp:Label>
    </strong>
   <asp:GridView ID="GridDetailsEvent" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Event Description" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#MappingEventId(DataBinder.Eval(Container.DataItem, "EventId"))%>  </div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="1%">
                <ItemTemplate><div style="text-align:center">
                    <%'DataBinder.Eval(Container.DataItem, "total")%>
                    <asp:LinkButton ID="lnkbut" Text='<%#DataBinder.Eval(Container.DataItem, "total")%>' runat="server" OnClick="SelectEvent" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "EventId")%>'></asp:LinkButton>
                </div></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
    <br />
    <asp:GridView ID="GridDetailsResult" 
        AutoGenerateColumns="false"  
        CssClass="tbl1"
        AlternatingRowStyle-BackColor="#eeefef"
        RowStyle-BackColor="#fffffe"
        AllowPaging="true"  
        PageSize="10"
        PagerSettings-Position="Top"  
        PagerStyle-HorizontalAlign="Right" 
        PagerStyle-CssClass="Pager"  
        ShowHeader="true"  
        HeaderStyle-CssClass="header"
        ShowFooter="false"  
        FooterStyle-CssClass="gridFooter"
        runat="server"
        emptydatatext="No content available"
        Width="100%"
        GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="Result Description" HeaderStyle-Width="9%">
                <ItemTemplate><div style="text-align:center"><%#MappingResultId(DataBinder.Eval(Container.DataItem, "ResulId"))%> </div></ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total User" HeaderStyle-Width="1%">
                <ItemTemplate>
                    <div style="text-align:center">
                        <%'DataBinder.Eval(Container.DataItem, "total")%> 
                        <asp:LinkButton ID="lnkbut" Text='<%#DataBinder.Eval(Container.DataItem, "total")%>' runat="server" OnClick="SelectResult" CommandName ="SelectItem" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "ResulId")%>'></asp:LinkButton>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
    </asp:GridView>
        </div> 
    </form>     
    </body>
</html>

