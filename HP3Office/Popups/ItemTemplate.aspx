<%@ Page Language="VB" debug="true"  validaterequest="false"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import NameSpace="system.io" %>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Box" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization" %>

 
<script runat="Server">
    Dim Titolo As String
    Dim Testo As String
    Dim strJs As String
    Dim strImage As String
    Private _strDomain As String
    Private _language As String
    
    Private masterPageManager As New MasterPageManager
    Private dictionaryManager As New DictionaryManager
    
    Private Property Domain() As String
        Get
            Return _strDomain
        End Get
        Set(ByVal value As String)
            _strDomain = value
        End Set
    End Property
    
    Private Property Language() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property
    
    Function LoadBoxContentNewsletter(ByVal oBoxNCSearcher As BoxContentNewsletterSearcher) As BoxContentNewsletterValue
        Dim oBoxManager As New BoxManager
        oBoxManager.Cache = False
        Dim oBoxNCCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
        If Not oBoxNCCollection Is Nothing AndAlso oBoxNCCollection.Count > 0 Then
            Return oBoxNCCollection.Item(0)
        End If
    End Function
    
    Sub Page_Load()
        Language() = masterPageManager.GetLang.Id
        'Domain() = WebUtility.MyPage(Me).DomainInfo
        InitBtnSave()
        
        If Request("Type") Is Nothing OrElse Request("Box") Is Nothing OrElse Request("idNl") Is Nothing Then
            strJs = "alert('Parameters absent. Impossible to continue');window.close()"
        End If
       
        Select Case Request("Type")
            Case "0"
                Titolo = "Insert image/banner"
                Testo = "Select the image"
            Case "1"
                Titolo = "Insert text"
                Testo = "Intert text"
        End Select
        
        If Not Request("idItem") Is Nothing Then
            Dim oBoxManager As New BoxManager
            oBoxManager.Cache = False
            Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
            oBoxNCSearcher.Key = New BoxContentNewsletterIdentificator(Request("idItem"))
            Dim oBoxNCCollection As BoxContentNewsletterCollection = oBoxManager.Read(oBoxNCSearcher)
            Dim oBoxNCValue As BoxContentNewsletterValue
            
            If Not oBoxNCCollection Is Nothing Then
                oBoxNCValue = oBoxNCCollection.Item(0)
                If Not oBoxNCValue Is Nothing Then
                    ActionByParam(oBoxNCValue)
                End If
            End If
                       
        End If
        
        openPanel(Request("Type"))
    End Sub
    
    Sub InitBtnSave()
        btnSalvaImg.Attributes.Add("onClick", "return confirm('This operation will save the association.Do you want to continue?')")
        btnSalvaTesto.Attributes.Add("onClick", "return confirm('This operation will save the association.Do you want to continue?')")
    End Sub
    
    Sub ActionByParam(ByVal oBoxValue As BoxContentNewsletterValue)
        If Page.IsPostBack Then Exit Sub
        Select Case Request("Type")
            Case 1
                'txtFreeText.Value = oBoxValue.FreeText
                txtFreeText.Content = oBoxValue.FreeText
            Case 0
                txtbannerlink.Text = oBoxValue.Link
                txtbannerdesc.Text = oBoxValue.Description                
                strImage = GetBanner(Request("idItem"), txtbannerlink.Text)
                lblFileName.Text = "<br/><strong>File existing: </strong>" & GetFileName(Request("idItem"))
        End Select
    End Sub
    
    Function GetFileName(ByVal id As String) As String
        Dim Path As String = PhisicalPath
        Dim objDir As New DirectoryInfo(Path)
        Dim objFile() As FileInfo
        
        objFile = objDir.GetFiles(id & ".*")
        If objFile.Length = 0 Then
            Return ""
        Else
            Return objFile(0).Name
        End If
                
        
    End Function
    
    Sub DelFile(ByVal strPathName As String, ByVal strFileName As String)
        
        Try
            Dim objDirInfo As New DirectoryInfo(strPathName)
            Dim objFiles() As FileInfo = objDirInfo.GetFiles(strFileName & ".*")
            Dim objFile As FileInfo
            
            For Each objFile In objFiles
                objFile.Delete()
            Next
        Catch ex As Exception
        End Try
        
    End Sub
    
    Sub openPanel(ByVal nStep As Int16)
               
        Dim ctl As Control
        ctl = FindControl("pnl_" & nStep)
        If Not ctl Is Nothing Then
            ctl.Visible = True
        End If
    End Sub
    
    ReadOnly Property PhisicalPath() As String
        Get
            Return SystemUtility.CurrentPhysicalApplicationPath & ConfigurationsManager.NewsletterImagePath
        End Get
    End Property
    
    ReadOnly Property VirtualPath() As String
        Get
            Return "\" & ConfigurationsManager.NewsletterImagePath
        End Get
    End Property
    
    Sub Salva_Img(ByVal s As Object, ByVal e As EventArgs)
        Dim Path As String = PhisicalPath
        Dim strFile As String = Request("idNL") & "_" & Request("Box")
        Dim outID As Int32
        
      
        If (Not Request("idItem") Is Nothing) Or (inputFile.PostedFile.ContentLength > 0) Then
            Dim strFileName As String = inputFile.PostedFile.FileName
            strFileName = strFileName.Substring(strFileName.LastIndexOf("\") + 1)
            strFileName = strFileName.Substring(strFileName.LastIndexOf(".") + 1)
            Select Case strFileName.ToUpper
                Case "JPG", "GIF", "SWF"
                    'Controllo della dimensione
                    If inputFile.PostedFile.ContentLength > ConfigurationsManager.MaxUploadFileSize Then                        
                        strJs = "alert('The file selected exceed the concurred maximum dimension of 1Mb')"
                    Else
                        Dim objDirInfo As New DirectoryInfo(Path)
                        Dim boxIdentificator As New BoxIdentificator
                        boxIdentificator.Id = Request("Box")
                        If EditImageBox(boxIdentificator, _
                                        New NewsletterIdentificator(Request("idNL")), _
                                        txtbannerlink.Text, _
                                        txtbannerdesc.Text, _
                                        outID, _
                                        IIf(Request("idItem") Is Nothing, Nothing, New BoxContentNewsletterIdentificator(Request("idItem")))) Then
                            'Controllo se il file esiste
                            DelFile(Path, outID)
                            inputFile.PostedFile.SaveAs(Path & outID & "." & strFileName)
                            strJs = "alert('File saved successfully.');window.opener.location.reload();window.close()"
                        Else
                            strJs = "alert('Error during file saving')"
                        End If
                    End If
                    
                Case Else
                    If Request("idItem") Is Nothing Then
                        strJs = "alert('The file selected must have extension GIF, jpg or swf');"
                    ElseIf EditImageBox(New BoxIdentificator(Request("Box")), New NewsletterIdentificator(Request("idNL")), txtbannerlink.Text, txtbannerdesc.Text, outID, IIf(Request("idItem") Is Nothing, Nothing, New BoxContentNewsletterIdentificator(Request("idItem")))) Then
                        strJs = "alert('The file have been updated correctly');window.opener.location.reload();window.close()"
                    End If
            End Select

        Else
            strJs = "alert('It is necessary to select an image with extension gif/jpg or a file with extension swf');"
        End If
    End Sub
    
    Function EditImageBox(ByVal keyBox As BoxIdentificator, ByVal keyNewletter As NewsletterIdentificator, ByVal Link As String, ByVal Descr As String, ByRef outId As Int32, ByVal key As BoxContentNewsletterIdentificator) As Boolean       
        Dim oBoxManger As New BoxManager
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        Dim oBoxNcCollection As BoxContentNewsletterCollection
        Dim oBoxNCValue As BoxContentNewsletterValue
       
        Dim order As Int32
        Try
            'Recupero dell' order
            oBoxNCSearcher.KeyBox = keyBox
            oBoxNCSearcher.KeyNewsletter = keyNewletter
            oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
            
            If oBoxNcCollection Is Nothing OrElse oBoxNcCollection.Count = 0 Then                
                order = order + 1
            Else
                order = oBoxNcCollection.Count + 1
            End If
            '---------------------------
            
            Dim bInsertNew As Boolean = True
            'Verifico l'esistenza dell'accoppiamento
            If Not key Is Nothing Then
                oBoxNCSearcher = New BoxContentNewsletterSearcher
                oBoxNCSearcher.Key = key
                oBoxNcCollection.Clear()
                oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
                If Not oBoxNcCollection Is Nothing AndAlso oBoxNcCollection.Count > 0 Then
                    'Esiste
                    oBoxNCValue = oBoxNcCollection.Item(0)
                    oBoxNCValue.KeyContent.Id = 0
                    oBoxNCValue.Link = Link
                    oBoxNCValue.Description = Descr
                   
                    oBoxManger.Update(oBoxNCValue)
                    
                    outId = key.Id
                    bInsertNew = False
                End If
            End If
            
            If bInsertNew Then
                oBoxNCValue = New BoxContentNewsletterValue
                oBoxNCValue.KeyBox = keyBox
                oBoxNCValue.KeyNewsletter = keyNewletter
                oBoxNCValue.Order = order
                oBoxNCValue.Link = Link
                oBoxNCValue.Description = Descr
                oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Banner
                oBoxNCValue = oBoxManger.Create(oBoxNCValue)
                outId = oBoxNCValue.Key.Id
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    Function GetBanner(ByVal idNl As Int32, ByVal link As String) As String             
        Dim strOut As String = ""
        Dim Path As String = PhisicalPath
        Dim Path2 As String = VirtualPath
        
        Dim objDir As New DirectoryInfo(Path)
        Dim objFile() As FileInfo
        Dim FileExt As String
        Dim nWidth, nHeight As Integer
        nWidth = 100
        nHeight = 100
       
        ''Cerco l' esistenza del file
        objFile = objDir.GetFiles(idNl & ".*")
        If objFile.Length = 0 Then Return ""
        ''Imposto il percorso
        Path += objFile(0).Name
        Path2 += objFile(0).Name
        
        FileExt = objFile(0).Extension.ToString.ToUpper.Substring(1)


        Select Case FileExt
            Case "SWF"
                strOut = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' width='[width]' height='[height]'>" & _
                  "<param name=movie value='[Path]?url_banner=[URL]'>" & _
                  "<param name=quality value=best>" & _
                  "<embed src='[Path]?url_banner=[URL]' quality=best pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' width='[width]' height='[height]'></embed>" & _
                 "</object>"

                strOut = Replace(Replace(Replace(Replace(strOut, "[width]", nWidth), "[height]", nHeight), "[Path]", Path2), "[URL]", link)

            Case "JPG", "GIF"
                strOut = "<a href= '[URL]' target='_blank'> <img src = '[Path]' alt = 'Anteprima' width = '[WIDTH]' height = '[HEIGHT]'></a>"
                strOut = Replace(Replace(Replace(Replace(strOut, "[Path]", Path2), "[WIDTH]", nWidth), "[HEIGHT]", nHeight), "[URL]", link)

        End Select
	
        objFile = Nothing

        Return strOut
    End Function
    
    Function EditFreeText(ByVal keyBox As BoxIdentificator, ByVal keyNewletter As NewsletterIdentificator, ByVal Freetext As String, ByVal key As BoxContentNewsletterIdentificator) As Boolean
        Dim oBoxManger As New BoxManager
        Dim oBoxNCSearcher As New BoxContentNewsletterSearcher
        Dim oBoxNcCollection As BoxContentNewsletterCollection
        Dim oBoxNCValue As BoxContentNewsletterValue
        
        Dim order As Int32
        Try
            'Recupero dell' order
            oBoxNCSearcher.KeyBox = keyBox
            oBoxNCSearcher.KeyNewsletter = keyNewletter
            oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
            
            If Not oBoxNcCollection Is Nothing Then
                order = oBoxNcCollection.Count + 1
            Else
                order += 1
            End If
            '---------------------------
            
            Dim bInsertNew As Boolean = True
            'Verifico l'esistenza dell'accoppiamento
            If Not key Is Nothing Then
                oBoxNCSearcher = New BoxContentNewsletterSearcher
                oBoxNCSearcher.Key = key
                oBoxNcCollection.Clear()
                oBoxNcCollection = oBoxManger.Read(oBoxNCSearcher)
                If Not oBoxNcCollection Is Nothing AndAlso oBoxNcCollection.Count > 0 Then
                    'Esiste
                    oBoxNCValue = oBoxNcCollection.Item(0)
                    oBoxNCValue.KeyContent.Id = 0
                    oBoxNCValue.Link = ""
                    oBoxNCValue.Description = ""
                    oBoxNCValue.FreeText = Freetext
                    oBoxManger.Update(oBoxNCValue)
                    bInsertNew = False                                
                End If
            End If
            
            If bInsertNew Then
               
                oBoxNCValue = New BoxContentNewsletterValue
                oBoxNCValue.KeyBox = keyBox
                oBoxNCValue.KeyNewsletter = keyNewletter
                oBoxNCValue.Order = order
                oBoxNCValue.FreeText = Freetext
                oBoxNCValue.BoxContentType = BoxContentNewsletterValue.ContentBoxType.Text
                oBoxManger.Create(oBoxNCValue)
            End If
            
            Return True
        Catch ex As Exception
            Response.Write(ex.ToString)
            Return False
        End Try
    End Function
    
    Sub Salva_Testo(ByVal s As Object, ByVal e As EventArgs)
        Dim boxIdentif As New BoxIdentificator
        boxIdentif.Id = Request("Box")
        ' If EditFreeText(boxIdentif, New NewsletterIdentificator(Request("idNL")), txtFreeText.Value, IIf(Request("idItem") Is Nothing, Nothing, New BoxContentNewsletterIdentificator(Request("idItem")))) Then
        If EditFreeText(boxIdentif, New NewsletterIdentificator(Request("idNL")), txtFreeText.Content, IIf(Request("idItem") Is Nothing, Nothing, New BoxContentNewsletterIdentificator(Request("idItem")))) Then

            strJs = "alert('Saved successfully.');window.opener.location.reload();window.close()"
        Else
            strJs = "alert('Error during association saving.')"
        End If
    End Sub
    
    Function getLabel(ByVal labelName As String, ByVal description As String) As String
        Return dictionaryManager.Read(labelName, masterPageManager.GetLang.Id, description)
    End Function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=Titolo%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link href="../_css/Popup.grp.css"  rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/hp3Office/_css/consolle.css" />
    <script>
        <%=strJs%>       
    </script>
</head>
    
    <body>
    
    <form id="form1" runat="server">
    <asp:panel runat="server" ID="pnl_0" Visible ="false" Width ="90%">
    <table class="form">
         <tr>
            <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormDescription&Help=cms_HelpBannerDescription&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormDescription", "Description")%></td>
            <td><asp:Textbox id ="txtbannerdesc" runat="Server" width="340px"/></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormLinkUrl&Help=cms_HelpBannerLinkUrl&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormLinkUrl", "Link Url")%></td>
            <td><asp:Textbox id ="txtbannerlink" runat="Server" width="340px"/><%=strImage%></td>
        </tr>
        <tr>
            <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormUploadBanner&Help=cms_HelpUploadBanner&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormUploadBanner", "Upload Banner")%> <asp:label ID="lblFileName" runat ="server" /></td>
            <td><input id="inputFile"  type="file" size="37" runat="server"  style="width:350px"/></td>
        </tr>
    </table>
    
    <div style="margin-top:10px">
        <input type="button" value="Close" class="button" title="Close the current window" onClick="window.close()"/>
        <asp:button id ="btnSalvaImg" runat="server" text="Save" cssclass="button" tooltip="Save" onClick="Salva_Img" />
    </div>
    </asp:panel>   
        <%--<asp:panel runat="server" ID="pnl_0" Visible ="false" Width ="100%">       
            <Strong>Immagine/Banner da associare</Strong>    
            <asp:label ID="lblFileName" runat ="server" />       
            <input id="inputFile"  type="file" size="37" runat="server"  style="width:350px"/>
            <br/>
            <Strong>Descrizione</Strong><br/>
            <asp:Textbox id ="txtbannerdesc" runat="Server" width="340px"/>
            <br/>
            <Strong>Link</Strong><br/>
            <asp:Textbox id ="txtbannerlink" runat="Server" width="340px"/>
            <%=strImage%>                        
		    <br />
		    <div style ="text-align:center ">
                <input type="button" value="Chiudi" class="button" title="Chiude la finestra corrente" onClick="window.close()"/>
                <asp:button id ="btnSalvaImg" runat="server" text="Salva" cssclass="button" tooltip="Salva i dati nel box" onClick="Salva_Img" />
            </div>
        </asp:panel>--%>
        
        <asp:panel runat="server" ID="pnl_1" Visible ="false" >            
            <%--<asp:textbox ID="txtFreeText" rows="8" runat="server" style="width:350px" TextMode ="multiline"/>--%>
            <table class="form">
                <tr>
                    <td><a href="#"  onclick="window.open('http://hp3office.healthware.it/hp3Office/Popups/popHelp.aspx?Label=cms_FormNewsletterText&Help=cms_HelpNewsletterText&Language=<%=Language()%>','Help','width=500,height=150,left=400,top=200,toolbar=no,scrollbar=yes,statu=no,resizable=no,scrollbars=yes')" ><img  src='/hp3Office/HP3Image/Ico/help.gif' alt="help" /></a> <%=getLabel("cms_FormNewsletterText", "Text")%></td>
                    <td>
<%--                        <FCKeditorV2:FCKeditor id="txtFreeText" Height="300px" width="580px" BasePath="/hp3office/js/FCKeditor/" runat="server" />
--%>                        <telerik:RadEditor ID="txtFreeText" runat="server" Language="it-IT" Skin="Vista" ToolProviderID="" Width="600px" AllowScripts="true" >
                                 <Content></Content>
                                 <Snippets>
                                     <telerik:EditorSnippet Name="MyList"><ol><li>One</li><li>Two</li><li>Three</li></ol></telerik:EditorSnippet>
                                 </Snippets>
                                 <Languages>
                                    <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                                    <telerik:SpellCheckerLanguage Code="fr-FR" Title="French" />
                                    <telerik:SpellCheckerLanguage Code="de-DE" Title="German" />
                                    <telerik:SpellCheckerLanguage Code="it-IT" Title="Italian" />
                                 </Languages>

                                 <ImageManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <FlashManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 <TemplateManager ViewPaths="~/HP3Image,~/HP3Media" UploadPaths="~/HP3Image,~/HP3Media" DeletePaths="~/HP3Image,~/HP3Media" />
                                 
                                 
                                 <Links>
                                    <telerik:EditorLink Name="Convatec IT" Href="http://testconvatecit.healthware.it" Target="_blank">
                                    <telerik:EditorLink Name="Convatec US" Href="http://testconvatecus.healthware.it" Target="_blank" />               
                                   </telerik:EditorLink>
                                 </Links>
                                 <Links>
                                   <telerik:EditorLink Name="Healthware" Href="http://www.healthware.it" Target="_blank" ToolTip="Healthware portal" />
                                 </Links>        
                                                     
                            </telerik:RadEditor>
                    </td>
                </tr>
            </table>
            
            <div style="margin-top:10px">
                <input type="button" value="Cancel" class="button" title="Close current window" onClick="window.close()"/>                
                <asp:button ID="btnSalvaTesto" runat="server" text="Save" cssclass="button" tooltip="Save" onClick="Salva_Testo" />
            </div>
        </asp:panel>
        
        
    </form>
    </body>

</html>    