<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Quest.Quest"%>

<%@ Register TagPrefix="UC" Namespace="RadioButtonGroupName" Assembly="RadioButtonGroupName" %>

<script runat="Server">
    Private QuestId As Integer = 0
    Private QManager As New QuestionnaireManager()
      
    Sub Page_Load()
        If Not (Request("QuestId") Is Nothing) Then
            Dim questValue As QuestionnaireValue = QManager.Read(New QuestionnaireIdentificator(Request("QuestId")))
            If Not (questValue Is Nothing) Then QuestTitle.Text = "Questionnaire: " & questValue.Description
            
            BindQuestions()
        End If
    End Sub
       
    'recupera tutte le domande relazionate ad un questionario
    Sub BindQuestions()
        Dim questId As Integer = Request("QuestId")
        Dim questionColl As QuestionCollection
        
        If (questId <> 0) Then
            QManager.Cache = False
            questionColl = QManager.ReadQuestions(New QuestionnaireIdentificator(questId))
            rpQuestion.DataSource = questionColl
            rpQuestion.DataBind()
            If Not (questionColl Is Nothing) AndAlso (questionColl.Count > 1) Then
                rpQuestion.Visible = True
            Else
                noItems.Text = "There aren't questions related to the questionnaire!"
            End If
           
        End If
    End Sub
    
    'usato nel binding del repeater di livello 2
    Function GetAnswers(ByVal question As QuestionValue) As AnswerCollection
        Dim QuestManager As New QuestionManager
        Dim answers As AnswerCollection
        
        QuestManager.Cache = False
        answers = QuestManager.ReadAnswers(question.Key)
        Return answers
    End Function
    
    'setta la property per idenificare il tipo di domanda nel questionario
    Function IsSingleType(ByVal answer As AnswerValue) As Boolean
        Dim QuestManager As New QuestionManager
        Dim QValue As QuestionValue = QuestManager.Read(New QuestionIdentificator(answer.KeyQuestion.Id))
                  
        If Not (QValue Is Nothing) Then
            If (QValue.AnswerType = QuestionValue.Answer.SingleResponse) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    'setta la property per idenificare il tipo di domanda nel questionario
    Function IsMultipleType(ByVal answer As AnswerValue) As Boolean
        Dim QuestManager As New QuestionManager
        Dim QValue As QuestionValue = QuestManager.Read(New QuestionIdentificator(answer.KeyQuestion.Id))
                  
        If Not (QValue Is Nothing) Then
            If (QValue.AnswerType = QuestionValue.Answer.MultipleChoice) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    'setta la property per idenificare il tipo di domanda nel questionario
    Function IsOpenedType(ByVal question As QuestionValue) As Boolean
        If question.AnswerType = QuestionValue.Answer.OpenendedAnswer Then
            Return True
        End If
        
        Return False
    End Function
    
    'True, se il tipo di risposta � Other (cio� una risposta ti tipo testo)
    Function IsOtherType(ByVal answer As AnswerValue) As Boolean
        If (answer.Type = AnswerValue.TypeAnswer.Other) Then
            Return True
        End If
        
        Return False
    End Function
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Preview Questionnaire</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="questionnaireForm" runat="server">
	        <div id="divQuest" style="margin-left:50px" runat="server">	
	        <div style="font-weight:bold"><asp:literal id="QuestTitle" runat="server"></asp:literal></div>
	        <div style="text-align:center;font-weight:bold;margin-top:30px"><asp:literal id="noItems" runat="server"></asp:literal></div>	
		        
		        <asp:Repeater id="rpQuestion" runat="server">
	                    <HeaderTemplate></HeaderTemplate>
	                    <ItemTemplate>
	                        <div style="margin-top:15px"><li><strong><%#DataBinder.Eval(Container.DataItem, "Order")%>) <%#DataBinder.Eval(Container.DataItem, "Description")%></strong></li></div>
    	                    <div class="clearer">
    	                          <asp:Literal id="KeyFquestion" Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' runat="server" Visible="false"></asp:Literal>
    	                          <asp:TextBox id="Frt" TextMode="MultiLine" runat="server"  Width="300px"  Height="100px" Visible='<%#IsOpenedType(Container.DataItem)%>'></asp:TextBox>
	                        <asp:Repeater id="rpAnswers" runat="server"  DataSource='<%#GetAnswers(Container.DataItem)%>'>
	                                <HeaderTemplate></HeaderTemplate>
	                                <ItemTemplate>
	                                    <div style="margin-left:40px">
	                                        <%--<input type="radio" name="RadioButt" id="Radio"   runat="server" visible="<%#IsSingleType(Container.DataItem)%>" /> --%>
	                                        <UC:GroupRadioButton Id="radioAnswerItem" runat="server" GroupName='radioAnswer' Text='<%#DataBinder.Eval(Container.DataItem, "Key.Id")%>' Visible="<%#IsSingleType(Container.DataItem)%>"/> 
	                                        <input type="checkbox" name="ChkButton" id="Chk"  runat="server"  visible="<%#IsMultipleType(Container.DataItem)%>"/>
	                                        <%#DataBinder.Eval(Container.DataItem, "Description")%> <br />
	                                            <asp:TextBox id="AnswFrt" TextMode="MultiLine" runat="server"  Width="300px"  Height="50px" Visible='<%#IsOtherType(Container.DataItem)%>'></asp:TextBox>
	                                    </div>
	                                <div class="clearer">
                                    </ItemTemplate>
	                               <FooterTemplate><div class="clearer"></div></FooterTemplate>
                           </asp:Repeater> 
	   	             </ItemTemplate>
	              <FooterTemplate><div class="clearer"></div></FooterTemplate>
              </asp:Repeater> 
        </div>
    </form>
</body>
</html>

