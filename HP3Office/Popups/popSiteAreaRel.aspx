<%@ Page Language="VB" EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="System.Collections.Generic"%>

<%@ Register tagprefix="HP3" TagName ="Text" Src ="/hp3Office/HP3Parts/ctlText.ascx" %>

<script runat="Server">
    Private ContentId As Integer = 0
    Private LangId As Integer = 0
    Private strJS As String = ""
    
    Sub Page_Load()

        If Not Request("ContentId") Is Nothing Then
            ContentId = Request("ContentId")
        End If

        If Not Request("LangId") Is Nothing Then
            LangId = Request("LangId")
        End If
        
        If Not Me.IsPostBack Then
            LoadSiteArea()
        End If
        
    End Sub

    Sub LoadSiteArea()
        Me.BusinessContentManager.Cache = False
        Dim areaList As SiteAreaCollection = Me.BusinessContentManager.ReadContentSiteArea(New ContentIdentificator(ContentId, LangId))

        If Not (areaList Is Nothing) AndAlso (areaList.Count > 0) Then
            areaList = areaList.FilterBySiteAreaType(SiteAreaCollection.SiteAreaType.Area)
        End If
      
        gridList.DataSource = areaList
        gridList.DataBind()
    End Sub
    
    Function getRelationPriority(ByVal siteId As Integer) As Integer
        Me.BusinessContentManager.Cache = False
        
        Dim vo As ContentSiteAreaValue = Me.BusinessContentManager.ReadContentSiteAreaRelation(New ContentIdentificator(ContentId, LangId), New SiteAreaIdentificator(siteId))
        If Not (vo Is Nothing) Then
            Return vo.Priority
        End If
        
        Return 0
    End Function
    
    Sub SaveRelation(ByVal keyArea As Integer, ByVal priority As Integer)
        Dim vo As New ContentSiteAreaValue
        vo.KeyContent.Id = ContentId
        vo.KeySiteArea.Id = keyArea
           
        Dim bool As Boolean = Me.BusinessContentManager.RemoveContentSiteArea(vo)
      
        If bool Then
            vo.KeyContent.Id = ContentId
            vo.KeySiteArea.Id = keyArea
            vo.Priority = priority
            
            Me.BusinessContentManager.CreateContentSiteArea(vo)
        End If
        
        LoadSiteArea()
        ' End If
    End Sub
    
    Sub gridList_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
       
        If e.CommandName = "Relation" Then
            Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = Me.gridList.Rows(rowIndex)

            If Not row Is Nothing Then
                
                Dim lblSaID As Label = CType(row.FindControl("lblSaID"), Label)
                Dim txtRelW As TextBox = CType(row.FindControl("relW"), TextBox)
                
                If Not lblSaID Is Nothing AndAlso Not txtRelW Is Nothing Then
                    SaveRelation(lblSaID.Text, txtRelW.Text)
                    
                    strJS = "alert('Priority updated.')"
                End If
            
            Else
                strJS = "alert('Priority not updated. Try again!')"
            End If
        End If
        
    End Sub

</script>

 <script type ="text/javascript" >
   <%=strJS%>
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>SiteArea List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="siteareaForm" runat="server">
	    <asp:PlaceHolder ID="phScriptMan" runat="server" />
	       <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">SiteArea List</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                        <tr>
                            <td><strong></strong></td>    
                            <td><strong></strong></td>
                        </tr>
                        <tr>
                            <td><strong></strong></td>
                            <td><strong></strong></td> 
                        </tr> 
                        <tr>
                             <td><strong></strong></td>
                        </tr>
                    </table>
                    
               </fieldset>
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerStyle-HorizontalAlign="Right"
                            PagerStyle-CssClass="Pager"   
                            OnRowCommand ="gridList_RowCommand"     EnableViewState="true"   
                            >
                          <Columns >
                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="Id" >
                                <ItemTemplate>
                                    <asp:Label ID="lblSaID" runat="server" 
                                            Text='<%#CType(Container.DataItem, SiteAreaValue).Key.Id%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Name">
                                <ItemTemplate><%#CType(Container.DataItem, SiteAreaValue).Name%></ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Priority" SortExpression="Name">
                                <ItemTemplate>
                                    <asp:TextBox ID="relW"  runat="server"  EnableViewState="true"
                                                 Text='<%#getRelationPriority(Container.Dataitem.Key.Id)%>' />
                                </ItemTemplate>
                            </asp:TemplateField> 

                            <asp:ButtonField ButtonType="Image" CommandName="Relation"   
                                             ImageUrl="~/HP3Office/HP3Image/Button/save_content.gif"/>                            
                         </Columns>
                         <EmptyDataTemplate>
                              No item available.
                         </EmptyDataTemplate>
            </asp:gridview>
        </div> 
    </form>
</body>
</html>

<script type="text/javascript">
</script>