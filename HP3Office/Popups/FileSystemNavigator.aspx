<%@Page Language="VB" debug="true"  ValidateRequest="false"%>

<script runat="Server">
    'True Seleziona File
    'False Seleziona Folder
    Private _TypeSel As Boolean
    Private _StartFolder As String
    Private DestControl As String
    Private curNodeSel As String
    Private curItemSel As String
    
	Dim PrecSelection As String
    Dim strJs As String
    
    Property Nodes()
        Get
            Return ViewState("Nodes")
        End Get
        
        Set(ByVal value)
            Try
                           
                Dim arr As ArrayList
                
                If ViewState("Nodes") Is Nothing Then
                    arr = New ArrayList
                Else
                    arr = ViewState("Nodes")
                End If
             
                If Not arr.Contains(CStr(value)) Then
                    arr.Add(CStr(value))
                 
                    ViewState("Nodes") = arr
                End If
                
            Catch ex As Exception
                Response.Write(ex.ToString)
            End Try
        End Set
    End Property
    
    Sub RecuperaParametri()
        _TypeSel = IIf(Request("TypeSel") Is Nothing, True, Request("TypeSel"))
        DestControl = IIf(Request("ctl") Is Nothing, "", Request("ctl"))
        
        If _TypeSel Then
            litTitle.Text = "Selezionare il file"
        Else
            litTitle.Text = "Selezionare il folder"
        End If
        
        '#M1330 _StartFolder = Server.MapPath("\HP3")
        _StartFolder = Server.MapPath("\")
    End Sub
    
  
    Sub Page_Load()
        Try
            PrecSelection = txtHidden.Text
            
            RecuperaParametri()
                    
            LoadTree()
            
            
            
        Catch ex As Exception
            writeMsg(ex.ToString)
        End Try
    End Sub
    
    
    Sub writeMsg(ByVal strMsg As String)
        lblMsg.Text = strMsg
        lblMsg.Visible = True
    End Sub
    
    Function isFile(ByVal Path As String) As Boolean
        Try
            Return IO.File.Exists(Path)
        Catch ex As Exception
            Return False
        End Try
    End Function
    
    Sub LoadTree()
        Try
            If pMain.Controls.Count > 0 Then pMain.Controls.Clear()
            
            pMain.Controls.Add(BuildTree(_StartFolder))
            
            Dim bIsFile As Boolean = isFile(curItemSel)
            img_Sel.Visible = False
            img_Fld.Visible = False
            
            If curNodeSel <> "" Then
                
                
                If Not bIsFile Then
                    
                    img_Sel.Visible = Not _TypeSel
                    
					'img_Sel.Alt = "Seleziona il percorso """ & curItemSel & """"
                    img_Sel.Attributes.Add("onclick", "RadioClick('" & curItemSel.Replace(_StartFolder, "").Replace("\", "~") & "','" & curNodeSel & "')")
                    
                    img_Fld.Visible = True
					'img_Fld.Alt = "Aggiunge una nuova directory a """ & curItemSel & """"
                    
                    
                    img_Fld.Attributes.Add("onclick", "ShowTxtFolder('" & curNodeSel & "')")
                Else
                    img_Sel.Visible = True
					'img_Sel.Alt = "Seleziona il file """ & curItemSel & """"
                    img_Sel.Attributes.Add("onclick", "RadioClick('" & curItemSel.Replace(_StartFolder, "").Replace("\", "~") & "','" & curNodeSel & "')")
                End If
                
                
            End If
                        
        Catch ex As Exception
            writeMsg(ex.ToString)
        End Try
        
    End Sub
    
    Sub GestClick(ByVal s As Object, ByVal e As EventArgs)
        
        If ExistsNodo(s.commandargument) Then
            CType(Nodes, ArrayList).Remove(s.commandargument)
        Else
            Nodes = s.commandargument
        End If
        
        curNodeSel = s.commandargument
        curItemSel = s.commandname
        
        LoadTree()
    End Sub
    
    Sub GestImageClick(ByVal s As Object, ByVal e As ImageClickEventArgs)
        If ExistsNodo(s.commandargument) Then
            CType(Nodes, ArrayList).Remove(s.commandargument)
        Else
            Nodes = s.commandargument
        End If
        
        curNodeSel = s.commandargument
        curItemSel = s.commandname
        
        LoadTree()
    End Sub
    
    Function ExistsNodo(ByVal id As String) As Boolean
        If Nodes Is Nothing Then Return False
            
        Return Nodes.Contains(id)
    End Function
    
    Function creaNodo(ByVal key As String, ByVal Valore As String, ByVal idKey As String, Optional ByVal isFile As Boolean = False) As Panel
        Dim objLinkText As LinkButton
        Dim objLinkSign As LinkButton      
        Dim objLit As Label
        Dim objImg As ImageButton
        Dim fileName As String
        Dim mainPanel As New Panel
        Dim innerP As PlaceHolder
        Dim objAnchor As HtmlAnchor
        Dim oRadio As HtmlInputRadioButton
        Dim objLink As Image
        Try
                      
            
            mainPanel.ID = "div_" & idKey
            mainPanel.CssClass = "dControl"
           
            'Ancora***********************
            objAnchor = New HtmlAnchor
            objAnchor.Name = "#" & idKey
            mainPanel.Controls.Add(objAnchor)
            '*****************************         
                       
            objLinkText = New LinkButton
            If Not isFile AndAlso HasSons(key) Then              
                ''*****************************************
                
                objLinkSign = New LinkButton
                
                'Segno**************************                
                objImg = New ImageButton
                
                If ExistsNodo(idKey) Then
                    fileName = "/hp3Office/hp3image/tree/ico_15x_minus.gif"
                Else
                    fileName = "/hp3Office/hp3image/tree/ico_15x_plus.gif"
                End If
                
                objImg.ID = "Sign_" & idKey
                objImg.ImageUrl = fileName
                objImg.CssClass = "imgPlusMinus"
                objImg.CommandArgument = idKey
                objImg.CommandName = key
                
                objImg.Attributes.Add("onMouseOver", "selDiv('" & idKey & "')")
                objImg.Attributes.Add("onMouseOut", "UnselDiv()")
                
                AddHandler objImg.Click, AddressOf GestImageClick
                mainPanel.Controls.Add(objImg)
                '*****************************
                
                'Immagine********************* 
                objImg = New ImageButton
                If ExistsNodo(idKey) Then
                    fileName = "/hp3Office/hp3image/tree/ico_16x_folder_open.gif"
                Else
                    fileName = "/hp3Office/hp3image/tree/ico_16x_folder_closed.gif"
                End If
                objImg.ImageUrl = fileName
                objImg.ID = "img_" & idKey
                objImg.CssClass = "image"
				                
                objImg.Attributes.Add("onMouseOver", "selDiv('" & idKey & "')")
                objImg.Attributes.Add("onMouseOut", "UnselDiv()")
                objImg.CommandArgument = idKey
                objImg.CommandName = key
                AddHandler objImg.Click, AddressOf GestImageClick
                mainPanel.Controls.Add(objImg)
                '*****************************
                
                'Testo************************                
                objLinkText.Text = " " & Valore
                objLinkText.ID = "txtValore_" & idKey
                objLinkText.CssClass = "itemText"
                objLinkText.CommandArgument = idKey
                objLinkText.CommandName = key                
                objLinkText.Attributes.Add("onMouseOver", "selDiv('" & idKey & "')")
                objLinkText.Attributes.Add("onMouseOut", "UnselDiv()")
                objLinkText.Style.Add("text-decoration", "none")
                
                If curNodeSel = idKey Then                    
                    mainPanel.CssClass = "dControlSelected"
                End If
                
                AddHandler objLinkText.Click, AddressOf GestClick
                mainPanel.Controls.Add(objLinkText)
                '*****************************         
               
                
            Else

                'Immagine**************************
                objImg = New ImageButton
                If isFile Then
                    fileName = "/hp3Office/hp3image/tree/ico_16x_file.gif"
                Else
                    fileName = "/hp3Office/hp3image/tree/ico_16x_folder_closed.gif"
                                     
                End If                              
				
                Dim img As New Image
				
                img.ImageUrl = "/hp3Office/hp3image/tree/spacer.gif"
                img.Width = Unit.Pixel(15)
                img.CssClass = "imgPlusMinus"
                mainPanel.Controls.Add(img)
				
				
                objImg.ImageUrl = "/hp3Office/hp3image/tree/ico_16x_new_folder.gif"
                objImg.ID = "img_" & idKey
                objImg.CssClass = "image"
                mainPanel.Controls.Add(objImg)
                '**********************************
                
                objImg.ImageUrl = fileName
                objImg.ID = "img_" & idKey
                objImg.CssClass = "image"
                mainPanel.Controls.Add(objImg)
                '*********************************
                            
                'Testo************************                
                objLinkText.Text = " " & Valore
                objLinkText.ID = "txtValore_" & idKey
                objLinkText.CssClass = "itemText"
				
                objLinkText.CommandArgument = idKey
                objLinkText.CommandName = key
                'objLinkText.Attributes.Add("onMouseOver", "selDiv(this,'" & idKey & "')")
                objLinkText.Attributes.Add("onMouseOver", "selDiv('" & idKey & "')")
                objLinkText.Attributes.Add("onMouseOut", "UnselDiv()")
                objLinkText.Style.Add("text-decoration", "none")
                If curNodeSel = idKey Then
                    'objLinkText.ForeColor = setColor
                    mainPanel.CssClass = "dControlSelected"
                End If
                
                
                AddHandler objLinkText.Click, AddressOf GestClick
                mainPanel.Controls.Add(objLinkText)
                '*****************************        
            End If
            
            If Not isFile Then
                Dim divAdd As New Panel
                Dim txtAdd As New TextBox
                Dim btnAdd As New Button
                
                'divAdd.CssClass = "dInnerControl"
            
                divAdd.Width = Unit.Percentage(100)
                divAdd.BackColor = Drawing.Color.White
                
                txtAdd.ID = "txtAddFolder_" & idKey
                txtAdd.Width = Unit.Pixel(150)
                btnAdd.ID = "btnAddFolder_" & idKey
                btnAdd.CssClass = "button"
                btnAdd.CommandArgument = key
                btnAdd.CommandName = idKey
                btnAdd.ToolTip = "Crea il percorso nella directory selezionata"
                btnAdd.Text = "Aggiungi"
                
                AddHandler btnAdd.Click, AddressOf addfolder
                
                objLink = New Image                
                objLink.ID = "closeFolder_" & idKey
                objLink.ImageUrl = "/hp3Office/hp3image/tree/ico_16x_disable.gif"
                objLink.ToolTip = "Chiudi il pannello"
                
                objLink.Attributes.Add("onclick", "ShowTxtFolder('" & idKey & "')")
                
                divAdd.ID = "divAddFolder_" & idKey
                divAdd.Controls.Add(txtAdd)
                divAdd.Controls.Add(btnAdd)
                divAdd.Controls.Add(objLink)
                
                divAdd.CssClass = "divAddFolder"
                mainPanel.Controls.Add(divAdd)
            End If
            
            'PlaceHolder           
            innerP = New PlaceHolder
            mainPanel.Controls.Add(innerP)
            '******************************
            Return mainPanel
        Catch ex As Exception
            writeMsg(ex.ToString)
        End Try
        
    End Function
    
    Sub addfolder(ByVal s As Object, ByVal e As EventArgs)
        Try
            Dim ctl As Object
            curNodeSel = s.commandname
            curItemSel = s.commandargument
            ctl = FindControl("txtAddFolder_" & s.commandname)
        
            If Not ctl Is Nothing Then
                If ctl.text <> "" Then
                    Dim newPath As String = s.commandargument & "\" & ctl.text
                    Dim fld As New IO.DirectoryInfo(newPath)
                
                    If IO.Directory.Exists(newPath) Then
                        strJs = "alert('La directory inserita gia esiste')"
                    Else
                        IO.Directory.CreateDirectory(newPath)
                        
                        LoadTree()
                    End If
                
                End If
            End If
        
        Catch ex As Exception
            writeMsg(ex.ToString)
        End Try
        'strJs = "alert('" & s.commandargument & "')"
    End Sub
     
    Function HasSons(ByVal strFolder)
        Dim oFolder As New IO.DirectoryInfo(strFolder)
        
        If _TypeSel Then
            Return oFolder.GetDirectories.Length > 0 OrElse oFolder.GetFiles.Length > 0
        Else
            Return oFolder.GetDirectories.Length > 0
        End If
            
    End Function
    
    Function BuildTree(ByVal Path As String, Optional ByVal Key As String = "", Optional ByVal nProg As Int32 = 0) As Panel
        Try
            
            
            Dim oFolder As New IO.DirectoryInfo(Path)
            Dim oFiles As IO.FileInfo
            Dim oSubFolder As IO.DirectoryInfo
            Dim bExist As Boolean
            Dim objPanel As New Panel
            Dim innerP As PlaceHolder
            Dim innerKey As Int16 = 0
            
            nProg += 1
            If Key = "" Then
                Key = nProg
            Else
                Key += "_" & nProg
            End If
            
            innerKey = 0
            
            objPanel.CssClass = "dGroup"
            'objPanel.ID = "div_" & Key
            
            For Each oSubFolder In oFolder.GetDirectories
                innerKey += 1
                
                bExist = ExistsNodo(Key & "_" & innerKey)
                
                objPanel.Controls.Add(creaNodo(oSubFolder.FullName, oSubFolder.Name, Key & "_" & innerKey))
                
                innerP = New PlaceHolder
                innerP.ID = "p_" & Key & "_" & innerKey
                
                If bExist Then
                    innerP.Controls.Add(BuildTree(oSubFolder.FullName, Key, innerKey - 1))
                End If
                
                objPanel.Controls.Add(innerP)
            Next
            
            If _TypeSel Then
                For Each oFiles In oFolder.GetFiles
                    innerKey += 1
                
                    objPanel.Controls.Add(creaNodo(oFiles.FullName, oFiles.Name, Key & "_" & innerKey, True))
                              
                Next
            End If
            
            Return objPanel
        Catch ex As Exception
            writeMsg(ex.ToString)
        End Try
    End Function
    
	Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
		Dim ctl As Object
		If txtHidden.Text <> "" Then
			ctl = FindControl("txtValore_" & txtHidden.Text)
			If Not ctl Is Nothing Then
				Response.Write(ctl.commandname)
			End If
		End If
	End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><asp:Literal id ="litTitle" runat="server" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />	
	<link href="../_css/popup.grp.css" rel="stylesheet" type="text/css" />	
	<link href="../_css/popups/file_system.css"  rel="stylesheet" type="text/css" />
	
</head>

<body>
<div style="text-align:left ">
	</div>
	<form id="Form1" runat="server">
	    <asp:textbox ID="txtHidden" runat="server" visible="false"/>
	    <asp:Label ID="lblMsg" runat="server" visible ="false" ForeColor ="red" />
	    <div class="dTreeContainerBorder">
		    <div class="dTreeContainer" >
				<asp:placeHolder ID="pMain" runat ="server" />
			</div>
		</div>
	    <div class="dToolbar">
	        <table cellpadding="0" cellspacing="0" width ="100%" >
	        <tr>
	            <td>	               
					<input type="button" id="img_Sel" value="Select" runat="server" />	                    
                    <input type="button" id="img_Fld" value="New Folder" runat="server" />
	            </td>
	            <td class="tRight">	                
	                <input type="button" class="btnCancelWText"  onclick="window.close()" value="Cancel" />	                
	            </td>
	        </tr> 
	        </table>
	    </div>	    
			<asp:Button id="btnSave" Cssclass="bigButton" OnClick="btnSave_OnClick" Text="Save" runat="server" Visible ="false"/>
	</form>
</body>
</html>	

<script type="text/javascript">
    
    function gestClick(key)        
        {
            var index = key.id.indexOf('_')
            var key = key.id.substring(index + 1)
            var oDiv = document.getElementById ('div_' + key)
            
            if (oDiv != null)
                {
                    var aAncor = document.getElementById ('a_' + key)
                    if (oDiv.style.display == '')
                        {
                            oDiv.style.display ='none';
                            aAncor.innerText = '+';
                        }
                    else
                        {
                            oDiv.style.display ='';
                            aAncor.innerText = '-';
                        }
                }
        }
</script>
<script type="text/javascript">
    
    var curTxtOpen
    var curDiv
    var curDivSel
    var colorOpen = 'Red'
    var colorSel = 'Yellow'
    var ItemSel
    
	    function openDiv(id)
	        {
	        
	            obj = document.getElementById ('div_' + id)
	            objAnchor = document.getElementById ('a_' + id)
	            objMain =document.getElementById ('main_' + id) 
	            if ((obj != null) && (objAnchor != null))
	                {
	                    if (obj.style.display=='')
	                        {
	                            obj.style.display='none'
	                            objMain.style.background=  'white'
	                            objAnchor.innerText = '+'
	                        }
	                    else
	                        {
	                            obj.style.display=''
	                            objMain.style.background=  colorOpen
	                            if (curDiv != null) curDiv.style.background=  'white'
	                            curDiv = objMain
	                            objAnchor.innerText = '-'
	                        }
	                }
	        }
	    
	 	    function selDiv(id)
	        {
	         	            
	            if (id == '<%=iif(curNodeSel="",0,curNodeSel)%>') return
	                
	            
	            if (curDivSel != null) curDivSel.style.color=  'black'       
	            	           
	        }     
	     
	   
	   function UnselDiv()
	    {
	        if (curDivSel != null) curDivSel.style.color=  'black' 
	    }   
	    
	   function Wait()
	    {
	        document.getElementById ('divMain').style.display ='none'
	        document.getElementById ('divWait').style.display =''
	    }
	    
	    function Normal()
	    {
	        document.getElementById ('divMain').style.display =''
	        document.getElementById ('divWait').style.display ='none'
	    }
	    
	    function RadioClick(path,id)
	        {		        
	        
			    if( confirm("Procedere con la selezione ?") == true) 
	                {
	                    while (path.indexOf('~') != -1)
	                        {
	                            path = path.replace("~","/" )
	                        }
	                  
	                  window.opener.addPath(path.substring(1,path.length),'<%=DestControl%>') 
	                 
	                  window.close();	                 
	                 	                 
                }
	        }
	        
	    function moveTo(id)
	        {
	        
	            location.href = '#' + id    
	        }
	    
	    function ShowTxtFolder(key)    
	        {
	            var obj = document.getElementById ('divAddFolder_' + key)
	            
	            if (obj != null)
	                {	                   
	                    if (obj.className == '')
	                        obj.className = 'divAddFolder';
	                    else
	                        {
	                        
	                            obj.className ='';
	                            if ((curTxtOpen != null) && (curTxtOpen != key))
	                                {
	                                    document.getElementById ('divAddFolder_' + curTxtOpen).className = 'divAddFolder'
	                                }
	                                
	                            curTxtOpen  = key;
	                            moveTo(key);
	                        }   
	                }
	        }
	        
	    function CloseDiv (key)    
	        {}
	        
	    moveTo('<%=iif(curNodeSel="",0,curNodeSel)%>')
	    
	    <%=strJs%>
	</script>