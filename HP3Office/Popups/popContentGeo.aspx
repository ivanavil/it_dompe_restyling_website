<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>

<%@ Import Namespace="Healthware.HP3.Core.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>

<%--<%@ Register tagprefix="HP3" TagName ="Text" Src ="~/hp3Office/HP3Parts/ctlText.ascx" %>--%>

<script runat="Server">
    Dim arrMatch() As String
    Dim bSingleSel As Boolean = False
    Dim nFirstId As Int32
    Dim ListId, HiddenId As String
    
    Private ogeSearcher As GeoSearcher
    Dim geoManager As New GeoManager
    
    Private utility As New WebUtility
    Const maxRow As Integer = 1
           
    Sub Page_Load()
        If Not Request("HiddenId") Is Nothing Then
            HiddenId = Request("HiddenId")
        End If
        
        If Not Request("ListId") Is Nothing Then
            ListId = Request("ListId")
        End If
        
        
        If Not Request("geos") Is Nothing Then
            arrMatch = Request("geos").Split(",")
        End If
		
        
        If Not Request("SingleSel") Is Nothing AndAlso Request("SingleSel") = 1 Then
            bSingleSel = True
        End If
                    
        If Request("sites") <> "" And bSingleSel Then
            txtHidden.Text = "chk_" & ReadLabel(Request("geos")).Description & "_" & Request("geos")
        End If
        
        If Not Page.IsPostBack Then
            BindComboType()
            LoadFatherControl()
        End If
        
        Dim geoColl As geoCollection = Read()
        
        gridList.DataSource = geoColl
        gridList.DataBind()
    End Sub
        
    Function Read() As geoCollection
        Dim geoColl As geoCollection
        
        Dim geoSearcher As New geoSearcher
         
        geoColl = geoManager.Read(geoSearcher)
        Return geoColl
    End Function
    
    Function ReadLabel(ByVal id As Integer) As geoValue
        Dim geoSearcher As New geoSearcher
        geoSearcher.id = id
        Dim geoColl As geoCollection = geoManager.Read(geoSearcher)
        
        If Not (geoColl Is Nothing) AndAlso (geoColl.Count > 0) Then
            Return geoColl(0)
        End If
        
        Return Nothing
    End Function
    
   	
    Function setVisibility(ByVal id As Int32) As String
        Dim nCollection = Read()
        If nCollection Is Nothing OrElse nCollection.count = 0 Then Return "style='display:none'"
        Return ""
    End Function
	
    Sub btnSave_OnClick(ByVal s As Object, ByVal e As EventArgs)
        Dim el As Object
        Dim strOut As String = ""
		
        For Each el In Request.Params
            If el.startsWith("chk_") Then
                strOut += el & "#"
            End If
        Next
 
        Dim strJs As String
        strJs = "<Script>"
        
        If strOut = "" Then
            strJs += "alert('Select an Item to continue.');</s" + "cript>"
        ElseIf Not bSingleSel Then
            strJs += "window.opener.AddItems('" & strOut & "','" & ListId & "','" & HiddenId & "');window.close();</s" + "cript>"
        Else
            'Questa gestione � fatta ad hoc per l'accoppiamento con i temi
            Dim arr() As String = strOut.Split("#")
            Dim arr2() As String = arr(0).Split("_")
            strJs += "window.opener.SetValue('" & HiddenId & "','" & ListId & "'," & arr2(2) & ",'" & Server.HtmlDecode(arr2(1)).Replace("'", "`").Replace("?", "\?") & "');window.close();</s" + "cript>"
        End If
        
        ClientScript.RegisterClientScriptBlock(Me.GetType, "js", strJs)
    End Sub
	
    Function fDisable(ByVal id As String) As String
        If Not bSingleSel Then
            If Array.IndexOf(arrMatch, id) <> -1 Then Return " disabled='disabled' "
        End If
        Return ""
    End Function
	
    Function fChecked(ByVal id As String) As String
        If arrMatch.Length > 0 AndAlso Array.IndexOf(arrMatch, id) <> -1 Then Return " checked='true' "
        Return ""
    End Function
    
    'metodo che si occupa della ricerca dei gruppi
    Sub Search(ByVal s As Object, ByVal e As EventArgs)
        BindWithSearch(gridList)
    End Sub
    
    Sub BindWithSearch(ByVal objGrid As GridView)
        ogeSearcher = New geoSearcher()
        
        If txtId.Value <> "" Then ogeSearcher.Id = CType(txtId.Value, Integer)
        If txtDescription.Value <> "" Then ogeSearcher.geoDescription = txtDescription.Value
                
        If Not drpThemeFather.SelectedItem Is Nothing AndAlso drpThemeFather.SelectedIndex <> 0 Then
            ogeSearcher.SonsOf = drpThemeFather.SelectedItem.Value
        Else
            If (dwlgeoType.SelectedValue <> -1) Then ogeSearcher.geoTypeId = dwlgeoType.SelectedItem.Value
        End If
                                
        BindGrid(objGrid, ogeSearcher)
    End Sub
    
    Sub BindGrid(ByVal objGrid As GridView, Optional ByVal Searcher As GeoSearcher = Nothing)
        If Searcher Is Nothing Then
            Searcher = New GeoSearcher
        End If
        
        Dim _ogeoColl As geoCollection = geoManager.Read(Searcher)
        If Not (_ogeoColl Is Nothing) Then
            ActiveGridView()
            
            objGrid.DataSource = _ogeoColl
            objGrid.DataBind()
        End If
    End Sub
    
    'gestisce la paginazione
    Protected Sub gridList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        sender.PageIndex = e.NewPageIndex
        
        BindWithSearch(sender)
    End Sub
         
    'rende visibile il GridView
    Sub ActiveGridView()
        gridList.Visible = True
      
    End Sub
    
    'rende visibile il Repeater
    Sub ActiveRepeater()
        gridList.Visible = False
       
    End Sub
    
    Sub LoadFatherControl()
        Dim GeoM As New GeoManager
        Dim geoType As New GeoSearcher
        Dim geoColl As New GeoCollection
        
        
        geoType.geoTypeDescription = dwlgeoType.SelectedItem.Text
        geoColl = GeoM.Read(geoType)
        
        Dim oWebUtil As New WebUtility
       
        
        'Combo dei themi per la selezione del padre
        drpThemeFather.Items.Clear()
        oWebUtil.LoadListControl(drpThemeFather, geoColl, "Description", "Key.Id")
        drpThemeFather.Items.Insert(0, New ListItem("---Select Father---", 0))
    End Sub
    
    Sub BindComboType()
            
        Dim _oGeoManager = New GeoManager
       
        
        Dim oWebUtility As New WebUtility
        Dim geoTypeColl As GeoTypeCollection = _oGeoManager.ReadType()
        
        oWebUtility.LoadListControl(dwlgeoType, geoTypeColl, "Description", "Id")
        dwlgeoType.Items.Insert(0, New ListItem("---Select Type---", -1))
       
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Geo List</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>

    <body>
	    <form id="geosForm" runat="server">
	        <asp:textbox id="txtHidden" runat="server" width="100%"  style="display:none"/>
            <asp:Panel id="pnlSearcher" cssclass="boxSearcher" runat="server"  Visible="true">
                 <table class="topContentSearcher">
                    <tr>
                        <td style="border:0px solid #ffffff;width:25px;background-image:url('/hp3Office/HP3Image/Ico/content_searcher.gif');background-repeat:no-repeat;"></td>
                        <td style="height:24px;border:0px solid #ffffff;" valign="middle"><span class="title">Geo Searcher</span></td>
                    </tr>
                </table>
                <fieldset class="_Search">
                    <table class="form">
                       <tr>
                            <td><strong>ID</strong></td>    
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                               <td><input id="txtId" type="text" runat="server"  style="width:50px"/></td>  
                              <td><input id="txtDescription" type="text" runat="server"  style="width:300px"/></td>
                       </tr>
                       <tr>
                            <td><strong>Type</strong></td>
                            <td><strong>Father</strong></td>
                        </tr>
                        <tr>
                              
                              <td><asp:DropDownList id="dwlgeoType" runat= "server" AutoPostBack="true" OnSelectedIndexChanged="LoadFatherControl" /></td>
                              <td><asp:dropdownlist ID="drpThemeFather" runat="server" /></td>
                       </tr>
                        <tr>
                            <td><asp:button runat="server" CssClass="button" ID="btnSearch" onclick="Search" Text="Search" style="margin-top:10px;padding-left:16px;width:80px;background-image:url('/HP3Office/HP3Image/Button/search_content.gif');background-repeat:no-repeat;background-position:1px 1px;"/></td> 
                        </tr>
                    </table>
               </fieldset>
               <hr />
            </asp:Panel>
        
            <div id="divGrid">
                <asp:gridview ID="gridList" 
                            runat="server"
                            AutoGenerateColumns="false" 
                            GridLines="None" 
                            CssClass="tbl1"
                            HeaderStyle-CssClass="header"
                            AlternatingRowStyle-BackColor="#E9EEF4"
                            AllowPaging="true"
                            PagerSettings-Position="Top"  
     
                            PagerStyle-CssClass="Pager2"             
                            AllowSorting="true"
                            PageSize ="8"
                            emptydatatext="No item available"                
                            OnPageIndexChanging="gridList_PageIndexChanging"
                           >
                          <Columns >
                             <asp:TemplateField HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                       <span    >
                                        <%If Not bSingleSel Then%>
                                            <input <%#fDisable(container.DataItem.Key.Id)%> <%#fChecked(container.DataItem.Key.Id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.Id%>' type="checkbox"/>
                                        <%Else%>
                                            <input  <%#fChecked(container.DataItem.Key.Id)%>  name = 'chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.Id%>' onclick="gestClick('chk_<%#Container.DataItem.Description.Replace("'", "`")%>_<%#Container.DataItem.Key.Id%>')" type="radio"/>
                                        <%end if%>
                                       </span> 
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Id">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Key.Id")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="90%" HeaderText="Description">
                                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                            </asp:TemplateField>
                     
                         </Columns>
            </asp:gridview>
        </div> 
                 
        
        <div style="padding-left:7px;margin-top:15px">
               <asp:Button id="btnSave" Cssclass="button"  OnClick="btnSave_OnClick" Text="Select Item" runat="server"/>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">

    var ItemsCount = 2
    if (ItemsCount == 1){
            gestSubItems(document.getElementById( 'a_<%=nFirstId%>'));
        }
    
	function gestSubItems(objSrc){
		id = objSrc.id.substring(objSrc.id.indexOf('_') + 1)
		if (objSrc.innerText=='+'){
				objSrc.innerText='-';
				document.getElementById('div_' + id).style.display='';
			}
		else{
				objSrc.innerText='+';
				document.getElementById('div_' + id).style.display='none';
			}
	    }
	
	function splitName(src){
	        if (src != ''){
	                var arr = src.split('_')
	                return arr[2];
	            }	        
	     }
	    
	    
	function gestClick(obj){	    
	        var oText = document.getElementById ('<%=txtHidden.clientid%>')
	       	var oText1 = document.getElementById(obj)
	        var ochk
	             
             if (oText.value!='') 
             {  
                ochk = document.getElementById (oText.value)
                if (ochk!=null)
                {
                    ochk.checked = false;
                }
            }
            oText.value= oText1.name;
            oText1.checked = true;
	    }
</script>