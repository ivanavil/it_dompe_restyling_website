<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Web"%>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility.CMS.Utility"%>
<%@ Import Namespace="Healthware.HP3.Core.Utility"%>   
<%@ Import Namespace="Healthware.HP3.Core.Community"%>
<%@ Import Namespace="Healthware.HP3.Core.Community.ObjectValues"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Linq"%>

<script runat="Server">
	Dim rowId as Integer = 0
	Dim userId as Integer = 0
	Dim strQuery As String = "readPartners"
	Dim _objCachedDataSet As DataSet = Nothing
	Dim oDt as DataTable = Nothing	
	
    Sub Page_Load()
		If Request("userId") <= 0 Then Response.End
		userId = request("userId")
		rowId = request("row")
		
		oDt = LoadPartner()
		If Not Page.IsPostBack Then 			
			'Filter List by PartnerId--------------------
			QueryRow()
		End If			
    End Sub
	
	Function LoadPartner() As DataTable
		Dim _cacheKey as String = "partnerDataSet"
		Dim oDt as DataTable = Nothing
		If request("cache") = "false" Then cacheManager.RemoveByKey(_cacheKey)
		
		_objCachedDataSet = cacheManager.Read(_cacheKey, 1)
		If _objCachedDataSet Is Nothing Then
			If Not String.IsNullOrEmpty(strQuery) Then
				Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQuery, False)
			
				If Not String.IsNullOrEmpty(query) Then
					Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, Nothing) 'Al posto di Nothing vanno  passati gli SqlParams
					If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
						_objCachedDataSet = ds
						'Cache Content and send to the oputput-------
						CacheManager.Insert(_cacheKey, _objCachedDataSet, 1, 120)
						'--------------------------------------------
					Else
						Return Nothing
					End If
				End If
			End If
		End If	

		Return  _objCachedDataSet.Tables(0)
	End Function
	
	Sub QueryRow()
		'ddlPartner.Items.Clear()
		'ddlCountry.Items.Clear()
		'ddlProduct.Items.Clear()
		'ddlTherapeutic.Items.Clear()
		
		'ddlPartner.Enabled = False
		'ddlCountry.Enabled = False
		'ddlProduct.Enabled = False
		'ddlTherapeutic.Enabled = False
			
		If userId > 0 Then
			If Not oDt Is Nothing Then
			

				
				'If Not productsArray Is NOthing Then
				'	rpPartnerData.DataSource = productsArray
				'	rpPartnerData.DataBind()
				'End if			

				Dim countries = (From row In oDt 
				Select row.Item("partner_country")).Distinct()			
				If Not countries Is NOthing AndAlso countries.Any Then
					For Each i In countries
						ddlCountry.Items.Add(new listitem(i, i))
					Next
				End If
				
				Dim dirPartner = (From row1 In oDt Where row1("partner_userId") = userId
				Select row1.Item("partner_directpartner")).Distinct()			
				If Not dirPartner Is NOthing AndAlso dirPartner.Any Then
					For Each i In dirPartner
						ddlPartner.Items.Add(new listitem(i, i))
					Next
				End If				
				
				Dim indirPartner = (From row2 In oDt 
				Select row2.Item("partner_indirectpartner")).Distinct()			
				If Not indirPartner Is NOthing AndAlso indirPartner.Any Then
					For Each i In indirPartner
						ddlIndPartner.Items.Add(new listitem(i, i))
					Next
				End If
				
				Dim at = (From row3 In oDt 
				Select row3.Item("partner_areaterapeutica")).Distinct()			
				If Not at Is NOthing AndAlso at.Any Then
					For Each i In at
						ddlAt.Items.Add(new listitem(i, i))
					Next
				End If				

				Dim brand = (From row4 In oDt 
				Select row4.Item("partner_brand")).Distinct()			
				If Not brand Is NOthing AndAlso brand.Any Then
					For Each i In brand
						ddlBrand.Items.Add(new listitem(i, i))
					Next
				End If	

				Dim prAt = (From row5 In oDt 
				Select row5.Item("partner_principioAttivo")).Distinct()			
				If Not prAt Is NOthing AndAlso prAt.Any Then
					For Each i In prAt
						ddlPa.Items.Add(new listitem(i, i))
					Next
				End If	
				
				
				Dim query = _
				From partner In oDt.AsEnumerable() _
				where partner("partner_id") = rowId And partner("partner_userid") = userId
				Select partner
				Dim rowArray = query.ToArray()
				If NOT rowArray Is Nothing Then
					ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(rowArray(0)("partner_country").ToString()))
					ddlPartner.SelectedIndex = ddlPartner.Items.IndexOf(ddlPartner.Items.FindByText(rowArray(0)("partner_directpartner").ToString()))
					ddlIndPartner.SelectedIndex = ddlIndPartner.Items.IndexOf(ddlIndPartner.Items.FindByText(rowArray(0)("partner_indirectpartner").ToString()))
					ddlAt.SelectedIndex = ddlAt.Items.IndexOf(ddlAt.Items.FindByText(rowArray(0)("partner_areaterapeutica").ToString()))
					ddlBrand.SelectedIndex = ddlBrand.Items.IndexOf(ddlBrand.Items.FindByText(rowArray(0)("partner_brand").ToString()))					
					ddlPa.SelectedIndex = ddlPa.Items.IndexOf(ddlPa.Items.FindByText(rowArray(0)("partner_principioAttivo").ToString()))					
				End If 
				
			End If
		Else
			'Redirect		
		End If	
	End Sub
	
	Sub btnSave_OnClick(sender as object, e as eventargs)
		Dim strQueryName As String = String.Empty
		If rowId > 0 Then 'Edit Mode
			strQueryName = "updatePartnerData"
		Else 'Create Mode
			strQueryName = "insertPartnerData"
		End If
		Dim query As String = Dompe.Helpers.Sql.ReadQuery(strQueryName, False)
		
		If Not String.IsNullOrEmpty(query) Then
			Dim partner_country As String = iif(Not String.IsNullOrEmpty(newCountry.Text.Trim), newCountry.Text.Trim, ddlCountry.SelectedItem.Value)
			Dim partner_directPartner As String = iif(Not String.IsNullOrEmpty(newPartner.Text.Trim), newPartner.Text.Trim, ddlPartner.SelectedItem.Value)
			Dim partner_indirectPartner	As String = iif(Not String.IsNullOrEmpty(newIndPartner.Text.Trim), newIndPartner.Text.Trim, ddlIndPartner.SelectedItem.Value)
			Dim partner_areaterapeutica As String = iif(Not String.IsNullOrEmpty(newAt.Text.Trim), newAt.Text.Trim, ddlAt.SelectedItem.Value)
			Dim partner_brand As String = iif(Not String.IsNullOrEmpty(newBrand.Text.Trim), newBrand.Text.Trim, ddlBrand.SelectedItem.Value)
			Dim partner_principioAttivo As String = iif(Not String.IsNullOrEmpty(newPa.Text.Trim), newPa.Text.Trim, ddlPa.SelectedItem.Value)
			Dim partner_userid As Integer = userId
			Dim partner_rowid As Integer = rowId
		
		
			Dim args = New List(Of SqlParameter)
			args.Add(New SqlParameter("@partner_country", partner_country))
			args.Add(New SqlParameter("@partner_directPartner", partner_directPartner))
			args.Add(New SqlParameter("@partner_indirectPartner", partner_indirectPartner))
			args.Add(New SqlParameter("@partner_areaterapeutica", partner_areaterapeutica))
			args.Add(New SqlParameter("@partner_brand", partner_brand))
			args.Add(New SqlParameter("@partner_principioAttivo", partner_principioAttivo))
			args.Add(New SqlParameter("@partner_userid", partner_userid))
			If rowId > 0 Then args.Add(New SqlParameter("@partner_rowid", partner_rowid))		
			Dim mIdNuovo As Integer = 0
			Dim ds As DataSet = Dompe.Helpers.Sql.ExecuteQuery(query, args.ToArray())
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dt = ds.Tables(0)
				If Not dt.Rows Is Nothing AndAlso 0 <> dt.Rows.Count Then
					Dim row = dt.Rows(0)
					mIdNuovo = row(0)
					If mIdNuovo > 0 Then 
						pnlOk.visible = True
						pnlForm.visible = False
					End If
				End If
			End If
		End If
		
	End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <title>Partnership details</title>
	    <link type="text/css" rel="stylesheet" href="../_css/consolle.css" />
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<style>
			td{padding:5px;}
			select {margin-right:10px}
			input {margin-left:5px;}
		</style>		
    </head>

    <body style="padding:30px;">
	<form id="userForm" runat="server">
		<asp:panel id="pnlForm" runat="server">
			<h3 style="margin-bottom:15px;">Gestione Partner</h3>
			<table cellpadding="0" cellspacing="0" style="border:0;">
				<tr>
					<td>Country:</td>
					<td><asp:dropdownlist id="ddlCountry" runat="server" /> Nuovo:<asp:textbox id="newCountry" runat="server" /></td>
				</tr>
				<tr>
					<td>Direct Partner:</td>
					<td><asp:dropdownlist id="ddlPartner" runat="server" /> Nuovo:<asp:textbox id="newPartner" runat="server" /></td>
				</tr>
				<tr>
					<td>Indirect Partner:</td>
					<td><asp:dropdownlist id="ddlIndPartner" runat="server" /> Nuovo:<asp:textbox id="newIndPartner" runat="server" /></td>
				</tr>
				<tr>
					<td>Area Terapeutica:</td>
					<td><asp:dropdownlist id="ddlAt" runat="server" /> Nuovo:<asp:textbox id="newAt" runat="server" /></td>
				</tr>
				<tr>
					<td>Brand:</td>
					<td><asp:dropdownlist id="ddlBrand" runat="server" /> Nuovo:<asp:textbox id="newBrand" runat="server" /></td>
				</tr>
				<tr>
					<td>Principio Attivo:</td>
					<td><asp:dropdownlist id="ddlPa" runat="server" /> Nuovo:<asp:textbox id="newPa" runat="server" /></td>
				</tr>			
			</table>
			<div style="padding-left:7px;margin-top:15px">
				<asp:Button id="btnSave" Cssclass="button" OnClick="btnSave_OnClick" Text="Save Item" runat="server"/>
			</div>
		</asp:panel>
				
		<asp:panel id="pnlOk" runat="server" visible="false">
			<div style="text-align:center;padding:20px;">
				Il salvataggio � avvunuto correttamente.<br />
				<input type="submit" class="button" id="btnClose" value="CLose" onclick="javascript:window.opener.location.reload();window.close();">
			</div>
		</asp:panel>
    </form>
</body>
</html>