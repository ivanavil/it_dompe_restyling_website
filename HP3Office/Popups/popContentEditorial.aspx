<%@ Page Language="VB" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Content"%>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues"%>
<%@ Register TagPrefix ="HP3" TagName="ctlContentEditorial" src="~/hp3Office/HP3Service/ContentEditorialManager.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public DomainInfo As String
    
    Private StrJs As String
    Sub SelItem(ByVal s As Object, ByVal e As EventArgs)
        Dim col As Object = ctlContentEditorial.GetSelection
        Dim ov As contenteditorialValue
        
        If Not col Is Nothing AndAlso col.count > 0 Then
            ov = col.item(0)
            
            If Not ov Is Nothing AndAlso Not Request("ctlHidden") Is Nothing AndAlso Not Request("ctlVisible") Is Nothing Then
                StrJs = "window.opener.SetValue('" & Request("ctlHidden") & "','" & Request("ctlVisible") & "'," & ov.Id & ",'" & Server.HtmlDecode(ov.Description).Replace("'", "`").Replace("?", "\?") & "')" & vbCrLf
                StrJs += "this.close()"
            End If
                      
        Else
            StrJs = "alert('Select an item to continue.')"
        End If
    End Sub
    
    Sub Page_Load()
        If Not Request("SelItem") Is Nothing AndAlso Not Page.IsPostBack Then
            ctlContentEditorial.SetDefaultChecked = Request("SelItem")
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Content Editorial Selector</title>    	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link type="text/css" rel="stylesheet" href="../_css/Default.grp.css" />		
	<script type="text/javascript" >
	    <%=StrJs%>
	</script>
</head>
<body>
    <form id="form1" runat="server">    
    <div style="text-align:left">
    <img src="../HP3Image/Logo/hp3logo.gif" />
    <HP3:ctlContentEditorial runat="server" ID="ctlContentEditorial" TypeControl="Selection" />
    <asp:button ID ="btnSel" runat="server" onclick="SelItem" Text="Select Item"/>
    </div>
    
    </form>
</body>
</html>
