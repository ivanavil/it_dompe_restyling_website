<%@ Control Language="VB" Debug="true" %>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content" %>
<%@ Import Namespace="Healthware.HP3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Box" %>
<%@ Import Namespace="Healthware.HP3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Web" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>

<script runat="server">
    '#Box_Start#
    Const BOX_ID_1 = 1
    Const BOX_ID_9 = 9
    Const BOX_ID_10 = 10
    '#Box_End#     		
	
    Dim oBoxManager As New BoxManager
    Dim oBoxContentNewsletterSearcher As BoxContentNewsletterSearcher
    Dim oBoxContentNewsletterCollection As BoxContentNewsletterCollection
    Dim oGenericUtility As New CMS.Utility.GenericUtility
    Dim newsletterManager As New NewsletterManager
    
    Function ReadDataAll(ByVal oBoxContentNewsletterSearcher As BoxContentNewsletterSearcher) As BoxContentNewsletterCollection
        oBoxManager.Cache = False
        Return oBoxManager.Read(oBoxContentNewsletterSearcher)
    End Function
    
    Private _idNl As Int32         
    Private _idlanguage As Int32
    
    Public Property idLanguage() As Int32
        Get
            Return _idlanguage
        End Get
        Set(ByVal value As Int32)
            _idlanguage = value
        End Set
    End Property
    
    Public Property idNL() As Int32
        Get
            Return _idNl
        End Get
        Set(ByVal value As Int32)
            _idNl = value
        End Set
    End Property
    
    Sub page_load()
        oBoxManager.Cache = False
        '**************************
        'Legge tutte le tipologie di contenuti associati ad un box
        'Prametri:Box_id, byVal, Id del box	    
        'Valore di ritorno:BoxContentNewsletterCollection
        'oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        'oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        'oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(<box_id>)
        'oBoxContentNewsletterCollection=ReadDataAll(oBoxContentNewsletterSearcher)
        '**************************
        oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(BOX_ID_1)
        oBoxContentNewsletterCollection = ReadDataAll(oBoxContentNewsletterSearcher)
        
        Box_1.DataSource = oBoxContentNewsletterCollection
        Box_1.DataBind()
        
        oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(BOX_ID_9)
        oBoxContentNewsletterCollection = ReadDataAll(oBoxContentNewsletterSearcher)
        
        Box_9.DataSource = oBoxContentNewsletterCollection
        Box_9.DataBind()
        
        
        oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(BOX_ID_10)
        oBoxContentNewsletterCollection = ReadDataAll(oBoxContentNewsletterSearcher)
        
        Box_10.DataSource = oBoxContentNewsletterCollection
        Box_10.DataBind()
        
        '*******************************
        'Legge tutti i contenuti di tipo content
        'Prametri:Box_id, byVal, Id del box
        'Prametri:Box_Type , byVal tipologia del box
        'Valore di ritorno:BoxContentNewsletterCollection
        'oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        'oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        'oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(<box_id>)
        'oBoxContentNewsletterSearcher.BoxContentType =BoxContentNewsletterValue.ContentBoxType.Content 
        'oBoxContentNewsletterCollection=ReadDataAll(oBoxContentNewsletterSearcher)
        '*******************************
	    
        '****************************
        'Legge tutti i contenuti di tipo testo libero
        'Prametri:Box_id, byVal, Id del box
        'Prametri:Box_Type , byVal tipologia del box
        'Valore di ritorno:BoxContentNewsletterCollection
        'oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        'oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        'oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(<box_id>)
        'oBoxContentNewsletterSearcher.BoxContentType =BoxContentNewsletterValue.ContentBoxType.Text 
        'oBoxContentNewsletterCollection=ReadDataAll(oBoxContentNewsletterSearcher)
        '****************************
	    
        '*************************
        'Descrizione:Legge tutti i contenuti di tipo Immagine/Banner
        'Prametri:Box_id, byVal, Id del box
        'Prametri:Box_Type , byVal tipologia del box
        'Valore di ritorno:BoxContentNewsletterCollection
        'oBoxContentNewsletterSearcher = New BoxContentNewsletterSearcher
        'oBoxContentNewsletterSearcher.KeyNewsletter = New NewsletterIdentificator(idNl)
        'oBoxContentNewsletterSearcher.KeyBox = New BoxIdentificator(<box_id>)
        'oBoxContentNewsletterSearcher.BoxContentType =BoxContentNewsletterValue.ContentBoxType.Banner 
        'oBoxContentNewsletterCollection=ReadDataAll(oBoxContentNewsletterSearcher)
        '*************************			
		
        '****************************************************
        'Descrizione:Recuperto il contentvalue dal BoxContentNewsletterValue
        'oContentValue as ContentValue = oGenericUtility.GetContentValue (BoxContentNewsletterValue.KeyContent.PrimaryKey)
        '****************************************************
        
        '*************************
        'BoxContentNewsletterValue.getBanner() Stampa l'immagine/banner associata
        '*************************	
    End Sub
	    
    Function ReadContentProperty(ByVal keyContent As ContentIdentificator, ByVal strProperty As String) As String
        Dim outVal As String = ""
        Dim oClassUtility As New ClassUtility
        Dim contentManager As New ContentManager
                
        Dim oContentValue As ContentValue = oGenericUtility.GetContentValue(keyContent.PrimaryKey)
       
        If Not oContentValue Is Nothing Then
            outVal = oClassUtility.GetPropertyValue(oContentValue, strProperty)
        End If
        
        Return outVal
    End Function
    
    'genera il link che gestisce il tracciamento sull'evento 'ClickThougt' per la newsletter.
    Function getLink(ByVal item As BoxContentNewsletterValue) As String
        Return newsletterManager.getLink(item)
    End Function
    
    'genera il link che gestisce il tracciamento sull'evento 'View' per la newsletter.
    Function getLink() As String
        Return newsletterManager.getLink(idNL)
    End Function
    
   </script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<title>MyNewsletter</title>
	<link href="http://netcaresupport.healthware.it/newsletter_data/netcare1/_css/interface.grp.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<img src='<%=getLink() %>' style="display:none" width="0" alt=""/>
   	<div id="page">
		<img src="http://hp3.healthware.it/HP3Newsletter/image/logo/hp3logo.gif"  style="margin-bottom:2px; margin-left:2px" alt="" />
        
		<div class="dIntro">
			 [Title]<strong> [Name] [Surname]</strong>, <br/><br/>			
	    </div>
	   
	    <img src="http://hp3.healthware.it/HP3Newsletter/image/logo/news_events.jpg"  style="margin-bottom:2px; margin-left:2px" alt="" />
	    <div style="border:1px solid #000">
	        
			<asp:repeater id="Box_1" runat="server">
				<ItemTemplate>
				    <div id="Div1" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Content%>">
				        <h3>CONTENT</h3>
				       <div><a href='<%#getLink(Container.DataItem) %>'><%#ReadContentProperty(Container.DataItem.keycontent, "Title")%></a></div>
				       <%#ReadContentProperty(Container.DataItem.keycontent, "Launch")%>
					</div>
					<div id="Div2" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Text%>">
					    <h3>FREETEXT</h3>
					    <div><a href='<%#getLink(Container.DataItem) %>'>My Test</a></div>
					    <%#Container.DataItem.FreeText%>
					</div>
					<div id="Div3" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Banner%>">
					    <h3>IMAGE/BANNER</h3>
					    <%#Container.DataItem.GETBANNER%>
					</div>
				</ItemTemplate>
			</asp:repeater>
		</div>
       
	    <div style="border:1px solid #000">
	    <asp:repeater id="Box_10" runat="server">
				<ItemTemplate>
				    <div id="Div4" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Content%>">
				        <h3>CONTENT</h3>
				        <a href='<%#getLink(Container.DataItem) %>'><%#ReadContentProperty(Container.DataItem.keycontent, "Title")%></a>
				        <%#ReadContentProperty(Container.DataItem.keycontent, "Launch")%>
					</div>
					<div id="Div5" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Text%>">
					    <h3>FREETEXT</h3>
					    <div><a href='<%#getLink(Container.DataItem) %>'>My Test</a></div>
					    <%#Container.DataItem.FreeText%>
					</div>
					<div id="Div6" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Banner%>">
					    <h3>IMAGE/BANNER</h3>
					    <%#Container.DataItem.GETBANNER%>
					</div>
				</ItemTemplate>
			</asp:repeater>
	    </div>
	   
	    <div style="border:1px solid #000">
	    <div style="background-color:#CBCEE1; color:#293364;font-size:18;font-family:Arial"><strong>Solution</strong></div>
        <asp:repeater id="Box_9" runat="server">
				<ItemTemplate>
				    <div id="Div1" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Content%>">
				        <h3>CONTENT</h3>
				        <div><a href='<%#getLink(Container.DataItem) %>'><%#ReadContentProperty(Container.DataItem.keycontent, "Title")%></a></div>
				        <%#ReadContentProperty(Container.DataItem.keycontent, "Launch")%>
					</div>
					<div id="Div2" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Text%>">
					    <h3>FREETEXT</h3>
					    <div><a href='<%#getLink(Container.DataItem) %>'>My Test</a></div>
					    <%#Container.DataItem.FreeText%>
					</div>
					<div id="Div3" style="border:1px solid #000" runat="server" visible="<%#Container.DataItem.BoxContentType=BoxContentNewsletterValue.ContentBoxType.Banner%>">
					    <h3>IMAGE/BANNER</h3>
					    <%#Container.DataItem.GETBANNER%>
					</div>
				</ItemTemplate>
			</asp:repeater>
	    </div>
		<div class="clearer"></div>
		<img class="bl" src="http://netcaresupport.healthware.it/newsletter_data/netcare1/_images/img_footer.gif" />
	</div>
	
	<div id="dDisclaimer">
		Healthware S.p.A. Qualora desiderasse non ricevere pi&ugrave; questa newsletter, la preghiamo di <Unsubscribe>.
		<br /><br />
		Le informazioni inserite nella newsletter sono fornite esclusivamente a titolo indicativo. 
		Healthware S.p.A. non rispondono in alcun modo dell'uso che venga fatto dei testi.
		<br />
	</div>

</body>
</html>
