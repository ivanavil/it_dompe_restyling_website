SELECT
	partner_id, partner_country, partner_directPartner, partner_indirectPartner, partner_areaterapeutica, partner_brand, partner_principioAttivo, partner_userid, user_name, user_surname, user_email, user_tel
FROM
	HP3_Dompe_Partner
	LEFT OUTER JOIN PP_User ON partner_userid = user_id