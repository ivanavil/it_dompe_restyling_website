UPDATE 
	HP3_Dompe_Partner
SET
	partner_country = @partner_country,
	partner_directPartner = @partner_directPartner,
	partner_indirectPartner = @partner_indirectPartner,
	partner_areaterapeutica = @partner_areaterapeutica,
	partner_brand = @partner_brand,
	partner_principioAttivo = @partner_principioAttivo,
	partner_userid = @partner_userid
WHERE
	partner_id = @partner_rowid

SELECT @partner_rowid 
