/*
DECLARE @partner_request_date DATETIME = NULL;
DECLARE @partner_request_fullname nvarchar(250) = '';
DECLARE @partner_request_phone nvarchar(50) = '';
DECLARE @partner_request_product nvarchar(50) = '';
DECLARE @partner_request_therapeutic nvarchar(50) = '';
DECLARE @partner_request_request NVARCHAR(MAX) = '';
DECLARE @partner_request_adverse NVARCHAR(5) = '';
DECLARE @partner_request_age NVARCHAR(5) = '';
DECLARE @partner_request_sex NVARCHAR(1) = '';
DECLARE @partner_request_user_id INT = 0;
*/

INSERT INTO  HP3_Dompe_Partner_MediInfo_Request
SELECT 
	@partner_request_date,
	@partner_request_fullname,
	@partner_request_phone,
	@partner_request_product,
	@partner_request_therapeutic,
	@partner_request_request,
	@partner_request_adverse,
	@partner_request_age,
	@partner_request_sex,
	@partner_request_user_id

SELECT scope_identity()