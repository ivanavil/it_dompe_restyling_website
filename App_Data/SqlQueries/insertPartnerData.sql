INSERT INTO  HP3_Dompe_Partner
SELECT 
	@partner_country,
	@partner_directPartner,
	@partner_indirectPartner,
	@partner_areaterapeutica,
	@partner_brand,
	@partner_principioAttivo,
	@partner_userid,
	@partner_rowid

SELECT scope_identity()