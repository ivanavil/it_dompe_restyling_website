<%@ WebService Language="VB" Class="NLTrace" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Healthware.HP3.Core.User
Imports Healthware.HP3.Core.User.ObjectValues
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues

 
<WebService(Namespace := "http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _  
Public Class NLTrace
    Inherits System.Web.Services.WebService 
    
    <WebMethod()> _
    Public Function NewsletterTrace(ByVal userIdent As UserIdentificator, ByVal newsletterK As Integer) As Boolean
        Dim ris As Boolean = False
        Dim nwlsManager As New NewsletterManager
        Dim traceValue As New NewsletterTraceValue
        traceValue.NewsletterId = New NewsletterIdentificator(newsletterK)
        traceValue.EventId = NewsletterTraceValue.TraceOperation.Send
        traceValue.UserId = userIdent

        Dim nslTraceValue As NewsletterTraceValue = nwlsManager.Create(traceValue)
  
        If Not nslTraceValue Is Nothing Then
            ris = True
        End If
        
        Return ris
    End Function

End Class
