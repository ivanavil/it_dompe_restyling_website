

Shadowbox.init({
    overlayColor: "#C7B18B",
    overlayOpacity: "0.7",
    language: "it",
    counterType: 'skip',
    handleOversize: 'drag',
    slideshowDelay: 3 //inserisce il paly    
});
function closeSHB() 
{Shadowbox.close();}

function testSHB() {
    $("#video_content").remove();
    $("#video_content").empty();
    Shadowbox.close();
}

function popup(file, x, y, r, s) {
    var par = "scrollbars=" + s + ",resizable=" + r + ",width=" + x + ",height=" + y + ",status=no,location=no,toolbar=no,directories=no,menubar=no";
    window.open(file, '', par);
}

function ExternalLinkPopupit(url) {

    $(function () {
        $("#dialog-message").dialog({
            modal: false,
            draggable: false,
            height: 300,
            width: 500,
            buttons: {
                Cancella: function () {
                   $(this).dialog("close");
                },
                Continua: function () {
                    $(this).dialog("close");
                    window.open(url, '', '', '');
                }
            }
        });
    });
}

function ExternalLinkPopupen(url) {

    $(function () {
        $("#dialog-message").dialog({
            modal: false,
            draggable: false,
            height: 300,
            width: 500,
            buttons: {
                Cancel: function () {
                    $(this).dialog("close");
                },
                Continue: function () {
                    $(this).dialog("close");
                    window.open(url, '', '', '');
                }
            }
        });
    });
}

/*Exsternal Links*/
function bindExternalLinks(curDomain, linkEsclude) {

    var domArray = linkEsclude.split(";");
    var domainSelector = '';

    var i;
    for (i = 0; i < (domArray.length) ; i++) {
        domainSelector += ":not([href^='" + domArray[i] + "'])";
    };

    //add Current Domain
    domainSelector += ":not([href^='" + curDomain + "'])";


    $("a" + domainSelector + "[href^='http://']").each(function (index) {
        var op = $(this).attr("data-op");
        if (op != null) {
            $(this).attr("href", "javascript:ExternalLinkPopupen('" + $(this).attr("href") + "'," + op + ");");
        } else {
            $(this).attr("href", "javascript:ExternalLinkPopupen('" + $(this).attr("href") + "');");
        }
        $(this).removeAttr("target");
    });

    $("a" + domainSelector + "[href^='https://']").each(function (index) {

        var op = $(this).attr("data-op");
        if (op != null) {
            $(this).attr("href", "javascript:ExternalLinkPopupen('" + $(this).attr("href") + "'," + op + ");");
        } else {
            $(this).attr("href", "javascript:ExternalLinkPopupen('" + $(this).attr("href") + "');");
        }
        $(this).removeAttr("target");
    });


};


function selectType() {
    if (selectStatus == 0) {
        $('.typeList').css('display', 'block');
        selectStatus = 1;
    } else {
        $('.typeList').css('display', 'none');
        selectStatus = 0;
    }
}

function valType(id) {
    CloseRecoveryPwd();
    $('.typeList').css('display', 'none');
    $('#ErrorUser').css('display', 'none');
    selectStatus = 0;
    var valName = $("#type" + id).attr("title");
    $('#typeLogin').val(id);
    $('.selecType').html(valName);
    if (id == "2" || id == "3") {
        $("#boxLogDocCheck").attr("style", "display:block");
        $("#boxLogStandard").attr("style", "display:none");
    } else {
        $("#boxLogDocCheck").attr("style", "display:none");
        $("#boxLogStandard").attr("style", "display:block");
    }
}

jQuery(function () {
    var labelOptions = {
        labelClass: 'label-default',
        attribute: 'title'
    };
    $.each($('#user'), function () {
        $(this).jHp3LabelInside(labelOptions);
    });
    $.each($('#pssw'), function () {
        $(this).jHp3LabelInside(labelOptions);
    });
    //		document.getElementById("user").value='';
    //		document.getElementById("pssw").value='';
});

function recordOutboundLink() {
    _gaq.push(['_trackPageview', location.pathname + location.search + location.hash]);
}

function recordOutboundLink(link, category, action, opt_label, opt_value) {
    _gaq.push(['_trackEvent', category, action, opt_label]);
    window.setTimeout('document.location.href = "' + link + '"', 500) 
}
function recordOutboundLink2(category, action) {
    _gaq.push(['_trackEvent', category, action]);
}
function recordOutboundFull(category, action, opt_label) {
    _gaq.push(['_trackEvent', category, action, opt_label]);
}

function recordOutboundExternalFull(category, action, opt_label, opt_value, optCodelang) {
    _gaq.push(['_trackEvent', category, action, opt_label]);
    window.setTimeout('javascript:ExternalLinkPopup' + optCodelang + '("' + opt_value + '")', 500);
}

