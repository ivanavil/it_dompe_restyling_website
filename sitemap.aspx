<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _currentCT As ContentTypeIdentificator = Nothing
    Protected _urlHome As String = ""
    Protected _totItem As Integer = 0
    Private totMenu As Integer = 0
    Private _Father As Integer = 0
   
    Private _langCode As String = String.Empty
    Private _langId As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _currentCT = Me.PageObjectGetContentType
        _langCode = "IT"'Me.PageObjectGetLang.Code.ToUpper
        _urlHome = "http://" & Request.Url.Host
        _langId = Me.PageObjectGetLang.Id
    End Sub
    
    Sub page_load()
        _currentTheme = Me.PageObjectGetTheme
        LoadMainMenu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
    End Sub
    
    Sub LoadMainMenu(ByVal domain As String, ByVal name As String)
		Dim _cacheKey As String = "cacheSiteMapGenerator:46|1"
		
		'--------------------------------------------------------------------->
		If request("cache") = "false" then CacheManager.RemoveByKey(_cacheKey)
		'--------------------------------------------------------------------->

        Dim _strMenu As String = CacheManager.Read(_cacheKey, 1)
		If String.IsnullOrEmpty(_strMenu) Then 
			Dim so As New ThemeSearcher
			so.KeySite = Me.PageObjectGetSite
			so.KeyLanguage = Me.PageObjectGetLang
			so.KeyFather.Domain = domain
			so.KeyFather.Name = name
			so.LoadHasSon = False
			so.LoadRoles = False
			Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
			If Not coll Is Nothing AndAlso coll.Any Then
				Dim _intCounter As Integer = 1
				
				Dim _sbSiteMap as StringBuilder = New StringBuilder()
				_sbSiteMap.Append("<?xml version='1.0' encoding='UTF-8'?><urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>")
				
				For Each thm as ThemeValue in coll
					_sbSiteMap.Append("<url><loc>" & getLinkFirstTheme(thm) & "</loc><lastmod>2012-01-23</lastmod><changefreq>weekly</changefreq><priority>1</priority></url>")
					Dim _internalThemColl as ThemeCollection = LoadRptSubMenu(thm)
					For Each internalThemeValue as ThemeValue in _internalThemColl
						_sbSiteMap.Append("<url><loc>" & getLinkSL(internalThemeValue) & "</loc><lastmod>2012-01-23</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>")						
						If internalThemeValue.KeyContent.Id = 0 Then
							_sbSiteMap.Append(getContentListXml(internalThemeValue))						
						End If
					Next
					_intCounter += 1
				Next
				_sbSiteMap.Append("</urlset>")
				_strMenu = _sbSiteMap.ToString()
				CacheManager.Insert(_cacheKey, _strMenu, 1, 60)
			End If			
		End If
		
		Response.Clear()
		Response.ContentType = "text/xml"
		Response.ContentEncoding = Encoding.UTF8
		Response.Write(_strMenu)
		Response.End()
    End Sub
	
	Function getContentListXml(ByVal obj as themevalue) as String
		Dim _return As StringBuilder = New StringBuilder
		Dim OSearch as new contentSearcher
		with OSearch
			.KeySiteArea.Id = obj.KeySiteArea.Id
			.KeyContentType.Id = obj.KeyContentType.Id
		End With

		Dim oColl as ContentCollection = Me.BusinessContentManager.Read(OSearch)
		If Not oColl Is Nothing AndAlso oColl.COunt > 0 Then
			For each oo as contentValue in oColl
			_return.Append("<url><loc>" & Me.BusinessMasterPageManager.GetLink(oo.Key, obj, False)& "</loc><lastmod>" & oo.DateUpdate.ToString() & "</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>"			)
			Next
		End If
	
		Return _return.ToString
	End Function    
	
    Function LoadRptSubMenu(ByVal thmval As ThemeValue) As ThemeCollection
        Dim themes As ThemeCollection = Nothing
        Dim Roles As Boolean = False
        If Not thmval Is Nothing Then
            themes = ReadMyTHChildren(thmval.Key)
        End If
        Return themes
    End Function
    
    Function ReadMyTHChildren(ByVal key As ThemeIdentificator) As ThemeCollection
        Dim coll As ThemeCollection = Nothing
        If Not key Is Nothing AndAlso key.Id > 0 Then
            Dim soTheme As New ThemeSearcher
            soTheme.KeyFather.Id = key.Id
            soTheme.KeySite = Me.PageObjectGetSite
            soTheme.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
            soTheme.LoadHasSon = False
            soTheme.LoadRoles = False
            coll = Me.BusinessThemeManager.Read(soTheme)
        End If

        Return coll
    End Function

    
    Function getLink(ByVal langValue As LanguageValue) As String
        Return Me.BusinessMasterPageManager.GetLink(langValue.Key, False)
    End Function
    
    Function GetLink() As String
        Return Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
    End Function
    
    'GetLink by Theme Domain/Name
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
        
    Function GetLink(ByVal vo As MenuItemValue) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.PageObjectGetLang
        so.KeySite = Me.PageObjectGetSite
        so.Key = vo.KeyTheme
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return Nothing
    End Function
    
    Function GetLink(ByVal vo As ThemeValue) As String
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
    End Function
    
    Function getLinkFirstTheme(ByVal vo As ThemeValue) As String
        Dim strLink As String = "javascript:void(0);"
        Dim themSearch As New ThemeSearcher
        Dim themColl As New ThemeCollection
        themSearch.KeyFather.Id = vo.Key.Id
        themSearch.KeyLanguage.Id = vo.KeyLanguage.Id
        themSearch.Properties.Add("Key.Id")
        themSearch.Properties.Add("KeyContent.Id")
        themColl = Me.BusinessThemeManager.Read(themSearch)
        If Not themColl Is Nothing AndAlso themColl.Count > 0 Then
            strLink = Me.BusinessMasterPageManager.GetLink(New ContentIdentificator(themColl(0).KeyContent.Id, _langId), themColl(0), False)
        End If
        Return strLink
    End Function
    
    
    Function GetLinkSL(ByVal vo As ThemeValue) As String
        Dim linkFinale As String = Me.BusinessMasterPageManager.GetLink(vo, False)
        
        'Controllo se il thema equivale alla sezione giornalisti
        If vo.Key.Id = Dompe.ThemeConstants.IDProgettiIniziative Then
            'linkFinale = Healthware.Dompe.Helper.Helper.getLinkProgettiIniziative(Me.BusinessMasterPageManager, _langId)
        End If

        If vo.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Then
            Dim Abilitato As Boolean = False
            If Me.BusinessAccessService.IsAuthenticated Then
                If Me.ObjectTicket.User.Key.Id > 0 Then
                    Dim id_ruolo_utente As Array
                    Dim id_r As Integer
                    id_ruolo_utente = Split("," & Me.ObjectTicket.Roles.ToString & ",", ",")
                    If id_ruolo_utente.Length() - 1 > 0 Then
                        
                        For id_r = 0 To id_ruolo_utente.Length() - 1
                            
                            'Controllo se l'utente loggato � un giornalista
                            If id_ruolo_utente(id_r).ToString = Dompe.Role.RoleMediaPress Then
                                'Link Alla sezione Dashboard
                                Abilitato = True
                                Exit For
                            End If
                          
                        Next
                    End If
                Else
                    linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista)
                End If
            Else
                linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista)
            End If
            If Abilitato = True Then
                'linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard)
            End If
        End If
        
        Return linkFinale
    End Function
</script>