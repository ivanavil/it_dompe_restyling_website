<%@ Page Language="VB" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site"%>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues"%>
<%@ Import Namespace="Healthware.HP3.Core.Base"%>
<%@ Import Namespace="Healthware.HP3.Core.User"%>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues"%>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>

<script language="vb" runat="server"> 
    Protected _siteFolder As String = String.Empty
    Protected _currentTheme As ThemeIdentificator = Nothing
    Protected _currentCT As ContentTypeIdentificator = Nothing
    Protected _urlHome As String = ""
    Protected _totItem As Integer = 0
    Private totMenu As Integer = 0
    Private _Father As Integer = 0
   
    Private _langCode As String = String.Empty
    Private _langId As Integer = 0
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        _siteFolder = Me.ObjectSiteFolder()
        _currentCT = Me.PageObjectGetContentType
        _langCode = Me.PageObjectGetLang.Code.ToUpper
        _urlHome = "http://" & Request.Url.Host
        _langId = Me.PageObjectGetLang.Id
    End Sub
    
    Sub page_load()
        _currentTheme = Me.PageObjectGetTheme
        'Dim userAgent As String = HttpContext.Current.Request.UserAgent.ToLower()
        'If (userAgent.Contains("iphone") Or userAgent.Contains("ipad")) Then
        'End If
        'hdntheme.Value = _currentTheme.Id

        LoadMainMenu(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
		
        'If (_currentTheme.Name <> Dompe.ThemeConstants.Name.Home) Then
            'Dim ctrlPath1 As String = String.Format("/{0}/HP3Common/BreadCrumbs.ascx", Me._siteFolder)
            'LoadSubControl(ctrlPath1, Me.plhBreadCrumbs)
        'End If
        'openByTheme(_currentTheme.Id)
    End Sub
    
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Sub openByTheme(ByVal idtheme As Integer)
        Dim themeVal As New ThemeValue
        themeVal = Me.BusinessThemeManager.Read(New ThemeIdentificator(idtheme))
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){ subMenuOpen(" & themeVal.KeyFather.Id & ");})", True)
    End Sub
    
    Sub LoadMainMenu(ByVal domain As String, ByVal name As String)
		Dim _cacheKey As String = "cacheSiteMapGenerator:46|1"
		
		'----------------------------------------->
		CacheManager.RemoveByKey(_cacheKey)
		'----------------------------------------->
		
		
        Dim _strMenu As String = CacheManager.Read(_cacheKey, 1)
		If Not String.IsnullOrEmpty(_strMenu) Then 
			'ltlMenu.Text = _strMenu
		Else
			Dim so As New ThemeSearcher
			so.KeySite = Me.PageObjectGetSite
			so.KeyLanguage = Me.PageObjectGetLang
			so.KeyFather.Domain = domain
			so.KeyFather.Name = name
			so.LoadHasSon = False
			so.LoadRoles = False
			Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
			If Not coll Is Nothing AndAlso coll.Any Then
				'For Each thmVal As ThemeValue In coll
				'    LoadSubMenu(thmVal)
				'Next
				
				Dim _intCounter As Integer = 1
				
				Dim ssssbb as StringBuilder = New StringBuilder()
				Dim ssssbb2 as StringBuilder = New StringBuilder()
				
				ssssbb2.Append("<?xml version='1.0' encoding='UTF-8'?><urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>")
				
				
				ssssbb.Append("<h1><a title='Domp� Corporate' href='" & _urlHome & "'>Domp� Corporate</a></h1>")
				ssssbb.Append("<ul class='menu sf-menu'>")
				
				For Each thm as ThemeValue in coll
					ssssbb.Append("<li id='d" & _intCounter & "' " & getClass(thm.Key.id,_intCounter) & ">")
					ssssbb.Append("<a title='" & thm.Description & "' href='" & getLinkFirstTheme(thm) & "'>" & thm.Description & "</a>")
					ssssbb.Append("<ul>")
					
					
					ssssbb2.Append("<url><loc>" & getLinkFirstTheme(thm) & "</loc><lastmod>2012-01-23</lastmod><changefreq>weekly</changefreq><priority>1</priority></url>")
					
					
					Dim _internalThemColl as ThemeCollection = LoadRptSubMenu(thm)
					For Each internalThemeValue as ThemeValue in _internalThemColl
						ssssbb.Append("<li id='s" & internalThemeValue.Key.Id & "'>")
						ssssbb.Append("<a title='" & internalThemeValue.Description & "' href='" & getLinkSL(internalThemeValue) & "'>" & internalThemeValue.Description & "</a>")

						ssssbb2.Append("<url><loc>" & getLinkSL(internalThemeValue) & "</loc><lastmod>2012-01-23</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>")						
						
						
						If internalThemeValue.KeyContent.Id = 0 Then
							ssssbb.Append(getContentList(internalThemeValue))
							
							ssssbb2.Append(getContentListXml(internalThemeValue))						
						End If
						ssssbb.Append("</li>")
					Next

					ssssbb.Append("</ul>")
					ssssbb.Append("</li>")
					_intCounter += 1
				Next
				ssssbb.Append("</ul>")
				
				ssssbb2.Append("</urlset>")
				
				CacheManager.Insert(_cacheKey, ssssbb.ToString(), 1, 60)
				
				'ltlMenu.Text = ssssbb.ToString()
				
				
				Response.Clear()
				Response.ContentType = "text/xml"
				Response.ContentEncoding = Encoding.UTF8
				Response.Write(ssssbb2.ToString())
				Response.End()
				
				
				
				
				
				'rptMenu.DataSource = coll
				'rptMenu.DataBind()
			End If			
		End If
    End Sub
	
	Function getContentList(ByVal obj as themevalue) as String
		Dim _return As String = String.Empty
		
		_return = "<ul><li>CONTENUTO</li></ul>"
	
		Return _return
	End Function
	
	Function getContentListXml(ByVal obj as themevalue) as String
		Dim _return As StringBuilder = New StringBuilder
		Dim OSearch as new contentSearcher
		with OSearch
			.KeySiteArea.Id = obj.KeySiteArea.Id
			.KeyContentType.Id = obj.KeyContentType.Id
		End With
		
		Dim oColl as ContentCollection = Me.BusinessContentManager.Read(OSearch)
		If Not oColl Is Nothing AndAlso oColl.COunt > 0 Then
			For each oo as contentValue in oColl
			_return.Append("<url><loc>" & Me.BusinessMasterPageManager.GetLink(oo.Key, obj, False)& "</loc><lastmod>" & oo.DateUpdate.ToString() & "</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>"			)
			Next
		End If
	
		Return _return.ToString
	End Function    
	
    Function LoadRptSubMenu(ByVal thmval As ThemeValue) As ThemeCollection
        Dim themes As ThemeCollection = Nothing
        Dim Roles As Boolean = False
                  
        If Not thmval Is Nothing Then
                                  
            themes = ReadMyTHChildren(thmval.Key)
      
        End If
        Return themes
    End Function
    
    Function ReadMyTHChildren(ByVal key As ThemeIdentificator) As ThemeCollection
        Dim coll As ThemeCollection = Nothing
        
        If Not key Is Nothing AndAlso key.Id > 0 Then
            Dim soTheme As New ThemeSearcher
            
            soTheme.KeyFather.Id = key.Id
            soTheme.KeySite = Me.PageObjectGetSite
            soTheme.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
            soTheme.LoadHasSon = False
            soTheme.LoadRoles = False
           
            coll = Me.BusinessThemeManager.Read(soTheme)
                    
        End If
          
        Return coll
    End Function
    
    
    'It loads the languages related to the site
    Function LoadLanguages() As String
        Dim CacheLngkey As String = String.Empty
          
        Dim CurrentLang As String = Me.PageObjectGetLang.Code
        
        Dim langSearcher As New LanguageSearcher
        Dim langColl As New LanguageCollection

        langColl = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(New SiteAreaIdentificator(Me.PageObjectGetSite.Id))
        langSearcher = Nothing
        
        ' Dim str As String = String.Empty
        Dim i As Integer = 0
        
        Dim allLangs As String = String.Empty
        allLangs = "<ul>"
        
        For Each elem As LanguageValue In langColl
                        
            allLangs = allLangs & "<li>"
            
            If Not CurrentLang.ToLower.Equals(elem.Key.Code.ToLower) Then allLangs = allLangs & "<a href=" & getLink(elem) & " title=" & elem.Description & ">" & elem.Description & "</a>"
            If CurrentLang.ToLower.Equals(elem.Key.Code.ToLower) Then allLangs = allLangs & "<span>" & elem.Description & "</span>"
            
            If (i <> langColl.Count - 1) Then allLangs = allLangs & "<span> | </span>"
                                 
            allLangs = allLangs & "</li>"
           
                
            i = i + 1
        Next
        
        allLangs = allLangs & "</ul>"
        
        Return allLangs
    End Function
    
    Function getLink(ByVal langValue As LanguageValue) As String
        Return Me.BusinessMasterPageManager.GetLink(langValue.Key, False)
    End Function
    
    Function GetLink() As String
        Return Me.GetLink(Dompe.ThemeConstants.Domain.DMP, Dompe.ThemeConstants.Name.Home)
    End Function
    
    'GetLink by Theme Domain/Name
    Function GetLink(ByVal domain As String, ByVal name As String) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.BusinessMasterPageManager.GetLanguage
        so.Key.Domain = domain
        so.Key.Name = name
        so.KeySite = Me.PageObjectGetSite
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Any Then
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
        Return String.Empty 'potrebbe restiture URL SiteDominio
    End Function
        
    Function GetLink(ByVal vo As MenuItemValue) As String
        Dim so As New ThemeSearcher
       
        so.KeyLanguage = Me.PageObjectGetLang
        so.KeySite = Me.PageObjectGetSite
        so.Key = vo.KeyTheme
        so.LoadHasSon = False
        so.LoadRoles = False
           
        Dim coll As ThemeCollection = Me.BusinessThemeManager.Read(so)
       
        If Not coll Is Nothing AndAlso coll.Count > 0 Then
            
            Return Me.BusinessMasterPageManager.GetLink(coll(0), False)
        End If
        
       
        Return Nothing
    End Function
    
    Function GetLink(ByVal vo As ThemeValue) As String
   
        Return Me.BusinessMasterPageManager.GetLink(vo, False)
         
    End Function
    
    Function getLinkFirstTheme(ByVal vo As ThemeValue) As String
        Dim strLink As String = "javascript:void(0);"
        Dim themSearch As New ThemeSearcher
        Dim themColl As New ThemeCollection
        
        themSearch.KeyFather.Id = vo.Key.Id
        themSearch.KeyLanguage.Id = vo.KeyLanguage.Id
        themSearch.Properties.Add("Key.Id")
        themSearch.Properties.Add("KeyContent.Id")
              
        themColl = Me.BusinessThemeManager.Read(themSearch)
        
        If Not themColl Is Nothing AndAlso themColl.Count > 0 Then
            strLink = Me.BusinessMasterPageManager.GetLink(New ContentIdentificator(themColl(0).KeyContent.Id, _langId), themColl(0), False)
        End If
        Return strLink
  
    End Function
    
    
    Function GetLinkSL(ByVal vo As ThemeValue) As String
        Dim linkFinale As String = Me.BusinessMasterPageManager.GetLink(vo, False)
        
        'Controllo se il thema equivale alla sezione giornalisti
        If vo.Key.Id = Dompe.ThemeConstants.IDProgettiIniziative Then
           
            
            linkFinale = Healthware.Dompe.Helper.Helper.getLinkProgettiIniziative(Me.BusinessMasterPageManager, _langId)

        End If
        
        
        
        If vo.Key.Id = Dompe.ThemeConstants.idThemaSezioneGiornalista Then
            Dim Abilitato As Boolean = False
            If Me.BusinessAccessService.IsAuthenticated Then
                If Me.ObjectTicket.User.Key.Id > 0 Then
                  
                    Dim id_ruolo_utente As Array
                    Dim id_r As Integer
                  
                    id_ruolo_utente = Split("," & Me.ObjectTicket.Roles.ToString & ",", ",")
                    If id_ruolo_utente.Length() - 1 > 0 Then
                        
                        For id_r = 0 To id_ruolo_utente.Length() - 1
                            
                            'Controllo se l'utente loggato � un giornalista
                            If id_ruolo_utente(id_r).ToString = Dompe.Role.RoleMediaPress Then
                                'Link Alla sezione Dashboard
                                Abilitato = True
                                Exit For
                            End If
                          
                        Next
                    End If
                Else
                    linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista)
                End If
            Else
                linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneGiornalista)
            End If
            
            If Abilitato = True Then
                linkFinale = Healthware.Dompe.Helper.Helper.getLinkByIdTheme(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard)
            End If
      
        End If
        
        Return linkFinale
    End Function
    
    Function GetLabel(ByVal label As String) As String
        Return Me.BusinessDictionaryManager.Read(label, Me.PageObjectGetLang.Id)
    End Function
    
    Function GetClass(ByVal idTheme As String, ByVal item As Integer) As String
        Dim res As String = ""
        res = "class=""menu" & item & """"
        Dim manHElp As New Healthware.Dompe.Helper.Helper
     
        Select Case Me.PageObjectGetTheme.Id
            Case idTheme
                res = "class=""selected " & res & """"
            Case Dompe.ThemeConstants.idAreaMedico
                If idTheme = Dompe.ThemeConstants.idAreaMedico Then
                    res = "class=""selected " & res & """"
                End If
        End Select
        Return res
    End Function
</script>