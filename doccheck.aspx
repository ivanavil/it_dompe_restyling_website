<%@ Page  EnableEventValidation="false"  Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>
<%@ Import Namespace="Healthware.Hp3.Core.User" %>
<%@ Import Namespace="Healthware.Hp3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site" %>
<%@ Import Namespace="Healthware.Hp3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web" %>
<%@ Import Namespace="Healthware.Hp3.Core.Web.ObjectValues" %>
<%@ Import Namespace="Healthware.Hp3.Core.Utility" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization"%>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues"%>
<%@ Import Namespace="Healthware.Hp3.Core.Utility.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues"%>
<%@ Import Namespace="Healthware.Dmp.Helpers" %>

<script runat="server" type="text/VB">
    Protected _siteFolder As String = String.Empty
    Protected _tabletCss As String = String.Empty
    Dim _intLanguageID As Integer = 1
    Protected _SiteUrlAnalitics As String = ""
    Dim _curDomain As String = String.Empty
    Dim _curDomainId As Integer = 0
	DIm _userIdEncripted As String = String.Empty
    Dim _landCode As String = ""
    Dim _langKey As LanguageIdentificator = Nothing
    Dim _strDocCheckHcpId As String = "1"
    Dim _strDocCheckPressId As String = "23"
	Protected _userTypeId As Integer = 0
	Protected _loggedInUserId as Integer = 0
	Protected _reservedAreaName As String = ""
	
    Dim _isPhysicianUser As Boolean = False
    Dim _isPressUser As Boolean = False
	
	Protected Enum _userType
		Press = 1
		HCP = 2
        Partner = 3
        Sede = 4
    End Enum	
	
    Sub Page_Init()
        'response.write(Request.Url.ToString())
        _siteFolder = Me.ObjectSiteFolder()
        _curDomainId = getSiteDomainId()
        _landCode = Me.PageObjectGetLang.Code
        _langKey = Me.PageObjectGetLang
    End Sub
   
    Public Function IsMobileDevice() As Boolean
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
        Return False
    End Function
    
    Sub Page_Load()
		If Me._siteFolder.ToLower() <> "prjdompemobile" Then
			Dim ctrlPath As String = String.Empty
			ctrlPath = String.Format("/{0}/HP3Common/header.ascx", Me._siteFolder)
			LoadSubControl(ctrlPath, Me.phHeader)
			
			ctrlPath = String.Format("/{0}/HP3Common/footer.ascx", Me._siteFolder)
			LoadSubControl(ctrlPath, Me.phFooter)
			
			If IsMobileDevice() Then
				_tabletCss = String.Format("<link href='/{0}/_css/tablet.css' rel='stylesheet' type='text/css' />", _siteFolder)
			End If
		End If		
        
        _SiteUrlAnalitics = Request.Url.Host
        'dc_beruf = ID di Doc Check delle professioni
        'dc_beruf = 1: Physician (Medico)
        'dc_beruf = 23: Medical Journalist (Press)
		
        'If Not String.IsNullOrEmpty(Request("dc_beruf")) AndAlso Not String.IsNullOrEmpty(Request("profession_id")) Then
        'If Request("dc_beruf").Trim() = "1" AndAlso Request("profession_id") = "1" Then _isPhysicianUser = True
        'If Request("dc_beruf").Trim() = "37" AndAlso Request("profession_id") = "23" Then _isPressUser = True
        'End If
        
        If Not String.IsNullOrEmpty(Request("dc_beruf")) Then
            If Request("dc_beruf").Trim() = "1" Then _isPhysicianUser = True
            If Request("dc_beruf").Trim() = "23" Then _isPressUser = True
            If Request("dc_beruf").Trim() = "37" Then _isPressUser = True
        End If
        
      		
        'gestire decodifica caratteri speciali-------------
        Dim strToSend As String = String.Empty
        For Each x As String In Request.QueryString.AllKeys
            strToSend += "<br />key:<b>" & x & "</b> value: <b>" & Request.QueryString(x) & "</b>-----" & Request.QueryString(x) & "------" & System.Text.Encoding.GetEncoding("utf-8").GetString(System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(Request.QueryString(x)))
        Next
        'If Not String.IsNullOrEmpty(strToSend) Then notifyImportParams(strToSend)
        '-------------------------------------------------
		
        Dim _docCheckUser As UserHCPValue = Nothing
	
        'Read user by DocCheckId--------------------------
        If Not String.IsNullOrEmpty(Request("Uniquekey")) Then
            _docCheckUser = ExistDocCheckId(Request("Uniquekey"))
        End If
		
        If Not _docCheckUser Is Nothing AndAlso _docCheckUser.Key.Id > 0 Then
            Select Case _docCheckUser.HCPMoreInfo.ToString.ToLower()
                Case "hcp"
                    _isPhysicianUser = True
                    
                Case "media"
                    _isPressUser = True
            End Select
        End If
        '-------------------------------------------------
		
        'Read user by email address------------
        '------------------- UTILE SE CI SONO UTENTI PRECARICATI
        'If _docCheckUser Is Nothing Then
        '    If Not String.IsNullOrEmpty(Request("dc_email")) Then
        '        _docCheckUser = ExistEmail(Request("dc_email"))
        '    End If
        'End If
        '------------------- END UTILE SE CI SONO UTENTI PRECARICATI
       
		'Riga commentata perch� ci hanno chiesto di disattivare l'aria media
		'FaF il 04/01/2017
        'If _isPressUser Or _isPhysicianUser Then
		'---------------------------------------------------------------------
		
		If _isPhysicianUser Then
            If _isPressUser Then
                _userTypeId = _userType.Press
                _reservedAreaName = "PRESS"
			
            ElseIf _isPhysicianUser Then
                _userTypeId = _userType.HCP
                _reservedAreaName = "HCP"
            End If
            processUser(_docCheckUser)
        End If
        '---------------------------------------->
        
        'Redirect alla home page o altra pagina per dire all'utente che non ha la professione adatta per accedere alle aree riservervate
		
		
		'Riga commentata perch� ci hanno chiesto di disattivare l'aria media
		'FaF il 04/01/2017
        'If Not _isPhysicianUser And Not _isPressUser Then
		'---------------------------------------------------------------------
		
        If Not _isPhysicianUser Then
            Dim _redUrl As String = String.Concat("http://", _curDomain)
            If Me._siteFolder.ToLower() = "prjdompemobile" Then _redUrl = "http://m.dompe.com"
            RedirectionToHomePageWithError(_redUrl)
        End If
    End Sub
	
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    Sub RedirectionToHomePageWithError(ByVal _strDomain as String)
        Response.Redirect(_strDomain, False)
        Response.End()
    End Sub
   
    Function GetLinkRedirect(ByVal currentTheme As ThemeIdentificator) As String
        Return Me.BusinessMasterPageManager.GetLink(currentTheme, Nothing, False)
    End Function
    
    Sub processUser(ByVal _docCheckUser As UserHCPValue)
        Dim status As Integer = 0
        Dim statusUser As String = ""
        Try
            'Response.Write("(_docCheckUser is Nothing) : " & (_docCheckUser Is Nothing) & "<br>")
			'Response.Write("_docCheckUser.Key.Id : " & _docCheckUser.Key.Id & "<br>")
			
            Dim usMail As UserHCPValue = getPopulatedUser(_docCheckUser)
            '-------------------------------------------------
			
            'Se l'utente esiste eseguo il login---------------
            If Not (usMail Is Nothing) AndAlso usMail.Key.Id > 0 Then
				_loggedInUserId = usMail.Key.Id
				
				'UPDATE
                Me.BusinessUserHCPManager.Update(usMail)
				
            Else 'Se non esiste deve essere creato-----------------
				'CREATE
                usMail = Me.BusinessUserHCPManager.Create(usMail)
				_loggedInUserId = usMail.Key.Id
				
                'associazione ruoli e profili x i Medici-------
                If _isPhysicianUser Then
                    Me.BusinessProfilingManager.CreateUserProfile(usMail.Key, New ProfileIdentificator(Dompe.Profile.ID.ProfileDoctor))
                    Me.BusinessProfilingManager.CreateUserRole(usMail.Key, New RoleIdentificator(Dompe.Role.RoleDoctor))
                End If

                'associazione ruoli e profili x i Giornalisti-------				
                If _isPressUser Then
                    Me.BusinessProfilingManager.CreateUserProfile(usMail.Key, New ProfileIdentificator(Dompe.Profile.ID.ProfileMediaPress))
                    Me.BusinessProfilingManager.CreateUserRole(usMail.Key, New RoleIdentificator(Dompe.Role.RoleMediaPress))
                End If
            End If

            Dim userTicket As TicketValue = Me.BusinessAccessService.Login(usMail.Key)
            If userTicket.ErrorOccurred.Number >= 0 Then
                TraceEvent(Dompe.TraceEvent.SilentLoginDocCheck, userTicket.User.Key.Id) 'traccio login da doccheck
                insExtrafield(userTicket.User.Key.Id, Request.Url.PathAndQuery, "LoginSilente-Ok--" & DateTime.Now.ToString)
                RedirectionToHomePage(_userIdEncripted)
            Else
                TraceEvent(Dompe.TraceEvent.FailedSilentLoginDocCheck)
                insExtrafield(userTicket.User.Key.Id, Request.Url.PathAndQuery, "LoginSilente-Error--" & DateTime.Now.ToString)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundFull('DocChek Login','Error','Login Silente');})", True)
                panelSj.Visible = True
                DivLoad.Visible = False
            End If
        Catch ex As Exception
			'response.write(ex.ToString)
        End Try
    End Sub
	
    Sub insExtrafield(ByVal IDuser As Integer, ByVal strQuery As String, ByVal label As String)
        Dim extf As New ExtraFieldsStoreValue
        extf.KeyUser.Id = IDuser
        extf.Label = label
        extf.Value = strQuery
        BusinessExtraFieldsManager.Create(extf)
    End Sub
    
    Function getSiteDomainId() As Integer
        'Get Site Id from URL domain-----------------------
        _curDomain = Request.Url.Host.ToString.ToLower
        Dim dm As New SiteCollection
        Dim ssea As New SiteSearcher
        With ssea
            .DomainName = Request.Url.Host.ToString.ToLower
        End With
        dm = Me.BusinessSiteManager.Read(ssea)
        If Not dm Is Nothing AndAlso dm.Count > 0 Then
            _curDomainID = dm(0).KeySiteArea.Id
        End If
        dm = Nothing
        ssea = Nothing
        '--------------------------------------------------
		
        Return _curDomainID
    End Function
	
    Function getPopulatedUser(ByVal userval As UserHCPValue) As UserHCPValue
        Dim userV As New UserHCPValue
        If Not userval Is Nothing AndAlso userval.Key.Id > 0 Then Return userval 'userV = userval

        Dim salutation As String = Request("dc_anrede")
        Dim profession As String = Request("dc_beruf")
        Dim specialty As String = Request("dc_fachgebiet")
        Dim uniqueKey As String = Request("Uniquekey")
		
        With userV
            .Gender = Request("dc_gender").ToUpper
            .Title = Request("dc_titel")
            .Name = Request("dc_vorname")
            .Surname = Request("dc_name")
            .Email.Clear()
            .Email.Add(Request("dc_email"))
            .HCPEmail.Clear()
            .HCPEmail.Add(Request("dc_email"))
            .HCPRegistration = uniqueKey.Trim
            .Telephone.Clear()
            If Not String.IsNullOrEmpty(Request("dc_phone")) Then 
				.Telephone.Clear()
				.Telephone.Add(Request("dc_phone"))
			End If
            .Status = 1 'utente immediatamente attivo 
            .Username = Request("dc_email") 'email
            .RegistrationSite.Id = 46 'SiteId
            .RegistrationDate = System.DateTime.Now
            .MaritalStatus = 0
            .Password = SimpleHash.GenerateUUID(7)
            .PasswordLastUpdate = System.DateTime.Now
            '----------------------------------------- 
            
            'da definire dove vanno salvati in base al valore di addr_type
            .Address.Clear()
            .Address.Add(Request("dc_strasse"))
            .Cap = Request("dc_plz")
            .City = Request("dc_ort")
            .Geo.Key.Id = 1 'Valore fisso, da non considerare. Fare affidamento al valore in Province.
            .Province = Request("dc_land")
			.HCPProfessionalCodes.Clear()
            .HCPProfessionalCodes.Add(profession)
            .HCPOtherInfo.Clear()
            .HCPOtherInfo.Add(specialty)
            
            .HCPMoreInfo.Clear()
            If _isPhysicianUser Then .HCPMoreInfo.Add("HCP")
            If _isPressUser Then .HCPMoreInfo.Add("Media")
        End With
        Return userV
    End Function
	
    'Reindirizzo allo scopo di effettuare il login silente
    Sub RedirectionToHomePage(ByVal encryptedId As String)
        Dim strrole As String = "undefined"
		Dim _strUrl As String = String.Empty
		
		'Sito Desktop
		If Me._siteFolder.ToLower() <> "prjdompemobile" Then
			_strUrl = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idHomeTheme, _langKey.Id)
			
			If _isPhysicianUser Then
				If Request("strReferrer") = "articolo" Then
                    _strUrl = "/richiedi-l-articolo"
                    
                ElseIf Request("strReferrer") = "specialtycare" Then
                    _strUrl = "/area-medico-welcome-page"
                    
                ElseIf Request("strReferrer") = "rcp" Then
                    _strUrl = "/rcp/?intRCP=" & Request("intRCP")
                    'Response.Write(" _strUrl:" & _strUrl)
                    'Response.End()
                Else
                    _strUrl = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHome, _langKey.Id)
                End If
                strrole = "doctor"
            End If
			
			If _isPressUser Then
				_strUrl = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboard, _langKey.Id)
				strrole = "press"
            End If
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('" & _strUrl & "','Login DocCheck','OK','role:" & strrole & "');return false;})", True)
			
			'Sito Mobile
		ElseIf Me._siteFolder.ToLower() = "prjdompemobile" Then 
			_strUrl = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idHomeThemeMobile, _langKey.Id)
            If Not String.IsNullOrEmpty(Request("strReferrer")) Then
                If Request("strReferrer") = "rcp" Then
                    _strUrl = "/rcp/?intRCP=" & Request("intRCP")
                    Response.Redirect(_strUrl, False)
                    Response.End()
                Else
                    _strUrl = Request("strReferrer")
                End If
            Else
                If _isPhysicianUser Then _strUrl = Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idMedicalHomeMobile, _langKey.Id)
                If _isPressUser Then Healthware.Dompe.Helper.Helper.GetThemeLink(Me.BusinessMasterPageManager, Dompe.ThemeConstants.idThemaSezioneDashboardMobile, _langKey.Id)
            End If
			
                If _isPhysicianUser Then strrole = "doctor"
                If _isPressUser Then strrole = "press"
            
            
                Dim idThmAP As Integer = 0
                If strrole = "press" Then
                    idThmAP = Dompe.ThemeConstants.idThemaSezioneGiornalistaMobile
                ElseIf strrole = "doctor" Then
                    idThmAP = Dompe.ThemeConstants.idMedicalHomeMobile
                Else
                    idThmAP = Dompe.ThemeConstants.idHomeThemeMobile
                End If
            
                If idThmAP > 0 Then
                    Dim vo As New ThemeValue
                    vo = Me.BusinessThemeManager.Read(New ThemeIdentificator(idThmAP))
                    If Not vo Is Nothing AndAlso vo.Key.Id > 0 Then
                        vo.KeyLanguage = _langKey
                        _strUrl = Me.BusinessMasterPageManager.GetLink(vo, False)
                    End If
                End If
					
            
                Response.Redirect(_strUrl, False)
                Response.End()
            End If
    End Sub

	
    'Read User by Id Doc Check-----------------------------------------
    Function ExistDocCheckId(ByVal _strDocCheckId As String) As UserHCPValue
        Dim _docCheckUser As New UserHCPValue
		
        Dim userSearcher As New UserHCPSearcher
        Dim userColl As New UserHCPCollection
        Me.BusinessUserManager.Cache = False
        Me.BusinessUserHCPManager.Cache = False
        userSearcher.HCPRegistration = _strDocCheckId.Trim() '--------------------------> Verificare dove scrivere
        userColl = Me.BusinessUserHCPManager.Read(userSearcher)

        If (Not (userColl Is Nothing) AndAlso userColl.Count > 0) Then
            _docCheckUser = userColl(0)
            Return _docCheckUser
        Else
            Return Nothing
        End If
    End Function
		
	'Read User by email address-----------------------------------------
    Function ExistEmail(ByVal email_address As String) As UserHCPValue
        Dim usMailVal As UserHCPValue = Nothing
        Dim userSearcher As New UserHCPSearcher
        Dim userColl As New UserHCPCollection
        Me.BusinessUserManager.Cache = False
        userSearcher.Email = email_address.Trim()
        userColl = Me.BusinessUserHCPManager.Read(userSearcher)
        If (Not (userColl Is Nothing) AndAlso userColl.Count > 0) Then
            usMailVal = New UserHCPValue
            usMailVal = userColl(0)
        End If
        
        userSearcher = Nothing
        userColl = Nothing
        Return usMailVal
    End Function
	'--------------------------------------------------------------------

    'Invia una email ad ogni login di utenti via DocCheck-----------
    Private Sub notifyImportParams(ByVal strToSend As String)
		Try
			Dim message As MailValue = New MailValue
			message.MailFrom = New MailAddress("info@dompe.com")
			message.MailTo.Add(New MailAddress("bbicio@gmail.com"))
			message.MailSubject = "DocCheck Input Data"
			message.MailBody = strToSend
			message.MailIsBodyHtml = True
			message.MailPriority = MailValue.Priority.Normal
			'MailHelper.SendMailMessage(message, True)
		Catch ex as exception
		End Try	
    End Sub	
    '-----------------------------------------------------------------
	
	'Tracciamento su richiesta---------------------------------------
    Sub TraceEvent(ByVal idEvent As Integer, Optional ByVal idUser As Integer = 0)
        Dim traceVal As New TraceValue
        traceVal.KeySite.Id = 46
        traceVal.KeySiteArea.Id = Me.PageObjectGetSiteArea.Id
        traceVal.KeyContentType.Id = Me.PageObjectGetContentType.Id
        traceVal.KeyEvent.Id = idEvent
        traceVal.KeyUser.Id = idUser
        traceVal.KeyLanguage.Id = Me.PageObjectGetLang.Id
        traceVal.DateInsert = System.DateTime.Now
        traceVal.IdSession = Session.SessionID  'da fare vedere se � possibile mantenerlo
        Me.BusinessTraceService.Create(traceVal)
    End Sub	
    '-----------------------------------------------------------------
</script>

<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Login Doc Check</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

	<link rel="stylesheet" title="Standard" type="text/css" media="print" href="/<%=_siteFolder %>/_css/print.css.axd" /> 
	<link rel="stylesheet" title="Standard" type="text/css" media="screen" href="/Js/shadowbox/shadowbox.css" />  
	<link rel="shortcut icon" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<link rel="icon" type="image/gif" href="/<%=_siteFolder %>/_slice/favicon/favicon.ico" />
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/ui/jquery-ui.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/json2.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/jHp3LabelInside.js.axd"></script>
	<script type="text/javascript" src="/<%=_siteFolder %>/_js/superfish.js.axd"></script>
	<link rel="stylesheet" href="/<%=_siteFolder %>/_js/css/jquery-ui.css" />
	<link href="/<%=_siteFolder %>/_css/it/font.css.axd" rel="stylesheet" type="text/css" />
	
    <link href="/<%=_siteFolder %>/_css/style.css" rel="stylesheet" type="text/css" />
	<link href="/<%=_siteFolder %>/_css/it/style.css.axd" rel="stylesheet" type="text/css" />
    <link href="/<%=_siteFolder %>/_css/custom.css" rel="stylesheet" type="text/css" />
    <%=_tabletCss%>

	<asp:Literal ID="MySiteareaCss" runat="server"></asp:Literal> 
	<script language="JavaScript" type="text/JavaScript" src="/Js/shadowbox/shadowbox.js.axd"></script>
    <script language="JavaScript" type="text/JavaScript" src="/Js/global.js"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/jquery.easing.1.3.js", _siteFolder)%>"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/jquery.jkit.1.2.16.min.js", _siteFolder)%>"></script>
    <script language="JavaScript" type="text/JavaScript" src="<%=String.Format("/{0}/_js/superfish.min.js", _siteFolder)%>"></script>
	
	 <script type="text/javascript">
	     var _gaq = _gaq || [];
	     _gaq.push(['_setAccount', 'UA-48312377-3']);
	     _gaq.push(['_setDomainName', '<%= _SiteUrlAnalitics %>']);
		_gaq.push(['_setCustomVar', 1, 'UserType', '<%=_userTypeId%>', 3]);
	     _gaq.push(['_setCustomVar', 2, 'UserId', '<%=_loggedInUserId%>', 1]);
	     _gaq.push(['_setCustomVar', 3, 'ReservedArea', '<%=_reservedAreaName%>', 3]);
	     _gaq.push(['_trackPageview', location.pathname + location.search + location.hash]);
	</script>	
</head>
<body id="BodyMS">
    <form id="myForm" runat="server" enctype="multipart/form-data">
        <asp:PlaceHolder ID="plhLogout" runat="server" EnableViewState="false"></asp:PlaceHolder>                   
        <asp:PlaceHolder ID="phHeader"  runat="server" EnableViewState="false" />
        <div class="body">
            <div class="container">
                <div class="colRight">
                    <div class="generic">

                        <div style="" id="DivLoad" runat="server"><img alt="loading" src="ProjectDOMPE/_slice/loading.gif" /></div>
                        <asp:PlaceHolder ID="panelSj" runat="server" Visible="false">
                            <div id="itBox" class="content" style="display:none;">
                                <%= Me.BusinessDictionaryManager.Read("DMP_ErrorLoginSilente", _intLanguageID, "DMP_ErrorLoginSilente")%>
						        <p class="readAll">
						            <a title="Torna alla Home Page" style="font-size:110%;" href="/">Torna alla Home Page</a>
						        </p> 
                            </div>
                            <div id="enBox" class="content" style="padding-top:20px;width:100%;display:none;">
                               <%= Me.BusinessDictionaryManager.Read("DMP_ErrorLoginSilente", _intLanguageID, "DMP_ErrorLoginSilente")%>
						        <p class="readAll">
							        <a title="Go back to Home Page"  style="font-size:110%;" href="/en">Go back to Home Page</a>
						        </p>
                            </div>
                       </asp:PlaceHolder>
                    </div>
                </div>
                <%--<div class="colLeft" style="position: absolute; top: 546px;">
                    <div class="boxBlueDett bigBoxBlueDett">
                    </div>
                    <div class="imgSideBlue">&nbsp;</div>
                </div>--%>
            </div>
        </div>
        <asp:PlaceHolder ID="phFooter"  runat="server"  EnableViewState="false"/>
    </form>
    <div id="dialog-message" style="display:none;" title="<%= Me.BusinessDictionaryManager.Read("PLY_TitleBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_TitleBoxLinkExternal") %>">
        <div class="popupCont">	
            <img alt="Domp� Corporate" width="120px" src="/ProjectDOMPE/_slice/logoWhite.gif" />
            <p style="text-align:center; margin-bottom:0; margin-top:0;">
                <%= Me.BusinessDictionaryManager.Read("PLY_MessageBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_MessageBoxLinkExternal")%>
            </p>
        </div>
    </div>
<script type="text/javascript" language="javascript">
	<% 'If Not Me.objectSiteDomain.IsTest Then %>
    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
	<% 'End If %>

    var _currDomain = '<%=String.Format("https://{0}", Me.ObjectSiteDomain.Domain.ToString)%>';
    var _linkToEsclude = '<%=Healthware.DMP.Configuration.AppSettingsData.InternalUrls%>';

    $(document).ready(function () {
        InitEvents();
        bindExternalLinks(_currDomain, _linkToEsclude);
    });

    function InitEvents() {
        $('ul.sf-menu').superfish({
            pathClass: 'current'
        });
        $('body').jKit();
    }

    $(document).scroll(function () {
        if ($(window).scrollTop() > 100) {
            secondHeader();
        } else {
            firstHeader();
        }
    });
    function firstHeader() {
        $('.header2').fadeOut();
        $('.header').fadeIn();
    }
    function secondHeader() {
        $('.header').fadeOut();
        $('.header2').fadeIn();
    }
</script>
</body>
</html>
