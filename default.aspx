﻿<%@ Page EnableEventValidation="false" Inherits="Healthware.HP3.PresentationLayer.Share.PageShare" Language="VB" %>

<%@ Import Namespace="Healthware.HP3.Core.Base" %>
<%@ Import Namespace="Healthware.HP3" %>
<%@ Import Namespace="Healthware.HP3.Core.Base.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Content.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Localization.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.User.ObjectValues" %>
<%@ Import Namespace="Healthware.HP3.Core.Site" %>
<%@ Import Namespace="Healthware.HP3.Core.Site.ObjectValues" %>
<%@ Import Namespace="Dompe" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Healthware.Dompe.Helper" %>
<%@ Import Namespace="Healthware.HP3.Core.Box" %>
<%@ Import Namespace="Healthware.HP3.Core.Box.ObjectValues" %>

<script language="vb" runat="server">     
    Protected _siteFolder As String = String.Empty
    Dim _text As String
    Protected _keyContentType As ContentTypeIdentificator = Nothing
    Protected _keySiteArea As SiteAreaIdentificator = Nothing
    Protected _keysiteAreaSite As SiteAreaIdentificator = Nothing
    Protected _keycurrTheme As ThemeIdentificator = Nothing
    Protected _keylang As LanguageIdentificator = Nothing
    Protected _reservedAreaName As String = "PUBBLICA"
    Protected _loggedInUserId As Integer = 0
    Protected _userTypeId As Integer = 0
    Protected _SiteUrlAnalitics As String = String.Empty
    Dim mPathRedir As String = ""

    Protected Enum _userType
        Press = 1
        HCP = 2
        Partner = 3
        Sede = 4
    End Enum
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim driverUrl As New Healthware.HP3.Core.Web.Drivers.MasterPageShortDriver()
        'Dim content As String = driverUrl.GetUrlPart("content", Session("pathRdr"))
        'Response.Write(content)
        'Session("pathRdr") = ""
        'Response.End()
                
        
        If Not IsMobileDevice() Then
            
            If Not String.IsNullOrEmpty(Session("pathRdr")) Then
                
                
                Dim cntRedir As Integer = 0
                cntRedir = hasDeskContent(Session("pathRdr"))
        
                If (cntRedir = 0) Then
                    'se non è incluso nella lista
                    mPathRedir = "http://www.dompe.com" & Session("pathRdr").ToString() '& IIf(Not (String.IsNullOrEmpty(Request.QueryString.ToString())), "?" & Request.QueryString.ToString(), "")  'Request.ServerVariables("QUERY_STRING")
                Else
                    'se è incluso nella lista dei contenuti che sul mobile hano una versione ad hoc
                    'getlink del nuovo contenuto sul sito mobile 
                    Dim mpM As New Healthware.HP3.Core.Web.MasterPageManager
                    Dim domain As String = Request.Url.GetLeftPart(UriPartial.Authority)
                    Dim newUrl As String
                    newUrl = mpM.GetLink(New ContentIdentificator(cntRedir, _keylang.Id), False)
            
                    If Not String.IsNullOrEmpty(newUrl) Then
                        mPathRedir = (newUrl).Replace(domain, "http://www.dompe.com") & IIf(Not (String.IsNullOrEmpty(Request.QueryString.ToString())), "?" & Request.QueryString.ToString(), "")
                    End If
                End If
                Session("pathRdr") = ""
                If Not String.IsNullOrEmpty(mPathRedir) Then
                    Response.Redirect(mPathRedir, False)
                End If
            Else
                Response.Redirect("http://www.dompe.com", False)
            End If
        End If
    End Sub
    
    Public Function IsMobileDevice() As Boolean
        
        Dim userAgent = Request.UserAgent.ToString().ToLower()
        If Not String.IsNullOrEmpty(userAgent) Then
            
            If (Request.Browser.IsMobileDevice OrElse userAgent.Contains("iphone") OrElse userAgent.Contains("blackberry") OrElse _
                userAgent.Contains("mobile") OrElse userAgent.Contains("windows ce") OrElse userAgent.Contains("opera mini") OrElse userAgent.Contains("palm") OrElse userAgent.Contains("android")) Then
                Return True
            End If
        End If
            
        Return False
        
    End Function
    
    Function hasDeskContent(pathRdr As String) As Integer
        
        'Response.Write("ArchivioCtype:" & Dompe.ContentTypeConstants.Id.ArchivioCtype & " ctype:" & Me.PageObjectGetContentType.Id)
        Dim deskCntID As Integer = 0
        Dim _cacheKey = "_deskContents"
        If Request("cache") = "false" Then CacheManager.RemoveByKey(_cacheKey)
        
        Dim hsdeskCnt As Hashtable
        hsdeskCnt = CacheManager.Read(_cacheKey, 1)
        
        If Not (hsdeskCnt Is Nothing) AndAlso (hsdeskCnt.Count > 0) Then
            If hsdeskCnt.Contains(Me.PageObjectGetContent.Id) Then
                deskCntID = hsdeskCnt(Me.PageObjectGetContent.Id)
            End If
        Else
            hsdeskCnt = New Hashtable
            hsdeskCnt.Add(544, 515) 'La sede di Tirana
            hsdeskCnt.Add(543, 139) 'La sede di New York
            hsdeskCnt.Add(542, 55)  'Il Centro Ricerche di Napoli
            hsdeskCnt.Add(541, 54)  'Il Polo dell’Aquila            
            hsdeskCnt.Add(540, 53) 'La sede di Milano
            hsdeskCnt.Add(549, 465) 'Drug discovery
            hsdeskCnt.Add(548, 44) 'Network R&D
            hsdeskCnt.Add(547, 46) 'Pipeline R&D
            hsdeskCnt.Add(539, 74) 'Processo selezione e candidature           
            
            CacheManager.Insert(_cacheKey, hsdeskCnt, 1, 60)
            
            If hsdeskCnt.Contains(Me.PageObjectGetContent.Id) Then
                deskCntID = hsdeskCnt(Me.PageObjectGetContent.Id)
            End If
        End If

        Return deskCntID
    End Function
    
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

	
        _keyContentType = Me.PageObjectGetContentType
        _keySiteArea = Me.PageObjectGetSiteArea
        _keysiteAreaSite = Me.ObjectSiteDomain.KeySiteArea
        _keylang = Me.BusinessMasterPageManager.GetLanguageFromUrl()
        _keycurrTheme = Me.PageObjectGetTheme
		
				If _keycurrTheme.Key.Id = 435 Then response.write("a"):response.end
        
        If Not Me.ObjectTicket Is Nothing AndAlso Not String.IsNullOrEmpty(ObjectTicket.Roles.ToString) AndAlso Me.ObjectTicket.User.Key.Id > 0 Then
            _loggedInUserId = Me.ObjectTicket.User.Key.Id
			
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleDoctor) Then _userTypeId = _userType.HCP 'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('User','" & utente & "-- doctor')});", True)
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleMediaPress) Then _userTypeId = _userType.Press 'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, System.Guid.NewGuid.ToString, "$(document).ready(function(){recordOutboundLink('User','" & utente & "-- media press')});", True)
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RolePartner) Then _userTypeId = _userType.Partner
            If Me.ObjectTicket.Roles.HasRole(Dompe.Role.RoleSede) Then _userTypeId = _userType.Sede
        End If
        
        'Response.Write("codeLanguage:" & Me.BusinessMasterPageManager.GetUrlParam(Healthware.HP3.Core.Web.MasterPageManager.ParamEnumerator.Language).Name.ToLower)
        
        If ChechLang(_keylang, _keysiteAreaSite) Then
            Dim codeLanguage As String = Me.BusinessMasterPageManager.GetUrlParam(Healthware.HP3.Core.Web.MasterPageManager.ParamEnumerator.Language).Name.ToLower
            
            If codeLanguage.ToLower <> Me.BusinessMasterPageManager.GetLanguage.Code.ToLower Then
                Me.BusinessAccessService.SetCurrentLanguageByCode(codeLanguage)
                Response.Redirect(Request.Url.ToString, False)
            End If
        End If
        _SiteUrlAnalitics = Request.Url.Host
        _reservedAreaName = getCustomVar(Me.PageObjectGetTheme.Id)
        
        'VERIFICA RUOLO UTENTE / SEZIONE
        Dim oRoleColl As New RoleCollection
        Dim accessoSezione As Boolean = False
        oRoleColl = Me.BusinessSiteAreaManager.ReadRoles(Me.PageObjectGetSiteArea)
             
        
        If oRoleColl Is Nothing OrElse oRoleColl.Count = 0 Then
            accessoSezione = True
        Else
            Dim id_ruolo_utente As Array
            Dim id_r As Integer
            If Not (oRoleColl Is Nothing) Then
                id_ruolo_utente = Split(Me.ObjectTicket.Roles.ToString, ",")
                If id_ruolo_utente.Length() - 1 > 0 Then
                    For id_r = 0 To id_ruolo_utente.Length() - 1
                        If (oRoleColl.HasRole(CLng(id_ruolo_utente(id_r).ToString))) Then accessoSezione = True
                    Next
                Else
                    If Not Me.ObjectTicket Is Nothing AndAlso Not String.IsNullOrEmpty(ObjectTicket.Roles.ToString) Then
                        If (oRoleColl.HasRole(CLng(Me.ObjectTicket.Roles.ToString))) Then accessoSezione = True
                    End If
                End If
            End If
        End If
 
       
        If Not accessoSezione = True Then
            Response.Redirect("/", False)
        End If
        '*******************************************************************  
        
        
        Dim ctrlPath As String = String.Empty
        _siteFolder = Me.ObjectSiteFolder()
        _text = Me.BusinessDictionaryManager.Read("firstLevIt", Me.PageObjectGetLang.Id)
        
        'GetPageTitle()
        'GetMetaTag()

        'convertire in stringa e aggiungerlo a _text
        
        Dim ctrlPathFtr As String = String.Format("/{0}/HP3Common/footerCtl.ascx", Me._siteFolder)
                            
        Dim pageHolderFtr = New Page()
        Dim viewControlFtr = New UserControl
        viewControlFtr = pageHolderFtr.LoadControl(ctrlPathFtr)       
       
        pageHolderFtr.Controls.Add(viewControlFtr)
        Dim outputFtr As New StringWriter()
        HttpContext.Current.Server.Execute(pageHolderFtr, outputFtr, False)
        
        
        _text = _text.Replace("[phFooter1]", outputFtr.ToString()).Replace("[phFooter2]", outputFtr.ToString()).Replace("[phFooter3]", outputFtr.ToString()).Replace("[phFooter4]", outputFtr.ToString()).Replace("[phFooter5]", outputFtr.ToString()).Replace("[phFooter6]", outputFtr.ToString()).Replace("[phFooter7]", outputFtr.ToString()).Replace("[HOMENEWS]", addNews()).Replace("[boxBanner]", boxSliderCampaign())
        

        ctrlPath = String.Format("/{0}/HP3Common/Menu.ascx", Me._siteFolder)
        LoadSubControl(ctrlPath, Me.phMenuSx)
    End Sub
    
    
    
    Function boxSliderCampaign() As String
        Dim res As String = String.Empty
        Dim bannSerch As New BannerSearcher
        Dim bannColl As New BannerCollection
       
        Dim _boxSlideKey As String = "hpBoxSliderNewMobile:" & _keylang.Id
        If Request("cache") = "false" Then CacheManager.RemoveByKey(_boxSlideKey)
        res = CacheManager.Read(_boxSlideKey, 1)
        If String.IsNullOrEmpty(res) Then
            bannSerch.KeyBox.Id = 4
            bannSerch.KeyLanguage.Id = _keylang.Id
            bannSerch.KeySite.Id = Me.PageObjectGetSite.Id
            
  
            bannSerch.SortType = BannerGenericComparer.SortType.ByDateStart
            bannSerch.SortOrder = BannerGenericComparer.SortOrder.ASC
            bannColl = Me.BusinessAdvertisingManager.Read(bannSerch)
            
            If Not bannColl Is Nothing AndAlso bannColl.Count > 0 Then
                Dim _sb As StringBuilder = New StringBuilder()
                Dim _sbUl As StringBuilder = New StringBuilder()
                Dim i As Integer = 1
                Dim imgBannerPor As String = ""
                Dim imgBannerLan As String = ""
                'inizio
                _sb.Append("<div class='swiper-container sliderHp'><div class='swiper-wrapper'>")
                
                'val nel ciclo
                Dim strSel As String = String.Empty
                For Each itm As BannerValue In bannColl
                    
                    
                    imgBannerPor = "/HP3Banner/" & itm.Source.Split(".")(0) & "_Por." & itm.Source.Split(".")(1)
                    imgBannerLan = "/HP3Banner/" & itm.Source.Split(".")(0) & "_Lan." & itm.Source.Split(".")(1)
                    
                    _sb.Append("<style>")
                    _sb.Append("@media only screen and (orientation: landscape){")
                    _sb.Append(".page1 .slideHp" & i & " { background-image: url('" & imgBannerLan & "') !important;}")
                    _sb.Append("}")
                    '_sb.Append("@media only screen and (orientation: portrait){")
                    '_sb.Append(".page1 .slideHp" & i & " { background-image: url('" & imgBannerPor & "') !important;}}")
                    _sb.Append("</style>")
                    
                    _sb.Append("<div class='swiper-slide slideHp" & i & "' style='background-image: url(""" & imgBannerPor & """)'>")
                    _sb.Append("<div class='cntItem'>")
                    _sb.Append("<div class='container'>")
                    _sb.Append("<div class='pageCnt'>")
                    _sb.Append("<div class='pageBox'>")
                    _sb.Append("<h2>" & itm.Name & "</h2>")
                    _sb.Append("<h3>" & itm.Description & "</h3>")
                    If (itm.Name = "The Rarest Ones") Then
                        _sb.Append("<a class='go' title='Read More' href='https://youtu.be/Tmv2LKNeEoI?list=PLrFVSK5Z1Ef9V93eCqS4idIQ_g1OXoxQb'>" & itm.Name & "</a>")
                    End If
                    _sb.Append("<a class='view' title='The Rarest Ones' href='javascript:void(0);' onclick=""javascript:secondLevel(1,1,'" & itm.Link & "');""><span>" & itm.Name & "</span></a>")
                    _sb.Append("</div>")
                    _sb.Append("</div>")
                    _sb.Append("</div>")
                    _sb.Append("</div>")
                    _sb.Append("</div>")
                    i += 1
                    imgBannerLan = ""
                    imgBannerPor = ""
                Next
                
                'fine
                _sb.Append("</div><div class='swiper-pagination swiper-pagination-h'></div></div>")
           
                
                res = _sb.ToString
                CacheManager.Insert(_boxSlideKey, res, 1, 60)
            End If
        End If
        Return res
    End Function
    
    
    Function addNews() As String
        'If HomePage.IndexOf("[HOMENEWS]") = -1 Then Return String.Empty
        
        'News da escludere
        Dim _out As String = String.Empty
        Dim _topKeysNews As String = String.Empty
        Dim thval As New ThemeValue
        thval = Me.BusinessThemeManager.Read(New ThemeIdentificator(Dompe.ThemeConstants.idNews))
        If Not thval Is Nothing AndAlso thval.KeyContent.Id > 0 Then
            _topKeysNews = Healthware.Dompe.Helper.Helper.GetContentPrimaryKey(thval.KeyContent.Id, _keylang.Id)
        End If
        '--------------------
        
        Dim so As New ContentExtraSearcher()
        Dim sum As Integer = 0
        Dim tmpInt As Integer = 0
 
        so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved
        so.KeySiteArea.Id = Dompe.SiteAreaConstants.IDSiteareaNews
        so.KeyContentType.Id = Dompe.ContentTypeConstants.Id.ArchivioCtype
        so.key.IdLanguage = _keylang.Id
        so.Display = SelectOperation.Enabled
        so.Active = SelectOperation.Enabled
        'so.Delete = SelectOperation.Disabled

        If Not String.IsNullOrWhiteSpace(_topKeysNews) Then so.KeysToExclude = _topKeysNews
       
        so.SortingCriteria.Add(New ContentSort(ContentGenericComparer.SortOrder.DESC, ContentGenericComparer.SortType.ByRank))
        so.SortingCriteria.Add(New ContentSort(ContentGenericComparer.SortOrder.DESC, ContentGenericComparer.SortType.ByData))

        so.PageNumber = 1
        so.PageSize = 3
        so.Properties.Add("Title")
        so.Properties.Add("Launch")
        so.Properties.Add("DatePublish")
        so.Properties.Add("ContentType.Key.Id")
        so.Properties.Add("ContentType.Key.Name")
        so.Properties.Add("ContentType.Key.Domain")
        so.Properties.Add("FullText")
        so.Properties.Add("LinkUrl")
        so.Properties.Add("DateExpiry")
        so.Properties.Add("LinkLabel")
        so.Properties.Add("Code")
        so.Properties.Add("ApprovalStatus")
        so.Properties.Add("Copyright")
        so.Properties.Add("SiteArea.Key.Id")
        so.Properties.Add("BookReferer")
        so.Properties.Add("FullTextComment")
        so.Properties.Add("Qty")
        so.Properties.Add("Rank")
       
        Dim oContColl As ContentExtraCollection = Me.BusinessContentExtraManager.Read(so)
        Dim _newsSb As New StringBuilder()
        
        If Not oContColl Is Nothing AndAlso oContColl.Count > 0 Then
            '_newsSb.Append("<div class='pageTxt page3Txt'><div class='container' style='height:50%;'><div class='pageCnt page3Cnt'><h3 style='letter-spacing:-2px;color:#e62139;'>News</h3>")
            
            Dim i As Integer = 1
            For Each oContExtra As ContentExtraValue In oContColl
                If i = 1 Then
                
                    _newsSb.Append("<div class='container'><h3>News</h3><div class='newsItem first'><span class='date'>" & Day(oContExtra.DatePublish.GetValueOrDefault) & " " & MonthName(Month(oContExtra.DatePublish.GetValueOrDefault), True) & "</span><span>" & oContExtra.Title & "</span><a class='view' title='" & oContExtra.Title & "' href='" & Helper.ResolveUrl(oContExtra, Dompe.ThemeConstants.idNewsMobile) & "'><span>Read More</span></a></div></div>")
                    _newsSb.Append("<div class='news_red'><div class='container'>")
                Else
                    '                    _newsSb.Append("<div class='newsItem' ""><span class='date' " & IIf(i = 2, "style='margin-top:0px;'", String.Empty) & ">" & Day(oContExtra.DatePublish.GetValueOrDefault) & " " & MonthName(Month(oContExtra.DatePublish.GetValueOrDefault), True) & "</span><span>" & oContExtra.Title & "</span><a href='" & Helper.ResolveUrl(oContExtra, Dompe.ThemeConstants.idNewsMobile) & "' title='" & oContExtra.Title & "' class='view'>Read More</a></div>")
                    _newsSb.Append("<div class='newsItem'><span class='date'>" & Day(oContExtra.DatePublish.GetValueOrDefault) & " " & MonthName(Month(oContExtra.DatePublish.GetValueOrDefault), True) & "</span><span>" & oContExtra.Title & "</span><a class='view' title='" & oContExtra.Title & "' href='" & Helper.ResolveUrl(oContExtra, Dompe.ThemeConstants.idNewsMobile) & "'><span>Read More</span></a></div>")
                End If
                i += 1
            Next
            _newsSb.Append("</div></div>")
        End If
        
        Return _newsSb.ToString()
    End Function
    
    
    'BISOGNA RIMAPPARE I TEMI CON QUELLI DEL SITO MOBILE
    Function getCustomVar(ByVal _intThemeId As Integer) As String
        Dim ht As New Hashtable
        ht.Add(296, "PARTNER")
        ht.Add(297, "PARTNER")
        ht.Add(298, "PARTNER")
        ht.Add(299, "PARTNER")
        
        ht.Add(256, "HCP")
        ht.Add(257, "HCP")
        ht.Add(258, "HCP")
        ht.Add(259, "HCP")
        ht.Add(260, "HCP")
        ht.Add(262, "HCP")
        
        ht.Add(240, "PRESS")
        ht.Add(241, "PRESS")
        ht.Add(242, "PRESS")
        ht.Add(243, "PRESS")
        ht.Add(295, "PRESS")
        
        If ht.Contains(_intThemeId) Then Return ht(_intThemeId)
        Return "PUBBLICA"
    End Function
    
    Function ChechLang(ByVal keyLang As LanguageIdentificator, ByVal keysiteareaSite As SiteAreaIdentificator) As Boolean
        If keyLang Is Nothing Then Return False
        Dim langColl As LanguageCollection = Me.BusinessSiteAreaManager.ReadLanguageSiteRelation(keysiteareaSite)
        If Not langColl Is Nothing AndAlso langColl.Count > 0 Then
            For Each lang As LanguageValue In langColl
                If lang.Key.Code = keyLang.Code Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function
    
    Protected Sub LoadSubControl(ByVal path As String, ByRef destPanel As Control)
        If destPanel Is Nothing Then
            Throw New ArgumentException("destPanel")
        End If
        
        If Not String.IsNullOrEmpty(path) Then
            Dim tmpCtrl As Control = LoadControl(path)
            If Not tmpCtrl Is Nothing Then
                destPanel.Controls.Add(tmpCtrl)
            End If
        End If
    End Sub
    
    
</script>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dompé Corporate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="/demo/_css/swiper.min.css">
    <link rel="stylesheet" href="/demo/_css/styleOld.css">
    <link rel="stylesheet" href="/demo/_css/style.css">	
    <script src="/demo/_js/jquery.easing.1.3.js"></script>
    <script src="/demo/_js/swiper.min.js"></script>
</head>



<body class="onePage">
	<div class="relHid">
    
  <%=_text%>


    <div class="secondPageLevel" id="panel1">
        <div class="secondPageLevel2">
                <div class="loading">Loading</div>
        </div>
    </div>
    
    <div class="secondPageLevel" id="panel2">
        <div class="secondPageLevel2">
            
                <div class="loading">Loading</div>
             
        </div>
   </div>
    <%--end first level swipers--%>
    <a href="#" title="Menu" class="menu lines-button x2"><span class="lines">&nbsp;</span></a>
    <a href="#" title="Previous Page" class="backBtn"><span>&nbsp;</span></a>

    <form id="myForm" runat="server" enctype="multipart/form-data">
		<asp:PlaceHolder ID="phMenuSx" runat="server" EnableViewState="false" />
	</form>

     <script type='text/javascript'>
         $(document).ready(function () {
			//GA
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-48312377-5', 'auto');
			ga('set', 'dimension1', '<%=_userTypeId%>');
			ga('set', 'dimension2', '<%=_loggedInUserId%>');
			ga('set', 'dimension3', '<%=_reservedAreaName%>');
			ga('set', 'dimension4', '<%=_keylang.Code.ToString()%>');
			ga('send', 'pageview');
			//Fine GA
			
			bindFirstLevelLinks();

		<%
		Dim pathRdr As String
		pathRdr = Session("pathRdr")
		If (Not String.IsNullOrEmpty(pathRdr)) Then
		%>
             secondLevel(1, 1, '<%=pathRdr%>', 1);
		 <% 
        Session("pathRdr") = ""
		End If 
		%>
         });

         // noBounce.init({ preventDefault: false, animate: true });
         function bindFirstLevelLinks() {
             var curDomain = 'http://m.dompe.com';
             var domainSelector = '';
             domainSelector += "[href^='" + curDomain + "']";
             $("a" + domainSelector + "[href^='http://']").each(function (index) {
                 if (!($(this).hasClass("newPage"))) {
                     var urlPage = $(this).attr("href").replace(curDomain, '');
                     $(this).attr("href", "javascript:void(0)");
                     $(this).attr("onclick", "javascript:secondLevel(1,1,'" + urlPage + "');");
                 }
             });
         };
    </script>

  
    <script>
        var currPage = 1;
        var numPage = 7;
        var i = 0;
        var j = 0;
        var ctrlTxt = 0;
        var ctrlTxtOriz = 0;
        var menuOpen = 0;
        var swiperPages = new Array();
        var swiperV;
		var ctrlbodyplus=0;
        $(window).resize(function () {
            $(".swiper-container").css('display', 'block');
            for (i = 0; i < numPage; i++) {
                j = i + 1;
                swiperPages[i].update();
				swiperV.slideTo(0, 0);
				swiperV.slideTo(1, 0);
				swiperV.update();
            }
            $(".swiper-container").css('display', 'none');
            $(".page" + currPage).css('display', 'block');
            $(".itemsMenu ul li a").removeClass('active');
            $(".home-link").removeClass('active');
            $(".itemsMenu ul li a.menu"+currPage).addClass('active');
            scrollBarHeight2();
        });

		$(window).load(function () {
			$('.slideHp1 .pageBox').css('display','block');
            document.ontouchmove = function (event) {
                event.preventDefault();
            }
            document.documentElement.style.webkitTapHighlightColor = "rgba(0,0,0,0)";
            init();
			if(ctrlInitPage==1){
				$(".swiper-container").css('display', 'none');
				$(".page1").css('display', 'block');
			}
            initOriz();

			window.addEventListener('orientationchange', function () {
				var originalBodyStyle = getComputedStyle(document.body).getPropertyValue('display');
				document.body.style.display='none';
				setTimeout(function () {
				document.body.style.display = originalBodyStyle;
			}, 10);
		});


		});
        $(document).ready(function () {
			$('.slideHp1 .pageBox').css('display','none');
            $(".itemsMenu ul li a").removeClass('active');
            $(".home-link").addClass('active');
       });

        function initOriz() {
			swiperV = new Swiper('.sliderHp', {
			autoplay: 2500,
			autoplayDisableOnInteraction: false,
			loop: true,
			speed:600,
			pagination: '.swiper-pagination-h',
			paginationClickable: true,
			onSlideChangeStart: function (swiper) {
				$(".page1 .sliderHp .cntItem").css('display', 'none');
			},
			onSlideChangeEnd: function (swiper) {
				ctrlTxtOriz = 0;
				var currInd = swiper.activeIndex;
				currInd = currInd + 1;
				if (swiper.activeIndex > swiper.previousIndex) {
		            $('.page1 .sliderHp .cntItem').css('top', '0');
					txtMoveOriz(currInd, 2, 1);
				} else {
		            $('.page1 .sliderHp .cntItem').css('top', '0');
					txtMoveOriz(currInd, 1, 1);
				}
			},
			onTouchMove: function (swiper) {
				var myDiff = swiper.touches.currentX - swiper.touches.startX;
				var currInd = swiper.activeIndex;
				currInd = currInd + 1;
				if (ctrlTxtOriz == 0) {
					if (myDiff > 10) {
						ctrlTxtOriz = 2;
						txtExitOriz(currInd, 2);
					} else if (myDiff < -10) {
						ctrlTxtOriz = 1;
						txtExitOriz(currInd, 1);
					}
				}
			},
			onTouchEnd: function (swiper) {
				var currInd = swiper.activeIndex;
				currInd = currInd + 1;
				if (ctrlTxtOriz == 1) {
					txtMoveOriz(currInd, 1, 0);
					ctrlTxtOriz = 0;
				} else if (ctrlTxtOriz == 2) {
					txtMoveOriz(currInd, 2, 0);
					ctrlTxtOriz = 0;
				}
			}
		});
	}
	
		function startAp(){
		   swiperV.startAutoplay();
		}

        function txtMoveOriz(currInd, mover, change) {
			if(currInd==5){
				currInd=1;
			}else{
				currInd=currInd-1;
			}
            $(".page1 .sliderHp .cntItem").css('display', 'none');
            $('.page1 .slideHp' + currInd + ' .cntItem').css('display', 'block');
            if (change == 1) {
                if (mover == 1) {
                    $('.page1 .slideHp' + currInd + ' .cntItem').css('left', '-200%');
                } else {
                    $('.page1 .slideHp' + currInd + ' .cntItem').css('left', '200%');
                }
            }
            $('.page1 .slideHp' + currInd + ' .cntItem').stop().animate({ left: "0" }, 600, 'easeOutCubic', function () {
            });
        }
        function txtExitOriz(currInd, mover) {
			if(currInd==5){
				currInd=1;
			}else{
				currInd=currInd-1;
			}
            if (mover == 1) {
                $('.page1 .slideHp' + currInd + ' .cntItem').stop().animate({ left: "0" }, 150, function () {
                    $('.page1 .slideHp' + currInd + ' .cntItem').stop().animate({ left: "-200%" }, 900, 'easeInCubic', function () {
                    });
                });
            } else {
                $('.page1 .slideHp' + currInd + ' .cntItem').stop().animate({ left: "0" }, 150, function () {
                    $('.page1 .slideHp' + currInd + ' .cntItem').stop().animate({ left: "200%" }, 900, 'easeInCubic', function () {
                    });
                });
            }
        }
		
        function init() {
            for (i = 0; i < numPage; i++) {
                j = i + 1;
                swiperPages[i] = new Swiper('.page' + j, {
                    direction: 'vertical',
                    nextButton: '.page' + j + ' .next',
                    prevButton: '.page' + j + ' .prev',
                    onSlideChangeStart: function (swiper) {
                        if (swiper.activeIndex > swiper.previousIndex) {
                            arrowAnim(2);
                        } else {
                            arrowAnim(1);
                        }
                        $('.page' + currPage).css('pointer-events', 'none');
                    },
                    onSlideChangeEnd: function (swiper) {
                        ctrlTxt = 0;
                        var currInd = swiper.activeIndex;
                        currInd = currInd + 1;
                        if (swiper.activeIndex > 0) {
                            $(swiper.params.prevButton).css("display", "block");
                        }
                        else {
                            $(swiper.params.prevButton).css("display", "none");
                        }
                        if (swiper.isEnd) {
                            $(swiper.params.nextButton).css("display", "none");
                        }
                        else {
                            $(swiper.params.nextButton).css("display", "block");
                        }
                        if (swiper.activeIndex > swiper.previousIndex) {
                            //console.log('onSlideChangeEnd 1');
                            txtMove(currInd, 2, 1);
                        } else {
                            //console.log('onSlideChangeEnd 2');
                            txtMove(currInd, 1, 1);
                        }
                        if (swiper.activeIndex > swiper.previousIndex) {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 2, 2);
                            }
                        } else {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 1, 2);
                            }
                        }
                        $('.page' + currPage).css('pointer-events', 'auto');
                    },
                    onTouchMove: function (swiper) {
                        var myDiff = swiper.touches.currentY - swiper.touches.startY;
                        var currInd = swiper.activeIndex;
                        currInd = currInd + 1;
                        if (myDiff > 10) {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 2, 1);
                            }
                        } else if (myDiff < -10) {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 1, 1);
                            }
                        }
                        if (ctrlTxt == 0) {
                            if (myDiff > 10) {
                                ctrlTxt = 2;
                                //console.log(swiper.touches.startY+' '+swiper.touches.currentY+' '+myDiff+' onTouchMove 2');
                                txtExit(currInd, 2);
                            } else if (myDiff < -10) {
                                ctrlTxt = 1;
                                //console.log(swiper.touches.startY+' '+swiper.touches.currentY+' '+myDiff+' onTouchMove 1');
                                txtExit(currInd, 1);
                            }
                        }
						swiperV.stopAutoplay();
                    },
                    onTouchEnd: function (swiper) {
                        var currInd = swiper.activeIndex;
                        currInd = currInd + 1;
                        if (ctrlTxt == 1) {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 1, 2);
                            }
                        } else if (ctrlTxt == 2) {
                            if (((currPage == 1) && (currInd == 3)) || ((currPage == 6) && (currInd == 2))) {
                                newsBg(currInd, 2, 2);
                            }
                        }
                        if (ctrlTxt == 1) {
                            //console.log('onTouchEnd 1');
                            txtMove(currInd, 1, 0);
                            ctrlTxt = 0;
                        } else if (ctrlTxt == 2) {
                            //console.log('onTouchEnd 2');
                            txtMove(currInd, 2, 0);
                            ctrlTxt = 0;
                        }
						startAp();
                    }
                });
            }
			
			if(ctrlInitPage!=1){
				swapPage(ctrlInitPage);
			}
        }
		
		var ctrlInitPage=1;
		var nameDomain='<%=ConfigurationManager.AppSettings("internalUrlsMobile").ToString()%>';
		if(window.location.href==(nameDomain+'chi-siamo/')){
			ctrlInitPage=2;
		}else if(window.location.href==(nameDomain+'ricerca-sviluppo/')){
			ctrlInitPage=3;
		}else if(window.location.href==(nameDomain+'soluzioni-terapeutiche/')){
			ctrlInitPage=4;
		}else if(window.location.href==(nameDomain+'responsabilita-sociale/')){
			ctrlInitPage=5;
		}else if(window.location.href==(nameDomain+'media/')){
			ctrlInitPage=6;
		}else if(window.location.href==(nameDomain+'lavorare/')){
			ctrlInitPage=7;
		}
        function txtMove(currInd, mover, change) {
            $(".cntItem").css('display', 'none');
			if((currPage==1)&&(currInd==1)){
	            $('.page1 .sliderHp .cntItem').css('left', '0');
            	$(".page1 .sliderHp .cntItem").css('display', 'block');
			}else{
            	$(".page1 .sliderHp .cntItem").css('display', 'none');
			}
            $('.page' + currPage + ' .slide' + currInd + ' .cntItem').css('display', 'block');
            if (change == 1) {
                if (mover == 1) {
                    $('.page' + currPage + ' .slide' + currInd + ' .cntItem').css('top', '-200%');
                } else {
                    $('.page' + currPage + ' .slide' + currInd + ' .cntItem').css('top', '200%');
                }
            }
            $('.page' + currPage + ' .slide' + currInd + ' .cntItem').stop().animate({ top: "0" }, 600, 'easeOutCubic', function () {
            });
        }
        function txtMoveNo(currInd, mover) {
            $(".cntItem").css('display', 'none');
            $('.page' + currPage + ' .slide' + currInd + ' .cntItem').css('display', 'block');
            $('.page' + currPage + ' .slide' + currInd + ' .cntItem').css('top', '0');
        }
        function txtExit(currInd, mover) {
            if (mover == 1) {
                $('.page' + currPage + ' .slide' + currInd + ' .cntItem').stop().animate({ top: "0" }, 150, function () {
                    $('.page' + currPage + ' .slide' + currInd + ' .cntItem').stop().animate({ top: "-200%" }, 900, 'easeInCubic', function () {
                    });
                });
            } else {
                $('.page' + currPage + ' .slide' + currInd + ' .cntItem').stop().animate({ top: "0" }, 150, function () {
                    $('.page' + currPage + ' .slide' + currInd + ' .cntItem').stop().animate({ top: "200%" }, 900, 'easeInCubic', function () {
                    });
                });
            }
        }
        function newsBg(currInd, mover, ctrl) {
            if (ctrl == 1) {
                if (mover == 1) {
                    $('.redBg').animate({ height: "+=2" }, 10, function () {
                    });
                } else {
                    $('.redBg').animate({ height: "-=2" }, 10, function () {
                    });
                }
            } else {
                $('.redBg').animate({ height: "50%" }, 300, function () {
                });
            }
        }
		var ctrlPageAn=0;
        function swapPage(itemPage) {
            $("#panel1").css('left', '100%');
            $("#panel1").css('z-index', '2');
            //document.getElementById("panel1").scrollTop = 0;
            $("#panel2").css('left', '100%');
            $("#panel2").css('z-index', '2');
            $("#panel1 .secondPageLevel2").html('<div class="loading">Loading</div>');
            $("#panel2 .secondPageLevel2").html('<div class="loading">Loading</div>');
            //document.getElementById("panel2").scrollTop = 0;
            //if ((currPage != itemPage) || (panel > 0)) {
                $(".swiper-container").css('display', 'none');
                $(".swiper-container").css('left', '100%');
                swiperPages[itemPage - 1].slideTo(0, 0);
                $(".page" + itemPage).css('display', 'block');
                $(".page" + itemPage).css('left', '0');
/*				if(itemPage==1){
					swiperV.slideTo(0, 0);
					swiperV.slideTo(1, 0);
					swiperV.update();
				}
*/              
				currPage = itemPage;
				$(".itemsMenu ul li a").removeClass('active');
				$(".home-link").removeClass('active');
				$(".itemsMenu ul li a.menu"+currPage).addClass('active');
                txtMoveNo(1, 1);
            //}
			
            menuOpen = 0;
            $(".menu").removeClass('close');
			if(currPage==1){
				$(".page" + itemPage + " .sliderHp").css('display', 'block');
				$(".page" + itemPage + " .sliderHp").css('left', '0');
				swiperV.slideTo(0, 0);
				swiperV.slideTo(1, 0);
			}
            $(".menuBox").animate({ left: "-100%" }, 500, 'easeOutCubic', function () {
				if(currPage==1){
					swiperV.update();
				    swiperV.startAutoplay();
				}else{
					swiperV.stopAutoplay();
				}
            });
            panel = 0;
        }
        $(".menu").click(function () {
            if (menuOpen == 0) {
                menuOpen = 1;
                $(".menu").addClass('close');
                $('.itemsMenu').show();
                $('.home-link').show();
                //cleanMenuForm();

                /*search*/
                $(".searchBox").hide();
                $(".search").removeClass("searchSel");

                /*login*/
                $(".loginBox").hide();
                $(".lock").removeClass('lockSel');

                $(".menuBox").animate({ left: "0" }, 500, 'easeInCubic', function () {
                });
            } else {
				swiperV.slideTo(0, 0);
				swiperV.slideTo(1, 0);
				swiperV.update();
                menuOpen = 0;
                $(".menu").removeClass('close');               
                $(".menuBox").animate({ left: "-100%" }, 500, 'easeOutCubic', function () {
                });
            }
        });

		
        $(".itemsMenu a").click(function () {
			navStory=[];
			cntNav=0;
            var str = $(this).attr('class');
            var itemPage = str.replace("active", "");
            var itemPage = itemPage.replace(" ", "");
            var itemPage = itemPage.replace("menu", "");
            swapPage(itemPage);
            $(".backBtn").css('left', '123%');
			
			//GA
			ga('send', 'event', 'Link Main Menu', 'Click', $(this).attr('title'));
			//Fine GA
        });
        $(".home-link").click(function () {
			navStory=[];
			cntNav=0;
            var itemPage = 1;
            swapPage(itemPage);
            $(".backBtn").css('left', '123%');
            $(".itemsMenu ul li a").removeClass('active');
            $(".home-link").addClass('active');
			
			//GA
			ga('send', 'event', 'Link Main Menu', 'Click', $(this).attr('title'));
			//Fine GA			
        });
        /*	$( ".prev" ).click(function() {
                $( this ).children('span').animate({top: "-100%"}, 400, function() {
                    $( this ).css('top','100%');
                    $( this ).animate({top: "0%"}, 400, function() {
                    });
                });
            });
            $( ".next" ).click(function() {
                $( this ).children('span').animate({top: "100%"}, 400, function() {
                    $( this ).css('top','-100%');
                    $( this ).animate({top: "0%"}, 400, function() {
                    });
                });
            });
        */	$(".view").click(function () {
    $(this).children('span').animate({ left: "100%" }, 200, function () {
        $(this).css('left', '-100%');
        $(this).animate({ left: "0%" }, 200, function () {
        });
    });
});

        var panel = 0;
        function arrowAnim(mover) {
            if (mover == 1) {
                $('.page' + currPage + ' a.prev span').animate({ top: "-100%" }, 400, function () {
                    $('.page' + currPage + ' a.prev span').css('top', '100%');
                    $('.page' + currPage + ' a.prev span').animate({ top: "0%" }, 400, function () {
                    });
                });
            } else {
                $('.page' + currPage + ' a.next span').animate({ top: "100%" }, 400, function () {
                    $('.page' + currPage + ' a.next span').css('top', '-100%');
                    $('.page' + currPage + ' a.next span').animate({ top: "0%" }, 400, function () {
                    });
                });
            }
        }

        var pagePrev = 0;
        var slidePrev = 0;

function navClass(page,slide,linkPage){
this.page=page;
this.slide=slide;
this.linkPage=linkPage;
return this;
}
var navStory=new Array();
var cntNav=0;


        function secondLevel(page, slide, linkPage, stateCtrl) {

			//GA
			ga('send', 'event', 'Link To Page', 'Click', linkPage);
			//Fine GA		
			
			if(stateCtrl!=1){
				navStory[cntNav]=new navClass(page,slide,linkPage);
				cntNav++;
			}
			
            pagePrev = page;
            var panOther = 0;
            if (panel == 1) {
                panel = 2;
                panOther = 1;
            } else {
                panel = 1;
                panOther = 2;
            }
            $("#panel" + panel).css('z-index', '3');
            $("#panel" + panOther).css('z-index', '2');

			//CHIAMATA AJAX E POI AL SUCCESS FAI ANIMATE		       
			scrollBarHeight();			
			$("#panel" + panel).animate({ left: "0" }, 300, function () {
				$("#panel" + panOther).css('left', '100%');
				//document.getElementById("panel" + panOther).scrollTop = 0;
				$("#panel" + panOther+' .secondPageLevel2').html('<div class="loading">Loading</div>');

				$.ajax({
				    contentType: "text/html; charset=utf-8",
				    url: linkPage,
				    dataType: "html",
				    data: { 'fromNav': 'yes', 'brandText': 'testval' },
				    success: function(data) {
				        $("#panel" + panel+ " .secondPageLevel2").html(data);
				        //svuota l'altro pannello
						
						
				        scrollBarHeight();
						
						
						
						setTimeout(scrollBarHeight2, 2000);
						
				    }
				});
				
				
				
			});

			$(".backBtn").animate({ left: "23%" }, 300, 'easeInCubic', function () {
			});			
			
			
            
        }

        function secondLevelReverse(page, slide, linkPage, stateCtrl) {
			
			
			if(stateCtrl!=1){
				navStory[cntNav]=new navClass(page,slide,linkPage);
				cntNav++;
			}
			
			
            pagePrev = page;
            var panOther = 0;
            if (panel == 1) {
                panel = 2;
                panOther = 1;
            } else {
                panel = 1;
                panOther = 2;
            }
            $("#panel" + panel).css('z-index', '1');
            $("#panel" + panel).css('left', '0');
            $("#panel" + panOther).css('z-index', '2');

			//CHIAMATA AJAX E POI AL SUCCESS FAI ANIMATE		       
			scrollBarHeight();			
			$("#panel" + panOther).animate({ left: "100%" }, 300, function () {
				$("#panel" + panel).css('z-index', '3');
				//document.getElementById("panel" + panOther).scrollTop = 0;
				$("#panel" + panOther+' .secondPageLevel2').html('<div class="loading">Loading</div>');

				$.ajax({
				    contentType: "text/html; charset=utf-8",
				    url: linkPage,
				    dataType: "html",
				    data: { 'fromNav': 'yes', 'brandText': 'testval' },
				    success: function(data) {
				        $("#panel" + panel+ " .secondPageLevel2").html(data);
				        //svuota l'altro pannello
						
						
				        scrollBarHeight();
						
						
						
						setTimeout(scrollBarHeight2, 2000);
						
				    }
				});
				
				
				
			});

			$(".backBtn").animate({ left: "23%" }, 300, 'easeInCubic', function () {
			});			
			
			
            
        }


        $(".backBtn").click(function () {
			
			if(navStory.length>0){
				navStory.pop(); 
				cntNav--;
			}
			if(navStory.length>0){
				var arrpos=navStory.length-1;
				secondLevelReverse(navStory[arrpos].page, navStory[arrpos].slide, navStory[arrpos].linkPage,1);
			}else{
				$(".swiper-container").css('display', 'block');
				for (i = 0; i < numPage; i++) {
					j = i + 1;
					swiperPages[i].update();
					swiperV.slideTo(0, 0);
					swiperV.slideTo(1, 0);
					swiperV.update();
				}
				$(".swiper-container").css('display', 'none');
				$(".page" + currPage).css('display', 'block');
	
	
	
				$(".backBtn").animate({ left: "123%" }, 300, 'easeInCubic', function () {
				});
				$("#panel" + panel).animate({ left: "100%" }, 300, 'easeInCubic', function () {
					$("#panel" + panel).css('z-index', '2');
					//document.getElementById("panel" + panel).scrollTop = 0;
	
					$("#panel" + panel+' .secondPageLevel2').html('<div class="loading">Loading</div>');
					
					panel = 0;
				});
			}
            //swapPageAnim(pagePrev);
        });
        function swapPageAnim(itemPage) {
            if ((currPage != itemPage) || (panel > 0)) {
                $(".swiper-container").css('display', 'none');
                $(".swiper-container").css('left', '100%');
                swiperPages[itemPage - 1].slideTo(0, 0);
                txtMoveNo(1, 1);
                $(".page" + itemPage).css('display', 'block');
                $(".page" + itemPage).css('z-index', '9');
                $(".page" + itemPage).animate({ left: "0%" }, 500, 'easeOutCubic', function () {
                    $("#panel1").css('left', '100%');
                    //document.getElementById("panel1").scrollTop = 0;
                    $("#panel1").css('z-index', '2');
                    $("#panel2").css('left', '100%');
                    //document.getElementById("panel2").scrollTop = 0;
                    $("#panel2").css('z-index', '2');
                    $("#panel1 .secondPageLevel2").html('<div class="loading">Loading</div>');
                    $("#panel2 .secondPageLevel2").html('<div class="loading">Loading</div>');
                    $(".page" + itemPage).css('z-index', '1');
                    panel = 0;
                });
                currPage = itemPage;
				$(".itemsMenu ul li a").removeClass('active');
				$(".home-link").removeClass('active');
				$(".itemsMenu ul li a.menu"+currPage).addClass('active');
            }
        }
        function backAnim() {
            $('a.backBtn span').animate({ left: "-100%" }, 400, function () {
                $('a.backBtn span').css('left', '100%');
                $('a.backBtn span').animate({ left: "0%" }, 400, function () {
                    $('a.backBtn').css('display', 'none');
                });
            });
        }




      
         <% If Me.PageObjectGetTheme.Id <> Dompe.ThemeConstants.idHomeThemeMobile Then%>
            

        $(window).load(function () {
            $("a[title]").attr("title", function () {
                return $(this).attr("title").toLowerCase();
            });

                var tit = '<%=(Request.Path.ToString().ToLower()).Replace("/", "").Replace("-", " ")%>';  //il request.path deve essere .tolower e nel testo del contenuto tutti i title devono essere riscritti in minuscolo
                $("a[title='" + tit.toLowerCase() + "']").click();
            });
          
        <% End If %>

        $(window).load(function () {
            var _currDomain = '<%=String.Format("http://{0}", Me.ObjectSiteDomain.Domain.ToString)%>';
             var _linkToEsclude = '<%=Healthware.DMP.Configuration.AppSettingsData.InternalUrlsMobile%>';

            bindExternalLinks(_currDomain, _linkToEsclude);

            if (jQuery.cookie('cookie-compliance-user-response')) {
                $('.boxCookies').hide();
            } else {
                $('.boxCookies').show();
            }

            $('#accept-cookie').bind('click', function (e) {
                $(".boxCookies").hide();                
                //e.preventDefault();
                $.cookie('cookie-compliance-user-response', '1', { path: '/' });
                
            });
         });

         function bindExternalLinks(curDomain, linkEsclude) {
             $("#btn_golink").attr("href", '');

             var domArray = linkEsclude.split(";");
             var domainSelector = '';
             

             var i;
             for (i = 0; i < (domArray.length) ; i++) {
                 domainSelector += ":not([href^='" + domArray[i] + "'])";
             };

             //add Current Domain
             domainSelector += ":not([href^='" + curDomain + "'])";

             var urlGo = '';
             
             $("a" + domainSelector + "[href^='http://']").each(function (index) {
                 urlGo = $(this).attr("href");
                 $(this).attr("onclick", "openPopExt('" + urlGo + "')");
                 $(this).attr("href","javascript:void(0);")
             });

             $("a" + domainSelector + "[href^='https://']").each(function (index) {
                 urlGo = $(this).attr("href");
                 $(this).attr("onclick", "openPopExt('" + urlGo + "')");
                 $(this).attr("href", "javascript:void(0);")
             });
            
         };
      
         function openPopExt(urlGo) {
            
             $("#btn_golink").attr("href", urlGo);

             $(".popExt").css('display', 'block');
         }

         function closePopExt() {
             $("#btn_golink").attr("href", '');
             $(".popExt").css('display', 'none');
         }
</script>
<style>


.secondPageLevel2 {
	position: absolute;
	z-index: 1;
	-webkit-tap-highlight-color: rgba(0,0,0,0);
	width: 100%;
	min-height:100%;
	-webkit-transform: translateZ(0);
	-moz-transform: translateZ(0);
	-ms-transform: translateZ(0);
	-o-transform: translateZ(0);
	transform: translateZ(0);
	-webkit-touch-callout: none;
	/*-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;*/
	-webkit-text-size-adjust: none;
	-moz-text-size-adjust: none;
	-ms-text-size-adjust: none;
	-o-text-size-adjust: none;
	text-size-adjust: none;
}</style>
    
	

	
<script type="text/javascript" src="/demo/js/iscroll.js"></script>

<script type="text/javascript">

var myScroll=new Array();


</script>



    <script>
        //myScroll[0] = new IScroll("#panel1", { mouseWheel: false, click:true });
		//myScroll[1] = new IScroll("#panel2", { mouseWheel: false, click:true });
        myScroll[0] = new IScroll("#panel1", { zprobeType:  3,
    mouseWheel: true,
    scrollbars: false,
    bounce: true,
    keyBindings: true,
    invertWheelDirection: false,
    momentum: true,
    fadeScrollbars: false,
    interactiveScrollbars: true,
    resizeScrollbars: true,
    shrinkScrollbars: false,
    click: false,
    preventDefaultException: { tagName:/.*/} });
		myScroll[1] = new IScroll("#panel2", { zprobeType:  3,
    mouseWheel: true,
    scrollbars: false,
    bounce: true,
    keyBindings: true,
    invertWheelDirection: false,
    momentum: true,
    fadeScrollbars: false,
    interactiveScrollbars: true,
    resizeScrollbars: true,
    shrinkScrollbars: false,
    click: false,
    preventDefaultException: { tagName:/.*/} });
		document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);



		function scrollBarHeight() {
		    var panctrl = panel - 1;
		    myScroll[panctrl].refresh();
		    myScroll[panctrl].scrollTo(0, 0);
		}

		function scrollBarHeight2() {
		    var panctrl = panel - 1;
		    myScroll[panctrl].refresh();
		    
		}

    </script>
</div>

    <div class="popExt" style="display:none;">
        <p><%= Me.BusinessDictionaryManager.Read("PLY_MessageBoxLinkExternal", Me.PageObjectGetLang.Id, "PLY_MessageBoxLinkExternal")%></p>
        <div class="btnPop">
            <a href="javascript:void(0);" onclick="closePopExt();" title="Cancella">Cancella</a>
            <a target="_blank" title="Vai" id="btn_golink" onclick="$('.popExt').css('display', 'none');" class="go">Vai</a>
        </div>
    </div>

    <%-- Box Cookies --%>
            <div class="boxCookies" style="display: none;">
                <p>
                    Questo sito utilizza cookie di terza parte. Per informazioni, anche su come bloccare i cookie <a style="color: #e62139;" title="Clicca qui per capire come bloccare i cookie" href="#">clicca qui</a>. Proseguendo nella navigazione del sito web acconsenti all’uso di cookie.
                </p>
                <span>
                    <a id="accept-cookie" href="#" style="color: #e62139;">Ok</a>
                </span>
            </div>

</body>
</html>


                        
                    